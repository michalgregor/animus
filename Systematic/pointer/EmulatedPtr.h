#ifndef Systematic_EmulatedPtr_H_
#define Systematic_EmulatedPtr_H_

#include <Systematic/configure.h>
#include <iostream>

#define SYS_RAW_PTR_SUPPORT(Type)								\
namespace systematic {											\
	template<>													\
	inline Type* getRawPointer(Type& ref) {						\
		return ref.rawPointer();								\
	}															\
	template<>													\
	inline const Type* getRawPointer(const Type& ref) {		\
		return ref.rawPointer();								\
	}															\
} //namespace systematic

namespace systematic {

/**
 * Returns a raw pointer to the ReferenceType object. A specialisation is
 * required for types with overloaded operator&().
 */
template<class ReferenceType>
inline ReferenceType* getRawPointer(ReferenceType& ref) {
	return &ref;
}

/**
 * @class EmulatedPair_Pointer
 * A type that serves as emulated pointer. Its operator->() returns a pointer
 * to the specified reference type (which is stored by value in the EmulatedPtr).
 *
 * \note For types with overloaded operator&(), provide a specialisation for
 * getRawPointer().
 *
 * @tparam ReferenceType A type that behaves as a reference. It must be
 * copy constructible.
 */

template<class ReferenceType>
class EmulatedPtr {
private:
	template<class U>
	friend class EmulatedPtr;

	// The reference type object.
	ReferenceType _ref;

public:
	ReferenceType* operator->() const {return const_cast<ReferenceType*>(getRawPointer(_ref));}
	ReferenceType operator*() const {return _ref;}

	/**
	 * Assignment operator. Creates a pointer that points to the same object
	 * as obj. (By copying the reference.)
	 */
	EmulatedPtr& operator=(const EmulatedPtr& obj) {
		ReferenceType* adr = getRawPointer(_ref);

		_ref.~ReferenceType();
		new(adr) ReferenceType(obj._ref);

		return *this;
	}

	/**
	 * Assignment operator. Creates a pointer that points to the same object
	 * as obj. (By copying the reference.)
	 */
	template<class U>
	EmulatedPtr& operator=(const EmulatedPtr<U>& obj) {
		ReferenceType* adr = getRawPointer(_ref);

		_ref.~ReferenceType();
		new(adr) ReferenceType(obj._ref);

		return *this;
	}

	/**
	 * Copy constructor. Creates a pointer that points to the same object
	 * as obj. (By copying the reference.)
	 */
	EmulatedPtr(const EmulatedPtr& obj): _ref(obj._ref) {}

	/**
	 * Copy constructor. Creates a pointer that points to the same object
	 * as obj. (By copying the reference.)
	 */
	template<class U>
	EmulatedPtr(const EmulatedPtr<U>& obj): _ref(obj._ref) {}

	/**
	 * Creates a pointer from the specified reference type.
	 */
	EmulatedPtr(const ReferenceType& ref): _ref(ref) {}
};

} //namespace systematic

#endif //Systematic_EmulatedPtr_H_
