#ifndef SMART_PTR_H_
#define SMART_PTR_H_

#include <Systematic/configure.h>

#include <boost/intrusive_ptr.hpp>

#ifdef SYS_USE_BOOST_SHARED_PTR
	#include <boost/shared_ptr.hpp>
	#include <boost/enable_shared_from_this.hpp>
	#include <boost/make_shared.hpp>
#else
	#include <memory>
#endif

namespace systematic {

using boost::intrusive_ptr;
using std::unique_ptr;

// provide make_unique if C++14 not available
#if __cplusplus == 201402L // C++14
    using std::make_unique;
#else // C++11
    template < typename T, typename... CONSTRUCTOR_ARGS >
    std::unique_ptr<T> make_unique( CONSTRUCTOR_ARGS&&... constructor_args )
    { return std::unique_ptr<T>( new T( std::forward<CONSTRUCTOR_ARGS>(constructor_args)... ) ); }
#endif // __cplusplus == 201402L

#ifdef SYS_USE_BOOST_SHARED_PTR
	using boost::shared_ptr;
	using boost::enable_shared_from_this;
	using boost::static_pointer_cast;
	using boost::dynamic_pointer_cast;
	using boost::const_pointer_cast;
	using boost::make_shared;
#else
	using std::shared_ptr;
	using std::enable_shared_from_this;
	using std::static_pointer_cast;
	using std::dynamic_pointer_cast;
	using std::const_pointer_cast;
	using std::make_shared;
#endif

    struct null_deleter {
		void operator()(void const *) const {}
    };

    template<typename T> shared_ptr<T> raw_ptr(T* raw) {
    	return shared_ptr<T>(raw, null_deleter());
    }

	/**
	 * An object of this class (there is a predefined global object
	 * shared_nullptr) can be cast to any type of NULL shared_ptr.
	 */
	class SharedNullPtr {
	public:
		template<class T>
		operator shared_ptr<T>() const {
			return shared_ptr<T>();
		}
	};

	//! An object of SharedNullPtr.
	extern SharedNullPtr shared_nullptr;

} //namespace systematic

#endif /* SMART_PTR_H_ */
