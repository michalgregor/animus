#ifndef SYS_SERIALIZATION_H_INCLUDED
#define SYS_SERIALIZATION_H_INCLUDED

//These headers needs to be included before boost serialization includes if
//extended_type_info_TypeInfo is to be used as the default extended_type_info
//implementation.

#include "type_id/detail/extended_type_info_TypeInfo.h"
#include "export.h"

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include "serialization/TypeID.h"
#include "serialization/boost_intrusive_ptr.h"
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

#include "type_info.h"
#include "serialization/archive.h"

#endif // SYS_SERIALIZATION_H_INCLUDED
