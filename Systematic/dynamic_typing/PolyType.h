#ifndef Systematic_PolyType_H_
#define Systematic_PolyType_H_

#include "../system.h"
#include "PolyConverter.h"

namespace systematic {

/**
 * @class PolyType
 *
 * A class template built atop PolyConverter - it provides an additional
 * parameter ConverterProvider. ConverterProvider is a class, which provides
 * method static ConverterRegister<Context>* ConverterProvider::provide() that
 * provides PolyType with its converter register so that it does not have to
 * be specified manually upon construction.
 */
SYS_WNONVIRT_DTOR_OFF
template<class Container, bool AlwaysCopy, class ConverterProvider, class Context = void>
class PolyType: public PolyConverter<Container, AlwaysCopy, Context> {
SYS_WNONVIRT_DTOR_ON
private:
	typedef PolyConverter<Container, AlwaysCopy, Context> base_type;

private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int /*version*/) {
    	ar & boost::serialization::base_object<base_type>(*this);
    }

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & boost::serialization::base_object<base_type>(*this);
	}

public:
	PolyType convert(const TypeID& type, Context& context) const {
		return base_type::convert(type, context);
	}

	template<class Type>
	PolyType convert(Context& context) const {
		return convert(type_id<Type>(), context);
	}

protected:
	PolyType(const base_type& obj): base_type(obj) {}

public:
	/**
	 * Constructor.
	 */
	PolyType():	base_type(ConverterProvider::provide()) {}

	/**
	 * Constructor.
	 * @param data The data to be contained.
	 */
	template<class Type>
	explicit PolyType(const Type& data): base_type(data, ConverterProvider::provide()) {}
};

//PolyType specialization for void Context.
SYS_WNONVIRT_DTOR_OFF
template<class Container, bool AlwaysCopy, class ConverterProvider>
class PolyType<Container, AlwaysCopy, ConverterProvider, void>: public PolyConverter<Container, AlwaysCopy, void> {
SYS_WNONVIRT_DTOR_ON
private:
	typedef PolyConverter<Container, AlwaysCopy, void> base_type;

private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int /*version*/) {
    	ar & boost::serialization::base_object<base_type>(*this);
    }

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & boost::serialization::base_object<base_type>(*this);
	}

public:
	PolyType convert(const TypeID& type) const {
		return base_type::convert(type);
	}

	template<class Type>
	PolyType convert() const {
		return convert(type_id<Type>());
	}

protected:
	PolyType(const base_type& obj): base_type(obj) {}

public:
	/**
	 * Constructor.
	 */
	PolyType():	base_type(ConverterProvider::provide()) {}

	/**
	 * Constructor.
	 * @param data The data to be contained.
	 */
	template<class Type>
	explicit PolyType(const Type& data): base_type(data, ConverterProvider::provide()) {}
};

} //namespace systematic

SYS_REGISTER_TEMPLATENAME_DETAILED(systematic::PolyType, 4, (SYS_TPARAM_CLASS, SYS_TPARAM_BOOL, SYS_TPARAM_CLASS, SYS_TPARAM_CLASS))

#endif //Systematic_PolyType_H_
