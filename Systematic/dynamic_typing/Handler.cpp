#include "Handler.h"

namespace systematic {

/**
 * Boost serialization.
 */

void Handler::serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

void Handler::serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

/**
 * Default constructor.
 */

Handler::Handler(){}

/**
 * Empty body of the pure virtual destructor.
 */

Handler::~Handler() {}

//---------------------specialization for void----------------------------------

/**
 * Returns a NULL pointer.
 */

void* GenericHandler<void>::getPtr() {return NULL;}

/**
 * Returns a NULL pointer.
 */

void* GenericHandler<void>::getDataCopy() const {
    return NULL;
}

/**
 * Returns the TypeID of the contained data type, in this case void.
 */

const TypeID& GenericHandler<void>::getType() const {
	static TypeID id = type_id<void>();
	return id;
}

/**
 * Creates a copy of the Handler. There is no data to be copied of course.
 */

GenericHandler<void>* GenericHandler<void>::copy() const {
	return new GenericHandler<void>();
}

/**
 * Default constructor.
 */

GenericHandler<void>::GenericHandler() {}

/**
 * Virtual destructor.
 */

GenericHandler<void>::~GenericHandler() {
	SYS_EXPORT_INSTANCE();
}

} //namespace systematic
