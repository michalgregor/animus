#ifndef Systematic_TypeConverter_H_
#define Systematic_TypeConverter_H_

#include "Handler.h"
#include "../system.h"

namespace systematic {

/**
* @class TypeConverter
* @author Michal Gregor
* Used by ConverterRegister (and thus by PolyContainer) to carry out conversions
* between various types of objects for which only their TypeID is known.
**/
template<class Context>
class TypeConverter {
private:
	friend class boost::serialization::access;

    void serialize(IArchive& /*ar*/, const unsigned int /*version*/) {}
	void serialize(OArchive& /*ar*/, const unsigned int /*version*/) {}

public:
	/**
	* Converts object obj from certain data type to another. The returned
	* void pointer should always point to newly allocated memory. The original
	* object is not deleted.
	**/

	virtual Handler* operator()(Handler* obj, Context& context) const = 0;

	/**
	* Empty body of the virtual destructor.
	**/
	virtual ~TypeConverter() {
		SYS_EXPORT_INSTANCE()
	}
};

// TypeConverter specialization for void.
template<>
class SYS_API TypeConverter<void> {
private:
	friend class boost::serialization::access;

    void serialize(IArchive& /*ar*/, const unsigned int /*version*/) {}
	void serialize(OArchive& /*ar*/, const unsigned int /*version*/) {}

public:
	/**
	* Converts object obj from certain data type to another. The returned
	* void pointer should always point to newly allocated memory. The original
	* object is not deleted.
	**/

	virtual Handler* operator()(Handler* obj) const = 0;

	/**
	* Empty body of the virtual destructor.
	**/

	virtual ~TypeConverter() {}
};

//------------------------------------------------------------------------------
//----------------------------Generic implementation.---------------------------
//------------------------------------------------------------------------------

/**
* @class GenericValueConverter
* @author Michal Gregor
* A generic implementation of TypeConverter interface. Requires type To to be
* copy-constructible from From type.
**/
template<class From, class To, class Context = void>
class GenericValueConverter: public TypeConverter<Context> {
private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int /*version*/) {
    	ar & boost::serialization::base_object<TypeConverter<Context> >(*this);
    }

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & boost::serialization::base_object<TypeConverter<Context> >(*this);
	}

public:
	virtual Handler* operator()(Handler* obj, Context& context) const;
	virtual ~GenericValueConverter() {}
};

/**
* This function assumes that void* is a pointer to From type, copy constructs
* a new object of To type from it and returns a void pointer to that object.
**/
template<class From, class To, class Context>
Handler* GenericValueConverter<From, To, Context>::operator()(Handler* obj, Context& UNUSED(context)) const {
	return new GenericHandler<To>(*static_cast<From*>(obj->getPtr()));
}

//GenericValueConverter specialization for void.
template<class From, class To>
class GenericValueConverter<From, To, void>: public TypeConverter<void> {
private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int /*version*/) {
    	ar & boost::serialization::base_object<TypeConverter<void> >(*this);
    }

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & boost::serialization::base_object<TypeConverter<void> >(*this);
	}

public:
	virtual Handler* operator()(Handler* obj) const;

	virtual ~GenericValueConverter() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* This function assumes that void* is a pointer to From type, copy constructs
* a new object of To type from it and returns a void pointer to that object.
**/
template<class From, class To>
Handler* GenericValueConverter<From, To, void>::operator()(Handler* obj) const {
	return new GenericHandler<To>(*static_cast<From*>(obj->getPtr()));
}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::TypeConverter, 1)
SYS_EXPORT_TEMPLATE(systematic::GenericValueConverter, 3)

#endif //Systematic_TypeConverter_H_
