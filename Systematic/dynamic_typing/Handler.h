#ifndef HANDLER_H_INCLUDED
#define HANDLER_H_INCLUDED

#include "../system.h"
#include "../serialization.h"
#include "../type_info.h"

#include <type_traits>
#include <boost/mpl/int.hpp>
#include "../utils/RefCounterVirtual.h"

namespace systematic {

/**
* @class Handler
* A handler of a dynamically allocated object with manually counted references.
* The counter is incremented using add() and decremented using remove(). When
* count is 1 and the object that holds the Handler is destructed, it should
* delete the handler. The point is to avoid some of the overhead of shared_ptrs
* when operating in controlled environment.
*
* This class should not be used to handle classic c-style arrays as it only uses
* delete, not delete[].
**/
class SYS_API Handler: public RefCounterVirtual {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

	//! Copy construction disallowed.
	Handler(const Handler&);

	//! Assignment disallowed.
    Handler& operator=(const Handler&);

public:
	/**
	 * Returns a void pointer to the contained data.
	 */
	virtual void* getPtr() = 0;

	/**
	* Makes a new copy of the contained data and returns a pointer to it.
	* Take care to delete such data using the delete somewhere so as not
	* to leak memory.
	*/
	virtual void* getDataCopy() const = 0;

	/**
	 * Creates a new copy of the Handler and its contained data and returns
	 * a pointer to it.
	 */
	virtual Handler* copy() const = 0;

	/**
	 * Returns a TypeID of the contained data type.
	 */

	virtual const TypeID& getType() const = 0;

	Handler();
	virtual ~Handler() = 0;
};

/**
* @class GenericHandler
* A generic template-based implementation of Handler.
* @param Type Type of the contained data. It is required that this type be
* copy constructible.
* @param Enable An auxiliary argument used to implement type traits. This
* parameter should never be explicitly specified.
**/
template<class Type, class Enable = void>
class GenericHandler: public Handler {
private:
	Type* _data;

private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Handler>(*this);
		ar & _data;
	}

    GenericHandler(const GenericHandler&);
    GenericHandler& operator=(const GenericHandler&);

public:
	virtual void* getPtr() {return _data;}

	void setPtr(Type* data) {
		_data = data;
	}

	virtual void* getDataCopy() const {
	    if(_data) return new Type(*_data);
	    else return NULL;
	}

	virtual const TypeID& getType() const {
		static TypeID id = type_id<Type>();
		return id;
	}

	virtual GenericHandler<Type>* copy() const {
		GenericHandler<Type>* handler = new GenericHandler<Type>();
		if(_data) handler->_data = new Type(*_data);
		return handler;
	}

	GenericHandler(): _data(NULL) {}
	GenericHandler(const Type& data): _data(new Type(data)) {}

	virtual ~GenericHandler() {
		SYS_EXPORT_INSTANCE();
		delete _data;
	}
};

//---------SPECIALIZATION FOR PRIMITIVE TYPES OTHER THAN VOID-------------------

using boost::serialization::implementation_level;

/**
* @class GenericHandler
* A per-type implementation of Handler. Specialization for primitive types other
* than void (void needs no serialization) as these are not tracked and thus
* cannot be serialized by pointer.
**/
template<class Type>
class GenericHandler<
		Type,
		typename std::enable_if<
		/* If Type is a primitive type and not void */
			std::is_same<
				typename implementation_level<Type>::type,
				typename boost::mpl::int_<boost::serialization::primitive_type>
			>::value and
			!std::is_same<Type, void>::value
		>::type
>: public Handler {
private:
	Type _data;

private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Handler>(*this);
		ar & _data;
	}

    GenericHandler(const GenericHandler&);
    GenericHandler& operator=(const GenericHandler&);

public:
	virtual void* getPtr() {return &_data;}

	virtual void* getDataCopy() const {
	    return new Type(_data);
	}

	virtual const TypeID& getType() const {
		static TypeID id = type_id<Type>();
		return id;
	}

	virtual GenericHandler<Type>* copy() const {
		return new GenericHandler<Type>(_data);
	}

	GenericHandler(): _data() {}
	GenericHandler(const Type& data): _data(data) {}

	virtual ~GenericHandler() {
		SYS_EXPORT_INSTANCE();
	}
};

//--------------------------SPECIALIZATION FOR VOID-----------------------------
template<>
class SYS_API GenericHandler<void, void>: public Handler {
private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Handler>(*this);
    }

    GenericHandler(const GenericHandler&);
    GenericHandler& operator=(const GenericHandler&);

public:
	virtual void* getPtr();
	virtual void* getDataCopy() const;
	virtual const TypeID& getType() const;
	virtual GenericHandler<void>* copy() const;
	GenericHandler();
	virtual ~GenericHandler();
};

} //namespace systematic

SYS_EXPORT_CLASS(systematic::Handler)
SYS_EXPORT_TEMPLATE(systematic::GenericHandler, 2)

#endif // HANDLER_H_INCLUDED
