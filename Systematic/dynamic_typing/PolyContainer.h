#ifndef POLYCONTAINER_H_INCLUDED
#define POLYCONTAINER_H_INCLUDED

#include "../system.h"
#include "../type_info.h"
#include "Handler.h"

namespace systematic {

/**
* @class PolyContainer
* @author Michal Gregor
* @brief This class is used as a generic container. It may contain any type
* that can be identified by the selected id provider.
*
* If user attempts to retrieve a different type
* than contained an exception is thrown.
*
* The contained object can be accessed by getData<>() or by a cast. Only
* const reference access is allowed. One has to explicitly ask for non-const
* access as that invokes the copy-on-access mechanism. Use method access() to
* get non-const access.
*
* PolyContainer should generally not be accessed from multiple threads - while
* one thread obtains a const reference to the contained data, another may get
* non-const access thus invoking the copy-on-access mechanism. The first thread
* then holds a reference to data that can potentially be deleted at any time
* (if the other PolyContainers that hold them are destroyed).
*
* Non-const methods are not thread-safe.
*
* If the container is to serialize properly, the contained type needs to be
* boost serializable as well. If this is not the case, at least an empty
* serialization function must be provided in order to build the code. This
* can easily be generated using macros provided in serialization/fake_serialize.h.
**/

class PolyAccessor;

class SYS_API PolyContainer {
private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	friend class PolyAccessor;

protected:
	//! Handler to the contained data.
	intrusive_ptr<Handler> _handler;

	template<class Type>
	Type& getNonConstData();

public:
	TypeID getType() const;

//-------------------------Reference accessors----------------------------------

	template<class Type>
	const Type& getData() const;

	template<class Type>
	shared_ptr<Type> getDataCopy() const;

	template<class Type>
	void setData(const Type& data);

	template<class Type>
	void setDataFromPtr(Type* dataPtr);

	void setData();

	template<class Type>
	PolyContainer& operator=(const Type& data);

	PolyAccessor access();

//------------------------------Other-------------------------------------------

	PolyContainer copy() const;

	PolyContainer();

	PolyContainer& operator=(const PolyContainer& container);
	PolyContainer(const PolyContainer& container);

	template<class Type>
	explicit PolyContainer(const Type& data);

	~PolyContainer();
};

/**
* Assignment operator that acts like an alias of setData<Type>(), the only
* exception being Type == PolyContainer in which case this work as regular
* assignment operator.
**/

template<class Type>
PolyContainer& PolyContainer::operator=(const Type& data) {
	setData<Type>(data);
	return *this;
}

/**
* Returns a reference to the contained data. If data of a different than
* contained type is asked for an exception is thrown. This method implements
* the copy on access mechanism.
**/

template<class Type>
Type& PolyContainer::getNonConstData() {
	if(!_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
	if(_handler->getType() != type_id<Type>())
		throw TracedError_InvalidArgument("Data is not of the type you ask for.");
	//copy on access if there are several shared_ptrs to this data
	if(!_handler->unique()) _handler = intrusive_ptr<Handler>(_handler->copy());
	return *static_cast<Type*>(_handler->getPtr());
}

/**
* Returns a reference to the contained data. If data of a different than
* contained type is asked for an exception is thrown.
**/

template<class Type>
const Type& PolyContainer::getData() const {
	if(!_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
	if(_handler->getType() != type_id<Type>())
		throw TracedError_InvalidArgument("Data is not of the type you ask for.");
	return *static_cast<Type*>(_handler->getPtr());
}

/**
* Creates a new copy of the container data and returns a pointer to it. If data
* of a different than contained type is asked for, an exception is thrown.
*/

template<class Type>
shared_ptr<Type> PolyContainer::getDataCopy() const {
    if(!_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
    if(_handler->getType() != type_id<Type>())
    		throw TracedError_InvalidArgument("Data is not of the type you ask for.");
    return shared_ptr<Type>(static_cast<Type*>(_handler->getDataCopy()));
}

/**
* Sets data by a const reference. Data must be of a type that the provided
* TypeID can identify - if it is not such call will result in a compile error.
**/
template<class Type>
void PolyContainer::setData(const Type& data) {
	_handler = intrusive_ptr<Handler>(new GenericHandler<Type>(data));
}

/**
* Sets data by setting the pointer to it directly.
* \warning PolyContainer takes ownership of the data and will manage their
* removal. It is required that the data be allocated using new, and deletable
* using operator delete.
**/
template<class Type>
void PolyContainer::setDataFromPtr(Type* dataPtr) {
	GenericHandler<Type> handler = new GenericHandler<Type>();
	handler->setPtr(dataPtr);
	_handler = intrusive_ptr<Handler>(handler);
}

/**
* Template copy constructor that allows PolyContainer to be initialized from
* any data type directly, rather than being initialized using a default
* constructor first.
**/

template<class Type>
PolyContainer::PolyContainer(const Type& data): _handler(NULL) {
	setData<Type>(data);
}

//------------------------------POLYACCESSOR------------------------------------

/**
* @class PolyAccessor
* This class provides explicit non-const copy-on-access access to PolyContainer
* data. It is generally not to be constructed directly. Instances of the class
* are returned by Polycontainer::access().
*
* Objects of this class should be created as temporary objects. They should not
* be stored, copied, serialized, etc.
**/

class SYS_API PolyAccessor {
private:
	//! Handler to the contained data.
	PolyContainer* _container;

public:
	TypeID getType() const {
		if(!_container) throw TracedError_InvalidPointer("NULL pointer to container.");
		return _container->getType();
	}

	template<class Type>
	Type& getData();

	template<class Type>
	const Type& getData() const;

	template<class Type>
	operator const Type&() const;

public:
    PolyAccessor(): _container(NULL) {}
    PolyAccessor(PolyContainer* container): _container(container) {}

    PolyAccessor& operator=(const PolyAccessor& obj) {
    	_container = obj._container;
    	return *this;
    }

	PolyAccessor(const PolyAccessor& obj): _container(obj._container) {}
};

/**
* Cast operator. Alias of getData<Type>().
**/

template<class Type>
PolyAccessor::operator const Type&() const {
	if(!_container) throw TracedError_InvalidPointer("NULL pointer to container.");
	return _container->getNonConstData<Type>();
}

/**
* Returns a reference to the contained data. If data of a different than
* contained type is asked for an exception is thrown. This method implements
* the copy on access mechanism.
**/

template<class Type>
Type& PolyAccessor::getData() {
	if(!_container) throw TracedError_InvalidPointer("NULL pointer to container.");
	return _container->getNonConstData<Type>();
}

/**
* Returns a reference to the contained data. If data of a different than
* contained type is asked for an exception is thrown.
**/

template<class Type>
const Type& PolyAccessor::getData() const {
	if(!_container) throw TracedError_InvalidPointer("NULL pointer to container.");
	return _container->getNonConstData<Type>();
}

} //namespace systematic

#endif // POLYCONTAINER_H_INCLUDED
