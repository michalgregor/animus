#ifndef Systematic_CastRegister_H_
#define Systematic_CastRegister_H_

#include <set>
#include <type_traits>

namespace systematic {

/**
 * @class CastRegister
 *
 * A class used to track castability relationships between types.
 *
 * Types can be registered as castable to one another, and this relationship
 * can later be queried using their corresponding TypeID representations.
 *
 * When SourceType == DestinationType, the relationship is implied, and does
 * not have to be registered explicitly.
 */
template<class TypeID>
class CastRegister {
private:
	typedef std::pair<TypeID, TypeID> type_pair;
	std::set<type_pair> _pairs;

public:
	//! Returns whether the register is empty or not.
	bool empty() const {
		return _pairs.empty();
	}

	/**
	 * Registers that type SourceType is castable to type DestinationType.
	 * It is required that SourceType* be convertible to DestinationType*.
	 *
	 * @note If a previous castability relationship between the types exists,
	 * it is replaced.
	 *
	 * @sa makeUnsafeCastable.
	 */
	template<class SourceType, class DestinationType>
	void makeCastable() {
		static_assert(std::is_convertible<SourceType*, DestinationType*>::value, "SourceType* must be castable to DestinationType*.");
		_pairs.insert(std::make_pair(systematic::type_id<SourceType, TypeID>(), systematic::type_id<DestinationType, TypeID>()));
	}

	/**
	 * Registers that type SourceType is castable to type DestinationType.
	 * This version of the function omits the castability check. Use with
	 * caution.
	 *
	 * @sa makeCastable.
	 */
	template<class SourceType, class DestinationType>
	void makeUnsafeCastable() {
		_pairs.insert(std::make_pair(systematic::type_id<SourceType, TypeID>(), systematic::type_id<DestinationType, TypeID>()));
	}

	/**
	 * Removes the castability relationship between type SourceType and type
	 * DestinationType from the register if such relationship exists. Otherwise
	 * does nothing.
	 */
	template<class SourceType, class DestinationType>
	void makeUncastable() {
		auto iter = _pairs.find(std::make_pair(systematic::type_id<SourceType, TypeID>(), systematic::type_id<DestinationType, TypeID>()));
		if(iter != _pairs.end()) _pairs.erase(iter);
	}

	/**
	 * Returns whether SourceType is registered as castable to DestinationType.
	 * If SourceType is the same as DestinationType, always returns true.
	 */
	template<class SourceType, class DestinationType>
	bool is_castable() {
		return is_castable(systematic::type_id<SourceType, TypeID>(), systematic::type_id<DestinationType, TypeID>());
	}

	/**
	 * Returns whether SourceType is registered as castable to DestinationType.
	 * If SourceType is the same as DestinationType, always returns true.
	 */
	bool is_castable(const TypeID& SourceType, const TypeID& DestinationType) {
		if(SourceType == DestinationType) return true;
		auto iter = _pairs.find(std::make_pair(SourceType, DestinationType));
		return iter != _pairs.end();
	}

public:
	CastRegister(): _pairs() {}
};

} //namespace systematic

#endif //Systematic_CastRegister_H_
