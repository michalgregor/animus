#ifndef POLYCONVERTOR_H_INCLUDED
#define POLYCONVERTOR_H_INCLUDED

#include "ConverterRegister.h"
#include "../system.h"

namespace systematic {

/**
* @class PolyConverter
* Provides a template wrapper for PolyContainer, PolyHandler, ... that provides
* methods convertible(), convert() and other. You should not cast from this
* class to the Container class as Container classes do not have virtual
* destructors.
*
* @tparam Container Type of the container to wrap.
* @tparam AlwaysCopy If there is a conversion from type1 to type2 such that
* type1 == type2, the data may either be copied or alternatively only the
* pointer may be copied. If AlwaysCopy == true, the data itself is copied.
* @tparam Context If context-dependent types are contained in the Container,
* the Context type that will be passed to the conversion function should
* be specified here. The default value - void - means that the types are not
* context dependent.
**/
SYS_WNONVIRT_DTOR_OFF
template<class Container, bool AlwaysCopy, class Context = void>
class PolyConverter: public Container {
SYS_WNONVIRT_DTOR_ON
private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int /*version*/) {
    	ar & boost::serialization::base_object<Container>(*this);
    	ar & _converterRegister;
    }

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & boost::serialization::base_object<Container>(*this);
		ar & _converterRegister;
	}

protected:
	//! Pointer to the ConverterRegister that is to be used to convert objects.
	ConverterRegister<Context>* _converterRegister;

public:
	//--------conversion----------------
	bool convertible(const TypeID& type) const {
		return _converterRegister->convertible(Container::_handler->getType(), type);
	}

	template<class Type>
	bool convertible() const {
		return convertible(type_id<Type>());
	}

	PolyConverter<Container, AlwaysCopy, Context> convert(const TypeID& type, Context& context) const {
		if(!Container::_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
		PolyConverter<Container, AlwaysCopy, Context> container(_converterRegister);

		if(AlwaysCopy) {
			container._handler = intrusive_ptr<Handler>(_converterRegister->convert(Container::_handler.get(), type, context)->copy());
		} else {
			container._handler = intrusive_ptr<Handler>(_converterRegister->convert(Container::_handler.get(), type, context));
		}

		return container;
	}

	template<class Type>
	PolyConverter<Container, AlwaysCopy, Context> convert(Context& context) const {
		return convert(type_id<Type>(), context);
	}

	//--------assignment, copying, construction-------

	template<class Type>
	PolyConverter<Container, AlwaysCopy, Context>& operator=(const Type& data) {
		Container::operator=(data);
		return *this;
	}

protected:
	/**
	 * Default constructor: for use in serialization.
	 */
	PolyConverter(): Container(), _converterRegister(nullptr) {}

public:
	/**
	 * Constructor.
	 * @param converterRegister The converter register used to convert objects.
	 */
	PolyConverter(ConverterRegister<Context>* converterRegister):
		Container(), _converterRegister(converterRegister) {}

	/**
	 * Constructor.
	 * @param data The data to be contained.
	 * @param converterRegister The converter register used to convert objects.
	 */
	template<class Type>
	explicit PolyConverter(const Type& data, ConverterRegister<Context>* converterRegister):
		Container(data), _converterRegister(converterRegister) {}
};

//-----------------------SPECIALIZATION FOR VOID--------------------------------

template<class Container, bool AlwaysCopy>
class PolyConverter<Container, AlwaysCopy, void>: public Container {
private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int /*version*/) {
    	ar & boost::serialization::base_object<Container>(*this);
    	ar & _converterRegister;
    }

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & boost::serialization::base_object<Container>(*this);
		ar & _converterRegister;
	}

private:
	//! Pointer to the ConverterRegister that is to be used to convert objects.
	ConverterRegister<void>* _converterRegister;

public:
	//--------conversion----------------

	bool convertible(const TypeID& type) const {
		return _converterRegister->convertible(Container::_handler->getType(), type);
	}

	template<class Type>
	bool convertible() const {
		return convertible(type_id<Type>());
	}

	PolyConverter<Container, AlwaysCopy, void> convert(const TypeID& type) const {
		if(!Container::_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
		PolyConverter<Container, AlwaysCopy, void> container(_converterRegister);

		if(AlwaysCopy) {
			container._handler = intrusive_ptr<Handler>(_converterRegister->convert(Container::_handler.get(), type)->copy());
		} else {
			container._handler = intrusive_ptr<Handler>(_converterRegister->convert(Container::_handler.get(), type));
		}

		return container;
	}

	template<class Type>
	PolyConverter<Container, AlwaysCopy, void> convert() const {
		return convert(type_id<Type>());
	}

	//--------assignment, copying, construction-------

	template<class Type>
	PolyConverter<Container, AlwaysCopy, void>& operator=(const Type& data) {
		Container::operator=(data);
		return *this;
	}

protected:
	/**
	 * Default constructor: for use in serialization.
	 */
	PolyConverter(): Container(), _converterRegister(nullptr) {}

public:
	/**
	 * Constructor.
	 * @param converterRegister The converter register used to convert objects.
	 */
	PolyConverter(ConverterRegister<void>* converterRegister):
		Container(), _converterRegister(converterRegister) {}

	/**
	 * Constructor.
	 * @param data The data to be contained.
	 * @param converterRegister The converter register used to convert objects.
	 */
	template<class Type>
	explicit PolyConverter(const Type& data, ConverterRegister<void>* converterRegister):
		Container(data), _converterRegister(converterRegister) {}
};

} //namespace systematic

SYS_REGISTER_TEMPLATENAME_DETAILED(systematic::PolyConverter, 3, (SYS_TPARAM_CLASS, SYS_TPARAM_BOOL, SYS_TPARAM_CLASS))

#endif // POLYCONVERTOR_H_INCLUDED
