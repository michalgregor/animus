#ifndef POLYHANDLER_H_INCLUDED
#define POLYHANDLER_H_INCLUDED

#include "../system.h"
#include "../type_info.h"
#include "Handler.h"

#include <boost/intrusive_ptr.hpp>

namespace systematic {

using boost::intrusive_ptr;

/**
* @class PolyHandler
* This is a generic type container similar to PolyContainer. The difference is
* that PolyContainer does not allow non-const access to data - one has to
* explicitly ask for it first in which case the container makes its own copy.
* PolyHandler on the other hand does allow non-const access, does not implement
* copy-on-access and every PolyHandler created using the copy constructor or
* copied using operator= points to the same piece of data.
*
* Non-const methods are not thread-safe.
*
* If the container is to serialize properly, the contained type needs to be
* boost serializable as well. If this is not the case, at least an empty
* serialization function must be provided in order to build the code. This
* can easily be generated using macros provided in serialization/fake_serialize.h.
**/
class SYS_API PolyHandler {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	//! Handler to the contained data.
	intrusive_ptr<Handler> _handler;

public:
	TypeID getType() const;

//-------------------------Reference accessors----------------------------------

	template<class Type>
	Type& getData();

	template<class Type>
	const Type& getData() const;

	template<class Type>
	Type* getDataPtr();

	template<class Type>
	const Type* getDataPtr() const;

	template<class Type>
	shared_ptr<Type> getDataCopy() const;

	template<class Type>
	void setData(const Type& data);

	void setData();

	template<class Type>
	operator Type&();

	template<class Type>
	operator const Type&() const;

	template<class Type>
	PolyHandler& operator=(const Type& data);

//------------------------------Other-------------------------------------------

	PolyHandler copy() const;

	PolyHandler();

	PolyHandler& operator=(const PolyHandler& container);
	PolyHandler(const PolyHandler& container);

	template<class Type>
	explicit PolyHandler(const Type& data);

	~PolyHandler();
};

/**
* Cast operator. Alias of getData<Type>().
**/

template<class Type>
PolyHandler::operator Type&() {
	return getData<Type>();
}

/**
* Cast operator. Alias of getData<Type>().
**/

template<class Type>
PolyHandler::operator const Type&() const {
	return getData<Type>();
}

/**
* Assignment operator that acts like an alias of setData<Type>(), the only
* exception being Type == PolyHandler in which case this work as regular
* assignment operator.
**/

template<class Type>
PolyHandler& PolyHandler::operator=(const Type& data) {
	setData<Type>(data);
	return *this;
}

/**
* Returns a reference to the contained data. If data of a different than
* contained type is asked for an exception is thrown. This method implements
* the copy on access mechanism.
**/

template<class Type>
Type& PolyHandler::getData() {
	if(!_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
	if(_handler->getType() != type_id<Type>())
		throw TracedError_InvalidArgument("Data is not of the type you ask for.");
	return *static_cast<Type*>(_handler->getPtr());
}

/**
* Returns a reference to the contained data. If data of a different than
* contained type is asked for an exception is thrown.
**/

template<class Type>
const Type& PolyHandler::getData() const {
	if(!_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
	if(_handler->getType() != type_id<Type>())
		throw TracedError_InvalidArgument("Data is not of the type you ask for.");
	return *static_cast<Type*>(_handler->getPtr());
}

/**
* Returns a pointer to the contained data. If data of a different than
* contained type is asked for an exception is thrown.
**/
template<class Type>
Type* PolyHandler::getDataPtr() {
	return &getData<Type>();
}

/**
* Returns a pointer to the contained data. If data of a different than
* contained type is asked for an exception is thrown.
**/
template<class Type>
const Type* PolyHandler::getDataPtr() const {
	return &getData<Type>();
}

/**
* Creates a new copy of the container data and returns a pointer to it. If data
* of a different than contained type is asked for, an exception is thrown.
*/

template<class Type>
shared_ptr<Type> PolyHandler::getDataCopy() const {
	if(!_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
	if(_handler->getType() != type_id<Type>())
		throw TracedError_InvalidArgument("Data is not of the type you ask for.");
	return shared_ptr<Type>(static_cast<Type*>(_handler->getDataCopy()));
}

/**
* Sets data by a const reference. Data must be of a type that the provided
* TypeID can identify - if it is not such call will result in a compile error.
**/

template<class Type>
void PolyHandler::setData(const Type& data) {
	_handler = intrusive_ptr<Handler>(new GenericHandler<Type>(data));
}

/**
* Template copy constructor that allows PolyHandler to be initialized from
* any data type directly, rather than being initialized using a default
* constructor first.
**/

template<class Type>
PolyHandler::PolyHandler(const Type& data): _handler() {
	setData<Type>(data);
}

} //namespace systematic

#endif // POLYHANDLER_H_INCLUDED
