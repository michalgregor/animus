#include "PolyContainer.h"

namespace systematic {

/**
 * Boost serialization.
 */

void PolyContainer::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _handler;
}

void PolyContainer::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _handler;
}

/**
* Returns id of the contained type as given by TypeID.
**/

TypeID PolyContainer::getType() const {
	if(!_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
	return _handler->getType();
}

/**
* Provides non-const copy-on-access accessor.
**/

PolyAccessor PolyContainer::access() {
	return PolyAccessor(this);
}

/**
 * Resets the data to void.
 */

void PolyContainer::setData() {
	_handler = intrusive_ptr<Handler>(new GenericHandler<void>());
}

/**
* Specialization of assignment operator template for PolyContainer so that it
* works like a standard assignment operator.
**/

template<>
PolyContainer& PolyContainer::operator=(const PolyContainer& container) {
	if(this != &container) {
		_handler = container._handler;
	}
	return *this;
}

/**
* Returns a new PolyContainer contaning a new copy of the contained data.
* class.
**/

PolyContainer PolyContainer::copy() const {
	PolyContainer container;
	if(_handler) container._handler = intrusive_ptr<Handler>(_handler->copy());
	return container;
}

/**
* Default contructor.
**/

PolyContainer::PolyContainer(): _handler(new GenericHandler<void>()) {}

/**
* Copy constructor.
**/

template<>
PolyContainer::PolyContainer(const PolyContainer& container):
_handler(container._handler) {}

/**
* This behaviour is already handled by template specialization, yet to suppress
* gcc's warnings this method is also provided.
**/

PolyContainer& PolyContainer::operator=(const PolyContainer& container) {
	if(this != &container) {
		_handler = container._handler;
	}
	return *this;
}

/**
* This behaviour is already handled by template specialization, yet to suppress
* gcc's warnings this method is also provided.
**/

PolyContainer::PolyContainer(const PolyContainer& container):
_handler(container._handler) {}

/**
* Destructor.
**/

PolyContainer::~PolyContainer() {}

} //namespace systematic
