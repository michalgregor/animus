#ifndef CONVERTORREGISTER_H_INCLUDED
#define CONVERTORREGISTER_H_INCLUDED

#include <vector>
#include <functional>
#include <utility>
#include <map>
#include <boost/serialization/map.hpp>

#include "../type_info.h"
#include <Systematic/configure.h>

#include "Handler.h"
#include "TypeConverter.h"

namespace systematic {
namespace detail {

/**
 * Auxiliary class that defines most of the interface for ConverterRegister.
 */
template<class Context = void>
class AuxImpl_ConverterRegister {
private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int /*version*/) {
    	ar & _converterMap;
    }

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & _converterMap;
	}

public:
	//! Type of converters used by the ConverterRegister.
	typedef TypeConverter<Context> converter_type;
	//! Type of the key used to retrieve a converter.
	typedef std::pair<TypeID, TypeID> type_pair;
	//! Type of the context.
	typedef Context context_type;

private:
	typedef typename std::map<type_pair, shared_ptr<converter_type> >::value_type value_type;
	//! A map containing the converters.
	std::map<type_pair, shared_ptr<converter_type> > _converterMap;

private:
	struct Compare {
		bool operator()(const value_type& tp1, const TypeID& t) {
			return tp1.first.first < t;
		}

		bool operator()(const TypeID& t, const value_type& tp1) {
			return t < tp1.first.first;
		}
	};

public:
	/**
	 * Returns a vector of types to which type t is convertible.
	 */
	std::vector<TypeID> convertible(const TypeID& t) {
		auto range = std::equal_range(
			_converterMap.begin(),
			_converterMap.end(),
			t,
			Compare()
		);

		std::vector<TypeID> ret;
		for(auto iter = range.first; iter != range.second; iter++) {
			ret.push_back(iter->first.second);
		}

		return ret;
	}

	/**
	 * Returns a vector of types to which type Type is convertible.
	 */
	template<class Type>
	std::vector<TypeID> convertible() {
		return convertible(systematic::type_id<Type>());
	}

	/**
	 * Returns whether type from is convertible to type to.
	 */
	bool convertible(const TypeID& from, const TypeID& to) {
		return (from == to) || (_converterMap.find(type_pair(from, to)) != _converterMap.end());
	}

	/**
	 * Returns whether type From is convertible to type To.
	 */
	template<class From, class To>
	std::vector<TypeID> convertible() {
		return convertible(systematic::type_id<From>(), systematic::type_id<To>());
	}

	/**
	 * Returns a vector of types to which type t is compatible (convertible
	 * to them and back).
	 */
	std::vector<TypeID> compatible(const TypeID& t) {
		auto range = std::equal_range(
			_converterMap.begin(),
			_converterMap.end(),
			t,
			Compare()
		);

		std::vector<TypeID> ret;
		for(auto iter = range.first; iter != range.second; iter++) {
			if(convertible(iter->first.second, t)) ret.push_back(iter->first.second);
		}

		return ret;
	}

	/**
	 * Returns a vector of types to which type Type is compatible (convertible
	 * to them and back).
	 */
	template<class Type>
	std::vector<TypeID> compatible() {
		return compatible(systematic::type_id<Type>());
	}

	/**
	 * Returns whether type type1 is compatible to type2 to (convertible to it
	 * and back).
	 */
	bool compatible(const TypeID& type1, const TypeID& type2) {
		return convertible(type1, type2) && convertible(type2, type1);
	}

	/**
	 * Returns whether type Type1 is compatible to Type2 to (convertible to it
	 * and back).
	 */
	template<class Type1, class Type2>
	bool compatible() {
		return compatible(systematic::type_id<Type1>(), systematic::type_id<Type2>());
	}

	/**
	 * Registers type from as convertible to type to, and specifies a converter
	 * that is to be used to accomplish the conversion.
	 */
	void makeConvertible(const TypeID& from, const TypeID& to, const shared_ptr<converter_type>& converter) {
		_converterMap.insert(std::make_pair(type_pair(from, to), converter));
	}

	/**
	 * Registers type From as convertible to type To, and specifies a converter
	 * that is to be used to accomplish the conversion.
	 */
	template<class From, class To>
	void makeConvertible(const shared_ptr<converter_type>& converter) {
		makeConvertible(systematic::type_id<From>(), systematic::type_id<To>(), converter);
	}

	/**
	 * If type from is registered as convertible to type to, the relationship
	 * is removed from the register. Otherwise does nothing.
	 */
	void makeUnconvertible(const TypeID& from, const TypeID& to) {
		auto iter = _converterMap.find(type_pair(from, to));
		if(iter != _converterMap.end()) _converterMap.erase(iter);
	}

	/**
	 * If type From is registered as convertible to type To, the relationship
	 * is removed from the register. Otherwise does nothing.
	 */
	template<class From, class To>
	void makeUnconvertible() {
		makeUnconvertible(systematic::type_id<From>(), systematic::type_id<To>());
	}

	/**
	 * Retrieves converter from type from to type to from the register.
	 * Throws an exception if from is not registered as convertible to to.
	 */
	const converter_type& getEntry(const TypeID& from, const TypeID& to) {
		auto iter = _converterMap.find(type_pair(from, to));
		if(iter == _converterMap.end()) throw TracedError_OutOfRange("There is no entry for types " + from.name() + ", and " + to.name() + ".");
		return *iter->second;
	}

	/**
	 * Retrieves converter from type From to type To from the register.
	 * Throws an exception if from is not registered as convertible to to.
	 */
	template<class From, class To>
	const converter_type& getEntry(const TypeID& from, const TypeID& to) {
		return getEntry(systematic::type_id<From>(), systematic::type_id<To>());
	}

	/**
	 * A helper function that creates a new converter for types From and To.
	 * It is required that To be copy-constructible from From.
	 */
	template<class From, class To>
	static TypeConverter<Context>* createGenericConverter() {
		return new GenericValueConverter<From, To, Context>();
	}

	virtual ~AuxImpl_ConverterRegister() = default;
};

} //namespace detail

/**
* @class ConverterRegister
* @author Michal Gregor
*
* This class is used by PolyConverter to tell which value types can be converted
* to which. It is necessary to register such types as convertible in advance
* by calling one of the makeConvertible() member functions.
*
* For every method there are two versions: one which operates on TypeIDs
* directly, and - for convenience - a template version, which generates TypeIDs
* from template arguments automatically.
**/
SYS_WNONVIRT_DTOR_OFF
template<class Context = void>
class ConverterRegister: private detail::AuxImpl_ConverterRegister<Context> {
SYS_WNONVIRT_DTOR_ON
private:
	typedef detail::AuxImpl_ConverterRegister<Context> base_type;

private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int /*version*/) {
    	ar & static_cast<base_type&>(*this);
    }

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & static_cast<base_type&>(*this);
	}

public:
	using base_type::converter_type;
	using base_type::type_pair;
	using base_type::convertible;
	using base_type::compatible;
	using base_type::makeConvertible;
	using base_type::makeUnconvertible;
	using base_type::getEntry;
	using base_type::createGenericConverter;

public:
	/**
	 * Retrieves the converter from obj's type to type to using getEntry, and
	 * then does the conversion, returning a Handler* to the converted object.
	 *
	 * @note If obj->getType() == to, obj is returned without applying
	 * any conversion.
	 */
	Handler* convert(Handler* obj, const TypeID& to, Context& context) {
		if(obj->getType() == to) return obj;
		return getEntry(obj->getType(), to)(obj, context);
	}

	/**
	 * Retrieves the converter from obj's type To type to using getEntry, and
	 * then does the conversion, returning a Handler* to the converted object.
	 *
	 * @note If obj->getType() == To, obj is returned without applying
	 * any conversion.
	 */
	template<class To>
	Handler* convert(Handler* obj, Context& context) {
		if(obj->getType() == systematic::type_id<To>()) return obj;
		return getEntry(obj->getType(), systematic::type_id<To>())(obj, context);
	}

public:
	virtual ~ConverterRegister() = default;
};

// ConverterRegister: specialization for void.
SYS_WNONVIRT_DTOR_OFF
template<>
class ConverterRegister<void>: private detail::AuxImpl_ConverterRegister<void> {
SYS_WNONVIRT_DTOR_ON
private:
	typedef detail::AuxImpl_ConverterRegister<void> base_type;

private:
	friend class boost::serialization::access;

    void serialize(IArchive& ar, const unsigned int /*version*/) {
    	ar & static_cast<base_type&>(*this);
    }

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & static_cast<base_type&>(*this);
	}

public:
	using base_type::converter_type;
	using base_type::type_pair;
	using base_type::convertible;
	using base_type::compatible;
	using base_type::makeConvertible;
	using base_type::makeUnconvertible;
	using base_type::getEntry;
	using base_type::createGenericConverter;

public:
	/**
	 * Retrieves the converter from obj's type to type to using getEntry, and
	 * then does the conversion, returning a Handler* to the converted object.
	 *
	 * @note If obj->getType() == to, obj is returned without applying
	 * any conversion.
	 */
	Handler* convert(Handler* obj, const TypeID& to) {
		if(obj->getType() == to) return obj;
		return getEntry(obj->getType(), to)(obj);
	}

	/**
	 * Retrieves the converter from obj's type To type to using getEntry, and
	 * then does the conversion, returning a Handler* to the converted object.
	 *
	 * @note If obj->getType() == To, obj is returned without applying
	 * any conversion.
	 */
	template<class To>
	Handler* convert(Handler* obj) {
		if(obj->getType() == systematic::type_id<To>()) return obj;
		return getEntry(obj->getType(), systematic::type_id<To>())(obj);
	}

public:
	virtual ~ConverterRegister() = default;
};

} //namespace systematic

#endif // CONVERTORREGISTER_H_INCLUDED
