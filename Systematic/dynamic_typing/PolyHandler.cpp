#include "PolyHandler.h"

namespace systematic {

/**
 * Boost serialization.
 */

void PolyHandler::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _handler;
}

void PolyHandler::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _handler;
}

/**
* Returns id of the contained type as given by TypeID.
**/

TypeID PolyHandler::getType() const {
	if(!_handler) throw TracedError_InvalidPointer("NULL pointer to handler.");
	return _handler->getType();
}

void PolyHandler::setData() {
	_handler = intrusive_ptr<Handler>(new GenericHandler<void>());
}

/**
* Specialization of assignment operator template for PolyHandler so that it
* works like a standard assignment operator.
**/

template<>
PolyHandler& PolyHandler::operator=(const PolyHandler& container) {
	if(this != &container) {
		_handler = container._handler;
	}
	return *this;
}

/**
* Returns a new PolyHandler contaning a new copy of the contained data.
**/

PolyHandler PolyHandler::copy() const {
	PolyHandler container;
	container._handler = intrusive_ptr<Handler>(_handler->copy());
	return container;
}

/**
* Default contructor.
**/

PolyHandler::PolyHandler(): _handler(new GenericHandler<void>()) {}

/**
* Copy constructor.
**/

template<>
PolyHandler::PolyHandler(const PolyHandler& container):
_handler(container._handler) {}

/**
* This behaviour is already handled by template specialization, yet to suppress
* gcc's warnings this method is also provided.
**/

PolyHandler& PolyHandler::operator=(const PolyHandler& container) {
	if(this != &container) {
		_handler = container._handler;
	}
	return *this;
}

/**
* This behaviour is already handled by template specialization, yet to suppress
* gcc's warnings this method is also provided.
**/

PolyHandler::PolyHandler(const PolyHandler& container):
_handler(container._handler) {}

/**
* Destructor.
**/

PolyHandler::~PolyHandler() {}

} //namespace systematic
