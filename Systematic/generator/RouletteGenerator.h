#ifndef ROULETTEGENERATOR_H_
#define ROULETTEGENERATOR_H_

#include "../system.h"
#include "../math/Compare.h"
#include "../math/numeric_cast.h"

#include <vector>
#include <type_traits>
#include <algorithm>

#include "Generator.h"
#include "RandEngine.h"
#include "BoundaryGenerator.h"

namespace systematic {

/**
* @class RouletteGenerator
* @author Michal Gregor
* Implements a roulette-based generator. This generator stores an array of
* values and their respective probabilities. It computes an array of cumulative
* probability sums. The principle is often used in genetic algorithms.
**/
template<class ProbabilityType = float, class Engine = DefaultRandEngine>
class RouletteGenerator: public Generator<unsigned int>, public RandEngine<Engine> {
	static_assert(std::is_floating_point<ProbabilityType>::value,
		"The ProbabilityType must be a floating point type.");

private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Generator<unsigned int> >(*this);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);
		ar & _cumulativeProbability;
		ar & _gen;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Stores the probability distribution vector.
	std::vector<ProbabilityType> _cumulativeProbability;
	//! The underlying boundary generator.
	BoundaryGenerator<ProbabilityType, Engine> _gen;

private:
	std::vector<ProbabilityType> computeCumulative(std::vector<ProbabilityType> probabilities) const;

public:
	using Generator<unsigned int>::operator();
	virtual unsigned int operator()();

	unsigned int size() const {
		return _cumulativeProbability.size();
	}

	unsigned int getLowerBound() const {
		return 0;
	}

	unsigned int getUpperBound() const {
		return _cumulativeProbability.size();
	}

	void setProbabilities(const std::vector<ProbabilityType>& probabilities);

	RouletteGenerator<ProbabilityType, Engine>& operator=(const RouletteGenerator<ProbabilityType, Engine>& obj);
	RouletteGenerator(const RouletteGenerator<ProbabilityType, Engine>& obj);

	RouletteGenerator(const std::vector<ProbabilityType>& probabilities, const shared_ptr<Engine>& engine = NewEngine<Engine>());
	explicit RouletteGenerator(const shared_ptr<Engine>& engine = NewEngine<Engine>());

	virtual ~RouletteGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Computes cumulative probabilities. Sets _generator's distribution accordingly.
* Probabilities cannot be negative and should not be zero. If probability is < 0
* an TracedError_DomainError is thrown. If it is 0 a warning is written into
* sysdebug stream.
*/
template<class ProbabilityType, class Engine>
std::vector<ProbabilityType> RouletteGenerator<ProbabilityType, Engine>::
computeCumulative(std::vector<ProbabilityType> probabilities) const {
	unsigned int sz(numeric_cast<unsigned int>(probabilities.size()));
	std::vector<ProbabilityType> cumulative; cumulative.reserve(sz);

	if(sz >= 1) {
		cumulative.push_back(probabilities[0]);

		for(unsigned int i = 1; i < sz; i++) {
			if(probabilities[i] < 0) throw TracedError_DomainError("Probability values must be >= 0.");
			cumulative.push_back(cumulative[i - 1] + probabilities[i]);
		}
	}

	return cumulative;
}

/**
* Rolls the roulette and return index of the chosen index.
**/
template<class ProbabilityType, class Engine>
unsigned int RouletteGenerator<ProbabilityType, Engine>::operator()() {
	return numeric_cast<unsigned int>(std::distance(_cumulativeProbability.begin(), std::lower_bound(_cumulativeProbability.begin(), _cumulativeProbability.end(), _gen())));
}

/**
* Computes cumulative probabilities and _generator boundaries for a new set of
* probabilities. The sum of probabilities supplied using the probabilities
* vector does not have to sum up to 1.
**/
template<class ProbabilityType, class Engine>
void RouletteGenerator<ProbabilityType, Engine>::
setProbabilities(const std::vector<ProbabilityType>& probabilities) {
	_cumulativeProbability = computeCumulative(probabilities);
	if(_cumulativeProbability.size()) _gen.createSpace(0, _cumulativeProbability.back());
	else _gen.createSpace(0, 0);
}

/**
* Assignment operator.
**/
template<class ProbabilityType, class Engine>
RouletteGenerator<ProbabilityType, Engine>& RouletteGenerator<ProbabilityType, Engine>::operator=(const RouletteGenerator<ProbabilityType, Engine>& obj) {
	if(&obj != this) {
		Generator<unsigned int>::operator=(obj);
		RandEngine<Engine>::operator=(obj);

		_cumulativeProbability = obj._cumulativeProbability;
		_gen = obj._gen;
	}

	return *this;
}

/**
* Copy constructor.
**/
template<class ProbabilityType, class Engine>
RouletteGenerator<ProbabilityType, Engine>::RouletteGenerator(const RouletteGenerator<ProbabilityType, Engine>& obj):
Generator<unsigned int>(obj), RandEngine<Engine>(obj),
_cumulativeProbability(obj._cumulativeProbability), _gen(obj._gen) {}

/**
* Constructor.
**/
template<class ProbabilityType, class Engine>
RouletteGenerator<ProbabilityType, Engine>::RouletteGenerator(
	const std::vector<ProbabilityType>& probabilities,
	const shared_ptr<Engine>& engine
): Generator<unsigned int>(), RandEngine<Engine>(engine),
_cumulativeProbability(computeCumulative(probabilities)),
_gen(
	(_cumulativeProbability.size())?
		(BoundaryGenerator<ProbabilityType, Engine>(0, _cumulativeProbability.back(), this->_engine)):
		(BoundaryGenerator<ProbabilityType, Engine>(0, 0, this->_engine))
)
{}

/**
* Constructor.
**/
template<class ProbabilityType, class Engine>
RouletteGenerator<ProbabilityType, Engine>::RouletteGenerator(
	const shared_ptr<Engine>& engine
): Generator<unsigned int>(), RandEngine<Engine>(engine),
_cumulativeProbability(), _gen(BoundaryGenerator<ProbabilityType, Engine>(0, 0, this->_engine)) {}

}//namespace systematic

SYS_EXPORT_TEMPLATE(systematic::RouletteGenerator, 2)

#endif /* ROULETTEGENERATOR_H_ */
