#ifndef HYBRIDSEQSELECTOR_H_INCLUDED
#define HYBRIDSEQSELECTOR_H_INCLUDED

#include "../system.h"
#include "SeqGenerator.h"
#include "CollisionSeqGenerator.h"
#include "BoundaryGenerator.h"
#include "SpaceSeqGenerator.h"
#include "RandEngine.h"

#include <type_traits>
#include <cmath>
#include <boost/math/special_functions/factorials.hpp>

namespace systematic {
/**
* @class HybridSeqGenerator
* @brief Delegates to a space generator or a boundary generator one of
* which is chosen automatically by efficiency.
*
* The generator is selected by a rule of thumb based on the ratio between
* the length of generated sequence and size of the possibility space.
*
* This is only implemented for integral types as CreateSpace has different
* behaviour for SpaceGenerator of floats and BoundaryGenerator of floats.
*
* @author Michal Gregor
**/
template<typename Type, class Engine = DefaultRandEngine>
class HybridSeqGenerator: public SeqGenerator<Type>, public RandEngine<Engine>
{
private:
	static_assert(std::is_integral<Type>::value, "The type must be integral.");

private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<SeqGenerator<Type> >(*this);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);
		ar & _generator1;
		ar & _generator2;
		ar & _lowerBound;
		ar & _upperBound;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The preset length of sequence.
	unsigned int k;
	//! Pointer to the first version of the underlying generator.
	shared_ptr<CollisionSeqGenerator<BoundaryGenerator<Type, Engine> > > _generator1;
	//! Pointer to the second version of the underlying generator.
	shared_ptr<SpaceSeqGenerator<Type, Engine> > _generator2;
	//! Bounds of the possibility space.
	Type _lowerBound, _upperBound;

public:
	using Generator<Type>::result_type;

	virtual Type operator()();
	virtual std::vector<Type> operator()(unsigned int size, bool repetition = 1);

	void setBounds(Type lowerBound, Type upperBound);

	Type getLowerBound() const;
	Type getUpperBound() const;

	unsigned int getSeqLen() const;
	void setSeqLen(unsigned int seqLen);

	void createSpace();

	virtual void createSpace(Type lowerBound, Type upperBound);
	virtual unsigned int size() const;

	explicit HybridSeqGenerator(unsigned int seqLen = 0, const shared_ptr<Engine>& engine = NewEngine<Engine>());
	HybridSeqGenerator(unsigned int seqLen, Type lowerBound, Type upperBound, const shared_ptr<Engine>& engine = NewEngine<Engine>());

	virtual ~HybridSeqGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Sets boundaries of the possiblity space to [lowerBound, upperBound].
* @param lowerBound Lower bound of the possibility space.
* @param upperBound Upper bound of the possibility space.
**/
template<typename Type, class Engine>
void HybridSeqGenerator<Type, Engine>::setBounds(Type lowerBound, Type upperBound) {
	_lowerBound = lowerBound;
	_upperBound = upperBound;
}

/**
* Returns the lower bound of the possibility space: [lower bound, upper bound].
**/
template<typename Type, class Engine>
Type HybridSeqGenerator<Type, Engine>::getLowerBound() const {
	return _lowerBound;
}

/**
* Returns the upper bound of the possibility space: [lower bound, upper bound].
**/
template<typename Type, class Engine>
Type HybridSeqGenerator<Type, Engine>::getUpperBound() const {
	return _upperBound;
}

/**
* Delegated to the underlying generator.
**/
template<typename Type, class Engine>
Type HybridSeqGenerator<Type, Engine>::operator()() {
	if(_generator1) return (*_generator1)();
	else if(_generator2) return (*_generator2)();
	else throw TracedError_InvalidPointer("Member _generator not yet initialized.");
}

/**
* Delegated to the underlying generator. This call is forwarded directly to
* the generator. In case size differs much from the preset value efficiency
* may be lost.
**/
template<typename Type, class Engine>
std::vector<Type> HybridSeqGenerator<Type, Engine>::
operator()(unsigned int size_, bool repetition) {
	if(_generator1) return (*_generator1)(size_, repetition);
	else if(_generator2) return (*_generator2)(size_, repetition);
	else throw TracedError_InvalidPointer("Member _generator not yet initialized.");
}

/**
* Returns the preset length of sequence.
**/
template<typename Type, class Engine>
unsigned int HybridSeqGenerator<Type, Engine>::getSeqLen() const {
	return k;
}

/**
* Sets the length of sequence.
**/
template<typename Type, class Engine>
void HybridSeqGenerator<Type, Engine>::setSeqLen(unsigned int seqLen) {
	k = seqLen;
}

/**
* An efficient generator with the given parameters is chosen.
**/
template<typename Type, class Engine>
void HybridSeqGenerator<Type, Engine>::createSpace() {
	if(_upperBound < _lowerBound) throw TracedError_InvalidArgument("_upperBound < _lowerBound.");
	unsigned int n = _upperBound - _lowerBound;
	if(n < k) throw TracedError_InvalidArgument("The preset length of sequence is > size of the possibility space.");

	_generator1.reset();
	_generator2.reset();

	if(n > 5 && boost::math::falling_factorial<unsigned int>(n, k)/pow(n, k) > 0.75) {
		_generator1 =
		shared_ptr<CollisionSeqGenerator<BoundaryGenerator<Type, Engine> > >(
			new CollisionSeqGenerator<BoundaryGenerator<Type, Engine> >(
				shared_ptr<BoundaryGenerator<Type, Engine> >(
					new BoundaryGenerator<Type, Engine>(_lowerBound, _upperBound, this->_engine)
				)
			)
		);
	} else {
		_generator2 =
		shared_ptr<SpaceSeqGenerator<Type, Engine> >(
			new SpaceSeqGenerator<Type, Engine>(_lowerBound, _upperBound, 1, this->_engine)
		);
	}
}

/**
* An efficient generator with the given parameters is chosen.
**/
template<typename Type, class Engine>
void HybridSeqGenerator<Type, Engine>::createSpace(Type lowerBound, Type upperBound) {
	setBounds(lowerBound, upperBound);
	createSpace();
}

/**
* Delegated to the underlying generator if available. If not created yet,
* return 0.
**/
template<typename Type, class Engine>
unsigned int HybridSeqGenerator<Type, Engine>::size() const {
	if(_generator1) return _generator1->getGenerator()->size();
	else if(_generator2) return _generator2->size();
	else return 0;
}

/**
* Constructor.
* @param seqLen Length of sequences that this selector will be used to
* generate. This parameter is required so that the more efficient selector
* may be chosen automatically.
* @param engine Pointer to the random generator engine.
**/
template<typename Type, class Engine>
HybridSeqGenerator<Type, Engine>::HybridSeqGenerator(unsigned int seqLen, const shared_ptr<Engine>& engine):
	RandEngine<Engine>(engine), k(seqLen), _generator1(), _generator2(),
	 _lowerBound(), _upperBound() {}

/**
* Constructor.
*
* Range of the possibility space is [lower bound, upper bound].
*
* @param seqLen Length of sequences that this selector will be used to
* generate. This parameter is required so that the more efficient selector
* may be chosen automatically.
* @param lowerBound The lowerBound of space of objects.
* @param seed Seed of the random generator engine.
* @param engine Pointer to the random generator engine.
**/
template<typename Type, class Engine>
HybridSeqGenerator<Type, Engine>::
HybridSeqGenerator(unsigned int seqLen, Type lowerBound, Type upperBound, const shared_ptr<Engine>& engine):
	RandEngine<Engine>(engine), k(seqLen), _generator1(), _generator2(),
	_lowerBound(lowerBound), _upperBound(upperBound)
{
	createSpace();
}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::HybridSeqGenerator, 2)

#endif // HYBRIDSEQSELECTOR_H_INCLUDED
