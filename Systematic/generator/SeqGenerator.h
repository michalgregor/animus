#ifndef SEQGENERATOR_H_INCLUDED
#define SEQGENERATOR_H_INCLUDED

#include "../system.h"
#include "Generator.h"

namespace systematic {

/**
* @class SeqGenerator
* @author Michal Gregor
*
* @brief Used to generate sequences of random numbers without
* repetition.
**/
template<class Type>
class SeqGenerator: public Generator<Type> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Generator<Type> >(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	using Generator<Type>::operator();
	virtual std::vector<Type> operator()(unsigned int size, bool repetition) = 0;
	virtual ~SeqGenerator() = 0;
};

template<class Type>
SeqGenerator<Type>::~SeqGenerator() {
	SYS_EXPORT_INSTANCE();
}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::SeqGenerator, 1)

#endif // SEQGENERATOR_H_INCLUDED
