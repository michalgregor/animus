#include "RowVectorGenerator.h"

namespace systematic {

void RowVectorGenerator::serialize(OArchive& ar, const unsigned int) const {
	ar & boost::serialization::base_object<Generator<RowVector> >(*this);
	ar & boost::serialization::base_object<RandEngine<DefaultRandEngine> >(*this);
	ar & _lowerBound;
	ar & _upperBound;
}

void RowVectorGenerator::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Generator<RowVector> >(*this);
	ar & boost::serialization::base_object<RandEngine<DefaultRandEngine> >(*this);
	ar & _lowerBound;
	ar & _upperBound;
}

void RowVectorGenerator::setBounds(
	const RowVector& lowerBound,
	const RowVector& upperBound
) {
	if(lowerBound.size() != upperBound.size())
		throw TracedError_InvalidArgument("The lower bound must have the same size as the upper bound.");

	_lowerBound = lowerBound;
	_upperBound = upperBound;
}

void RowVectorGenerator::setBounds(RowVector&& lowerBound, RowVector&& upperBound) {
	if(lowerBound.size() != upperBound.size())
		throw TracedError_InvalidArgument("The lower bound must have the same size as the upper bound.");

	_lowerBound = std::move(lowerBound);
	_upperBound = std::move(upperBound);
}

RowVector RowVectorGenerator::operator()() {
	RowVector res(_lowerBound.size());

	for(AlgebraIndex i = 0; i < res.size(); i++) {
		std::uniform_real_distribution<RealType> distro(_lowerBound[i], _upperBound[i]);
		res[i] = distro(engine());
	}

	return res;
}

RowVectorGenerator::RowVectorGenerator():
	RandEngine<DefaultRandEngine>(), _lowerBound(), _upperBound() {}

RowVectorGenerator::RowVectorGenerator(
	const RowVector& lowerBound,
	const RowVector& upperBound,
	const shared_ptr<DefaultRandEngine>& engine
):
	RandEngine<DefaultRandEngine>(engine),
	_lowerBound(), _upperBound()
{
	setBounds(lowerBound, upperBound);
}

RowVectorGenerator::RowVectorGenerator(
	RowVector&& lowerBound,
	RowVector&& upperBound,
	const shared_ptr<DefaultRandEngine>& engine
):
	RandEngine<DefaultRandEngine>(engine)
{
	setBounds(std::move(lowerBound), std::move(upperBound));
}

} // namespace systematic

