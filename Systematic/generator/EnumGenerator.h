#ifndef ENUMGENERATOR_H_INCLUDED
#define ENUMGENERATOR_H_INCLUDED

#include "../system.h"
#include "BoundaryGenerator.h"

namespace systematic {

/**
* @class EnumGenerator
* Generates values from an enum. It is expected that the numeric values of items
* are assigned continuously. Great care should be taken as the bounds must be
* specified manually.
**/
template<class Enum>
class EnumGenerator: public Generator<Enum> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Generator<Enum> >(*this);
		ar & _generator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The internal unsigned int generator.
	BoundaryGenerator<unsigned int> _generator;

public:
	/**
	* Generates the data.
	**/

	virtual Enum operator()() {
		return Enum(_generator());
	}

	EnumGenerator& operator=(const EnumGenerator& obj) {
		if(this != &obj) {
			Generator<Enum>::operator=(obj);
			_generator = obj._generator;
		}
		return *this;
	}

	//! Copy constructor.
	EnumGenerator(const EnumGenerator& obj): Generator<Enum>(obj),
	_generator(obj._generator) {}

	//! For purposes of serialization mainly.
	EnumGenerator(): Generator<Enum>(), _generator() {}

	/**
	* Constructor. It is expected that the numeric values of items
	* are assigned continuously. Great care should be taken as the bounds -
	* lbound being the lower bound and ubound being the upper bound - must be
	* specified manually.
	**/

	EnumGenerator(unsigned int lbound, unsigned int ubound):
	Generator<Enum>(), _generator(lbound, ubound) {}

	virtual ~EnumGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::EnumGenerator, 1)

#endif // ENUMGENERATOR_H_INCLUDED
