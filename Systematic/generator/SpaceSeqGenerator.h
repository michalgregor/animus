#ifndef SPACESEQGENERATOR_H_INCLUDED
#define SPACESEQGENERATOR_H_INCLUDED

#include "../system.h"
#include "SeqGenerator.h"

#include <vector>
#include <type_traits>
#include "RandEngine.h"

#include "BoundaryGenerator.h"

#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/locks.hpp>

#include <boost/serialization/vector.hpp>

namespace systematic {

/**
* Implementation of interface common for SpaceSeqGenerator for arithmetic and
* non-arithmetic types.
**/
template<class Type, class Engine = DefaultRandEngine>
class AuxSys_SpaceSeqGeneratorImpl:
	public SeqGenerator<Type>, public RandEngine<Engine>
{
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<SeqGenerator<Type> >(*this);
		boost::shared_lock<boost::shared_mutex> lock(mut);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);
		ar & _space;
		ar & _gen;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Stores the space of possible values.
	std::vector<Type> _space;
	//! The underlying generator.
	BoundaryGenerator<unsigned int, Engine> _gen;
	//! Mutex used to ensure that multiple rand_shuffles are not applied to the space at once.
	mutable boost::shared_mutex mut;

	inline std::vector<Type> selectRS(unsigned int size);

public:
	using SeqGenerator<Type>::operator();

	virtual Type operator()();
	virtual std::vector<Type> operator()(unsigned int size, bool repetition);

	void stealSpace(std::vector<Type>& space);
	void setSpace(const std::vector<Type>& space);
	const std::vector<Type>& getSpace() const;

	unsigned int size() const;

	AuxSys_SpaceSeqGeneratorImpl& operator=(const AuxSys_SpaceSeqGeneratorImpl& generator);
	AuxSys_SpaceSeqGeneratorImpl(const AuxSys_SpaceSeqGeneratorImpl& generator);

	explicit AuxSys_SpaceSeqGeneratorImpl(const std::vector<Type>& space, const shared_ptr<Engine>& engine = NewEngine<Engine>());
	explicit AuxSys_SpaceSeqGeneratorImpl(const shared_ptr<Engine>& engine = NewEngine<Engine>());

	virtual ~AuxSys_SpaceSeqGeneratorImpl() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Returns a random value from the space of possible values.
**/
template<class Type, class Engine>
Type AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::operator()() {
	boost::unique_lock<boost::shared_mutex> lock(mut);
	if(!_space.size()) throw TracedError_OutOfRange("Possibility space contains no entries.");
	return _space.at(_gen());
}

/**
* This method imlements generation of sequence without repetition.
* @param size Size of the sequence.
**/
template<class Type, class Engine>
std::vector<Type> AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::selectRS(unsigned int ssize) {
	boost::unique_lock<boost::shared_mutex> lock(mut);
	if(ssize > _space.size()) throw TracedError_InvalidArgument("Requested size of sequence without repetition > size of possibility space.");
	std::vector<Type>& space = const_cast<std::vector<Type>& >(_space);
	random_shuffle(space.begin(), space.begin() + space.size());
	return std::vector<Type>(space.begin(), space.begin() + ssize);
}

/**
* Selects a random sequence of values from the space.
* @param size Size of the sequence that is to be generated.
* @param repetition If set to true, values may repeat, if to false
* a sequence without repetition is generated.
**/
template<class Type, class Engine>
std::vector<Type> AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::operator()(unsigned int ssize, bool repetition) {
	if(repetition) return SeqGenerator<Type>::operator()(ssize);
	else return selectRS(ssize);
}

/**
* Sets the space by swapping data from the provided input vector. This
* moves contents of the current space into the input vector.
**/
template<class Type, class Engine>
void AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::stealSpace(std::vector<Type>& space) {
	boost::unique_lock<boost::shared_mutex> lock(mut);
	if(_space.size() != space.size()) _gen.createSpace(0, space.size()-1);
	_space.swap(space);
}

/**
* Sets the space. Contents of the input vector are copied.
**/
template<class Type, class Engine>
void AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::setSpace(const std::vector<Type>& space) {
	boost::unique_lock<boost::shared_mutex> lock(mut);
	if(_space.size() != space.size()) _gen.createSpace(0, space.size()-1);
	_space = space;
}

/**
* Returns reference to the space of possibilities.
**/
template<class Type, class Engine>
const std::vector<Type>& AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::getSpace() const {
	boost::shared_lock<boost::shared_mutex> lock(mut);
	return _space;
}

/**
* Returns size of the possibility space.
**/

template<class Type, class Engine>
unsigned int AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::size() const {
	boost::shared_lock<boost::shared_mutex> lock(mut);
	return numeric_cast<unsigned int>(_space.size());
}

/**
* Assignment operator.
**/
template<class Type, class Engine>
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>&
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::
operator=(const AuxSys_SpaceSeqGeneratorImpl& generator) {
	if(this != &generator) {
		SeqGenerator<Type>::operator=(generator);
		RandEngine<Engine>::operator=(generator);
		boost::unique_lock<boost::shared_mutex> lock1(mut);
		boost::shared_lock<boost::shared_mutex> lock2(generator.mut);
		_space = generator._space;
		_gen = generator._gen;
	}
	return *this;
}

/**
* Copy constructor.
**/
template<class Type, class Engine>
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::
AuxSys_SpaceSeqGeneratorImpl(const AuxSys_SpaceSeqGeneratorImpl& generator):
SeqGenerator<Type>(generator), RandEngine<Engine>(generator),
_space(), _gen(), mut() {
	boost::shared_lock<boost::shared_mutex> lock(generator.mut);
	_space = generator._space;
	_gen = generator._gen;
}

/**
* Constructor.
* @param space The space of possibilities.
* @param engine Engine of the pseudorandom generator.
**/

template<class Type, class Engine>
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::
AuxSys_SpaceSeqGeneratorImpl(const std::vector<Type>& space, const shared_ptr<Engine>& engine):
RandEngine<Engine>(engine), _space(space), _gen(0, _space.size()-1, this->_engine),
mut() {}

/**
* Constructor. The space of possibilities must be specified before the
* object is used to generate.
* @param engine Engine of the pseudorandom generator.
**/
template<class Type, class Engine>
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::
AuxSys_SpaceSeqGeneratorImpl(const shared_ptr<Engine>& engine):
RandEngine<Engine>(engine), _space(),
_gen(0, 0, this->_engine), mut() {}

//------------------------------------------------------------------------------
//							For non-arithmetic
//------------------------------------------------------------------------------

/**
* @class SpaceSeqGenerator
* @author Michal Gregor
*
* @brief Used to generate random values from a given enumerated space.
**/
template<class Type, class Engine = DefaultRandEngine, class Enable = void>
class SpaceSeqGenerator: public AuxSys_SpaceSeqGeneratorImpl<Type, Engine> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<AuxSys_SpaceSeqGeneratorImpl<Type, Engine> >(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	SpaceSeqGenerator<Type, Engine, Enable>& operator=(const SpaceSeqGenerator<Type, Engine, Enable>& obj);
	SpaceSeqGenerator(const SpaceSeqGenerator<Type, Engine, Enable>& obj);

	explicit SpaceSeqGenerator(const std::vector<Type>& space, const shared_ptr<Engine>& engine = NewEngine<Engine>());
	explicit SpaceSeqGenerator(const shared_ptr<Engine>& engine = NewEngine<Engine>());
	virtual ~SpaceSeqGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
 * Assignment operator.
 */
template<class Type, class Engine, class Enable>
SpaceSeqGenerator<Type, Engine, Enable>&
SpaceSeqGenerator<Type, Engine, Enable>::
operator=(const SpaceSeqGenerator<Type, Engine, Enable>& obj) {
	AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::operator=(obj);
	return *this;
}

/**
 * Copy constructor.
 */
template<class Type, class Engine, class Enable>
SpaceSeqGenerator<Type, Engine, Enable>::
SpaceSeqGenerator(const SpaceSeqGenerator<Type, Engine, Enable>& obj):
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>(obj) {}

/**
* Constructor.
* @param space The space of possibilities.
* @param engine Engine of the pseudorandom generator.
**/
template<class Type, class Engine, class Enable>
SpaceSeqGenerator<Type, Engine, Enable>::
SpaceSeqGenerator(const std::vector<Type>& space, const shared_ptr<Engine>& engine):
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>(space, engine) {}

/**
* Constructor. The space of possibilities must be specified before the
* object is used to generate.
* @param engine Engine of the pseudorandom generator.
**/
template<class Type, class Engine, class Enable>
SpaceSeqGenerator<Type, Engine, Enable>::
SpaceSeqGenerator(const shared_ptr<Engine>& engine):
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>(engine) {}

//------------------------------------------------------------------------------
//						SPECIALIZATION for arithmetic
//------------------------------------------------------------------------------

template<class Type>
using SpaceSeqGeneratorTraitArithmetic = typename std::enable_if<std::is_arithmetic<Type>::value>::type;

/**
* @class SpaceSeqGenerator
* @author Michal Gregor
*
* @brief Used to generate random values from a given enumerated space.
*
* Specialization for arithmetic types.
**/
template<class Type, class Engine>
class SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >:
	public AuxSys_SpaceSeqGeneratorImpl<Type, Engine>
{
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<AuxSys_SpaceSeqGeneratorImpl<Type, Engine> >(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	void createSpace(Type lowerBound, Type upperBound);
	void createSpace(Type lowerBound, Type upperBound, Type step);

	SpaceSeqGenerator& operator=(const SpaceSeqGenerator& obj);
	SpaceSeqGenerator(const SpaceSeqGenerator& obj);

	explicit SpaceSeqGenerator(std::vector<Type> space, const shared_ptr<Engine>& engine = NewEngine<Engine>());
	explicit SpaceSeqGenerator(const shared_ptr<Engine>& engine = NewEngine<Engine>());
	SpaceSeqGenerator(Type lowerBound, Type upperBound, Type step = 1, const shared_ptr<Engine>& engine = NewEngine<Engine>());

	virtual ~SpaceSeqGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Creates a space of possibilities containing numbers in range
* [lowerBound, upperBound] with step 1.
**/
template<class Type, class Engine>
void SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >::
createSpace(Type lowerBound, Type upperBound) {
	createSpace(lowerBound, upperBound, 1);
}

/**
* Creates a space of possibilities containing numbers in range
* [lowerBound, upperBound] with a specified step.
**/
template<class Type, class Engine>
void SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >::
createSpace(Type lowerBound, Type upperBound, Type step) {
	//if lower > upper, throw an exception
	if(upperBound < lowerBound) throw TracedError_InvalidArgument("LowerBound > upperBound.");

	//calculate spaceSize
	unsigned int spaceSize = abs((int)((upperBound - lowerBound)/step)) + 1;
	std::vector<Type> space; space.reserve(spaceSize);

	for(unsigned int i = 0; i < spaceSize; i++) {
		space.push_back(i*step + lowerBound);
	}

	this->stealSpace(space);
}

/**
 * Assignment operator.
 */
template<class Type, class Engine>
SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >&
SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >::
operator=(const SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >& obj) {
	AuxSys_SpaceSeqGeneratorImpl<Type, Engine>::operator=(obj);
	return *this;
}

/**
 * Copy constructor.
 */
template<class Type, class Engine>
SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >::
SpaceSeqGenerator(const SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >& obj):
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>(obj) {}

/**
* Constructor. The space of possibilities must be specified before the
* object is used to generate.
* @param engine Engine of the pseudorandom generator.
**/

template<class Type, class Engine>
SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >::
SpaceSeqGenerator(const shared_ptr<Engine>& engine):
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>(engine) {}

/**
* Constructor.
* @param space The space of possibilities.
* @param engine Engine of the pseudorandom generator.
**/

template<class Type, class Engine>
SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >::
SpaceSeqGenerator(std::vector<Type> space, const shared_ptr<Engine>& engine):
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>(space, engine) {}

/**
* Constructor. Automatically constructs a possibility space using the
* CreateSpace method (space of numbers from interval [lowerBound, upperBound]
* with a specified step is created).
* @param engine Engine of the pseudorandom generator.
**/

template<class Type, class Engine>
SpaceSeqGenerator<Type, Engine, SpaceSeqGeneratorTraitArithmetic<Type> >::
SpaceSeqGenerator(Type lowerBound, Type upperBound, Type step, const shared_ptr<Engine>& engine):
AuxSys_SpaceSeqGeneratorImpl<Type, Engine>(engine)
{
	createSpace(lowerBound, upperBound, step);
}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::AuxSys_SpaceSeqGeneratorImpl, 2)
SYS_EXPORT_TEMPLATE(systematic::SpaceSeqGenerator, 3)

#endif // SPACESEQGENERATOR_H_INCLUDED
