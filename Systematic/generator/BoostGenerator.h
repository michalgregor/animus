#ifndef BOOSTGENERATOR_H_INCLUDED
#define BOOSTGENERATOR_H_INCLUDED

#include "../system.h"
#include "Generator.h"
#include "RandEngine.h"

#include <boost/random/variate_generator.hpp>
#include "../serialization/random_typenames.h"
#include "../serialization/random_common.h"

namespace systematic {

/**
 * @class BoostGenerator
 * @author Michal Gregor
 *
 * @brief Wraps boost random generators within the Generator interface and
 * manages the engine in the same way that other Systematic generators do.
 **/
template<class Distribution, class Engine = DefaultRandEngine>
class BoostGenerator: public Generator<
        typename boost::variate_generator<Engine, Distribution>::result_type>,
        public RandEngine<Engine> {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization function.
	 **/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & boost::serialization::base_object<Generator<result_type> >(*this);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);

		if (!_generator) throw TracedError_InvalidPointer("Null pointer to generator.");

		serialize_distribution(ar, _generator->distribution(), version);
		ar & _generator->engine();
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	typedef boost::variate_generator<Engine&, Distribution> GeneratorType;

	//! The underlying generator. Mutable because operator()
	//! of variate_generator is non-const.
	GeneratorType* _generator;

public:
	typedef typename boost::variate_generator<Engine, Distribution>::result_type result_type;
	using Generator<result_type>::operator();

	virtual result_type operator()();

	/**
	 * Returns a reference to the underlying distribution.
	 */
	Distribution& distribution() {
		if (!_generator) throw TracedError_InvalidPointer("Null pointer to generator.");
		return _generator->distribution();
	}
	/**
	 * Returns a const reference to the underlying distribution.
	 */
	const Distribution& distribution() const {
		if (!_generator) throw TracedError_InvalidPointer("Null pointer to generator.");
		return _generator->distribution();
	}

	BoostGenerator<Distribution, Engine>& operator=(
	        const BoostGenerator<Distribution, Engine>& obj);
	BoostGenerator(const BoostGenerator<Distribution, Engine>& obj);

	BoostGenerator(
	        const shared_ptr<Engine>& engine_ = NewEngine<Engine>());
	BoostGenerator(const Distribution& distribution_,
	        const shared_ptr<Engine>& engine_ = NewEngine<Engine>());

	virtual ~BoostGenerator() {
		SYS_EXPORT_INSTANCE();
		if(_generator) delete _generator;
	}
};

/**
 * Generates a random value.
 **/

template<class Distribution, class Engine>
typename boost::variate_generator<Engine, Distribution>::result_type BoostGenerator<
Distribution, Engine>::operator()() {
	if (!_generator) throw TracedError_InvalidPointer("Null pointer to generator.");
	return _generator->operator()();
}

/**
 * Assignment operator.
 */
template<class Distribution, class Engine>
BoostGenerator<Distribution, Engine>&
BoostGenerator<Distribution, Engine>::operator=(
        const BoostGenerator<Distribution, Engine>& obj) {
	if (this != &obj) {
		Generator<result_type>::operator=(obj);
		RandEngine<Engine>::operator=(obj);

		if(obj._generator) {
			if(_generator) delete _generator;
			_generator = new GeneratorType(*(this->_engine), obj._generator->distribution());
		} else {
			delete _generator;
			_generator = nullptr;
		}
	}
	return *this;
}

/**
 * Copy constructor.
 */
template<class Distribution, class Engine>
BoostGenerator<Distribution, Engine>::BoostGenerator(
        const BoostGenerator<Distribution, Engine>& obj) :
		Generator<
		        typename boost::variate_generator<Engine, Distribution>::result_type>(), RandEngine<
		        Engine>(obj), _generator((obj._generator)?(new GeneratorType(*(this->_engine), obj._generator->distribution())):(nullptr)) {
}

/**
 * Default constructor.
 *
 * \warning This does not construct a valid generator. You need to initialise
 * distribution in order to do that.
 **/
template<class Distribution, class Engine>
BoostGenerator<Distribution, Engine>::BoostGenerator(const shared_ptr<Engine>& engine_):
RandEngine<Engine>(engine_), _generator(nullptr) {
}

/**
 * Constructor.
 * @param engine Pointer to the underlying generator engine.
 * @param distribution Probability distribution of the underlying generator.
 **/
template<class Distribution, class Engine>
BoostGenerator<Distribution, Engine>::BoostGenerator(const Distribution& distribution_,
        const shared_ptr<Engine>& engine_) :
        RandEngine<Engine>(engine_), _generator(new GeneratorType(*(this->_engine), distribution_)) {
}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::BoostGenerator, 2)

#endif // BOOSTGENERATOR_H_INCLUDED
