#ifndef SYS_ROWVECTOR_GENERATOR_H_INCLUDED
#define SYS_ROWVECTOR_GENERATOR_H_INCLUDED

#include "../math/algebra.h"
#include "Generator.h"
#include "RandEngine.h"

namespace systematic {

/**
* @class RowVectorGenerator
* @author Michal Gregor
*
* Generates random RowVector objects taken from the specified bounds.
**/
class RowVectorGenerator: public Generator<RowVector>, public RandEngine<DefaultRandEngine> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	void serialize(OArchive& ar, const unsigned int version) const;
	void serialize(IArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The lower bound in each dimension.
	RowVector _lowerBound;
	//! The upper bound in each dimension.
	RowVector _upperBound;

public:
	void setBounds(const RowVector& lowerBound, const RowVector& upperBound);
	void setBounds(RowVector&& lowerBound, RowVector&& upperBound);
	const RowVector& getLowerBound() const {return _lowerBound;}
	const RowVector& getUpperBound() const {return _upperBound;}

public:
	//! Generates the vectors.
	virtual RowVector operator()();

public:
	RowVectorGenerator();

	RowVectorGenerator(
		const RowVector& lowerBound,
		const RowVector& upperBound,
		const shared_ptr<DefaultRandEngine>& engine = NewDefaultEngine()
	);

	RowVectorGenerator(
		RowVector&& lowerBound,
		RowVector&& upperBound,
		const shared_ptr<DefaultRandEngine>& engine = NewDefaultEngine()
	);

	virtual ~RowVectorGenerator() = default;
};

} // namespace annalyst

SYS_EXPORT_CLASS(systematic::RowVectorGenerator)

#endif // SYS_ROWVECTOR_GENERATOR_H_INCLUDED

