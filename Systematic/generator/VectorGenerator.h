#ifndef VECTORGENERATOR_H_
#define VECTORGENERATOR_H_

#include "../system.h"
#include "Generator.h"

namespace systematic {

/**
* @class VectorGenerator
* @author Michal Gregor
*
* Wraps in a Generator<Type> of a certain type and uses it to generate a vector
* of Type objects.
*
* @tparam Type Type of the generated vector's element.
**/
template<class Type>
class VectorGenerator: public Generator<std::vector<Type> > {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Generator<std::vector<Type> > >(*this);
		ar & _vectorSize;
		ar & _generator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The size that the generated vector is to have.
	unsigned int _vectorSize;
	//! The underlying generator.
	shared_ptr<Generator<Type> > _generator;

public:
	using Generator<std::vector<Type> >::operator();
	virtual std::vector<Type> operator()();

	VectorGenerator<Type>& operator=(const VectorGenerator<Type>& obj);
	VectorGenerator(const VectorGenerator<Type>& obj);

	VectorGenerator();
	VectorGenerator(unsigned int vectorSize, const shared_ptr<Generator<Type> >& generator);

	virtual ~VectorGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
 * Generates a std::vector<Type> of the pre-specified size.
 */
template<class Type>
std::vector<Type> VectorGenerator<Type>::operator()() {
	return (*_generator)(_vectorSize);
}

/**
* Assignment operator.
**/
template<class Type>
VectorGenerator<Type>& VectorGenerator<Type>::operator=(const VectorGenerator<Type>& obj) {
	if(&obj != this) {
		Generator<std::vector<Type> >::operator=(obj);
		_generator = shared_ptr<Generator<Type> >(clone(obj._generator));
	}

	return *this;
}

/**
* Copy constructor.
**/
template<class Type>
VectorGenerator<Type>::VectorGenerator(const VectorGenerator<Type>& obj):
Generator<std::vector<Type> >(obj),
_generator(clone(obj._generator)) {}

/**
 * Default constructor.
 *
 * The generator needs to be initialized using assignment in order to function.
 */
template<class Type>
VectorGenerator<Type>::VectorGenerator():
_vectorSize(), _generator() {}

/**
* Constructor.
* @param vectorSize The size that the generated vector is to have.
* @param generator The underlying generator used to generate individual
* elements of the vector.
**/
template<class Type>
VectorGenerator<Type>::VectorGenerator(
	unsigned int vectorSize,
	const shared_ptr<Generator<Type> >& generator
): _vectorSize(vectorSize), _generator(generator) {}

}//namespace systematic

SYS_EXPORT_TEMPLATE(systematic::VectorGenerator, 1)

#endif /* VECTORGENERATOR_H_ */
