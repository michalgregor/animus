#ifndef COLLISIONSEQGENERATOR_H_INCLUDED
#define COLLISIONSEQGENERATOR_H_INCLUDED

#include "../system.h"
#include <vector>
#include "../math/Compare.h"
#include "SeqGenerator.h"

namespace systematic {

#define __max_tries__ 10

/**
* @class CollisionSeqGenerator
* @author Michal Gregor
*
* @brief Used to generate sequences of random data with and without
* repetition.
*
* Generation of sequences without repetition is based on the collision
* detection principle and the maximum number of retries. This class is
* specifically designed to be used with the BoundaryGenerator but works
* for other Generators as well.
**/
template<class GeneratorType>
class CollisionSeqGenerator: public SeqGenerator<typename GeneratorType::result_type> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<SeqGenerator<result_type> >(*this);
		ar & _maxTries;
		ar & _generator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	typedef typename GeneratorType::result_type result_type;

private:
	//! The maximum number of retries in RandSequenceCD selection.
	unsigned int _maxTries;
	//! The Generator that provides generation of individual pieces of data.
	shared_ptr<GeneratorType> _generator;

protected:
	std::vector<result_type> selectCD(unsigned int size);

public:
	using SeqGenerator<result_type>::operator();

	virtual result_type operator()();
	std::vector<result_type> operator()(unsigned int size, bool repetition);

	inline shared_ptr<GeneratorType> getGenerator();
	inline void setGenerator(const shared_ptr<GeneratorType>& generator);

	CollisionSeqGenerator<GeneratorType>& operator=(const CollisionSeqGenerator<GeneratorType>& obj);
	CollisionSeqGenerator(const CollisionSeqGenerator<GeneratorType>& obj);

	CollisionSeqGenerator();
	explicit CollisionSeqGenerator(const shared_ptr<GeneratorType>& generator, unsigned int maxTries = __max_tries__);

	virtual ~CollisionSeqGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Returns the Generator that provides generation of invididual pieces of data.
**/
template<class GeneratorType>
shared_ptr<GeneratorType> CollisionSeqGenerator<GeneratorType>::getGenerator() {
	return _generator;
}

/**
* Sets the Generator that provides generation of invididual pieces of data.
**/
template<class GeneratorType>
void CollisionSeqGenerator<GeneratorType>::setGenerator(const shared_ptr<GeneratorType>& generator) {
	_generator = generator;
}

/**
* Selects a random number that satisfies the preset constraints.
**/
template<class GeneratorType>
typename CollisionSeqGenerator<GeneratorType>::result_type
CollisionSeqGenerator<GeneratorType>::operator()() {
	return (*_generator)();
}

/**
* Selects a sequence of numbers without repetition.
* @param size The length of the sequence.
**/
template<class GeneratorType>
std::vector<typename CollisionSeqGenerator<GeneratorType>::result_type>
CollisionSeqGenerator<GeneratorType>::selectCD(unsigned int size) {
	std::vector<result_type> selection; selection.resize(size);

	for(unsigned int i = 0; i < size; i++) {
		unsigned int j;

		for(j = 0; j < _maxTries; j++) {
			result_type choice = (*_generator)();

			unsigned int k;

			for(k = 0; k < i; k++) {
				if(Compare(choice, selection[k])) break;
			}

			//if no collision found
			if(k == i) {
				selection[i] = choice;
				break;
			}
		}

		if(j == _maxTries) throw TracedError_Generic("The maximum number of tries reached.");
	}

	return selection;
}

/**
* Selects a sequence of numbers.
* @param size The length of the sequence.
* @param repetition Specifies whether repetition of numbers within the
* sequence is allowed. If set to true, numbers may repeat. If set to
* false, repetition will be prevented.
**/
template<class GeneratorType>
std::vector<typename CollisionSeqGenerator<GeneratorType>::result_type>
CollisionSeqGenerator<GeneratorType>::operator()(unsigned int size, bool repetition) {
	if(repetition) return (*_generator)(size);
	else return selectCD(size);
}

/**
 * Assignment operator.
 */
template<class GeneratorType>
CollisionSeqGenerator<GeneratorType>&
CollisionSeqGenerator<GeneratorType>::operator=(const CollisionSeqGenerator<GeneratorType>& obj) {
	if(this != &obj) {
		SeqGenerator<typename GeneratorType::result_type>::operator=(obj);
		_maxTries = obj._maxTries;
		_generator = obj._generator;
	}
	return *this;
}

/**
 * Copy constructor.
 */
template<class GeneratorType>
CollisionSeqGenerator<GeneratorType>::
CollisionSeqGenerator(const CollisionSeqGenerator<GeneratorType>& obj):
SeqGenerator<typename GeneratorType::result_type>(obj), _maxTries(obj._maxTries),
_generator(obj._generator) {}

/**
* Constructor.
* @param lowerBound the lower boundary – the lowest value the randomly selected
* numbers are to have.
* @param upperBound The upper boundary – the greatest value the randomly selected
* numbers are to have.
* @param maxTries The maximum number of retries in RandSequenceCD selection.
**/
template<class GeneratorType>
CollisionSeqGenerator<GeneratorType>::CollisionSeqGenerator(
	const shared_ptr<GeneratorType>& generator, unsigned int maxTries
): _maxTries(maxTries), _generator(generator) {}

/**
* Default constructor.
**/
template<class GeneratorType>
CollisionSeqGenerator<GeneratorType>::CollisionSeqGenerator():
	_maxTries(__max_tries__), _generator() {}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::CollisionSeqGenerator, 1)

#endif // COLLISIONSEQGENERATOR_H_INCLUDED
