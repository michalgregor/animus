#ifndef ROULETTEOBJECTGENERATOR_H_
#define ROULETTEOBJECTGENERATOR_H_

#include "../system.h"
#include "Generator.h"
#include "RouletteGenerator.h"

namespace systematic {

/**
* @class RouletteObjectGenerator
* @author Michal Gregor
* Based on the roulette-based RouletteGenerator, this generator selects
* objects of an arbitrary type from a supplied vector as opposed to just
* returning indices as integers as RouletteGenerator does.
**/
template<class Type, class ProbabilityType = float, class Engine = DefaultRandEngine>
class RouletteObjectGenerator: public Generator<Type> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Generator<Type> >(*this);
		ar & _generator;
		ar & _objects;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The underlying roulette generator.
	RouletteGenerator<ProbabilityType, Engine> _generator;
	//! A vector of objects associated with the prescribed probability distribution.
	std::vector<Type> _objects;

public:
	using Generator<Type>::operator();
	virtual Type operator()();

	unsigned int size() const {
		return _objects.size();
	}

	void setProbabilities(const std::vector<ProbabilityType>& probabilities, const std::vector<Type>& objects);

	RouletteObjectGenerator<Type, ProbabilityType, Engine>& operator=(const RouletteObjectGenerator<Type, ProbabilityType, Engine>& obj);
	RouletteObjectGenerator(const RouletteObjectGenerator<Type, ProbabilityType, Engine>& obj);

	RouletteObjectGenerator(
		const std::vector<Type>& objects,
		const std::vector<ProbabilityType>& probabilities,
		const shared_ptr<Engine>& engine = NewEngine<Engine>()
	);

	explicit RouletteObjectGenerator(const shared_ptr<Engine>& engine = NewEngine<Engine>());

	virtual ~RouletteObjectGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Rolls the roulette and return index of the chosen index.
**/
template<class Type, class ProbabilityType, class Engine>
Type RouletteObjectGenerator<Type, ProbabilityType, Engine>::operator()() {
	return _objects.at(_generator());
}

/**
* Computes cumulative probabilities and _generator boundaries for a new set of
* probabilities. The sum of probabilities supplied using the probabilities
* vector does not have to sum up to 1.
**/
template<class Type, class ProbabilityType, class Engine>
void RouletteObjectGenerator<Type, ProbabilityType, Engine>::
setProbabilities(
	const std::vector<ProbabilityType>& probabilities,
	const std::vector<Type>& objects
) {
	if(probabilities.size() != objects.size())
		throw TracedError_InvalidArgument("Probabilities vector is of different size than the objects vector.");

	_generator.setProbabilities(probabilities);
	_objects = objects;
}

/**
* Assignment operator.
**/
template<class Type, class ProbabilityType, class Engine>
RouletteObjectGenerator<Type, ProbabilityType, Engine>&
RouletteObjectGenerator<Type, ProbabilityType, Engine>::operator=(const RouletteObjectGenerator<Type, ProbabilityType, Engine>& obj) {
	if(&obj != this) {
		Generator<Type>::operator=(obj);
		_generator = obj._generator;
		_objects = obj._objects;
	}

	return *this;
}

/**
* Copy constructor.
**/
template<class Type, class ProbabilityType, class Engine>
RouletteObjectGenerator<Type, ProbabilityType, Engine>::RouletteObjectGenerator(const RouletteObjectGenerator<Type, ProbabilityType, Engine>& obj):
Generator<Type>(obj), _generator(obj._generator), _objects(obj._objects) {}

/**
* Constructor.
**/
template<class Type, class ProbabilityType, class Engine>
RouletteObjectGenerator<Type, ProbabilityType, Engine>::RouletteObjectGenerator(
	const std::vector<Type>& objects,
	const std::vector<ProbabilityType>& probabilities,
	const shared_ptr<Engine>& engine
): Generator<Type>(), _generator(probabilities, engine), _objects(objects) {
	if(_generator.size() != _objects.size()) throw TracedError_InvalidArgument("Probabilities vector is of different size than the objects vector.");
}

/**
* Constructor.
**/
template<class Type, class ProbabilityType, class Engine>
RouletteObjectGenerator<Type, ProbabilityType, Engine>::RouletteObjectGenerator(
	const shared_ptr<Engine>& engine
): Generator<Type>(), _generator(engine), _objects() {}

}//namespace systematic

SYS_EXPORT_TEMPLATE(systematic::RouletteObjectGenerator, 3)

#endif /* ROULETTEOBJECTGENERATOR_H_ */
