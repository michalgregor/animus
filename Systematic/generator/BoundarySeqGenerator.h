#ifndef BOUNDARYSEQGENERATOR_H_INCLUDED
#define BOUNDARYSEQGENERATOR_H_INCLUDED

#include "../system.h"
#include "BoundaryGenerator.h"
#include "CollisionSeqGenerator.h"

namespace systematic {

/**
* @class BoundarySeqGenerator
* @author Michal Gregor
* @brief Utility class combining CollisionSeqGenerator and BoundaryGenerator.
* The same behaviour can be acchieved using the classes directly.
**/
template<class Type, class Engine = DefaultRandEngine>
class BoundarySeqGenerator: public CollisionSeqGenerator<BoundaryGenerator<Type, Engine> > {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<CollisionSeqGenerator<BoundaryGenerator<Type, Engine> > >(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	BoundarySeqGenerator<Type, Engine>& operator=(const BoundarySeqGenerator<Type, Engine>& obj);
	BoundarySeqGenerator(const BoundarySeqGenerator<Type, Engine>& obj);

	BoundarySeqGenerator(
		Type lowerBound = 0,
		Type upperBound = 0,
		unsigned int maxTries = __max_tries__
	);

	BoundarySeqGenerator(
		Type lowerBound,
		Type upperBound,
		const shared_ptr<Engine>& engine,
		unsigned int maxTries = __max_tries__
	);

	//! Empty body of the virtual destructor.
	virtual ~BoundarySeqGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
 * Assignment operator.
 */
template<class Type, class Engine>
BoundarySeqGenerator<Type, Engine>&
BoundarySeqGenerator<Type, Engine>::operator=(const BoundarySeqGenerator<Type, Engine>& obj) {
	CollisionSeqGenerator<BoundaryGenerator<Type, Engine> >::operator=(obj);
	return *this;
}

/**
 * Copy constructor.
 */
template<class Type, class Engine>
BoundarySeqGenerator<Type, Engine>::BoundarySeqGenerator(const BoundarySeqGenerator<Type, Engine>& obj):
CollisionSeqGenerator<BoundaryGenerator<Type, Engine> >(obj) {}

/**
* Constructor.
* @param lowerBound the lower boundary – the lowest value the randomly selected
* numbers are to have.
* @param upperBound The upper boundary – the greatest value the randomly selected
* numbers are to have.
* @param maxTries The maximum number of retries in RandSequenceCD selection.
**/
template<class Type, class Engine>
BoundarySeqGenerator<Type, Engine>::BoundarySeqGenerator(
	Type lowerBound,
	Type upperBound,
	unsigned int maxTries
): CollisionSeqGenerator<BoundaryGenerator<Type, Engine> >(
	shared_ptr<BoundaryGenerator<Type, Engine> >(new BoundaryGenerator<Type, Engine>(lowerBound, upperBound)), maxTries
) {}

/**
* Constructor.
* @param lowerBound the lower boundary – the lowest value the randomly selected
* numbers are to have.
* @param upperBound The upper boundary – the greatest value the randomly selected
* numbers are to have.
* @param engine Engine of the random generator.
* @param maxTries The maximum number of retries in RandSequenceCD selection.
**/
template<class Type, class Engine>
BoundarySeqGenerator<Type, Engine>::BoundarySeqGenerator(
	Type lowerBound,
	Type upperBound,
	const shared_ptr<Engine>& engine,
	unsigned int maxTries
): CollisionSeqGenerator<BoundaryGenerator<Type, Engine> >(
	shared_ptr<BoundaryGenerator<Type, Engine> >(new BoundaryGenerator<Type, Engine>(lowerBound, upperBound, engine)), maxTries
) {}

//---------------------------specialization for bool----------------------------

/**
* @class BoundarySeqGenerator
* @author Michal Gregor
* @brief Utility class combining CollisionSeqGenerator and BoundaryGenerator.
* The same behaviour can be acchieved using the classes directly.
*
* BoundaryGenerator<bool> uses a different constructor, hence the specialization.
**/
template<class Engine>
class BoundarySeqGenerator<bool, Engine>:
public CollisionSeqGenerator<BoundaryGenerator<bool, Engine> > {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<CollisionSeqGenerator<BoundaryGenerator<bool, Engine> > >(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	BoundarySeqGenerator<bool, Engine>& operator=(const BoundarySeqGenerator<bool, Engine>& obj);
	BoundarySeqGenerator(const BoundarySeqGenerator<bool, Engine>& obj);

	explicit BoundarySeqGenerator(unsigned int maxTries = __max_tries__);

	explicit BoundarySeqGenerator(
		const shared_ptr<Engine>& engine,
		unsigned int maxTries = __max_tries__
	);

	//! Empty body of the virtual destructor.
	virtual ~BoundarySeqGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
 * Assignment operator.
 */
template<class Engine>
BoundarySeqGenerator<bool, Engine>&
BoundarySeqGenerator<bool, Engine>::operator=(const BoundarySeqGenerator<bool, Engine>& obj) {
	CollisionSeqGenerator<BoundaryGenerator<bool, Engine> >::operator=(obj);
	return *this;
}

/**
 * Copy constructor.
 */
template<class Engine>
BoundarySeqGenerator<bool, Engine>::BoundarySeqGenerator(const BoundarySeqGenerator<bool, Engine>& obj):
CollisionSeqGenerator<BoundaryGenerator<bool, Engine> >(obj) {}

/**
* Constructor.
* @param maxTries The maximum number of retries in RandSequenceCD selection.
**/
template<class Engine>
BoundarySeqGenerator<bool, Engine>::BoundarySeqGenerator(unsigned int maxTries):
CollisionSeqGenerator<BoundaryGenerator<bool, Engine> >(
	shared_ptr<BoundaryGenerator<bool, Engine> >(new BoundaryGenerator<bool, Engine>()),
	maxTries
) {}

/**
* Constructor.
* @param engine Engine of the random generator.
* @param maxTries The maximum number of retries in RandSequenceCD selection.
**/
template<class Engine>
BoundarySeqGenerator<bool, Engine>::BoundarySeqGenerator(
	const shared_ptr<Engine>& engine,
	unsigned int maxTries
): CollisionSeqGenerator<BoundaryGenerator<bool, Engine> >(
	shared_ptr<BoundaryGenerator<bool, Engine> >(new BoundaryGenerator<bool, Engine>(engine)),
	maxTries
) {}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::BoundarySeqGenerator, 2)

#endif // BOUNDARYSEQGENERATOR_H_INCLUDED
