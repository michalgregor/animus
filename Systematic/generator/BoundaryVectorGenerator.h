#ifndef BoundaryVectorGenerator_H_
#define BoundaryVectorGenerator_H_

#include "../system.h"
#include "Generator.h"
#include "BoundaryGenerator.h"

namespace systematic {

/**
* @class BoundaryVectorGenerator
* @author Michal Gregor
*
* Wraps in a Generator<Type> of a certain type and uses it to generate a vector
* of Type objects.
*
* @tparam Type Type of the generated vector's element.
**/
template<class VectorType>
class BoundaryVectorGenerator: public Generator<VectorType> {
public:
	typedef typename VectorType::value_type value_type;

private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Generator<VectorType> >(*this);
		ar & _generators;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The underlying per-dimension generators.
	std::vector<BoundaryGenerator<value_type> > _generators;

public:
	using Generator<VectorType>::operator();
	virtual VectorType operator()();

	BoundaryVectorGenerator<VectorType>& operator=(const BoundaryVectorGenerator& obj);
	BoundaryVectorGenerator(const BoundaryVectorGenerator& obj);

	BoundaryVectorGenerator();
	BoundaryVectorGenerator(const VectorType& lbound, const VectorType& ubound);

	virtual ~BoundaryVectorGenerator() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
 * Generates a std::vector<Type> of the pre-specified size.
 */
template<class VectorType>
VectorType BoundaryVectorGenerator<VectorType>::operator()() {
	VectorType res(_generators.size());

	auto sz = _generators.size();
	for(decltype(sz) i = 0; i < sz; i++) {
		res[i] = _generators[i]();
	}

	return res;
}

/**
* Assignment operator.
**/
template<class VectorType>
BoundaryVectorGenerator<VectorType>& BoundaryVectorGenerator<VectorType>::
	operator=(const BoundaryVectorGenerator<VectorType>& obj)
{
	if(&obj != this) {
		Generator<VectorType>::operator=(obj);
		_generators = obj._generators;
	}

	return *this;
}

/**
* Copy constructor.
**/
template<class VectorType>
BoundaryVectorGenerator<VectorType>::
	BoundaryVectorGenerator(const BoundaryVectorGenerator<VectorType>& obj):
Generator<VectorType>(obj),
_generators(obj._generators) {}

/**
 * Default constructor.
 *
 * The generator needs to be initialized using assignment in order to function.
 */
template<class VectorType>
BoundaryVectorGenerator<VectorType>::BoundaryVectorGenerator():
_generators() {}

/**
* Constructor.
* @param vectorSize The size that the generated vector is to have.
* @param generator The underlying generator used to generate individual
* elements of the vector.
**/
template<class VectorType>
BoundaryVectorGenerator<VectorType>::BoundaryVectorGenerator(
	const VectorType& lbound, const VectorType& ubound
): _generators(lbound.size()) {
	if(lbound.size() != ubound.size())
		throw TracedError_InvalidArgument("lbound.size() must equal ubound.size().");

	auto sz = lbound.size();
	for(decltype(sz) i = 0; i < sz; i++) {
		_generators[i].createSpace(lbound[i], ubound[i]);
	}
}

}//namespace systematic

SYS_EXPORT_TEMPLATE(systematic::BoundaryVectorGenerator, 1)

#endif /* BoundaryVectorGenerator_H_ */
