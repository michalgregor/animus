#ifndef BOUNDARYGENERATOR_H_INCLUDED
#define BOUNDARYGENERATOR_H_INCLUDED

#include "../system.h"
#include "Generator.h"
#include "RandEngine.h"

#include <type_traits>
#include <Systematic/serialization/random_common.h>

namespace systematic {

/**
* @class BoundaryGenerator
* @author Michal Gregor
*
* @brief Used to generate random numbers from a given range.
*
* The range that the numbers are to be chosen from is [lower bound, upper bound]
* for integral numbers and [lower bound, upper bound) for floating point
* numbers.
*
* There is no generic implementation, it is necessary to provide a
* specialization.
**/
template<class Type, class Engine = DefaultRandEngine, class Enable = void>
class BoundaryGenerator {};

//------------------------------------------------------------------------------
//						SPECIALIZATION FOR INTEGRAL
//------------------------------------------------------------------------------

template<class Type>
using BoundaryGenerator_IntegralTrait = typename std::enable_if<std::is_integral<Type>::value && !std::is_same<Type, bool>::value>::type;

/**
* @class BoundaryGenerator
* @author Michal Gregor
*
* Specialization for integral types. Generates numbers from
* [lowerBound, upperBound].
**/
template<class Type, class Engine>
class BoundaryGenerator<Type, Engine, BoundaryGenerator_IntegralTrait<Type> >:
public Generator<Type>, public RandEngine<Engine> {
public:
	typedef std::uniform_int_distribution<Type> distro_type;

private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	void serialize(OArchive& ar, const unsigned int version) const {
		ar & boost::serialization::base_object<Generator<Type> >(*this);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);

		serialize_distribution(ar, _distribution, version);
	}

	/**
	* Boost serialization function.
	**/
	void serialize(IArchive& ar, const unsigned int version) {
		ar & boost::serialization::base_object<Generator<Type> >(*this);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);

		serialize_distribution(ar, _distribution, version);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The underlying distribution.
	distro_type _distribution;

public:
	using Generator<Type>::operator();
	virtual Type operator()();

	void createSpace(Type lowerBound, Type upperBound);
	unsigned int size() const;

	Type getLowerBound() const;
	Type getUpperBound() const;

public:
	distro_type& getDistribution() {
		return _distribution;
	}

	const distro_type& getDistribution() const {
		return _distribution;
	}

public:
	/**
	 * Assignment. Copies parameters of the distribution -- not its state.
	 */
	BoundaryGenerator& operator=(const BoundaryGenerator& obj) {
		Generator<Type>::operator=(obj);
		RandEngine<Engine>::operator=(obj);

		_distribution.param(obj._distribution.param());

		return *this;
	}

	/**
	 * Copy constructor. Copies parameters of the distribution -- not its state.
	 */
	BoundaryGenerator(const BoundaryGenerator& obj):
		Generator<Type>(obj), RandEngine<Engine>(obj),
		_distribution(obj._distribution.param()) {}

	BoundaryGenerator(Type lowerBound, Type upperBound, const shared_ptr<Engine>& engine = NewEngine<Engine>());
	explicit BoundaryGenerator(const shared_ptr<Engine>& engine = NewEngine<Engine>());

	virtual ~BoundaryGenerator() {
		SYS_EXPORT_INSTANCE()
	}
};

template<class Type, class Engine>
unsigned int BoundaryGenerator<Type, Engine, BoundaryGenerator_IntegralTrait<Type> >::
size() const {
	return _distribution.max() - _distribution.min() + 1;
}

/**
* Selects a random number that satisfies the preset constraints.
**/
template<class Type, class Engine>
Type BoundaryGenerator<Type, Engine, BoundaryGenerator_IntegralTrait<Type> >::
operator()() {
	return _distribution(*this->_engine);
}

/**
* Sets the constraints – the lowest and greatest value the randomly
* selected numbers are to have.
* @param lowerBound The lower boundary.
* @param upperBound The upper boundary.
**/
template<class Type, class Engine>
void BoundaryGenerator<Type, Engine, BoundaryGenerator_IntegralTrait<Type> >::
createSpace(Type lowerBound, Type upperBound) {
	_distribution.param(typename distro_type::param_type(lowerBound, upperBound));
}

/**
* Returns the lower boundary – the lowest value the randomly selected
* numbers are to have.
**/
template<class Type, class Engine>
Type BoundaryGenerator<Type, Engine, BoundaryGenerator_IntegralTrait<Type> >::
getLowerBound() const {
	return _distribution.min();
}

/**
* Returns the upper boundary – the greatest value the randomly selected
* numbers are to have.
**/
template<class Type, class Engine>
Type BoundaryGenerator<Type, Engine, BoundaryGenerator_IntegralTrait<Type> >::
getUpperBound() const {
	return _distribution.max();
}

/**
* Constructor.
* @param lowerBound the lower boundary – the lowest value the randomly selected
* numbers are to have.
* @param upperBound The upper boundary – the greatest value the randomly selected
* numbers are to have.
* @param engine Engine of the random generator.
**/
template<class Type, class Engine>
BoundaryGenerator<Type, Engine, BoundaryGenerator_IntegralTrait<Type> >::
BoundaryGenerator(Type lowerBound, Type upperBound, const shared_ptr<Engine>& engine):
Generator<Type>(), RandEngine<Engine>(engine), _distribution(lowerBound, upperBound) {}

/**
* Default constructor.
* @param engine Engine of the random generator.
**/
template<class Type, class Engine>
BoundaryGenerator<Type, Engine, BoundaryGenerator_IntegralTrait<Type> >::
BoundaryGenerator(const shared_ptr<Engine>& engine):
Generator<Type>(), RandEngine<Engine>(engine), _distribution() {}

//------------------------------------------------------------------------------
//							SPECIALIZATION FOR BOOL
//------------------------------------------------------------------------------

/**
* @class BoundaryGenerator
* @author Michal Gregor
*
* Specialization for bool.
**/
template<class Engine>
class BoundaryGenerator<bool, Engine, void>:
public Generator<bool>, public RandEngine<Engine> {
public:
	typedef std::uniform_int_distribution<char> distro_type;

private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	void serialize(OArchive& ar, const unsigned int version) const {
		ar & boost::serialization::base_object<Generator<bool> >(*this);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);

		serialize_distribution(ar, _distribution, version);
	}

	/**
	* Boost serialization function.
	**/
	void serialize(IArchive& ar, const unsigned int version) {
		ar & boost::serialization::base_object<Generator<bool> >(*this);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);

		serialize_distribution(ar, _distribution, version);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The underlying distribution
	distro_type _distribution;

public:
	using Generator<bool>::operator();
	virtual bool operator()();

	unsigned int size() const;

	bool getLowerBound() const;
	bool getUpperBound() const;

	/**
	 * Assignment. Copies parameters of the distribution -- not its state.
	 */
	BoundaryGenerator& operator=(const BoundaryGenerator& obj) {
		Generator<bool>::operator=(obj);
		RandEngine<Engine>::operator=(obj);

		_distribution.param(obj._distribution.param());

		return *this;
	}

	/**
	 * Copy constructor. Copies parameters of the distribution -- not its state.
	 */
	BoundaryGenerator(const BoundaryGenerator& obj):
		Generator<bool>(obj), RandEngine<Engine>(obj),
		_distribution(obj._distribution.param()) {}

	explicit BoundaryGenerator(const shared_ptr<Engine>& engine = NewEngine<Engine>());
	virtual ~BoundaryGenerator() {
		SYS_EXPORT_INSTANCE()
	}
};

template<class Engine>
unsigned int BoundaryGenerator<bool, Engine, void>::size() const {
	return 2;
}

/**
* Selects a random number that satisfies the preset constraints.
**/
template<class Engine>
bool BoundaryGenerator<bool, Engine, void>::operator()() {
	return _distribution(*this->_engine);
}

/**
* Returns the lower boundary – the lowest value the randomly selected
* numbers are to have.
**/
template<class Engine>
bool BoundaryGenerator<bool, Engine, void>::getLowerBound() const {
	return 0;
}

/**
* Returns the upper boundary – the greatest value the randomly selected
* numbers are to have.
**/
template<class Engine>
bool BoundaryGenerator<bool, Engine, void>::getUpperBound() const {
	return 1;
}

/**
* Default constructor.
* @param engine Engine of the random generator.
**/
template<class Engine>
BoundaryGenerator<bool, Engine, void>::BoundaryGenerator(const shared_ptr<Engine>& engine):
Generator<bool>(), RandEngine<Engine>(engine), _distribution(0, 1) {}

//------------------------------------------------------------------------------
//						 SPECIALIZATION-FOR-FLOATING
//------------------------------------------------------------------------------

template<class Type>
using BoundaryGenerator_Floating = typename std::enable_if<std::is_floating_point<Type>::value>::type;

/**
* @class BoundaryGenerator
* @author Michal Gregor
*
* @brief Used to generate random numbers from a given range.
*
* Specialization for floating point types. Generates numbers from
* [lowerBound, upperBound).
**/
template<class Type, class Engine>
class BoundaryGenerator<Type, Engine, BoundaryGenerator_Floating<Type> >:
	public Generator<Type>, public RandEngine<Engine>
{
public:
	typedef std::uniform_real_distribution<Type> distro_type;

private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	void serialize(OArchive& ar, const unsigned int version) const {
		ar & boost::serialization::base_object<Generator<Type> >(*this);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);

		serialize_distribution(ar, _distribution, version);
	}

	/**
	* Boost serialization function.
	**/
	void serialize(IArchive& ar, const unsigned int version) {
		ar & boost::serialization::base_object<Generator<Type> >(*this);
		ar & boost::serialization::base_object<RandEngine<Engine> >(*this);

		serialize_distribution(ar, _distribution, version);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The underlying distribution.
	distro_type _distribution;

public:
	distro_type& getDistribution() {
		return _distribution;
	}

	const distro_type& getDistribution() const {
		return _distribution;
	}

public:
	using Generator<Type>::operator();
	virtual Type operator()();

	void createSpace(Type lowerBound, Type upperBound);
	Type getLowerBound() const;
	Type getUpperBound() const;

	/**
	 * Assignment. Copies parameters of the distribution -- not its state.
	 */
	BoundaryGenerator& operator=(const BoundaryGenerator& obj) {
		Generator<Type>::operator=(obj);
		RandEngine<Engine>::operator=(obj);

		_distribution.param(obj._distribution.param());

		return *this;
	}

	/**
	 * Copy constructor. Copies parameters of the distribution -- not its state.
	 */
	BoundaryGenerator(const BoundaryGenerator& obj):
		Generator<Type>(obj), RandEngine<Engine>(obj),
		_distribution(obj._distribution.param()) {}

	BoundaryGenerator(Type lowerBound, Type upperBound, const shared_ptr<Engine>& engine = NewEngine<Engine>());
	explicit BoundaryGenerator(const shared_ptr<Engine>& engine = NewEngine<Engine>());

	virtual ~BoundaryGenerator() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Selects a random number that satisfies the preset constraints.
**/
template<class Type, class Engine>
Type BoundaryGenerator<Type, Engine, BoundaryGenerator_Floating<Type>>::
operator()() {
	return _distribution(*this->_engine);
}

/**
* Sets the constraints – the lowest and greatest value the randomly
* selected numbers are to have.
* @param lowerBound The lower boundary.
* @param upperBound The upper boundary.
**/
template<class Type, class Engine>
void BoundaryGenerator<Type, Engine, BoundaryGenerator_Floating<Type>>::
createSpace(Type lowerBound, Type upperBound) {
	_distribution.param(typename distro_type::param_type(lowerBound, upperBound));
}

/**
* Returns the lower boundary – the lowest value the randomly selected
* numbers are to have.
**/
template<class Type, class Engine>
Type BoundaryGenerator<Type, Engine, BoundaryGenerator_Floating<Type>>::
getLowerBound() const {
	return _distribution.min();
}

/**
* Returns the upper boundary – the greatest value the randomly selected
* numbers are to have.
**/
template<class Type, class Engine>
Type BoundaryGenerator<Type, Engine, BoundaryGenerator_Floating<Type>>::
getUpperBound() const {
	return _distribution.max();
}

/**
* Constructor.
* @param lowerBound the lower boundary – the lowest value the randomly selected
* numbers are to have.
* @param upperBound The upper boundary – the greatest value the randomly selected
* numbers are to have.
* @param engine Engine of the random generator.
**/
template<class Type, class Engine>
BoundaryGenerator<Type, Engine, BoundaryGenerator_Floating<Type>>::
BoundaryGenerator(Type lowerBound, Type upperBound, const shared_ptr<Engine>& engine):
Generator<Type>(), RandEngine<Engine>(engine), _distribution(lowerBound, upperBound) {}

/**
* Default constructor.
* @param engine Engine of the random generator.
**/
template<class Type, class Engine>
BoundaryGenerator<Type, Engine, BoundaryGenerator_Floating<Type>>::
BoundaryGenerator(const shared_ptr<Engine>& engine):
Generator<Type>(), RandEngine<Engine>(engine), _distribution() {}

template<class Type, class Engine = DefaultRandEngine>
shared_ptr<BoundaryGenerator<Type, Engine> > make_boundary(Type lbound, Type ubound) {
	return make_shared<BoundaryGenerator<Type, Engine> >(lbound, ubound);
}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::BoundaryGenerator, 3)

#endif // BOUNDARYGENERATOR_H_INCLUDED
