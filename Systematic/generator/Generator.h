#ifndef GENERATOR_H_INCLUDED
#define GENERATOR_H_INCLUDED

#include "../system.h"
#include <vector>

namespace systematic {

/**
* @class Generator
* @author Michal Gregor
* @brief The common base of generators.
**/

template<typename Type>
class Generator {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& UNUSED(ar), const unsigned int UNUSED(version)) {}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	//! Type of the result returned by operator().
	typedef Type result_type;

	//! Generates the data.
	virtual Type operator()() = 0;
	virtual std::vector<Type> operator()(unsigned int size);

	virtual ~Generator() = 0;
};

/**
* Returns a sequence of random values as a vector of specified size.
**/

template<typename Type>
std::vector<Type> Generator<Type>::operator()(unsigned int size) {
	std::vector<Type> seq; seq.reserve(size);

	for(unsigned int i = 0; i < size; i++)
		seq.push_back((*this)());

	return seq;
}

/**
* Empty body of the pure virtual destructor.
**/

template<typename Type>
Generator<Type>::~Generator() {
	SYS_EXPORT_INSTANCE();
}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::Generator, 1)

#endif // GENERATOR_H_INCLUDED
