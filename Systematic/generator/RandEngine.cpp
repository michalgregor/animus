#include "RandEngine.h"

namespace systematic {

unsigned int RandSeed() {
	static thread_local DefaultRandEngine seedEngine(std::random_device{}());
	std::uniform_int_distribution<unsigned int> distro;
	return distro(seedEngine);
}

} //namespace systematic
