#ifndef RANDENGINE_H_INCLUDED
#define RANDENGINE_H_INCLUDED

#include "../system.h"
#include "../serialization/random_common.h"

namespace systematic {

typedef std::default_random_engine DefaultRandEngine;

/**
 * Generates a random seed in some thread-safe fashion.
 */
unsigned int RandSeed();

/**
 * Creates a random engine of the specified type, seeding it with RandSeed().
 *
 * @tparam Engine Type of the engine to provide. The type must be constructible
 * from an integer seed or otherwise a specialization must be provided for type
 * Engine.
 */
template<class Engine>
shared_ptr<Engine> NewEngine() {
	static thread_local shared_ptr<Engine> engine(new Engine(RandSeed()));
	return engine;
}

/**
 * Creates a random engine of type DefaultRandEngine,
 * seeding it with RandSeed().
 */
inline shared_ptr<DefaultRandEngine> NewDefaultEngine() {
	static thread_local shared_ptr<DefaultRandEngine> engine(new DefaultRandEngine(RandSeed()));
	return engine;
}

/**
* @class RandEngine
* @author Michal Gregor
* This class provides a base for other classes that utilize random generator
* engines. It has no virtual destructor, thus inheritance should not be public
* to disallow casting to RandEngine*.
**/
template<class Engine>
class RandEngine {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & _engine;
	}

protected:
	//! Pointer to the engine.
	shared_ptr<Engine> _engine;

public:
	//! Returns the engine.
	Engine& engine() {return *_engine;}

	//! Returns a pointer to the engine.
	const shared_ptr<Engine>& getEngine() {return _engine;}
	//! Returns a pointer to the engine.
	shared_ptr<const Engine> getEngine() const {return _engine;}

	//! Sets the pointer to the engine.
	void setEngine(const shared_ptr<Engine>& engine) {
		_engine = engine;
	}

	explicit RandEngine(unsigned int seed);
	explicit RandEngine(const shared_ptr<Engine>& engine = NewEngine<Engine>());
	virtual ~RandEngine() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Creates an Engine and stores the pointer. This may have to be specialized
* for generators that are constructed differently.
**/
template<class Engine>
RandEngine<Engine>::RandEngine(unsigned int seed):
_engine(new Engine(seed)) {}

/**
* Stores the pointer to engine.
**/
template<class Engine>
RandEngine<Engine>::RandEngine(const shared_ptr<Engine>& engine):
_engine(engine) {}

} //namespace systematic

SYS_REGISTER_TYPENAME(systematic::DefaultRandEngine)
SYS_EXPORT_TEMPLATE(systematic::RandEngine, 1)

#endif // RANDENGINE_H_INCLUDED
