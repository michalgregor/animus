#ifndef SYS_STREAMS_H_INCLUDED
#define SYS_STREAMS_H_INCLUDED

#include <ostream>
#include "macros.h"

namespace systematic {

//! A null buffer; similar effect as redirecting to /dev/null.
struct SYS_API NullBuffer: std::streambuf {
	int overflow(int c) { return traits_type::not_eof(c); }
};

//! A null buffer; similar effect as redirecting to /dev/null.
struct SYS_API WNullBuffer: std::wstreambuf {
	int overflow(int c) { return traits_type::not_eof(c); }
};

//! Null buffer instance.
extern SYS_API NullBuffer nullBuffer;
//! Null buffer instance.
extern SYS_API WNullBuffer wnullBuffer;
//! A stream constructed above nullBuffer.
extern SYS_API std::ostream nullstream;
//! A stream constructed above wnullBuffer.
extern SYS_API std::wostream wnullstream;

//! The standard output usually redirected to cout.
extern SYS_API std::ostream sysout;
//! Use for reporting additional information.
extern SYS_API std::ostream sysinfo;
//! Use for reporting debug information.
extern SYS_API std::ostream sysdebug;

void SYS_API RedirectStream(std::ostream& from, std::ostream& to);

} //namespace systematic

#endif // SYS_STREAMS_H_INCLUDED
