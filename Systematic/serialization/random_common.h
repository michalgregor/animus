#ifndef BOOSTRANDOMSERIALIZATION_H_
#define BOOSTRANDOMSERIALIZATION_H_

#include <sstream>
#include <random>
#include <boost/serialization/string.hpp>
#include <Systematic/configure.h>
#include "archive.h"
#include "../export.h"

namespace systematic {

/**
 * Serialization for boost distribution.
 */
template<class Distribution>
void serialize_distribution(OArchive& ar, Distribution& distribution,
        const unsigned int UNUSED(version)) {
	std::stringstream stream;
	stream << distribution;
	std::string str = stream.str();

	ar & str;
}

template<class Distribution>
void serialize_distribution(IArchive& ar, Distribution& distribution,
        const unsigned int UNUSED(version)) {
	std::string str;
	ar & str;

	std::stringstream stream(str);

	stream >> distribution;
}

} //namespace systematic

namespace boost {
namespace serialization {

//------------------------------------------------------------------------------
//							default_random_engine
//------------------------------------------------------------------------------

/**
 * Save for std::default_random_engine.
 */
void SYS_API serialize(systematic::IArchive& ar, std::default_random_engine& engine,
        const unsigned int UNUSED(version));

/**
 * Load for std::default_random_engine.
 */
void SYS_API serialize(systematic::OArchive& ar, std::default_random_engine& engine,
        const unsigned int UNUSED(version));

} // namespace serialization
} // namespace boost

#endif /* BOOSTRANDOMSERIALIZATION_H_ */
