#ifndef PTRSERIALIZE_H_
#define PTRSERIALIZE_H_

#include <type_traits>
#include <boost/mpl/int.hpp>
#include "../exception/TracedError.h"
#include <Systematic/configure.h>

#include "archive.h"

namespace systematic {

template<class Type, class Enable = void>
struct PtrSerialize {

	/**
	* Boost does not allow to serialize pointers to data types that are not tracked,
	* like PODs. These either need to be wrapped in a wrapper class or serialized
	* by value. This function serializes the given data by pointer if possible
	* and by value if the data type is not tracked.
	**/
	static void serialize(IArchive& ar, Type*& type) {
		ar & type;
	}

	static void serialize(OArchive& ar, Type*& type) {
		ar & type;
	}
};

//---------------------specialization for untracked types----------------------

template<class Type>
struct PtrSerialize<Type,
	typename std::enable_if<
	/* If Type is a primitive type and not void */
		std::is_same<
			typename boost::serialization::implementation_level<Type>::type,
			typename boost::mpl::int_<boost::serialization::primitive_type>
		>::value
		and
		!std::is_same<Type, void>::value
	>::type
>{
	//saving
	static void serialize(OArchive& ar, Type*& type) {
		if(!type) throw TracedError_InvalidPointer("NULL pointer to POD: cannot serialize.");
		ar & *type;
	}

	//loading
	static void serialize(IArchive& ar, Type*& type) {
		if(!type) type = new Type;
		ar & *type;
	}
};

} //namespace systematic

#endif /* PTRSERIALIZE_H_ */
