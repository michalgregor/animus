#ifndef TYPEIDSERIALIZATION_H_INCLUDED
#define TYPEIDSERIALIZATION_H_INCLUDED

#include "../type_id/TypeID.h"
#include <Systematic/configure.h>
#include "archive.h"

namespace boost {
namespace serialization {

void SYS_API serialize(systematic::OArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version));
void SYS_API serialize(systematic::IArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version));

} //namespace serialization
} //namespace boost

#endif // TYPEIDSERIALIZATION_H_INCLUDED
