#ifndef Systematic_archive_H_
#define Systematic_archive_H_

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

namespace systematic {
	using IArchive = boost::archive::text_iarchive;
	using OArchive = boost::archive::text_oarchive;
} //namespace systematic

#define SYS_ARCHIVES(ADD_ARCHIVE)									\
	ADD_ARCHIVE(boost::archive::text_iarchive) 						\
	ADD_ARCHIVE(boost::archive::text_oarchive)

#endif //Systematic_archive_H_
