#ifndef BOOST_RANDOM_MT19937_H_
#define BOOST_RANDOM_MT19937_H_

#include <sstream>
#include <boost/random/mersenne_twister.hpp>
#include <Systematic/configure.h>

#include <boost/serialization/string.hpp>
#include "archive.h"
#include "../export.h"

namespace boost {
namespace serialization {

//------------------------------------------------------------------------------
//---------------------------mersenne_twister-----------------------------------
//------------------------------------------------------------------------------

void SYS_API serialize(systematic::OArchive& ar, const boost::mt19937& engine, const unsigned int UNUSED(version));
void SYS_API serialize(systematic::IArchive& ar, boost::mt19937& engine, const unsigned int UNUSED(version));

} // namespace serialization
} // namespace boost

SYS_REGISTER_TYPENAME(boost::mt19937)

#endif /* BOOST_RANDOM_MT19937_H_ */
