#include "boost_random_mt19937.h"

namespace boost {
namespace serialization {

/**
 * Save for boost::mersenne_twister.
 */
void SYS_API serialize(systematic::OArchive& ar, const boost::mt19937& engine, const unsigned int UNUSED(version)) {
	std::stringstream sstream;
	sstream << engine;
	std::string str(sstream.str());
	ar & str;
}

/**
 * Load for boost::mersenne_twister.
 */
void SYS_API serialize(systematic::IArchive& ar, boost::mt19937& engine, const unsigned int UNUSED(version)) {
	std::string str;
	ar & str;
	std::stringstream sstream(str);
	sstream >> engine;
}

} //namespace serialization
} //namespace boost
