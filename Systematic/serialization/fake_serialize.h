#ifndef FAKE_SERIALIZE_H_
#define FAKE_SERIALIZE_H_

#include <Systematic/configure.h>
#include <boost/preprocessor/repetition/enum_params.hpp>

/**
 * This macro provides an empty boost serialization function for the specified
 * class (parameter TYPE). This is useful when using a type for which a boost
 * serialize function cannot easily be implemented with a class like
 * PolyContainer which requires that any contained type be serializable.
 *
 * Note also that this macro should only be invoked in the global namespace.
 *
 * @example
 *
 * class Foo {};
 *
 * SYS_FAKE_SERIALIZE_CLASS(Foo)
 *
 */

#define SYS_FAKE_SERIALIZE_CLASS(TYPE)									\
namespace boost {														\
namespace serialization {												\
template<class Archive>													\
void serialize(Archive&, TYPE&, const unsigned int) {}					\
} /*namespace serialization*/											\
} /*namespace boost*/

/**
 * This macro provides an empty boost serialization function for the specified
 * class template (parameter TEMPLATE) with a given number of template
 * parameters of class (parameter NUM_PARAMS).
 *
 * This is useful when using a type for which a boost serialize function cannot
 * easily be implemented with a class like PolyContainer which requires that any
 * contained type be serializable.
 *
 * @example
 *
 * template<class A1, class A2>
 * class Foo {};
 *
 * SYS_FAKE_SERIALIZE_TEMPLATE(Foo, 2)
 *
 * Note also that this macro should only be invoked in the global namespace.
 */

#define SYS_FAKE_SERIALIZE_TEMPLATE(TEMPLATE, NUM_PARAMS)				\
namespace boost {														\
namespace serialization {												\
template<class Archive, BOOST_PP_ENUM_PARAMS(NUM_PARAMS, class T)>		\
void serialize(													\
	Archive&,															\
	TEMPLATE<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T)>&,						\
	const unsigned int													\
) {}																	\
} /*namespace serialization*/											\
} /*namespace boost*/

/**
 * This macro provides an empty boost serialization function for the specified
 * class template (parameter TEMPLATE) with a given list of template
 * parameters - type or non-type.
 *
 * Parameter TPARAMS_DECL should contain the list of types and names of template
 * parameters separated by SYS_COMMA() (e.g. class A1 SYS_COMMA() bool A2). Any
 * commas are to be replaced by SYS_COMMA() as well.
 *
 * Parameter TPARAMS_VALS should contain the list of names of template
 * parameters separated by SYS_COMMA() (e.g. A1 SYS_COMMA() A2). Any commas are to
 * be replaced by SYS_COMMA() as well.
 *
 * This macro is useful when using a type for which a boost serialize function
 * cannot easily be implemented with a class like PolyContainer which requires
 * that any contained type be serializable.
 *
 * @example
 *
 * template<int A1, class A2, bool A3>
 * class Foo {};
 *
 * SYS_FAKE_SERIALIZE_TEMPLATE_DETAILED(Foo, int A1 SYS_COMMA() class A2 SYS_COMMA() bool A3, A1 SYS_COMMA() A2 SYS_COMMA() A3)
 *
 * Note also that this macro should only be invoked in the global namespace.
 */

#define SYS_FAKE_SERIALIZE_TEMPLATE_DETAILED(TEMPLATE, TPARAMS_DECL, TPARAMS_VALS)	\
namespace boost {																	\
namespace serialization {															\
template<class Archive, TPARAMS_DECL >												\
void serialize(																		\
	Archive&,																		\
	TEMPLATE<TPARAMS_VALS >&,														\
	const unsigned int																\
) {}																				\
} /*namespace serialization*/														\
} /*namespace boost*/

#endif /* FAKE_SERIALIZE_H_ */
