#ifndef BOOSTCIRCULARBUFFER_H_
#define BOOSTCIRCULARBUFFER_H_

#include "../system.h"
#include "archive.h"
#include "../container/circular_buffer.h"

namespace boost {
namespace serialization {

/**
 * Save for circular_buffer.
 */
template<class Type>
void serialize(systematic::OArchive& ar, circular_buffer<Type>& buf, const unsigned int UNUSED(version)) {
	std::vector<Type> vec(buf.begin(), buf.end());
	typename circular_buffer<Type>::capacity_type capacity(buf.capacity());

	ar & capacity;
	ar & vec;
}

/**
 * Load for circular_buffer.
 */
template<class Type>
void serialize(systematic::IArchive& ar, circular_buffer<Type>& buf, const unsigned int UNUSED(version)) {
	std::vector<Type> vec;
	typename circular_buffer<Type>::capacity_type capacity;

	ar & capacity;
	ar & vec;

	buf.clear();
	buf.set_capacity(capacity);
	buf.insert(buf.begin(), vec.begin(), vec.end());
}

} //namespace serialization
} //namespace boost

#endif /* BOOSTCIRCULARBUFFER_H_ */
