#ifndef BOOST_INTRUSIVE_PTR_SERIALIZATION_H_INCLUDED
#define BOOST_INTRUSIVE_PTR_SERIALIZATION_H_INCLUDED

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

//  Copyright (c) 2003 Vladimir Prus.
// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

// Provides non-intrusive serialization for boost::intrusive_ptr
// Does not allow to serialize intrusive_ptr's to builtin types.

// NOTE: I'm not sure if this will work, it is just copied from scoped_ptr source...

#include <boost/config.hpp>

#include <boost/intrusive_ptr.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/split_free.hpp>

#include <Systematic/configure.h>
#include "archive.h"

namespace boost {
namespace serialization {

    template<class T>
    void serialize(
        systematic::OArchive& ar,
        boost::intrusive_ptr<T>& t,
        const unsigned int /* version */
    ){
        T* r = t.get();
        ar << boost::serialization::make_nvp("intrusive_ptr", r);
    }

    template<class T>
    void serialize(
		systematic::IArchive& ar,
        boost::intrusive_ptr<T>& t,
        const unsigned int /* version */
    ){
        T* r;
        ar >> boost::serialization::make_nvp("intrusive_ptr", r);
        t.reset(r);
    }

} // namespace serialization
} // namespace boost

#endif // BOOST_INTRUSIVE_PTR_SERIALIZATION_H_INCLUDED
