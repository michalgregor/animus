#include "random_common.h"

namespace boost {
namespace serialization {

/**
 * Save for std::default_random_engine.
 */
void SYS_API serialize(systematic::IArchive& ar, std::default_random_engine& engine,
        const unsigned int UNUSED(version)) {
	std::stringstream sstream;
	sstream << engine;
	std::string str(sstream.str());
	ar & str;
}

/**
 * Load for std::default_random_engine.
 */
void SYS_API serialize(systematic::OArchive& ar, std::default_random_engine& engine,
        const unsigned int UNUSED(version)) {
	std::string str;
	ar & str;
	std::stringstream sstream(str);
	sstream >> engine;
}

} //namespace serialization
} //namespace boost
