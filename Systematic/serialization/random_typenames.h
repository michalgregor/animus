#ifndef Systematic_random_typenames_H_
#define Systematic_random_typenames_H_

#include "../export.h"
#include <boost/random.hpp>

SYS_REGISTER_TEMPLATENAME(boost::normal_distribution, 1)

SYS_REGISTER_TEMPLATENAME(boost::uniform_int, 1)
SYS_REGISTER_TEMPLATENAME(boost::uniform_smallint, 1)

#endif //Systematic_random_typenames_H_
