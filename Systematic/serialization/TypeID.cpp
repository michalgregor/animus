#include "TypeID.h"
#include <boost/serialization/string.hpp>

namespace boost {
namespace serialization {

/**
* TypeID serialization.
**/
void SYS_API serialize(systematic::OArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version)) {
   const std::string& name(systematic::TypeIDTranslator::getName(g._idcode));
	ar & name;
}

/**
* TypeID deserialization.
**/
void SYS_API serialize(systematic::IArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version)) {
   std::string name;
	ar & name;
	g._idcode = systematic::TypeIDTranslator::getID(name);
}

} //namespace serialization
} //namespace boost
