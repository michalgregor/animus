#ifndef Systematic_systhrow_H_
#define Systematic_systhrow_H_

#include "../macros.h"

/**
 * @macro SYS_THROW_ASSERT
 *
 * Provides an assert-like syntax for throwing exceptions. If the CONDITION is
 * not true, EXCEPTIONTYPE is thrown. EXCEPTIONTYPE must be constructible from
 * a string literal message. The message passed to the constructor is the
 * stringified CONDITION.
 */
#define SYS_THROW_ASSERT(CONDITION, EXCEPTIONTYPE)					\
if(!(CONDITION)) throw EXCEPTIONTYPE(SYS_STRINGIFY(CONDITION));

/**
 * @macro SYS_THROW_ASSERT
 *
 * Provides an assert-like syntax for throwing exceptions. If the CONDITION is
 * not true, EXCEPTIONTYPE is thrown. MESSAGE is passed to EXCEPTIONTYPE's
 * constructor.
 */
#define SYS_THROW_ASSERT_MSG(CONDITION, EXCEPTIONTYPE, MESSAGE)		\
if(!(CONDITION)) throw EXCEPTIONTYPE(MESSAGE);

#endif //Systematic_systhrow_H_
