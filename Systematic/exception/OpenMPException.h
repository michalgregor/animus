#ifndef SYS_OPENMPEXCEPTION_H_INCLUDED
#define SYS_OPENMPEXCEPTION_H_INCLUDED

#include <exception>
#include <string>
#include "TracedError.h"

namespace systematic {

/**
* @class OpenMPException
* @author Michal Gregor
*
* A utility class used to carry exceptions out from OpenMP sections. The
* error message of the first exception (as only one exception can be rethrown)
* is saved and the exception may be rethrown by calling Rethrow once it is safe.
*
* @tparam Exception The type of exception to rethrow as. A constructor
* taking in an error message as string is required.
*
* @example
* OpenMPException<TracedError_Generic> openMPException;
* #pragma omp parallel for
* for(unsigned int i = 0; i < 500; i++) {
*	try {
*
*		// code
*
*	} catch(std::exception& err) {
*		openMPException << err;
*	} catch(...) {
*		openMPException << "Unknown exception.";
*	}
* }
*
*  openMPException.rethrow();
**/
template<class Exception = TracedError>
class OpenMPException {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & _errorSet;
		ar & _message;
	}

private:
	bool _errorSet;
	std::string _message;

public:
	/**
	* Returns true if an exception has been caught and false if no exception has
	* been caught.
	**/
	bool exceptionCaught() {
		return _errorSet;
	}

	/**
	* Stores error message to be rethrown later.
	**/
	void operator<<(std::exception& err) {
		#pragma omp critical(OpenMPException)
		{
			if(!_errorSet) {
				_errorSet = true;
				_message = err.what();
			}
		}
	}

	/**
	* Stores error message to be rethrown later.
	**/
	void operator<<(std::string err) {
		#pragma omp critical(OpenMPException)
		{
			if(!_errorSet) {
				_errorSet = true;
				_message = err;
			}
		}
	}

	/**
	* Rethrows an exception (if any) as an exception of type specified by the
	* template parameter and with the original error message.
	**/
	void rethrow() {
		if(_errorSet) throw Exception(_message);
	}

	/**
	 * Clears the exception, returning *this into its initial state.
	 */
	void clearException() {
		_errorSet = 0;
		_message.clear();
	}

	/**
	* Constructor.
	**/
	OpenMPException(): _errorSet(0), _message() {}
};

} //namespace systematic

#endif // SYS_OPENMPEXCEPTION_H_INCLUDED
