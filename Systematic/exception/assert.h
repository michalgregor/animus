#ifndef Systematic_assert_H_
#define Systematic_assert_H_

#include <cassert>
#include <iostream>

/**
 * SYS_ASSERT(CONDITION)
 * Evaluates to assert(CONDITION) if SYS_DISABLE_ASSERT is not defined and
 * to ((void)0) if it is.
 */

#ifndef SYS_DISABLE_ASSERT
	#define SYS_ASSERT(CONDITION) \
		if(!(CONDITION)) {std::cerr << #CONDITION << std::endl; std::abort();}
#else
	#define SYS_ASSERT(CONDITION) ((void)0)
#endif

/**
 * SYS_ASSERT_MSG(CONDITION, MESSAGE)
 * Evaluates to if(!CONDITION) {std::cerr << MESSAGE << std::endl; std::abort();}
 * if SYS_DISABLE_ASSERT is not defined and to ((void)0) if it is.
 */

#ifndef SYS_DISABLE_ASSERT
	#define SYS_ASSERT_MSG(CONDITION, MESSAGE) \
	if(!(CONDITION)) {std::cerr << MESSAGE << std::endl; std::abort();}
#else
	#define SYS_ASSERT_MSG(CONDITION, MESSAGE) ((void)0)
#endif

/**
 * SYS_ALWAYS_ASSERT(CONDITION)
 * Evaluates to assert(CONDITION). (Even if SYS_DISABLE_ASSERT is defined.)
 */

#define SYS_ALWAYS_ASSERT(CONDITION) \
	if(!(CONDITION)) {std::cerr << #CONDITION << std::endl; std::abort();}

/**
 * SYS_ALWAYS_ASSERT_MSG(CONDITION, MESSAGE)
 * Evaluates to if(!CONDITION) {std::cerr << MESSAGE << std::endl; std::abort();}.
 * (Even if SYS_DISABLE_ASSERT is defined.)
 */

#define SYS_ALWAYS_ASSERT_MSG(CONDITION, MESSAGE) \
	if(!(CONDITION)) {std::cerr << MESSAGE << std::endl; std::abort();}

#endif //Systematic_assert_H_
