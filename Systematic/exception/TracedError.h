#ifndef SYS_TRACEDERROR_H_INCLUDED
#define SYS_TRACEDERROR_H_INCLUDED

#include <stdexcept>
#include <iostream>
#include <string>
#include <Systematic/configure.h>

namespace systematic {

/**
 * @class TracedError
 *
 * @brief An exception class derived from std::runtime_error. Adds a
 * macro-based mechanism which adds a refined function signature to
 * the error message.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError: public std::runtime_error {
protected:
	TracedError(const std::string &__arg): std::runtime_error(__arg) {}

public:
	virtual ~TracedError() throw() {}
};

/**
 * @class TracedError_Generic
 *
 * @brief An exception class based on TracedError. It is a generic exception
 * class used to handle all kinds of exceptions for which no dedicated classes
 * are implemented.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_Generic: public TracedError {
public:
	TracedError_Generic(const std::string &__arg): TracedError(__arg) {}
	virtual ~TracedError_Generic() throw() {};
};

/**
 * @class TracedError_InvalidPointer
 *
 * @brief An exception class based on TracedError. Throw when an invalid pointer
 * is encountered.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_InvalidPointer: public TracedError {
public:
	TracedError_InvalidPointer(const std::string &__arg):
		TracedError("TracedError_InvalidPointer, " + __arg) {}

	virtual ~TracedError_InvalidPointer() throw() {};
};

/**
 * @class TracedError_OutOfRange
 *
 * @brief An exception class based on TracedError. Throw at attempt to point
 * to an out-of-range element.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_OutOfRange: public TracedError {
public:
	TracedError_OutOfRange(const std::string &__arg) :
		TracedError("TracedError_OutOfRange, " + __arg) {}

	virtual ~TracedError_OutOfRange() throw() {};
};

/**
 * @class TracedError_SizeMismatch
 *
 * @brief An exception class based on TracedError. Throw when several containers
 * are to be used together and their sizes do not match.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_SizeMismatch: public TracedError {
public:
	TracedError_SizeMismatch(const std::string &__arg) :
		TracedError("TracedError_SizeMismatch, " + __arg) {}

	virtual ~TracedError_SizeMismatch() throw() {};
};

/**
 * @class TracedError_InvalidArgument
 *
 * @brief An exception class based on TracedError. Throw when upon receipt of
 * an invaliud argument.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_InvalidArgument: public TracedError {
public:
	TracedError_InvalidArgument(const std::string &__arg) :
		TracedError("TracedError_InvalidArgument, " + __arg) {}

	virtual ~TracedError_InvalidArgument() throw() {};
};

/**
 * @class TracedError_InvalidFormat
 *
 * @brief An exception class based on TracedError. Throw when a file that you
 * are reading is of unexpected and invalid format.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_InvalidFormat: public TracedError {
public:
	TracedError_InvalidFormat(const std::string &__arg) :
		TracedError("TracedError_InvalidArgument, " + __arg) {}

	virtual ~TracedError_InvalidFormat() throw() {};
};

/**
 * @class TracedError_TypeOverflow
 *
 * @brief An exception class based on TracedError. For exceptions concerning
 * overflow or underflow of integer types, etc..
 *
 * \sa TracedError_DomainError
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_TypeOverflow: public TracedError {
public:
	TracedError_TypeOverflow(const std::string &__arg) :
		TracedError("TracedError_DomainError, " + __arg) {}

	virtual ~TracedError_TypeOverflow() throw() {};
};

/**
 * @class TracedError_DomainError
 *
 * @brief An exception class based on TracedError. For exceptions concerning
 * domain of a mathematical function.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_DomainError: public TracedError {
public:
	TracedError_DomainError(const std::string &__arg) :
		TracedError("TracedError_DomainError, " + __arg) {}

	virtual ~TracedError_DomainError() throw() {};
};

/**
 * @class TracedError_ResourcesMissing
 *
 * @brief An exception class based on TracedError. Thrown when some of the required
 * resources are missing.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_ResourcesMissing: public TracedError {
public:
	TracedError_ResourcesMissing(const std::string &__arg) :
		TracedError("TracedError_ResourcesMissing, " + __arg) {}

	virtual ~TracedError_ResourcesMissing() throw() {};
};

/**
 * @class TracedError_CapacityOverflow
 *
 * @brief An exception class based on TracedError. Thrown when capacity (e.g.
 * of a container) is exceeded.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_CapacityOverflow: public TracedError {
public:
	TracedError_CapacityOverflow(const std::string &__arg) :
		TracedError("TracedError_CapacityOverflow, " + __arg) {}

	virtual ~TracedError_CapacityOverflow() throw() {};
};

/**
 * @class TracedError_NullSize
 *
 * @brief An exception class based on TracedError. Thrown when something
 * has null size and the opposite is required (e.g. there are no elements
 * in a container and you call a method which is expected to return the 0th
 * element).
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_NullSize: public TracedError {
public:
	TracedError_NullSize(const std::string &__arg) :
		TracedError("TracedError_NullSize, " + __arg) {}

	virtual ~TracedError_NullSize() throw() {};
};

/**
 * @class TracedError_NotAvailable
 *
 * @brief An exception class based on TracedError. Thrown when some required
 * resource (file, network, object, ...) is not available.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_NotAvailable: public TracedError {
public:
	TracedError_NotAvailable(const std::string &__arg) :
		TracedError("TracedError_NotAvailable, " + __arg) {}

	virtual ~TracedError_NotAvailable() throw() {};
};

/**
 * @class TracedError_AlreadyExists
 *
 * @brief An exception class based on TracedError. Thrown when creating or
 * inserting an element that already exists.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_AlreadyExists: public TracedError {
public:
	TracedError_AlreadyExists(const std::string &__arg) :
		TracedError("TracedError_AlreadyExists, " + __arg) {}

	virtual ~TracedError_AlreadyExists() throw() {};
};

/**
 * @class TracedError_FileError
 *
 * @brief An exception class based on TracedError. Thrown when a file cannot
 * be opened, read from, written to, etc.
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_FileError: public TracedError {
public:
	TracedError_FileError(const std::string &__arg) :
		TracedError("TracedError_FileError, " + __arg) {}

	virtual ~TracedError_FileError() throw() {};
};

/**
 * @class TracedError_NotImplemented
 *
 * @brief An exception class based on TracedError. Thrown when calling
 * a function which has not been implemented yet (e.g. a class does not have
 * its serialization method properly implemented).
 *
 * @author Michal Gregor
 */
class SYS_API TracedError_NotImplemented: public TracedError {
public:
	TracedError_NotImplemented(const std::string &__arg) :
		TracedError("TracedError_NotImplemented, " + __arg) {}

	virtual ~TracedError_NotImplemented() throw() {};
};

//! A macro that will emit a compile-time warning and throw a TracedError_NotImplemented
//! when the method is called.
#define SYS_METHOD_NOT_IMPLEMENTED(MESSAGE) 							\
SYS_COMPILE_WARNING(MESSAGE)											\
throw TracedError_NotImplemented(MESSAGE);

} //namespace systematic

#endif // SYS_TRACEDERROR_H_INCLUDED
