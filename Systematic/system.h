#ifndef SYS_SYSTEM_INCLUDED
#define SYS_SYSTEM_INCLUDED

/*!
\ifnot COMMON
\mainpage Systematic

\author Michal Gregor
<a href="michal.gregor.sk">(home page)</a>

\endif
*/
#include <Systematic/configure.h>
#include "streams.h"
#include "exception/TracedError.h"
#include "exception/assert.h"
#include "exception/systhrowassert.h"
#include "serialization.h"

#include "pointer/smart_ptr.h"
#include "utils/clone.h"
#include "utils/enumdef.h"

#include "utils/function.h"
#include "macros.h"
#include "math/Constant.h"
#include "math/numeric_cast.h"
#include "math/Compare.h"

#include "exception/backward.hpp"
#include "iterator/iterator.h"

// import std::swap so that we
// can use unqualified swap calls
#include <utility>
namespace systematic {
	using std::swap;
}

#endif // SYS_SYSTEM_INCLUDED
