#ifndef Systematic_fixed_H_
#define Systematic_fixed_H_

#include "FixedArithmetic.h"

namespace systematic {
	typedef typename fixed_from_type<unsigned int>::fixed_type fixed;
} //namespace systematic

#endif //Systematic_fixed_H_
