#ifndef Systematic_exp2_H_
#define Systematic_exp2_H_

#include <cmath>

namespace systematic {

#if __cplusplus < 201103L

	/**
	 * Computes the value of a base-2 exponential function.
	 */
	inline double exp2(double x) {
		return std::exp((double)0.69314718055994528622676398299518041312694549560546875*x);
	}

	/**
	 * Computes the value of a base-2 exponential function.
	 */
    inline float exp2(float x) {
    	return std::exp((float)0.69314718055994528622676398299518041312694549560546875*x);
    }

    /**
	 * Computes the value of a base-2 exponential function.
	 */
    inline long double exp2(long double x) {
    	return std::exp((long double)0.69314718055994528622676398299518041312694549560546875*x);
    }

#else

	using std::exp2;

#endif

} //namespace systematic

#endif //Systematic_exp2_H_
