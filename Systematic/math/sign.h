#ifndef Animus_sign_H_
#define Animus_sign_H_

namespace systematic {

template<class Type>
Type sign(Type val) {
	if(val > 0) return 1;
	else if(val < 0) return -1;
	else return 0;
}

} //namespace systematic

#endif //Animus_sign_H_
