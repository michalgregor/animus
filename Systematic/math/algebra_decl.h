#ifndef SYSTEMATIC_MATH_ALGEBRA_DECL_H_
#define SYSTEMATIC_MATH_ALGEBRA_DECL_H_

#include "../system.h"
#include "RealType.h"

#include <Eigen/Sparse>
#include <Eigen/Dense>

namespace systematic {

#define ALGEBRA_GENERIC_PUBLIC_INTERFACE(DERIVED) \
  typedef typename Eigen::internal::traits<DERIVED>::Scalar Scalar; /*!< \brief Numeric type, e.g. float, double, int or std::complex<float>. */ \
  typedef typename Eigen::NumTraits<Scalar>::Real RealScalar; /*!< \brief The underlying numeric type for composed scalar types. \details In cases where Scalar is e.g. std::complex<T>, T were corresponding to RealScalar. */ \
  typedef typename Base::CoeffReturnType CoeffReturnType; /*!< \brief The return type for coefficient access. \details Depending on whether the object allows direct coefficient access (e.g. for a MatrixXd), this type is either 'const Scalar&' or simply 'Scalar' for objects that do not allow direct coefficient access. */ \
  typedef DERIVED Derived; \
  typedef typename Eigen::internal::traits<DERIVED>::StorageKind StorageKind; \
  enum { RowsAtCompileTime = Eigen::internal::traits<DERIVED>::RowsAtCompileTime, \
        ColsAtCompileTime = Eigen::internal::traits<DERIVED>::ColsAtCompileTime, \
        Flags = Eigen::internal::traits<DERIVED>::Flags, \
        SizeAtCompileTime = Base::SizeAtCompileTime, \
        MaxSizeAtCompileTime = Base::MaxSizeAtCompileTime, \
        IsVectorAtCompileTime = Base::IsVectorAtCompileTime };

#if !EIGEN_VERSION_AT_LEAST(3, 2, 8)
	namespace Eigen {
		typedef int AlgebraIndex;
	}
#endif

typedef Eigen::Index AlgebraIndex;
typedef Eigen::Triplet<RealType, AlgebraIndex> Triplet;

using Eigen::EigenBase;
using Eigen::DenseBase;
using Eigen::ArrayBase;
using Eigen::MatrixBase;

typedef Eigen::SparseMatrix<RealType, 0, AlgebraIndex> BaseSparseMatrix;
typedef Eigen::Block<BaseSparseMatrix> SparseBlock;
typedef Eigen::Block<const BaseSparseMatrix> ConstSparseBlock;

template<class ScalarType>
class MatrixTpl;

template<class ScalarType>
class RowVectorTpl;

typedef MatrixTpl<RealType> Matrix;
typedef RowVectorTpl<RealType> RowVector;

template<class MatrixType>
class Ref;

typedef Ref<Matrix> MatrixRef;
typedef Ref<const Matrix> MatrixCRef;
typedef Ref<RowVector> RowVectorRef;
typedef Ref<const RowVector> RowVectorCRef;

} // namespace systematic

#endif /* SYSTEMATIC_MATH_ALGEBRA_DECL_H_ */
