#ifndef Systematic_algebra_H_
#define Systematic_algebra_H_

#include "algebra_decl.h"

#include "../format/ostream.h"
#include "../format/xformat.h"
#include "../iterator/eigen_sparse_iterator.h"
#include "../iterator/eigen_dense_iterator.h"

namespace systematic {

SYS_WNONVIRT_DTOR_OFF

//------------------------------------------------------------------------------
//								Matrix
//------------------------------------------------------------------------------

template<class ScalarType>
class MatrixTpl: public Eigen::Map<
	typename std::conditional<
		std::is_const<ScalarType>::value,
		const Eigen::Matrix<typename std::remove_const<ScalarType>::type,
			Eigen::Dynamic, Eigen::Dynamic>,
		Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic>
	>::type,
	Eigen::Aligned, Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>
> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	typedef
	Eigen::Map<
		typename std::conditional<
			std::is_const<ScalarType>::value,
			const Eigen::Matrix<typename std::remove_const<ScalarType>::type,
				Eigen::Dynamic, Eigen::Dynamic>,
			Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic>
		>::type,
		Eigen::Aligned, Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>
	> BaseClass;

	typedef Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic> StrideType;

	typedef ScalarType value_type;
	typedef ScalarType& reference;

	template<class ScalarTypeB>
	friend class MatrixTpl;

private:
	//! True if the data is not owned by this object.
	bool _dataOwned = true;

	//! Holds the data.
	//! \note You need to provide a std::default_delete<RealType[]>() deleter
	//! at construction. Ideally use allocateData() for allocation.
	shared_ptr<value_type> _data = {};

protected:
	//! Reinitializes the underlying Eigen::Map using current
	//! _data and the specified dimensions and stride.
	void initMap(AlgebraIndex rows, AlgebraIndex cols, StrideType stride);

public:
	//! Returns a StrideType object for the specified dimensions and strides.
	static StrideType makeStride(AlgebraIndex rows, AlgebraIndex cols, bool isRowMajor);
	static StrideType makeStride(AlgebraIndex rowStride, AlgebraIndex colStride);
	static StrideType makeStrideOI(AlgebraIndex outerStride, AlgebraIndex innerStride);
	static std::pair<AlgebraIndex, AlgebraIndex> strideToRowCol(const StrideType& stride);

	//! Allocates size RealType items and returns a shared_ptr to it with
	//! the appropriate deleter.
	static shared_ptr<value_type> allocateData(AlgebraIndex size);

	AlgebraIndex rowStride() const;
	AlgebraIndex colStride() const;
	StrideType getStride() const;

	//! Returns a raw pointer to the data. \see shareData, releaseData.
	value_type* data() {return _data.get();}

	//! Returns a raw pointer to the data. \see shareData, releaseData.
	const value_type* data() const {return _data.get();}

	//! Returns a shared_ptr to the contained data. While the data is shared,
	//! resizes and other operations that would invalidate the data or
	//! dissociate it from the Matrix are disallowed and yield exceptions.
	//! \see data, releaseData
	const shared_ptr<value_type>& shareData() const {
		return _data;
	}

	//! Resets the Matrix to its initial state with 0 dimensions and no data.
	void resetData();

	//! Resets the Matrix to to its initial state with 0 dimensions and no data
	//! and then resizes it to the specified dimensions.
	void resetData(AlgebraIndex rows, AlgebraIndex cols);

	//! Sets the contents of the Matrix to the specified data.
	//! @param dataOwned Specifies whether the data is owned by the Matrix.
	//! If so, the Matrix is allowed to make resizes and similar operations
	//! provided that the shared_ptr is unique.
	//!
	//! \note A shared_ptr that does not delete data upon destruction can be
	//! constructed using shared_ptr<RealType>(data, null_deleter);
	void referenceData(
		const shared_ptr<value_type>& data, bool dataOwned,
		AlgebraIndex rows, AlgebraIndex cols,
		AlgebraIndex rowStride, AlgebraIndex colStride
	);

	//! Sets up this Matrix to share the data of the other specified Matrix.
	//! This has the same effect as calling referenceData(obj.shareData(), obj.rows(),
	//! obj.cols(), obj.rowStride(), obj.colStride()).
	//! @tparam The ScalarTypeB is to allow switching between ScalarType
	//! and const ScalarType.
	template<class ScalarTypeB>
	void referenceData(MatrixTpl<ScalarTypeB>& obj) {
		referenceData(
			obj._data, obj._dataOwned,
			obj.rows(), obj.cols(),
			obj.rowStride(), obj.colStride()
		);
	}

	//! Sets up this Matrix to share the data of the other specified Matrix.
	//! This has the same effect as calling referenceData(obj.shareData(), obj.rows(),
	//! obj.cols(), obj.rowStride(), obj.colStride()).
	//! @tparam The ScalarTypeB is to allow switching between ScalarType
	//! and const ScalarType.
	template<class ScalarTypeB>
	void referenceData(MatrixTpl<ScalarTypeB>&& obj) {
		referenceData(
			obj._data, obj._dataOwned,
			obj.rows(), obj.cols(),
			obj.rowStride(), obj.colStride()
		);
	}

	//! Sets the Matrix up to reference data of the matrix-like object obj.
	//! \warning It is the responsibility of the programmer to guarantee that
	//! the data interfaced by the object is not destroyed before this Matrix.
	template<class MatrixType>
	void referenceData(MatrixType&& obj);

	//! Returns the state of the dataOwned flag (set upon construction or when
	//! referencing data). If the data is owned by the Matrix, it is allowed
	//! to make resizes and similar operations provided that the shared_ptr
	//! is unique.
	//! \sa isDataShared
	bool dataOwned() const {
		return _dataOwned;
	}

	//! Returns whether the contained data is shared with other objects or
	//! unshared and owned by the matrix. If so, this matrix cannot be resized
	//! (resizing yields exceptions). Other operations that would invalidate
	//! the data or dissociate it from the Matrix are similarly disallowed.
	//!
	//! This is true if either that data shared_ptr is not unique, or the
	//! dataOwned flag is not set.
	//! \sa dataOwned
	bool isDataShared() const;

	//! A single-dimension resize is only available for vectors.
	void resize(AlgebraIndex size) = delete;

	//! Resizes the matrix, clearing the existing data (unless the new
	//! dimensions are exactly equal to the old dimensions in which case this
	//! has no effect).
	//!
	//! \note A resize is only allowed if isDataShared() == false.
	//! \sa conservativeResize
	void resize(AlgebraIndex rows, AlgebraIndex cols, bool isRowMajor = MatrixTpl::IsRowMajor);
	void resize(AlgebraIndex rows, AlgebraIndex cols, AlgebraIndex rowStride, AlgebraIndex colStride);
	void resize(AlgebraIndex rows, AlgebraIndex cols, StrideType stride);

	//! Resizes the matrix, keeping the existing data. If the new dimensions are
	//! exactly equal to the old dimensions, the method has no effect. If new
	//! elements have to be added, they are left uninitialized.
	//!
	//! \note A resize is only allowed if isDataShared() == false.
	//! \sa resize
	void conservativeResize(AlgebraIndex rows, AlgebraIndex cols, bool isRowMajor = MatrixTpl::IsRowMajor);
	void conservativeResize(AlgebraIndex rows, AlgebraIndex cols, AlgebraIndex rowStride, AlgebraIndex colStride);
	void conservativeResize(AlgebraIndex rows, AlgebraIndex cols, StrideType stride);

public:
	template<class XprType>
	static MatrixTpl ref(XprType& xpr) {
		MatrixTpl mat;
		mat.referenceData(xpr);
		return mat;
	}

public:
	void saveData(std::ostream& stream) const;
	void loadData(std::istream& stream);

public:
	MatrixTpl pinv() const;

public:
	using BaseClass::operator=;
	MatrixTpl& operator=(std::initializer_list<std::initializer_list<value_type> > list);
	MatrixTpl& operator=(const MatrixTpl& obj);
	MatrixTpl& operator=(MatrixTpl&& obj);

public:
	template<class Derived>
	MatrixTpl(const Eigen::MatrixBase<Derived>& xpr): BaseClass(nullptr, 0, 0, makeStride(0, 0)) {
		resize(xpr.rows(), xpr.cols());
		BaseClass::operator=(xpr);
	}

	template<class Derived>
	MatrixTpl(const Eigen::SparseMatrixBase<Derived>& xpr): BaseClass(nullptr, 0, 0, makeStride(0, 0)) {
		resize(xpr.rows(), xpr.cols());
		BaseClass::operator=(xpr);
	}

	MatrixTpl();
	MatrixTpl(MatrixTpl&& obj);
	MatrixTpl(MatrixTpl& obj);
	MatrixTpl(const MatrixTpl& obj);

	MatrixTpl(AlgebraIndex rows, AlgebraIndex cols, bool isRowMajor = MatrixTpl::IsRowMajor);
	MatrixTpl(AlgebraIndex rows, AlgebraIndex cols, AlgebraIndex rowStride, AlgebraIndex colStride);
	MatrixTpl(std::initializer_list<std::initializer_list<value_type> > list);

	//! Constructs a Matrix from existing data using referenceData.
	MatrixTpl(
		const shared_ptr<value_type>& data, bool dataOwned,
		AlgebraIndex rows, AlgebraIndex cols,
		AlgebraIndex rowStride, AlgebraIndex colStride
	): MatrixTpl(data, dataOwned, rows, cols, makeStride(rowStride, colStride)) {}

	//! Constructs a Matrix from existing data using referenceData.
	MatrixTpl(
		const shared_ptr<value_type>& data, bool dataOwned,
		AlgebraIndex rows, AlgebraIndex cols, StrideType stride
	);

	~MatrixTpl() {
		SYS_EXPORT_INSTANCE()
	}
};

template<class ScalarType>
void MatrixTpl<ScalarType>::serialize(IArchive&, const unsigned int) {
//	ar & boost::serialization::make_array(data(), this->size());
	throw TracedError_NotImplemented("Serialization for matrices not implemented yet.");
}

template<class ScalarType>
void MatrixTpl<ScalarType>::serialize(OArchive&, const unsigned int) {
//	ar & boost::serialization::make_array(data(), this->size());
	throw TracedError_NotImplemented("Serialization for matrices not implemented yet.");
}

template<class ScalarType>
void MatrixTpl<ScalarType>::saveData(std::ostream& stream) const {
	fmt::print(stream, "{}, {} ", this->rows(), this->cols());
	AlgebraIndex size = this->rows() * this->cols();
	auto* data = this->data();

	for(AlgebraIndex i = 0; i < size; i++) {
		fmt::print(stream, "{} ", data[i]);
	}
}

template<class ScalarType>
void MatrixTpl<ScalarType>::loadData(std::istream& stream) {
	AlgebraIndex rows, cols;
	int res = xscanf(stream, "%d, %d", rows, cols);
	if(res != 2) throw TracedError_InvalidFormat("Cannot load data: invalid matrix format.");
	AlgebraIndex size = rows * cols;

	this->resize(rows, cols);
	auto* data = this->data();

	res = 0;
	for(AlgebraIndex i = 0; i < size; i++) {
		res += xscanf(stream, "%g ", data[i]);
	}

	if(res != size) throw TracedError_InvalidFormat("Cannot load data: invalid matrix format.");
}

//! Sets up this Matrix to reference the data of the specified block.
//! \warning It is the responsibility of the programmer to guarantee that
//! the data interfaced by the block is not destroyed before this Matrix.
template<class ScalarType>
template<class MatrixType>
void MatrixTpl<ScalarType>::referenceData(MatrixType&& obj) {
	auto stride = ((bool) MatrixTpl::IsRowMajor == (bool) std::remove_reference<MatrixType>::type::IsRowMajor) ?
		MatrixTpl::makeStrideOI(obj.outerStride(), obj.innerStride()) :
		MatrixTpl::makeStrideOI(obj.innerStride(), obj.outerStride());
	auto row_col_stride = MatrixTpl::strideToRowCol(stride);
	referenceData(shared_ptr<value_type>(obj.data(), null_deleter()), false,
		obj.rows(), obj.cols(), row_col_stride.first, row_col_stride.second);
}

template<class ScalarType>
shared_ptr<ScalarType> MatrixTpl<ScalarType>::
allocateData(AlgebraIndex size) {
	Eigen::aligned_allocator<value_type> allocator;

	return shared_ptr<value_type>(allocator.allocate(size), [](value_type* ptr){
		Eigen::aligned_allocator<value_type> alloc;
		alloc.deallocate(ptr, 0);
	});
}

template<class ScalarType>
typename MatrixTpl<ScalarType>::StrideType MatrixTpl<ScalarType>::makeStride(
	AlgebraIndex rows, AlgebraIndex cols, bool isRowMajor
) {
	AlgebraIndex rowStride, colStride;

	if(isRowMajor) {
		rowStride = cols;
		colStride = 1;
	} else {
		rowStride = 1;
		colStride = rows;
	}

	return MatrixTpl<ScalarType>::IsRowMajor ?
		StrideType(rowStride, colStride): StrideType(colStride, rowStride);
}

template<class ScalarType>
typename MatrixTpl<ScalarType>::StrideType MatrixTpl<ScalarType>::makeStride(
	AlgebraIndex rowStride, AlgebraIndex colStride
) {
	return MatrixTpl<ScalarType>::IsRowMajor ?
		StrideType(rowStride, colStride): StrideType(colStride, rowStride);
}

template<class ScalarType>
typename MatrixTpl<ScalarType>::StrideType MatrixTpl<ScalarType>::makeStrideOI(
	AlgebraIndex outerStride, AlgebraIndex innerStride
) {
	return StrideType(outerStride, innerStride);
}

template<class ScalarType>
std::pair<AlgebraIndex, AlgebraIndex> MatrixTpl<ScalarType>::strideToRowCol(
	const StrideType& stride
) {
	return MatrixTpl<ScalarType>::IsRowMajor ?
		std::pair<AlgebraIndex, AlgebraIndex>(stride.outer(), stride.inner()) :
		std::pair<AlgebraIndex, AlgebraIndex>(stride.inner(), stride.outer());
}

template<class ScalarType>
void MatrixTpl<ScalarType>::initMap(
	AlgebraIndex rows, AlgebraIndex cols, StrideType stride
) {
	new(static_cast<BaseClass*>(this)) BaseClass(_data.get(), rows, cols, stride);
}

template<class ScalarType>
AlgebraIndex MatrixTpl<ScalarType>::rowStride() const {
	return MatrixTpl<ScalarType>::IsRowMajor ? this->outerStride() : this->innerStride();
}

template<class ScalarType>
AlgebraIndex MatrixTpl<ScalarType>::colStride() const {
	return MatrixTpl<ScalarType>::IsRowMajor ? this->innerStride() : this->outerStride();
}

template<class ScalarType>
typename MatrixTpl<ScalarType>::StrideType MatrixTpl<ScalarType>::getStride() const {
	return StrideType(this->outerStride(), this->innerStride());
}

template<class ScalarType>
void MatrixTpl<ScalarType>::resetData() {
	_data = allocateData(0);
	_dataOwned = true;
	initMap(0, 0, StrideType(0, 0));
}

template<class ScalarType>
void MatrixTpl<ScalarType>::resetData(AlgebraIndex rows, AlgebraIndex cols) {
	_data = allocateData(rows*cols);
	_dataOwned = true;
	initMap(rows, cols, makeStride(rows, cols, MatrixTpl::IsRowMajor));
}

template<class ScalarType>
void MatrixTpl<ScalarType>::referenceData(
	const shared_ptr<value_type>& data, bool dataOwned,
	AlgebraIndex rows, AlgebraIndex cols,
	AlgebraIndex rowStride, AlgebraIndex colStride
) {
	_data = data;
	_dataOwned = dataOwned;
	initMap(rows, cols, makeStride(rowStride, colStride));
}

template<class ScalarType>
void MatrixTpl<ScalarType>::resize(AlgebraIndex rows_, AlgebraIndex cols_, StrideType stride) {
	if(
		this->rows() == rows_ and this->cols() == cols_ and
		this->outerStride() == stride.outer() and this->innerStride() == stride.inner()
	) {
		return;
	} else if(isDataShared()) {
		throw TracedError_NotAvailable("A matrix cannot be resized while isDataShared() == true.");
	}

	_data = allocateData(rows_*cols_);
	_dataOwned = true;
	initMap(rows_, cols_, stride);
}

template<class ScalarType>
void MatrixTpl<ScalarType>::resize(AlgebraIndex rows_, AlgebraIndex cols_, bool isRowMajor) {
	StrideType stride = makeStride(rows_, cols_, isRowMajor);
	resize(rows_, cols_, stride);
}

template<class ScalarType>
void MatrixTpl<ScalarType>::resize(
	AlgebraIndex rows_, AlgebraIndex cols_,
	AlgebraIndex rowStride, AlgebraIndex colStride
) {
	StrideType stride = makeStride(rowStride, colStride);
	resize(rows_, cols_, stride);
}

template<class ScalarType>
void MatrixTpl<ScalarType>::conservativeResize(
	AlgebraIndex rows_, AlgebraIndex cols_, StrideType stride
) {
	if(
		this->rows() == rows_ and this->cols() == cols_ and
		this->outerStride() == stride.outer() and this->innerStride() == stride.inner()
	) {
		return;
	} else if(isDataShared()) {
		throw TracedError_NotAvailable("A matrix cannot be resized while isDataShared() == true.");
	}

	// We make another ref to the old matrix temporarily.
	MatrixTpl<ScalarType> tmp(std::move(*this));

	// Then we reeinitialize everything using the new size.
	_data = allocateData(rows_*cols_);
	_dataOwned = true;
	initMap(rows_, cols_, stride);

	// Finally, we copy as much of the old contents as possible.
	AlgebraIndex copyRows = std::min(rows_, tmp.rows());
	AlgebraIndex copyCols = std::min(cols_, tmp.cols());
	this->topLeftCorner(copyRows, copyCols) = tmp.topLeftCorner(copyRows, copyCols);
}

template<class ScalarType>
void MatrixTpl<ScalarType>::conservativeResize(
	AlgebraIndex rows_, AlgebraIndex cols_, bool isRowMajor
) {
	StrideType stride = makeStride(rows_, cols_, isRowMajor);
	conservativeResize(rows_, cols_, stride);
}

template<class ScalarType>
void MatrixTpl<ScalarType>::conservativeResize(
	AlgebraIndex rows_, AlgebraIndex cols_,
	AlgebraIndex rowStride, AlgebraIndex colStride
) {
	StrideType stride = makeStride(rowStride, colStride);
	conservativeResize(rows_, cols_, stride);
}

template<class ScalarType>
bool MatrixTpl<ScalarType>::isDataShared() const {
	return !_dataOwned or (_data and !_data.unique());
}

template<class ScalarType>
MatrixTpl<ScalarType> MatrixTpl<ScalarType>::pinv() const {
	if(this->rows() < this->cols()) {
		auto svd = this->derived().transpose().jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV);
		ScalarType tolerance = (ScalarType)std::numeric_limits<ScalarType>::epsilon() * std::max((ScalarType)this->cols(), (ScalarType)this->rows()) * svd.singularValues().array().abs().maxCoeff();
		return (svd.matrixV() * MatrixTpl<ScalarType>((svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0)).asDiagonal() * svd.matrixU().adjoint()).transpose();
	}

	auto svd = this->jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV);
	ScalarType tolerance = (ScalarType)std::numeric_limits<ScalarType>::epsilon() * std::max((ScalarType)this->cols(), (ScalarType)this->rows()) * svd.singularValues().array().abs().maxCoeff();
	return svd.matrixV() * MatrixTpl<ScalarType>((svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0)).asDiagonal() * svd.matrixU().adjoint();
}

template<class ScalarType>
MatrixTpl<ScalarType>& MatrixTpl<ScalarType>::operator=(const MatrixTpl<ScalarType>& obj) {
	// resize takes care of all the required checks
	resize(obj.rows(), obj.cols());
	BaseClass::operator=(obj);
	return *this;
}

template<class ScalarType>
MatrixTpl<ScalarType>& MatrixTpl<ScalarType>::operator=(MatrixTpl<ScalarType>&& obj) {
	if(isDataShared()) {
		// if the data we at least try to make a copy in the normal way
		resize(obj.rows(), obj.cols());
		BaseClass::operator=(obj);
	} else {
		// if possible, we just take over the existing data
		referenceData(obj._data, obj._dataOwned, obj.rows(), obj.cols(), obj.rowStride(), obj.colStride());
	}

	return *this;
}

template<class ScalarType>
MatrixTpl<ScalarType>& MatrixTpl<ScalarType>::operator=(
	std::initializer_list<std::initializer_list<ScalarType> > list
) {
	// if the initializer list is empty...
	if(!list.size()) {
		_data.reset();
		resize(0, 0);
		return *this;
	}

	// otherwise proceed normally
	resize(list.size(), list.begin()->size());

	AlgebraIndex i = 0;
	for(auto& line: list) {
		SYS_ASSERT_MSG(numeric_cast<decltype(line.size())>(this->cols()) == line.size(),
			"Every line of the matrix must have the same number of columns.")

		AlgebraIndex j = 0;
		for(auto& item: line) {
			this->operator()(i, j++) = item;
		}

		i++;
	}

	return *this;
}

template<class ScalarType>
MatrixTpl<ScalarType>::MatrixTpl(): BaseClass(nullptr, 0, 0, makeStride(0, 0)) {}

template<class ScalarType>
MatrixTpl<ScalarType>::MatrixTpl(MatrixTpl<ScalarType>&& obj):
	BaseClass(obj._data.get(), obj.rows(), obj.cols(), obj.getStride()
) {
	_data = std::move(obj._data);
	_dataOwned = obj._dataOwned;
}

template<class ScalarType>
MatrixTpl<ScalarType>::MatrixTpl(const MatrixTpl<ScalarType>& obj):
BaseClass(nullptr, 0, 0, makeStride(0, 0)) {
	resize(obj.rows(), obj.cols());
	BaseClass::operator=(obj);
}

template<class ScalarType>
MatrixTpl<ScalarType>::MatrixTpl(MatrixTpl<ScalarType>& obj):
	BaseClass(nullptr, 0, 0, makeStride(0, 0))
{
	resize(obj.rows(), obj.cols());
	BaseClass::operator=(obj);
}

template<class ScalarType>
MatrixTpl<ScalarType>::MatrixTpl(AlgebraIndex rows, AlgebraIndex cols, bool isRowMajor):
BaseClass(nullptr, 0, 0, makeStride(0, 0)) {
	resize(rows, cols, isRowMajor);
}

template<class ScalarType>
MatrixTpl<ScalarType>::MatrixTpl(
	AlgebraIndex rows, AlgebraIndex cols,
	AlgebraIndex rowStride, AlgebraIndex colStride
):
	BaseClass(nullptr, 0, 0, makeStride(0, 0))
{
	resize(rows, cols, rowStride, colStride);
}

template<class ScalarType>
MatrixTpl<ScalarType>::MatrixTpl(std::initializer_list<std::initializer_list<ScalarType> > list):
	BaseClass(nullptr, 0, 0, makeStride(0, 0))
{
	operator=(list);
}

template<class ScalarType>
MatrixTpl<ScalarType>::MatrixTpl(
	const shared_ptr<ScalarType>& data, bool dataOwned,
	AlgebraIndex rows, AlgebraIndex cols,
	StrideType stride
):
	BaseClass(nullptr, 0, 0, makeStride(0, 0))
{
	auto row_col = strideToRowCol(stride);
	referenceData(data, dataOwned, rows, cols, row_col.first, row_col.second);
}

// Matrix iterators

template<typename C>
constexpr auto begin(C& c, AlgebraIndex outer) -> decltype(c.begin(outer)) {
	return c.begin(outer);
}

template<typename C>
constexpr auto end(C& c, AlgebraIndex outer) -> decltype(c.end(outer)) {
	return c.end(outer);
}

template<typename C>
constexpr auto begin(const C& c, AlgebraIndex outer) -> decltype(c.begin(outer)) {
	return c.begin(outer);
}

template<typename C>
constexpr auto end(const C& c, AlgebraIndex outer) -> decltype(c.end(outer)) {
	return c.end(outer);
}

template<class C>
constexpr auto cbegin(const C& c, AlgebraIndex outer) -> decltype(begin(c, outer)) {
	return begin(c, outer);
}

template<class C>
constexpr auto cend(const C& c, AlgebraIndex outer) -> decltype(end(c, outer)) {
	return end(c, outer);
}

template<typename C>
constexpr auto rbegin(C& c, AlgebraIndex outer) -> decltype(c.rbegin(outer)) {
	return c.rbegin(outer);
}

template<typename C>
constexpr auto rend(C& c, AlgebraIndex outer) -> decltype(c.rend(outer)) {
	return c.rend(outer);
}

template<typename C>
constexpr auto rbegin(const C& c, AlgebraIndex outer) -> decltype(c.rbegin(outer)) {
	return c.rbegin(outer);
}

template<typename C>
constexpr auto rend(const C& c, AlgebraIndex outer) -> decltype(c.rend(outer)) {
	return c.rend(outer);
}

template<class C>
constexpr auto crbegin(const C& c, AlgebraIndex outer) -> decltype(rbegin(c, outer)) {
	return rbegin(c, outer);
}

template<class C>
constexpr auto crend(const C& c, AlgebraIndex outer) -> decltype(rend(c, outer)) {
	return rend(c, outer);
}

//------------------------------------------------------------------------------
//								RowVector
//------------------------------------------------------------------------------

template<class ScalarType>
class RowVectorTpl: public Eigen::Map<
	typename std::conditional<
		std::is_const<ScalarType>::value,
		const Eigen::Matrix<typename std::remove_const<ScalarType>::type, 1, Eigen::Dynamic>,
		Eigen::Matrix<ScalarType, 1, Eigen::Dynamic>
	>::type,
	Eigen::Aligned, Eigen::InnerStride<Eigen::Dynamic>
> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	typedef Eigen::Map<
		typename std::conditional<
			std::is_const<ScalarType>::value,
			const Eigen::Matrix<typename std::remove_const<ScalarType>::type, 1, Eigen::Dynamic>,
			Eigen::Matrix<ScalarType, 1, Eigen::Dynamic>
		>::type,
		Eigen::Aligned, Eigen::InnerStride<Eigen::Dynamic>
	> BaseClass;

	typedef Eigen::InnerStride<Eigen::Dynamic> StrideType;

public:
	typedef ScalarType value_type;
	typedef ScalarType& reference;

	template<class ScalarTypeB>
	friend class RowVectorTpl;

private:
	//! Holds the data.
	//! \note You need to provide a std::default_delete<value_type[]>() deleter
	//! at construction. Ideally use allocateData() for allocation.
	shared_ptr<value_type> _data = {};

	//! True if the data is not owned by this object.
	bool _dataOwned = true;

protected:
	//! Reinitializes the underlying Eigen::Map using current
	//! _data and the specified size and stride.
	void initMap(AlgebraIndex size, StrideType stride);

public:
	//! Returns a StrideType object for the specified stride.
	static StrideType makeStride(AlgebraIndex stride);

	//! Allocates size value_type items and returns a shared_ptr to it with
	//! the appropriate deleter.
	static shared_ptr<value_type> allocateData(AlgebraIndex size);

	AlgebraIndex stride() const;
	StrideType getStride() const;

	//! Returns a raw pointer to the data. \see shareData, releaseData.
	value_type* data() {return _data.get();}

	//! Returns a raw pointer to the data. \see shareData, releaseData.
	const value_type* data() const {return _data.get();}

	//! Returns a shared_ptr to the contained data. While the data is shared,
	//! resizes and other operations that would invalidate the data or
	//! dissociate it from the vector are disallowed and yield exceptions.
	//! \see data, releaseData
	const shared_ptr<value_type>& shareData() const {
		return _data;
	}

	//! Resets the vector to its initial state with 0 dimensions and no data.
	void resetData();

	//! Resets the vector to to its initial state with 0 dimensions and no data
	//! and then resizes it to the specified size.
	void resetData(AlgebraIndex size);

	//! Sets the contents of the Matrix to the specified data.
	//! @param dataOwned Specifies whether the data is owned by the RowVector.
	//! If so, the RowVector is allowed to make resizes and similar operations
	//! provided that the shared_ptr is unique.
	//!
	//! \note A shared_ptr that does not delete data upon destruction can be
	//! constructed using shared_ptr<value_type>(data, null_deleter);
	void referenceData(
		const shared_ptr<value_type>& data, bool dataOwned,
		AlgebraIndex size, AlgebraIndex stride = 1
	);

	//! Sets up this RowVector to share the data of the other specified RowVector.
	//! This has the same effect as calling referenceData(obj.shareData(), obj.size(),
	//! obj.stride()).
	//! @tparam The ScalarTypeB is to allow switching between ScalarType
	//! and const ScalarType.
	template<class ScalarTypeB>
	void referenceData(RowVectorTpl<ScalarTypeB>& obj) {
		referenceData(
			obj._data, obj._dataOwned,
			obj.size(), obj.stride()
		);
	}

	//! Sets up this RowVector to share the data of the other specified RowVector.
	//! This has the same effect as calling referenceData(obj.shareData(), obj.size(),
	//! obj.stride()).
	//! @tparam The ScalarTypeB is to allow switching between ScalarType
	//! and const ScalarType.
	template<class ScalarTypeB>
	void referenceData(RowVectorTpl<ScalarType>&& obj) {
		referenceData(
			obj._data, obj._dataOwned,
			obj.size(), obj.stride()
		);
	}

	//! Sets up this RowVector to reference the vector-like obj object.
	//! \pre The vector-like object must be 1D at runtime.
	//! \warning It is the responsibility of the programmer to guarantee that
	//! the data interfaced by the object is not destroyed before the RowVector.
	template<class VectorType>
	void referenceData(VectorType&& obj);

	//! Returns the state of the dataOwned flag (set upon construction or when
	//! referencing data). If the data is owned by the RowVector, it is allowed
	//! to make resizes and similar operations provided that the shared_ptr
	//! is unique.
	//! \sa isDataShared
	bool dataOwned() const {
		return _dataOwned;
	}

	//! Returns whether the contained data is shared with other objects or
	//! unshared and owned by the matrix. If so, this matrix cannot be resized
	//! (resizing yields exceptions). Other operations that would invalidate
	//! the data or dissociate it from the RowVector are similarly disallowed.
	//!
	//! This is true if either that data shared_ptr is not unique, or the
	//! dataOwned flag is not set.
	//! \sa dataOwned
	bool isDataShared() const;

	//! Resizes the vector, clearing the existing data (unless the new
	//! size is exactly equal to the old size in which case this
	//! has no effect).
	//!
	//! \note A resize is only allowed if isDataShared() == false.
	//! \sa conservativeResize
	void resize(AlgebraIndex size, AlgebraIndex stride = 1);

	//! Resizes the vector, keeping the existing data. If the new size is
	//! exactly equal to the old size, the method has no effect. If new
	//! elements have to be added, they are left uninitialized.
	//!
	//! \note A resize is only allowed if isDataShared() == false.
	//! \sa resize
	void conservativeResize(AlgebraIndex size, AlgebraIndex stride = 1);

public:
	template<class XprType>
	RowVectorTpl ref(XprType xpr) {
		RowVectorTpl vec;
		vec.referenceData(xpr);
		return vec;
	}

	void saveData(std::ostream& stream) const;
	void loadData(std::istream& stream);

public:
	using BaseClass::operator=;
	RowVectorTpl& operator=(std::initializer_list<value_type> list);
	RowVectorTpl& operator=(const RowVectorTpl& obj);
	RowVectorTpl& operator=(RowVectorTpl&& obj);

public:
	template<class Derived>
	RowVectorTpl(const Eigen::MatrixBase<Derived>& xpr): BaseClass(nullptr, 0, makeStride(1)) {
		resize(xpr.size());
		BaseClass::operator=(xpr);
	}

	template<class Derived>
	RowVectorTpl(const Eigen::SparseMatrixBase<Derived>& xpr): BaseClass(nullptr, 0, makeStride(1)) {
		resize(xpr.size());
		BaseClass::operator=(xpr);
	}

	RowVectorTpl();
	RowVectorTpl(RowVectorTpl&& obj);
	RowVectorTpl(RowVectorTpl& obj);
	RowVectorTpl(const RowVectorTpl& obj);
	RowVectorTpl(AlgebraIndex size, AlgebraIndex stride = 1);
	RowVectorTpl(std::initializer_list<value_type> list);

	//! Constructs a Matrix from existing data using referenceData.
	RowVectorTpl(const shared_ptr<value_type>& data, bool dataOwned, AlgebraIndex size, AlgebraIndex stride = 1);

	~RowVectorTpl() {
		SYS_EXPORT_INSTANCE()
	}
};

template<class ScalarType>
template<class VectorType>
void RowVectorTpl<ScalarType>::referenceData(VectorType&& obj) {
	SYS_ASSERT(obj.rows() == 1 or obj.cols() == 1);
	AlgebraIndex stride = (std::remove_reference<VectorType>::type::IsRowMajor and (obj.cols() == 1)) ?
		obj.outerStride() : obj.innerStride();
	referenceData(shared_ptr<value_type>(obj.data(), null_deleter()), false,
		obj.size(), stride);
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::saveData(std::ostream& stream) const {
	fmt::print(stream, "{} ", this->size());
	AlgebraIndex size = this->size();
	auto* data = this->data();

	for(AlgebraIndex i = 0; i < size; i++) {
		fmt::print(stream, "{} ", data[i]);
	}
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::loadData(std::istream& stream) {
	AlgebraIndex size;
	int res = xscanf(stream, "%d ", size);
	if(res != 1) throw TracedError_InvalidFormat("Cannot load data: invalid matrix format.");

	this->resize(size);
	ScalarType* data = this->data();

	res = 0;
	for(AlgebraIndex i = 0; i < size; i++) {
		res += xscanf(stream, "%g ", data[i]);
	}

	if(res != size) throw TracedError_InvalidFormat("Cannot load data: invalid matrix format.");
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::serialize(IArchive&, const unsigned int) {
//	ar & boost::serialization::make_array(data(), size());
	throw TracedError_NotImplemented("Serialization for matrices not implemented yet.");
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::serialize(OArchive&, const unsigned int) {
//	ar & boost::serialization::make_array(data(), size());
	throw TracedError_NotImplemented("Serialization for matrices not implemented yet.");
}

template<class ScalarType>
shared_ptr<ScalarType> RowVectorTpl<ScalarType>::allocateData(AlgebraIndex size) {
	Eigen::aligned_allocator<ScalarType> allocator;

	return shared_ptr<ScalarType>(allocator.allocate(size), [](ScalarType* ptr){
		Eigen::aligned_allocator<ScalarType> alloc;
		alloc.deallocate(ptr, 0);
	});
}

template<class ScalarType>
AlgebraIndex RowVectorTpl<ScalarType>::stride() const {
	return this->innerStride();
}

template<class ScalarType>
typename RowVectorTpl<ScalarType>::StrideType RowVectorTpl<ScalarType>::getStride() const {
	return StrideType(this->innerStride());
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::initMap(AlgebraIndex size, StrideType stride) {
	new(static_cast<BaseClass*>(this)) BaseClass(_data.get(), size, stride);
}

template<class ScalarType>
typename RowVectorTpl<ScalarType>::StrideType
RowVectorTpl<ScalarType>::makeStride(AlgebraIndex stride) {
	return StrideType(stride);
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::resetData() {
	_data = allocateData(0);
	_dataOwned = true;
	initMap(0, makeStride(0));
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::resetData(AlgebraIndex size) {
	_data = allocateData(size);
	_dataOwned = true;
	initMap(size, makeStride(1));
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::referenceData(
	const shared_ptr<value_type>& data, bool dataOwned,
	AlgebraIndex size, AlgebraIndex stride
) {
	_data = data;
	_dataOwned = dataOwned;
	initMap(size, makeStride(stride));
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::resize(AlgebraIndex size, AlgebraIndex stride) {
	if(this->size() == size) {
		return;
	} else if(isDataShared()) {
		throw TracedError_NotAvailable("A vector cannot be resized while isDataShared() == true.");
	}

	_data = allocateData(size);
	_dataOwned = true;
	initMap(size, makeStride(stride));
}

template<class ScalarType>
void RowVectorTpl<ScalarType>::conservativeResize(AlgebraIndex size, AlgebraIndex stride) {
	if(this->size() == size) {
		return;
	} else if(isDataShared()) {
		throw TracedError_NotAvailable("A vector cannot be resized while isDataShared() == true.");
	}

	RowVectorTpl<ScalarType> tmp(std::move(*this));

	_data = allocateData(size);
	_dataOwned = true;
	initMap(size, makeStride(stride));

	AlgebraIndex copySize = std::min(size, tmp.size());
	this->head(copySize) = tmp.head(copySize);
}

template<class ScalarType>
bool RowVectorTpl<ScalarType>::isDataShared() const {
	return !_dataOwned or (_data and !_data.unique());
}

template<class ScalarType>
RowVectorTpl<ScalarType>& RowVectorTpl<ScalarType>::operator=(const RowVectorTpl& obj) {
	// resize takes care of all the required checks
	resize(obj.size());
	BaseClass::operator=(obj);
	return *this;
}

template<class ScalarType>
RowVectorTpl<ScalarType>& RowVectorTpl<ScalarType>::operator=(RowVectorTpl&& obj) {
	if(isDataShared()) {
		// if the data we at least try to make a copy in the normal way
		resize(obj.size());
		BaseClass::operator=(obj);
	} else {
		// if possible, we just take over the existing data
		referenceData(obj._data, obj._dataOwned, obj.size(), obj.stride());
	}

	return *this;
}

template<class ScalarType>
RowVectorTpl<ScalarType>& RowVectorTpl<ScalarType>::operator=(std::initializer_list<value_type> list) {
	// if the initializer list is empty...
	if(!list.size()) {
		_data.reset();
		resize(0);
		return *this;
	}

	// otherwise proceed normally
	resize(list.size());

	AlgebraIndex i = 0;
	for(auto& item: list) {
		this->operator[](i++) = item;
	}

	return *this;
}

template<class ScalarType>
RowVectorTpl<ScalarType>::RowVectorTpl(): BaseClass(nullptr, 0, makeStride(1)) {}

template<class ScalarType>
RowVectorTpl<ScalarType>::RowVectorTpl(RowVectorTpl<ScalarType>&& obj):
	BaseClass(obj._data.get(), obj.size(), obj.getStride())
{
	_data = std::move(obj._data);
	_dataOwned = obj._dataOwned;
}

template<class ScalarType>
RowVectorTpl<ScalarType>::RowVectorTpl(const RowVectorTpl& obj):
	BaseClass(nullptr, 0, makeStride(1))
{
	resize(obj.size());
	BaseClass::operator=(obj);
}

template<class ScalarType>
RowVectorTpl<ScalarType>::RowVectorTpl(RowVectorTpl<ScalarType>& obj):
	BaseClass(nullptr, 0, makeStride(1))
{
	resize(obj.size());
	BaseClass::operator=(obj);
}

template<class ScalarType>
RowVectorTpl<ScalarType>::RowVectorTpl(AlgebraIndex size, AlgebraIndex stride):
	BaseClass(nullptr, 0, makeStride(1))
{
	resize(size, stride);
}

template<class ScalarType>
RowVectorTpl<ScalarType>::RowVectorTpl(std::initializer_list<value_type> list):
	BaseClass(nullptr, 0, makeStride(1))
{
	operator=(list);
}

template<class ScalarType>
RowVectorTpl<ScalarType>::RowVectorTpl(
	const shared_ptr<value_type>& data, bool dataOwned,
	AlgebraIndex size, AlgebraIndex stride
):
	BaseClass(nullptr, 0, makeStride(1))
{
	referenceData(data, dataOwned, size, stride);
}

//------------------------------------------------------------------------------
//							SparseMatrix
//------------------------------------------------------------------------------

class SparseMatrix: public BaseSparseMatrix {
public:
	using BaseClass = BaseSparseMatrix;

public:
	typedef RealType value_type;
	typedef RealType& reference;

	typedef EigenSparseIterator<SparseMatrix> iterator;
	typedef EigenSparseIterator<SparseMatrix, const RealType> const_iterator;
	typedef eigen_reverse_iterator<EigenSparseIterator<SparseMatrix> > reverse_iterator;
	typedef eigen_reverse_iterator<EigenSparseIterator<SparseMatrix, const RealType> > const_reverse_iterator;

	iterator begin(AlgebraIndex outer) {
		return {*this, outer, outerIndexPtr()[outer]};
	}

	iterator end(AlgebraIndex outer) {
		AlgebraIndex iend;

		if(isCompressed()) iend = outerIndexPtr()[outer+1];
		else iend = outerIndexPtr()[outer] + innerNonZeroPtr()[outer];

		return {*this, outer, iend};
	}

	const_iterator begin(AlgebraIndex outer) const {
		return {const_cast<SparseMatrix&>(*this), outer, outerIndexPtr()[outer]};
	}

	const_iterator end(AlgebraIndex outer) const {
		AlgebraIndex iend;

		if(isCompressed()) iend = outerIndexPtr()[outer+1];
		else iend = outerIndexPtr()[outer] + innerNonZeroPtr()[outer];

		return {const_cast<SparseMatrix&>(*this), outer, iend};
	}

	reverse_iterator rbegin(AlgebraIndex outer) {
		return reverse_iterator{end(outer)};
	}

	reverse_iterator rend(AlgebraIndex outer) {
		return reverse_iterator{begin(outer)};
	}

	const_reverse_iterator rbegin(AlgebraIndex outer) const {
		return const_reverse_iterator{end(outer)};
	}

	const_reverse_iterator rend(AlgebraIndex outer) const {
		return const_reverse_iterator{begin(outer)};
	}

private:
	class AuxCopyShape {
	private:
		RealType _initValue = 0;

	public:
		RealType operator()(RealType) const {
			return _initValue;
		}

		AuxCopyShape(RealType initValue):
			_initValue(initValue) {}
	};

public:
	//! The shape_type returned by copyShape.
	typedef decltype(((BaseClass*)(0))->unaryExpr(AuxCopyShape(0_r))) shape_type;

	//! Returns an expression with the same shape as the SparseMatrix, where
	//! all non-zeros are initialized to initValue (this is zero by default
	//! in which case the sparse elements are still present).
	shape_type copyShape(RealType initValue = 0) const {
		return unaryExpr(AuxCopyShape(initValue));
	}

public:
	SparseMatrix& operator=(const SparseMatrix& obj) = default;
	using BaseClass::operator=;

public:
	using BaseClass::BaseClass;

	/**
	 * Constructs a sparse matrix from triplets.
	 * @param rows Number of rows.
	 * @param cols Number of columns.
	 * @param first Begin iterator of the triplet container.
	 * @param last End iterator of the triplet container.
	 */
	template<class InputIterator>
	SparseMatrix(AlgebraIndex rows_, AlgebraIndex cols_, const InputIterator& first, const InputIterator& last):
		BaseClass(rows_, cols_) {setFromTriplets(first, last);}

	/**
	 * Constructs a sparse matrix from triplets.
	 * @param rows Number of rows.
	 * @param cols Number of columns.
	 * @param triplets A container of triplets: the type must support iteration.
	 */
	template<class TripletContainer>
	SparseMatrix(AlgebraIndex rows_, AlgebraIndex cols_, const TripletContainer& triplets):
		BaseClass(rows_, cols_) {setFromTriplets(triplets.begin(), triplets.end());}

	SparseMatrix() = default;
	SparseMatrix(const SparseMatrix&) = default;
};

SYS_WNONVIRT_DTOR_ON

//------------------------------------------------------------------------------
//								Other
//------------------------------------------------------------------------------

class TripletAlwaysTrue {
public:
	bool operator()(const Triplet&) {
		return true;
	}
};

class TripletPassThrough {
public:
	Triplet& operator()(Triplet& t) {
		return t;
	}
};

/**
 * Extracts the data from a sparse matrix in form of triplets. Applies Functor
 * func() to every extracted triplet.
 * @param pred A triplet is filtered out if pred(triplet) is false. Defaults
 * to a predicate which is always true.
 */
template<class Functor = TripletPassThrough, class Predicate = TripletAlwaysTrue>
std::vector<Triplet> extractTriplets(const SparseMatrix& mat, Functor func = Functor(), Predicate pred = Predicate()) {
	std::vector<Triplet> triplets;
	triplets.reserve(mat.nonZeros());

	for(int i = 0; i < mat.outerSize(); ++i) {
	  for(auto iter = begin(mat, i); iter != end(mat, i); iter++) {
		  Triplet t(iter.row(), iter.col(), iter.value());
		  if(pred(t)) triplets.push_back(func(t));
	  }
	}

	return triplets;
}

namespace detail {

	template<class MatrixType, bool IsRowMajor = MatrixType::IsRowMajor>
	struct RowColComputer {
		typedef typename MatrixType::AlgebraIndex AlgebraIndex;

		static AlgebraIndex index(const MatrixType& mat, AlgebraIndex row, AlgebraIndex col) {
			return col*mat.rows() + row;
		}

		static AlgebraIndex row(const MatrixType& mat, AlgebraIndex index) {
			return index % mat.rows();
		}

		static AlgebraIndex col(const MatrixType& mat, AlgebraIndex index) {
			return index / mat.rows();
		}
	};

	template<class MatrixType>
	struct RowColComputer<MatrixType, true> {
		typedef typename MatrixType::AlgebraIndex AlgebraIndex;

		static AlgebraIndex index(const MatrixType& mat, AlgebraIndex row, AlgebraIndex col) {
			return row*mat.cols() + col;
		}

		static AlgebraIndex row(const MatrixType& mat, AlgebraIndex index) {
			return index / mat.cols();
		}

		static AlgebraIndex col(const MatrixType& mat, AlgebraIndex index) {
			return index % mat.cols();
		}
	};

} //namespace detail

/**
 * Given a matrix, converts a 1-dimensional index into a row number.
 */
template<class MatrixType>
typename MatrixType::AlgebraIndex to_row(const MatrixType& mat, typename MatrixType::AlgebraIndex index) {
	return detail::RowColComputer<MatrixType>::row(mat, index);
}

/**
 * Given a matrix, converts a 1-dimensional index into a column number.
 */
template<class MatrixType>
typename MatrixType::AlgebraIndex to_col(const MatrixType& mat, typename MatrixType::AlgebraIndex index) {
	return detail::RowColComputer<MatrixType>::col(mat, index);
}

/**
 * Given a matrix, converts a row number and column number into
 * a 1-dimensional index.
 */
template<class MatrixType>
typename MatrixType::AlgebraIndex to_index(const MatrixType& mat, typename MatrixType::AlgebraIndex row, typename MatrixType::AlgebraIndex col) {
	return detail::RowColComputer<MatrixType>::index(mat, row, col);
}

} //namespace systematic

namespace boost {
namespace serialization {

void save_construct_data(
	systematic::OArchive& ar, const systematic::Triplet* obj, const unsigned int version);
void load_construct_data(
	systematic::IArchive& ar, systematic::Triplet* obj, const unsigned int version);

void serialize(systematic::IArchive& ar, systematic::Triplet& obj, const unsigned int version);
void serialize(systematic::OArchive& ar, systematic::Triplet& obj, const unsigned int version);

void serialize(systematic::IArchive& ar, systematic::SparseMatrix& obj, const unsigned int version);
void serialize(systematic::OArchive& ar, systematic::SparseMatrix& obj, const unsigned int version);

} //namespace serialization
} //namespace boost

namespace Eigen {
namespace internal {

SYS_WNONVIRT_DTOR_OFF

template<class ScalarType>
class traits<systematic::MatrixTpl<ScalarType> >: public traits<typename systematic::MatrixTpl<ScalarType>::BaseClass> {};

template<class ScalarType>
class traits<systematic::RowVectorTpl<ScalarType> >: public traits<typename systematic::RowVectorTpl<ScalarType>::BaseClass> {};

template<>
class traits<systematic::SparseMatrix>: public traits<systematic::SparseMatrix::BaseClass> {};

#ifdef EIGEN_3_3

template<class ScalarType>
class evaluator<systematic::MatrixTpl<ScalarType> >: public evaluator<typename systematic::MatrixTpl<ScalarType> ::BaseClass> {};

template<class ScalarType>
class evaluator<systematic::RowVectorTpl<ScalarType> >: public evaluator<typename systematic::RowVectorTpl<ScalarType>::BaseClass> {};

template<>
class evaluator<systematic::SparseMatrix>: public evaluator<systematic::SparseMatrix::BaseClass> {};

#endif

SYS_WNONVIRT_DTOR_ON

} //namespace internal
} //namespace Eigen

SYS_EXPORT_TEMPLATE(systematic::MatrixTpl, 1)
SYS_EXPORT_TEMPLATE(systematic::RowVectorTpl, 1)
SYS_EXPORT_CLASS(systematic::SparseMatrix)

//------------------------------------------------------------------------------
//								The Ref template
//------------------------------------------------------------------------------

namespace systematic {

/**
 * @class Ref
 * A class used to express references to various types of Matrices.
 * The reference can be made either to a const or to a non-const version of
 * Matrix/RowVector. Internally it behaves as a Matrix/RowVector referring
 * to the original data.
 *
 * \warning In the case of blocks, the referenced data must not be deleted
 * before the Ref is destroyed. In other cases, the mere act of copying the
 * underlying shared pointer to the interfaced data should take care of
 * prolonging its life where necessary.
 *
 * \note Ref can also be used to refer to blocks/segments from
 * a Matrix/RowVector.
 */
template<class MatrixType>
class Ref {};

//! Ref specialization for a non-const Matrix.
template<>
class Ref<Matrix>: public Matrix {
public:
	using Matrix::operator=;

	template<class MatrixTypeB>
	Ref& operator=(Ref<MatrixTypeB>&) = delete;

	template<class MatrixTypeB>
	Ref& operator=(const Ref<MatrixTypeB>&) = delete;

	Ref& operator=(Ref<Matrix>&& mat) {
		referenceData(mat);
		return *this;
	}

public:
	template<class MatrixType>
	Ref(MatrixType&& obj, typename std::enable_if<!std::remove_reference<MatrixType>::type::IsVectorAtCompileTime, int*>::type = nullptr
	):
	Matrix(std::move(Matrix::ref(obj))) {}

	//! We allow construction from vector-like blocks, but it must be explicit
	//! so that disambiguation is possible between vector and matrix versions
	//! of the same function.
	template<class MatrixType>
	explicit Ref(MatrixType&& obj, typename std::enable_if<std::remove_reference<MatrixType>::type::IsVectorAtCompileTime, int*>::type = nullptr
	):
	Matrix(std::move(Matrix::ref(obj))) {}

	Ref(
		const shared_ptr<value_type>& data,  bool dataOwned,
		AlgebraIndex rows, AlgebraIndex cols, StrideType stride
	): Matrix(data, dataOwned, rows, cols, stride) {}

	Ref(
		const shared_ptr<value_type>& data, bool dataOwned,
		AlgebraIndex rows, AlgebraIndex cols,
		AlgebraIndex rowStride, AlgebraIndex colStride
	): Matrix(data, dataOwned, rows, cols, rowStride, colStride) {}

	Ref(const Ref<Matrix>& obj):
		Matrix(obj.shareData(), false, obj.rows(), obj.cols(),
			obj.rowStride(), obj.colStride()) {}

	Ref() = default;
};

//! Ref specialization for a const Matrix.
template<>
class Ref<const Matrix>: public MatrixTpl<const RealType> {
public:
	void resize(AlgebraIndex, AlgebraIndex) = delete;

	Ref& operator=(Ref<Matrix>&& mat) {
		referenceData(mat);
		return *this;
	}

	Ref& operator=(Ref<const Matrix>&& mat) {
		referenceData(mat);
		return *this;
	}

public:
	template<class MatrixType>
	Ref(MatrixType&& obj, typename std::enable_if<!std::remove_reference<MatrixType>::type::IsVectorAtCompileTime, int*>::type = nullptr
	):
	MatrixTpl<const RealType>(std::move(MatrixTpl<const RealType>::ref(obj))) {}

	//! We allow construction from vector-like blocks, but it must be explicit
	//! so that disambiguation is possible between vector and matrix versions
	//! of the same function.
	template<class MatrixType>
	explicit Ref(MatrixType&& obj, typename std::enable_if<std::remove_reference<MatrixType>::type::IsVectorAtCompileTime, int*>::type = nullptr
	):
	MatrixTpl<const RealType>(std::move(MatrixTpl<const RealType>::ref(obj))) {}

	Ref(
		const shared_ptr<value_type>& data, bool dataOwned,
		AlgebraIndex rows, AlgebraIndex cols, StrideType stride
	): MatrixTpl<const RealType>(data, dataOwned, rows, cols, stride) {}

	Ref(
		const shared_ptr<value_type>& data, bool dataOwned,
		AlgebraIndex rows, AlgebraIndex cols,
		AlgebraIndex rowStride, AlgebraIndex colStride
	): MatrixTpl<const RealType>(data, dataOwned, rows, cols, rowStride, colStride) {}

	Ref(const Ref<const Matrix>& obj):
		MatrixTpl<const RealType>(obj.shareData(), false, obj.rows(), obj.cols(),
			obj.rowStride(), obj.colStride()) {}

	Ref(const Ref<Matrix>& obj):
		MatrixTpl<const RealType>(obj.shareData(), false, obj.rows(), obj.cols(),
			obj.rowStride(), obj.colStride()) {}

	Ref() = default;
};

//! Ref specialization for a non-const RowVector.
template<>
class Ref<RowVector>: public RowVector {
public:
	using RowVector::operator=;

	template<class VectorTypeB>
	Ref& operator=(Ref<VectorTypeB>&) = delete;

	template<class VectorTypeB>
	Ref& operator=(const Ref<VectorTypeB>&) = delete;

	Ref& operator=(Ref<RowVector>&& vec) {
		referenceData(vec);
		return *this;
	}

public:
	template<class VectorType>
	Ref(VectorType&& obj, typename std::enable_if<std::remove_reference<VectorType>::type::IsVectorAtCompileTime, int*>::type = nullptr
	):
	RowVector(std::move(RowVector::ref(obj))) {}

	//! We allow construction from matrix-like blocks, but it must be explicit
	//! so that disambiguation is possible between vector and matrix versions
	//! of the same function.
	template<class VectorType>
	explicit Ref(VectorType&& obj, typename std::enable_if<!std::remove_reference<VectorType>::type::IsVectorAtCompileTime, int*>::type = nullptr
	):
	RowVector(std::move(RowVector::ref(obj))) {}

	Ref(const shared_ptr<value_type>& data, bool dataOwned, AlgebraIndex size, AlgebraIndex stride = 1):
		RowVector(data, dataOwned, size, stride) {}

	Ref(const Ref<RowVector>& obj):
		RowVector(obj.shareData(), false, obj.size(), obj.stride()) {}

	Ref() = default;
};

//! Ref specialization for a const RowVector.
template<>
class Ref<const RowVector>: public RowVectorTpl<const RealType> {
public:
	void resize(AlgebraIndex) = delete;

	Ref& operator=(Ref<RowVector>&& vec) {
		referenceData(vec);
		return *this;
	}

	Ref& operator=(Ref<const RowVector>&& vec) {
		referenceData(vec);
		return *this;
	}

public:
	template<class VectorType>
	Ref(VectorType&& obj, typename std::enable_if<std::remove_reference<VectorType>::type::IsVectorAtCompileTime, int*>::type = nullptr
	):
	RowVectorTpl<const RealType>(std::move(RowVectorTpl<const RealType>::ref(obj))) {}

	//! We allow construction from matrix-like blocks, but it must be explicit
	//! so that disambiguation is possible between vector and matrix versions
	//! of the same function.
	template<class VectorType>
	explicit Ref(VectorType&& obj, typename std::enable_if<!std::remove_reference<VectorType>::type::IsVectorAtCompileTime, int*>::type = nullptr
	):
	RowVectorTpl<const RealType>(std::move(RowVectorTpl<const RealType>::ref(obj))) {}

	Ref(const shared_ptr<value_type>& data, bool dataOwned, AlgebraIndex size, AlgebraIndex stride = 1):
		RowVectorTpl<const RealType>(data, dataOwned, size, stride) {}

	Ref(const Ref<RowVector>& obj):
		RowVectorTpl<const RealType>(obj.shareData(), false, obj.size(), obj.stride()) {}

	Ref(const Ref<const RowVector>& obj):
		RowVectorTpl<const RealType>(obj.shareData(), false, obj.size(), obj.stride()) {}

	Ref() = default;
};

} // namespace systematic

#endif //Systematic_algebra_H_
