#ifndef ROUND_H_
#define ROUND_H_

#include <cmath>
#include <Systematic/configure.h>

namespace systematic {

/**
 * Implements rounding to integer with round half up tie-braking.
 */
template<class Type>
Type Round(Type val) {
	return (val > Type(0.0)) ? std::floor(val + Type(0.5)) : std::ceil(val - Type(0.5));
}

} //namespace systematic

#endif /* ROUND_H_ */
