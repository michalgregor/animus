#include "Hypervolume.h"
#include "../system.h"

#include <algorithm>
#include <boost/iterator/counting_iterator.hpp>
#include <boost/asio/yield.hpp>

namespace systematic {

//------------------------------------------------------------------------------
//						Common Pareto routines
//------------------------------------------------------------------------------

/**
 * Returns whether pointA Pareto dominates pointB. A minimization task
 * is presumed.
 */
bool dominates(
    const std::vector<RealType>& pointA,
    const std::vector<RealType>& pointB) {
	SYS_ASSERT(pointA.size() == pointB.size());

	auto sz = pointA.size();
	bool dominates = false;

	for(decltype(sz) i = 0; i < sz; i++) {
		if(pointA[i] > pointB[i]) return false;
		if(pointA[i] < pointB[i]) dominates = true;
	}

	return dominates;
}

/**
 * Returns whether pointA weakly Pareto dominates pointB. A minimization task
 * is presumed.
 */
bool weaklyDominates(
    const std::vector<RealType>& pointA,
    const std::vector<RealType>& pointB) {
	SYS_ASSERT(pointA.size() == pointB.size());

	bool better = true;

	for(std::vector<RealType>::size_type i = 0; i < pointA.size() && better;
	    i++) {
		better = pointA[i] <= pointB[i];
	}

	return better;
}

/**
 * Changes signs of all elements so as to turn minimization task into
 * a maximization task and vice-versa.
 */
void invertPoint(std::vector<RealType>& point) {
	auto sz = point.size();
	for(decltype(sz) i = 0; i < sz; i++) {
		point[i] = -point[i];
	}
}

/**
 * Changes signs of all elements so as to turn minimization task into
 * a maximization task and vice-versa.
 */
void invertPoints(std::vector<std::vector<RealType> >& points) {
	for(auto& point : points) {
		invertPoint(point);
	}
}

/**
 * Filters points so that for every point in the resulting set there is no
 * point, which dominates it. A minimization task is presumed.
 */
std::vector<std::vector<RealType> > paretoFrontFilter(
    const std::vector<std::vector<RealType> >& points,
    const std::vector<RealType>& refPoint) {
	auto sz = points.size();
	std::vector<bool> toComp(points.size(), true);
	std::vector<std::vector<RealType> > front;

	for(decltype(sz) i = 0; i < sz; i++) {
		//test whether weakly dominates refPoint
		if(!dominates(points[i], refPoint)) {
			toComp[i] = false;
			continue;
		}

		decltype(sz) j = 0;
		for(; j < sz; j++) {
			if(toComp[j] && dominates(points[j], points[i])) break;
		}

		if(j >= sz) front.push_back(points[i]);
		else toComp[i] = false;
	}

	std::sort(front.begin(), front.end());
	auto newend = std::unique(front.begin(), front.end());
	front.erase(newend, front.end());

	return front;
}

//------------------------------------------------------------------------------
//									WFG
//------------------------------------------------------------------------------

RealType exclhv(
    const std::vector<std::vector<RealType> >& points,
    const std::vector<RealType>& refPoint,
    unsigned int k
);

RealType inclhv(
    const std::vector<RealType>& point,
    const std::vector<RealType>& refPoint
);

std::vector<std::vector<RealType> > limitset(
    const std::vector<std::vector<RealType> >& points,
    unsigned int k
);

/**
 * Computes hypervolume for the specified set of points (front), and
 * the reference point (refPoint). A minimization task is presumed. (The points
 * can be inverted using invertPoints).
 *
 * This implements a version of the WFG method described in
 * While, L., Bradstreet, L., Barone, L. A Fast Way of Calculating Exact
 * Hypervolumes. 2011.
 *
 * @note The slicing optimization is not implemented yet.
 *
 * @pre For every point of front there must be no point in front that weakly
 * dominates it. This can be achieved using the paretoFrontFilter function.
 **/
RealType wfg(
    std::vector<std::vector<RealType> >& points,
    const std::vector<RealType>& refPoint
) {
	if(!points.size()) return 0;

	// to optimize execution, we sort the points first in a single dimension
	std::sort(points.begin(), points.end(),
		[](const std::vector<RealType>& v1, const std::vector<RealType>& v2) {
			return v1.front() > v2.front();
	});

	RealType sum = 0;

	if(refPoint.size() == 2) {
		sum += std::abs((points[0][0] - refPoint[0]) * (points[0][1] - refPoint[1]));
		for(unsigned int k = 1; k < points.size(); k++) {
			sum += std::abs((points[k][1] - refPoint[1]) * (points[k][0] - points[k-1][0]));
		}
	} else {
		for(unsigned int k = 0; k < points.size(); k++) {
			sum += std::abs(points[k].front() - refPoint.front()) * exclhv(points, refPoint, k);
		}
	}

	return sum;
}

RealType exclhv(
    const std::vector<std::vector<RealType> >& points,
    const std::vector<RealType>& refPoint,
    unsigned int k
) {
	std::vector<std::vector<RealType> > limpoints = paretoFrontFilter(
	    limitset(points, k), refPoint);

	// we need to drop the first dimension of the reference point here due to slicing
	return inclhv(points[k], refPoint) - wfg(limpoints, {refPoint.begin() + 1, refPoint.end()});
}

RealType inclhv(
    const std::vector<RealType>& point,
    const std::vector<RealType>& refPoint
) {
	SYS_ASSERT(point.size() == refPoint.size());
	RealType product = 1;

	// j starts from 1 due to objective slicing
	for(std::vector<RealType>::size_type j = 1; j < point.size(); j++) {
		product *= std::abs(point[j] - refPoint[j]);
	}

	return product;
}

std::vector<std::vector<RealType> > limitset(
    const std::vector<std::vector<RealType> >& points,
    unsigned int k
) {
	std::vector<std::vector<RealType> > limpoints;

	if(points.size()) {
		std::vector<RealType>::size_type num_dims = points[0].size();
		// we make the vector one dimension shorter due to slicing
		limpoints.assign(points.size(), std::vector<RealType>(num_dims - 1, 0));

		for(unsigned int i = 1; i < points.size() - k; i++) {
			SYS_ASSERT(points[i].size() == num_dims);

			// j starts from 1 due to slicing
			for(std::vector<RealType>::size_type j = 1; j < num_dims; j++) {
				limpoints[i][j - 1] = std::max(points[k][j], points[k + i][j]);
			}
		}
	}

	return limpoints;
}

//------------------------------------------------------------------------------
//									HSO
//------------------------------------------------------------------------------

class HSO {
private:
	class Comp {
	private:
		const std::vector<std::vector<RealType> >& _points;
		unsigned int _k;

	public:
		bool operator()(
			unsigned int p1ind,
			unsigned int p2ind
		) const {
			return _points[p1ind].at(_k) < _points[p2ind].at(_k);
		}

	public:
		Comp(const std::vector<std::vector<RealType> >& points, unsigned int k):
			_points(points), _k(k) {}
	};

	typedef std::set<unsigned int, Comp> PointSet;
	typedef std::pair<RealType, PointSet> SliceType;

	class Slice: public boost::asio::coroutine {
	private:
		const std::vector<std::vector<RealType> >& _points;
		const PointSet& _pl;
		const std::vector<RealType>& _refPoint;
		unsigned int _k;

		PointSet::iterator _p;
		PointSet::iterator _phead;
		PointSet::iterator _pp;

		PointSet _ql;

	public:
		SliceType operator()();

		Slice(
			const std::vector<std::vector<RealType> >& points,
			const PointSet& pl,
			const std::vector<RealType>& refPoint,
			unsigned int k
		):
			_points(points), _pl(pl), _refPoint(refPoint), _k(k),
			_p(), _phead(), _pp(), _ql(Comp(_points, _k+1)) {}
	};

private:
	static bool dominates(
	    const std::vector<RealType>& pointA,
	    const std::vector<RealType>& pointB,
	    unsigned int ibegin,
		unsigned int iend
	);

	static void insert(
		const std::vector<std::vector<RealType> >& points,
		unsigned int p,
	    unsigned int k,
	    PointSet& pl
	);

public:
	static RealType hso(
	    const std::vector<std::vector<RealType> >& points,
	    const std::vector<RealType>& refPoint
	);
};

/**
 * Returns whether pointA Pareto dominates pointB in dimensions [ibegin, iend).
 * A minimization task is presumed.
 */
bool HSO::dominates(
    const std::vector<RealType>& pointA,
    const std::vector<RealType>& pointB,
    unsigned int ibegin,
	unsigned int iend
) {
	SYS_ASSERT(pointA.size() == pointB.size());
	SYS_ASSERT(ibegin <= iend);
	SYS_ASSERT(iend <= pointA.size());

	auto sz = pointA.size();
	bool dominate = false;

	for(decltype(sz) i = ibegin; i < iend; i++) {
		if(pointA[i] > pointB[i]) return false;
		if(pointA[i] < pointB[i]) dominate = true;
	}

	return dominate;
}

/**
* Computes hypervolume for the specified set of points (front), and
* the reference point (refPoint). A minimization task is presumed. (The points
* can be inverted using invertPoints).
*
* @pre For every point of front there must be no point in front that weakly
* dominates it. This can be achieved using the paretoFrontFilter function.
**/
RealType HSO::hso(
	const std::vector<std::vector<RealType> >& points,
	const std::vector<RealType>& refPoint
) {
	PointSet pl(boost::counting_iterator<unsigned int>(0),
		boost::counting_iterator<unsigned int>(numeric_cast<unsigned int>(points.size())), Comp(points, 0));

	std::vector<SliceType> s;
	s.push_back({1.0, std::move(pl)});

	for(unsigned int k = 0; k < refPoint.size() - 1; k++) {
		std::vector<SliceType> ss;

		for(auto& sitem: s) {
			auto x = sitem.first;
			auto& ql = sitem.second;

			Slice slice(points, ql, refPoint, k);

			while(!slice.is_complete()) {
				auto ssitem = slice();

				auto xx = ssitem.first;
				auto& qql = ssitem.second;

				ss.push_back({x*xx, std::move(qql)});
			}
		}

		s = std::move(ss);
	}

	RealType vol = 0;

	for(auto& item: s) {
		auto x = item.first;
		auto& ql = item.second;

		vol += x * std::abs(points[*ql.begin()].back() - refPoint.back());
	}

	return vol;
}

HSO::SliceType HSO::Slice::operator()() {
	reenter(this) {
		_p = _pl.begin();
		_phead = _pl.begin(); _phead++;

		for(; _phead != _pl.end(); _phead++) {
			insert(_points, *_p, _k+1, _ql);
			_pp = _phead;

			yield return SliceType(std::abs(_points[*_p][_k] - _points[*_pp][_k]), _ql);
			_p = _pp;
		}
	}

	insert(_points, *_p, _k+1, _ql);
	return SliceType(std::abs(_points[*_p][_k] - _refPoint[_k]), std::move(_ql));
}

void HSO::insert(
	const std::vector<std::vector<RealType> >& points,
	unsigned int p,
	unsigned int k,
	PointSet& pl
) {
	auto phead = pl.insert(p).first;

	for(; phead != pl.end(); phead++) {
		if(dominates(points[p], points[*phead], k, numeric_cast<unsigned int>(points[p].size()))) pl.erase(phead++);
	}
}

/**
 * Computes hypervolume for the specified set of points (front), and
 * the reference point (refPoint). A minimization task is presumed. (The points
 * can be inverted using invertPoints).
 *
 * Implements the HSO method as described in While, L., Hingston, P., Barone,
 * L., Huband S. A Faster Algorithm for Calculating Hypervolume. 2006.
 *
 * @note The slicing optimization is not implemented yet.
 *
 * @pre For every point of front there must be no point in front that weakly
 * dominates it. This can be achieved using the paretoFrontFilter function.
 **/
RealType hso(const std::vector<std::vector<RealType> >& points, const std::vector<RealType>& refPoint) {
	return HSO::hso(points, refPoint);
}

} //namespace systematic
