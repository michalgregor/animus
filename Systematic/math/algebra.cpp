#include "algebra.h"
#include <vector>

namespace boost {
namespace serialization {

void save_construct_data(
	systematic::OArchive& ar, const systematic::Triplet* obj, const unsigned int){

	ar << obj->row();
	ar << obj->col();
	ar << obj->value();
}

void load_construct_data(
	systematic::IArchive& ar, systematic::Triplet* obj, const unsigned int){

	systematic::AlgebraIndex row, col;
	systematic::SparseMatrix::Scalar val;

	ar >> row;
	ar >> col;
	ar >> val;

	::new(obj)systematic::Triplet(row, col, val);
}

void serialize(systematic::IArchive& ar, systematic::Triplet& obj, const unsigned int) {
	ar & obj;
}

void serialize(systematic::OArchive& ar, systematic::Triplet& obj, const unsigned int) {
	ar & obj;
}

void serialize(systematic::IArchive& ar, systematic::SparseMatrix& obj, const unsigned int) {
	std::vector<systematic::Triplet> data;
	systematic::AlgebraIndex rows, cols;

	ar & rows;
	ar & cols;
	ar & data;

	obj.resize(rows, cols);
	obj.setFromTriplets(data.begin(), data.end());
}

void serialize(systematic::OArchive& ar, systematic::SparseMatrix& obj, const unsigned int) {
	std::vector<systematic::Triplet> data;
	data.reserve(obj.nonZeros());
	for (int k = 0; k < obj.outerSize(); ++k) {
		for (typename systematic::SparseMatrix::InnerIterator it(obj, k); it; ++it) {
				data.push_back(systematic::Triplet(it.row(), it.col(), it.value()));
		}
	}

	systematic::AlgebraIndex rows = obj.rows();
	systematic::AlgebraIndex cols = obj.cols();

	ar & rows;
	ar & cols;
	ar & data;
}

} //namespace serialization
} //namespace boost
