#ifndef SYS_COMPARE_H_INCLUDED
#define SYS_COMPARE_H_INCLUDED

#include <cmath>
#include <type_traits>
#include <Systematic/configure.h>
#include "../macros.h"

namespace systematic {

namespace detail {

	const float PredefEpsilon = 1e-15f;

	/**
	* A template comparison function with a generic implementation for general
	* types and a specialized implmentation for floating point numbers. Returns
	* 1 if A compares to B as equal and 0 if not.
	**/
	template<typename TypeA, typename TypeB, typename Enable = void>
	struct __Comparator {
		template<class EpsilonType>
		inline static bool Compare(const TypeA& A, const TypeB& B, EpsilonType epsilon) {
			return std::abs(A - B) <= epsilon;
		}

		inline static bool Compare(const TypeA& A, const TypeB& B) {
			return A == B;
		}
	};

	/**
	* An implementation of __Comparator specialized for floating point numbers.
	* Returns 1 if A compares to B as equal and 0 if not. Precision may be
	* specified. If not the default value (defined by __predef_epsilon__)
	* will be used.
	*
	* The comparison
	**/
	template<typename TypeA, typename TypeB>
	struct __Comparator<TypeA, TypeB, typename std::enable_if<
		std::is_floating_point<TypeA>::value or std::is_floating_point<TypeB>::value
	>::type> {
		template<class EpsilonType>
		static bool Compare(TypeA a, TypeB b, EpsilonType epsilon) {
			int exponent;
			std::frexp(std::abs(a) > std::abs(b) ? a : b, &exponent);
			EpsilonType delta(std::ldexp(epsilon, exponent));
			EpsilonType difference(a - b);

			if (difference > delta)
			return 0; /* a > b */
			else if (difference < -delta)
			return 0;  /* a < b */
			else /* -delta <= difference <= delta */
			return 1;  /* a == b */
		}

		static bool Compare(TypeA a, TypeB b) {
			return Compare(a, b, static_cast<decltype(a + b)>(PredefEpsilon));
		}
	};
}

/**
* Returns 1 if A compares to B as equal and 0 if not.
*
* A comparison method for floating point numbers. It allows to specify
* precision. This method is only defined for floating point numbers.
*
* @param epsilon Specifies precision.
**/
template<typename TypeA, typename TypeB, typename EpsilonType>
inline bool Compare(TypeA A, TypeB B, EpsilonType epsilon) {
	return detail::__Comparator<TypeA, TypeB, EpsilonType>::Compare(A, B, epsilon);
}

/**
* Returns 1 if A compares to B as equal and 0 if not.
*
* A comparison method for floating point numbers. It allows to specify
* precision. This method is only defined for floating point numbers.
*
* A default value of epsilon is supplied. This should be systematic::detail::PredefEpsilon
* for floating point types and 0 for other types.
**/
template<typename TypeA, typename TypeB>
inline bool Compare(TypeA A, TypeB B) {
	return detail::__Comparator<TypeA, TypeB>::Compare(A, B);
}

/**
* Returns 1 if A compares to B as equal and 0 if not.
*
* This version simply does A == B, even for floating point numbers, etc. It is
* used to express that this is really what one had in mind. The associated
* warning is suppressed using SYS_WFLOAT_EQUAL_OFF.
**/
template<typename TypeA, typename TypeB>
inline bool CompareSimply(TypeA A, TypeB B) {
	SYS_WFLOAT_EQUAL_OFF
	return A == B;
	SYS_WFLOAT_EQUAL_ON
}

} //namespace systematic

#endif // SYS_COMPARE_H_INCLUDED
