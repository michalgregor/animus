#ifndef Systematic_RealType_H_
#define Systematic_RealType_H_

#include <Systematic/configure.h>

namespace systematic {

//! The floating point type for use in algebraic operations.
#ifdef SYS_ALGEBRA_FLOAT
	typedef float RealType;
#else
	typedef double RealType;
#endif

inline constexpr RealType operator "" _r(long double x) {return static_cast<RealType>(x);}
inline constexpr RealType operator "" _r(unsigned long long x) {return static_cast<RealType>(x);}

} //namespace systematic

#endif //Systematic_RealType_H_
