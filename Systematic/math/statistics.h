#ifndef Systematic_statistics_H_
#define Systematic_statistics_H_

#include "../system.h"
#include <iterator>

namespace systematic {

/**
 * Computes and returns the mean squared error computed over the range of
 * [first1, last), and the range beginning with first2 of the same length.
 */
template<class ForwardIterator1, class ForwardIterator2>
typename std::iterator_traits<ForwardIterator1>::value_type mse(ForwardIterator1 first, ForwardIterator1 last, ForwardIterator2 first2) {
	typedef typename std::iterator_traits<ForwardIterator1>::value_type ValType;
	ValType sse = 0; size_t num = 0;

	for(; first != last; first++, first2++) {
		ValType diff = *first - *first2;
		sse += diff*diff;
		num++;
	}

	return sse/static_cast<ValType>(num);
}

} //namespace systematic

#endif //Systematic_statistics_H_
