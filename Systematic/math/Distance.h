#ifndef Systematic_Distance_H_
#define Systematic_Distance_H_

#include <iterator>
#include <cmath>

namespace systematic {

/**
 * Computes euclidean distance between two sequences.
 *
 * @param first The first element of the first sequence.
 * @param last The one-past-the-end element of the first sequence.
 * @param first2 The first element of the second sequence.
 *
 * @return The distance is of value type ValueType, which is
 * std::iterator_traits<IteratorType1>::value_type unless otherwise specified.
 */
template<class IteratorType1, class IteratorType2, class ValueType = typename std::iterator_traits<IteratorType1>::value_type>
ValueType euclidean_distance(IteratorType1 first, IteratorType1 last, IteratorType2 first2) {
	IteratorType1 iter1 = first;
	IteratorType2 iter2 = first2;

	ValueType sum = 0;

	for(; iter1 != last; ++iter1, ++iter2) {
		ValueType product = *iter1 - *iter2;
		sum += product * product;
	}

	return std::sqrt(sum);
}

} //namespace systematic

#endif //Systematic_Distance_H_
