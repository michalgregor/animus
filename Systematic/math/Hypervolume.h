#ifndef Systematic_Hypervolume_H_
#define Systematic_Hypervolume_H_

#include "../exception/assert.h"
#include "RealType.h"

#include <vector>
#include <set>

namespace systematic {

bool dominates(const std::vector<RealType>& pointA, const std::vector<RealType>& pointB);
bool weaklyDominates(const std::vector<RealType>& pointA, const std::vector<RealType>& pointB);

void invertPoint(std::vector<RealType>& point);
void invertPoints(std::vector<std::vector<RealType> >& points);

std::vector<std::vector<RealType> > paretoFrontFilter(const std::vector<std::vector<RealType> >& points, const std::vector<RealType>& refPoint);

RealType wfg(std::vector<std::vector<RealType> >& points, const std::vector<RealType>& refPoint);
RealType hso(const std::vector<std::vector<RealType> >& points, const std::vector<RealType>& refPoint);

/**
 * Computes hypervolume for the specified set of points (front), and
 * the reference point (refPoint). A minimization task is presumed. (The points
 * can be inverted using invertPoints).
 *
 * If front is empty, returns 0. Otherwise forwards to the best implementation
 * of exact hypervolume computation available.
 *
 * \warning With some implementation, the front may be resorted in place.
 *
 * @pre For every point of front there must be no point in front that weakly
 * dominates it. This can be achieved using the paretoFrontFilter function.
 */
inline RealType computeHypervolume(
	std::vector<std::vector<RealType> >& front,
	const std::vector<RealType>& refPoint
) {
	if(!front.size()) return 0;
	return wfg(front, refPoint);
}

} //namespace systematic

#endif // Systematic_Hypervolume_H_
