#ifndef Systematic_numeric_cast_H_
#define Systematic_numeric_cast_H_

#include <boost/numeric/conversion/cast.hpp>

namespace systematic {
	using boost::numeric_cast;
	using boost::bad_numeric_cast;
} //namespace systematic

#endif //Systematic_numeric_cast_H_
