#ifndef Systematic_Constants_H_
#define Systematic_Constants_H_

#include "../macros.h"

namespace systematic {

/**
 * @class Constant
 * @author Michal Gregor
 * A class that contains a list of static constants.
 */
class SYS_API Constant {
public:
	//! Provides the value of PI.
	static constexpr double pi = 3.141592653589793115997963468544185161590576171875;
	//! Multiply by this to convert radians to degrees.
	static constexpr double rad2deg = 57.29577951308232286464772187173366546630859375;
	//! Multiply by this to convert degrees to radians.
	static constexpr double deg2rad = 0.0174532925199432954743716805978692718781530857086181640625;
	//! Provides the value of e: Euler number.
	static constexpr double e = 2.718281828459045090795598298427648842334747314453125;
};

} //namespace systematic

#endif //Systematic_Constants_H_
