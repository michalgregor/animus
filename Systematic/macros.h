#ifndef MACROS_H_
#define MACROS_H_

#include <Systematic/configure.h>

//! Stringifies the argument with macro expansion.
#define SYS_XSTRINGIFY(s) SYS_STRINGIFY(s)

//! Stringifies the argument without macro expansion.
#define SYS_STRINGIFY(s) #s

//! Expands to nothing.
#define SYS_EMPTY

//! Expands to a comma.
#define SYS_COMMA() ,

//! Expands to the argument.
#define SYS_IDENTITY(X) X

//! Unused parameter macro.
#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
# define UNUSED(x) /*@unused@*/ x
#else
# define UNUSED(x)
#endif

/**
* Wrap deprecated function declarations in this macro like so:
* DEPRECATED(void OldFunc(int a, float b));
*
* Courtesy of http://stackoverflow.com/questions/295120/c-mark-as-deprecated
**/
#ifdef __GNUC__
#define SYS_DEPRECATED(func) func __attribute__ ((deprecated))
#elif defined(_MSC_VER)
#define SYS_DEPRECATED(func) __declspec(deprecated) func
#else
#pragma message("WARNING: You need to implement SYS_DEPRECATED for this compiler")
#define SYS_DEPRECATED(func) func
#endif

//! Defines macros SYS_COMPILE_MESSAGE(TEXT) and SYS_COMPILE_WARNING(TEXT) that
//! can be used to display compile-time messages on compilers that support
//! _Pragma() and pragma message.
#ifdef _Pragma

#define AUXSYS_DO_PRAGMA(X) _Pragma(#X)

#define SYS_COMPILE_MESSAGE(TEXT) AUXSYS_DO_PRAGMA(message(#TEXT))
#define SYS_COMPILE_WARNING(TEXT) AUXSYS_DO_PRAGMA(message("Warning: "#TEXT))

#else

#define SYS_COMPILE_MESSAGE(TEXT)
#define SYS_COMPILE_WARNING(TEXT)

#endif

//------------------------------------------------------------------------------
//---------------------------warning suppression--------------------------------
//------------------------------------------------------------------------------

//! Defines a macro that can be used to suppress a given type of warning on gcc.
#if ((__GNUC__ * 100) + __GNUC_MINOR__) >= 402
# define SYS_DIAG_STR(s) #s
# define SYS_DIAG_JOINSTR(x,y) SYS_DIAG_STR(x ## y)
# define SYS_DIAG_DO_PRAGMA(x) _Pragma (#x)
# define SYS_DIAG_PRAGMA(x) SYS_DIAG_DO_PRAGMA(GCC diagnostic x)
# if ((__GNUC__ * 100) + __GNUC_MINOR__) >= 406
#  define SYS_DIAG_OFF(x) SYS_DIAG_PRAGMA(push) \
          SYS_DIAG_PRAGMA(ignored SYS_DIAG_JOINSTR(-W,x))
#  define SYS_DIAG_ON(x) SYS_DIAG_PRAGMA(pop)
# else
#  define SYS_DIAG_OFF(x) SYS_DIAG_PRAGMA(ignored SYS_DIAG_JOINSTR(-W,x))
#  define SYS_DIAG_ON(x)  SYS_DIAG_PRAGMA(warning SYS_DIAG_JOINSTR(-W,x))
# endif
#else
# define SYS_DIAG_OFF(x)
# define SYS_DIAG_ON(x)
#endif

//!Suppresses the non-virtual destructor warning (for GCC it is the Weffc++)
//!until SYS_WNONVIRT_DTOR_ON is used.
#define SYS_WNONVIRT_DTOR_OFF SYS_IDENTITY(SYS_DIAG_OFF(effc++))
//!Turns the non-virtual destructor warning (for GCC it is the Weffc++)
//!back on after it has been suppressed by SYS_WNONVIRT_DTOR_OFF.
#define SYS_WNONVIRT_DTOR_ON SYS_IDENTITY(SYS_DIAG_ON(effc++))

//!Suppresses the float equality comparison warning (for GCC it is the
//! Wfloat-equal warning) until SYS_WFLOAT_EQUAL_ON is used.
#define SYS_WFLOAT_EQUAL_OFF SYS_IDENTITY(SYS_DIAG_OFF(float-equal))
//!Turns the float equality comparison warning (for GCC it is the
//! Wfloat-equal warning) back on after it has been suppressed by
//! SYS_WFLOAT_EQUAL_OFF.
#define SYS_WFLOAT_EQUAL_ON SYS_IDENTITY(SYS_DIAG_ON(float-equal))

//define which macro should be used for generating function signatures
#ifdef _MSC_VER
	#define _SYS_FUNC_SIG_ __FUNCSIG__
#else

	#define _SYS_FUNC_SIG_ __PRETTY_FUNCTION__
#endif

#endif /* MACROS_H_ */
