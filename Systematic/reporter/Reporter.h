#ifndef Systematic_Reporter_H_
#define Systematic_Reporter_H_

#include "../system.h"

namespace systematic {

/**
* @class Reporter
* @brief The common interface of reporters – objects used to report data so
* that it may be stored and/or processed.
* @author Michal Gregor
**/
template<class DataType>
class Reporter {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */
	template<class Archive>
    void serialize(Archive& UNUSED(ar), const unsigned int UNUSED(version)) {}

protected:
	//! Implement operator() to provide reporting of supplied data.
	virtual void operator()(const DataType& data) = 0;

public:
	//! Data type of the reporter data.
	typedef DataType InputType;

public:
	//! This functor can be used by mechanisms that need to operate on the
	//! reported data, such as adaptive mechanisms. Most reporters don't require
	//! non-const access so this function calls the protected const version by
	//! default.
	//! Classes that require non-const access should provide an empty
	//! implementation for the const version and re-implement this for
	//! the actual processing.
	virtual void operator()(DataType& data) {
		(*this)(static_cast<const DataType&>(data));
	}

	virtual ~Reporter() = 0;
};

/**
* Empty body of the pure virtual destructor.
**/
template<class DataType>
Reporter<DataType>::~Reporter() {
	SYS_EXPORT_INSTANCE()
}

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::Reporter, 1)

#endif //Systematic_Reporter_H_
