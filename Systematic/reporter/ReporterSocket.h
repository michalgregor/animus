#ifndef Systematic_ReporterSocket_H_
#define Systematic_ReporterSocket_H_

#include "../system.h"
#include <Systematic/utils/FunctorSocketBase.h>
#include "Reporter.h"

namespace systematic {

/**
* @class ReporterSocket
* @author Michal Gregor
*
* The connection point to which multiple Reporters can be added. All of these
* reporters will then be notified as new data becomes available.
**/
template<class DataType, class ReporterType = Reporter<DataType> >
class ReporterSocket: private FunctorSocketBase<ReporterType> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<FunctorSocketBase<ReporterType> >(*this);
	}

public:
	using FunctorSocketBase<ReporterType>::add;
	using FunctorSocketBase<ReporterType>::erase;
	using FunctorSocketBase<ReporterType>::clear;

	void operator()(typename ReporterType::InputType data);
};

/**
* Passes reported data to every connected reporter.
**/

template<class DataType, class ReporterType>
void ReporterSocket<DataType, ReporterType>::operator()(typename ReporterType::InputType data) {
	typename FunctorSocketBase<ReporterType>::iterator iter;
	for(iter = this->begin(); iter != this->end(); iter++) {
		(**iter)(data);
	}
}

} //namespace systematic

#endif //Systematic_ReporterSocket_H_
