#ifndef ALGO_EMPTYOBJECT_H_INCLUDED
#define ALGO_EMPTYOBJECT_H_INCLUDED

#include <boost/smart_ptr/detail/atomic_count.hpp>
#include "../system.h"

namespace systematic {

/**
* @class EmptyObject
* @author Michal Gregor
* Pointers to objects of this class are used as storage tokens
* (class StorageToken).
**/

class SYS_API EmptyObject {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version));
	void serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version));

private:
	//! The reference counter itself.
	boost::detail::atomic_count refcount;

	friend void SYS_API intrusive_ptr_add_ref(EmptyObject* obj);
	friend void SYS_API intrusive_ptr_release(EmptyObject* obj);

public:
	long int count();
	EmptyObject& operator=(const EmptyObject&) {return *this;}
	EmptyObject(): refcount(0) {}
	EmptyObject(const EmptyObject&): refcount(0) {}
};

SYS_API void intrusive_ptr_add_ref(EmptyObject* obj);
SYS_API void intrusive_ptr_release(EmptyObject* obj);

} //namespace systematic

#endif // ALGO_EMPTYOBJECT_H_INCLUDED
