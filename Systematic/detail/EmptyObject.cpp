#include "EmptyObject.h"

namespace systematic {

/**
* Empty serialization function (reference count should not be saved it will
* be reconstructed by deserializing the intrusive ptrs.
**/

void EmptyObject::serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

/**
* Empty serialization function (reference count should not be saved it will
* be reconstructed by deserializing the intrusive ptrs.
**/
void EmptyObject::serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}


/**
* Returns the current number of references to the object. 1 means that there is
* only a single pointer to the object. If count is zero there are no intrusive
* pointers pointing to the object.
**/
long int EmptyObject::count() {
	return refcount;
}

//! Required by boost::intrusive_ptr. Increases the reference count.
void SYS_API intrusive_ptr_add_ref(EmptyObject* obj) {
	++(obj->refcount);
}

//! Required by boost::intrusive_ptr. Decreases the reference count and deletes
//! the object when appropriate.
void SYS_API intrusive_ptr_release(EmptyObject* obj) {
	if(obj->refcount <=1) delete obj;
	else --(obj->refcount);
}

} //namespace systematic
