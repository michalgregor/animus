#include "streams.h"
#include <Systematic/configure.h>
#include <iostream>

namespace systematic {

NullBuffer nullBuffer;
WNullBuffer wnullBuffer;

std::ostream nullstream(&nullBuffer);
std::wostream wnullstream(&wnullBuffer);

std::ostream sysout(std::cout.rdbuf());

#ifdef SYS_ENABLE_INFO
	std::ostream sysinfo(std::cout.rdbuf());
#else
	std::ostream sysinfo(&nullBuffer);
#endif

#ifdef SYS_ENABLE_DEBUG
	std::ostream sysdebug(std::cout.rdbuf());
#else
	std::ostream sysdebug(&nullBuffer);
#endif

void RedirectStream(std::ostream& from, std::ostream& to) {
	from.rdbuf(to.rdbuf());
}

} //namespace systematic
