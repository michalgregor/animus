#ifndef SYS_EXTENDED_TYPE_INFO_TYPEINFO_H_INCLUDED
#define SYS_EXTENDED_TYPE_INFO_TYPEINFO_H_INCLUDED

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#ifdef __GNUC__
#pragma GCC system_header
#endif

#ifndef BOOST_SERIALIZATION_DEFAULT_TYPE_INFO
#define BOOST_SERIALIZATION_DEFAULT_TYPE_INFO
	namespace systematic {
		//forward declaration
		template<class T>
		class extended_type_info_TypeInfo;
	} //namespace systematic
    namespace boost {
    namespace serialization {
    template<class T>
    struct extended_type_info_impl {
        typedef ::systematic::extended_type_info_TypeInfo<T> type;
    };
    } // namespace serialization
    } // namespace boost
#endif

#include <typeinfo>
#include <cstdarg>
#include <boost/assert.hpp>
#include <boost/config.hpp>

#include "../../type_info.h"
#include "../../utils/Singleton.h"
#include <boost/utility.hpp>

#include <boost/static_assert.hpp>
#include <boost/serialization/static_warning.hpp>
#include <boost/type_traits/is_polymorphic.hpp>
#include <boost/type_traits/remove_const.hpp>

#include <boost/serialization/config.hpp>
#include <boost/serialization/extended_type_info.hpp>
#include <boost/serialization/factory.hpp>

// hijack serialization access
#include <boost/serialization/access.hpp>

#include <boost/mpl/if.hpp>

#include <boost/config/abi_prefix.hpp> // must be the last header

#ifdef BOOST_MSVC
#  pragma warning(push)
#  pragma warning(disable : 4251 4231 4660 4275 4511 4512)
#endif

namespace systematic {

namespace typeid_system {

class SYS_API extended_type_info_TypeInfo_0:
    public boost::serialization::extended_type_info
{
    virtual const char * get_debug_info() const {
        if(static_cast<const std::type_info *>(0) == m_ti)
            return static_cast<const char *>(0);
        return m_ti->name();
    }
protected:
    const std::type_info * m_ti;
    extended_type_info_TypeInfo_0(const char * key);
    void type_register(const std::type_info & ti);
    void type_unregister();
    const extended_type_info *
    get_extended_type_info(const std::type_info & ti) const;
public:
    virtual bool
    is_less_than(const extended_type_info &rhs) const;
    virtual bool
    is_equal(const extended_type_info &rhs) const;
    const std::type_info & get_typeid() const {
        return *m_ti;
    }

    virtual ~extended_type_info_TypeInfo_0();
};

template<class T, class Enabled = void>
struct TypeInfoKey {
	static inline const char * getKey() {
		return NULL;
	}
};

template<class T>
struct TypeInfoKey<T, typename boost::enable_if<boost::serialization::guid_defined<T> >::type> { // typename HasTypeInfo<T>::type
	static inline const char * getKey() {
		return type_id<T>().name().c_str();
	}
};

} // typeid_system

/**
* An implementation of the extended_type_info concept used by the boost
* serialiation library based on extended_type_info_typeid. The get_key() method
* is implemented in terms of type_id<T>().name().
**/

template<class T>
class extended_type_info_TypeInfo:
public typeid_system::extended_type_info_TypeInfo_0,
public Singleton<extended_type_info_TypeInfo<T> > {
public:

	extended_type_info_TypeInfo():
	typeid_system::extended_type_info_TypeInfo_0(get_key()) {
        type_register(typeid(T));
        key_register();
    }

    virtual ~extended_type_info_TypeInfo() {
        key_unregister();
        type_unregister();
    }

    // get the eti record for the true type of this record
    // relying upon standard type info implemenation (rtti)
    const extended_type_info *
    get_derived_extended_type_info(const T & t) const {
    	// note: this implementation - based on usage of typeid (rtti)
        // only does something if the class has at least one virtual function.
        BOOST_STATIC_WARNING(boost::is_polymorphic< T >::value);
        return
            typeid_system::extended_type_info_TypeInfo_0::get_extended_type_info(
                typeid(t)
            );
    }
    const char * get_key() const {
       return typeid_system::TypeInfoKey<T>::getKey();
    }
    virtual void * construct(unsigned int count, ...) const{
        // count up the arguments
        std::va_list ap;
        va_start(ap, count);
        switch(count){
        case 0:
            return boost::serialization::factory<BOOST_DEDUCED_TYPENAME boost::remove_const< T >::type, 0>(ap);
        case 1:
            return boost::serialization::factory<BOOST_DEDUCED_TYPENAME boost::remove_const< T >::type, 1>(ap);
        case 2:
            return boost::serialization::factory<BOOST_DEDUCED_TYPENAME boost::remove_const< T >::type, 2>(ap);
        case 3:
            return boost::serialization::factory<BOOST_DEDUCED_TYPENAME boost::remove_const< T >::type, 3>(ap);
        case 4:
            return boost::serialization::factory<BOOST_DEDUCED_TYPENAME boost::remove_const< T >::type, 4>(ap);
        default:
            BOOST_ASSERT(false); // too many arguments
            // throw exception here?
            return NULL;
        }
    }
    virtual void destroy(void const * const p) const {
        boost::serialization::access::destroy(
            static_cast<T const * const>(p)
        );
        //delete static_cast<T const * const>(p);
    }
};

} //namespace systematic

#ifdef BOOST_MSVC
#pragma warning(pop)
#endif
#include <boost/config/abi_suffix.hpp> // pops abi_suffix.hpp pragmas

#endif // SYS_EXTENDED_TYPE_INFO_TYPEINFO_H_INCLUDED
