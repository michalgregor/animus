#include "TypeIDTranslator.h"
#include "../../exception/TracedError.h"

#include <boost/bimap.hpp>
#include <boost/static_assert.hpp>
#include <memory>

namespace systematic {

SYS_WNONVIRT_DTOR_OFF
struct Lexicon: boost::bimap<std::string, long> {};
SYS_WNONVIRT_DTOR_ON
typedef boost::bimap<std::string, long>::value_type LexiconEntry;

Lexicon& TypeIDTranslator::lexicon() {
	static Lexicon* lex = new Lexicon;
	if(!lex->size()) lex->insert(LexiconEntry(std::string("No-type"), 0));
	return *lex;
}

long& TypeIDTranslator::currentIdMax() {
	static long* max = new long(1);
	return *max;
}

/**
* Registers the typename and returns a numeric identifier.
**/

long TypeIDTranslator::registerTypeName(const std::string& name) {
	#ifdef __unix__
			lexicon().insert(LexiconEntry(name, currentIdMax()));
			return currentIdMax()++;
	#else
		#ifdef _WIN32
			auto i = lexicon().left.find(name);

			if(i != lexicon().left.end()) {
				return i->second;
			} else {
				lexicon().insert(LexiconEntry(name, currentIdMax()));
				return currentIdMax()++;
			}
		#else
			auto i = lexicon().left.find(name);

			if(i != lexicon().left.end()) {
				return i->second;
			} else {
				lexicon().insert(LexiconEntry(name, currentIdMax()));
				return currentIdMax()++;
			}

			BOOST_STATIC_WARNING(!"YOU SHOULD USE THE __unix__ BRANCH IF STATIC TEMPLATE MEMBERS ARE SHARED AMONG SHARED LIBRARIES ON YOUR SYSTEM. YOU SHOULD USE THE _WIN32 BRANCH OTHERWISE. THE ELSE BRANCH IS THERE SO THAT THE CODE COMPILES AND WORKS SAFELY IN ANY CASE.");
		#endif
	#endif
}

/**
* Returns the numeric type id corresponding to the specified type name.
* If lexicon contains no such entry, a TracedError_Generic is thrown.
**/

long TypeIDTranslator::getID(const std::string& name) {
	Lexicon& lex = lexicon();
	Lexicon::left_iterator iter = lex.left.find(name);
	if(iter == lex.left.end()) throw TracedError_Generic("No such entry found.");
	else return iter->second;
}

/**
* Returns the type name corresponding to the specified numeric id.
* If lexicon contains no such entry, a TracedError_Generic is thrown.
**/

const std::string& TypeIDTranslator::getName(long id) {
	Lexicon& lex = lexicon();
	Lexicon::right_iterator iter = lex.right.find(id);
	if(iter == lex.right.end()) throw TracedError_Generic("No such entry found.");
	else return iter->second;
}

} /* namespace systematic */
