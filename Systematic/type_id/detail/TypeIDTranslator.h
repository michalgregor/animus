#ifndef TYPEIDTRANSLATOR_H_
#define TYPEIDTRANSLATOR_H_

#include <string>
#include "../../macros.h"

namespace systematic {

struct Lexicon;

/**
* @class TypeIDTranslator
* Used to map numeric type ids to type names and vice versa. The numbering
* starts from 1. Zero is reserved for no-type.
**/

class SYS_API TypeIDTranslator {
public:
	//! Stores the lexicon used to do the bidirectional mapping.
	static Lexicon& lexicon();
	//! Currently the greatest id - the ids are assigned sequentially.
	static long& currentIdMax();

private:
	TypeIDTranslator();

public:
	static long registerTypeName(const std::string& name);
	static long getID(const std::string& name);
	static const std::string& getName(long id);
};

} //namespace systematic

#endif /* TYPEIDTRANSLATOR_H_ */
