#ifndef SYSTEMATIC_TYPE_ID_PAIR_H_
#define SYSTEMATIC_TYPE_ID_PAIR_H_

#include "../export.h"

#include <utility>
SYS_EXPORT_TEMPLATE(std::pair, 2)

#endif /* SYSTEMATIC_TYPE_ID_PAIR_H_ */
