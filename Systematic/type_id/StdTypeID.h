#ifndef STDTYPEID_H_
#define STDTYPEID_H_

#include <typeinfo>
#include <string>

namespace systematic {

/**
* @class StdTypeID
* A convenience wrapper around std::type_info. Provides interface similar to
* systematic::TypeID.
**/
class StdTypeID {
private:
	//friend functions needed to serialize this
	friend void boost::serialization::serialize(systematic::OArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version));
	friend void boost::serialization::serialize(systematic::IArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version));

private:
	//! Stores type info of the type (stored as pointer - and has to be
	//! dereferenced for comparisons to be safe!!!).
	const std::type_info* _typeinfo = &typeid(void);

public:
	/**
	* Returns a 0 if both TypeIDs are equal, a negative value if this is less
	* than typeList and a positive value if this is greater than type.
	 */
	int compare(const StdTypeID& type) const {
		if(*this == type) return 0;
		if(*this < type) return -1;
		else return 1;
	}

	bool operator==(const StdTypeID& type) const {
		return *_typeinfo == *type._typeinfo;
	}

	bool operator!=(const StdTypeID& type) const {
		return *_typeinfo != *type._typeinfo;
	}

	bool operator<(const StdTypeID& type) const {
		return _typeinfo->before(*type._typeinfo);
	}

	bool operator>(const StdTypeID& type) const {
		return type._typeinfo->before(*_typeinfo);
	}

	bool operator<=(const StdTypeID& type) const {
		return *this < type || *this == type;
	}

	bool operator>=(const StdTypeID& type) const {
		return *this > type || *this == type;
	}

	inline const char* rawName() const {
		return _typeinfo->name();
	}

	std::string name() const {
		return rawName();
	}

public:
	StdTypeID& operator=(const StdTypeID&) = default;
	StdTypeID(const StdTypeID&) = default;

	StdTypeID() = default;
	StdTypeID(const std::type_info& typeinfo): _typeinfo(&typeinfo) {}
};

inline std::ostream& operator<<(std::ostream& os, const systematic::StdTypeID& id) {
	os << id.name() << std::endl;
	return os;
}

} //namespace systematic

#endif /* STDTYPEID_H_ */
