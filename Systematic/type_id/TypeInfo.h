#ifndef SYS_TYPEINFO_H_INCLUDED
#define SYS_TYPEINFO_H_INCLUDED

#include <boost/preprocessor/repetition/enum_params.hpp>
#include <boost/preprocessor/arithmetic/dec.hpp>
#include <boost/preprocessor/seq/for_each_i.hpp>
#include <boost/preprocessor/tuple/to_seq.hpp>
#include <boost/preprocessor/comparison/less.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>

#include <cstring>
#include <string>
#include <type_traits>
#include "TypeID.h"
#include <Systematic/configure.h>
#include "../utils/ToString.h"

//---------------------------AUXILIARY MACROS-----------------------------------
//! An auxiliary macro used to implement AUXSYS_TPL_PARAM_SEL.
#define AUXSYS_TPL_PARAM_SEL_STYPE(TYPE, NAME, HASTYPEINFO) TYPE
//! An auxiliary macro used to implement AUXSYS_TPL_PARAM_SEL.
#define AUXSYS_TPL_PARAM_SEL_SNAME(TYPE, NAME, HASTYPEINFO) NAME
//! An auxiliary macro used to implement AUXSYS_TPL_PARAM_SEL.
#define AUXSYS_TPL_PARAM_SEL_SHASTYPEINFO(TYPE, NAME, HASTYPEINFO) HASTYPEINFO

//! An auxiliary macro - return TYPE for SEL == STYPE, NAME for SEL == SNAME,
//! and HASTYPEINFO for SEL == SHASTYPEINFO. Undefined for other values of SEL.
#define AUXSYS_TPL_PARAM_SEL(SEL, TYPE, NAME, HASTYPEINFO) 		\
AUXSYS_TPL_PARAM_SEL_##SEL(TYPE, NAME, HASTYPEINFO)
//-------------------------END AUXILIARY MACROS---------------------------------

namespace systematic {
namespace detail {

/**
* @struct TypeInfo
* A structure that provides the name of its template argument type. These names
* are stored in the structure using template specialization. Auxiliary macros
* are provided that make the process of specialization very straight-forward:
* SYS_REGISTER_TYPENAME, SYS_REGISTER_TEMPLATENAME. Macro
* SYS_REGISTER_TEMPLATENAME also creates a specialization of the TemplateInfo
* structure template.
**/

template<class Type, class Aux = void>
class TypeInfo {};

char* __SYS_REGISTER_TEMPLATENAME_AUX_FUNCTION__(char* pos);

/**
* @struct HasTypeInfo
* @author Michal Gregor
* This type trait may be used to determine whether a given Type has been
* registered with TypeInfo, that is whether a name and TypeID can be assigned to
* it. The trait looks for the name_undefined typedef to determine this.
**/
SYS_WNONVIRT_DTOR_OFF

template<class Type, class Enable = void>
struct HasTypeInfo: public std::false_type {
	typedef HasTypeInfo type;
};

SYS_WNONVIRT_DTOR_ON

template<bool>
struct HasTypeInfo_CompileError {};

template<>
struct HasTypeInfo_CompileError<true> {
	typedef char type;
};

template<char Enable>
struct TiAllDefined {
	typedef void type;
};

} //namespace detail
} //namespace systematic

/**
* Auxiliary macro called from __SYS_TEMPLATENAME_TI_ALL_DEFINED__. Used to
* detect whether any component type is unregistered.
**/

#define __SYS_UNREGISTERED_COMPONENT_CHECK__(z, n, d) 				\
+ sizeof(typename systematic::detail::HasTypeInfo_CompileError<		\
systematic::detail::HasTypeInfo<									\
	typename std::remove_const<										\
		typename std::remove_pointer<								\
			typename std::remove_reference<							\
				T##n												\
			>::type													\
		>::type														\
	>::type															\
>::value>::type)

//Records that HasTypeInfo<TYPE>.

#define __SYS_TYPE_HASTYPEINFO__(TYPE) 								\
SYS_WNONVIRT_DTOR_OFF												\
namespace systematic {												\
namespace detail {													\
template<>															\
struct HasTypeInfo<TYPE, void>: public std::true_type {					\
	typedef HasTypeInfo type;										\
};																	\
}																	\
}																	\
SYS_WNONVIRT_DTOR_ON

#define __SYS_TEMPLATE_HASTYPEINFO__(TEMPLATE, NUM_PARAMS) 									\
SYS_WNONVIRT_DTOR_OFF																		\
namespace systematic {																		\
namespace detail {																			\
template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, class T) >										\
struct HasTypeInfo<																			\
	TEMPLATE<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) >,											\
	typename detail::TiAllDefined<															\
		0 BOOST_PP_REPEAT(NUM_PARAMS, __SYS_UNREGISTERED_COMPONENT_CHECK__, defined) 		\
	>::type																					\
>: public std::true_type {																		\
	typedef HasTypeInfo<																	\
		TEMPLATE<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) >,										\
		typename detail::TiAllDefined<														\
			0 BOOST_PP_REPEAT(NUM_PARAMS, __SYS_UNREGISTERED_COMPONENT_CHECK__, defined) 	\
		>::type																				\
	> type;																					\
};																							\
}																							\
}																							\
SYS_WNONVIRT_DTOR_ON

//for TypeID with translation
#define __SYS_REGISTER_NAME_TYPEID_0()										\
private:																	\
	static long& idcode() {													\
		static long code = TypeIDTranslator::registerTypeName(getName());	\
		return code;														\
	}																		\
public:																		\
	static const TypeID& getID() {static TypeID id(idcode()); return id;}

/**
* SYS_REGISTER_TYPENAME macro registers type T with the TypeInfo class so that
* it can be used to query for its name. You should always specify full path
* (including namespaces) to the class to avoid naming collisions.
**/
#define SYS_REGISTER_TYPENAME(T) 								\
	AUXSYS_REGISTER_TYPENAME(T)									\
	AUXSYS_REGISTER_TYPENAME(const T)							\
	AUXSYS_REGISTER_TYPENAME(T&)								\
	AUXSYS_REGISTER_TYPENAME(const T&)							\
	AUXSYS_REGISTER_TYPENAME(T*)								\
	AUXSYS_REGISTER_TYPENAME(const T*)							\
	__SYS_TYPE_HASTYPEINFO__(T)

#define AUXSYS_REGISTER_TYPENAME(T) 								\
namespace systematic {												\
namespace detail {													\
template<class Aux>													\
class TypeInfo<T, Aux> {											\
public:																\
	static const std::string& getName() {							\
		static std::string sname; sname = #T;						\
		return sname;												\
	}																\
__SYS_REGISTER_NAME_TYPEID_0()										\
};																	\
}																	\
}

/**
* SYS_REGISTER_TEMPLATENAME macro registers a template class T
* with the TypeInfo class so that it can be used to query for its name.
* @param T Name of the template class. You should always specify full path
* (including namespaces) to the class to avoid naming collisions.
* @param NUM_PARAMS Number of parameters that the template takes.
**/
#define SYS_REGISTER_TEMPLATENAME(Template, NUM_PARAMS)									\
	AUXSYS_REGISTER_TEMPLATENAME(Template, , NUM_PARAMS)								\
	AUXSYS_REGISTER_TEMPLATENAME(const Template, , NUM_PARAMS)							\
	AUXSYS_REGISTER_TEMPLATENAME(Template, &, NUM_PARAMS)								\
	AUXSYS_REGISTER_TEMPLATENAME(const Template, &, NUM_PARAMS)							\
	AUXSYS_REGISTER_TEMPLATENAME(Template, *, NUM_PARAMS)								\
	AUXSYS_REGISTER_TEMPLATENAME(const Template, *, NUM_PARAMS)							\
	__SYS_TEMPLATE_HASTYPEINFO__(Template, NUM_PARAMS)

#define AUXSYS_REGISTER_TEMPLATENAME(Template, REFCHAR, NUM_PARAMS)						\
SYS_WNONVIRT_DTOR_OFF																	\
namespace systematic {																	\
namespace detail {																		\
template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, class T) >									\
class TypeInfo<																			\
	Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > REFCHAR,								\
	typename detail::TiAllDefined<														\
		0 BOOST_PP_REPEAT(NUM_PARAMS, __SYS_UNREGISTERED_COMPONENT_CHECK__, defined) 	\
	>::type																				\
> {																						\
public:																					\
	static const std::string& getName() {												\
		static std::string sname;														\
																						\
		if(!sname.size()) {																\
			auto reflen = std::strlen(#REFCHAR);										\
			sname.reserve(																\
				sizeof(#Template"<") + reflen											\
				BOOST_PP_REPEAT(														\
					NUM_PARAMS,															\
					__SYS_REGISTER_TEMPLATENAME_AUX1__,									\
					0																	\
				) + 2*NUM_PARAMS - 1													\
			);																			\
																						\
			sname = std::string(#Template"<")											\
			BOOST_PP_REPEAT(															\
				NUM_PARAMS,																\
				__SYS_REGISTER_TEMPLATENAME_AUX2__,										\
			);																			\
			sname[sname.size()-2-reflen] = ' ';											\
			sname[sname.size()-1-reflen] = '>';											\
			std::strcpy(&sname[sname.size()-reflen], #REFCHAR);							\
		}																				\
		return sname;																	\
	}																					\
__SYS_REGISTER_NAME_TYPEID_0()															\
};																						\
}																						\
}																						\
SYS_WNONVIRT_DTOR_ON

/**
* Auxiliary macro called from SYS_REGISTER_TEMPLATENAME.
**/

#define __SYS_REGISTER_TEMPLATENAME_AUX1__(z, n, d)					\
+ ::systematic::detail::TypeInfo<T##n>::getName().size()

/**
* Auxiliary macro called from SYS_REGISTER_TEMPLATENAME.
**/

#define __SYS_REGISTER_TEMPLATENAME_AUX2__(z, n, d)					\
+ ::systematic::detail::TypeInfo<T##n>::getName() + ", "

//------------------------------------------------------------------------------
//------------Generalised definition for non-type template parameters-----------
//------------------------------------------------------------------------------

/**
* SYS_REGISTER_TEMPLATENAME_DETAILED macro registers a template class T
* with the TypeInfo class so that it can be used to query for its name.
* It is a generalized version of the more simple SYS_REGISTER_TEMPLATENAME
* macro. This version also works with non-type template parameters.
* @param T Name of the template class. You should always specify full path
* (including namespaces) to the class to avoid naming collisions.
* @param NUM_PARAMS Number of parameters that the template takes.
* @param TPARAMS A tuple of template parameter macros. It has the following form
* for N parameters: (SYS_TPARAM_$T1, SYS_TPARAM_$T2, ..., SYS_TPARAM_$TN), where
* $T1 is a macro for the corresponding type, e.g. SYS_TPARAM_CLASS for
* a parameter of class type, or SYS_TPARAM_BOOL for a boolean paramter.
*
* @example
*
* Let us have the following class template:
*
* template<class TParam1, bool TParam2, int TParam3>
* class FooClass {};
*
* This is registered as follows:
*
* SYS_REGISTER_TEMPLATENAME_DETAILED(FooClass, 3, (SYS_TPARAM_CLASS, SYS_TPARAM_BOOL, SYS_TPARAM_INT))
*
* == Defining Custom Type Macros ==
*
* A type macro takes 2 parameters: N, S. N is a switch which determines what
* S should be expanded to. S is an identifier of a template parameter (such
* as TParam1, or TParam2 in the previous example).
*
* N can take one of the following values:
* 	- STYPE: The macro should expand to the type of the template parameter and
* 			 its identifier passed to the macro using S, such as: class S,
* 			 int S, bool S. This is used to generate the template declaration
* 	         (such as template<class TParam1, bool TParam2, int TParam3>).
*
*   - SNAME: The macro should expand to an expression returning std::string
*   		 or a reference to an std::string (const or non-const) containing
*   		 the name of the argument. E.g. "int#15" for a parameter of type int
*   		 with the value of 15.
*
*   - SHASTYPEINFO: The macro should expand to a TruthValue type:
*   				std::true_type, std::false_type, or to a type
*   				derived thereof. Expanding to std::true_type means that a type
*   				name exists, std::false_type means undefined. The type name may
*   				be undefined, e.g. if there is a template parameter of
*   				class type which has itself not been registered. To provide
*   				an example - SYS_TPARAM_CLASS(SHASTYPEINFO, S) expands to:*
*   				systematic::detail::HasTypeInfo<S>.
 */

#define SYS_REGISTER_TEMPLATENAME_DETAILED(Template, NUM_PARAMS, TPARAMS) 				\
SYS_WNONVIRT_DTOR_OFF																	\
namespace systematic {																	\
namespace detail {																		\
template<																				\
	BOOST_PP_SEQ_FOR_EACH_I(															\
		AUXSYS_REGISTER_FOR_EACH_TPARAM_TYPE,											\
		NUM_PARAMS,																		\
		BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)										\
	) 																					\
>																						\
class TypeInfo<																			\
	Template<																			\
		BOOST_PP_SEQ_FOR_EACH_I(														\
			AUXSYS_REGISTER_FOR_EACH_TPARAM_VAL,										\
			NUM_PARAMS,																	\
			BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)									\
		)																				\
	>,																					\
	typename detail::TiAllDefined<														\
		0 BOOST_PP_SEQ_FOR_EACH_I(														\
			AUXSYS_REGISTER_FOR_EACH_TPARAM_HASTYPEINFO,								\
			NUM_PARAMS,																	\
			BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)									\
		)																				\
	>::type																				\
> {																						\
public:																					\
	static const std::string& getName() {												\
		static std::string sname;														\
		sname.reserve(																	\
			sizeof(#Template"<")														\
		    BOOST_PP_SEQ_FOR_EACH_I(													\
				AUXSYS_REGISTER_FOR_EACH_TPARAM_NAME_SIZE,								\
				NUM_PARAMS,																\
				BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)								\
		    ) + 2*NUM_PARAMS - 1														\
		);																				\
																						\
		if(!sname.size()) { 															\
			sname += std::string(#Template"<");											\
			BOOST_PP_SEQ_FOR_EACH_I(													\
				AUXSYS_REGISTER_FOR_EACH_TPARAM_NAME,									\
				sname,																	\
				BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)								\
			);																			\
			sname[sname.size()-2] = ' ';												\
			sname[sname.size()-1] = '>';												\
		}																				\
		return sname;																	\
	}																					\
__SYS_REGISTER_NAME_TYPEID_0()															\
};																						\
}																						\
}																						\
AUXSYS_TEMPLATE_HASTYPEINFO_DETAILED(Template, NUM_PARAMS, TPARAMS)						\
SYS_WNONVIRT_DTOR_ON

//! Auxiliary macro.
#define AUXSYS_TEMPLATE_HASTYPEINFO_DETAILED(TEMPLATE, NUM_PARAMS, TPARAMS) 			\
SYS_WNONVIRT_DTOR_OFF																	\
namespace systematic {																	\
namespace detail {																		\
template<																				\
	BOOST_PP_SEQ_FOR_EACH_I(															\
		AUXSYS_REGISTER_FOR_EACH_TPARAM_TYPE,											\
		NUM_PARAMS,																		\
		BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)										\
	) 																					\
>																						\
struct HasTypeInfo<																		\
	TEMPLATE<																			\
		BOOST_PP_SEQ_FOR_EACH_I(														\
			AUXSYS_REGISTER_FOR_EACH_TPARAM_VAL,										\
			NUM_PARAMS,																	\
			BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)									\
		)																				\
	>,																					\
	typename detail::TiAllDefined<														\
		0 BOOST_PP_SEQ_FOR_EACH_I(														\
			AUXSYS_REGISTER_FOR_EACH_TPARAM_HASTYPEINFO,								\
			NUM_PARAMS,																	\
			BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)									\
		)																				\
	>::type																				\
>: public std::true_type {																	\
	typedef HasTypeInfo type;															\
};																						\
}																						\
}																						\
SYS_WNONVIRT_DTOR_ON

//! Auxiliary macro.
#define AUXSYS_REGISTER_FOR_EACH_TPARAM_TYPE(r, SIZE, i, elem)				\
elem(STYPE, A##i) BOOST_PP_COMMA_IF(BOOST_PP_LESS(i, BOOST_PP_DEC(SIZE)))

//! Auxiliary macro.
#define AUXSYS_REGISTER_FOR_EACH_TPARAM_VAL(r, SIZE, i, elem)				\
A##i BOOST_PP_COMMA_IF(BOOST_PP_LESS(i, BOOST_PP_DEC(SIZE)))

//! Auxiliary macro.
#define AUXSYS_REGISTER_FOR_EACH_TPARAM_HASTYPEINFO(r, SIZE, i, elem)		\
+ sizeof(																	\
	typename systematic::detail::											\
	HasTypeInfo_CompileError<elem(SHASTYPEINFO, A##i)::value>::type			\
)

//! Auxiliary macro.
#define AUXSYS_REGISTER_FOR_EACH_TPARAM_NAME_SIZE(r, SIZE, i, elem)			\
+ elem(SNAME, A##i).size()

//! Auxiliary macro.
#define AUXSYS_REGISTER_FOR_EACH_TPARAM_NAME(r, NAME_STRING, i, elem)		\
(NAME_STRING += elem(SNAME, A##i)) += std::string(", ");

//-----------------------EXPORT OF SOME COMMON POD TYPES------------------------

AUXSYS_REGISTER_TYPENAME(void)
__SYS_TYPE_HASTYPEINFO__(void)
SYS_REGISTER_TYPENAME(bool)
SYS_REGISTER_TYPENAME(unsigned char)
SYS_REGISTER_TYPENAME(signed char)
SYS_REGISTER_TYPENAME(char)
SYS_REGISTER_TYPENAME(int)
SYS_REGISTER_TYPENAME(unsigned int)
SYS_REGISTER_TYPENAME(short int)
SYS_REGISTER_TYPENAME(unsigned short int)
SYS_REGISTER_TYPENAME(long int)
SYS_REGISTER_TYPENAME(unsigned long int)
SYS_REGISTER_TYPENAME(float)
SYS_REGISTER_TYPENAME(double)
SYS_REGISTER_TYPENAME(long double)
SYS_REGISTER_TYPENAME(wchar_t)

//------------------EXPORT SOME OTHER COMMONLY USED TYPES-----------------------

#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/intrusive_ptr.hpp>
#include "../pointer/smart_ptr.h"

SYS_REGISTER_TEMPLATENAME(shared_ptr, 1)
SYS_REGISTER_TEMPLATENAME(boost::intrusive_ptr, 1)
SYS_REGISTER_TEMPLATENAME(std::allocator, 1)
SYS_REGISTER_TEMPLATENAME(std::vector, 2)

//-----------------PROVIDE MACROS FOR INTEGRAL TPARAM TYPES---------------------
//--------------FOR USE IN SYS_REGISTER_TEMPLATENAME_DETAILED-------------------

//! Macro for class template parameters for use
//! with SYS_REGISTER_TEMPLATENAME_DETAILED.

#define SYS_TPARAM_CLASS(N, S)												\
AUXSYS_TPL_PARAM_SEL(														\
	N,																		\
	class S,																\
	::systematic::detail::TypeInfo<S>::getName(),							\
	::systematic::detail::HasTypeInfo<S>									\
)

//! Macro for bool template parameters for use
//! with SYS_REGISTER_TEMPLATENAME_DETAILED.

#define SYS_TPARAM_BOOL(N, S) 												\
AUXSYS_TPL_PARAM_SEL(														\
	N,																		\
	bool S,																	\
	("bool#" + systematic::ToString(S)),									\
	std::true_type															\
)

//! Macro for int template parameters for use
//! with SYS_REGISTER_TEMPLATENAME_DETAILED.

#define SYS_TPARAM_INT(N, S) 												\
AUXSYS_TPL_PARAM_SEL(														\
	N,																		\
	int S,																	\
	("int#" + systematic::ToString(S)),										\
	std::true_type															\
)

//! Macro for unsigned int template parameters for use
//! with SYS_REGISTER_TEMPLATENAME_DETAILED.

#define SYS_TPARAM_UNSIGNED_INT(N, S) 										\
AUXSYS_TPL_PARAM_SEL(														\
	N,																		\
	unsigned int S,															\
	("unsigned%int#" + systematic::ToString(S)),							\
	std::true_type															\
)

//! Macro for long template parameters for use
//! with SYS_REGISTER_TEMPLATENAME_DETAILED.

#define SYS_TPARAM_LONG(N, S) 												\
AUXSYS_TPL_PARAM_SEL(														\
	N,																		\
	long S,																	\
	("long#" + systematic::ToString(S)),									\
	std::true_type															\
)

//! Macro for unsigned long template parameters for use
//! with SYS_REGISTER_TEMPLATENAME_DETAILED.

#define SYS_TPARAM_UNSIGNED_LONG(N, S) 										\
AUXSYS_TPL_PARAM_SEL(														\
	N,																		\
	unsigned long S,														\
	("unsigned%long#" + systematic::ToString(S)),							\
	std::true_type															\
)

//! Macro for char template parameters for use
//! with SYS_REGISTER_TEMPLATENAME_DETAILED.

#define SYS_TPARAM_CHAR(N, S) 												\
AUXSYS_TPL_PARAM_SEL(														\
	N,																		\
	char S,																	\
	("char#" + systematic::ToString(S)),									\
	std::true_type															\
)

//! Macro for signed char template parameters for use
//! with SYS_REGISTER_TEMPLATENAME_DETAILED.

#define SYS_TPARAM_SIGNED_CHAR(N, S) 										\
AUXSYS_TPL_PARAM_SEL(														\
	N,																		\
	signed char S,															\
	("signed%char#" + systematic::ToString(S)),								\
	std::true_type															\
)

//! Macro for unsigned char template parameters for use
//! with SYS_REGISTER_TEMPLATENAME_DETAILED.

#define SYS_TPARAM_UNSIGNED_CHAR(N, S) 										\
AUXSYS_TPL_PARAM_SEL(														\
	N,																		\
	unsigned char S,														\
	("unsigned%char#" + systematic::ToString(S)),							\
	std::true_type															\
)

#endif //SYS_TYPEINFO_H_INCLUDED
