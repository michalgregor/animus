#ifndef Systematic_NoType_H_
#define Systematic_NoType_H_

#include "TypeInfo.h"
#include <Systematic/configure.h>

namespace systematic {

/**
 * A class used to represent that there is no type (in type ids, etc.).
 */

class SYS_API NoType {};

} //namespace systematic

SYS_REGISTER_TYPENAME(systematic::NoType)

#endif //Systematic_NoType_H_
