#ifndef SYS_TYPEID_H_INCLUDED
#define SYS_TYPEID_H_INCLUDED

#include "../macros.h"
#include "../serialization/archive.h"
#include "detail/TypeIDTranslator.h"
#include <string>
#include <iostream>

//forward declarations
namespace systematic {
	class TypeID;

namespace detail {
	template<class Type, class Aux>
	class TypeInfo;
} //namespace detail

} //namespace systematic

namespace boost {
namespace serialization {
	//forward declaration of serialization functions
	void serialize(systematic::OArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version));
	void serialize(systematic::IArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version));
} //namespace serialization
} //namespace boost

namespace systematic {

/**
* @class TypeID
* A unique type identifier. The information about the type is represented by a
* long int in this imlementation. TypeIDTranslator is used to translate between
* the numeric IDs and type names.
*
* Can be retrieved using the TypeInfo class for types registered with the
* TypeInfo system.
**/
class SYS_API TypeID {
private:
	//friend functions needed to serialize this
	friend void boost::serialization::serialize(systematic::OArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version));
	friend void boost::serialization::serialize(systematic::IArchive& ar, systematic::TypeID& g, const unsigned int UNUSED(version));

private:
	//! Stores the id code of the type.
	long _idcode;

private:
	template<class Type, class Aux>
	friend class ::systematic::detail::TypeInfo;

	TypeID(long idcode);
	static long registerTypeName(const std::string& name);

public:
	int compare(const TypeID& type) const;
	bool operator==(const TypeID& type) const;
	bool operator!=(const TypeID& type) const;
	bool operator<(const TypeID& type) const;
	bool operator>(const TypeID& type) const;
	bool operator<=(const TypeID& type) const;
	bool operator>=(const TypeID& type) const;
	inline const std::string& name() const;
	TypeID& operator=(const TypeID& typeID);
	TypeID(const TypeID& typeID);
	TypeID();
};

inline std::ostream& operator<<(std::ostream& os, const systematic::TypeID& id) {
	os << id.name() << std::endl;
	return os;
}

/**
 * Used from TypeInfo to register type names with the TypeIDTranslator.
 */
inline long TypeID::registerTypeName(const std::string& name) {
	return TypeIDTranslator::registerTypeName(name);
}

/**
* Returns a 0 if both TypeIDs are equal, a negative value if this is less
* than typeList and a positive value if this is greater than type.
 */
inline int TypeID::compare(const TypeID& type) const {
	if(_idcode < type._idcode) return -1;
	else if(_idcode == type._idcode) return 0;
	else return 1;
}

/**
 * Comparison based on the numeric id.
 */
inline bool TypeID::operator==(const TypeID& type) const {
	return _idcode == type._idcode;
}

/**
 * Comparison based on the numeric id.
 */
inline bool TypeID::operator!=(const TypeID& type) const {
	return _idcode != type._idcode;
}

/**
 * Comparison based on the numeric id.
 */
inline bool TypeID::operator<(const TypeID& type) const {
	return _idcode < type._idcode;
}

/**
 * Comparison based on the numeric id.
 */
inline bool TypeID::operator>(const TypeID& type) const {
	return _idcode > type._idcode;
}

/**
 * Comparison based on the numeric id.
 */
inline bool TypeID::operator<=(const TypeID& type) const {
	return _idcode <= type._idcode;
}

/**
 * Comparison based on the numeoperatorric id.
 */
inline bool TypeID::operator>=(const TypeID& type) const {
	return _idcode >= type._idcode;
}

/**
 * Translates the TypeID into its string representation.
 */
inline const std::string& TypeID::name() const {
	return TypeIDTranslator::getName(_idcode);
}

/**
 * Assignment.
 */
inline TypeID& TypeID::operator=(const TypeID& typeID) {
	_idcode = typeID._idcode;
	return *this;
}

/**
 * Copy constructor.
 */
inline TypeID::TypeID(const TypeID& typeID): _idcode(typeID._idcode) {}

/**
 * Default constructor. The id is set to 0, which is the reserved No-type value.
 */
inline TypeID::TypeID(): _idcode(0) {}

/**
 * Private constructor. For use by class TypeInfo (a friend class).
 */
inline TypeID::TypeID(long idcode): _idcode(idcode) {}

} //namespace systematic

#endif // SYS_TYPEID_H_INCLUDED
