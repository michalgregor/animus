#include "TypeInfo.h"

namespace systematic {
namespace detail {

/**
* Auxiliary function called from SYS_REGISTER_TEMPLATENAME.
**/

char* __SYS_REGISTER_TEMPLATENAME_AUX_FUNCTION__(char* pos) {
	//insert ", " into the string
	*pos++ = ','; *pos++ = ' ';
	return pos;
}

} //namespace detail
} //namespace systematic
