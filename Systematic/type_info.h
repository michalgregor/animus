#ifndef Systematic_type_id_H_
#define Systematic_type_id_H_

#include "type_id/TypeID.h"
#include "type_id/TypeInfo.h"
#include "type_id/StdTypeID.h"
#include <Systematic/configure.h>

namespace systematic {
namespace detail {

	template<class Type, class TypeID>
	struct Aux_type_id {
		static TypeID provide() {
			return systematic::detail::TypeInfo<Type>::getID();
		}
	};

	template<class Type>
	struct Aux_type_id<Type, StdTypeID> {
		static StdTypeID provide() {
			return StdTypeID(typeid(Type));
		}
	};

} //namespace detail

/**
 * Returns TypeID of the specified type.
 */
template<class Type, class TypeID = systematic::TypeID>
inline TypeID type_id() {
	return detail::Aux_type_id<Type, TypeID>::provide();
}

} //namespace systematic

#endif //Systematic_type_id_H_
