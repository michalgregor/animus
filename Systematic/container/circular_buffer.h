#ifndef Systematic_circular_buffer_H_
#define Systematic_circular_buffer_H_

// circular buffer will cause trouble if NDEBUG is defined when compiling the
// library and not defined when compiling the executable and vice versa
#ifdef NDEBUG
	#define SYS_NDEBUG_MEM
#else
	#ifdef SYS_NDEBUG_MEM
		#undef SYS_NDEBUG_MEM
	#endif
	#define NDEBUG
#endif

#include <boost/circular_buffer.hpp>

#ifndef SYS_NDEBUG_MEM
#undef NDEBUG
#endif

namespace systematic {

	using boost::circular_buffer;

} //namespace systematic

#endif //Systematic_circular_buffer_H_
