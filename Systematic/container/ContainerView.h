#ifndef Systematic_ContainerView_H_
#define Systematic_ContainerView_H_

#include <iterator>
#include <boost/type_traits/is_same.hpp>
#include <boost/utility/enable_if.hpp>
#include "../system.h"

namespace systematic {

/**
 * @class ContainerView
 * @author Michal Gregor
 *
 * Acts as a wrapper for an iterator range that provides it with
 * a container-like interface.
 *
 * \warning Any action that invalidates the iterators from which this view
 * has been constructed invalidates the view itself.
 *
 * @tparam IterType Type of the iterator from which the ContainerView is
 * constructed.
 **/
template<class IterType, class Enable = void>
class ContainerView {
public:
	typedef size_t size_type;
	typedef IterType iterator;
	typedef std::reverse_iterator<IterType> reverse_iterator;

	typedef typename std::iterator_traits<IterType>::reference reference;
	typedef typename std::iterator_traits<IterType>::value_type value_type;

private:
	//! Iterator to the first element.
	iterator _first;
	//! Iterator to the past-the-end element.
	iterator _last;
	//! Stores size; so that it does not have to be computed using std::distance
	//! every time which is costly for forward iterators and such.
	size_type _size;

public:
	size_type size() const {
		return _size;
	}

	iterator begin() const {
		return _first;
	}
	iterator end() const {
		return _last;
	}

	reverse_iterator rbegin() const {
		return reverse_iterator(_last);
	}
	reverse_iterator rend() const {
		return reverse_iterator(_first);
	}

	reference front() {
		if(!_size) throw TracedError_OutOfRange("No elements available.");
		return *_first;
	}

	reference back() {
		if(!_size) throw TracedError_OutOfRange("No elements available.");
		iterator back_ = _last; --back_;
		return *back_;
	}

public:
	ContainerView();
	ContainerView(iterator first, iterator last);
};

/**
 * Default constructor.
 *
 * Constructs a ContainerView from default constructed iterators. Which usually
 * corresponds to an empty range.
 **/

template<class IterType, class Enable>
ContainerView<IterType, Enable>::ContainerView() :
		_first(), _last(), _size(std::distance(_first, _last)) {
}

/**
 * Constructor.
 *
 * Constructs a ContainerView to the range [first, last).
 **/

template<class IterType, class Enable>
ContainerView<IterType, Enable>::ContainerView(iterator first, iterator last) :
		_first(first), _last(last), _size(std::distance(_first, _last)) {
}

/**
 * Specialization for random access iterators.
 */
template<class IterType>
class ContainerView<IterType, typename boost::enable_if<typename boost::is_same<typename std::iterator_traits<IterType>::iterator_category, std::random_access_iterator_tag>::type >::type> {
public:
	typedef size_t size_type;
	typedef IterType iterator;
	typedef std::reverse_iterator<IterType> reverse_iterator;

	typedef typename std::iterator_traits<IterType>::reference reference;
	typedef typename std::iterator_traits<IterType>::value_type value_type;

private:
	//! Iterator to the first element.
	iterator _first;
	//! Iterator to the past-the-end element.
	iterator _last;
	//! Stores size; so that it does not have to be computed using std::distance
	//! every time which is costly for forward iterators and such.
	size_type _size;

public:
	//! Returns reference to the element at position pos.
	reference operator[](size_type pos) {
		return _first[pos];
	}

	size_type size() const {
		return _size;
	}

	iterator begin() const {
		return _first;
	}
	iterator end() const {
		return _last;
	}

	reverse_iterator rbegin() const {
		return reverse_iterator(_last);
	}
	reverse_iterator rend() const {
		return reverse_iterator(_first);
	}

	reference front() {
		if(!_size) throw TracedError_OutOfRange("No elements available.");
		return *_first;
	}

	reference back() {
		if(!_size) throw TracedError_OutOfRange("No elements available.");
		iterator back_ = _last; --back_;
		return *back_;
	}

public:
	ContainerView();
	ContainerView(iterator first, iterator last);
};

/**
 * Default constructor.
 *
 * Constructs a ContainerView from default constructed iterators. Which usually
 * corresponds to an empty range.
 **/

template<class IterType>
ContainerView<IterType, typename boost::enable_if<typename boost::is_same<typename std::iterator_traits<IterType>::iterator_category, std::random_access_iterator_tag>::type >::type>::ContainerView() :
		_first(), _last(), _size(std::distance(_first, _last)) {
}

/**
 * Constructor.
 *
 * Constructs a ContainerView to the range [first, last).
 **/

template<class IterType>
ContainerView<IterType, typename boost::enable_if<typename boost::is_same<typename std::iterator_traits<IterType>::iterator_category, std::random_access_iterator_tag>::type >::type>::ContainerView(iterator first, iterator last) :
		_first(first), _last(last), _size(std::distance(_first, _last)) {
}

} //namespace systematic

#endif //Systematic_ContainerView_H_
