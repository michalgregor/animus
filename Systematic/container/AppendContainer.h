#ifndef Systematic_AppendContainer_H_
#define Systematic_AppendContainer_H_

#include "../system.h"

namespace systematic {

/**
* @class AppendContainer
* @author Michal Gregor
*
* Acts as a wrapper for the specified Container type and allows for its easier
* in-place construction.
*
* AppendContainer features an implementation of operator[](), which appends
* a specified value to the end of the list and allows for chaining.
*
* \example To construct an AppendList with values 5, 7 and 8, and pass it to
* function foo(), one would do the following:
*
* \code
*	foo(systematic::AppendList()[5][7][8]);
* \endcode
*
* \note AppendContainer can be cast to Container& and const Container&.
*
* @tparam Container Type of the container to be wrapped-in. It should have
* at least the following interface:
* 	- Should contain a member type value_type;
* 	- Should implement a function push_back(const value_type& value), which
* 	  appends value to the end of the container;
* 	- Should be default-constructible;
* 	- Should have the following non-default constructor
* 	  Container(size_type n , value_type v), which creates a Container with
* 	  n copies of value v.
* 	- Should have method insert(iterator position, size_type n, value_type value),
* 	  which inserts n copies of value before the position specified by the iterator.
* 	- Should have methods begin() and end(), which return the begin and end
* 	  iterator respectively.
**/
template<class Container>
class AppendContainer {
public:
	typedef Container container_type;
	typedef typename Container::value_type value_type;
	typedef typename Container::size_type size_type;

private:
	friend class boost::serialization::access;

	/**
	* Boost serialization save function.
	**/
	void serialize(IArchive& ar, const unsigned int UNUSED(version)) {
		ar & _container;
	}

	/**
	* Boost serialization load function.
	**/
	void serialize(OArchive& ar, const unsigned int UNUSED(version)) {
		ar & _container;
	}

private:
	//! The underlying container.
	container_type _container;

public:
	/**
	 * Casts the AppendContainer to the underlying Container type.
	 */
	container_type& container() {
		return _container;
	}

	/**
	 * Casts the AppendContainer to the underlying Container type.
	 */
	const container_type& container() const {
		return _container;
	}

	/**
	 * Casts the AppendContainer to the underlying Container type.
	 */
	operator container_type&() {
		return _container;
	}

	/**
	 * Casts the AppendContainer to the underlying Container type.
	 */
	operator const container_type&() const {
		return _container;
	}

	/**
	 * Appends value to the end of the container.
	 * \return Returns *this so that calls to the operator can be chained.
	 */
	AppendContainer& operator[](const value_type& value) {
		_container.push_back(value);
		return *this;
	}

	/**
	 * Appends n copies of value to the end of the container.
	 * \return Returns *this so that calls to the operator can be chained.
	 */
	AppendContainer& operator()(size_type n, const value_type& value) {
		_container.insert(_container.end(), n, value);
		return *this;
	}

	/**
	 * Appends contents of AppendContainer cont.
	 * \return Returns *this so that calls to the operator can be chained.
	 */
	AppendContainer& operator()(const AppendContainer& cont) {
		_container.insert(_container.end(), cont._container.begin(), cont._container.end());
		return *this;
	}

public:
	/**
	 * Copy assignment operator.
	 */
	AppendContainer& operator=(const AppendContainer& obj) {
		_container = obj._container;
		return *this;
	}

	/**
	 * Move assignment operator.
	 */
	AppendContainer& operator=(AppendContainer&& obj) {
		_container = std::move(obj._container);
		return *this;
	}

	/**
	 * Copy constructor.
	 */
	AppendContainer(const AppendContainer& obj): _container(obj._container) {}

	/**
	 * Move constructor.
	 */
	AppendContainer(AppendContainer&& obj): _container(std::move(obj._container)) {	}

	/**
	 * Default constructor. Creates an empty AppendContainer.
	 */
	AppendContainer(): _container() {}

	/**
	 * Constructor. Creates a AppendContainer with n copies of value.
	 */
	AppendContainer(size_type n, const value_type& value): _container(n, value) {}

	/**
	 * Destructor.
	 */
	~AppendContainer() {
		SYS_EXPORT_INSTANCE()
	}
};

} //namespace systematic

SYS_EXPORT_TEMPLATE(systematic::AppendContainer, 1)

#endif //Systematic_AppendContainer_H_
