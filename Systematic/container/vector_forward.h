namespace annalyst {

/**
 * A macro that forwards vector-like interface to a class member.
 *
 * @param CLASS Type of the wrapping class.
 * @param VECTYPE Type of the wrapped vector.
 * @param MEMBER_NAME Name of the wrapped member.
 */
#define SYS_FORWARD_TO_VECTOR(CLASS, VECTYPE, MEMBER_NAME)					\
public:																		\
	typedef VECTYPE::value_type value_type;									\
	typedef VECTYPE::reference reference;									\
	typedef VECTYPE::const_reference const_reference;						\
																			\
	typedef VECTYPE::pointer pointer;										\
	typedef VECTYPE::const_pointer const_pointer;							\
	typedef VECTYPE::iterator iterator;										\
	typedef VECTYPE::const_iterator const_iterator;							\
	typedef VECTYPE::reverse_iterator reverse_iterator;						\
	typedef VECTYPE::const_reverse_iterator const_reverse_iterator;			\
	typedef VECTYPE::difference_type difference_type;						\
	typedef VECTYPE::size_type size_type;									\
																			\
public:																		\
	iterator begin() {return MEMBER_NAME.begin();}							\
	const_iterator begin() const {return MEMBER_NAME.begin();}				\
	const_iterator cbegin() const {return MEMBER_NAME.cbegin();}			\
																			\
	iterator end() {return MEMBER_NAME.end();}								\
	const_iterator end() const {return MEMBER_NAME.end();}					\
	const_iterator cend() const {return MEMBER_NAME.cend();}				\
																			\
	reverse_iterator rbegin() {return MEMBER_NAME.rbegin();}				\
	const_reverse_iterator rbegin() const {return MEMBER_NAME.rbegin();}	\
	const_reverse_iterator crbegin() const {return MEMBER_NAME.crbegin();}	\
																			\
	reverse_iterator rend() {return MEMBER_NAME.rend();}					\
	const_reverse_iterator rend() const {return MEMBER_NAME.rend();}		\
	const_reverse_iterator crend() const {return MEMBER_NAME.crend();}		\
																			\
public:																		\
	size_type size() const noexcept {return MEMBER_NAME.size();}			\
	size_type max_size() const noexcept {return MEMBER_NAME.max_size();}	\
																			\
	void resize(size_type n) {MEMBER_NAME.resize(n);}						\
	void resize(size_type n, const value_type& val) {						\
		MEMBER_NAME.resize(n, val);											\
	}																		\
																			\
	size_type capacity() const noexcept {return MEMBER_NAME.capacity();}	\
	bool empty() const noexcept {return MEMBER_NAME.empty();}				\
	void reserve(size_type n) {MEMBER_NAME.reserve(n);}						\
	void shrink_to_fit() {MEMBER_NAME.shrink_to_fit();}						\
																			\
public:																		\
	reference operator[] (size_type n) {return MEMBER_NAME[n];}				\
	const_reference operator[] (size_type n) const {return MEMBER_NAME[n];}	\
	reference at (size_type n) {return MEMBER_NAME.at(n);}					\
	const_reference at (size_type n) const {return MEMBER_NAME.at(n);}		\
																			\
	reference front() {return MEMBER_NAME.front();}							\
	const_reference front() const {return MEMBER_NAME.front();}				\
	reference back() {return MEMBER_NAME.back();}							\
	const_reference back() const {return MEMBER_NAME.back();}				\
																			\
	value_type* data() noexcept {return MEMBER_NAME.data();}				\
	const value_type* data() const noexcept {return MEMBER_NAME.data();}	\
																			\
public:																		\
	template <class InputIterator>											\
	void assign(InputIterator first, InputIterator last) {					\
		MEMBER_NAME.assign(first, last);									\
	}																		\
																			\
	void assign(size_type n, const value_type& val)							\
		{MEMBER_NAME.assign(n, val);}										\
	void assign(std::initializer_list<value_type> il)						\
		{MEMBER_NAME.assign(il);}											\
																			\
	void push_back(const value_type& val)									\
		{MEMBER_NAME.push_back(val);}										\
	void push_back(value_type&& val)										\
		{MEMBER_NAME.push_back(std::move(val));}							\
																			\
	void pop_back(){MEMBER_NAME.pop_back();}								\
																			\
public:																		\
	iterator insert(iterator position, const value_type& val) {				\
		return MEMBER_NAME.insert(position, val);							\
	}																		\
																			\
	void insert(															\
		iterator position, size_type n, const value_type& val)				\
	{																		\
		MEMBER_NAME.insert(position, n, val);								\
	}																		\
																			\
	template <class InputIterator>											\
	iterator insert(														\
		iterator position, InputIterator first, InputIterator last) 		\
	{																		\
		return MEMBER_NAME.insert(position, first, last);					\
	}																		\
																			\
	iterator insert(iterator position, value_type&& val) {					\
		return MEMBER_NAME.insert(position, val);							\
	}																		\
																			\
	void insert(															\
		iterator position, std::initializer_list<value_type> il)			\
	{																		\
		MEMBER_NAME.insert(position, il);									\
	}																		\
																			\
	iterator erase(iterator position) {										\
		return MEMBER_NAME.erase(position);									\
	}																		\
																			\
	void erase(iterator first, iterator last) {								\
		MEMBER_NAME.erase(first, last);										\
	}																		\
																			\
	void swap(CLASS& obj) {MEMBER_NAME.swap(obj.MEMBER_NAME);}				\
	void clear() noexcept {MEMBER_NAME.clear();}							\
																			\
public:																		\
	template <class... Args>												\
	iterator emplace(iterator position, Args&&... args) {					\
		return MEMBER_NAME.emplace(position, args...);						\
	}																		\
																			\
	template <class... Args>												\
	void emplace_back(Args&&... args) {										\
		MEMBER_NAME.emplace_back(args...);									\
	}

/**
 * A macro that forwards vector-like constructors to a class member.
 *
 * @param CLASS Type of the wrapping class.
 * @param MEMBER_NAME Name of the wrapped member.
 */
#define SYS_FORWARD_TO_VECTOR_CONSTRUCTORS(CLASS, MEMBER_NAME)							\
public:																					\
	CLASS& operator=(const CLASS& x) {MEMBER_NAME = x.MEMBER_NAME; return *this;}		\
	CLASS& operator=(CLASS&& x) {MEMBER_NAME = std::move(x.MEMBER_NAME); return *this;}	\
	CLASS& operator=(std::initializer_list<value_type> il) {							\
		MEMBER_NAME = il; return *this;													\
	}																					\
																						\
	explicit CLASS(size_type n): MEMBER_NAME(n) {}										\
	CLASS(size_type n, const value_type& val): MEMBER_NAME(n, val) {}					\
																						\
	template <class InputIterator>														\
	CLASS(InputIterator first, InputIterator last): MEMBER_NAME(first, last) {}			\
																						\
	CLASS(const CLASS& x): MEMBER_NAME(x.MEMBER_NAME) {}								\
	CLASS(CLASS&& x): MEMBER_NAME(std::move(x.MEMBER_NAME)) {}							\
	CLASS(std::initializer_list<value_type> il): MEMBER_NAME(il) {}

} //namespace annalyst
