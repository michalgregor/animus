#ifndef SYS_VECTORSTREAMOPERATOR_H_INCLUDED
#define SYS_VECTORSTREAMOPERATOR_H_INCLUDED

#include <vector>
#include <Systematic/configure.h>

namespace std {

/**
 * Definition of the ostream operator<< for vector. It prints the individual
 * items enclosed in braces and separated by commas, e.g. {1, 2, 3}.
 */
template<class Type>
std::ostream& operator<<(std::ostream& os, const std::vector<Type>& vec) {
	os << "{";

	if(vec.size()) {
		auto iter = vec.begin(), iend = vec.end() - 1;

		for(; iter != iend; iter++) {
			os << *iter << ", ";
		}

		os << vec.back();
	}

	os << "}";

	return os;
}

} //namespace std

#endif // SYS_VECTORSTREAMOPERATOR_H_INCLUDED
