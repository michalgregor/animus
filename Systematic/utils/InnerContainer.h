#ifndef SYS_INNERCONTAINER_H_INCLUDED
#define SYS_INNERCONTAINER_H_INCLUDED

#include "../system.h"

namespace systematic {

/**
* This is used to wrap data that are normally untracked, such as ints,
* floats etc. so that the serialization works nicely. As Variable is used
* as a handle for the contained data it is vital that it is tracked.
**/

template<class Type>
class InnerContainer {
private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & data;
	}

public:
	Type data;
	InnerContainer(): data() {}
	InnerContainer(const Type& in): data(in) {}
};

} //namespace systematic

SYS_REGISTER_TEMPLATENAME(systematic::InnerContainer, 1)

#endif // SYS_INNERCONTAINER_H_INCLUDED
