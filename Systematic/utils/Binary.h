#ifndef SYS_BINARY_H_INCLUDED
#define SYS_BINARY_H_INCLUDED

#include "../system.h"
#include <vector>

namespace systematic {

	void SYS_API BinarySwap(char& a, char& b, unsigned int pos1, unsigned int pos2);

	void SYS_API BinarySwap(char* a, char* b, unsigned int pos1, unsigned int pos2);

	template<class Type>
	inline void BinarySwap(Type& a, Type& b, unsigned int pos1, unsigned int pos2) {
		BinarySwap(reinterpret_cast<char*>(&a), reinterpret_cast<char*>(&b), pos1, pos2);
	}

	SYS_API std::vector<bool> LongToBinary(unsigned long val);
	SYS_API std::vector<bool> LongToBinary(unsigned long val, unsigned int num);

	SYS_API unsigned long BinaryToLong(const std::vector<bool>& bits);

} //namespace systematic

#endif // SYS_BINARY_H_INCLUDED
