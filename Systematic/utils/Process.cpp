#include "Process.h"

#ifdef _WIN32
	#include <stdio.h>
	#define sys_popen _popen
	#define sys_pclose _pclose
#else
	#define sys_popen popen
	#define sys_pclose pclose
#endif

namespace systematic {

//--------------------------------ProcessIn-------------------------------------

/**
* Runs the specified process and retrieves the corresponding file pointer.
**/

void ProcessIn::open(std::string cmd) {
	close();
	_process = sys_popen(cmd.c_str(), "w");
	if(!_process) throw TracedError_Generic("The attempt to run the specified process was unsuccessful.");
}

/**
* Closes the process file.
**/

void ProcessIn::close() {
	if(_process) sys_pclose(_process);
	_process = NULL;
}

/**
* Writes in into the stdin of the process.
**/

ProcessIn& ProcessIn::operator << (std::string in) {
	if(!_process) throw TracedError_InvalidPointer("Process file not open.");
	fprintf(_process, "%s", in.c_str());
	fflush(_process);
	return *this;
}

/**
* ProcessIn constructor.
**/

ProcessIn::ProcessIn(): _process(NULL) {}

/**
* ProcessIn constructor. Calls open(std::string).
**/

ProcessIn::ProcessIn(std::string cmd):_process(NULL) {
	open(cmd);
}

/**
* ProcessIn destructor.
**/

ProcessIn::~ProcessIn() {
	close();
}

//--------------------------------ProcessOut------------------------------------

/**
* Reads from stdout of the process and writes the result into out.
**/

std::string& ProcessOut::operator >> (std::string& out) {
	if(!_process) throw TracedError_InvalidPointer("Process file not open.");
	char c;

	while(c = static_cast<char>(fgetc(_process)), (c != '\n' && c != EOF)) {
		out.push_back(c);
	}

	return out;
}

/**
* Runs the specified process and retrieves the corresponding file pointer.
**/

void ProcessOut::open(std::string cmd) {
	close();
	_process = sys_popen(cmd.c_str(), "r");
	if(!_process) throw TracedError_InvalidPointer("The attempt to run the specified process was unsuccessful.");
}

/**
* Closes the process file.
**/

void ProcessOut::close() {
	if(_process) sys_pclose(_process);
	_process = NULL;
}

/**
* ProcessIn default constructor.
**/

ProcessOut::ProcessOut(): _process(NULL) {}

/**
* ProcessIn constructor. Calls open(std::string).
**/

ProcessOut::ProcessOut(std::string cmd): _process(NULL) {
	open(cmd);
}

/**
* ProcessIn destructor.
**/

ProcessOut::~ProcessOut() {
	close();
}

} //namespace systematic
