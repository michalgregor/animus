#ifndef Systematic_rand_H_
#define Systematic_rand_H_

#include <functional>

namespace systematic {

/**
 * Returns an std::function, which behaves as a well-seeded thread-safe rand()
 * function.
 */
std::function<int(void)> make_rand();

} //namespace systematic

#endif //Systematic_rand_H_
