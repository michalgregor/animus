#ifndef STORAGETOKEN_H_
#define STORAGETOKEN_H_

#include "../system.h"
#include "../detail/EmptyObject.h"

namespace systematic {

//forward declarations
class StorageToken;
SYS_API std::ostream& operator<<(std::ostream& os, const systematic::StorageToken& token);

/**
* @class StorageToken
* @author Michal Gregor
*
* A class used to implement unique IDs. Every StorageToken contains a pointer
* to an EmptyObject - it is the address that is used as an id. This way it is
* also guaranteed that while at least one StorageToken with the id exists no
* newly created StorageToken can claim the id (a dynamically allocated object
* still exists in that location).
*
* The tokens can be compared using the provided comparison operators. Thus, they
* can easily be used with sorted containers such as map. Count of existing
* instances of StorageToken with the same ID can also be retrieved.
**/
class SYS_API StorageToken {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	friend SYS_API std::ostream& operator<<(std::ostream& os, const systematic::StorageToken& token);

	//! A pointer shared by all instances of the same token: used for comparison checks.
	intrusive_ptr<EmptyObject> _token;

public:
	bool operator==(const StorageToken& token) const {return _token.get() == token._token.get();}
	bool operator!=(const StorageToken& token) const {return _token.get() != token._token.get();}
	bool operator<(const StorageToken& token) const {return _token.get() < token._token.get();}
	bool operator<=(const StorageToken& token) const {return _token.get() <= token._token.get();}
	bool operator>(const StorageToken& token) const {return _token.get() > token._token.get();}
	bool operator>=(const StorageToken& token) const {return _token.get() >= token._token.get();}

public:
	//! Returns the number for StorageTokens associated with the particular storage entry.
	long int count() const {return _token->count();}

	//! Returns true if the token has been reset (NULL ptr).
	bool isValid() const {return _token.get();}

	//! Resets the token, thus making the StorageToken invalid.
	void reset() {_token.reset();}

	StorageToken& operator=(const StorageToken& token);
	StorageToken(const StorageToken& token);

	StorageToken(bool valid);
	StorageToken();
};

} //namespace systematic

SYS_EXPORT_CLASS(systematic::StorageToken)

#endif /* STORAGETOKEN_H_ */
