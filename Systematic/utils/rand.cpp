#include "rand.h"

#include <random>
#include <cstdlib>
#include <Systematic/generator/RandEngine.h>

namespace systematic {

std::function<int(void)> make_rand() {
	return std::bind(std::uniform_int_distribution<int>(1, RAND_MAX), DefaultRandEngine(RandSeed()));
}

} //namespace systematic
