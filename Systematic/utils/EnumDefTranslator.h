#ifndef Systematic_EnumDefTranslator_H_
#define Systematic_EnumDefTranslator_H_

#include <string>
#include <locale>
#include <boost/optional.hpp>
#include <sstream>

#include "enumdef.h"

namespace systematic {

/**
 * @class enumdefTranslator
 *
 * This is a boost::property_tree Translator implementation for enums with
 * SYS_ENUMDEF() support (see enumdef.h).
 */
template<class EnumType>
class EnumDefTranslator {
public:
    typedef std::string  internal_type;
    typedef EnumType     external_type;

    boost::optional<external_type> get_value(internal_type const &v);
    boost::optional<internal_type> put_value(external_type const& v);
};

template<class EnumType>
boost::optional<typename EnumDefTranslator<EnumType>::external_type>
EnumDefTranslator<EnumType>::get_value(internal_type const& v) {
	try {
		return str2enum<external_type>(v);
	} catch(TracedError_NotAvailable& err) {
		return boost::none;
	}
}

template<class EnumType>
boost::optional<typename EnumDefTranslator<EnumType>::internal_type>
EnumDefTranslator<EnumType>::put_value(external_type const& v) {
    return enum2str(v);
}

} //namespace systematic

#endif //Systematic_EnumDefTranslator_H_
