#ifndef Systematic_tuple_for_each_H_
#define Systematic_tuple_for_each_H_

#include <tuple>
#include <utility>

namespace systematic {

template<std::size_t I = 0, typename FuncT, typename ... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
for_each(std::tuple<Tp...> &, FuncT) {}

template<std::size_t I = 0, typename FuncT, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
for_each(std::tuple<Tp...>& t, FuncT f) {
	f(std::get<I>(t));
	for_each<I + 1, FuncT, Tp...>(t, f);
}

template<std::size_t I = 0, typename FuncT, typename ... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
for_each_i(std::tuple<Tp...> &, FuncT) {}

template<std::size_t I = 0, typename FuncT, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
for_each_i(std::tuple<Tp...>& t, FuncT f) {
	f(std::get<I>(t), I);
	for_each_i<I + 1, FuncT, Tp...>(t, f);
}

template<std::size_t I = 0, typename FuncT, typename ... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
for_each(const std::tuple<Tp...>&, FuncT) {}

template<std::size_t I = 0, typename FuncT, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
for_each(const std::tuple<Tp...>& t, FuncT f) {
	f(std::get<I>(t));
	for_each<I + 1, FuncT, Tp...>(t, f);
}

template<std::size_t I = 0, typename FuncT, typename ... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
for_each_i(const std::tuple<Tp...> &, FuncT) {}

template<std::size_t I = 0, typename FuncT, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
for_each_i(const std::tuple<Tp...>& t, FuncT f) {
	f(std::get<I>(t), I);
	for_each_i<I + 1, FuncT, Tp...>(t, f);
}

} //namespace systematic

#endif //Systematic_tuple_for_each_H_
