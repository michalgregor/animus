#ifndef Systematic_enumdef_H_
#define Systematic_enumdef_H_

#include "../macros.h"
#include "../exception/TracedError.h"
#include "../utils/ToString.h"
#include "../utils/EnumStreamOperator.h"

#include <map>
#include <initializer_list>
#include <string>

#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>

namespace systematic {

/**
 * A macro used to define enum class in such way that it is possible to convert
 * between numeric and string representations of its value using str2enum and
 * enum2str.
 * @param NAME Name of the enum class type.
 * @param ENUM_VALS Values of the enum formatted as a boost sequence.
 *
 * @example An enum class Color with values red, greed and blue can be defined
 * as follows: SYS_ENUMDEF(Color, (red)(green)(blue));
 */
#define SYS_ENUMDEF(NAME, ENUM_VALS)										\
enum class NAME {															\
	BOOST_PP_SEQ_FOR_EACH(AUXSYS_ENUMITEM, NONE, ENUM_VALS) 				\
};																			\
SYS_ENUMWRAP(NAME, ENUM_VALS)

/**
 * A macro used to wrap existing enum classes in such way that it is possible
 * to convert between numeric and string representations of its value using
 * str2enum and enum2str. Alternatively, the enum class can be defined using
 * SYS_ENUMDEF so that one does not have to list its values multiple times.
 * @param NAME Name of the enum class type.
 * @param ENUM_VALS Values of the enum formatted as a boost sequence.
 *
 * @example An enum class Color with values red, greed and blue can be defined
 * as follows: SYS_ENUMDEF(Color, (red)(green)(blue));
 */
#define SYS_ENUMWRAP(NAME, ENUM_VALS)										\
template<class DUMMY = void>												\
std::map<std::string, NAME>& SYS_GET_ENUM_MAP(NAME) {						\
	static std::map<std::string, NAME> enumMap = {							\
	BOOST_PP_SEQ_FOR_EACH(AUXSYS_ENUMSTRING, NAME, ENUM_VALS)				\
	};																		\
	return enumMap;															\
}																			\
template<class DUMMY = void>												\
const char* SYS_GET_ENUM_STR(NAME NAME##val) {								\
	switch(NAME##val)	{													\
	BOOST_PP_SEQ_FOR_EACH(AUXSYS_ENUMSVAL, NAME, ENUM_VALS)					\
	default:																\
		throw systematic::TracedError_NotAvailable(							\
			"Unknown enum value '" + systematic::ToString(NAME##val) + "'.");\
	}																		\
}

// Auxiliary macros.
#define AUXSYS_ENUMITEM(r, data, elem) elem BOOST_PP_COMMA_IF(r)
#define AUXSYS_ENUMSTRING(r, NAME, elem) {SYS_STRINGIFY(elem), NAME::elem} BOOST_PP_COMMA_IF(r)
#define AUXSYS_ENUMSVAL(r, NAME, elem) case NAME::elem: return SYS_STRINGIFY(elem);

/**
 * A function that can turn a string representation of enum value into its
 * numeric representation. As a prerequisite, the corresponding type must
 * be defined using the SYS_ENUMDEF macro, or wrapped using SYS_ENUMWRAP.
 * @param str The string representation of enum value to be converted.
 * @exception TracedError_NotAvailable An exception is thrown if str does not
 * correspond to any existing value.
 */
template<class EnumType>
EnumType str2enum(const std::string& str) {
	std::map<std::string, EnumType>& enumMap = SYS_GET_ENUM_MAP(EnumType{});
	auto iter = enumMap.find(str);

	if(iter == enumMap.end()) throw TracedError_NotAvailable("Unknown enum value name '" + str + "'.");

	return iter->second;
}

/**
 * A function that can turn a numeric representation of enum value into its
 * string representation. As a prerequisite, the corresponding type must
 * be defined using the SYS_ENUMDEF macro, or wrapped using SYS_ENUMWRAP.
 * @param en The numeric representation of enum value to be converted.
 * @exception TracedError_NotAvailable An exception is thrown if en does not
 * correspond to any existing value.
 */
template<class EnumType>
inline const char* enum2str(EnumType en) {
	return SYS_GET_ENUM_STR(en);
}

} //namespace systematic

#endif //Systematic_enumdef_H_
