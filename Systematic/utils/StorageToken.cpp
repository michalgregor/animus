#include "StorageToken.h"

namespace systematic {

/**
 * Boost serialization.
 */

void StorageToken::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _token;
}

void StorageToken::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _token;
}

/**
* Assignment operator.
*/

StorageToken& StorageToken::operator=(const StorageToken& token) {
	if(this != &token) {
		_token = token._token;
	}
	return *this;
}

/**
* Copy constructor.
*/

StorageToken::StorageToken(const StorageToken& token): _token(token._token) {}

/**
 * Constructor.
 * @param valid Specifies whether the newly created token is to be valid
 * (non-NULL).
 */

StorageToken::StorageToken(bool valid):
	_token((valid)?(new EmptyObject):(NULL)) {}

/**
* Default constructor.
*/

StorageToken::StorageToken(): _token(new EmptyObject) {}

/**
* Overload of ostream operator << for StorageToken. It prints
* StorageToken($addr), where $addr is the address used as the unique id.
**/

std::ostream& SYS_API operator<<(std::ostream& os, const systematic::StorageToken& token) {
	os << "StorageToken" << token._token << std::endl;
	return os;
}

} //namespace systematic
