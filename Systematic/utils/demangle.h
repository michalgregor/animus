#ifndef SYS_DEMANGLE_H_INCLUDED
#define SYS_DEMANGLE_H_INCLUDED

#if (__GNUC__ && __cplusplus && __GNUC__ >= 3)
#include <cxxabi.h>
#endif // __GNUC__

#include <boost/algorithm/string.hpp>
#include <Systematic/configure.h>

namespace systematic {

/**
* Demangle wrapper function borrowed from Boost.
**/

SYS_API inline std::string demangle(const char* name) {
	#if (__GNUC__ && __cplusplus && __GNUC__ >= 3)
	// need to demangle C++ symbols
	char*       realname;
	std::size_t len;
	int         stat;

	realname = abi::__cxa_demangle(name,NULL,&len,&stat);

	if (realname != NULL) {
		std::string out(realname);

		std::free(realname);
		boost::replace_all(out,"boost::units::","");

		return out;
	}

	return std::string("demangle :: error - unable to demangle specified symbol");
	#else
	return name;
	#endif
}

} //namespace systematic

#endif // SYS_DEMANGLE_H_INCLUDED
