#ifndef Systematic_function_H_
#define Systematic_function_H_

#if __cplusplus < 201103L
	#include <boost/function.hpp>
	namespace systematic {using boost::function;}
#else
	#include <functional>
	namespace systematic {using std::function;}
#endif

#endif //Systematic_function_H_
