#include "RefCounterVirtual.h"

namespace systematic {

/**
* Empty serialization function (reference count should not be saved it will
* be reconstructed by deserializing the intrusive ptrs.
**/

void RefCounterVirtual::serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

void RefCounterVirtual::serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

} //namespace systematic
