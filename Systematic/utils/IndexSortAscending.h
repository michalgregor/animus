#ifndef SYS_INDEXSORTASCENDING_H_INCLUDED
#define SYS_INDEXSORTASCENDING_H_INCLUDED

#include <boost/utility.hpp>
#include <boost/type_traits/is_reference.hpp>
#include <Systematic/configure.h>

namespace systematic {

	/**
* @class IndexSortAscending
* This class serves as a comparer for std::sort. It is used when
* indexes of vector A's elements are stored in vector B and need to be
* sorted so that A[B[i]] < A[B[i+1]]; 0 <= i < A.size().
**/

template<class T, class Enable = void>
class IndexSortAscending {
private:
	//! Reference to array A.
	const T& _A;

public:
	bool operator()(const unsigned int b1, const unsigned int b2) const;
	IndexSortAscending(const T& A);
};

/**
* Returns whether A[b1] < A[b2], where b1 a and b2 are elements of array B
* (see class description).
**/

template<class T, class Enable>
bool IndexSortAscending<T, Enable>::
operator()(const unsigned int b1, const unsigned int b2) const {
	return _A[b1] < _A[b2];
}

/**
* Constructor.
* @param A Array A (see class description).
**/

template<class T, class Enable>
IndexSortAscending<T, Enable>::IndexSortAscending(const T& A): _A(A) {}

/**
* @class IndexSortAscending
* This class serves as a comparer for std::sort. It is used when
* indexes of vector A's elements are stored in vector B and need to be
* sorted so that A[B[i]] < A[B[i+1]]; 0 <= i < A.size().
*
* Specialization for T&.
**/

template<class T>
class IndexSortAscending<T, typename boost::enable_if<boost::is_reference<T> >::type> {
	private:
		const T _A;

	public:
		bool operator()(const unsigned int b1, const unsigned int b2) const;
		IndexSortAscending(const T A);
};

/**
* Returns whether A[b1] < A[b2], where b1 a and b2 are elements of array B
* (see class description).
**/

template<class T>
bool IndexSortAscending<T, typename boost::enable_if<boost::is_reference<T> >::type>::
operator()(const unsigned int b1, const unsigned int b2) const {
	return _A[b1] < _A[b2];
}

/**
* Constructor.
* @param A Array A (see class description).
**/

template<class T>
IndexSortAscending<T, typename boost::enable_if<boost::is_reference<T> >::type>::
IndexSortAscending(const T A): _A(A) {}

} //namespace systematic

#endif // SYS_INDEXSORTASCENDING_H_INCLUDED
