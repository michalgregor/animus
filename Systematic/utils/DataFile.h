#ifndef Systematic_DataFile_H_
#define Systematic_DataFile_H_

#include <vector>
#include <map>
#include <utility>
#include <string>
#include <iostream>

#include "../system.h"

namespace systematic {

/**
* @class DataFile
* @author Michal Gregor
*
* A class used to load and save plain text data files consisting of data in
* three sections: a header (the first non-empty and non-commented [using #]
* line); options and their values (option names being separated from values
* using colons); and data (the first data line is the first non-empty,
* and non-commented line that has no colons).
*
* The three sections must be contiguous and occur in the specified order;
* they are not allowed to be interleaved. The header line is compulsory.
**/

class SYS_API DataFile {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	typedef std::map<std::string, std::string> OptionContainer;
	typedef OptionContainer::value_type Option;
	typedef std::vector<std::string> DataContainer;

private:
	//! Stores the header of the DataFile.
	std::string _header;
	//! Stores data as pairs of strings containing the option and the value
	//! respectively.
	OptionContainer _options;
	//! Stores the data lines.
	DataContainer _data;

private:
	static void trimComment(std::string& line) {
		size_t pos = line.find('#');
		line = line.substr(0, pos);
	}

public:
	//! Returns a reference to the header.
	std::string& header() {return _header;}
	//! Returns a reference to the header.
	const std::string& header() const {return _header;}

	//! Returns a reference to the OptionContainer.
	OptionContainer& options() {return _options;}
	//! Returns a reference to the OptionContainer.
	const OptionContainer& options() const {return _options;}

	//! Returns a reference to the DataContainer.
	DataContainer& data() {return _data;}
	//! Returns a reference to the DataContainer.
	const DataContainer& data() const {return _data;}

	void load(std::istream& stream);
	void save(std::ostream& stream) const;

public:
	DataFile(): _header(), _options(), _data() {}
};

#ifndef SWIG

/**
 * Writes the DataFile to the specified stream (has the same effect as calling
 * obj.save(stream)).
 */

inline std::ostream& operator<<(std::ostream& stream, const DataFile& obj) {
	obj.save(stream);
	return stream;
}

/**
 * Reads the DataFile from the specified stream (has the same effect as calling
 * obj.load(stream)).
 */

inline std::istream& operator>>(std::istream& stream, DataFile& obj) {
	obj.load(stream);
	return stream;
}

#endif //#ifndef SWIG

}//namespace systematic

SYS_EXPORT_CLASS(systematic::DataFile)

#endif //Systematic_DataFile_H_
