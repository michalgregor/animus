#ifndef Systematic_Singleton_H_
#define Systematic_Singleton_H_

#include <Systematic/configure.h>
#include "../exception/assert.h"
#include <boost/noncopyable.hpp>

namespace systematic {
namespace detail {

class SYS_API singleton_module: public boost::noncopyable {
private:
    static bool& get_lock() {
        static bool lock = false;
        return lock;
    }
public:
    static void lock() {
        get_lock() = true;
    }
    static void unlock( ){
        get_lock() = false;
    }
    static bool is_locked() {
        return get_lock();
    }
};

template<class T>
class singleton_wrapper: public T {
public:
    static bool m_is_destroyed;

    ~singleton_wrapper(){
        m_is_destroyed = true;
    }
};

template<class T>
bool detail::singleton_wrapper< T >::m_is_destroyed = false;

} //namespace detail

template <class T>
class Singleton: public detail::singleton_module {
private:
    static T& instance;

    // include this to provoke instantiation at pre-execution time
    static void use(T const &) {}

    static T& get_instance() {
        static detail::singleton_wrapper<T> t;
        // refer to instance, causing it to be instantiated (and
        // initialized at startup on working compilers)
        SYS_ALWAYS_ASSERT(! detail::singleton_wrapper< T >::m_is_destroyed);
        use(instance);
        return static_cast<T &>(t);
    }

public:
    static T & get_mutable_instance(){
    	SYS_ALWAYS_ASSERT(! is_locked());
        return get_instance();
    }

    static const T & get_const_instance(){
        return get_instance();
    }

    static bool is_destroyed(){
        return detail::singleton_wrapper< T >::m_is_destroyed;
    }
};

template <class T>
T& Singleton<T>::instance = Singleton<T>::get_instance();

} //namespace systematic

#endif //Systematic_Singleton_H_
