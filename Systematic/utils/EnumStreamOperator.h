#ifndef ENUMSTREAMOPERATOR_H_
#define ENUMSTREAMOPERATOR_H_

#include <iostream>
#include <type_traits>

namespace std {

/**
 * Specialization of the istream >> operator for enums. Unsigned int is used
 * as the integer representation.
 */
template<class EnumType>
typename std::enable_if<std::is_enum<EnumType>::value, std::istream&>::type
operator>>(std::istream& in, EnumType& en) {
	unsigned int enumVal = 0;

	in >> enumVal;
	en = static_cast<EnumType>(enumVal);

	return in;
}

/**
 * Specialization of the ostream << operator for enums. Unsigned int is used
 * as the integer representation.
 */
template<class EnumType>
typename std::enable_if<std::is_enum<EnumType>::value, std::ostream&>::type
operator<<(std::ostream& out, EnumType& en) {
	out << static_cast<unsigned int>(en);
	return out;
}

} //namespace std

#endif /* ENUMSTREAMOPERATOR_H_ */
