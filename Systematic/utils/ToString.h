#ifndef SYS_TOSTRING_H_INCLUDED
#define SYS_TOSTRING_H_INCLUDED

#include <string>
#include <sstream>
#include <Systematic/configure.h>

namespace systematic {

/**
* Turns DataType into string. The DataType in question must have the
* ostream << operator implemented.
**/
template<typename DataType>
std::string ToString(const DataType& data) {
	std::stringstream stream;
	stream << data;
	return stream.str();
}

} //namespace systematic

#endif // SYS_TOSTRING_H_INCLUDED
