#ifndef SYS_RefCounterVirtual_H_INCLUDED
#define SYS_RefCounterVirtual_H_INCLUDED

#include <boost/smart_ptr/detail/atomic_count.hpp>
#include "../system.h"

namespace systematic {

/**
* @class RefCounterVirtual
* This may be used as a base class providing reference count for intrusive
* shared pointers. This may not be perfectly optimal for classes that do not
* already have a table of virtual functions as this adds one. Consider
* re-implemeting this for such classes.
**/

class SYS_API RefCounterVirtual {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The reference counter itself.
	boost::detail::atomic_count refcount;

	friend inline void intrusive_ptr_add_ref(RefCounterVirtual* obj);
	friend inline void intrusive_ptr_release(RefCounterVirtual* obj);

public:
	/**
	* Returns the current number of references to the object. 1 means that there is
	* only a single pointer to the object. If count is zero there are no intrusive
	* pointers pointing to the object.
	**/
	long int count() {return refcount;}

	//! Returns if reference count is not greater than 1.
	bool unique() {return refcount <= 1;}

	RefCounterVirtual& operator=(const RefCounterVirtual&) {return *this;}
	RefCounterVirtual(): refcount(0) {}
	RefCounterVirtual(const RefCounterVirtual&): refcount(0) {}
	virtual ~RefCounterVirtual() {}
};

//! Required by boost::intrusive_ptr. Increases the reference count.
inline void intrusive_ptr_add_ref(RefCounterVirtual* obj) {
	++(obj->refcount);
}

//! Required by boost::intrusive_ptr. Decreases the reference count and deletes
//! the object when appropriate.
inline void intrusive_ptr_release(RefCounterVirtual* obj) {
	if(obj->refcount <=1) delete obj;
	else --(obj->refcount);
}

} //namespace systematic

SYS_EXPORT_CLASS(systematic::RefCounterVirtual)

#endif // SYS_RefCounterVirtual_H_INCLUDED
