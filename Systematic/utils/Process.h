#ifndef SYS_PROCESS_H_INCLUDED
#define SYS_PROCESS_H_INCLUDED

#include <string>
#include <cstdio>
#include <fstream>
#include "../system.h"

namespace systematic {

/**
* @class ProcessIn
*
* @brief This class provides an OS-independent wrapper that allows to run
* a process and write into its stdin.
**/

class SYS_API ProcessIn {
private:
	ProcessIn(const ProcessIn&);
	ProcessIn& operator=(const ProcessIn&);

	FILE* _process;

public:
	ProcessIn& operator << (std::string in);

	void open(std::string cmd);
	void close();

	ProcessIn();
	ProcessIn(std::string cmd);
	~ProcessIn();
};

/**
* @class ProcessOut
*
* @brief This class provides an OS-independent wrapper that allows to run
* a process and read from its stdout.
**/

class SYS_API ProcessOut {
private:
	ProcessOut(const ProcessOut&);
	ProcessOut& operator=(const ProcessOut&);

	FILE* _process;

public:
	std::string& operator >> (std::string& out);

	void open(std::string cmd);
	void close();

	ProcessOut();
	ProcessOut(std::string cmd);
	~ProcessOut();
};

} //namespace systematic

#endif // SYS_PROCESS_H_INCLUDED
