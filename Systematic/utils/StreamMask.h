#ifndef Systematic_StreamMask_H_
#define Systematic_StreamMask_H_

#include "../system.h"
#include <iostream>
#include <cstring>
#include <string>

namespace systematic {

/**
* @class StreamMask
* @author Michal Gregor
*
* A class that can be used with an istream to check whether a certain part
* of input conforms to a specified mask – a Type object is read from the stream
* and compared with the Type object stored in the StreamMask.
*
* @param Greedy If set to true, the object can be read from the stream using
* operator>>(), otherwise only as many characters are to be consumed, as
* necessary for comparison (e.g. size of the stored string).
**/
template<class Type, bool Greedy = false>
class StreamMask {
private:
	//! Stores a copy of the object used for comparison.
	Type _obj;

public:
	/**
	 * Compares an object read from the stream with the object stored in the
	 * StreamMask. Returns true if equal and false otherwise.
	 */
	bool equals(std::istream& stream) const {
		Type obj; stream >> obj;
		return obj == _obj;
	}

	/**
	* Assignment operator. Copies the underlying object.
	**/
	StreamMask<Type>& operator=(const StreamMask<Type>& obj) {
		_obj = obj._obj;
		return *this;
	}

	/**
	* Copy constructor.
	**/
	StreamMask(const StreamMask<Type>& obj): _obj(obj._obj) {}

	/**
	* Constructor.
	**/
	StreamMask(const Type& obj): _obj(obj) {}
};

/**
 * Specialisation for a C-style string.
 */
template<bool Greedy>
class StreamMask<char*, Greedy> {
private:
	//! Stores a copy of the object used for comparison.
	char* _obj;

public:
	/**
	 * Compares an object read from the stream with the object stored in the
	 * StreamMask. Returns true if equal and false otherwise.
	 *
	 * The string is read character by character. Once all characters of the
	 * stored string have been used up, no further characters are consumed from
	 * the stream.
	 */
	bool equals(std::istream& stream) const {
		for(size_t i = 0; _obj[i] != '\0'; i++) {
			if(!stream.good()) return false;
			if(stream.get() != _obj[i]) return false;
		}

		return true;
	}

	/**
	* Assignment operator. Copies the underlying object.
	**/
	StreamMask& operator=(const StreamMask& obj) {
		_obj = obj._obj;
		return *this;
	}

	/**
	* Copy constructor.
	**/
	StreamMask(const StreamMask& obj): _obj(obj._obj) {}

	/**
	* Constructor.
	**/
	StreamMask(char* obj): _obj(obj) {}
};

/**
 * Compares an object read from the stream with the object stored in the
 * StreamMask. Returns true if equal and false otherwise. This is the greedy
 * version, which uses operator>> to read from the stream.
 */
template<>
inline bool StreamMask<char*, true>::equals(std::istream& stream) const {
	std::string obj; stream >> obj;
	return !strcmp(obj.c_str(), _obj);
}

/**
 * Specialisation for a C-style string.
 */
template<bool Greedy>
class StreamMask<const char*, Greedy> {
private:
	//! Stores a copy of the object used for comparison.
	const char* _obj;

public:
	/**
	 * Compares an object read from the stream with the object stored in the
	 * StreamMask. Returns true if equal and false otherwise.
	 *
	 * The string is read character by character. Once all characters of the
	 * stored string have been used up, no further characters are consumed from
	 * the stream.
	 */
	bool equals(std::istream& stream) const {
		for(size_t i = 0; _obj[i] != '\0'; i++) {
			if(!stream.good()) return false;
			if(stream.get() != _obj[i]) return false;
		}

		return true;
	}

	/**
	* Assignment operator. Copies the underlying object.
	**/
	StreamMask& operator=(const StreamMask& obj) {
		_obj = obj._obj;
		return *this;
	}

	/**
	* Copy constructor.
	**/
	StreamMask(const StreamMask& obj): _obj(obj._obj) {}

	/**
	* Constructor.
	**/
	StreamMask(const char* obj): _obj(obj) {}
};

/**
 * Compares an object read from the stream with the object stored in the
 * StreamMask. Returns true if equal and false otherwise. This is the greedy
 * version, which uses operator>> to read from the stream.
 */
template<>
inline bool StreamMask<const char*, true>::equals(std::istream& stream) const {
	std::string obj; stream >> obj;
	return !strcmp(obj.c_str(), _obj);
}

/**
 * Specialisation for an std::string.
 */
template<bool Greedy>
class StreamMask<std::string, Greedy> {
private:
	//! Stores a copy of the object used for comparison.
	std::string _obj;

public:
	/**
	 * Compares an object read from the stream with the object stored in the
	 * StreamMask. Returns true if equal and false otherwise.
	 *
	 * The string is read character by character. Once all characters of the
	 * stored string have been used up, no further characters are consumed from
	 * the stream.
	 */
	bool equals(std::istream& stream) const {
		std::string::size_type sz = _obj.size();

		for(size_t i = 0; i < sz; i++) {
			if(!stream.good()) return false;
			if(stream.get() != _obj[i]) return false;
		}

		return true;
	}

	/**
	* Assignment operator. Copies the underlying object.
	**/
	StreamMask& operator=(const StreamMask& obj) {
		_obj = obj._obj;
		return *this;
	}

	/**
	* Copy constructor.
	**/
	StreamMask(const StreamMask& obj): _obj(obj._obj) {}

	/**
	* Constructor.
	**/
	StreamMask(const std::string& obj): _obj(obj) {}
};

/**
 * Compares an object read from the stream with the object stored in the
 * StreamMask. Returns true if equal and false otherwise. This is the greedy
 * version, which uses operator>> to read from the stream.
 */
template<>
inline bool StreamMask<std::string, true>::equals(std::istream& stream) const {
	std::string obj; stream >> obj;
	return obj == _obj;
}

/**
* An auxiliary function that returns a StreamMask<Type>(obj), which is
* an object that can be used with an istream to check whether a certain part
* of input conforms to a specified mask – a Type object is read from the stream
* and compared with the Type object stored in the StreamMask (the obj object in
* this case).
*
* For types which cannot be compared for equality using operator==(), or should
* not be compared that way (e.g. C-style strings) need a specialisation of
* StreamMask.
*
* Specialisations for std::string and C-style strings are provided, which do not
* read the Type object using operator>>, but read character by character and
* stop consuming characters once the entire stored object is read.
*/
template<class Type>
inline StreamMask<Type, false> stream_mask(const Type& obj) {
	return StreamMask<Type, false>(obj);
}

/**
 * A stream_mask version for const char*.
 */
inline StreamMask<const char*, false> stream_mask(const char* obj) {
	return StreamMask<const char*, false>(obj);
}

/**
 * A stream_mask version for char*.
 */
inline StreamMask<const char*, false> stream_mask(char* obj) {
	return StreamMask<const char*, false>(obj);
}

/**
* An auxiliary function that returns a StreamMask<Type>(obj), which is
* an object that can be used with an istream to check whether a certain part
* of input conforms to a specified mask – a Type object is read from the stream
* and compared with the Type object stored in the StreamMask (the obj object in
* this case).
*
* For types which cannot be compared for equality using operator==(), or should
* not be compared that way (e.g. C-style strings) need a specialisation of
* StreamMask.
*
* Specialisations for std::string and C-style strings are provided.
*
* The greedy version of StreamMask is used (objects are simply read using
* operator>>() from the stream).
*/
template<class Type>
inline StreamMask<Type, true> stream_mask_greedy(const Type& obj) {
	return StreamMask<Type, true>(obj);
}

/**
 * A stream_mask version for const char*.
 */
inline StreamMask<const char*, true> stream_mask_greedy(const char* obj) {
	return StreamMask<const char*, true>(obj);
}

/**
 * A stream_mask version for char*.
 */
inline StreamMask<const char*, true> stream_mask_greedy(char* obj) {
	return StreamMask<const char*, true>(obj);
}

/**
 * Overload of the istream operator>> for StreamMask<Type>.
 */
template<class Type, bool Greedy>
std::istream& operator>>(std::istream& stream, const StreamMask<Type, Greedy>& mask) {
	if(!mask.equals(stream)) throw TracedError_InvalidFormat("The data in the stream does not conform to the format specified by the mask.");
	return stream;
}

}//namespace systematic

#endif //Systematic_StreamMask_H_
