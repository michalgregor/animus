#ifndef SYS_TEMPIO_H_INCLUDED
#define SYS_TEMPIO_H_INCLUDED

#include <string>
#include <cstdlib>
#include <fstream>
#include "../system.h"

namespace systematic {
	std::string SYS_API openTempFile(std::string filename, std::ofstream& f);
	std::string SYS_API openTempFile(std::string filename, std::FILE*& file);
}

#endif // SYS_TEMPIO_H_INCLUDED
