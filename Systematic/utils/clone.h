#ifndef Systematic_clone_H_
#define Systematic_clone_H_

#include <boost/type_traits/is_polymorphic.hpp>
#include "../pointer/smart_ptr.h"
#include "../macros.h"
#include "../exception/TracedError.h"

#include <type_traits>

namespace systematic {

//! Defines a typedef named SYS_CLASS_TYPE with the type of the current class.
//! The macro can be used at class scope.
#define SYS_DECLARE_CLASS_TYPE()											\
static auto __class_type_aux__() -> decltype(this) {return nullptr;}		\
typedef typename std::remove_const<											\
		typename std::remove_pointer<										\
			typename std::remove_reference<									\
				decltype(__class_type_aux__())								\
			>::type															\
		>::type																\
	>::type	SYS_CLASS_TYPE;

/**
 * @macro SYS_CLONEABLE
 * The SYS_CLONEABLE macro can be used in a class definition to provide the class
 * with support for clone(). The macro must be embedded in definition of every
 * derived class as well if the most-derived type is to be copied by clone().
 */
#define SYS_CLONEABLE()														\
SYS_DECLARE_CLASS_TYPE()													\
friend class systematic::clone_access;										\
virtual SYS_CLASS_TYPE* clone_impl() const {								\
return new typename std::remove_const<										\
	typename std::remove_reference<decltype(*this)>::type>::type(*this);	\
}

/**
 * @macro SYS_CLONEABLE_VOID
 * Does the same as SYS_CLONEABLE, but for abstract classes.
 */
#define SYS_CLONEABLE_VOID()												\
SYS_DECLARE_CLASS_TYPE()													\
friend class systematic::clone_access;										\
virtual SYS_CLASS_TYPE* clone_impl() const {								\
	throw TracedError_NotImplemented("Abstract classes cannot be cloned");	\
}

class SYS_API clone_access {
public:
	template<class Type>
	inline static Type* clone(const Type& obj) {
		return obj.clone_impl();
	}
};

/**
 * Creates a new copy of obj and returns a pointer to it. It is ensured that
 * the copy is always made of the most-derived class.
 *
 * In order to do this, it is required that Type as well as the most derived
 * type implement method clone_impl() with such behaviour.
 *
 * This method should not be used for non-polymorphic types (these can easily
 * be copied using their copy constructor).
 *
 * For polymorphic types that do not implement clone_impl(), a specialization
 * has to be provided if they are to cloned.
 *
 * A specialization is provided by default for shared_ptr, intrusive_ptr,
 * and raw pointers. These specializations call clone() on the
 * pointed-to object. These versions will return a null pointer if a null
 * pointer is passed to them.
 */
template<class Type>
inline Type* clone(const Type& obj) {
	static_assert(boost::is_polymorphic<Type>::value, "Type must be polymorphic.");
	return clone_access::clone(obj);
}

/**
 * Specialization for shared_ptr.
 */
template<class Type>
inline Type* clone(const shared_ptr<const Type>& obj) {
	static_assert(boost::is_polymorphic<Type>::value, "Type must be polymorphic.");
	if(!obj) return nullptr;
	else return clone_access::clone(*obj);
}

/**
 * Specialization for shared_ptr. Const version.
 */
template<class Type>
inline Type* clone(const shared_ptr<Type>& obj) {
	static_assert(boost::is_polymorphic<Type>::value, "Type must be polymorphic.");
	if(!obj) return nullptr;
	else return clone_access::clone(*obj);
}

/**
 * Specialization for intrusive_ptr.
 */
template<class Type>
inline Type* clone(const intrusive_ptr<const Type>& obj) {
	static_assert(boost::is_polymorphic<Type>::value, "Type must be polymorphic.");
	if(!obj) return nullptr;
	else return clone_access::clone(*obj);
}

/**
 * Specialization for intrusive_ptr.
 */
template<class Type>
inline Type* clone(const intrusive_ptr<Type>& obj) {
	static_assert(boost::is_polymorphic<Type>::value, "Type must be polymorphic.");
	if(!obj) return nullptr;
	else return clone_access::clone(*obj);
}

/**
 * Specialization for raw pointer.
 */
template<class Type>
inline Type* clone(Type* obj) {
	static_assert(boost::is_polymorphic<Type>::value, "Type must be polymorphic.");
	if(!obj) return nullptr;
	else return clone_access::clone(*obj);
}

/**
 * Specialization for raw pointer. Const version.
 */
template<class Type>
inline Type* clone(const Type* obj) {
	static_assert(boost::is_polymorphic<Type>::value, "Type must be polymorphic.");
	if(!obj) return nullptr;
	else return clone_access::clone(*obj);
}

} //namespace systematic

#endif //Systematic_clone_H_
