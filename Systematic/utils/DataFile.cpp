#include "DataFile.h"

#include <boost/serialization/map.hpp>
#include <boost/algorithm/string/trim.hpp>

namespace systematic {

/**
* Boost serialization function.
**/

void DataFile::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _header;
	ar & _options;
	ar & _data;
}

void DataFile::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _header;
	ar & _options;
	ar & _data;
}

/**
 * Load the DataFile from the specified stream.
 *
 * \exception TracedError_InvalidFormat Throws when the data file is unreadable,
 * empty, or not of the appropriate format.
 */

void DataFile::load(std::istream& stream) {
	if(!stream.good()) throw TracedError_InvalidFormat("Invalid stream.");
	std::string line;

	//read the header
	while(stream.good()) {
		getline(stream, line);
		boost::trim(line);
		trimComment(line);

		//ignore comments and empty lines
		if(!line.size()) continue;

		_header = line;
		break;
	}

	if(!_header.size()) throw TracedError_InvalidFormat("The file contains no header.");

	//read the options
	while(stream.good()) {
		getline(stream, line);
		trimComment(line);
		boost::trim(line);

		//ignore comments and empty lines
		if(!line.size()) continue;

		//look for the colon and split the option to its name and value
		size_t pos = line.find(':');

		//if there is no colon, the data section has begun
		if(pos == std::string::npos) {
			_data.push_back(line);
			break;
		} else {
			std::string value;

			if(pos + 1 < line.size()) {
				value = line.substr(pos + 1);
				boost::trim(value);
			}

			_options[line.substr(0, pos)] = value;
		}
	}

	//read the data section
	while(stream.good()) {
		getline(stream, line);
		trimComment(line);
		boost::trim(line);

		//options are not allowed in this section any more
		if(line.find(':') != std::string::npos)
			throw TracedError_InvalidFormat("An unexpected colon encountered: options not allowed in the data section.");

		_data.push_back(line);
	}

}

/**
 * Writes the data file into the specified stream.
 */

void DataFile::save(std::ostream& stream) const {
	stream << _header << std::endl << std::endl;

	for(OptionContainer::const_iterator iter = _options.begin(); iter != _options.end(); iter++) {
		stream << iter->first << ": " << iter->second << std::endl;
	}

	stream << std::endl;

	for(DataContainer::const_iterator iter = _data.begin(); iter != _data.end(); iter++) {
		stream << *iter << std::endl;
	}
}

}//namespace systematic
