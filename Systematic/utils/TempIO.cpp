#include "TempIO.h"
#include "../exception/TracedError.h"

namespace systematic {

#ifdef _WIN32
#include <windows.h>

/**
* Opens a temporary file, using the provided stream and returns its filename.
* @param filename Preffered name of the temporary file.
* @param f The input stream that is to be used to open the file.
**/

	std::string SYS_API openTempFile(std::string filename, std::ofstream& f) {
		filename.resize(MAX_PATH+1); std::string name; name.resize(MAX_PATH+1);

		if (GetTempPathA (filename.size(), const_cast<char*>(filename.c_str())) == 0)
		throw TracedError_Generic("TempIO: could create directory path.");

		if(GetTempFileNameA (const_cast<char*>(filename.c_str()), "tmp", 0, const_cast<char*>(name.c_str())) == 0)
		throw TracedError_Generic("TempIO: could create filename.");

		filename = filename+name;

		f.open(filename.c_str(), std::ios_base::trunc | std::ios_base::out);
		if(!f.good()) throw TracedError_Generic("TempIO: could not open file " +filename + ".");

		return filename;
	}

	/**
	* Opens a temporary file, using the provided FILE* and returns its filename.
	* @param filename Preffered name of the temporary file.
	* @param file The FILE* that is to be used to open the file.
	**/

	std::string SYS_API openTempFile(std::string filename, std::FILE*& file) {
		filename.resize(MAX_PATH+1); std::string name; name.resize(MAX_PATH+1);

		if (GetTempPathA (filename.size(), const_cast<char*>(filename.c_str())) == 0)
		throw TracedError_Generic("TempIO: could create directory path.");

		if(GetTempFileNameA (const_cast<char*>(filename.c_str()), "tmp", 0, const_cast<char*>(name.c_str())) == 0)
		throw TracedError_Generic("TempIO: could create filename.");

		filename = filename+name;

		if((file = fopen(filename.c_str(), "w"), file == NULL))
		throw TracedError_Generic("TempIO: could not open file " + filename + ".");

		return filename;
	}

#else

	/**
	* Opens a temporary file, using the provided stream and returns its filename.
	* @param filename Preffered name of the temporary file.
	* @param f The input stream that is to be used to open the file.
	**/

	std::string openTempFile(std::string filename, std::ofstream& f) {
		filename = "/tmp/" + filename + "XXXXXX";

		int fd = mkstemp(const_cast<char*>(filename.c_str()));
		if(fd != -1 && (f.open(filename.c_str(), std::ios_base::trunc | std::ios_base::out), f.good())) {
			close(fd);
		} else throw TracedError_Generic("TempIO: could not open file " + filename + ".");
		return filename;
	}

	/**
	* Opens a temporary file, using the provided FILE* and returns its filename.
	* @param filename Preffered name of the temporary file.
	* @param file The FILE* that is to be used to open the file.
	**/

	std::string openTempFile(std::string filename, std::FILE*& file) {
		filename = "/tmp/" + filename + "XXXXXX";

		int fd = mkstemp(const_cast<char*>(filename.c_str()));
		if(fd != -1 && (file = fopen(filename.c_str(), "w"), file != NULL)) close(fd);
		else throw TracedError_Generic("TempIO: could not open file " + filename + ".");

		return filename;
	}

#endif

} //namespace systematic
