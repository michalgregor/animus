#ifndef Systematic_FunctorSocketBase_H_
#define Systematic_FunctorSocketBase_H_

#include <list>
#include <boost/serialization/list.hpp>
#include "../system.h"

namespace systematic {

//TODO: Check whether it would not be better to use std::vector here.

/**
* @class FunctorSocketBase
* Contains implementation details of add, erase and clear methods.
**/
SYS_DIAG_OFF(effc++)
template<class Functor>
class FunctorSocketBase: private std::list<shared_ptr<Functor> > {
SYS_DIAG_ON(effc++)
private:
	// A convenience typedef.
	typedef std::list<shared_ptr<Functor> > ContainerType;

private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	void serialize(IArchive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<ContainerType>(*this);
	}

	void serialize(OArchive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<ContainerType>(*this);
	}

public: //forwarding the std::list interface
	using ContainerType::reference;
	using ContainerType::const_reference;
	using ContainerType::iterator;
	using ContainerType::const_iterator;
	using ContainerType::size_type;
	using ContainerType::difference_type;
	using ContainerType::value_type;
	using ContainerType::allocator_type;
	using ContainerType::pointer;
	using ContainerType::const_pointer;
	using ContainerType::reverse_iterator;
	using ContainerType::const_reverse_iterator;

	using ContainerType::begin;
	using ContainerType::end;
	using ContainerType::rbegin;
	using ContainerType::rend;

	using ContainerType::empty;
	using ContainerType::size;
	using ContainerType::max_size;
	using ContainerType::resize;

	using ContainerType::front;
	using ContainerType::back;

	using ContainerType::assign;
	using ContainerType::push_front;
	using ContainerType::pop_front;
	using ContainerType::push_back;
	using ContainerType::pop_back;
	using ContainerType::insert;
	using ContainerType::erase;
	using ContainerType::swap;
	using ContainerType::clear;

	using ContainerType::splice;
	using ContainerType::remove;
	using ContainerType::remove_if;
	using ContainerType::unique;
	using ContainerType::merge;
	using ContainerType::sort;
	using ContainerType::reverse;

	using ContainerType::get_allocator;

public:
	void add(const shared_ptr<Functor>& functor);
	void erase(const shared_ptr<Functor>& functor);

	FunctorSocketBase(): ContainerType() {}
};

/**
* Connects the specified functor.
**/

template<class Functor>
void FunctorSocketBase<Functor>::add(const shared_ptr<Functor>& functor) {
	push_back(functor);
}

/**
* Disconnects the specified functor. Use sparingly - functors are stored in a
* list, it may take some time to find the functor in question.
**/

template<class Functor>
void FunctorSocketBase<Functor>::erase(const shared_ptr<Functor>& functor) {
	typename ContainerType::iterator iter = std::find(begin(), end(), functor);
	if(iter != end()) erase(iter);
}

} //namespace systematic

#endif //Systematic_FunctorSocketBase_H_
