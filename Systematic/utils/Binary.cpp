#include "Binary.h"
#include <algorithm>
#include <cmath>
#include <climits>
#include <bitset>

namespace systematic {

	/**
	* Swaps bits starting with pos1 (inclusive) through pos2 (exclusive).
	**/

	void BinarySwap(char& a, char& b, unsigned int pos1, unsigned int pos2) {
		// create a mask and fill it with 1s where the swapping is to occur
		char mask(static_cast<char>(static_cast<unsigned int>(pow(float(2), float(pos2 - pos1)) - 1) << pos1));
		//fill the swapped bits by 1s in the mask
		a ^= mask & b;
		b ^= mask & a;
		a ^= mask & b;
	}

	/**
	* Swaps bits from pos1 (inclusive) to pos2 (exclusive).
	* @param a The first piece of data as an array of floats.
	* @param b The second piece of data as an array of floats.
	* @param pos1 The lower boundary of the swap (inclusive). This should be < size
	* and < pos2. No checks are employed.
	* @param pos2 The higher boundary of the swap (exclusive). This should be <= size.
	* No checks are employed.
	**/

	void BinarySwap(char* a, char* b, unsigned int pos1, unsigned int pos2) {
		//Devide position by number of chars in a byte to find number of
		//the byte into which it belongs.
		unsigned int lbound = pos1/CHAR_BIT, ubound = pos2/CHAR_BIT;
		//Make the positions relative to their byte.
		pos1 -= lbound*CHAR_BIT; pos2 -= ubound*CHAR_BIT;
		// Lowerbound byte for whole-byte swapping. Here the lbound needs to be
		// modified so as not to include the byte into which the boundary falls.
		unsigned int wlbound = (pos1 == 0)?(lbound):(lbound + 1);
		// Swap whole bytes. Check for wlbound < ulbound as std::swap_ranges
		// only check for inequality of iterators thus resulting in segfault.
		if(wlbound < ubound) std::swap_ranges(a + wlbound, a + ubound,  b + wlbound);

		//If both boundaries fall into the same byte, a single swap is necessary,
		if(lbound == ubound) BinarySwap(a[lbound], b[lbound], pos1, pos2);
		else {
			//if not, swap needs to be done in both bytes.
			//if pos1 == 0, the whole byte has already been swapped
			if(pos1 != 0) BinarySwap(a[lbound], b[lbound], pos1, CHAR_BIT);
			//if pos2 == 0 there is nothing to swap in this byte
			if(pos2 != 0) BinarySwap(a[ubound], b[ubound], 0, pos2);
		}
	}

	/**
	 * Returns a binary representation of val.
	 */

	std::vector<bool> LongToBinary(unsigned long val) {
		std::bitset<10> bs(val);
		size_t num_bits(bs.size());
		std::vector<bool> res; res.reserve(num_bits);

		for(unsigned int i = 0; i < num_bits; i++) {
			res.push_back(bs[i]);
		}

		return res;
	}

	/**
	 * Returns num lower bits of binary representation of val. If num is greater
	 * than the actual number of bits that unsigned long has, the vector is
	 * padded with zeros.
	 */

	std::vector<bool> LongToBinary(unsigned long val, unsigned int num) {
		std::bitset<sizeof(unsigned long)*CHAR_BIT> bs(val);
		size_t num_bits((num <= bs.size())?(num):(bs.size()));
		std::vector<bool> res; res.reserve(num);

		for(unsigned int i = 0; i < num_bits; i++) {
			res.push_back(bs[i]);
		}

		//pad with zeros
		for(size_t i = num_bits; i < num; i++) {
			res.push_back(0);
		}

		return res;
	}

	/**
	 * Returns an unsigned long value corresponding to the binary representation
	 * specified in the bits vector. If there are less bits than
	 * sizeof(unsigned long)*CHAR_BIT, the bit at position 0 is treated as the
	 * LSB and the missing bits are assumed to be 0. If more bits than
	 * sizeof(unsigned long)*CHAR_BIT are provided, the excessive bits are ignored.
	 */

	unsigned long BinaryToLong(const std::vector<bool>& bits) {
		std::bitset<sizeof(unsigned long)*CHAR_BIT> bs;
		size_t num((bits.size() > sizeof(unsigned long)*CHAR_BIT)?(sizeof(unsigned long)*CHAR_BIT):(bits.size()));

		for(size_t i = 0; i < num; i++) {
			bs[i] = bits[i];
		}

		return bs.to_ulong();
	}

} //namespace systematic
