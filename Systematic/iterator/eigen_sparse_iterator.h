#ifndef Systematic_eigen_sparse_iterator_H_
#define Systematic_eigen_sparse_iterator_H_

#include <iterator>
#include "../math/algebra_decl.h"
#include "eigen_reverse_iterator.h"

namespace systematic {

/**
 * @class EigenSparseIterator
 * The iterator class for Eigen SparseMatrix type.
 */
template<class MatrixType, class ScalarType = typename MatrixType::Scalar>
class EigenSparseIterator {
public:
	typedef ptrdiff_t difference_type;
	typedef ScalarType value_type;
	typedef ScalarType* pointer;
	typedef ScalarType& reference;
	typedef std::random_access_iterator_tag iterator_category;

	template<class MatrixTypeB, class ScalarTypeB>
	friend class EigenSparseIterator;

private:
	value_type* _values;
	AlgebraIndex* _indices;
	AlgebraIndex _outer;
	AlgebraIndex _id;

public:
	bool operator==(const EigenSparseIterator& iter) const {
		return (_values == iter._values) && (_outer == iter._outer)
		        && (_id == iter._id);
	}

	bool operator!=(const EigenSparseIterator& iter) const {
		return !(*this == iter);
	}

	inline EigenSparseIterator& operator++() {
		_id++;
		return *this;
	}

	inline EigenSparseIterator operator++(int) {
		EigenSparseIterator tmpIter(*this);
		_id++;
		return tmpIter;
	}

	inline EigenSparseIterator& operator--() {
		_id--;
		return *this;
	}

	inline EigenSparseIterator operator--(int) {
		EigenSparseIterator tmpIter(*this);
		_id--;
		return tmpIter;
	}

	EigenSparseIterator operator+(difference_type n) const {
		return EigenSparseIterator(_values, _indices, _outer, _id + n);
	}

	EigenSparseIterator operator-(difference_type n) const {
		return EigenSparseIterator(_values, _indices, _outer, _id - n);
	}

	template<typename DerivedB, class ScalarTypeB>
	difference_type operator-(const EigenSparseIterator<DerivedB, ScalarTypeB>& iter) const {
		if(_values != iter._values || _outer != iter._outer)
			throw TracedError_InvalidArgument("_values != iter._values || _outer != iter._outer");
		return static_cast<difference_type>(_id) - static_cast<difference_type>(iter._id);
	}

	template<typename DerivedB, class ScalarTypeB>
	bool operator<(const EigenSparseIterator<DerivedB, ScalarTypeB>& iter) const {
		if(_values != iter._values)
			throw TracedError_InvalidArgument("_values != iter._values");

		if(_outer < iter._outer) return true;
		else if(_outer == iter._outer && _id < iter._id) return true;
		else return false;
	}

	template<typename DerivedB, class ScalarTypeB>
	bool operator>(const EigenSparseIterator<DerivedB, ScalarTypeB>& iter) const {
		if(_values != iter._values)
			throw TracedError_InvalidArgument("_values != iter._values");

		if(_outer > iter._outer) return true;
		else if(_outer == iter._outer && _id > iter._id) return true;
		else return false;
	}

	template<typename DerivedB, class ScalarTypeB>
	bool operator<=(const EigenSparseIterator<DerivedB, ScalarTypeB>& iter) const {
		if(_values != iter._values)
			throw TracedError_InvalidArgument("_values != iter._values");

		if(_outer < iter._outer) return true;
		else if(_outer == iter._outer && _id <= iter._id) return true;
		else return false;
	}

	template<typename DerivedB, class ScalarTypeB>
	bool operator>=(const EigenSparseIterator<DerivedB, ScalarTypeB>& iter) const {
		if(_values != iter._values)
			throw TracedError_InvalidArgument("_values != iter._values");

		if(_outer > iter._outer) return true;
		else if(_outer == iter._outer && _id >= iter._id) return true;
		else return false;
	}

	EigenSparseIterator& operator+=(difference_type n) {
		_id = numeric_cast<AlgebraIndex>(_id + n);
		return *this;
	}

	EigenSparseIterator& operator-=(difference_type n) {
		_id = numeric_cast<AlgebraIndex>(_id - n);
		return *this;
	}

	reference operator[](difference_type n) {
		return *(*this + n);
	}

	value_type* operator->() const {
		return &(_values[_id]);
	}

	value_type& operator*() const {
		return _values[_id];
	}

	value_type& value() const {
		return _values[_id];
	}

	AlgebraIndex index() const {
		return _indices[_id];
	}

	AlgebraIndex id() const {
		return _id;
	}

	AlgebraIndex outer() const {
		return _outer;
	}

	AlgebraIndex row() const {
		return MatrixType::IsRowMajor ? _outer : index();
	}

	AlgebraIndex col() const {
		return MatrixType::IsRowMajor ? index() : _outer;
	}

private:
	EigenSparseIterator(
		value_type* values,
		AlgebraIndex* indices,
		AlgebraIndex outer_,
		AlgebraIndex id_
	): _values(values), _indices(indices), _outer(outer_), _id(id_) {}

public:
	EigenSparseIterator& operator=(const EigenSparseIterator& obj) = default;
	EigenSparseIterator(const EigenSparseIterator& obj) = default;

	EigenSparseIterator():
		_values(nullptr), _indices(nullptr), _outer(0), _id(0) {}

	EigenSparseIterator(MatrixType& mat, AlgebraIndex outer_, AlgebraIndex id_):
		_values(mat.valuePtr()), _indices(mat.innerIndexPtr()),
		_outer(outer_), _id(id_) {}

	template<class IterType>
	EigenSparseIterator(const IterType& iter):
		_values(iter._values), _indices(iter._indices),
		_outer(iter._outer), _id(iter._id) {}
};

} //namespace systematic

#endif //Systematic_eigen_sparse_iterator_H_
