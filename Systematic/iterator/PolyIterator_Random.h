#ifndef POLYITERATOR_RANDOM_H_
#define POLYITERATOR_RANDOM_H_

#include "../system.h"

namespace systematic {

namespace detail {

//------------------------------------------------------------------------------
//----------------------------random declaration--------------------------------
//------------------------------------------------------------------------------

template<class IterType>
class PolyIteratorRandom_Derived;

//------------------------------------------------------------------------------
//-----------------------------forward-base-------------------------------------
//------------------------------------------------------------------------------

/*
* @class PolyIteratorRandom_Base
* @author Michal Gregor
*
* An ABC providing a common interface for classes wrapping iterator types in
* a polymorphic manner.
*
* @tparam ValueType Value type of the wrapped-in iterator.
*/

SYS_WNONVIRT_DTOR_OFF

template<class ValueType>
class PolyIteratorRandom_Base {
private:
	/*
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

private:
	// This section is private so that no classes other than the friend class
	// are able to derive from this base.
	template<class IterType>
	friend class PolyIteratorRandom_Derived;

	// Derived classes use this method to retrieve their unique ID used when
	// comparing instances of derived classes.
	static int getNextDerivedID() {
		static int id = 0;
		return ++id;
	}

	//Returns the unique ID of the derived class.
	virtual int getID() const = 0;

	PolyIteratorRandom_Base() {}

public:
	typedef size_t difference_type;

	virtual bool equals(const PolyIteratorRandom_Base& iter) const = 0;

	virtual bool operator<(const PolyIteratorRandom_Base& iter) const = 0;
	virtual bool operator>(const PolyIteratorRandom_Base& iter) const = 0;
	virtual bool operator<=(const PolyIteratorRandom_Base& iter) const = 0;
	virtual bool operator>=(const PolyIteratorRandom_Base& iter) const = 0;
	virtual difference_type operator-(const PolyIteratorRandom_Base& iter) const = 0;

	virtual const ValueType* getPtr() const = 0;
	virtual ValueType* getPtr() = 0;
	virtual void increment() = 0;
	virtual void increment(difference_type n) = 0;
	virtual void decrement() = 0;
	virtual void decrement(difference_type n) = 0;

	virtual ValueType& at(difference_type index) = 0;
	virtual const ValueType& at(difference_type index) const = 0;

	virtual ~PolyIteratorRandom_Base() = 0;
};

/*
 * Empty body of the pure virtual destructor.
 */

template<class ValueType>
PolyIteratorRandom_Base<ValueType>::~PolyIteratorRandom_Base() {}

SYS_WNONVIRT_DTOR_ON

//------------------------------------------------------------------------------
//-----------------------------random-derived-----------------------------------
//------------------------------------------------------------------------------

template<class IterType>
class PolyIteratorRandom_Derived:
public PolyIteratorRandom_Base<typename std::iterator_traits<IterType>::value_type> {
private:
	/*
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//Returns the unique ID of the derived class.
	virtual int getID() const {
		static int id = this->getNextDerivedID();
		return id;
	}

	//Stores the underlying iterator.
	IterType _iter;

public:
	typedef typename std::iterator_traits<IterType>::value_type ValueType;
	typedef typename PolyIteratorRandom_Base<ValueType>::difference_type difference_type;

public:
	bool equals(const PolyIteratorRandom_Derived& obj) const {
		return _iter == obj._iter;
	}

	bool operator<(const PolyIteratorRandom_Derived& iter) const {
		return _iter < iter._iter;
	}

	bool operator>(const PolyIteratorRandom_Derived& iter) const {
		return _iter > iter._iter;
	}

	bool operator<=(const PolyIteratorRandom_Derived& iter) const {
		return _iter <= iter._iter;
	}

	bool operator>=(const PolyIteratorRandom_Derived& iter) const {
		return _iter >= iter._iter;
	}

	difference_type operator-(const PolyIteratorRandom_Derived& iter) const {
		return _iter - iter._iter;
	}

public:
	virtual bool equals(const PolyIteratorRandom_Base<ValueType>& iter) const {
		//check whether the iterator is of the same type; if so, it can be
		//safely cast using static_cast.
		if(getID() != iter.getID()) throw TracedError_InvalidArgument("The iterator is not of the same derived type.");
		return equals(static_cast<const PolyIteratorRandom_Derived&>(iter));
	}

	virtual bool operator<(const PolyIteratorRandom_Base<ValueType>& iter) const {
		//check whether the iterator is of the same type; if so, it can be
		//safely cast using static_cast.
		if(getID() != iter.getID()) throw TracedError_InvalidArgument("The iterator is not of the same derived type.");
		return operator<(static_cast<const PolyIteratorRandom_Derived&>(iter));
	}

	virtual bool operator>(const PolyIteratorRandom_Base<ValueType>& iter) const {
		//check whether the iterator is of the same type; if so, it can be
		//safely cast using static_cast.
		if(getID() != iter.getID()) throw TracedError_InvalidArgument("The iterator is not of the same derived type.");
		return operator>(static_cast<const PolyIteratorRandom_Derived&>(iter));
	}

	virtual bool operator<=(const PolyIteratorRandom_Base<ValueType>& iter) const {
		//check whether the iterator is of the same type; if so, it can be
		//safely cast using static_cast.
		if(getID() != iter.getID()) throw TracedError_InvalidArgument("The iterator is not of the same derived type.");
		return operator<=(static_cast<const PolyIteratorRandom_Derived&>(iter));
	}

	virtual bool operator>=(const PolyIteratorRandom_Base<ValueType>& iter) const {
		//check whether the iterator is of the same type; if so, it can be
		//safely cast using static_cast.
		if(getID() != iter.getID()) throw TracedError_InvalidArgument("The iterator is not of the same derived type.");
		return operator>=(static_cast<const PolyIteratorRandom_Derived&>(iter));
	}

	virtual difference_type operator-(const PolyIteratorRandom_Base<ValueType>& iter) const {
		//check whether the iterator is of the same type; if so, it can be
		//safely cast using static_cast.
		if(getID() != iter.getID()) throw TracedError_InvalidArgument("The iterator is not of the same derived type.");
		return operator-(static_cast<const PolyIteratorRandom_Derived&>(iter));
	}

	virtual const ValueType* getPtr() const {
		return _iter.operator->();
	}

	virtual ValueType* getPtr() {
		return _iter.operator->();
	}

	virtual void increment() {
		_iter++;
	}

	virtual void increment(difference_type n) {
		_iter += n;
	}

	virtual void decrement() {
		_iter--;
	}

	virtual void decrement(difference_type n) {
		_iter -= n;
	}

	virtual ValueType& at(difference_type index) {
		return _iter[index];
	}

	virtual const ValueType& at(difference_type index) const {
		return _iter[index];
	}

	PolyIteratorRandom_Derived(const PolyIteratorRandom_Derived& obj):
	_iter(obj._iter) {}

	PolyIteratorRandom_Derived(const IterType& iter): _iter(iter) {}

	virtual ~PolyIteratorRandom_Derived() {}
};

} //namespace detail

//------------------------------------------------------------------------------
//----------------------------PolyIterator_Random-------------------------------
//------------------------------------------------------------------------------

/**
 * @class PolyIterator_Random
 * @author Michal Gregor
 *
 * A wrapper class for an arbitrary kind of forward iterator. This class can
 * be used in place of standard iterators in a virtual method (virtual methods
 * cannot be templates and thus type of the iterator would have to be fixed
 * otherwise).
 */

template<class ValueType>
class PolyIterator_Random:
public std::iterator<
	std::random_access_iterator_tag,
	ValueType,
	typename detail::PolyIteratorRandom_Base<ValueType>::difference_type
> {
private:
	//! Pointer to the underlying wrapped iterator.
	detail::PolyIteratorRandom_Base<ValueType>* _polyIter;

public:
	typedef typename detail::PolyIteratorRandom_Base<ValueType>::difference_type difference_type;

	/**
	 * Throws a TracedError_InvalidArgument when two PolyIterator_Random
	 * iterators constructed from different underlying iterators are compared.
	 *
	 * Throws TracedError_InvalidPointer if one or both of the iterators are
	 * uninitialized.
	 */
	bool operator==(const PolyIterator_Random& iter) const {
		if(!(_polyIter && iter._polyIter)) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->equals(*(iter._polyIter));
	}

	/**
	 * Throws a TracedError_InvalidArgument when two PolyIterator_Random
	 * iterators constructed from different underlying iterators are compared.
	 *
	 * Throws TracedError_InvalidPointer if one or both of the iterators are
	 * uninitialized.
	 */

	bool operator!=(const PolyIterator_Random& iter) const {
		if(!(_polyIter && iter._polyIter)) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return !(_polyIter->equals(*(iter._polyIter)));
	}

	/**
	 * Throws a TracedError_InvalidArgument when two PolyIterator_Random
	 * iterators constructed from different underlying iterators are compared.
	 *
	 * Throws TracedError_InvalidPointer if one or both of the iterators are
	 * uninitialized.
	 */

	bool operator<(const PolyIterator_Random& iter) const {
		if(!(_polyIter && iter._polyIter)) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->operator<(*(iter._polyIter));
	}

	/**
	 * Throws a TracedError_InvalidArgument when two PolyIterator_Random
	 * iterators constructed from different underlying iterators are compared.
	 *
	 * Throws TracedError_InvalidPointer if one or both of the iterators are
	 * uninitialized.
	 */

	bool operator>(const PolyIterator_Random& iter) const {
		if(!(_polyIter && iter._polyIter)) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->operator>(*(iter._polyIter));
	}

	/**
	 * Throws a TracedError_InvalidArgument when two PolyIterator_Random
	 * iterators constructed from different underlying iterators are compared.
	 *
	 * Throws TracedError_InvalidPointer if one or both of the iterators are
	 * uninitialized.
	 */

	bool operator<=(const PolyIterator_Random& iter) const {
		if(!(_polyIter && iter._polyIter)) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->operator<=(*(iter._polyIter));
	}

	/**
	 * Throws a TracedError_InvalidArgument when two PolyIterator_Random
	 * iterators constructed from different underlying iterators are compared.
	 *
	 * Throws TracedError_InvalidPointer if one or both of the iterators are
	 * uninitialized.
	 */

	bool operator>=(const PolyIterator_Random& iter) const {
		if(!(_polyIter && iter._polyIter)) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->operator>=(*(iter._polyIter));
	}

	/**
	 * Throws a TracedError_InvalidArgument when two PolyIterator_Random
	 * iterators constructed from different underlying iterators are compared.
	 *
	 * Throws TracedError_InvalidPointer if one or both of the iterators are
	 * uninitialized.
	 */

	difference_type operator-(const PolyIterator_Random& iter) const {
		if(!(_polyIter && iter._polyIter)) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->operator-(*(iter._polyIter));
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	const ValueType* operator->() const {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->getPtr();
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	ValueType* operator->() {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->getPtr();
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	const ValueType& operator*() const {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return *(_polyIter->getPtr());
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	ValueType& operator*() {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return *(_polyIter->getPtr());
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Random& operator++() {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		_polyIter->increment();
		return *this;
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Random operator++(int) {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		PolyIterator_Random tmp(*this);
		_polyIter->increment();
		return tmp;
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Random& operator--() {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		_polyIter->decrement();
		return *this;
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Random operator--(int) {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		PolyIterator_Random tmp(*this);
		_polyIter->decrement();
		return tmp;
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Random operator+(difference_type n) const {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		PolyIterator_Random tmp(*this);
		tmp += n;
		return tmp;
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Random operator-(difference_type n) const {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		PolyIterator_Random tmp(*this);
		tmp -= n;
		return tmp;
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Random& operator+=(difference_type n) {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		_polyIter->increment(n);
		return *this;
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Random& operator-=(difference_type n) {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		_polyIter->decrement(n);
		return *this;
	}

	ValueType& operator[](difference_type n) {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->at(n);
	}

	PolyIterator_Random& operator=(const PolyIterator_Random& obj) {
		if(this != &obj)  {
			if(_polyIter) delete _polyIter;
			_polyIter = clone(obj._polyIter);
		}
		return *this;
	}

	PolyIterator_Random(const PolyIterator_Random& obj):
	_polyIter(clone(obj._polyIter)) {}

	/**
	 * Template constructor. Allows construction of PolyIterator_Random
	 * from an arbitrary forward iterator so long as its value type is the same
	 * as the ValueType template parameter.
	 */

	template<class IterType>
	explicit PolyIterator_Random(const IterType& iter):
	_polyIter(new detail::PolyIteratorRandom_Derived<IterType>(iter)) {}

	/**
	 * Default constructor. Initialize using operator= before use.
	 */

	PolyIterator_Random(): _polyIter(NULL) {}

	/**
	 * Destructor.
	 */

	~PolyIterator_Random() {
		if(_polyIter) delete _polyIter;
	}
};

template<class ValueType>
PolyIterator_Random<ValueType> operator+(
	typename PolyIterator_Random<ValueType>::difference_type n,
	const PolyIterator_Random<ValueType>& iter
) {
	PolyIterator_Random<ValueType> tmp(iter);
	tmp += n;
	return tmp;
}

} //namespace systematic

#endif /* POLYITERATOR_RANDOM_H_ */
