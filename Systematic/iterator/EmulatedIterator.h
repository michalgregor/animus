#ifndef Systematic_EmulatedIterator_H_
#define Systematic_EmulatedIterator_H_

#include "../pointer/EmulatedPtr.h"

namespace systematic {

/**
 * @class EmulatedTypeConstructor
 *
 * A functor for use with EmulatedIterator. It constructs a new object of type
 * Type, given its index and a pointer to the raw container by calling
 * constructor Type(container, index).
 */
template<class Type, class RawContainer, class SizeType>
class EmulatedTypeConstructor {
public:
	Type operator()(RawContainer* container, SizeType index) const {
		return Type(container, index);
	}

public:
	template<class TypeB, class RawContainerB, class SizeTypeB>
	EmulatedTypeConstructor(const EmulatedTypeConstructor<TypeB, RawContainerB, SizeTypeB>&) {}

	EmulatedTypeConstructor(const EmulatedTypeConstructor&) = default;
	EmulatedTypeConstructor() = default;
};

/**
 * @class EmulatedIterator
 *
 * An emulated iterator, iterating over a range of objects that do not really
 * exists. Dereferences to fake references. Pointers are provided using
 * EmulatedPtr.
 *
 * @tparam Type The fake reference type.
 * @tparam RawContainer The raw container, which holds data required to
 * instantiate Type objects.
 * @tparam SizeType The numeric type used to represent sizes and indices.
 * @tparam TypeCreator Type of the functor used to create Type objects from
 * an index and a pointer to the raw container. Defaults to just using
 * constructor Type(index, container).
 */
template<class Type, class RawContainer, class SizeType = size_t, class TypeCreator = EmulatedTypeConstructor<Type, RawContainer, SizeType> >
class EmulatedIterator {
public:
	typedef Type reference;
	typedef Type value_type;
	typedef EmulatedPtr<Type> pointer;
	typedef SizeType size_type;
	typedef ptrdiff_t difference_type;
	typedef std::random_access_iterator_tag iterator_category;

private:
	template<class Typeb, class RawContainerb, class SizeTypeb, class TypeCreatorb>
	friend class EmulatedIterator;

private:
	// A pointer back to the ConnectionManager. May be a nullptr.
	RawContainer* _container;
	// Subscript of the element in the NeuronContainer.
	size_type _index;
	//! The functor used to construct the Type from index and RawContainer.
	TypeCreator _typeCreator;

public:
	size_type index() const {
		return _index;
	}

	template<class Typeb, class Containerb, class SizeTypeb, class TypeCreatorb>
	bool operator==(const EmulatedIterator<Typeb, Containerb, SizeTypeb, TypeCreatorb>& obj) const {
		return (_container == obj._container) && (_index == obj._index);
	}

	template<class Typeb, class Containerb, class SizeTypeb, class TypeCreatorb>
	bool operator!=(const EmulatedIterator<Typeb, Containerb, SizeTypeb, TypeCreatorb>& obj)  const{
		return (_container != obj._container) || (_index != obj._index);
	}

	reference operator*() const {
		return _typeCreator(_container, _index);
	}

	pointer operator->() const {
		return pointer(_typeCreator(_container, _index));
	}

	EmulatedIterator& operator++() {
		_index++;
		return *this;
	}

	EmulatedIterator& operator--() {
		_index--;
		return *this;
	}

	EmulatedIterator operator++(int) {
		EmulatedIterator tmp(*this);
		_index++;
		return tmp;
	}

	EmulatedIterator operator--(int) {
		EmulatedIterator tmp(*this);
		_index--;
		return tmp;
	}

	EmulatedIterator operator+(difference_type n) const {
		return EmulatedIterator(_container, numeric_cast<size_type>((_index + n)));
	}

	EmulatedIterator operator-(difference_type n) const {
		return EmulatedIterator(_container, numeric_cast<size_type>(_index - n));
	}

	template<class Typeb, class Containerb, class SizeTypeb, class TypeCreatorb>
	difference_type operator-(const EmulatedIterator<Typeb, Containerb, SizeTypeb, TypeCreatorb>& iter) const {
		return numeric_cast<difference_type>(_index - iter._index);
	}

	template<class Typeb, class Containerb, class SizeTypeb, class TypeCreatorb>
	bool operator<(const EmulatedIterator<Typeb, Containerb, SizeTypeb, TypeCreatorb>& iter) const {
		return _index < iter._index;
	}

	template<class Typeb, class Containerb, class SizeTypeb, class TypeCreatorb>
	bool operator>(const EmulatedIterator<Typeb, Containerb, SizeTypeb, TypeCreatorb>& iter) const {
		return _index > iter._index;
	}

	template<class Typeb, class Containerb, class SizeTypeb, class TypeCreatorb>
	bool operator<=(const EmulatedIterator<Typeb, Containerb, SizeTypeb, TypeCreatorb>& iter) const {
		return _index <= iter._index;
	}

	template<class Typeb, class Containerb, class SizeTypeb, class TypeCreatorb>
	bool operator>=(const EmulatedIterator<Typeb, Containerb, SizeTypeb, TypeCreatorb>& iter) const {
		return _index >= iter._index;
	}

	EmulatedIterator& operator+=(difference_type n) {
		_index = numeric_cast<size_type>(_index + n);
		return *this;
	}

	EmulatedIterator& operator-=(difference_type n) {
		_index = numeric_cast<size_type>(_index - n);
		return *this;
	}

	/**
	 * Returns a reference to the element at position n, relatively to the
	 * element pointed to by this iterator.
	 *
	 * \warning If index of the element reffered to by this iterator
	 * (its absolute index in the NeuronContainer) is smaller than -n, this
	 * results in an integer underflow!
	 **/
	reference operator[](difference_type n) {
		return _typeCreator(_container, numeric_cast<size_type>(_index + n));
	}

	//! Assignment operator.
	EmulatedIterator& operator=(const EmulatedIterator& obj) {
		_container = obj._container;
		_index = obj._index;
		return *this;
	}

	//! Assignment operator for const iterator.
	template<class Typeb, class Containerb, class SizeTypeb, class TypeCreatorb>
	EmulatedIterator& operator=(const EmulatedIterator<Typeb, Containerb, SizeTypeb, TypeCreatorb>& obj) {
		_container = obj._container;
		_index = obj._index;
		_typeCreator = obj._typeCreator;
		return *this;
	}

	/**
	 * Constructor.
	 * @param manager Pointer to the ConnectionManager with which
	 * the iterator is associated.
	 * @param iter Iterator of the underlying _connections std::vector.
	 */
	EmulatedIterator(RawContainer* container, size_type index_, TypeCreator typeCreator = TypeCreator()):
				_container(container), _index(index_), _typeCreator(typeCreator) {}

	//! Copy construction of const iterator from iterator.
	template<class Typeb, class Containerb, class SizeTypeb, class TypeCreatorb>
	EmulatedIterator(const EmulatedIterator<Typeb, Containerb, SizeTypeb, TypeCreatorb>& obj):
		_container(obj._container), _index(obj._index), _typeCreator(obj._typeCreator) {}

	//! Copy construction.
	EmulatedIterator(const EmulatedIterator& obj):
		_container(obj._container), _index(obj._index), _typeCreator(obj._typeCreator) {}

	//! Default constructor.
	EmulatedIterator(TypeCreator typeCreator = TypeCreator()):
		_container(nullptr), _index(0), _typeCreator(typeCreator) {}
};

} //namespace systematic

#endif //Systematic_EmulatedIterator_H_
