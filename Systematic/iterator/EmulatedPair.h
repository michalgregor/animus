#ifndef Systematic_EmulatedPair_H_
#define Systematic_EmulatedPair_H_

#include <Systematic/configure.h>
#include "../pointer/EmulatedPtr.h"

namespace systematic {

//forward declarations
template<class T1, class T2>
class EmulatedPair_ValuePair;
template<class T1, class T2>
class EmulatedPair_Reference;
template<class T1, class T2>
class EmulatedPair_ConstReference;

/**
 * Returns a raw pointer to the object. A specialization is
 * required for types with overloaded operator&().
 */
template<class T1, class T2>
inline EmulatedPair_ValuePair<T1, T2>* getRawPointer(EmulatedPair_ValuePair<T1, T2>& ref) {
	return ref.rawPointer();
}

/**
 * Returns a raw pointer to the object. A specialization is
 * required for types with overloaded operator&().
 */
template<class T1, class T2>
inline const EmulatedPair_ValuePair<T1, T2>* getRawPointer(const EmulatedPair_ValuePair<T1, T2>& ref) {
	return ref.rawPointer();
}

/**
 * Returns a raw pointer to the object. A specialization is
 * required for types with overloaded operator&().
 */
template<class T1, class T2>
inline EmulatedPair_Reference<T1, T2>* getRawPointer(EmulatedPair_Reference<T1, T2>& ref) {
	return ref.rawPointer();
}

/**
 * Returns a raw pointer to the object. A specialization is
 * required for types with overloaded operator&().
 */
template<class T1, class T2>
inline const EmulatedPair_Reference<T1, T2>* getRawPointer(const EmulatedPair_Reference<T1, T2>& ref) {
	return ref.rawPointer();
}

/**
 * Returns a raw pointer to the object. A specialization is
 * required for types with overloaded operator&().
 */
template<class T1, class T2>
inline EmulatedPair_ConstReference<T1, T2>* getRawPointer(EmulatedPair_ConstReference<T1, T2>& ref) {
	return ref.rawPointer();
}

/**
 * Returns a raw pointer to the object. A specialization is
 * required for types with overloaded operator&().
 */
template<class T1, class T2>
inline const EmulatedPair_ConstReference<T1, T2>* getRawPointer(const EmulatedPair_ConstReference<T1, T2>& ref) {
	return ref.rawPointer();
}

//------------------------------------------------------------------------------
//--------------------End of specifications for getRawPointer-------------------
//------------------------------------------------------------------------------

/**
 * @class EmulatedPair_ValuePair
 * A pair structure similar to std::pair. This can be used to provide iterators
 * that iterate over two separate containers simultaneously – EmulatedPair_ValuePair
 * is then used as value_type of the virtual container.
 *
 * Additionally, as EmulatedPair_ValuePair will be generated on the fly and not
 * really stored in any container, there is the EmulatedPair_Pointer class and
 * EmulatedPair_Reference class – these serve as emulators of pointer and
 * reference type for those non-existent EmulatedPair_ValuePair objects.
 */

template<class T1, class T2>
class EmulatedPair_ValuePair {
public:
	typedef T1 first_type;
	typedef T2 second_type;

	typedef EmulatedPtr<EmulatedPair_Reference<T1, T2> > pointer;
	typedef EmulatedPtr<EmulatedPair_Reference<T1, T2> > const_pointer;

private:
	first_type _first;
	second_type _second;

public:
	first_type& first() {return _first;}
	const first_type& first() const {return _first;}

	first_type& second() {return _second;}
	const second_type& second() const {return _second;}

	EmulatedPair_ValuePair() : _first(T1()), _second(T2()) {}
	EmulatedPair_ValuePair(const T1& x, const T2& y) : _first(x), _second(y) {}

	template <class U, class V>
	EmulatedPair_ValuePair(const EmulatedPair_ValuePair<U,V> &p) : _first(p.first), _second(p.second) {}

public:
	/**
	 * Returns a real %pointer to the LayerRef as opposed to returning the
	 * pointer type.
	 */
	EmulatedPair_ValuePair* rawPointer() {return this;}

	/**
	 * Returns a real %pointer to the LayerRef as opposed to returning the
	 * pointer type.
	 */
	const EmulatedPair_ValuePair* rawPointer() const {return this;}

	/**
	 * Returns an object that acts as a pointer to the Layer.
	 */
	pointer operator&() {
		return pointer(*this);
	}

	/**
	 * Returns an object that acts as a pointer to the Layer.
	 */
	const_pointer operator&() const {
		return const_pointer(*this);
	}
};

/**
 * @class EmulatedPair_Reference
 * A type that serves as a reference to an emulated pair of values.
 * See documentation for EmulatedPair_ValuePair for further detail.
 */

template<class T1, class T2>
class EmulatedPair_Reference {
	template<class U, class V>
	friend class EmulatedPair_Reference;
	template<class U, class V>
	friend class EmulatedPair_ConstReference;

public:
	typedef T1 first_type;
	typedef T2 second_type;

	typedef EmulatedPtr<EmulatedPair_Reference<T1, T2> > pointer;
	typedef EmulatedPtr<EmulatedPair_Reference<T1, T2> > const_pointer;

private:
	first_type* _first;
	second_type* _second;

public:
	first_type& first() {return *_first;}
	const first_type& first() const {return *_first;}

	second_type& second() {return *_second;}
	const second_type& second() const {return *_second;}

	EmulatedPair_Reference(T1* x, T2* y) : _first(x), _second(y) {}

	template<class U, class V>
	EmulatedPair_Reference& operator=(const EmulatedPair_ValuePair<U, V>& obj) {
		*_first = obj.first();
		*_second = obj.second();
		return *this;
	}

	template<class U, class V>
	EmulatedPair_Reference& operator=(const EmulatedPair_Reference<U, V>& obj) {
		*_first = *(obj._first);
		*_second = *(obj._second);
		return *this;
	}

	EmulatedPair_Reference& operator=(const EmulatedPair_Reference& obj) {
		*_first = *(obj._first);
		*_second = *(obj._second);
		return *this;
	}

	EmulatedPair_Reference(const EmulatedPair_Reference &p) : _first(p._first), _second(p._second) {}

	template <class U, class V>
	EmulatedPair_Reference(const EmulatedPair_Reference<U,V> &p) : _first(p._first), _second(p._second) {}

	template <class U, class V>
	EmulatedPair_Reference(const EmulatedPair_ValuePair<U,V> &p) : _first(&(p.first())), _second(&(p.second())) {}

	template <class U, class V>
	EmulatedPair_Reference(EmulatedPair_ValuePair<U,V> &p) : _first(&(p.first())), _second(&(p.second())) {}

	template<class U, class V>
	operator EmulatedPair_ValuePair<U, V>() {
		return EmulatedPair_ValuePair<U, V>(*_first, *_second);
	}

public:
	/**
	 * Returns a real %pointer to the LayerRef as opposed to returning the
	 * pointer type.
	 */
	EmulatedPair_Reference* rawPointer() {return this;}

	/**
	 * Returns a real %pointer to the LayerRef as opposed to returning the
	 * pointer type.
	 */
	const EmulatedPair_Reference* rawPointer() const {return this;}

	/**
	 * Returns an object that acts as a pointer to the Layer.
	 */
	pointer operator&() {
		return EmulatedPtr<EmulatedPair_Reference<T1, T2> >(*this);
	}

	/**
	 * Returns an object that acts as a pointer to the Layer.
	 */
	const_pointer operator&() const {
		return EmulatedPtr<EmulatedPair_ConstReference<T1, T2> >(*this);
	}
};

template<class T1, class T2>
class EmulatedPair_ConstReference {
public:
	typedef T1 first_type;
	typedef T2 second_type;

	typedef EmulatedPtr<EmulatedPair_Reference<T1, T2> > pointer;
	typedef EmulatedPtr<EmulatedPair_Reference<T1, T2> > const_pointer;

private:
	const first_type* _first;
	const second_type* _second;

public:
	first_type& first() {return *_first;}
	const first_type& first() const {return *_first;}

	second_type& second() {return *_second;}
	const second_type& second() const {return *_second;}

	EmulatedPair_ConstReference(T1* x, T2* y) : _first(x), _second(y) {}

	template <class U, class V>
	EmulatedPair_ConstReference(const EmulatedPair_ConstReference<U,V> &p) : _first(p._first), _second(p._second) {}

	template <class U, class V>
	EmulatedPair_ConstReference(const EmulatedPair_Reference<U,V> &p) : _first(p._first), _second(p._second) {}

	template <class U, class V>
	EmulatedPair_ConstReference(const EmulatedPair_ValuePair<U,V> &p) : _first(&(p.first())), _second(&(p.second())) {}

	template<class U, class V>
	operator EmulatedPair_ValuePair<U, V>() {
		return EmulatedPair_ValuePair<U, V>(*_first, *_second);
	}

public:
	/**
	 * Returns a real %pointer to the LayerRef as opposed to returning the
	 * pointer type.
	 */
	const EmulatedPair_ConstReference* rawPointer() const {return this;}

	/**
	 * Returns an object that acts as a pointer to the Layer.
	 */
	const_pointer operator&() const {
		return EmulatedPtr<EmulatedPair_ConstReference<T1, T2> >(*this);
	}
};

/**
 * @enum EmulatedPair_Comparison
 * @brief An enum used to specify how EmulatedPair_ValuePair and
 * EmulatedPair_Reference comparison functors should work.
 */

enum EmulatedPair_Comparison {
	EMP_BY_FIRST_ONLY, //! < Compare values of first() only.
	EMP_BY_SECOND_ONLY, //! < Compare values of second() only.
	EMP_FIRST_THEN_SECOND, //! < Compare values by values of first(), then by values of second().
	EMP_SECOND_THEN_FIRST //! < Compare values by values of second(), then by values of first().
};

/**
 * @class EmulatedPair_Lower
 * @brief A functor used to Compare EmulatedPair_ValuePair and
 * EmulatedPair_Reference objects, providing strict weak ordering.
 * For use with std::sort() and similar.
 *
 * @tparam Comparison Specifies how the actual comparison works.
 */

template<EmulatedPair_Comparison Comparison>
class EmulatedPair_Lower {
public:
	template<class T1, class T2>
	inline bool operator()(const T1& t1, const T2& t2);
};

template<EmulatedPair_Comparison Comparison>
template<class T1, class T2>
inline bool EmulatedPair_Lower<Comparison>::operator()(const T1& t1, const T2& t2) {
	if(t1.first() < t2.first()) return true;
	if(t1.first() > t2.first()) return false;
	if(t1.second() < t2.second()) return true;
	return false;
}

template<>
template<class T1, class T2>
inline bool EmulatedPair_Lower<EMP_BY_FIRST_ONLY>::operator()(const T1& t1, const T2& t2) {
	return t1.first() < t2.first();
}

template<>
template<class T1, class T2>
inline bool EmulatedPair_Lower<EMP_BY_SECOND_ONLY>::operator()(const T1& t1, const T2& t2) {
	return t1.second() < t2.second();
}

template<>
template<class T1, class T2>
inline bool EmulatedPair_Lower<EMP_SECOND_THEN_FIRST>::operator()(const T1& t1, const T2& t2) {
	if(t1.second() < t2.second()) return true;
	if(t1.second() > t2.second()) return false;
	if(t1.first() < t2.first()) return true;
	return false;
}

/**
 * @class EmulatedPair_Equal
 * @brief A functor used to Compare EmulatedPair_ValuePair and
 * EmulatedPair_Reference objects. Returns true if both objects compare as equal
 * and false otherwise.
 *
 * For use with std::unique() and similar.
 *
 * @tparam Comparison Specifies how the actual comparison works.
 */

template<EmulatedPair_Comparison Comparison>
class EmulatedPair_Equal {
public:
	template<class T1, class T2>
	inline bool operator()(const T1& t1, const T2& t2);
};

template<EmulatedPair_Comparison Comparison>
template<class T1, class T2>
inline bool EmulatedPair_Equal<Comparison>::operator()(const T1& t1, const T2& t2) {
	return (t1.first() == t2.first()) && (t1.second() == t2.second());
}

template<>
template<class T1, class T2>
inline bool EmulatedPair_Equal<EMP_BY_FIRST_ONLY>::operator()(const T1& t1, const T2& t2) {
	return t1.first() == t2.first();
}

template<>
template<class T1, class T2>
inline bool EmulatedPair_Equal<EMP_BY_SECOND_ONLY>::operator()(const T1& t1, const T2& t2) {
	return t1.second() == t2.second();
}

} //namespace systematic

#endif //Systematic_EmulatedPair_H_
