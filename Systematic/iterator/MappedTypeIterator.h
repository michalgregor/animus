#ifndef Systematic_MappedTypeIterator_H_
#define Systematic_MappedTypeIterator_H_

#include <iterator>
#include <boost/type_traits/is_const.hpp>
#include <boost/type_traits/remove_reference.hpp>
#include <boost/type_traits/conditional.hpp>

#include <Systematic/configure.h>

namespace systematic {
namespace detail {

template<class IterType>
struct MappedTypeIterator_ValueType {
	typedef typename boost::conditional<
		boost::is_const<typename boost::remove_reference<
			typename IterType::reference>::type>::value,
		const typename IterType::value_type::second_type,
		typename IterType::value_type::second_type
	>::type value_type;
};

} //namespace detail

/**
 * @class MappedTypeIterator
 * @author Michal Gregor
 * An adaptor that wraps in a map iterator and acts as an iterator
 * to the mapped type (MapType::mapped_type).
 * @tparam IterType Is either MapType::iterator or MapType::const_iterator.
 */

template<class IterType>
class MappedTypeIterator {
public:
	typedef typename IterType::value_type::second_type value_type;
	typedef typename IterType::difference_type difference_type;
	typedef typename detail::MappedTypeIterator_ValueType<IterType>::value_type* pointer;
	typedef typename detail::MappedTypeIterator_ValueType<IterType>::value_type& reference;
	typedef std::bidirectional_iterator_tag iterator_category;

private:
	template<class OtherIterType>
	friend class MappedTypeIterator;

	//! The underlying iterator.
	IterType _iter;

public:
	IterType getRawIterator() const {
		return _iter;
	}

	bool operator==(const MappedTypeIterator& iter) const {
		return _iter == iter._iter;
	}

	bool operator!=(const MappedTypeIterator& iter) const {
		return _iter != iter._iter;
	}

	reference operator*() const {
		return _iter->second;
	}

	pointer operator->() const {
		return &(_iter->second);
	}

	MappedTypeIterator& operator++() {
		_iter++;
		return *this;
	}

	MappedTypeIterator operator++(int) {
		MappedTypeIterator tmpiter(_iter);
		_iter++;
		return tmpiter;
	}

	MappedTypeIterator& operator--() {
		_iter--;
		return *this;
	}

	MappedTypeIterator operator--(int) {
		MappedTypeIterator tmpiter(_iter);
		_iter--;
		return tmpiter;
	}

	/**
	 * An assignment operator that also allows casts from iterator to
	 * const iterator.
	 */

	template<class OtherIterType>
	MappedTypeIterator& operator=(
	        const MappedTypeIterator<OtherIterType>& iter) {
		_iter = iter._iter;
		return *this;
	}

	/**
	 * A copy constructor that also allows casts from iterator to
	 * const iterator.
	 */

	template<class OtherIterType>
	MappedTypeIterator(
	        const MappedTypeIterator<OtherIterType>& iter) :
			_iter(iter._iter) {
	}

	/**
	 * Constructor. Enables construction of MappedTypeIterator<IterType> from
	 * IterType.
	 */

	MappedTypeIterator(IterType iter) :
			_iter(iter) {
	}

	/**
	 * Default constructor.
	 */

	MappedTypeIterator() :
			_iter() {
	}
};

} //namespace systematic

#endif //Systematic_MappedTypeIterator_H_
