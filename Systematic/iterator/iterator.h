#ifndef Systematic_iterator_H_
#define Systematic_iterator_H_

#include <iterator>

namespace systematic {

	using std::begin;
	using std::end;

	template<class C>
	constexpr auto cbegin(const C& c) -> decltype(begin(c)) {
		return begin(c);
	}

	template<class C>
	constexpr auto cend(const C& c) -> decltype(end(c)) {
		return end(c);
	}

	template<class C>
	constexpr auto rbegin(C& c) -> decltype(c.rbegin()) {
		return c.rbegin();
	}

	template<class C>
	constexpr auto rend(C& c) -> decltype(c.rend()) {
		return c.rend();
	}

	template<class C>
	constexpr auto crbegin(const C& c) -> decltype(rbegin(c)) {
		return rbegin(c);
	}

	template<class C>
	constexpr auto crend(const C& c) -> decltype(rend(c)) {
		return rend(c);
	}

} //namespace systematic

#endif //Systematic_iterator_H_
