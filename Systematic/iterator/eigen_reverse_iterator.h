#ifndef Systematic_eigen_reverse_iterator_H_
#define Systematic_eigen_reverse_iterator_H_

#include <iterator>
#include <type_traits>

namespace systematic {

template<class Iterator>
class eigen_reverse_iterator {
public:
	typedef Iterator iterator_type;
	typedef typename std::iterator_traits<Iterator>::difference_type difference_type;
	typedef typename std::iterator_traits<Iterator>::reference reference;
	typedef typename std::iterator_traits<Iterator>::pointer pointer;
	typedef typename std::iterator_traits<Iterator>::value_type value_type;
	typedef typename std::iterator_traits<Iterator>::iterator_category iterator_category;

private:
	//! The base() iterator - 1.
	Iterator iter() const {
		auto tmpIter = const_cast<typename std::remove_const<Iterator>::type&>(_current);
		--tmpIter;
		return tmpIter;
	}

public:
	Iterator base() const {
		return _current;
	}

	reference operator*() const {
		return *iter();
	}

	pointer operator->() const {
		return iter().operator->();
	}

	eigen_reverse_iterator& operator++() {
		_current--;
		return *this;
	}

	eigen_reverse_iterator operator++(int) {
		eigen_reverse_iterator tmpIter(*this);
		_current--;
		return tmpIter;
	}

	eigen_reverse_iterator& operator--() {
		_current++;
		return *this;
	}

	eigen_reverse_iterator operator--(int) {
		eigen_reverse_iterator tmpIter(*this);
		_current++;
		return tmpIter;
	}

	eigen_reverse_iterator operator+(difference_type n) const {
		return eigen_reverse_iterator(_current - n);
	}

	eigen_reverse_iterator operator-(difference_type n) const {
		return eigen_reverse_iterator(_current + n);
	}

	template<typename IteratorB>
	difference_type operator-(const eigen_reverse_iterator<IteratorB>& iter_) const {
		return iter_._current - _current;
	}

	template<typename IteratorB>
	bool operator<(const eigen_reverse_iterator<IteratorB>& iter_) const {
		return _current > iter_._current;
	}

	template<typename IteratorB>
	bool operator>(const eigen_reverse_iterator<IteratorB>& iter_) const {
		return _current < iter_._current;
	}

	template<typename IteratorB>
	bool operator<=(const eigen_reverse_iterator<IteratorB>& iter_) const {
		return _current >= iter_._current;
	}

	template<typename IteratorB>
	bool operator>=(const eigen_reverse_iterator<IteratorB>& iter_) const {
		return _current <= iter_._current;
	}

	eigen_reverse_iterator& operator+=(difference_type n) {
		_current -= n;
		return *this;
	}

	eigen_reverse_iterator& operator-=(difference_type n) {
		_current += n;
		return *this;
	}

	reference operator[](difference_type n) {
		return *(*this + n);
	}

public:
	value_type& value() const {
		return iter().value();
	}

	AlgebraIndex index() const {
		return iter().index();
	}

	AlgebraIndex outer() const {
		return iter().outer();
	}

	AlgebraIndex row() const {
		return iter().row();
	}

	AlgebraIndex col() const {
		return iter().col();
	}

protected:
	Iterator _current;

public:
	eigen_reverse_iterator() = default;

	explicit eigen_reverse_iterator(Iterator x): _current(x) {}

	template<class U>
	eigen_reverse_iterator(const eigen_reverse_iterator<U>& u):
		_current(u.base()) {}

	template<class U>
	eigen_reverse_iterator& operator=(const eigen_reverse_iterator<U>& u) {
		_current = u.base();
		return *this;
	}
};

template<class Iterator1, class Iterator2>
inline bool operator==(const eigen_reverse_iterator<Iterator1>& x,
        const eigen_reverse_iterator<Iterator2>& y) {
	return x.base() == y.base();
}

template<class Iterator1, class Iterator2>
inline bool operator!=(const eigen_reverse_iterator<Iterator1>& x,
        const eigen_reverse_iterator<Iterator2>& y) {
	return x.base() != y.base();
}

} //namespace systematic

#endif //Systematic_eigen_reverse_iterator_H_
