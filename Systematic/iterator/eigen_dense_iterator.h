#ifndef Systematic_eigen_dense_iterator_H_
#define Systematic_eigen_dense_iterator_H_

#include <type_traits>
#include "iterator.h"
#include "../math/algebra_decl.h"
#include "../type_traits/matrix_type.h"
#include <Systematic/pointer/EmulatedPtr.h>

namespace systematic {

template<typename Derived, bool IterateRows = true>
class EigenMatrixIterator {
public:
	typedef decltype(static_cast<Derived*>(0)->row(0)) row_type;
	typedef decltype(static_cast<Derived*>(0)->col(0)) col_type;

	typedef ptrdiff_t difference_type;
	typedef typename std::conditional<IterateRows, row_type, col_type>::type value_type;
	typedef EmulatedPtr<value_type> pointer;
	typedef value_type reference;
	typedef std::random_access_iterator_tag iterator_category;

	template<class DerivedB, bool IterateRowsB>
	friend class EigenMatrixIterator;

	inline static constexpr std::integral_constant<bool, IterateRows>
		IterateRowsValue() {return {};}

protected:
	inline reference block(AlgebraIndex index, std::true_type) const {
		return _expression->row(index);
	}

	inline reference block(AlgebraIndex index, std::false_type) const {
		return _expression->col(index);
	}

	inline reference block(AlgebraIndex index) const {
		return block(index, IterateRowsValue());
	}

	static constexpr inline AlgebraIndex size_bound(const Derived& mat, std::true_type) {
		return mat.rows();
	}

	static constexpr inline AlgebraIndex size_bound(const Derived& mat, std::false_type) {
		return mat.cols();
	}

public:
	static constexpr inline AlgebraIndex size_bound(const Derived& mat) {
		return size_bound(mat, IterateRowsValue());
	}

protected:
	Derived* _expression;
	AlgebraIndex _index;

public:
	bool operator==(const EigenMatrixIterator& iter) const {
		SYS_ASSERT_MSG(_expression == iter._expression, "Cannot compare iterators from two distinct expressions.");
		return _index == iter._index;
	}

	bool operator!=(const EigenMatrixIterator& iter) const {
		return !(*this == iter);
	}

	inline EigenMatrixIterator& operator++() {
		_index++;
		return *this;
	}

	inline EigenMatrixIterator operator++(int) {
		EigenMatrixIterator tmpIter(*this);
		_index++;
		return tmpIter;
	}

	inline EigenMatrixIterator& operator--() {
		_index--;
		return *this;
	}

	inline EigenMatrixIterator operator--(int) {
		EigenMatrixIterator tmpIter(*this);
		_index--;
		return tmpIter;
	}

	inline EigenMatrixIterator operator+(difference_type n) const {
		return EigenVectorIterator(*_expression, _index + n);
	}

	inline EigenMatrixIterator operator-(difference_type n) const {
		return EigenVectorIterator(*_expression, _index - n);
	}

	template<typename DerivedB, bool IterateRowsB>
	difference_type operator-(const EigenMatrixIterator<DerivedB, IterateRowsB>& iter) const {
		static_assert(IterateRowsValue() == iter.IterateRowsValue(), "Both iterators must be iterating over the same dimension.");
		if(_expression != iter._expression)
			throw TracedError_InvalidArgument("_expression != iter._expression");
		return static_cast<difference_type>(_index) - static_cast<difference_type>(_index);
	}

	template<typename DerivedB, bool IterateRowsB>
	bool operator<(const EigenMatrixIterator<DerivedB, IterateRowsB>& iter) const {
		if(_expression != iter._expression)
			throw TracedError_InvalidArgument("_expression != iter._expression");

		if(_index < iter._index) return true;
		else return false;
	}

	template<typename DerivedB, bool IterateRowsB>
	bool operator>(const EigenMatrixIterator<DerivedB, IterateRowsB>& iter) const {
		if(_expression != iter._expression)
			throw TracedError_InvalidArgument("_expression != iter._expression");

		if(_index > iter._index) return true;
		else return false;
	}

	template<typename DerivedB, bool IterateRowsB>
	bool operator<=(const EigenMatrixIterator<DerivedB, IterateRowsB>& iter) const {
		if(_expression != iter._expression)
			throw TracedError_InvalidArgument("_expression != iter._expression");

		if(_index < iter._index) return true;
		else return false;
	}

	template<typename DerivedB, bool IterateRowsB>
	bool operator>=(const EigenMatrixIterator<DerivedB, IterateRowsB>& iter) const {
		if(_expression != iter._expression)
			throw TracedError_InvalidArgument("_expression != iter._expression");

		if(_index > iter._index) return true;
		else return false;
	}

	EigenMatrixIterator& operator+=(difference_type n) {
		_index = numeric_cast<AlgebraIndex>(_index + n);
		return *this;
	}

	EigenMatrixIterator& operator-=(difference_type n) {
		_index = numeric_cast<AlgebraIndex>(_index - n);
		return *this;
	}

	reference operator[](difference_type n) {
		return *(*this + n);
	}

	value_type* operator->() const {
		return pointer(block(_index));
	}

	reference operator*() const {
		return block(_index);
	}

	value_type value() const {
		return block(_index);
	}

	AlgebraIndex index() const {
		return _index;
	}

public:
	EigenMatrixIterator& operator=(const EigenMatrixIterator& obj) = default;
	EigenMatrixIterator(const EigenMatrixIterator& obj) = default;

	EigenMatrixIterator():
		_expression(nullptr), _index(0) {}

	EigenMatrixIterator(Derived& expr, AlgebraIndex index):
		_expression(&expr), _index(index) {}

	template<class IterType>
	EigenMatrixIterator(const IterType& iter):
		_expression(iter._expression), _index(iter._index) {}
};

template<typename Derived, bool IterateInner>
class EigenVectorIterator {
public:
	typedef typename std::conditional<std::is_const<Derived>::value,
		const typename Derived::Scalar, typename Derived::Scalar>::type ScalarType;

	typedef ptrdiff_t difference_type;
	typedef ScalarType value_type;
	typedef ScalarType* pointer;
	typedef ScalarType& reference;
	typedef std::random_access_iterator_tag iterator_category;

	template<class DerivedB, bool IterateInnerB>
	friend class EigenVectorIterator;

	inline static constexpr std::integral_constant<bool, IterateInner>
		IterateInnerValue() {return {};}

	inline static constexpr std::integral_constant<bool, Derived::IsRowMajor>
		IsRowMajorValue() {return {};}

protected:
	Derived* _expression;
	AlgebraIndex _inner;
	AlgebraIndex _outer;

protected:
	inline AlgebraIndex& iter_index(std::true_type) {return _inner;}
	inline AlgebraIndex& iter_index(std::false_type) {return _outer;}
	inline AlgebraIndex iter_index(std::true_type) const {return _inner;}
	inline AlgebraIndex iter_index(std::false_type) const {return _outer;}

	inline AlgebraIndex& other_index(std::true_type) {return _outer;}
	inline AlgebraIndex& other_index(std::false_type) {return _inner;}
	inline AlgebraIndex other_index(std::true_type) const {return _outer;}
	inline AlgebraIndex other_index(std::false_type) const {return _inner;}

	value_type* op_arrow(std::true_type) const {
		return &(_expression->coeffRef(_outer, _inner));
	}

	value_type* op_arrow(std::false_type) const {
		return &(_expression->coeffRef(_inner, _outer));
	}

	value_type& op_deref(std::true_type) const {
		return _expression->coeffRef(_outer, _inner);
	}

	value_type& op_deref(std::false_type) const {
		return _expression->coeffRef(_inner, _outer);
	}

public:
	inline AlgebraIndex& iter_index() {return iter_index(IterateInnerValue());}
	inline AlgebraIndex iter_index() const {return iter_index(IterateInnerValue());}
	inline AlgebraIndex& other_index() {return other_index(IterateInnerValue());}
	inline AlgebraIndex other_index() const {return other_index(IterateInnerValue());}

	bool operator==(const EigenVectorIterator& iter) const {
		SYS_ASSERT_MSG(_expression == iter._expression, "Cannot compare iterators from two distinct expressions.");
		return (_inner == iter._inner) && (_outer == iter._outer);
	}

	bool operator!=(const EigenVectorIterator& iter) const {
		return !(*this == iter);
	}

	inline EigenVectorIterator& operator++() {
		iter_index()++;
		return *this;
	}

	inline EigenVectorIterator operator++(int) {
		EigenVectorIterator tmpIter(*this);
		iter_index()++;
		return tmpIter;
	}

	inline EigenVectorIterator& operator--() {
		iter_index()--;
		return *this;
	}

	inline EigenVectorIterator operator--(int) {
		EigenVectorIterator tmpIter(*this);
		iter_index()--;
		return tmpIter;
	}

	inline EigenVectorIterator operator+(difference_type n) const {
		return EigenVectorIterator(*_expression, iter_index() + n, _outer);
	}

	inline EigenVectorIterator operator-(difference_type n) const {
		return EigenVectorIterator(*_expression, iter_index() - n, _outer);
	}

	template<typename DerivedB, bool IterateInnerB>
	difference_type operator-(const EigenVectorIterator<DerivedB, IterateInnerB>& iter) const {
		static_assert(IterateInnerValue() == iter.IterateInnerValue(), "Both iterators must be iterating over the same dimension.");
		if(_expression != iter._expression or other_index() != iter.other_index())
			throw TracedError_InvalidArgument("_expression != iter._expression or indices in the other dimension do not agree");
		return static_cast<difference_type>(iter_index()) - static_cast<difference_type>(iter.iter_index());
	}

	template<typename DerivedB, bool IterateInnerB>
	bool operator<(const EigenVectorIterator<DerivedB, IterateInnerB>& iter) const {
		if(_expression != iter._expression)
			throw TracedError_InvalidArgument("_expression != iter._expression");

		if(_outer < iter._outer) return true;
		else if(_outer == iter._outer && _inner < iter._inner) return true;
		else return false;
	}

	template<typename DerivedB, bool IterateInnerB>
	bool operator>(const EigenVectorIterator<DerivedB, IterateInnerB>& iter) const {
		if(_expression != iter._expression)
			throw TracedError_InvalidArgument("_expression != iter._expression");

		if(_outer > iter._outer) return true;
		else if(_outer == iter._outer && _inner > iter._inner) return true;
		else return false;
	}

	template<typename DerivedB, bool IterateInnerB>
	bool operator<=(const EigenVectorIterator<DerivedB, IterateInnerB>& iter) const {
		if(_expression != iter._expression)
			throw TracedError_InvalidArgument("_expression != iter._expression");

		if(_outer < iter._outer) return true;
		else if(_outer == iter._outer && _inner <= iter._inner) return true;
		else return false;
	}

	template<typename DerivedB, bool IterateInnerB>
	bool operator>=(const EigenVectorIterator<DerivedB, IterateInnerB>& iter) const {
		if(_expression != iter._expression)
			throw TracedError_InvalidArgument("_expression != iter._expression");

		if(_outer > iter._outer) return true;
		else if(_outer == iter._outer && _inner >= iter._inner) return true;
		else return false;
	}

	EigenVectorIterator& operator+=(difference_type n) {
		iter_index() = numeric_cast<AlgebraIndex>(iter_index() + n);
		return *this;
	}

	EigenVectorIterator& operator-=(difference_type n) {
		iter_index() = numeric_cast<AlgebraIndex>(iter_index() - n);
		return *this;
	}

	reference operator[](difference_type n) {
		return *(*this + n);
	}

	value_type* operator->() const {
		return op_arrow(IsRowMajorValue());
	}

	value_type& operator*() const {
		return op_deref(IsRowMajorValue());
	}

	value_type& value() const {
		return op_deref(IsRowMajorValue());
	}

	AlgebraIndex inner() const {
		return _inner;
	}

	AlgebraIndex outer() const {
		return _outer;
	}

	AlgebraIndex row() const {
		return Derived::IsRowMajor ? _outer : _inner;
	}

	AlgebraIndex col() const {
		return Derived::IsRowMajor ? _inner : _outer;
	}

public:
	EigenVectorIterator& operator=(const EigenVectorIterator& obj) = default;
	EigenVectorIterator(const EigenVectorIterator& obj) = default;

	EigenVectorIterator():
		_expression(nullptr), _inner(0), _outer(0) {}

	EigenVectorIterator(Derived& expr, AlgebraIndex inner_, AlgebraIndex outer_):
		_expression(&expr), _inner(inner_), _outer(outer_) {}

	template<class IterType>
	EigenVectorIterator(const IterType& iter):
		_expression(iter._expression), _inner(iter._inner),
		_outer(iter._outer) {}
};

namespace detail {

template<class VectorType>
class IterateOverInner {
public:
	static constexpr bool value =
		// row vector and row majority
		(VectorType::RowsAtCompileTime != Eigen::Dynamic and VectorType::IsRowMajor) or
		// col vector and col majority
		(VectorType::ColsAtCompileTime != Eigen::Dynamic and !VectorType::IsRowMajor);

public:
	static constexpr inline AlgebraIndex inner_bound(const VectorType& vec) {
		return value ? vec.size() : 0;
	}

	static constexpr inline AlgebraIndex outer_bound(const VectorType& vec) {
		return value ? 0 : vec.size();
	}
};

template<class Derived, bool IterateRows>
class MatrixSpecialIter {
private:
	Derived& _expression;

public:
	inline static constexpr std::integral_constant<bool, IterateRows>
		IterateRowsValue() {return {};}

	Derived& expression() const {
		return _expression;
	}

	MatrixSpecialIter(Derived& expression_): _expression(expression_) {}
};

} // namespace detail
} // namespace systematic

namespace Eigen {

template<class MatrixType>
struct IterateRowsTrait {
	static constexpr bool value = true;
	static constexpr bool is_special = false;
	typedef MatrixType matrix_type;
};

// vector iteration

template<class VectorType>
using VectorIter =
	systematic::EigenVectorIterator<typename std::remove_reference<VectorType>::type,
	systematic::detail::IterateOverInner<typename std::remove_reference<VectorType>::type>::value>;

template<class VectorType>
using EnableVectorIter =
	typename std::enable_if<std::remove_reference<VectorType>::type::IsVectorAtCompileTime,
		VectorIter<VectorType>
	>::type;

template<class VectorType>
using VectorReverseIter =
	std::reverse_iterator<systematic::EigenVectorIterator<typename std::remove_reference<VectorType>::type,
	systematic::detail::IterateOverInner<typename std::remove_reference<VectorType>::type>::value> >;

template<class VectorType>
using EnableVectorReverseIter =
	typename std::enable_if<std::remove_reference<VectorType>::type::IsVectorAtCompileTime,
		VectorReverseIter<VectorType>
	>::type;

template<class VectorType>
using OverInner = systematic::detail::IterateOverInner<typename std::remove_reference<VectorType>::type>;

template<class VectorType>
EnableVectorIter<VectorType> begin(VectorType&& vec) {
	static_assert(!std::is_rvalue_reference<decltype(vec)>::value, "Cannot be used with an rvalue.");
	return {vec, 0, 0};
}

template<class VectorType>
EnableVectorIter<VectorType> end(VectorType&& vec) {
	static_assert(!std::is_rvalue_reference<decltype(vec)>::value, "Cannot use begin on an rvalue.");
	return {vec, OverInner<VectorType>::inner_bound(vec), OverInner<VectorType>::outer_bound(vec)};
}

template<class VectorType>
EnableVectorReverseIter<VectorType> rbegin(VectorType&& vec) {
	static_assert(!std::is_rvalue_reference<decltype(vec)>::value, "Cannot use begin on an rvalue.");
	return VectorReverseIter<VectorType>({vec, OverInner<VectorType>::inner_bound(vec), OverInner<VectorType>::outer_bound(vec)});
}

template<class VectorType>
EnableVectorReverseIter<VectorType> rend(VectorType&& vec) {
	static_assert(!std::is_rvalue_reference<decltype(vec)>::value, "Cannot use begin on an rvalue.");
	return VectorReverseIter<VectorType>({vec, 0, 0});
}

// matrix iteration

template<class MatrixType>
using MatrixIter = systematic::EigenMatrixIterator<typename std::remove_reference<MatrixType>::type, IterateRowsTrait<MatrixType>::value>;

template<class MatrixType>
using EnableMatrixIter =
	typename std::enable_if<!std::remove_reference<MatrixType>::type::IsVectorAtCompileTime,
		MatrixIter<MatrixType>
	>::type;

template<class MatrixType>
using MatrixReverseIter = std::reverse_iterator<systematic::EigenMatrixIterator<typename std::remove_reference<MatrixType>::type, IterateRowsTrait<MatrixType>::value> >;

template<class MatrixType>
using EnableMatrixReverseIter =
	typename std::enable_if<!std::remove_reference<MatrixType>::type::IsVectorAtCompileTime,
		MatrixReverseIter<MatrixType>
	>::type;

template<class MatrixType>
EnableMatrixIter<MatrixType> begin(MatrixType&& mat) {
	static_assert(!std::is_rvalue_reference<decltype(mat)>::value, "Cannot use begin on an rvalue.");
	return {mat, 0};
}

template<class MatrixType>
EnableMatrixIter<MatrixType> end(MatrixType&& mat) {
	static_assert(!std::is_rvalue_reference<decltype(mat)>::value, "Cannot use begin on an rvalue.");
	return {mat, EnableMatrixIter<MatrixType>::size_bound(mat)};
}

template<class MatrixType>
EnableMatrixReverseIter<MatrixType> rbegin(MatrixType&& mat) {
	static_assert(!std::is_rvalue_reference<decltype(mat)>::value, "Cannot use begin on an rvalue.");
	return MatrixReverseIter<MatrixType>({mat, EnableMatrixIter<MatrixType>::size_bound(mat)});
}

template<class MatrixType>
EnableMatrixReverseIter<MatrixType> rend(MatrixType&& mat) {
	static_assert(!std::is_rvalue_reference<decltype(mat)>::value, "Cannot use begin on an rvalue.");
	return MatrixReverseIter<MatrixType>({mat, 0});
}

} // namespace Eigen

namespace systematic {

using Eigen::begin;
using Eigen::end;

} // namespace systematic

#endif //Systematic_eigen_dense_iterator_H_

