#ifndef POLYITERATOR_FORWARD_H_
#define POLYITERATOR_FORWARD_H_

#include "../system.h"

namespace systematic {

namespace detail {

//------------------------------------------------------------------------------
//---------------------------forward declaration--------------------------------
//------------------------------------------------------------------------------

template<class IterType>
class PolyIteratorForward_Derived;

//------------------------------------------------------------------------------
//-----------------------------forward-base-------------------------------------
//------------------------------------------------------------------------------

/*
* @class PolyIteratorForward_Base
* @author Michal Gregor
*
* An ABC providing a common interface for classes wrapping iterator types in
* a polymorphic manner.
*
* @tparam value_type Value type of the wrapped-in iterator.
*/

SYS_WNONVIRT_DTOR_OFF

template<class value_type>
class PolyIteratorForward_Base {
private:
	/*
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

private:
	// This section is private so that no classes other than the friend class
	// are able to derive from this base.
	template<class IterType>
	friend class PolyIteratorForward_Derived;

	// Derived classes use this method to retrieve their unique ID used when
	// comparing instances of derived classes.
	static int getNextDerivedID() {
		static int id = 0;
		return ++id;
	}

	//Returns the unique ID of the derived class.
	virtual int getID() const = 0;

	PolyIteratorForward_Base() {}

public:
	typedef size_t difference_type;

	virtual bool equals(const PolyIteratorForward_Base& iter) const = 0;
	virtual const value_type* getPtr() const = 0;
	virtual value_type* getPtr() = 0;
	virtual void increment() = 0;

	virtual ~PolyIteratorForward_Base() = 0;
};

/*
 * Empty body of the pure virtual destructor.
 */

template<class value_type>
PolyIteratorForward_Base<value_type>::~PolyIteratorForward_Base() {}

SYS_WNONVIRT_DTOR_ON

//------------------------------------------------------------------------------
//----------------------------forward-derived-----------------------------------
//------------------------------------------------------------------------------

template<class IterType>
class PolyIteratorForward_Derived:
public PolyIteratorForward_Base<typename std::iterator_traits<IterType>::value_type> {
private:
	/*
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//Returns the unique ID of the derived class.
	virtual int getID() const {
		static int id = this->getNextDerivedID();
		return id;
	}

	//Stores the underlying iterator.
	IterType _iter;

public:
	typedef typename std::iterator_traits<IterType>::value_type value_type;

public:
	bool equals(const PolyIteratorForward_Derived& obj) const {
		return _iter == obj._iter;
	}

public:
	virtual bool equals(const PolyIteratorForward_Base<typename std::iterator_traits<IterType>::value_type>& iter) const {
		//check whether the iterator is of the same type; if so, it can be
		//safely cast using static_cast.
		if(getID() != iter.getID()) throw TracedError_InvalidArgument("The iterator is not of the same derived type.");
		return equals(static_cast<const PolyIteratorForward_Derived&>(iter));
	}

	virtual const value_type* getPtr() const {
		return _iter.operator->();
	}

	virtual value_type* getPtr() {
		return _iter.operator->();
	}

	virtual void increment() {
		_iter++;
	}

	PolyIteratorForward_Derived(const PolyIteratorForward_Derived& obj):
	_iter(obj._iter) {}

	PolyIteratorForward_Derived(const IterType& iter): _iter(iter) {}

	virtual ~PolyIteratorForward_Derived() {}
};

} //namespace detail

//------------------------------------------------------------------------------
//------------------------PolyIterator_Forward---------------------------
//------------------------------------------------------------------------------

/**
 * @class PolyIterator_Forward
 * @author Michal Gregor
 *
 * A wrapper class for an arbitrary kind of forward iterator. This class can
 * be used in place of standard iterators in a virtual method (virtual methods
 * cannot be templates and thus type of the iterator would have to be fixed
 * otherwise).
 */

template<class value_type>
class PolyIterator_Forward:
public std::iterator<
	std::forward_iterator_tag,
	value_type,
	typename detail::PolyIteratorForward_Base<value_type>::difference_type
> {
private:
	//! Pointer to the underlying wrapped iterator.
	detail::PolyIteratorForward_Base<value_type>* _polyIter;

public:
	/**
	 * Throws a TracedError_InvalidArgument when two PolyIterator_Forward
	 * iterators constructed from different underlying iterators are compared.
	 *
	 * Throws TracedError_InvalidPointer if one or both of the iterators are
	 * uninitialized.
	 */
	bool operator==(const PolyIterator_Forward& iter) const {
		if(!(_polyIter && iter._polyIter)) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->equals(*(iter._polyIter));
	}

	/**
	 * Throws a TracedError_InvalidArgument when two PolyIterator_Forward
	 * iterators constructed from different underlying iterators are compared.
	 *
	 * Throws TracedError_InvalidPointer if one or both of the iterators are
	 * uninitialized.
	 */

	bool operator!=(const PolyIterator_Forward& iter) const {
		if(!(_polyIter && iter._polyIter)) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return !(_polyIter->equals(*(iter._polyIter)));
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	const value_type* operator->() const {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->getPtr();
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	value_type* operator->() {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return _polyIter->getPtr();
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	const value_type& operator*() const {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return *(_polyIter->getPtr());
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	value_type& operator*() {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		return *(_polyIter->getPtr());
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Forward& operator++() {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		_polyIter->increment();
		return *this;
	}

	/**
	* Throws TracedError_InvalidPointer if the iterator has not been initialized.
	 */

	PolyIterator_Forward operator++(int) {
		if(!_polyIter) throw TracedError_InvalidPointer("Uninitialized iterator.");
		PolyIterator_Forward tmp(*this);
		_polyIter->increment();
		return tmp;
	}

	PolyIterator_Forward& operator=(const PolyIterator_Forward& obj) {
		if(this != &obj)  {
			if(_polyIter) delete _polyIter;
			_polyIter = clone(obj._polyIter);
		}
		return *this;
	}

	PolyIterator_Forward(const PolyIterator_Forward& obj):
	_polyIter(clone(obj._polyIter)) {}

	/**
	 * Template constructor. Allows construction of PolyIterator_Forward
	 * from an arbitrary forward iterator so long as its value type is the same
	 * as the value_type template parameter.
	 */

	template<class IterType>
	explicit PolyIterator_Forward(const IterType& iter):
	_polyIter(new detail::PolyIteratorForward_Derived<IterType>(iter)) {}

	/**
	 * Default constructor. Initialize using operator= before use.
	 */

	PolyIterator_Forward(): _polyIter(NULL) {}

	/**
	 * Destructor.
	 */

	~PolyIterator_Forward() {
		if(_polyIter) delete _polyIter;
	}
};

} //namespace systematic

#endif /* POLYITERATOR_FORWARD_H_ */
