#ifndef Systematic_checked_advance_H_
#define Systematic_checked_advance_H_

#include <iterator>
#include <Systematic/configure.h>

namespace systematic {
namespace detail {
	/*
	 * A helper class. Implements checked advance for iterators and provides
	 * a specialized version for random access iterators.
	 */

	template <class IterType, class IterCategory, class DistanceType>
	struct CheckedAdvanceHelper {
		static void advance(IterType& iter, DistanceType n, const IterType& last) {
			for(unsigned int i = 0; i < n; i++) {
				if(iter == last) return;
				iter++;
			}
		}
	};

	/*
	 * Specialization for random access iterators.
	 */

	template <class IterType, class DistanceType>
	struct CheckedAdvanceHelper<IterType, std::random_access_iterator_tag, DistanceType> {
		static void advance(IterType& iter, DistanceType n, const IterType& last) {
			if(last - iter <= n) iter += n;
			else iter = last;
		}
	};
}

/**
 * Advances iterator iter by n points or until last is reached, whichever
 * happens first.
 *
 * Includes a specialization for random access iterators.
 */

template<class IterType, class DistanceType>
void checked_advance(IterType& iter, DistanceType n, const IterType& last) {
	detail::CheckedAdvanceHelper<IterType, typename IterType::iterator_category, DistanceType>::advance(iter, n, last);
}

} //namespace systematic

#endif //Systematic_checked_advance_H_
