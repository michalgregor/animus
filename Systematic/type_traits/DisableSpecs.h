#ifndef DISABLESPECS_H_INCLUDED
#define DISABLESPECS_H_INCLUDED

#include <Systematic/configure.h>

namespace systematic {

	/**
	* @struct DisableSpecs
	* A type used internally to switch to default template disabling all
	* enable_if specializations. This may be acchieved using any type, however
	* a convention of using DisableSpecs is maintained for clarity.
	**/

	struct SYS_API DisableSpecs {};

} //namespace systematic

#endif // DISABLESPECS_H_INCLUDED
