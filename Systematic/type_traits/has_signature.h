#ifndef Systematic_has_signature_H_
#define Systematic_has_signature_H_

#include <type_traits>

namespace systematic {
namespace detail {

template<class Functor>
class __has_signature_check: public Functor {
private:
	class Holder {};

public:
	void operator()(Holder&) {}
	using Functor::operator();
};

template<class Functor, class Return, class Enable, class... Params>
class __has_signature_impl: public std::false_type {};

template<class Functor, class Return, class... Params>
class __has_signature_impl<Functor, Return,
	decltype((void)(Return (__has_signature_check<Functor>::*)( Params... ))(&__has_signature_check<Functor>::operator())),
Params...>: public std::true_type {};

} //namespace detail

/**
 * @class has_signature
 *
 * Inherits from std::true_type when the specified Functor has the signature
 * Return (Params... params). Inherits from std::false_type otherwise.
 *
 * \note Only works for functor - not for function or lambdas.
 */
template<class Functor, class Return, class... Params>
class has_signature: public detail::__has_signature_impl<Functor, Return, void, Params...> {};

} //namespace systematic

#endif //Systematic_has_signature_H_
