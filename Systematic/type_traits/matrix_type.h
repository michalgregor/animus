#ifndef Systematic_matrix_type_H_
#define Systematic_matrix_type_H_

#include <type_traits>
#include "../macros.h"

namespace Eigen {
	class Dense;
	class Sparse;
} //namespace Eigen

namespace systematic {

SYS_WNONVIRT_DTOR_OFF

/**
 * @class is_dense
 *
 * Inherits from std::true_type iff MatrixType is a dense matrix.
 */
template<class MatrixType, class Enable = std::true_type>
class is_dense: public std::false_type {};

template<class MatrixType>
class is_dense<MatrixType, typename std::is_same<typename MatrixType::StorageKind, Eigen::Dense>::type>: public std::true_type {};

/**
 * @class is_sparse
 *
 * Inherits from std::true_type iff MatrixType is a sparse matrix.
 */
template<class MatrixType, class Enable = std::true_type>
class is_sparse: public std::false_type {};

template<class MatrixType>
class is_sparse<MatrixType, typename std::is_same<typename MatrixType::StorageKind, Eigen::Sparse>::type>: public std::true_type {};

SYS_WNONVIRT_DTOR_ON

} // namespace systematic

#endif //Systematic_matrix_type_H_
