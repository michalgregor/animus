#ifndef Systematic_IterRangeRAContainer_H_
#define Systematic_IterRangeRAContainer_H_

#include "../system.h"
#include <iterator>

namespace systematic {

/**
* @class IterRangeRAContainer
* @author Michal Gregor
*
* This class accepts iterators to the first element of a certain range
* and to the element one step past the end of the range and wraps them into
* an object that has an interface similar to containers.
*
* @tparam IterType Type of the iterator.
* @tparam ConstIterType The corresponding constant iterator type. Optional,
* defaults to IterType: therefore, there is no need to specify this if IterType
* already is a constant iterator type.
**/
template<class IterType, class ConstIterType = IterType, class SizeType = size_t>
class IterRangeRAContainer {
private:
	//! Iterator to the first element of the range.
	IterType _first;
	//! Iterator one step past the last element of the range.
	IterType _last;

public:
	typedef IterType iterator;
	typedef ConstIterType const_iterator;

	typedef SizeType size_type;
	typedef typename std::iterator_traits<IterType>::value_type value_type;

	typedef typename std::iterator_traits<IterType>::reference reference;
	typedef typename std::iterator_traits<ConstIterType>::reference const_reference;

public:
	iterator begin() {return _first;}
	iterator end() {return _last;}

	const_iterator begin() const {return _first;}
	const_iterator end() const {return _last;}

	reference operator[](size_type pos) {
		return _first[pos];
	}

	const_reference operator[](size_type pos) const {
		return _first[pos];
	}

	size_type size() const {return numeric_cast<size_type>(std::distance(_first, _last));}

	IterRangeRAContainer<IterType, ConstIterType, SizeType>& operator=(const IterRangeRAContainer<IterType, ConstIterType, SizeType>& obj);
	IterRangeRAContainer(const IterRangeRAContainer<IterType, ConstIterType, SizeType>& obj);

	IterRangeRAContainer();
	IterRangeRAContainer(const IterType& first, const IterType& last);
};

/**
* Assignment operator.
**/
template<class IterType, class ConstIterType, class SizeType>
IterRangeRAContainer<IterType, ConstIterType, SizeType>& IterRangeRAContainer<IterType, ConstIterType, SizeType>::
operator=(const IterRangeRAContainer<IterType, ConstIterType, SizeType>& obj) {
	if(&obj != this) {
		_first = obj._first;
		_last = obj._last;
	}

	return *this;
}

/**
* Copy constructor.
**/
template<class IterType, class ConstIterType, class SizeType>
IterRangeRAContainer<IterType, ConstIterType, SizeType>::
IterRangeRAContainer(const IterRangeRAContainer<IterType, ConstIterType, SizeType>& obj):
_first(obj._first), _last(obj._last) {}

/**
* Default constructor.
**/
template<class IterType, class ConstIterType, class SizeType>
IterRangeRAContainer<IterType, ConstIterType, SizeType>::IterRangeRAContainer():
_first(), _last() {}

/**
 * Constructor.
 * @param first An iterator to the first element of a certain range.
 * @param last An iterator to the element one step past the end of the range.
 */
template<class IterType, class ConstIterType, class SizeType>
IterRangeRAContainer<IterType, ConstIterType, SizeType>::
IterRangeRAContainer(const IterType& first, const IterType& last):
_first(first), _last(last) {}

}//namespace systematic

#endif //Systematic_IterRangeRAContainer_H_
