#ifndef Systematic_IterRangeContainer_H_
#define Systematic_IterRangeContainer_H_

#include "../system.h"
#include <iterator>

namespace systematic {

/**
* @class IterRangeContainer
* @author Michal Gregor
*
* This class accepts iterators to the first element of a certain range
* and to the element one step past the end of the range and wraps them into
* an object that has an interface similar to containers.
*
* @tparam IterType Type of the iterator.
* @tparam ConstIterType The corresponding constant iterator type. Optional,
* defaults to IterType: therefore, there is no need to specify this if IterType
* already is a constant iterator type.
**/
template<class IterType, class ConstIterType = IterType, class SizeType = size_t>
class IterRangeContainer {
private:
	//! Iterator to the first element of the range.
	IterType _first;
	//! Iterator one step past the last element of the range.
	IterType _last;

public:
	typedef IterType iterator;
	typedef ConstIterType const_iterator;

	typedef SizeType size_type;
	typedef typename std::iterator_traits<IterType>::value_type value_type;

public:
	iterator begin() {return _first;}
	iterator end() {return _last;}

	const_iterator begin() const {return _first;}
	const_iterator end() const {return _last;}

	size_type size() const {return numeric_cast<size_type>(std::distance(_first, _last));}

	IterRangeContainer<IterType, ConstIterType, SizeType>& operator=(const IterRangeContainer<IterType, ConstIterType, SizeType>& obj);
	IterRangeContainer(const IterRangeContainer<IterType, ConstIterType, SizeType>& obj);

	IterRangeContainer();
	IterRangeContainer(const IterType& first, const IterType& last);
};

/**
* Assignment operator.
**/
template<class IterType, class ConstIterType, class SizeType>
IterRangeContainer<IterType, ConstIterType, SizeType>& IterRangeContainer<IterType, ConstIterType, SizeType>::
operator=(const IterRangeContainer<IterType, ConstIterType, SizeType>& obj) {
	if(&obj != this) {
		_first = obj._first;
		_last = obj._last;
	}

	return *this;
}

/**
* Copy constructor.
**/
template<class IterType, class ConstIterType, class SizeType>
IterRangeContainer<IterType, ConstIterType, SizeType>::
IterRangeContainer(const IterRangeContainer<IterType, ConstIterType, SizeType>& obj):
_first(obj._first), _last(obj._last) {}

/**
* Default constructor.
**/
template<class IterType, class ConstIterType, class SizeType>
IterRangeContainer<IterType, ConstIterType, SizeType>::IterRangeContainer():
_first(), _last() {}

/**
 * Constructor.
 * @param first An iterator to the first element of a certain range.
 * @param last An iterator to the element one step past the end of the range.
 */
template<class IterType, class ConstIterType, class SizeType>
IterRangeContainer<IterType, ConstIterType, SizeType>::
IterRangeContainer(const IterType& first, const IterType& last):
_first(first), _last(last) {}

}//namespace systematic

#endif //Systematic_IterRangeContainer_H_
