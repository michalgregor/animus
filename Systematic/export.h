#ifndef SYS_EXPORT_H_INCLUDED
#define SYS_EXPORT_H_INCLUDED

#include <boost/preprocessor/repetition/enum_params.hpp>
#include <boost/serialization/export.hpp>
#include <boost/version.hpp>
#include "type_info.h"
#include <Systematic/configure.h>

/**
* SYS_EXPORT_TEMPLATE exports template polymorphic class for use with
* boost::serialization library. In order to accomplish this it is also necessary
* to include SYS_INSTANTIATION_EXPORT() into class's destructor (or any function
* that code is generated for at that matter).
* @param Template Name of the class template. The full path to the class must
* be specified (including namespaces).
* @param NUM_PARAMS Number of parameters that the template takes.
**/

#if BOOST_VERSION <= 104100

#define SYS_EXPORT_TEMPLATE_0(Template, NUM_PARAMS) 																\
namespace boost { 																									\
namespace archive { 																								\
namespace detail {																									\
namespace {																											\
    template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, class T) > 															\
    struct init_guid<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > > {										\
        static ::boost::archive::detail::guid_initializer<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > >			\
         const & guid_initializer;																					\
    };																												\
    template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, class T) >															\
    ::boost::archive::detail::guid_initializer< ::Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > > const &			\
        ::boost::archive::detail::init_guid<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > >::guid_initializer =	\
           ::boost::serialization::singleton<																		\
               ::boost::archive::detail::guid_initializer<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > >			\
           >::get_mutable_instance().export_guid(																	\
				::systematic::detail::TypeInfo<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > >::getName()			\
			);																										\
}																													\
}																													\
}																													\
}

/**
**/

//there used to be no SYS_EXPORT_CLASS in some older versions of BOOST,
//in that case AUXSYS_EXPORT_CLASS_0 is defined as BOOST_CLASS_EXPORT
//and SYS_EXPORT_CLASS_IMPLEMENT is empty
#define AUXSYS_EXPORT_CLASS_0 						\
BOOST_CLASS_EXPORT

#else

#define AUXSYS_INSTANCE_ADD_ARCHIVE(A) 										\
(void) (typename boost::archive::detail::_ptr_serialization_support<A,	T>::type*) 0;

namespace systematic {
namespace detail {

template<class T>
struct guid_initializer
{
    void export_guid(boost::mpl::false_) const {
        // generates the statically-initialized objects whose constructors
        // register the information allowing serialization of T objects
        // through pointers to their base classes.
        SYS_ARCHIVES(AUXSYS_INSTANCE_ADD_ARCHIVE)
    }
    void export_guid(boost::mpl::true_) const {}
    guid_initializer const & export_guid() const {
        BOOST_STATIC_WARNING(boost::is_polymorphic< T >::value);
        // note: exporting an abstract base class will have no effect
        // and cannot be used to instantitiate serialization code
        // (one might be using this in a DLL to instantiate code)
        //BOOST_STATIC_WARNING(! boost::serialization::is_abstract< T >::value);
        export_guid(boost::serialization::is_abstract< T >());
        return *this;
    }
};

template<typename T, class Aux = void>
struct init_guid;

//This is used by SYS_EXPORT_INSTANCE.
template<class T>
void init_guid_type_registrator(const T*) {
	#if BOOST_VERSION <= 104100
		(void)::boost::archive::detail::init_guid<T>::guid_initializer;
	#else
		(void)::systematic::detail::init_guid<T>::g;
	#endif
}

} // namespace detail
} // namespace systematic

/**
**/

#define AUXSYS_EXPORT_TEMPLATE_0(Template, NUM_PARAMS) 											\
SYS_WNONVIRT_DTOR_OFF																			\
namespace boost {                              													\
namespace serialization {                      													\
template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, class T) > 											\
struct guid_defined<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > >: boost::mpl::true_ {}; 	\
}}																								\
namespace systematic {                                   										\
namespace detail {                                       										\
template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, class T), class Aux>     								\
struct init_guid<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) >, Aux> {          				\
	static guid_initializer<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > > const & g;     	\
};																								\
template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, class T), class Aux>									\
guid_initializer<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > > 								\
const & init_guid<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) >, Aux>::g =        				\
        ::boost::serialization::singleton<                   									\
            guid_initializer<Template<BOOST_PP_ENUM_PARAMS(NUM_PARAMS, T) > >                   \
        >::get_mutable_instance().export_guid();             									\
}}																								\
SYS_WNONVIRT_DTOR_OFF

/**
**/

#include <boost/preprocessor/seq/for_each_i.hpp>
#include <boost/preprocessor/tuple/to_seq.hpp>
#include <boost/preprocessor/comparison/less.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>
#include <boost/preprocessor/arithmetic/dec.hpp>

#define AUXSYS_EXPORT_TEMPLATE_DETAILED_0(Template, NUM_PARAMS, TPARAMS) 						\
SYS_WNONVIRT_DTOR_OFF																			\
namespace boost {                              													\
namespace serialization {                      													\
template<																						\
	BOOST_PP_SEQ_FOR_EACH_I(																	\
		AUXSYS_REGISTER_FOR_EACH_TPARAM_TYPE,													\
		NUM_PARAMS,																				\
		BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)												\
	) 																							\
> 																								\
struct guid_defined<																			\
	Template<																					\
		BOOST_PP_SEQ_FOR_EACH_I(																\
			AUXSYS_REGISTER_FOR_EACH_TPARAM_VAL,												\
			NUM_PARAMS,																			\
			BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)											\
		)																						\
	>																							\
>: boost::mpl::true_ {}; 																		\
}}																								\
namespace systematic {                                   										\
namespace detail {                                       										\
template<																						\
	BOOST_PP_SEQ_FOR_EACH_I(																	\
		AUXSYS_REGISTER_FOR_EACH_TPARAM_TYPE,													\
		NUM_PARAMS,																				\
		BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)												\
	),																							\
	class Aux																					\
>     																							\
struct init_guid<																				\
	Template<																					\
		BOOST_PP_SEQ_FOR_EACH_I(																\
			AUXSYS_REGISTER_FOR_EACH_TPARAM_VAL,												\
			NUM_PARAMS,																			\
			BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)											\
		)																						\
	>, Aux																						\
> {          																					\
	static guid_initializer<																	\
		Template<																				\
			BOOST_PP_SEQ_FOR_EACH_I(															\
				AUXSYS_REGISTER_FOR_EACH_TPARAM_VAL,											\
				NUM_PARAMS,																		\
				BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)										\
			)																					\
		>																						\
	> const & g;     																			\
};																								\
template<																						\
	BOOST_PP_SEQ_FOR_EACH_I(																	\
		AUXSYS_REGISTER_FOR_EACH_TPARAM_TYPE,													\
		NUM_PARAMS,																				\
		BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)												\
	),																							\
	class Aux																					\
>																								\
guid_initializer<																				\
	Template<																					\
		BOOST_PP_SEQ_FOR_EACH_I(																\
			AUXSYS_REGISTER_FOR_EACH_TPARAM_VAL,												\
			NUM_PARAMS,																			\
			BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)											\
		)																						\
	>																							\
> 																								\
const & init_guid<																				\
	Template<																					\
		BOOST_PP_SEQ_FOR_EACH_I(																\
			AUXSYS_REGISTER_FOR_EACH_TPARAM_VAL,												\
			NUM_PARAMS,																			\
			BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)											\
		)																						\
	>, Aux>::g =        																		\
	::boost::serialization::singleton<                   										\
		guid_initializer<																		\
			Template<																			\
				BOOST_PP_SEQ_FOR_EACH_I(														\
					AUXSYS_REGISTER_FOR_EACH_TPARAM_VAL,										\
					NUM_PARAMS,																	\
					BOOST_PP_TUPLE_TO_SEQ(NUM_PARAMS, TPARAMS)									\
				)																				\
			> 																					\
		>                   																	\
	>::get_mutable_instance().export_guid();             										\
}}																								\
SYS_WNONVIRT_DTOR_OFF

/**
**/

#define AUXSYS_EXPORT_CLASS_0(T)					\
SYS_WNONVIRT_DTOR_OFF									\
namespace boost {                              			\
namespace serialization {                      			\
template<>                                     			\
struct guid_defined<T> : boost::mpl::true_ {}; 			\
}}														\
SYS_WNONVIRT_DTOR_ON

#endif

/**
* SYS_EXPORT_INSTANCE is complementary to (and requires)
* SYS_EXPORT_TEMPLATE macro. It is to be placed into the destructor.
**/

#define SYS_EXPORT_INSTANCE()											\
::systematic::detail::init_guid_type_registrator(this);

/**
* SYS_EXPORT_TEMPLATE exports the template class for serialization. It is required
* for polymorphic classes that will be serialized through base class pointer.
*
* For description of the arguments see documentation for macro
* SYS_REGISTER_TEMPLATENAME in header Systematic/type_id/type_info.h.
*
* SYS_EXPORT_TEMPLATE also registers the class using SYS_REGISTER_TEMPLATENAME
* so this needs not to be called separately.
**/
#define SYS_EXPORT_TEMPLATE(Template, NUM_PARAMS) 						\
SYS_REGISTER_TEMPLATENAME(Template, NUM_PARAMS) 						\
AUXSYS_EXPORT_TEMPLATE_0(Template, NUM_PARAMS)

/**
* SYS_EXPORT_TEMPLATE_DETAILED exports the template class for serialization.
* It is required for polymorphic classes that will be serialized through base
* class pointer.
*
* For description of the arguments see documentation for macro
* SYS_REGISTER_TEMPLATENAME_DETAILED in header Systematic/type_id/type_info.h.
*
* SYS_EXPORT_TEMPLATE_DETAILED also registers the class using
* SYS_REGISTER_TEMPLATENAME_DETAILED so this needs not to be called separately.
**/
#define SYS_EXPORT_TEMPLATE_DETAILED(Template, NUM_PARAMS, TPARAMS) 	\
SYS_REGISTER_TEMPLATENAME_DETAILED(Template, NUM_PARAMS, TPARAMS) 		\
AUXSYS_EXPORT_TEMPLATE_DETAILED_0(Template, NUM_PARAMS, TPARAMS)

/**
* Exports the class for serialization using boost. This is required
* for polymorphic classes that will be serialized through base class pointer.
*
* SYS_EXPORT_CLASS also registers the class using SYS_REGISTER_TYPENAME
* so this needs not to be called separately.
*
* You also need to call SYS_EXPORT_CLASS_IMPLEMENT at some point to register
* the class with the archives.
*
* \sa SYS_EXPORT_CLASS
**/
#define SYS_EXPORT_CLASS_KEY(T) 										\
SYS_REGISTER_TYPENAME(T)												\
AUXSYS_EXPORT_CLASS_0(T)

/**
 * Registers the class with the archives. You need to call
 * SYS_EXPORT_CLASS_KEY(T) first.
 */
#define SYS_EXPORT_CLASS_IMPLEMENT(T)                        \
    namespace systematic {                                   \
    namespace detail {                                       \
    template<class Aux>  		                             \
    struct init_guid< T, Aux > {                             \
        static guid_initializer< T > const & g;              \
    };                                                       \
    template<class Aux>									 	 \
    guid_initializer< T > const & init_guid< T, Aux >::g =   \
        ::boost::serialization::singleton<                   \
            guid_initializer< T >                            \
        >::get_mutable_instance().export_guid();             \
    template class init_guid<T, void>;					 	 \
    }}                                                       \
/**/

/**
* Exports the class for serialization using BOOST_CLASS_EXPORT. This is required
* for polymorphic classes that will be serialized through base class pointer.
*
* This macro combines SYS_EXPORT_CLASS_KEY(T) and SYS_EXPORT_CLASS_IMPLEMENT(T).
**/
#define SYS_EXPORT_CLASS(T) 		\
SYS_EXPORT_CLASS_KEY(T)				\
SYS_EXPORT_CLASS_IMPLEMENT(T)

#endif //SYS_EXPORT_H_INCLUDED
