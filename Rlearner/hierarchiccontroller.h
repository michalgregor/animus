// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_chierarchiccontroller_H_
#define Rlearner_chierarchiccontroller_H_

#include <list>
#include <vector>

#include "agentcontroller.h" 
#include "agentlistener.h"

namespace rlearner {

class StateCollection;
class StateProperties;
class State;
class Action;
class ActionSet;
class ActionDataSet;
class ActionList;
class ExtendedAction;
class HierarchicalStack;

/**
 * @class HierarchicalStackListener
 * Listener who gets the Hierarchical Stack instead of an action.
 *
 * These listeners can only be added to the hierachical controller, they get
 * the full information about the current hierarchical stack, which can also
 * be used for learning.
 */
class HierarchicalStackListener {
public:
	virtual void nextStep(StateCollection *oldState,
	        HierarchicalStack *actionStack, StateCollection *newState) = 0;
	virtual void newEpisode() {}

public:
	virtual ~HierarchicalStackListener() {}
};

//! Base class for sending the hierarchical stack to the listeners.
class HierarchicalStackSender {
protected:
	std::list<HierarchicalStackListener *> *stackListeners;

public:
	void addHierarchicalStackListener(HierarchicalStackListener *listener);
	void removeHierarchicalStackListener(HierarchicalStackListener *listener);

	virtual void startNewEpisode();
	virtual void sendNextStep(StateCollection *oldState,
	        HierarchicalStack *actionStack, StateCollection *newState);

public:
	HierarchicalStackSender& operator=(const HierarchicalStackSender&) = delete;
	HierarchicalStackSender(const HierarchicalStackSender&) = delete;

	HierarchicalStackSender();
	virtual ~HierarchicalStackSender();
};

/**
 * @class HierarchicalStackEpisode
 * Class for logging the Hierarchical Stack of a training trial.
 *
 * Stores each step the Hierarchical stack in an action List. Must be added
 * as listener of a HierarchicalController object.
 **/
class HierarchicalStackEpisode: public HierarchicalStackListener {
protected:
	std::vector<ActionList *> *actionStacks;
	ActionSet *behaviors;

public:
	virtual void nextStep(HierarchicalStack *actionStack);
	virtual void newEpisode();

	void load(std::istream& stream);
	void save(std::ostream& stream);

	void getHierarchicalStack(unsigned int index,
	        HierarchicalStack *actionStack, bool clearStack = true);
	virtual int getNumSteps();

public:
	HierarchicalStackEpisode& operator=(const HierarchicalStackEpisode&) = delete;
	HierarchicalStackEpisode(const HierarchicalStackEpisode&) = delete;

	HierarchicalStackEpisode(ActionSet *behaviors);
	virtual ~HierarchicalStackEpisode();
};

/**
 * @class HierarchicalController
 * Class for calculating a hierarchical execution of a hierarchical structure.
 *
 * The hierarchical controller calculates a hierarchical policy, given the root
 * element from an hierarchical learning structure. The hierarchical learning
 * structure is build by the Hierarchical Semi Markov Decision Processes. The
 * hierarchical stores an hierarchical action stack and executes each
 * hierarchical action as long as it (or an action with higher hierarchy in
 * the stack) is finished. The hierarchical controller also manages the
 * calculation of the duration and the finished flag of all extended actions.
 * The controller also has a listener list of stack listeners (from superclass
 * HierarchicalStackSender). Each component which needs access to the
 * hierarchical stack needs to be a hierarchical stack listener
 * (CHierarchicStackListener) and must be added to the listener list of the
 * controller. The controller, if added to the listener list of the again then
 * sends each step the states (old and new state) and the hierarchical stack
 * to his listeners. So all hierarchical semi-MDP's must be added as to the
 * controller's listeners, even some MDP's which are not directly the root
 * of the hierarchy.
 *
 * You can also set the duration of the hierarchical execution, so if
 * an extended action has a already took longer than the maximum duration
 * the extended action is finished by the controller. With this feature you
 * can make a transition from hierarchical to flat execution during learning.
 *
 * The hierarchical controller always returns a primitive action (the last
 * action on the stack) from his getNextAction method for the agent to execute.
 * All primitive actions returned from the hierarchical structure must be
 * member of the controllers action set!
 *
 * @see HierarchicalStackListener
 * @see HierarchicalSemiMarkovDecisionProcess.
 */
class HierarchicalController: public AgentController,
        public HierarchicalStackSender,
        public SemiMDPListener
{
protected:
	//! The agent's actions.
	ActionSet *agentActions;
	//! The actual hierarchical stack.
	HierarchicalStack *actionStack;
	//! The root of the hierarchical structure.
	ExtendedAction *rootAction;

	ActionDataSet *hierarchichActionDataSet;

	//! Returns the action for the agent from the stack (the last action
	//! on stack).
	virtual Action* getAgentAction(HierarchicalStack *stack,
	        ActionDataSet *actionDataSet);

public:
	//! Get maximum duration of hierarchical execution.
	int getMaxHierarchicalExecution();
	//! Set maximum duration of hierarchical execution.
	void setMaxHierarchicalExecution(int maxExec);

	/**
	 * Returns the primitive action from the action stack.
	 *
	 * Builds and renews missing parts of the action stack.
	 */
	virtual Action *getNextAction(StateCollection *state,
	        ActionDataSet *actionDataSet);

	/**
	 * Sends the states and action stacks to the listeners.
	 *
	 * Calculates the duration of the actions (adds the duration of
	 * the executed primitive action to each extended action), and the finished
	 * flags. If one action is finished, all other actions with lower hierarchy
	 * in the action stack get marked as finished too.
	 *
	 * After that calculation it sends the action stack to all listeners and
	 * then it deletes the finished actions from the stack. The missing stack
	 * elements are renewed by the method getAction.
	 */
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *newState);

	//! Only calls nextStep.
	virtual void intermediateStep(StateCollection *oldState, Action *action,
	        StateCollection *newState);

	virtual void newEpisode();

public:
	HierarchicalController& operator=(const HierarchicalController&) = delete;
	HierarchicalController(const HierarchicalController&) = delete;

	/**
	 * Creates the hierarchical controller with the actionset he can choose
	 * from and the root of the hierarchical structure.
	 *
	 * All primitive actions returned from the hierarchical structure must be
	 * member of the action set!
	 */
	HierarchicalController(ActionSet *agentActions, ActionSet *allActions,
	        ExtendedAction *rootAction);

	virtual ~HierarchicalController();
};

} //namespace rlearner

#endif //Rlearner_chierarchiccontroller_H_
