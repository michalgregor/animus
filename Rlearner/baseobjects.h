// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cbaseobjects_H_
#define Rlearner_cbaseobjects_H_

#include "system.h"
#include <list>
#include <set>

namespace rlearner {

class StateModifier;
class StateProperties;
class StateCollection;
class State;
class Action;
class ActionSet;
class ActionData;
class ActionDataSet;
class StateModifierList;

/**
 * @class ActionObject
 * Base Class for all Classes which have to maintain an action set.
 *
 * It just has a pointer to the actionset an some functions to get information
 * about the actionset.
 **/
class ActionObject {
protected:
	ActionSet *actions;
	bool ownActionSet;
public:
	ActionSet *getActions();
	unsigned int getNumActions();

public:
	ActionObject& operator=(const ActionObject&) = delete;
	ActionObject(const ActionObject&) = delete;

	ActionObject(ActionSet *actions, bool createNew = false);
	virtual ~ActionObject();
};

/**
 * @class StateObject
 * Base class for all classes which have to manage with states.
 *
 * The class just saves a state properties object pointer which can be used
 * to retrieve a state from a state collection.
 **/
class StateObject {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	//! The properties of the wanted state.
	StateProperties *properties;

public:
	StateProperties *getStateProperties();

	//! Compares the two properties of the objects.
	bool equalsModelProperties(StateObject *object);
	//! Just returns the number of continuous states from the properties object.
	unsigned int getNumContinuousStates();
	//! Just returns the number of discrete states from the properties object.
	unsigned int getNumDiscreteStates();

public:
	StateObject& operator=(const StateObject&) = delete;
	StateObject(const StateObject&) = delete;

	StateObject(StateProperties *properties);
	virtual ~StateObject() = default;
};

/**
 * Base class for all classes that have to maintain lists of modifiers.
 *
 * StateModifiers may be owned by agents, simulators and other objects that
 * act as self-contained entities. This is why we allow multiple state modifier
 * lists instead of a single global list. Derived classes that need fast
 * lookup by modifier pointer are encouraged to use a cache over the individual
 * lists.
 */
class StateModifiersObject: public StateObject {
protected:
	std::set<StateModifierList*> _modifierLists;
	
public:
	//! Adds a list of StateModifier objects.
	virtual void addModifierList(StateModifierList* modifierList);
	//! Removes a list of StateModifier objects.
	virtual void removeModifierList(StateModifierList* modifierList);

	std::set<StateModifierList*>& getModifierLists() {return _modifierLists;}
	const std::set<StateModifierList*>& getModifierLists() const {return _modifierLists;}

public:
	StateModifiersObject& operator=(const StateModifiersObject&) = delete;
	StateModifiersObject(const StateModifiersObject&) = delete;

	StateModifiersObject(StateProperties *modelState);

	StateModifiersObject(
	    StateProperties *modelState,
	    const std::set<StateModifierList*>& modifierLists
	);

	virtual ~StateModifiersObject();
};

} //namespace rlearner

#endif //Rlearner_cbaseobjects_H_
