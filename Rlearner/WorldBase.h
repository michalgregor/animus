#ifndef RLEARNER_WORLDBASE_H_
#define RLEARNER_WORLDBASE_H_

#include <set>

#include "agent.h"
#include "environmentmodel.h"
#include "statecollection.h"
#include "stateproperties.h"

namespace rlearner {

/**
 * @class WorldBase
 *
 * A base class for World implementations. Worlds are basically containers for
 * agents, which communicate with external environments, manage states and
 * ask for agent's decisions.
 *
 * They are similar to simulator environments, but lack some of the features,
 * such as the ability to start a new episode or reset the model at any point.
 **/
class WorldBase {
protected:
	//! The list of agents.
	std::set<shared_ptr<Agent> > _agents;

	unsigned int _currentEpisodeNumber;
	unsigned int _currentSteps;
	unsigned int _totalSteps;

	unique_ptr<StateCollectionImpl> _lastState;
	unique_ptr<StateCollectionImpl> _currentState;
	std::vector<Action*> _lastActions;

	StateModifierList _modifierList;

protected:
	//! Tells all Listeners that a new Episode has occurred and resets the model.
	void startNewEpisode();

protected:
	//! Registers the agent with the world base, exchanging state modifier
	//! lists with it, etc.
	//!
	//! \note This method does NOT add the agent into the agent list.
	void registerAgent(const shared_ptr<Agent>& agent);

	//! Unregisters the agent from the world base, removing the appropriate
	//! state modifier, etc.
	void unregisterAgent(const shared_ptr<Agent>& agent);

	//! Carries out a single step:
	//! * Sets up the current state.
	//! * Sends the _lastState, action, _currentState tuple to every agent.
	//! * Makes every agent select a new action.
	//! * Increments the step and total step counter.
	//! Returns the selected actions.
	const std::vector<Action*>& makeStep();

	/**
	 * Writes the current world state into the provided state.
	 *
	 * This method is usually called by getState(StateCollectionImpl *).
	 * The user has to override this function for his specific model and write
	 * the internal state of the model in the state object. The state object
	 * always has the properties of the model state.
	 */
	virtual void writeState(State* state) = 0;

	/**
	 * Writes the current world state into the provided state.
	 *
	 * The default implementation call writeState(State*) with the model state
	 * of the collection and signals that a new model state has been written
	 * using newModelState.
	 */
	virtual void writeState(StateCollectionImpl* stateCollection);

public:
	//! Adds the agent into the world and registers it. If the agent has been
	//! added already, this has no effect.
	void addAgent(const shared_ptr<Agent>& agent);
	//! Unregisters the agent and removes it from the world. If there is no
	//! such agent in the world, this has no effect.
	void removeAgent(const shared_ptr<Agent>& agent);

	//! Adds the StateModifier to this object's StateModifierList (the list
	//! itself is already added to the underlying StateCollections).
	void addStateModifier(StateModifier* modifier);
	//! Removes the StateModifier from this object's StateModifierList.
	void removeStateModifier(StateModifier* modifier);

	//! Adds the StateModifierList to the StateCollections.
	void addModifierList(StateModifierList* modifierList);
	//! Removes the StateModifierList from the StateCollections.
	void removeModifierList(StateModifierList* modifierList);

	unsigned int getTotalSteps() const {return _totalSteps;}
	unsigned int getCurrentStep() const {return _currentSteps;}
	unsigned int getCurrentEpisodeNumber() const {return _currentEpisodeNumber;}

	StateCollection *getCurrentState();

public:
	WorldBase(StateProperties* properties, const std::set<shared_ptr<Agent> >& agentList);
	WorldBase(StateProperties* properties, std::set<shared_ptr<Agent> >&& agentList = {});
	~WorldBase();
};

} // namespace rlearner

#endif /* RLEARNER_WORLDBASE_H_ */
