// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "ril_debug.h"
#include "reinforce.h"

#include "rewardfunction.h"
#include "policies.h"
#include "gradientfunction.h"
#include "featurefunction.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "statemodifier.h"
#include "vetraces.h"
#include "vfunction.h"
#include "qfunction.h"
#include "action.h"

namespace rlearner {

ConstantReinforcementBaseLineCalculator::ConstantReinforcementBaseLineCalculator(
    RealType b) {
	addParameter("ReinforcementBaseLine", b);
}

RealType ConstantReinforcementBaseLineCalculator::getReinforcementBaseLine(int) {
	return getParameter("ReinforcementBaseLine");
}

AverageReinforcementBaseLineCalculator::AverageReinforcementBaseLineCalculator(
    RewardFunction *rewardFunction, RealType updateRate = 0.1) :
	SemiMDPRewardListener(rewardFunction) {
	averageReward = 0;
	addParameter("AverageRewardMinUpdateRate", updateRate);
	steps = 0;
}

RealType AverageReinforcementBaseLineCalculator::getReinforcementBaseLine(int) {
	return averageReward;
}

void AverageReinforcementBaseLineCalculator::nextStep(
    StateCollection *, Action *, RealType reward, StateCollection *) {
	steps++;
	RealType gamma = getParameter("AverageRewardMinUpdateRate");
	RealType alpha = 1 / steps;

	if(alpha < gamma) {
		alpha = gamma;
	}

	averageReward = (1 - alpha) * averageReward + alpha * reward;
}

void AverageReinforcementBaseLineCalculator::newEpisode() {
	steps = 0;
	averageReward = 0;
}

REINFORCELearner::REINFORCELearner(
    RewardFunction *reward, StochasticPolicy *policy,
    GradientUpdateFunction *updateFunction,
    ReinforcementBaseLineCalculator *baseLine) :
	SemiMDPRewardListener(reward) {
	this->policy = policy;
	this->baseLine = baseLine;
	this->updateFunction = updateFunction;

	addParameters(policy);
	addParameters(baseLine);

	addParameter("REINFORCELearningRate", 0.2);

	gradient = new FeatureList();

	eTraces = new GradientVETraces(nullptr);

	addParameters(eTraces);
}

REINFORCELearner::~REINFORCELearner() {
	delete gradient;
	delete eTraces;
}

void REINFORCELearner::nextStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *) {
	gradient->clear();

	policy->getActionProbabilityLnGradient(oldState, action,
	    action->getActionData(), gradient);

	eTraces->updateETraces(action->getDuration());
	eTraces->addGradientETrace(gradient, 1.0);

	gradient->clear();
	FeatureList *eTraceList = eTraces->getGradientETraces();

	FeatureList::iterator it = eTraceList->begin();
	for(; it != eTraceList->end(); it++) {
		gradient->update((*it)->featureIndex,
		    (*it)->factor
		        * (reward
		            - baseLine->getReinforcementBaseLine((*it)->featureIndex)));
	}

	updateFunction->updateGradient(gradient,
	    getParameter("REINFORCELearningRate"));
}

void REINFORCELearner::newEpisode() {
	eTraces->resetETraces();
}

GradientVETraces *REINFORCELearner::getETraces() {
	return eTraces;
}

} //namespace rlearner
