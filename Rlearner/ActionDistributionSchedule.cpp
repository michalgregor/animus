#include "ActionDistributionSchedule.h"
#include <Systematic/utils/ToString.h>

namespace rlearner {

void ActionDistributionSchedule::swap(ActionDistributionSchedule& obj) {
	_distributions.swap(obj._distributions);
	unsigned int tmp;

	tmp = _counter;
	_counter = obj._counter;
	obj._counter = tmp;

	tmp = _currentDistribution;
	_currentDistribution = obj._currentDistribution;
	obj._currentDistribution = tmp;
}

void ActionDistributionSchedule::getDistribution(
    StateCollection* state, ActionSet* availableActions, RealType* values) {
	SYS_ASSERT_MSG((_currentDistribution < _distributions.size()),
	    "Distribution index overflow (index: " + ToString(_currentDistribution)
	        + "; size: " + ToString(_distributions.size()) + ").");

	_distributions[_currentDistribution].first->getDistribution(state,
	    availableActions, values);

	_counter++;
	if(_counter >= _distributions[_currentDistribution].second) {
		_counter = 0;
		_currentDistribution = (_currentDistribution + 1)
		    % numeric_cast<unsigned int>(_distributions.size());
	}
}

ActionDistributionSchedule::ActionDistributionSchedule(
    const distro_vector& distributions) :
	_distributions(distributions) {
}

ActionDistributionSchedule::ActionDistributionSchedule(
    distro_vector&& distributions) :
	_distributions(std::move(distributions)) {
}

} //rlearner
