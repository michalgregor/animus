// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>
#include <iostream>
#include <algorithm>

#include "algebra.h"
#include "rbftrees.h"

namespace rlearner {

RBFBasisFunction::RBFBasisFunction(
    ColumnVector *l_center, ColumnVector *l_sigma
) {
	center = new ColumnVector(*l_center);
	sigma = new ColumnVector(*l_sigma);
}

RealType RBFBasisFunction::getActivationFactor(ColumnVector *x) {
	RealType buffer = 0.0;

	for(int i = 0; i < center->rows(); i++) {
		buffer += pow((x->operator[](i) - center->operator[](i)), 2.0)
		    / sigma->operator[](i);
	}
	buffer = -buffer / 2;

	return exp(buffer);
}

ColumnVector *RBFBasisFunction::getCenter() {
	return center;
}

ColumnVector *RBFBasisFunction::getSigma() {
	return sigma;
}

void RBFBasisFunction::setSigma(ColumnVector *l_sigma) {
	sigma = l_sigma;
}

void RBFBasisFunction::setCenter(ColumnVector *l_center) {
	center = l_center;
}

RBFBasisFunctionLinearWeight::RBFBasisFunctionLinearWeight(
    ColumnVector *center, ColumnVector *sigma, RealType l_weight) :
	RBFBasisFunction(center, sigma) {
	weight = l_weight;
}

RealType RBFBasisFunctionLinearWeight::getOutputWeight() {
	return weight;
}

void RBFBasisFunctionLinearWeight::setWeight(RealType l_weight) {
	weight = l_weight;
}

RBFDataFactory::RBFDataFactory(
    DataSet *l_inputData, ColumnVector *l_varMultiplier,
    ColumnVector *l_minVar) {
	minVar = new ColumnVector(*l_minVar);
	varMultiplier = new ColumnVector(*l_varMultiplier);

	inputData = l_inputData;
}

RBFDataFactory::RBFDataFactory(DataSet *l_inputData) {
	inputData = l_inputData;

	minVar = new ColumnVector(inputData->getNumDimensions());
	varMultiplier = new ColumnVector(inputData->getNumDimensions());

	minVar->setConstant(0.001);
	varMultiplier->setConstant(1.5);
}

RBFDataFactory::~RBFDataFactory() {
	delete minVar;
	delete varMultiplier;
}

RBFBasisFunction *RBFDataFactory::createTreeData(DataSubset *dataSubset, int) {
	ColumnVector center(inputData->getNumDimensions());
	ColumnVector sigma(inputData->getNumDimensions());

	inputData->getMean(dataSubset, &center);
	inputData->getVariance(dataSubset, &sigma);

	sigma = sigma.array() * varMultiplier->array();

	for(int i = 0; i < sigma.rows(); i++) {
		if(sigma.operator[](i) < minVar->operator[](i)) {
			sigma.operator[](i) = minVar->operator[](i);
		}
	}

	return new RBFBasisFunction(&center, &sigma);
}

void RBFDataFactory::deleteData(RBFBasisFunction *basisFunction) {
	delete basisFunction;
}

RBFLinearWeightDataFactory::RBFLinearWeightDataFactory(
    DataSet *l_inputData, DataSet1D *l_outputData,
    ColumnVector *l_varMultiplier, ColumnVector *l_minVar) {
	minVar = new ColumnVector(*l_minVar);
	varMultiplier = new ColumnVector(*l_varMultiplier);

	inputData = l_inputData;
	outputData = l_outputData;
}

RBFLinearWeightDataFactory::~RBFLinearWeightDataFactory() {
	delete minVar;
	delete varMultiplier;
}

RBFBasisFunctionLinearWeight *RBFLinearWeightDataFactory::createTreeData(
    DataSubset *dataSubset, int) {
	ColumnVector center(inputData->getNumDimensions());
	ColumnVector sigma(inputData->getNumDimensions());

	inputData->getMean(dataSubset, &center);
	inputData->getVariance(dataSubset, &sigma);

	sigma = sigma.array() * varMultiplier->array();

	RealType value = 0; //outputData->getMean(dataSubset);

	for(int i = 0; i < sigma.rows(); i++) {
		if(sigma.operator[](i) < minVar->operator[](i)) {
			sigma.operator[](i) = minVar->operator[](i);
		}
	}

	RBFBasisFunctionLinearWeight *basisFunction =
	    new RBFBasisFunctionLinearWeight(&center, &sigma, value);

	DataSubset::iterator it = dataSubset->begin();

	value = 0;
	RealType norm = 0.0;

	//printf("New Var: %f %f\n", sigma.element(0), sigma.element(1));

	for(; it != dataSubset->end(); it++) {
		RealType factor = basisFunction->getActivationFactor((*inputData)[*it]);
		norm += factor;
		value += factor * (*outputData)[*it];

		//printf("Sample: %f %f (%f %f)\n", factor, (*outputData)[*it], sample->operator[](0), sample->operator[](1));
	}
	if(norm > 0) {
		value /= norm;
	}
	//printf("value: %f\n", value);
	basisFunction->setWeight(value);

	return basisFunction;
}

void RBFLinearWeightDataFactory::deleteData(
    RBFBasisFunctionLinearWeight *basisFunction) {
	delete basisFunction;
}

RBFExtraRegressionTree::RBFExtraRegressionTree(
    DataSet *inputData, DataSet1D *outputData, unsigned int K,
    unsigned int n_min, RealType treshold, ColumnVector *varMultiplier,
    ColumnVector *minVar) :
	    ExtraTree<RBFBasisFunctionLinearWeight *>(inputData, outputData,
	        new RBFLinearWeightDataFactory(inputData, outputData, varMultiplier,
	            minVar), K, n_min, treshold) {

}

RBFExtraRegressionTree::~RBFExtraRegressionTree() {
	delete root;
	root = nullptr;
	delete dataFactory;
}

void KNearestRBFCenters::addDataElements(
    ColumnVector *point, Leaf<RBFBasisFunctionLinearWeight *> *leaf,
    KDRectangle *) {
	RBFBasisFunctionLinearWeight *basis = leaf->getTreeData();

	*buffVector = *basis->getCenter();
	*buffVector = *buffVector - *point;

	addAndSortDataElements(leaf->getLeafNumber(), buffVector->norm());
}

KNearestRBFCenters::KNearestRBFCenters(
    Tree<RBFBasisFunctionLinearWeight *> *tree, int K) :
	KNearestNeighborsTreeData<int, RBFBasisFunctionLinearWeight *>(tree, K) {
	buffVector = new ColumnVector(tree->getNumDimensions());
}

KNearestRBFCenters::~KNearestRBFCenters() {
	delete buffVector;
}

RBFRegressionTreeOutputMapping::RBFRegressionTreeOutputMapping(
    Tree<RBFBasisFunctionLinearWeight *> *l_tree, int K) :
	Mapping<RealType>(l_tree->getNumDimensions()) {
	tree = l_tree;

	nearestLeaves = new KNearestRBFCenters(tree, K);

}

RBFRegressionTreeOutputMapping::~RBFRegressionTreeOutputMapping() {
	delete nearestLeaves;
}

RealType RBFRegressionTreeOutputMapping::doGetOutputValue(ColumnVector *input) {
	std::list<int> neighbors;

	nearestLeaves->getNearestNeighbors(input, &neighbors);

	RealType factor = 0.0;
	RealType value = 0.0;

	std::list<int>::iterator it = neighbors.begin();
	for(; it != neighbors.end(); it++) {
		RBFBasisFunctionLinearWeight *basis = tree->getLeaf(*it)->getTreeData();

		//basis->setSigma(&mean_distance);

		RealType l_factor = basis->getActivationFactor(input);

		factor += l_factor;
		value += l_factor * basis->getOutputWeight();

		//printf("(%f %f %f %f) ", l_factor, basis->getOutputWeight(), factor, value);
	}

	//cout << "Input: " << input->t() << endl;
	it = neighbors.begin();
	for(; it != neighbors.end(); it++) {
		RBFBasisFunctionLinearWeight *basis = tree->getLeaf(*it)->getTreeData();

//		ColumnVector dist(*basis->getCenter());
//		cout << "Center: " << dist.t() << " ";
//		dist = dist - *input;
		RealType l_factor = basis->getActivationFactor(input);

		factor += l_factor;
		value += l_factor * basis->getOutputWeight();

		//	printf("(%f %f %f %f)\n", l_factor, basis->getOutputWeight(), dist.norm_Frobenius(), basis->getSigma()->norm_Frobenius());
	}
	//printf("\n");

	if(factor > 0) {
		value = value / factor;
	} else {
		printf("Warning: RBF Tree network: Summed Factor == %f (%d)!!!\n",
		    factor, neighbors.size());

		it = neighbors.begin();
		std::cout << "Input : " << input->transpose() << std::endl;
		for(; it != neighbors.end(); it++) {
			RBFBasisFunctionLinearWeight *basis =
			    tree->getLeaf(*it)->getTreeData();

			ColumnVector dist(*basis->getCenter());
			std::cout << "Center (Samples "
			    << tree->getLeaf(*it)->getNumSamples() << "): "
			    << dist.transpose() << " ";
			dist = dist - *input;

			RealType l_factor = basis->getActivationFactor(input);

			factor += l_factor;
			value += l_factor * basis->getOutputWeight();

			printf("(%f %f %f %f)\n", l_factor, basis->getOutputWeight(),
			    dist.norm(), basis->getSigma()->norm());
		}

		std::vector<RealType> distances;
		DataSet *inputData = tree->getInputData();
		for(unsigned int j = 0; j < inputData->size(); j++) {
			ColumnVector distance(*(*inputData)[j]);
			distance = distance - *input;
			RealType dist = distance.norm();

			distances.push_back(dist);
		}
		std::sort(distances.begin(), distances.end());

		printf("Distances of NNs (Real) : ");
		for(unsigned int j = 0; j < 5 && j < distances.size(); j++) {
			printf("%f ", distances[j]);
		}
		printf("\n");
		//assert(false);
	}

	return value;
}

RBFExtraRegressionForest::RBFExtraRegressionForest(
    int numTrees, int kNN, DataSet *inputData, DataSet1D *outputData,
    unsigned int K, unsigned int n_min, RealType treshold,
    ColumnVector *varMultiplier, ColumnVector *minVar) :
	    Forest<RBFBasisFunctionLinearWeight *>(numTrees),
	    Mapping<RealType>(inputData->getNumDimensions()) {
	dataFactory = new RBFLinearWeightDataFactory(inputData, outputData,
	    varMultiplier, minVar);
	for(int i = 0; i < numTrees; i++) {
		addTree(i,
		    new ExtraTree<RBFBasisFunctionLinearWeight *>(inputData, outputData,
		        dataFactory, K, n_min, treshold));
	}
	mapping = new RBFRegressionTreeOutputMapping *[numTrees];
	initRBFMapping(kNN);
}

RBFExtraRegressionForest::~RBFExtraRegressionForest() {
	for(int i = 0; i < numTrees; i++) {
		delete forest[i];
		delete mapping[i];
	}
	delete dataFactory;
}

void RBFExtraRegressionForest::initRBFMapping(int kNN) {
	for(int i = 0; i < numTrees; i++) {
		mapping[i] = new RBFRegressionTreeOutputMapping(forest[i], kNN);
	}
}

RealType RBFExtraRegressionForest::doGetOutputValue(ColumnVector *input) {
	RealType mean = 0;

	for(int i = 0; i < numTrees; i++) {
		mean += mapping[i]->getOutputValue(input);
	}
	return mean / numTrees;
}
;

RBFLinearWeightForest::RBFLinearWeightForest(int numTrees, int numDim) :
	Forest<RBFBasisFunctionLinearWeight *>(numTrees), Mapping<RealType>(numDim) {

}

RBFLinearWeightForest::~RBFLinearWeightForest() {}

void RBFLinearWeightForest::save(std::ostream& stream) {
	fmt::fprintf(stream, "%f %f\n", getAverageDepth(), getAverageNumLeaves());
}

RealType RBFLinearWeightForest::getOutputValue(ColumnVector *input) {
	RealType sum = 0;
	RealType norm = 0;
	for(int i = 0; i < numTrees; i++) {
		Tree<RBFBasisFunctionLinearWeight *> *tree = getTree(i);
		RBFBasisFunctionLinearWeight *rbfBasis = tree->getOutputValue(input);
		RealType factor = rbfBasis->getActivationFactor(input);
		sum += factor * rbfBasis->getOutputWeight();
		norm += factor;
	}

	if(fabs(norm) > 0) {
		sum = sum / norm;
	}

	return sum;
}

ExtraTreeRBFLinearWeightForest::ExtraTreeRBFLinearWeightForest(
    int numTrees, DataSet *inputData, DataSet1D *outputData, unsigned int K,
    unsigned int n_min, RealType treshold, ColumnVector *varMultiplier,
    ColumnVector *minVar) :
	RBFLinearWeightForest(numTrees, inputData->getNumDimensions()) {
	dataFactory = new RBFLinearWeightDataFactory(inputData, outputData,
	    varMultiplier, minVar);
	for(int i = 0; i < numTrees; i++) {
		addTree(i,
		    new ExtraTree<RBFBasisFunctionLinearWeight *>(inputData, outputData,
		        dataFactory, K, n_min, treshold));

	}
}

ExtraTreeRBFLinearWeightForest::~ExtraTreeRBFLinearWeightForest() {
	for(int i = 0; i < numTrees; i++) {
		delete forest[i];
	}

	delete dataFactory;
}

} //namespace rlearner
