// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>
#include <sstream>
#include <cstring>

#include "ril_debug.h"
#include "vfunction.h"
#include "vfunctionfromqfunction.h"
#include "rewardfunction.h"
#include "vetraces.h"
#include "statemodifier.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "action.h"
#include "discretizer.h"
#include "gradientfunction.h"
#include "qfunction.h"

namespace rlearner {

AbstractVFunction::AbstractVFunction(StateProperties *prop) :
		StateObject(prop) {
	type = 0;
	mayDiverge = false;
}

AbstractVFunction::~AbstractVFunction() {
}

void AbstractVFunction::updateValue(StateCollection *state, RealType td) {
	updateValue(state->getState(properties), td);
}

void AbstractVFunction::setValue(StateCollection *state, RealType qValue) {
	setValue(state->getState(properties), qValue);
}

RealType AbstractVFunction::getValue(StateCollection *state) {
	return getValue(state->getState(properties));
}

void AbstractVFunction::updateValue(State *state, RealType td) {
	setValue(state, getValue(state) + td);
}

int AbstractVFunction::getType() {
	return type;
}

bool AbstractVFunction::isType(int isT) {
	int temp = type & isT;
	return (temp == isT);
}

void AbstractVFunction::addType(int newType) {
	type = type | newType;
}

void AbstractVFunction::saveData(std::ostream& file) {
	fmt::fprintf(file, "V-Function:\n");
	fmt::fprintf(file, "\n");
}

void AbstractVFunction::loadData(std::istream& file) {
	assert(!file.fail());

	xscanf(file, "V-Function:\n");
	xscanf(file, "\n");

	// these reading operations are allowed to fail
	file.clear();
}

AbstractVETraces *AbstractVFunction::getStandardETraces() {
	return new StateVETraces(this, properties);
}

ZeroVFunction::ZeroVFunction() :
		AbstractVFunction(nullptr) {

}

RealType ZeroVFunction::getValue(State *) {
	return 0;
}

VFunctionSum::VFunctionSum() :
		AbstractVFunction(nullptr) {
	vFunctions = new std::map<AbstractVFunction *, RealType>;
}

VFunctionSum::~VFunctionSum() {
	delete vFunctions;
}

RealType VFunctionSum::getValue(StateCollection *state) {
	std::map<AbstractVFunction *, RealType>::iterator it = vFunctions->begin();

	RealType sum = 0.0;
	for (; it != vFunctions->end(); it++) {
		AbstractVFunction *vFunc = (*it).first;

		sum += (*it).second * vFunc->getValue(state);
	}
	return sum;
}

RealType VFunctionSum::getVFunctionFactor(AbstractVFunction *vFunction) {
	return (*vFunctions)[vFunction];
}

void VFunctionSum::setVFunctionFactor(AbstractVFunction *vFunction,
        RealType factor) {
	(*vFunctions)[vFunction] = factor;
}

void VFunctionSum::addVFunction(AbstractVFunction *vFunction, RealType factor) {
	(*vFunctions)[vFunction] = factor;
}

void VFunctionSum::removeVFunction(AbstractVFunction *vFunction) {
	std::map<AbstractVFunction *, RealType>::iterator it = vFunctions->find(
	        vFunction);
	if (it != vFunctions->end()) vFunctions->erase(it);
}

void VFunctionSum::normFactors(RealType factor) {
	std::map<AbstractVFunction *, RealType>::iterator it;

	RealType sum = 0.0;
	for (it = vFunctions->begin(); it != vFunctions->end(); it++) {
		sum += (*it).second;
	}

	for (it = vFunctions->begin(); it != vFunctions->end(); it++) {
		(*it).second *= factor / sum;
	}
}

DivergentVFunctionException::DivergentVFunctionException(
	std::string vFunctionName, AbstractVFunction *vFunction, State *state,
	RealType value
):
	TracedError(vFunctionName + " diverges (value = " + std::to_string(value) + ", |value| > 1000000).")
{
	this->vFunction = vFunction;
	this->vFunctionName = vFunctionName;
	this->state = state;
	this->value = value;
}

GradientVFunction::GradientVFunction(StateProperties *properties) :
		AbstractVFunction(properties) {
	this->addType(GRADIENTVFUNCTION);
}

GradientVFunction::~GradientVFunction() {}

void GradientVFunction::updateValue(StateCollection *state, RealType td) {
	localGradientFeatureBuffer->clear();
	getGradient(state, localGradientFeatureBuffer);

	updateGradient(localGradientFeatureBuffer, td);
}

void GradientVFunction::updateValue(State *state, RealType td) {
	localGradientFeatureBuffer->clear();
	getGradient(state, localGradientFeatureBuffer);

	updateGradient(localGradientFeatureBuffer, td);
}

AbstractVETraces *GradientVFunction::getStandardETraces() {
	return new GradientVETraces(this);
}

VFunctionInputDerivationCalculator::VFunctionInputDerivationCalculator(
        StateProperties *modelState) {
	this->modelState = modelState;
}

unsigned int VFunctionInputDerivationCalculator::getNumInputs() {
	return modelState->getNumContinuousStates();
}

void VFunctionInputDerivationCalculator::addModifierList(StateModifierList*) {}
void VFunctionInputDerivationCalculator::removeModifierList(StateModifierList*) {}

VFunctionNumericInputDerivationCalculator::VFunctionNumericInputDerivationCalculator(
        StateProperties *modelState, AbstractVFunction *vFunction,
        RealType stepSize, const std::set<StateModifierList*>& modifierLists) :
		VFunctionInputDerivationCalculator(modelState) {
	this->vFunction = vFunction;
	this->stateBuffer = new StateCollectionImpl(modelState, modifierLists);

	addParameter("NumericInputDerivationStepSize", stepSize);
}

VFunctionNumericInputDerivationCalculator::~VFunctionNumericInputDerivationCalculator() {
	delete stateBuffer;
}

void VFunctionNumericInputDerivationCalculator::addModifierList(StateModifierList* modifierList) {
	stateBuffer->addModifierList(modifierList);
}

void VFunctionNumericInputDerivationCalculator::removeModifierList(StateModifierList* modifierList) {
	stateBuffer->removeModifierList(modifierList);
}

void VFunctionNumericInputDerivationCalculator::getInputDerivation(
        StateCollection *state, ColumnVector *targetVector) {
	State *inputState = stateBuffer->getState(modelState);
	inputState->setState(state->getState(modelState));

	RealType stepSize = getParameter("NumericInputDerivationStepSize");

	for (unsigned int i = 0; i < modelState->getNumContinuousStates(); i++) {
		RealType stepSize_i = (modelState->getMaxValue(i)
		        - modelState->getMinValue(i)) * stepSize;
		inputState->setContinuousState(i,
		        inputState->getContinuousState(i) + stepSize_i);
		stateBuffer->newModelState();
		RealType vPlus = vFunction->getValue(stateBuffer);
		inputState->setContinuousState(i,
		        inputState->getContinuousState(i) - 2 * stepSize_i);
		stateBuffer->newModelState();
		RealType vMinus = vFunction->getValue(stateBuffer);

		inputState->setContinuousState(i,
		        inputState->getContinuousState(i) + stepSize_i);
		targetVector->operator[](i) = (vPlus - vMinus) / (2 * stepSize_i);
	}
}

FeatureVFunction::FeatureVFunction(int numFeatures) :
		GradientVFunction(nullptr), FeatureFunction(numFeatures) {

}

FeatureVFunction::FeatureVFunction(StateProperties *prop) :
		GradientVFunction(prop), FeatureFunction(
		        prop->getDiscreteStateSize(0)) {
}

FeatureVFunction::FeatureVFunction(FeatureQFunction *qfunction,
        StochasticPolicy *policy) :
		GradientVFunction(qfunction->getFeatureCalculator()), FeatureFunction(
		        qfunction->getFeatureCalculator()->getDiscreteStateSize()) {
	setVFunctionFromQFunction(qfunction, policy);
}

FeatureVFunction::~FeatureVFunction() {
}

void FeatureVFunction::setVFunctionFromQFunction(FeatureQFunction *qfunction,
        StochasticPolicy *policy) {
	StateProperties properties(0, 1, DISCRETESTATE);
	properties.setDiscreteStateSize(0, numFeatures);
	State discState(&properties);

	AbstractVFunction *tempFunction;

	if (policy) {
		tempFunction = new VFunctionFromQFunction(qfunction, policy,
		        qfunction->getFeatureCalculator());
	} else {
		tempFunction = new OptimalVFunctionFromQFunction(qfunction,
		        qfunction->getFeatureCalculator());
	}

	for (unsigned int i = 0; i < numFeatures; i++) {
		discState.setDiscreteState(0, i);
		setFeature(i, tempFunction->getValue(&discState));
	}
	delete tempFunction;
}

void FeatureVFunction::getGradient(StateCollection *stateCol,
        FeatureList *gradient) {
	State *state = stateCol->getState(properties);

	int type = state->getStateProperties()->getType()
	        & (DISCRETESTATE | FEATURESTATE);
	switch (type) {
	case FEATURESTATE: {
		for (unsigned int i = 0; i < state->getNumActiveDiscreteStates(); i++) {
			gradient->update(state->getDiscreteState(i),
			        state->getContinuousState(i));
		}
		break;
	}
	case DISCRETESTATE: {
		gradient->set(state->getDiscreteState(0), 1.0);
		break;
	}
	default: {
		gradient->set(state->getDiscreteStateNumber(), 1.0);
		break;
	}
	}
	if (DebugIsEnabled('v')) {
		DebugPrint('v', "Calculating feature Gradient List : ");
		gradient->save(DebugGetFileHandle('v'));
		DebugPrint('v', "\n");
	}
}

void FeatureVFunction::updateValue(State *state, RealType td) {
	if (DebugIsEnabled('v')) {
		DebugPrint('v', "Update V-Value: %f", td);
		state->save(DebugGetFileHandle('v'));
		DebugPrint('v', "\n");
	}

	int type = state->getStateProperties()->getType()
	        & (DISCRETESTATE | FEATURESTATE);
	switch (type) {
	case FEATURESTATE: {
		for (unsigned int i = 0; i < state->getNumActiveDiscreteStates(); i++) {
			updateFeature(state->getDiscreteState(i),
			        td * state->getContinuousState(i));
		}
		break;
	}
	case DISCRETESTATE: {
		updateFeature(state->getDiscreteState(0), td);
		break;
	}
	default: {
		updateFeature(state->getDiscreteStateNumber(), td);
	}
	}
}

void FeatureVFunction::setValue(State *state, RealType qValue) {
	if (DebugIsEnabled('v')) {
		DebugPrint('v', "Set V-Value: %f", qValue);
		state->save(DebugGetFileHandle('v'));
		DebugPrint('v', "\n");
	}

	int type = state->getStateProperties()->getType()
	        & (DISCRETESTATE | FEATURESTATE);
	switch (type) {
	case FEATURESTATE: {
		for (unsigned int i = 0; i < state->getNumActiveDiscreteStates(); i++) {
			setFeature(state->getDiscreteState(i),
			        qValue * state->getContinuousState(i));
		}
		break;
	}
	case DISCRETESTATE: {
		setFeature(state->getDiscreteState(0), qValue);
		break;
	}
	default: {
		setFeature(state->getDiscreteStateNumber(), qValue);
	}
	}
}

RealType FeatureVFunction::getValue(State *state) {
	RealType value = 0;

	int type = state->getStateProperties()->getType()
	        & (DISCRETESTATE | FEATURESTATE);
	switch (type) {
	case FEATURESTATE: {
		for (unsigned int i = 0; i < state->getNumActiveDiscreteStates(); i++) {
			value += getFeature(state->getDiscreteState(i))
			        * state->getContinuousState(i);
		}
		break;
	}
	case DISCRETESTATE: {
		value = getFeature(state->getDiscreteState(0));
		break;
	}
	default: {
		value = getFeature(state->getDiscreteStateNumber());
	}
	}

	if (DebugIsEnabled('v')) {
		DebugPrint('v', "Get V-Value: %f", value);
		state->save(DebugGetFileHandle('v'));
		DebugPrint('v', "\n");
	}
	if (!mayDiverge
	        && (value < - DIVERGENTVFUNCTIONVALUE
	                || value > DIVERGENTVFUNCTIONVALUE)) {
		throw DivergentVFunctionException("Feature Function", this, state,
		        value);
	}
	return value;
}

void FeatureVFunction::saveData(std::ostream& file) {
	GradientVFunction::saveData(file);
}

void FeatureVFunction::loadData(std::istream& file) {
	AbstractVFunction::loadData(file);
	GradientVFunction::loadData(file);
}

void FeatureVFunction::printValues() {
	AbstractVFunction::printValues();
	printFeatures();
}

AbstractVETraces *FeatureVFunction::getStandardETraces() {
	return new FeatureVETraces(this);
}

void FeatureVFunction::updateWeights(FeatureList *gradientFeatures) {
	this->updateFeatureList(gradientFeatures, 1.0);
}

int FeatureVFunction::getNumWeights() {
	return this->numFeatures;
}

void FeatureVFunction::resetData() {
	FeatureFunction::init(0.0);
}

void FeatureVFunction::getWeights(RealType *parameters) {
	memcpy(parameters, this->features, sizeof(RealType) * getNumFeatures());
}

void FeatureVFunction::setWeights(RealType *parameters) {
	memcpy(this->features, parameters, sizeof(RealType) * getNumFeatures());
}

void FeatureVFunction::setFeatureCalculator(FeatureCalculator *featCalc) {
	assert(featCalc->getNumFeatures() < numFeatures);

	properties = featCalc;
}

VTable::VTable(AbstractStateDiscretizer *discretizer) :
		FeatureVFunction(discretizer) {
}

VTable::~VTable() {
}

void VTable::setDiscretizer(AbstractStateDiscretizer *discretizer) {
	assert(
	        discretizer == nullptr
	                || discretizer->getDiscreteStateSize() == numFeatures);

	this->properties = discretizer;
}

AbstractStateDiscretizer *VTable::getDiscretizer() {
	return (AbstractStateDiscretizer*) properties;
}

int VTable::getNumStates() {
	return getNumFeatures();
}

RewardAsVFunction::RewardAsVFunction(StateReward *reward) :
		AbstractVFunction(reward->getStateProperties()) {
	this->reward = reward;
}

RealType RewardAsVFunction::getValue(State *state) {
	return reward->getStateReward(state);
}

} //namespace rlearner
