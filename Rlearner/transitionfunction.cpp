// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>
#include <fstream>

#include "transitionfunction.h"
#include "action.h"
#include "statecollection.h"
#include "stateproperties.h"
#include "state.h"
#include "vfunction.h"
#include "continuousactions.h"
#include "utility.h"
#include "ril_debug.h"
#include "regions.h"
#include "theoreticalmodel.h"

#include "system.h"

namespace rlearner {

TransitionFunction::TransitionFunction(
    StateProperties *properties, ActionSet *actions
):
	StateObject(properties), ActionObject(actions)
{
	type = 0;
	resetType = DM_RESET_TYPE_ZERO;
}

int TransitionFunction::getType() {
	return type;
}

void TransitionFunction::getDerivationU(State *, Matrix *) {}

void TransitionFunction::addType(int Type) {
	type = type | Type;
}

bool TransitionFunction::isType(int type) {
	return (this->type & type) == type;
}

void TransitionFunction::getResetState(State *modelState) {
	switch(resetType) {
	case DM_RESET_TYPE_ZERO:
		for(unsigned int i = 0; i < modelState->getNumContinuousStates(); i++) {
			modelState->setContinuousState(i, 0.0);
		}

		for(unsigned int i = 0; i < modelState->getNumDiscreteStates(); i++) {
			modelState->setDiscreteState(i, 0);
		}
	break;
	case DM_RESET_TYPE_ALL_RANDOM:
	case DM_RESET_TYPE_RANDOM: {
		auto rand = make_rand();

		for(unsigned int i = 0; i < modelState->getNumContinuousStates(); i++) {
			RealType stateSize = properties->getMaxValue(i)
			    - properties->getMinValue(i);
			RealType randNum = (((RealType) rand()) / RAND_MAX);
			modelState->setContinuousState(i,
			    randNum * stateSize + properties->getMinValue(i));
		}

		for(unsigned int i = 0; i < modelState->getNumDiscreteStates(); i++) {
			modelState->setDiscreteState(i,
			    rand() % properties->getDiscreteStateSize(i));
		}
	}
	break;
	default:
	break;
	}
}

void TransitionFunction::setResetType(int resetType) {
	this->resetType = resetType;
}

ExtendedActionTransitionFunction::ExtendedActionTransitionFunction(
    ActionSet *actions, TransitionFunction *model,
    const std::set<StateModifierList*>& modifierLists, RewardFunction *l_rewardFunction
):
	TransitionFunction(model->getStateProperties(), actions),
	dynModel(model),
	intermediateState(new StateCollectionImpl(properties, modifierLists)),
	nextState(new StateCollectionImpl(properties, modifierLists)),
	actionDataSet(nullptr),
	rewardFunction(l_rewardFunction),
	lastReward(0)
{
	addType(DM_AF_ExtendedActionMODEL);
	addParameters(dynModel);
	addParameter("MaxHierarchicExecution", 50);
	addParameter("DiscountFactor", 0.95);
}

ExtendedActionTransitionFunction::~ExtendedActionTransitionFunction() {
	delete intermediateState;
	delete nextState;
}

RealType ExtendedActionTransitionFunction::getReward(
    StateCollection *, Action *, StateCollection *
) {
	return lastReward;
}

void ExtendedActionTransitionFunction::transitionFunction(
    State *oldstate, Action *action, State *newState, ActionData *data
) {
	lastReward = transitionFunctionAndReward(oldstate, action, newState, data,
	    rewardFunction, getParameter("DiscountFactor"));
}

RealType ExtendedActionTransitionFunction::transitionFunctionAndReward(
    State *oldState, Action *action, State *newState, ActionData *data,
    RewardFunction *rewardFunction, RealType gamma
) {
	RealType reward = 0;
	StateCollectionImpl *buf = nullptr;

	intermediateState->getState(properties)->setState(oldState);

	if(action->isType(AF_PrimitiveAction)) {
		dynModel->transitionFunction(oldState, action, newState, data);

		nextState->getState(properties)->setState(newState);

		if(rewardFunction) {
			reward = rewardFunction->getReward(intermediateState, action,
			    nextState);
		}
	}

	if(action->isType(AF_ExtendedAction)) {
		int duration = 0;

		Action *primAction = nullptr;
		ExtendedAction *extAction = nullptr;

		extAction = dynamic_cast<ExtendedAction *>(action);

		do {
			primAction = action;

			ExtendedAction *extAction2;
			int lduration = 1;

			while(primAction->isType(AF_ExtendedAction)) {
				extAction2 = dynamic_cast<ExtendedAction *>(primAction);
				primAction = extAction2->getNextHierarchyLevel(
				    intermediateState);
			}

			ActionData *primActionData = actionDataSet->getActionData(
			    primAction);

			if(primActionData) {
				primActionData->setData(primAction->getActionData());
			}

			dynModel->transitionFunction(
			    intermediateState->getState(dynModel->getStateProperties()),
			    action, nextState->getState(dynModel->getStateProperties()),
			    primActionData);
			nextState->newModelState();

			if(primActionData && primAction->isType(AF_MultistepAction)) {
				lduration = dynamic_cast<MultiStepActionData *>(data)->duration;
			} else {
				lduration = primAction->getDuration();
			}

			duration += lduration;

			if(rewardFunction) {
				reward = reward * pow(gamma, lduration)
				    + rewardFunction->getReward(intermediateState, primAction,
				        nextState);
			}

			//exchange Model State

			buf = intermediateState;
			intermediateState = nextState;
			nextState = buf;
		}

		// Execute the action until the state changed
		while(duration < getParameter("MaxHierarchicExecution")
		    && !extAction->isFinished(intermediateState, nextState));

		if(data) {
			dynamic_cast<MultiStepActionData *>(data)->duration = duration;
		}

		newState->setState(nextState->getState(properties));
	}

	return reward;
}

void ExtendedActionTransitionFunction::getDerivationU(
    State *oldstate, Matrix *derivation) {
	dynModel->getDerivationU(oldstate, derivation);
}

bool ExtendedActionTransitionFunction::isResetState(State *state) {
	return dynModel->isResetState(state);
}

bool ExtendedActionTransitionFunction::isFailedState(State *state) {
	return dynModel->isFailedState(state);
}

void ExtendedActionTransitionFunction::getResetState(State *resetState) {
	dynModel->getResetState(resetState);
}

void ExtendedActionTransitionFunction::setResetType(int resetType) {
	dynModel->setResetType(resetType);
}

ComposedTransitionFunction::ComposedTransitionFunction(
    StateProperties *properties) :
	    TransitionFunction(properties, new ActionSet()),
	    _transitionFunction(new std::list<TransitionFunction *>()) {
}

ComposedTransitionFunction::~ComposedTransitionFunction() {
	delete _transitionFunction;
	delete actions;
}

void ComposedTransitionFunction::addTransitionFunction(
    TransitionFunction *model) {
	_transitionFunction->push_back(model);
	actions->add(model->getActions());
}

void ComposedTransitionFunction::transitionFunction(
    State *oldstate, Action *action, State *newState, ActionData *data) {
	std::list<TransitionFunction *>::iterator it;

	for(it = _transitionFunction->begin(); it != _transitionFunction->end();
	    it++) {
		if((*it)->getActions()->isMember(action)) {
			(*it)->transitionFunction(oldstate, action, newState, data);
		}
	}
}

ContinuousTimeTransitionFunction::ContinuousTimeTransitionFunction(
    StateProperties *properties, ActionSet *actions, RealType dt) :
	TransitionFunction(properties, actions) {
	this->dt = dt;
	this->simulationSteps = 1;

	addType(DM_CONTINUOUSMODEL);

	derivation = new ColumnVector(
	    getStateProperties()->getNumContinuousStates());
}

ContinuousTimeTransitionFunction::~ContinuousTimeTransitionFunction() {
	delete derivation;
}

void ContinuousTimeTransitionFunction::doSimulationStep(
    State *state, RealType timeStep, Action *action, ActionData *data
) {
	for(unsigned int i = 0; i < state->getNumContinuousStates(); i++) {
		this->getDerivationX(state, action, derivation, data);

		state->setContinuousState(i,
		    state->getContinuousState(i)
		        + timeStep * derivation->operator[](i));
	}
}

void ContinuousTimeTransitionFunction::transitionFunction(
    State *oldState, Action *action, State *newState, ActionData *data) {
	RealType timestep = this->dt / simulationSteps;

	newState->setState(oldState);

	for(int i = 0; i < simulationSteps; i++) {
		this->doSimulationStep(newState, timestep, action, data);
	}
}

RealType ContinuousTimeTransitionFunction::getTimeIntervall() {
	return dt;
}

void ContinuousTimeTransitionFunction::setTimeIntervall(RealType dt) {
	this->dt = dt;
}

void ContinuousTimeTransitionFunction::setSimulationSteps(int steps) {
	assert(steps > 0);
	this->simulationSteps = steps;
}

int ContinuousTimeTransitionFunction::getSimulationSteps() {
	return simulationSteps;
}

ContinuousTimeAndActionTransitionFunction::ContinuousTimeAndActionTransitionFunction(
    StateProperties *properties, ContinuousAction *action, RealType dt) :
	ContinuousTimeTransitionFunction(properties, new ActionSet(), dt) {
	actions->add(action);
	this->actionProp = action->getContinuousActionProperties();
	this->contAction = action;
}

ContinuousTimeAndActionTransitionFunction::~ContinuousTimeAndActionTransitionFunction() {
	delete actions;
}

void ContinuousTimeAndActionTransitionFunction::getDerivationX(
    State *oldState, Action *action, ColumnVector *derivationX,
    ActionData *data) {
	assert(action->isType(AF_ContinuousAction));

	if(data) {
		getCADerivationX(oldState, dynamic_cast<ContinuousActionData *>(data),
		    derivationX);
	} else {
		getCADerivationX(oldState,
		    dynamic_cast<ContinuousActionData *>(action->getActionData()),
		    derivationX);
	}
}

ContinuousAction *ContinuousTimeAndActionTransitionFunction::getContinuousAction() {
	return contAction;
}

LinearActionContinuousTimeTransitionFunction::LinearActionContinuousTimeTransitionFunction(
    StateProperties *properties, ContinuousAction *action, RealType dt) :
	ContinuousTimeAndActionTransitionFunction(properties, action, dt) {
	A = new ColumnVector(properties->getNumContinuousStates());
	B = new Matrix(properties->getNumContinuousStates(),
	    actionProp->getNumActionValues());

	addType(DM_DERIVATIONUMODEL);
}

LinearActionContinuousTimeTransitionFunction::~LinearActionContinuousTimeTransitionFunction() {
	delete A;
	delete B;
}

void LinearActionContinuousTimeTransitionFunction::getCADerivationX(
    State *oldState, ContinuousActionData *contAction,
    ColumnVector *derivationX
) {
	assert(oldState->rows() == derivationX->rows());
	Matrix *B = getB(oldState);
	ColumnVector *a = getA(oldState);

	*derivationX = (*B) * (*contAction);
	*derivationX = (*derivationX) + (*a); // x' = B(x) * u + a(x)
}

void LinearActionContinuousTimeTransitionFunction::getDerivationU(
    State *oldstate, Matrix *derivation) {
	*derivation = *getB(oldstate);
}

DynamicLinearContinuousTimeModel::DynamicLinearContinuousTimeModel(
    StateProperties *properties, ContinuousAction *action, RealType dt, Matrix *A,
    Matrix *l_B
):
	LinearActionContinuousTimeTransitionFunction(properties, action, dt),
	B(nullptr),
	AMatrix(nullptr)
{
	assert(
	    (unsigned int ) A->rows() == properties->getNumContinuousStates()
	        && (unsigned int ) A->cols() == properties->getNumContinuousStates()
	        && (unsigned int ) B->cols()
	            == action->getContinuousActionProperties()->getNumActionValues()
	        && (unsigned int ) B->rows()
	            == properties->getNumContinuousStates());

	B = l_B;
	AMatrix = new Matrix(properties->getNumContinuousStates(),
	    properties->getNumContinuousStates());
	(*AMatrix) = *A;
}

DynamicLinearContinuousTimeModel::~DynamicLinearContinuousTimeModel() {
	delete AMatrix;
}

Matrix *DynamicLinearContinuousTimeModel::getB(State *) {
	return B;
}

ColumnVector *DynamicLinearContinuousTimeModel::getA(State *state) {
	*A = (*AMatrix) * (*state); // a(x) = A * x
	return A;
}

TransitionFunctionEnvironment::TransitionFunctionEnvironment(
    TransitionFunction *model) :
	EnvironmentModel(model->getStateProperties()), _transitionFunction(model) {
	modelState = new State(getStateProperties());
	nextState = new State(getStateProperties());

	startStates = nullptr;
	nEpisode = 0;
	createdStartStates = false;

	failedRegion = nullptr;
	sampleRegion = nullptr;
	targetRegion = nullptr;

	resetModel();
}

TransitionFunctionEnvironment::~TransitionFunctionEnvironment() {
	delete modelState;
	delete nextState;

	if(createdStartStates) {
		delete startStates;
	}
}

void TransitionFunctionEnvironment::doNextState(PrimitiveAction *action) {
	_transitionFunction->transitionFunction(modelState, action, nextState);
	State *buf = modelState;
	modelState = nextState;
	nextState = buf;

	if(targetRegion == nullptr) {
		reset = _transitionFunction->isResetState(modelState);
	} else {
		reset = targetRegion->isStateInRegion(modelState);
	}

	if(failedRegion == nullptr) {
		failed = _transitionFunction->isFailedState(modelState);
	} else {
		failed = failedRegion->isStateInRegion(modelState);
	}
}

void TransitionFunctionEnvironment::doResetModel() {
	if(startStates != nullptr) {
		startStates->getState(nEpisode, modelState);
		nEpisode++;
		nEpisode = nEpisode % startStates->getNumStates();
	} else {
		if(sampleRegion == nullptr) {
			_transitionFunction->getResetState(modelState);
		} else {
			sampleRegion->getRandomStateSample(modelState);
		}
	}
}

void TransitionFunctionEnvironment::getState(State *state) {
	assert(state->getStateProperties()->equals(getStateProperties()));
	state->setState(modelState);
}

void TransitionFunctionEnvironment::setState(State *state) {
	assert(state->getStateProperties()->equals(getStateProperties()));
	modelState->setState(state);
}

void TransitionFunctionEnvironment::setStartStates(StateList *startStates) {
	if(createdStartStates) {
		delete this->startStates;
		createdStartStates = false;
	}
	this->startStates = startStates;
	nEpisode = 0;
}

void TransitionFunctionEnvironment::setStartStates(const std::string& filename) {
	std::ifstream startStateFile(filename);
	startStates = new StateList(getStateProperties());
	startStates->load(startStateFile);
	nEpisode = 0;
}

void TransitionFunctionEnvironment::setSampleRegion(Region *l_sampleRegion) {
	this->sampleRegion = l_sampleRegion;
}

void TransitionFunctionEnvironment::setFailedRegion(Region *l_failedRegion) {
	this->failedRegion = l_failedRegion;
}

void TransitionFunctionEnvironment::setTargetRegion(Region *l_targetRegion) {
	this->targetRegion = l_targetRegion;
}

TransitionFunctionFromStochasticModel::TransitionFunctionFromStochasticModel(
    StateProperties *properties, AbstractFeatureStochasticModel *model) :
	TransitionFunction(properties, model->getActions()) {
	this->stochasticModel = model;

	startStates = new std::list<int>();
	startProbabilities = new std::list<RealType>();
	endStates = new std::map<int, RealType>();
}

TransitionFunctionFromStochasticModel::~TransitionFunctionFromStochasticModel() {
	delete startStates;
	delete startProbabilities;
	delete endStates;
}

void TransitionFunctionFromStochasticModel::addEndState(
    int state, RealType probability) {
	(*endStates)[state] = probability;
}

void TransitionFunctionFromStochasticModel::addStartState(
    int state, RealType probability) {
	startStates->push_back(state);
	startProbabilities->push_back(probability);
}

bool TransitionFunctionFromStochasticModel::isResetState(State *state) {
	int stateIndex = state->getDiscreteStateNumber();
	auto rand = make_rand();

	std::map<int, RealType>::iterator it = endStates->find(stateIndex);

	if(it != endStates->end()) {
		RealType prob = (*it).second;
		RealType z = ((RealType) rand()) / RAND_MAX;

		return z < prob;
	}

	return false;
}

void TransitionFunctionFromStochasticModel::getResetState(State *state) {
	if(startStates->size() > 0) {
		RealType *prob = new RealType[startStates->size()];
		std::list<RealType>::iterator it = startProbabilities->begin();
		for(int i = 0; it != startProbabilities->end(); it++, i++) {
			prob[i] = *it;
		}

		int stateIndex = Distributions::getSampledIndex(prob,
		    startStates->size());

		std::list<int>::iterator it2 = startStates->begin();
		for(int i = 0; i < stateIndex; it2++, i++) {}

		stateIndex = *it2;

		int stateDim = properties->getDiscreteStateSize();

		for(unsigned int i = 0; i < state->getNumDiscreteStates(); i++) {
			stateDim = properties->getDiscreteStateSize(i);
			state->setDiscreteState(i, stateIndex % stateDim);
			stateIndex = stateIndex / stateDim;
		}

		delete prob;
	} else {
		TransitionFunction::getResetState(state);
	}
}

void TransitionFunctionFromStochasticModel::transitionFunction(
    State *oldstate, Action *action, State *newState, ActionData *) {
	unsigned int statenum = oldstate->getDiscreteStateNumber();

	int actionIndex = actions->getIndex(action);
	TransitionList *list = stochasticModel->getForwardTransitions(actionIndex,
	    statenum);

	assert(list->size() > 0);

	RealType *prob = new RealType[list->size()];
	TransitionList::iterator it = list->begin();
	for(int i = 0; it != list->end(); it++, i++) {
		prob[i] = (*it)->getPropability();
	}

	int index = Distributions::getSampledIndex(prob, list->size());
	delete prob;

	it = list->begin();
	for(int i = 0; i < index; i++) {
		it++;
	}

	assert(it != list->end());

	int stateIndex = (*it)->getEndState();
	int stateDim = properties->getDiscreteStateSize();

	for(unsigned int i = 0; i < oldstate->getNumDiscreteStates(); i++) {
		stateDim = properties->getDiscreteStateSize(i);
		newState->setDiscreteState(i, stateIndex % stateDim);
		stateIndex = stateIndex / stateDim;
	}
}

QFunctionFromTransitionFunction::QFunctionFromTransitionFunction(
    ActionSet *actions, AbstractVFunction *vfunction, TransitionFunction *model,
    RewardFunction *rewardfunction, const std::set<StateModifierList*>& modifierLists
):
	AbstractQFunction(actions),
	StateModifiersObject(model->getStateProperties())
{

	this->vfunction = vfunction;
	this->model = model;
	this->rewardfunction = rewardfunction;

	this->actionDataSet = new ActionDataSet(actions);

	nextState = new StateCollectionImpl(model->getStateProperties());
	intermediateState = new StateCollectionImpl(model->getStateProperties());

	this->stateCollectionList = new StateCollectionList(
	    model->getStateProperties());

	addParameter("SearchDepth", 1);
	addParameter("DiscountFactor", 0.95);
	addParameter("VFunctionScale", 1.0);

	for(auto& list: modifierLists) {
		addModifierList(list);
	}
}

QFunctionFromTransitionFunction::~QFunctionFromTransitionFunction() {
	delete actionDataSet;
	delete nextState;
	delete intermediateState;
	delete stateCollectionList;
}

void QFunctionFromTransitionFunction::addModifierList(StateModifierList* modifierList) {
	StateModifiersObject::addModifierList(modifierList);
	nextState->addModifierList(modifierList);
	intermediateState->addModifierList(modifierList);
	stateCollectionList->addModifierList(modifierList);
}

RealType QFunctionFromTransitionFunction::getValue(
    StateCollection *state, Action *action, ActionData *data) {
	stateCollectionList->clearStateLists();
	stateCollectionList->addStateCollection(state);

	return getValueDepthSearch(stateCollectionList, action, data,
	    my_round(getParameter("SearchDepth")));
}

RealType QFunctionFromTransitionFunction::getValueDepthSearch(
    StateCollectionList *stateList, Action *action, ActionData *data,
    int depth) {
	stateList->getStateCollection(stateList->getNumStateCollections() - 1,
	    intermediateState);
	if(depth == 0) {
		RealType vFunctionScale = getParameter("VFunctionScale");
		return vfunction->getValue(intermediateState) * vFunctionScale;
	}

	if(data) {
		actionDataSet->getActionData(action)->setData(data);
	}

	ActionData *ldata = actionDataSet->getActionData(action);

	int duration = 1;

	RealType rewardValue = 0;
	if(model->isType(DM_AF_ExtendedActionMODEL)) {
		ExtendedActionTransitionFunction *extModel =
		    dynamic_cast<ExtendedActionTransitionFunction *>(model);
		rewardValue = extModel->transitionFunctionAndReward(
		    intermediateState->getState(model->getStateProperties()), action,
		    nextState->getState(model->getStateProperties()), ldata,
		    rewardfunction, getParameter("DiscountFactor"));
		nextState->newModelState();
	} else {
		model->transitionFunction(
		    intermediateState->getState(model->getStateProperties()), action,
		    nextState->getState(model->getStateProperties()), ldata);
		nextState->newModelState();
		rewardValue = rewardfunction->getReward(intermediateState, action,
		    nextState);
	}

	if((action)->isType(AF_MultistepAction)) {
		ActionData *actionData = actionDataSet->getActionData(action);
		MultiStepActionData *multiStepActionData =
		    dynamic_cast<MultiStepActionData *>(actionData);
		duration = multiStepActionData->duration;
	} else {
		duration = action->getDuration();
	}

	if(DebugIsEnabled('q')) {
		DebugPrint('q', "Calculated NextState for Action: %d (",
		    actions->getIndex(action));

		if(ldata) {
			ldata->save(DebugGetFileHandle('q'));
		}

//		data->saveASCII(DebugGetFileHandle('q'));

		DebugPrint('q', ")\n");
		nextState->getState()->save(DebugGetFileHandle('q'));
		DebugPrint('q', "\n");
	}

	RealType value = 0.0;

	if(!model->isResetState(nextState->getState())) {

		if(depth > 1) {
			stateList->addStateCollection(nextState);

			ActionSet::iterator it = actions->begin();

			value = getValueDepthSearch(stateList, *it, nullptr, depth - 1);
			RealType max = value;

			it++;

			for(; it != actions->end(); it++) {
				value = getValueDepthSearch(stateList, *it, nullptr, depth - 1);
				if(max < value) {
					max = value;
				}
			}
			value = max;

			stateList->removeLastStateCollection();
		} else {
			RealType vFunctionScale = getParameter("VFunctionScale");
			value = vfunction->getValue(nextState) * vFunctionScale;
		}
	} else {
		DebugPrint('q', "Endstate... ");
	}

	DebugPrint('q', "Value: %f Reward %f\n", value, rewardValue);

	return rewardValue + pow(getParameter("DiscountFactor"), duration) * value;
}

ContinuousTimeQFunctionFromTransitionFunction::ContinuousTimeQFunctionFromTransitionFunction(
    ActionSet *actions, VFunctionInputDerivationCalculator *vfunction,
    ContinuousTimeTransitionFunction *model, RewardFunction *rewardfunction,
    const std::set<StateModifierList*>& modifierLists
):
	AbstractQFunction(actions),
	StateModifiersObject(model->getStateProperties())
{
	this->vfunction = vfunction;
	this->model = model;
	this->rewardfunction = rewardfunction;

	nextState = new StateCollectionImpl(model->getStateProperties());
	derivationXModel = new State(model->getStateProperties());
	derivationXVFunction = new State(model->getStateProperties());

	for(auto& list: modifierLists) {
		addModifierList(list);
	}
}

ContinuousTimeQFunctionFromTransitionFunction::ContinuousTimeQFunctionFromTransitionFunction(
    ActionSet *actions, VFunctionInputDerivationCalculator *vfunction,
    ContinuousTimeTransitionFunction *model, RewardFunction *rewardfunction
):
	AbstractQFunction(actions),
	StateModifiersObject(model->getStateProperties())
{
	this->vfunction = vfunction;
	this->model = model;
	this->rewardfunction = rewardfunction;

	nextState = new StateCollectionImpl(model->getStateProperties());
	derivationXModel = new State(model->getStateProperties());
	derivationXVFunction = new State(model->getStateProperties());
}

ContinuousTimeQFunctionFromTransitionFunction::~ContinuousTimeQFunctionFromTransitionFunction() {
	delete nextState;
	delete derivationXModel;
	delete derivationXVFunction;
}

RealType ContinuousTimeQFunctionFromTransitionFunction::getValueVDerivation(
    StateCollection *state, Action *action, ActionData *data,
    ColumnVector *derivationXVFunction
) {
	model->getDerivationX(state->getState(model->getStateProperties()), action,
	    derivationXModel, data);
	model->transitionFunction(state->getState(model->getStateProperties()),
	    action, nextState->getState(model->getStateProperties()), data);

//	RealType reward = rewardfunction->getReward(state, action, nextState);
	return derivationXVFunction->dot(*derivationXModel);
}

void ContinuousTimeQFunctionFromTransitionFunction::getActionValues(
    StateCollection *state, ActionSet *actions, RealType *actionValues,
    ActionDataSet *actionDataSet
) {
	vfunction->getInputDerivation(state, derivationXVFunction);
	ActionSet::iterator it = actions->begin();

	for(int i = 0; it != actions->end(); it++, i++) {
		actionValues[i] = getValueVDerivation(state, *it,
		    actionDataSet->getActionData(*it), derivationXVFunction);
	}

	if(DebugIsEnabled('v')) {
		DebugPrint('v', "CTQ Function: ");
		for(unsigned int i = 0; i < actions->size(); i++) {
			DebugPrint('v', "%f ", actionValues[i]);
		}
		DebugPrint('v', "\n");
	}
}

RealType ContinuousTimeQFunctionFromTransitionFunction::getValue(
    StateCollection *state, Action *action, ActionData *data) {
	vfunction->getInputDerivation(state, derivationXVFunction);

	return getValueVDerivation(state, action, data, derivationXVFunction);
}

;

void ContinuousTimeQFunctionFromTransitionFunction::addModifierList(
	StateModifierList* modifierList)
{
	StateModifiersObject::addModifierList(modifierList);
	nextState->addModifierList(modifierList);
}

} //namespace rlearner
