// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_chierarchicbehaviours_H_
#define Rlearner_chierarchicbehaviours_H_

#include "agent.h"
#include "rewardfunction.h"
#include "action.h"
#include "agentcontroller.h"

namespace rlearner {

class Region;
class StateProperties;
class StateCollection;
class State;

class SubGoalBehaviour:
	public HierarchicalSemiMarkovDecisionProcess,
	public StateReward
{
protected:
	std::map<Region *, std::pair<RealType, RealType> > *rewardFactors;

	std::list<Region *> *targetRegions;
	std::list<Region *> *failRegions;

	RealType standardReward;

	Region *availableRegion;
	StateProperties *modelProperties;

	std::string subgoalName;

public:
	virtual bool isFinished(StateCollection *oldState,
	        StateCollection *newState);
	virtual bool isAvailable(StateCollection *currentState);

	virtual bool isInGoalRegion(State *state);
	virtual bool isInFailRegion(State *state);

	virtual RealType getStateReward(State *modelState);
	virtual void getInputDerivation(State *modelState,
	        ColumnVector *targetState);

	virtual void addTargetRegion(Region *target, RealType rewardFactor = 1.0,
	        RealType rewardTau = 10);
	virtual void addFailRegion(Region *target, RealType rewardFactor = -1.0,
	        RealType rewardTau = 10);

	void setRewardFactor(Region *region, RealType rewardFactor);
	void setRewardTau(Region *region, RealType rewardTau);
	void setStandardReward(RealType l_standardReward) {
		this->standardReward = l_standardReward;
	}

	virtual Region *getAvailAbleRegion() {
		return availableRegion;
	}

	virtual void sendNextStep(Action *action);

	std::string getSubGoalName() {
		return subgoalName;
	}

public:
	SubGoalBehaviour(StateProperties *modelProperties, Region *avialableRegion,
	        char *subgoalName = "");

	virtual ~SubGoalBehaviour();
};

class SubGoalController: public AgentController {
public:
	SubGoalController(ActionSet *hierarchicActions);

	virtual Action *getNextAction(
		StateCollection *state, ActionDataSet *data = nullptr);
};

class SubGoalOutput: public SemiMDPListener {
protected:
	SubGoalBehaviour *lastAction;
	AgentController *policy;

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *newState);
	virtual void newEpisode();

public:
	SubGoalOutput(AgentController *policy);
};

/*
class SubGoalTrainer: public TransitionFunctionEnvironment {
protected:
	SubGoalBehaviour *subGoal;
	Region *sampleRegion;
public:
	virtual void doNextState(PrimitiveAction *action);
	virtual void doResetModel();

	virtual void setSubGoal(SubGoalBehaviour *subGoal);
	virtual void setSampleRegion(Region *l_sampleRegion);

public:
	SubGoalTrainer(TransitionFunction *transitionFunction, SubGoalBehaviour *subGoal);
};
*/

class ExtendedPrimitiveAction: public ExtendedAction {
protected:
	Action *primitiveAction;

public:
	int extendedActionDuration;

public:
	virtual bool isFinished(StateCollection *oldState,
	        StateCollection *newState);
	virtual Action* getNextHierarchyLevel(StateCollection *state,
	        ActionDataSet *actionDataSet = nullptr);

public:
	ExtendedPrimitiveAction(Action *primitiveAction, int extendedActionDuration);
};

/**
 * @class PrimitiveActionStateChange
 *
 * This class represents an primitive action which gets executed until
 * a specific (mostly discrete) state changes.
 *
 * This extended action subclass executes a primitive action until a specified
 * state changes. This can be useful for example in gridworlds with local
 * states. The state which has to change is given by "stateToChange" an will
 * be most time a discrete state (continuous states or features normally always
 * change).
 *
 * The isFinished method returns as long false as the 2 states (oldState and
 * newState) are the same.
 **/
class PrimitiveActionStateChange: public ExtendedAction {
protected:
	//! The Properties of the state which has to change.
	StateProperties *stateToChange;
	PrimitiveAction *primitiveAction;

public:
	//! Always returns primitiveAction.
	virtual Action* getNextHierarchyLevel(StateCollection *state,
	        ActionDataSet *actionDataSet = nullptr);

	//! Returns true if the 2 states are not equal.
	virtual bool isFinished(StateCollection *oldState,
	        StateCollection *newState);

	//! Sets the state which has to change.
	void setStateToChange(StateProperties *stateToChange);

public:
	PrimitiveActionStateChange(PrimitiveAction *action, StateProperties *stateToChange);
};

} //namespace rlearner

#endif //Rlearner_chierarchicbehaviours_H_
