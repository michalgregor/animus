#include "EvaluatorNEA.h"

namespace rlearner {

void NEAComputer::addReward(RealType reward) {
	if(!_hadRewards) {
		_numEpisodes++;
		_episodeScore = 0;
		_hadRewards = true;
	}

	_score += reward;
	_episodeScore += reward;
	if(_episodeScore > _maxScore) _maxScore = _episodeScore;
}

void NEAComputer::nextEpisode() {
	_hadRewards = false;
}

RealType NEAComputer::combineEvaluation(EvaluatorNEA nea) const {
	switch(nea) {
	case EvaluatorNEA::Sum:
		return _score;
	break;
	case EvaluatorNEA::Average:
		return _score/_numEpisodes;
	break;
	case EvaluatorNEA::Max:
		return _maxScore;
	break;
	case EvaluatorNEA::Last:
		return _episodeScore;
	break;
	default: {
		SYS_ALWAYS_ASSERT_MSG(0, std::string("Unknown EvaluatorNEA type ") + enum2str(nea) + ".");
	} break;
	}

	return 0;
}

} //namespace example
