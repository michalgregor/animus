#include "TaskEvaluator_MultiReward.h"

namespace rlearner {

RealType TaskEvaluator_MultiReward::getEvaluation() const {
	return _neaComputer.combineEvaluation(_nea);
}

void TaskEvaluator_MultiReward::resetEvaluation() {
	_neaComputer = NEAComputer();
}

void TaskEvaluator_MultiReward::nextStep(StateCollection* oldStateCol, Action* action, StateCollection* newStateCol) {
	RealType score = 0;

	for(auto& r: _rewardFunctions) {
		score += r.second * r.first->getReward(oldStateCol, action, newStateCol);
	}

	_neaComputer.addReward(score);
}

//! Tells the Listener that a new Episode has started.
void TaskEvaluator_MultiReward::newEpisode() {
	_neaComputer.nextEpisode();
}

TaskEvaluator_MultiReward::TaskEvaluator_MultiReward(EvaluatorNEA nea):
	_nea(nea) {}

/**
 * Constructs a TaskEvaluator_MultiReward, and automatically adds reward
 * functions of the specified task - all with the weight of 1.
 */
TaskEvaluator_MultiReward::TaskEvaluator_MultiReward(
        const shared_ptr<Task>& task, EvaluatorNEA nea) :
		_rewardFunctions(), _nea(nea)
{
	unsigned int sz = task->numRewardFunctions();

	for(unsigned int i = 0; i < sz; i++) {
		add(raw_ptr(task->getRewardFunction(i)), 1);
	}
}

} //namespace rlearner
