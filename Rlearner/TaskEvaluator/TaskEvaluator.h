#ifndef RlearnerStateChart_TaskEvaluator_H_
#define RlearnerStateChart_TaskEvaluator_H_

#include "../system.h"
#include "../agentlistener.h"

namespace rlearner {

class TaskEvaluator: public SemiMDPListener {
public:
	virtual RealType getEvaluation() const = 0;
	virtual void resetEvaluation() = 0;

public:
	virtual ~TaskEvaluator() = 0;
};

} //namespace rlearner

#endif //RlearnerStateChart_TaskEvaluator_H_
