#ifndef RlearnerStateChart_TaskEvaluator_MultiReward_H_
#define RlearnerStateChart_TaskEvaluator_MultiReward_H_

#include "../system.h"
#include "../Task.h"
#include "../rewardfunction.h"

#include "TaskEvaluator.h"
#include "EvaluatorNEA.h"

namespace rlearner {

class TaskEvaluator_MultiReward: public TaskEvaluator {
private:
	//! Specifies how episode evaluations should be combined.
	EvaluatorNEA _nea = EvaluatorNEA::Average;
	//! The nea computer.
	NEAComputer _neaComputer = {};

private:
	typedef std::vector<std::pair<shared_ptr<RewardFunction>, RealType> > VectorType;
	VectorType _rewardFunctions;

public:
	typedef VectorType::iterator iterator;
	typedef VectorType::reverse_iterator reverse_iterator;
	typedef VectorType::const_iterator const_iterator;
	typedef VectorType::const_reverse_iterator const_reverse_iterator;
	typedef VectorType::size_type size_type;

public:
	const_iterator begin() const {
		return _rewardFunctions.begin();
	}

	const_iterator end() const {
		return _rewardFunctions.end();
	}

	const_reverse_iterator rbegin() const {
		return _rewardFunctions.rbegin();
	}

	const_reverse_iterator rend() const {
		return _rewardFunctions.rend();
	}

	iterator begin() {
		return _rewardFunctions.begin();
	}

	iterator end() {
		return _rewardFunctions.end();
	}

	reverse_iterator rbegin() {
		return _rewardFunctions.rbegin();
	}

	reverse_iterator rend() {
		return _rewardFunctions.rend();
	}

public:
	/**
	 * Returns the coefficient of the reward function at position iter.
	 */
	RealType getCoefficient(const_iterator iter) const {
		return iter->second;
	}

	/**
	 * Sets the coefficient of the reward function at position iter.
	 */
	void setCoefficient(iterator iter, RealType coefficient) {
		iter->second = coefficient;
	}

	/**
	 * Adds the specified reward function with the specified coefficient and
	 * returns the iterator.
	 */
	iterator add(const shared_ptr<RewardFunction>& rewardFunction, RealType coefficient) {
		_rewardFunctions.push_back(std::make_pair(rewardFunction, coefficient));
		return end() - 1;
	}

	/**
	 * Finds the first occurrence of rewardFunction and returns iterator to that
	 * element. Returns end() if no occurrence is found.
	 *
	 * @param rewardFunction The reward function that is to be found.
	 */
	const_iterator find(const shared_ptr<RewardFunction>& rewardFunction) const {
		return std::find_if(begin(), end(), [&rewardFunction](const VectorType::value_type& val){return val.first == rewardFunction;});
	}

	/**
	 * Finds the first occurrence of rewardFunction and returns iterator to that
	 * element. Returns end() if no occurrence is found.
	 *
	 * @param rewardFunction The reward function that is to be found.
	 */
	iterator find(const shared_ptr<RewardFunction>& rewardFunction) {
		return std::find_if(begin(), end(), [&rewardFunction](const VectorType::value_type& val){return val.first == rewardFunction;});
	}

	/**
	 * Removes the corresponding element from the MultiMotivator.
	 */
	void remove(iterator iter) {
		_rewardFunctions.erase(iter);
	}

	/**
	 * Finds the first occurrence of rewardFunction and removes it. If no
	 * occurrence is found this has no effect.
	 *
	 * @return Returns true if the element has been found and removed.
	 */
	bool remove(const shared_ptr<RewardFunction>& rewardFunction) {
		auto iter = find(rewardFunction);
		if(iter != end()) {
			remove(iter);
			return true;
		} else return false;
	}

	/**
	 * Returns the number of reward functions.
	 */
	size_type size() const {
		return _rewardFunctions.size();
	}

	/**
	 * Removes all reward functions.
	 */
	void clear() {
		_rewardFunctions.clear();
	}

public:
	virtual RealType getEvaluation() const;
	virtual void resetEvaluation();

	/**
	 * Sets EvaluatorNEA.
	 */
	void setNEA(EvaluatorNEA nea) {
		_nea = nea;
	}

	/**
	 * Returns EvaluatorNEA.
	 */
	EvaluatorNEA getNEA() const {
		return _nea;
	}

public:
	virtual void nextStep(StateCollection* oldStateCol, Action* action, StateCollection* newStateCol);
	virtual void newEpisode();

public:
	TaskEvaluator_MultiReward& operator=(const TaskEvaluator_MultiReward&) = delete;
	TaskEvaluator_MultiReward(const TaskEvaluator_MultiReward&) = delete;

	TaskEvaluator_MultiReward(EvaluatorNEA nea = EvaluatorNEA::Average);
	TaskEvaluator_MultiReward(const shared_ptr<Task>& task, EvaluatorNEA nea = EvaluatorNEA::Average);
	virtual ~TaskEvaluator_MultiReward() = default;
};


} //namespace rlearner

#endif //RlearnerStateChart_TaskEvaluator_MultiReward_H_
