#ifndef RlearnerStateChart_EvaluatorNEA_H_
#define RlearnerStateChart_EvaluatorNEA_H_

#include "../system.h"
#include <limits>

namespace rlearner {

/**
 * @enum EvaluatorNEA
 * Specifies how a reward-based task evaluator should combine evaluations from
 * multiple episodes.
 *
 * Sum - scores are added up
 * Average - scores are averaged
 * Max - the maximum score is kept
 * Last - score from the last episode is kept and reset to 0 with every new episode
 */
enum class EvaluatorNEA {
	Sum,
	Average,
	Max,
	Last
};

// We have problems with SYS_ENUMDEF in SWIG so we use this instead.
SYS_ENUMWRAP(EvaluatorNEA, (Sum)(Average)(Max)(Last))

class NEAComputer {
private:
	RealType _score = 0;
	RealType _maxScore = -std::numeric_limits<RealType>::max();
	RealType _episodeScore = 0;
	RealType _numEpisodes = 0;

	//! We need to track whether there have been any rewards at all
	//! in the last episode in order to compute the average correctly.
	bool _hadRewards = false;

public:
	RealType getScore() const {
		return _score;
	}

	RealType getMaxScore() const {
		return _maxScore;
	}

	RealType getEpisodeScore() const {
		return _episodeScore;
	}

	RealType getNumEpisodes() const {
		return _numEpisodes;
	}

	void setScore(RealType score) {
		_score = score;
	}

	void setMaxScore(RealType maxScore) {
		_maxScore = maxScore;
	}

	void setEpisodeScore(RealType episodeScore) {
		_episodeScore = episodeScore;
	}

	void setNumEpisodes(RealType numEpisodes) {
		_numEpisodes = numEpisodes;
	}

public:
	void addReward(RealType reward);
	void nextEpisode();
	RealType combineEvaluation(EvaluatorNEA nea) const;
};

} //namespace rlearner

#endif //RlearnerStateChart_EvaluatorNEA_H_
