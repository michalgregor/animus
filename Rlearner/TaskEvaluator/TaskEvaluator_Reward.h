#ifndef RlearnerStateChart_TaskEvaluator_Reward_H_
#define RlearnerStateChart_TaskEvaluator_Reward_H_

#include "../system.h"
#include "../rewardfunction.h"

#include "TaskEvaluator.h"
#include "EvaluatorNEA.h"

namespace rlearner {

/**
 * @class TaskEvaluator_Reward
 *
 * A task evaluator that computes the evaluation as a total sum of all
 * rewards awarded by the specified reward function.
 */
class TaskEvaluator_Reward: public TaskEvaluator {
private:
	//! The reward function.
	shared_ptr<RewardFunction> _rewardFunction;
	//! Specifies how episode evaluations should be combined.
	EvaluatorNEA _nea = EvaluatorNEA::Average;
	//! The nea computer.
	NEAComputer _neaComputer = {};

public:
	virtual RealType getEvaluation() const;
	virtual void resetEvaluation();

public:
	virtual void nextStep(StateCollection* oldStateCol, Action* action, StateCollection* newStateCol);
	virtual void newEpisode();

public:
	const shared_ptr<RewardFunction>& getRewardFunction() const {
		return _rewardFunction;
	}

	void setRewardFunction(const shared_ptr<RewardFunction>& rewardFunction) {
		_rewardFunction = rewardFunction;
	}

	/**
	 * Sets EvaluatorNEA.
	 */
	void setNEA(EvaluatorNEA nea) {
		_nea = nea;
	}

	/**
	 * Returns EvaluatorNEA.
	 */
	EvaluatorNEA getNEA() const {
		return _nea;
	}

public:
	TaskEvaluator_Reward& operator=(const TaskEvaluator_Reward&) = delete;
	TaskEvaluator_Reward(const TaskEvaluator_Reward&) = delete;

	TaskEvaluator_Reward(const shared_ptr<RewardFunction>& rewardFunction = shared_nullptr, EvaluatorNEA nea = EvaluatorNEA::Average);
	virtual ~TaskEvaluator_Reward() = default;
};

} //namespace rlearner

#endif //RlearnerStateChart_TaskEvaluator_Reward_H_
