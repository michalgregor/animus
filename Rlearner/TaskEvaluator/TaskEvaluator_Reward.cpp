#include "TaskEvaluator_Reward.h"

namespace rlearner {

RealType TaskEvaluator_Reward::getEvaluation() const {
	return _neaComputer.combineEvaluation(_nea);
}

void TaskEvaluator_Reward::resetEvaluation() {
	_neaComputer = NEAComputer();
}

/**
 * Adds the reward for the current step to the evaluation score.
 */
void TaskEvaluator_Reward::nextStep(StateCollection* oldStateCol, Action* action, StateCollection* newStateCol){
	_neaComputer.addReward(_rewardFunction->getReward(oldStateCol, action, newStateCol));
}

//! Tells the Listener that a new Episode has started.
void TaskEvaluator_Reward::newEpisode() {
	_neaComputer.nextEpisode();
}

TaskEvaluator_Reward::TaskEvaluator_Reward(const shared_ptr<RewardFunction>& rewardFunction, EvaluatorNEA nea):
	_rewardFunction(rewardFunction), _nea(nea)
{}

} //namespace rlearner
