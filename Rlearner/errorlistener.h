// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cerrorlistener_H_
#define Rlearner_cerrorlistener_H_

#include "parameters.h"

namespace rlearner {

class State;
class StateCollection;
class Action;
class ActionData;
class AbstractVFunction;
class AbstractQFunction;

class ErrorListener: virtual public ParameterObject {
public:
	virtual void receiveError(RealType error, StateCollection *state,
	        Action *action, ActionData *data = nullptr) = 0;

public:
	virtual ~ErrorListener() = default;
};

class ErrorSender {
protected:
	std::list<ErrorListener *> *errorListeners;

public:
	void sendErrorToListeners(RealType error, StateCollection *state,
	        Action *action, ActionData *data = nullptr);

	void addErrorListener(ErrorListener *listener);
	void removeErrorListener(ErrorListener *listener);

public:
	ErrorSender& operator=(const ErrorSender&) = delete;
	ErrorSender(const ErrorSender&) = delete;

	ErrorSender();
	virtual ~ErrorSender();
};

class StateErrorLearner: public ErrorListener {
protected:
	AbstractVFunction *stateErrors;

public:
	virtual void receiveError(RealType error, StateCollection *state,
	        Action *action, ActionData *data = nullptr);

public:
	StateErrorLearner& operator=(const StateErrorLearner&) = delete;
	StateErrorLearner(const StateErrorLearner&) = delete;

	StateErrorLearner(AbstractVFunction *stateErrors);
	virtual ~StateErrorLearner() = default;
};

class StateActionErrorLearner: public ErrorListener {
protected:
	AbstractQFunction *stateActionErrors;

public:
	virtual void receiveError(RealType error, StateCollection *state,
	        Action *action, ActionData *data = nullptr);

public:
	StateActionErrorLearner& operator=(const StateActionErrorLearner&) = delete;
	StateActionErrorLearner(const StateActionErrorLearner&) = delete;

	StateActionErrorLearner(AbstractQFunction *stateActionErrors);
	virtual ~StateActionErrorLearner() = default;
};

} //namespace rlearner

#endif //Rlearner_cerrorlistener_H_
