// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "forest.h"
#include "datafactory.h"
#include "extratrees.h"

namespace rlearner {

RegressionForest::RegressionForest(int numTrees, int numDim) :
	Forest<RealType>(numTrees), Mapping<RealType>(numDim) {}

RegressionForest::~RegressionForest() {}

RealType RegressionForest::doGetOutputValue(ColumnVector *vector) {
	RealType average = 0;
	int numVal = 0;
	for(int i = 0; i < numTrees; i++) {
		if(forest[i] != nullptr) {
			average += forest[i]->getOutputValue(vector);
			numVal++;
		}
	}
	return average / numVal;
}

void RegressionForest::save(std::ostream& stream) {
	fmt::fprintf(stream, "%f %f\n", getAverageDepth(), getAverageNumLeaves());
}

ExtraTreeRegressionForest::ExtraTreeRegressionForest(
    int numTrees, DataSet *inputData, DataSet1D *outputData, unsigned int K,
    unsigned int n_min, RealType treshold, DataSet1D *weightData) :
	RegressionForest(numTrees, inputData->getNumDimensions()) {
	for(int i = 0; i < numTrees; i++) {
		addTree(i,
		    new ExtraRegressionTree(inputData, outputData, K, n_min, treshold,
		        weightData));
	}
}

ExtraTreeRegressionForest::~ExtraTreeRegressionForest() {
	for(int i = 0; i < numTrees; i++) {
		delete forest[i];
	}
}

RealType RegressionMultiMapping::doGetOutputValue(ColumnVector *inputVector) {
	RealType value = 0;

	for(int i = 0; i < numMappings; i++) {
		value += mappings[i]->getOutputValue(inputVector);
	}
	value = value / numMappings;

	return value;
}

RegressionMultiMapping::RegressionMultiMapping(
    int l_numMappings, int numDimensions) :
	Mapping<RealType>(numDimensions) {
	numMappings = l_numMappings;

	mappings = new Mapping<RealType> *[numMappings];

	deleteMappings = true;
}

RegressionMultiMapping::~RegressionMultiMapping() {
	if(deleteMappings) {
		for(int i = 0; i < numMappings; i++) {
			delete mappings[i];
		}
	}

	delete mappings;
}

void RegressionMultiMapping::addMapping(int index, Mapping<RealType> *mapping) {
	mappings[index] = mapping;
}

} //namespace rlearner
