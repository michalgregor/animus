// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_csupervisedlearner_H_
#define Rlearner_csupervisedlearner_H_

#include <Systematic/configure.h>

#include "parameters.h"
#include "algebra.h"

namespace rlearner {

class GradientFunction;
class GradientUpdateFunction;
class DataSet;
class DataSet1D;
class Action;
class FeatureList;
class FeatureFunction;

#ifdef USE_TORCH
class TorchGradientFunction;
#endif // USE_TORCH

class SupervisedLearner: virtual public ParameterObject {
public:
	virtual void learnFA(DataSet *inputData, DataSet1D *outputData) = 0;
	virtual void resetLearner() {}

public:
	SupervisedLearner() {}
	virtual ~SupervisedLearner() {}
};

class SupervisedWeightedLearner: virtual public ParameterObject {
public:
	virtual void learnWeightedFA(
	    DataSet *inputData,
	    DataSet1D *outputData,
	    DataSet1D *weighting) = 0;

	virtual void resetLearner() {}

public:
	SupervisedWeightedLearner() {}
	virtual ~SupervisedWeightedLearner() {}
};

class SupervisedQFunctionLearner: virtual public ParameterObject {
protected:
public:
	virtual void learnQFunction(
	    Action *action,
	    DataSet *inputData,
	    DataSet1D *outputData) = 0;

	virtual void resetLearner() {}

public:
	SupervisedQFunctionLearner() {}
	virtual ~SupervisedQFunctionLearner() {}
};

class SupervisedQFunctionWeightedLearner: virtual public ParameterObject {
public:
	virtual void learnQFunction(
	    Action *action,
	    DataSet *inputData,
	    DataSet1D *outputData,
	    DataSet1D *weightingData) = 0;

	virtual void resetLearner() {}

public:
	SupervisedQFunctionWeightedLearner() {}
	virtual ~SupervisedQFunctionWeightedLearner() {}
};

class LeastSquaresLearner: virtual public ParameterObject {
protected:
	Matrix *A;
	Matrix *A_pinv;
	ColumnVector *b;

	GradientUpdateFunction *featureFunc;

public:
	virtual RealType doOptimization();
	static RealType doOptimization(
	    Matrix *A,
	    Matrix *A_pinv,
	    ColumnVector *b,
	    ColumnVector *w,
	    RealType lambda);

public:
	LeastSquaresLearner(GradientUpdateFunction *featureFunc, int numData);
	virtual ~LeastSquaresLearner();
};

class GradientCalculator: virtual public ParameterObject {
protected:
public:
	virtual void getGradient(FeatureList *gradient) = 0;
	virtual RealType getFunctionValue() = 0;

	virtual void resetGradientCalculator() {}

public:
	virtual ~GradientCalculator() {}
};

class SupervisedGradientCalculator: public GradientCalculator {
protected:
	DataSet *inputData;
	DataSet1D *outputData1D;
	DataSet *outputData;
	GradientFunction *gradientFunction;

public:
	virtual void getGradient(FeatureList *gradient);
	virtual RealType getFunctionValue();

	virtual void setData(DataSet *inputData, DataSet1D *outputData1D);
	virtual void setData(DataSet *inputData, DataSet *outputData);

public:
	SupervisedGradientCalculator(
	    GradientFunction *gradientFunction,
	    DataSet *inputData,
	    DataSet *outputData
	);

	virtual ~SupervisedGradientCalculator();
};

class SupervisedFeatureGradientCalculator: public SupervisedGradientCalculator {
protected:
	FeatureFunction *featureFunction;
	FeatureList *featureList;

public:
	FeatureList *getFeatureList(ColumnVector *input);
	virtual void getGradient(FeatureList *gradient);
	virtual RealType getFunctionValue();

public:
	SupervisedFeatureGradientCalculator(FeatureFunction *featureFunction);
	virtual ~SupervisedFeatureGradientCalculator();
};

class GradientFunctionUpdater: virtual public ParameterObject {
protected:
	GradientUpdateFunction *updateFunction;

public:
	virtual void updateWeights(FeatureList *gradient) = 0;
	void addRandomParams(RealType randSize);

	GradientUpdateFunction *getUpdateFunction() {
		return updateFunction;
	}

public:
	GradientFunctionUpdater(GradientUpdateFunction *updateFunction);
	virtual ~GradientFunctionUpdater() {}
};

class ConstantGradientFunctionUpdater: public GradientFunctionUpdater {
public:
	virtual void updateWeights(FeatureList *gradient);

public:
	ConstantGradientFunctionUpdater(
	    GradientUpdateFunction *updateFunction,
	    RealType learningRate
	);

	virtual ~ConstantGradientFunctionUpdater() {}
};

class LineSearchGradientFunctionUpdater: public GradientFunctionUpdater {
protected:
	RealType *startParameters;
	RealType *workParameters;
	int maxSteps;
	GradientCalculator *gradientCalculator;
	RealType precision_treshold;

protected:
	void setWorkingParamters(
	    FeatureList *gradient,
	    RealType stepSize,
	    RealType *startParameters,
	    RealType *workParameters
	);

	virtual RealType getFunctionValue(
	    RealType *startParameters,
	    FeatureList *gradient,
	    RealType stepSize
	);

	void bracketMinimum(
	    RealType *startParameters,
	    FeatureList *gradient,
	    RealType fa,
	    RealType &a,
	    RealType &b,
	    RealType &c
	);

public:
	virtual void updateWeights(FeatureList *gradient);

	virtual RealType updateWeights(
	    FeatureList *gradient,
	    RealType fold,
	    RealType &lmin
	);

public:
	LineSearchGradientFunctionUpdater(
	    GradientCalculator *gradientCalculator,
	    GradientUpdateFunction *updateFunction,
	    int maxSteps
	);

	virtual ~LineSearchGradientFunctionUpdater();
};

class GradientLearner: virtual public ParameterObject {
protected:
	GradientCalculator *gradientCalculator;

public:
	virtual RealType doOptimization(int maxSteps) = 0;

	virtual void resetOptimization() {
		gradientCalculator->resetGradientCalculator();
	}

public:
	GradientLearner(GradientCalculator *gradientCalculator);
	virtual ~GradientLearner() {}
};

class SupervisedGradientLearner: public SupervisedLearner {
protected:
	GradientLearner *gradientLearner;
	SupervisedGradientCalculator *gradientCalculator;

public:
	virtual void learnFA(DataSet *inputData, DataSet1D *outputData);
	virtual void resetLearner();

public:
	SupervisedGradientLearner(
	    GradientLearner *gradientLearner,
	    SupervisedGradientCalculator *gradientCalculator,
	    int episodes
	);

	virtual ~SupervisedGradientLearner();
};

class SupervisedQFunctionLearnerFromLearners: public SupervisedQFunctionLearner {
public:
	typedef std::map<Action *, shared_ptr<SupervisedLearner> > ActionLearnerMap;

protected:
	ActionLearnerMap _learnerMap;

public:
	virtual void learnQFunction(
	    Action *action,
	    DataSet *inputData,
	    DataSet1D *outputData);

	virtual void resetLearner();

public:
	SupervisedQFunctionLearnerFromLearners(
	    const ActionLearnerMap& learnerMap
	);

	SupervisedQFunctionLearnerFromLearners(
		ActionLearnerMap&& learnerMap
	);

	virtual ~SupervisedQFunctionLearnerFromLearners();
};

class SupervisedQFunctionWeightedLearnerFromLearners:
    public SupervisedQFunctionLearner {
protected:
	std::map<Action *, SupervisedWeightedLearner *> *learnerMap;

public:
	virtual void learnQFunction(
	    Action *action,
	    DataSet *inputData,
	    DataSet1D *outputData,
	    DataSet1D *weightData);

	virtual void resetLearner();

public:
	SupervisedQFunctionWeightedLearnerFromLearners(
	    std::map<Action *, SupervisedWeightedLearner *> *learnerMap
	);

	virtual ~SupervisedQFunctionWeightedLearnerFromLearners();
};

class BatchGradientLearner: public GradientLearner {
protected:
	GradientFunctionUpdater *updater;
	FeatureList *gradient;
	RealType treshold_f;

public:
	virtual RealType doOptimization(int maxSteps);

public:
	BatchGradientLearner(
	    GradientCalculator *gradientCalculator,
	    GradientFunctionUpdater *updater
	);

	virtual ~BatchGradientLearner();
};

class ConjugateGradientLearner: public GradientLearner {
protected:
	LineSearchGradientFunctionUpdater *gradientUpdater;

	FeatureList *gradnew;
	FeatureList *gradold;
	FeatureList *d;

	RealType treshold_x;
	RealType treshold_f;
	RealType fnew;
	int exiting;

public:
	virtual RealType doOptimization(int maxGradientUpdates);
	virtual void resetOptimization();

public:
	ConjugateGradientLearner(
	    GradientCalculator *gradientCalculator,
	    LineSearchGradientFunctionUpdater *updater
	);

	virtual ~ConjugateGradientLearner();
};

#ifdef USE_TORCH

class SupervisedNeuralNetworkMatlabLearner: public SupervisedLearner {
protected:
	TorchGradientFunction *mlpFunction;

public:
	virtual void learnFA(DataSet *inputData, DataSet1D *outputData);
	virtual void resetLearner();

public:
	SupervisedNeuralNetworkMatlabLearner(
		TorchGradientFunction *mlpFunction,
		int numHidden
	);

	virtual ~SupervisedNeuralNetworkMatlabLearner();
};

class SupervisedNeuralNetworkTorchLearner:
	public SupervisedLearner, public SupervisedWeightedLearner {
protected:
	TorchGradientFunction *mlpFunction;

public:
	virtual void learnFA(DataSet *inputData, DataSet1D *outputData);
	virtual void learnWeightedFA(DataSet *inputData, DataSet1D *outputData, DataSet1D *weighting);
	virtual void resetLearner();

public:
	SupervisedNeuralNetworkTorchLearner(TorchGradientFunction *mlpFunction);
	virtual ~SupervisedNeuralNetworkTorchLearner();
};

#endif // USE_TORCH

} //namespace rlearner

#endif //Rlearner_csupervisedlearner_H_

