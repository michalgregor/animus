// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cfeaturefunction_H_
#define Rlearner_cfeaturefunction_H_

#include <iostream>
#include <list>
#include <map>

#include "system.h"

namespace rlearner {

/**
 * @class Feature
 * Class for storing a single feature (feature index and feature factor).
 *
 * Used for creating a sparse array of features in combination of FeatureList.
 */
class Feature {
public:
	unsigned int featureIndex;
	RealType factor;

public:
	Feature();
	Feature(unsigned int Index, RealType factor);

	~Feature();
};

/**
 * @class FeatureList
 * Class for storing features in the form of a sparse array.
 *
 * The class maintains represents a list of features and is often used
 * for gradient calculation.
 *
 * In a feature list, not all features have to be included, the features which
 * are not in the list have automatically the value of 0.0.
 *
 * Every feature in the list is stored as a Feature object, so the index
 * and the feature factor are stored. The FeatureList class also contains
 * methods for directly setting (set), updating (adding a factor, update)
 * or getting (getFeatureFactor) feature factors, without the need of Feature
 * objects.
 *
 * FeatureList implements all functionality of the std::list class, so iterators
 * can be used in the same way. The class provides its own resource management,
 * so it always uses its own Feature objects, so you don't have to care about
 * where the Feature objects come from or where they will go (cleaning up).
 * If the feature list gets too small, it automatically allocates new resources.
 * If the list gets smaller again, it always holds the allocated resources
 * (even if you call clear) because its likely that the resources will be needed
 * again. The allocated Feature resources get deleted only when the feature list
 * itself is deleted or clearAndDelete is called.
 *
 * For performance reasons the feature list also contains a map of Feature
 * object, with its feature index as search index. So the access to a particular
 * feature is very quick. All features that are in the list are always in
 * the map too.
 *
 * You have also the possibility to add another feature list to the current list
 * (feature factors are added, non-existent features in one list count as feature
 * factor 0, or to multiply 2 feature lists (needed for gradient multiplication).
 * The multiplied feature list naturally contains only the features which are
 * in both components of the product.
 *
 * Feature lists can also be sorted by the feature factor. This is done
 * for example in the case of etraces. If you need a sorted feature list, set
 * the isSortet flag of the constructor to true. The first feature in the list
 * will then always be the one with the highest factor.
 */
SYS_WNONVIRT_DTOR_OFF
class FeatureList: protected std::list<Feature*> {
SYS_WNONVIRT_DTOR_ON
protected:
	std::list<Feature *> *freeResources;
	std::list<Feature *> *allResources;
	std::map<int, Feature *> *featureMap;
	bool isSorted;
	bool sortAbs;

	/**
	 * Internal function, used for resource management
	 *
	 * Returns an unused or a new Feature object.
	 */
	Feature *getFreeFeatureResource();

	//! insert the given feature in the correct position.
	void sortFeature(Feature *feature);

public:
	typedef FeatureList::iterator iterator;
	typedef FeatureList::reverse_iterator reverse_iterator;
	typedef std::list<Feature *>::size_type size_type;

public:
	//! Returns iterator pointing on the given feature (if its in the list,
	//! other wise end will be returned)
	FeatureList::iterator getFeaturePos(unsigned int feature);

	/**
	 * Add a Feature object to the list.
	 *
	 * The value of the feature is added to the feature in the list or, if the
	 * feature isn't already in the list, a new feature is added to the list.
	 * Even though a Feature object is given, the feature list uses its own
	 * Feature objects.
	 */
	void add(Feature *feature);

	/**
	 * Add all features of the given list to the current feature list. Three
	 * feature factors of the given list are multiplied by "factor".
	 */
	void add(FeatureList *featureList, RealType factor = 1.0);

	/**
	 * Set the given feature to the given factor.
	 *
	 * If the feature is not already in the list a new Feature object is
	 * requested (by the function getFreeFeatureResource.
	 */
	void set(int feature, RealType factor);

	//! Multiply all feature factors with the given factor.
	void multFactor(RealType factor);

	/**
	 * Multiply all feature of the given list with the features of
	 * the current list.
	 *
	 * This calculation is like the dot product, features that are not in a
	 * list have the feature factors 0.0.
	 */
	RealType multFeatureList(FeatureList *featureList);

	//! Add to all features the given index offset.
	void addIndexOffset(int Offset);

	/**
	 * Add the given feature factor to the given feature.
	 *
	 * If the feature is not already in the list a new Feature object is
	 * requested (by the function getFreeFeatureResource) and the feature
	 * factor is set according to factor.
	 */
	void update(int feature, RealType factor);

	//! Returns the feature factor of the given feature
	RealType getFeatureFactor(int featureIndex);

	/**
	 * Returns the whole Feature object with the given factor.
	 *
	 * If the specified feature isn't in the list, nullptr is returned. Don't
	 * change any values or delete that Feature object!!!
	 */
	Feature* getFeature(int featureIndex);

	//! Removes the given feature from the list.
	void remove(Feature *feature);
	void remove(int feature);

	/**
	 * Clears the feature list.
	 *
	 * The allocated Feature objects remain in memory, they are just removed
	 * from the list and put in the freeResources list.
	 */
	void clear();

	//! Clears the feature list and deletes all Feature objects.
	void clearAndDelete();

	//! Load the feature list to a ascii stream.
	void load(std::istream& stream);
	//! Save the feature list to a ascii stream.
	void save(std::ostream& stream);

	//! Normalizes the feature list (sum of all factors = 1).
	void normalize();

	//! Returns the Euclidean length of the feature space.
	RealType getLength();

	FeatureList::iterator begin();
	FeatureList::iterator end();
	FeatureList::reverse_iterator rbegin();
	FeatureList::reverse_iterator rend();

	size_type size() {
		return std::list<Feature *>::size();
	}

public:
	FeatureList& operator=(const FeatureList&) = delete;
	FeatureList(const FeatureList&) = delete;

	/**
	 * Create a new feature list.
	 *
	 * The parameter initMemSize specifies the number of Features object that
	 * are allocated in the beginning. The second flag specifies whether
	 * the list is sorted.
	 */
	FeatureList(int initMemSize = 0, bool isSorted = false, bool sortAbs =
	        false);
	~FeatureList();
};

/**
 * @class FeatureFunction
 * The feature function for storing features in an RealType array.
 *
 * This class is base class of all V-Functions which use features (used by
 * linear approximators) and discrete states. A feature function is a table
 * storing the values of every feature. The class provides direct access to
 * the feature values through the functions setFeature, updateFeature and
 * getFeature. It also provides functions for working with feature lists
 * (setFeatureList, updateFeatureList, getFeatureList). When working with
 * feature lists, not a single feature value, but all feature values of the
 * features in the list get accessed, but each access is "multiplied" by the
 * features activation factors. So, for example if you want to update the
 * features of a feature list by the factor 5.0, and the feature list contains
 * two features, feature nr. 80 and feature nr. 85, each having the same
 * activation factor (often also called feature factor) of 0.5, than the update
 * for both features would be 2.5. The same concept is true for setFeatureList
 * and getFeatureList.
 */
class FeatureFunction {
protected:
	//! Number of features.
	unsigned int numFeatures;
	//! Feature RealType array.
	RealType *features;

	bool externFeatures;

public:
	/**
	 * Initializes the features with random values.
	 *
	 * The random values are sampled from an uniform distribution between
	 * min and max.
	 */
	void randomInit(RealType min = -1.0, RealType max = 1.0);

	void init(RealType value);

	/**
	 * Sets the feature to the specified value.
	 *
	 * The value gets multiplied by the feature factor of update object.
	 */
	void setFeature(Feature *update, RealType value);

	//! Sets the feature to the specified value.
	void setFeature(unsigned int featureIndex, RealType value);

	/**
	 * Sets all features of the list to the specified values.
	 *
	 * Calls setFeature(Feature *update, RealType value), so the value gets
	 * multiplied by the feature factor for each feature.
	 */
	void setFeatureList(FeatureList *updateList, RealType value);

	//! Adds the difference to the specified feature.
	void updateFeature(int feature, RealType difference);

	/**
	 * Adds the difference to the specified feature.
	 *
	 * The difference is multiplied by the feature factor of the update object
	 * before updating.
	 **/
	void updateFeature(Feature *update, RealType difference);

	/**
	 * Adds the difference to all features in the feature list.
	 *
	 * Calls updateFeature(Feature *update, RealType difference), so the
	 * difference is multiplied by the feature factor before updating.
	 **/
	void updateFeatureList(FeatureList *updateList, RealType value);

	//! Returns the value of the feature.
	virtual RealType getFeature(unsigned int featureIndex);

	/**
	 * Returns the summed values of the features in the list.
	 *
	 * Each value of a feature gets multiplied by the feature factor and then
	 * summed up.
	 */
	virtual RealType getFeatureList(FeatureList *featureList);

	virtual void saveFeatures(std::ostream& stream);
	virtual void loadFeatures(std::istream& stream);
	virtual void printFeatures();

	virtual unsigned int getNumFeatures();

	void postProcessWeights(RealType mean, RealType std);

public:
	FeatureFunction& operator=(const FeatureFunction&) = delete;
	FeatureFunction(const FeatureFunction&) = delete;

	//! Creates a feature function with numFeatures features.
	FeatureFunction(unsigned int numFeatures);
	FeatureFunction(unsigned int numFeatures, RealType *features);

	virtual ~FeatureFunction();
};

} //namespace rlearner

#endif //Rlearner_cfeaturefunction_H_
