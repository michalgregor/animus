// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <ctime>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <cstring>

#include "parameters.h"
#include "utility.h"
#include "ril_debug.h"

#include <boost/serialization/utility.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/list.hpp>

namespace rlearner {

/**
 * Boost serialization function.
 **/
void ParameterObject::serialize(
    IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Parameters>(*this);
	ar & parameterObjects;
}

void ParameterObject::serialize(
    OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Parameters>(*this);
	ar & parameterObjects;
}

/**
 * Boost serialization function.
 **/
void Parameters::serialize(
    IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _parameters;
	ar & _isAdaptive;
}

void Parameters::serialize(
    OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _parameters;
	ar & _isAdaptive;
}

Parameters::Parameters() :
	_parameters(), _isAdaptive() {
}

Parameters::Parameters(const Parameters& obj) :
	_parameters(), _isAdaptive() {
	addParameters(&obj);
}

Parameters& Parameters::operator=(const Parameters& obj) {
	_parameters.clear();
	_isAdaptive.clear();
	addParameters(&obj);

	return *this;
}

Parameters::~Parameters() {
}

void Parameters::setAsAdaptiveParameter(
    const std::string& parameter, bool adaptive) {
	_isAdaptive[parameter] = adaptive;
}

bool Parameters::isAdaptiveParameter(const std::string& parameter) const {
	auto iter = _isAdaptive.find(parameter);
	if(iter == _isAdaptive.end()) throw TracedError_OutOfRange(
	    "No such parameter present.");

	return iter->second;
}

void Parameters::loadParameters(std::istream& stream) {
	std::string description;
	std::string line;
	int numParams;

	stream >> description;
	stream >> numParams;

	if(stream.fail()) {
		throw TracedError_InvalidFormat("Invalid parameter file format.");
	}

	RealType value = 0.0;
	for(int i = 0; i < numParams; i++) {
		xscanf(stream, "%s : %lf\n", description, value);

		if(getParameterIndex(description) >= 0) {
			setParameter(description, value);
		} else {
			addParameter(description, value);
		}
	}
}

void Parameters::saveParameters(std::ostream& stream) {
	stream << "Parameters " << _parameters.size() << std::endl;

	for(auto it = _parameters.begin(); it != _parameters.end(); it++) {
		stream << (*it).first.c_str() << " : " << (*it).second << std::endl;
	}
}

void Parameters::addParameter(const std::string& name, RealType value) {
	auto it = _parameters.find(name);

	if(it == _parameters.end()) {
		_parameters[name] = value;
		_isAdaptive[name] = false;
	}
}

void Parameters::removeParameter(const std::string& name) {
	auto it = _parameters.find(name);

	if(it != _parameters.end()) {
		_parameters.erase(it);
	}
}

void Parameters::addParameters(const Parameters* lparameters) {
	for(int i = 0; i < lparameters->getNumParameters(); i++) {
		std::string name = lparameters->getParameterName(i);
		auto it = _parameters.find(name);

		if(it == _parameters.end()) {
			_parameters[name] = lparameters->getParameter(name);
			_isAdaptive[name] = lparameters->isAdaptiveParameter(name);
		}
	}
}

RealType Parameters::getParameter(const std::string& name) const {
	if(_parameters.find(name) != _parameters.end()) {
		return (_parameters.find(name))->second;
	} else {
		printf("Getting unknown Parameter %s, Abort!!\n", name.c_str());
		assert(false);
		return -1;
	}
}

void Parameters::setParameter(const std::string& name, RealType value) {
	auto it = _parameters.find(name);

	if(it != _parameters.end()) {
		(*it).second = value;
	} else {
		printf("Setting Parameter, Warning : Unknown Parameter %s (Value %f)\n",
		    name.c_str(), value);
	}
}

void Parameters::setParameters(Parameters *lparameters) {
	auto it = _parameters.begin();

	for(; it != _parameters.end(); it++) {
		if(lparameters->getParameterIndex((*it).first) >= 0) {
			setParameter((*it).first, lparameters->getParameter((*it).first));
		}
	}
}

RealType Parameters::getParameterFromIndex(unsigned int index) const {
	return getParameter(getParameterName(index));
}

const std::string& Parameters::getParameterName(unsigned int index) const {
	assert(index < _parameters.size());

	auto it = _parameters.begin();
	std::advance(it, index);

	return (*it).first;
}

void Parameters::setParameterWithIndex(unsigned int index, RealType value) {
	auto it = _parameters.begin();
	std::advance(it, index);

	if(it != _parameters.end()) {
		setParameter((*it).first, value);
	}
}

int Parameters::getParameterIndex(const std::string& name) const {
	auto it = _parameters.begin();

	int i = 0;
	while(it != _parameters.end() && name != (*it).first) {
		it++;
		i++;
	}

	if(it != _parameters.end()) {
		return i;
	} else {
		return -1;
	}
}

bool Parameters::containsParameters(Parameters *parametersObject) const {

	if(parametersObject->getNumParameters() > getNumParameters()) {
		return false;
	}

	for(int i = 0; i < parametersObject->getNumParameters(); i++) {
		std::string name = parametersObject->getParameterName(i);

		if(getParameterIndex(name) < 0) {
			return false;
		}
		if(fabs(getParameter(name) - parametersObject->getParameter(name))
		    > 0.00001) {
			//printf("Different Param Values for Parameter %s, %f %f\n", (*it).first.c_str(), (*it).second, parameterObject.getParameter((*it).first));
			return false;
		}
	}
	return true;
}

int Parameters::getNumParameters() const {
	return _parameters.size();
}

bool Parameters::operator==(Parameters &parameterObject) const {
	auto it = _parameters.begin();

	if(parameterObject.getNumParameters() != getNumParameters()) {
		return false;
	}

	for(int i = 0; it != _parameters.end(); it++, i++) {
		if(parameterObject.getParameterIndex((*it).first) >= 0) {
			if(!isAdaptiveParameter((*it).first)
			    && !parameterObject.isAdaptiveParameter((*it).first)
			    && fabs(
			        (*it).second - parameterObject.getParameter((*it).first))
			        > 0.00001) {
				//printf("Different Param Values for Parameter %s, %f %f\n", (*it).first.c_str(), (*it).second, parameterObject.getParameter((*it).first));
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

bool Parameters::operator <(Parameters &compareParams) const {
	auto it = _parameters.begin();
	while(it != _parameters.end()
	    && ((*it).second == compareParams.getParameter((*it).first)
	        || isAdaptiveParameter((*it).first)
	        || compareParams.isAdaptiveParameter((*it).first))) {
		it++;
	}
	if(it != _parameters.end()) {
		return (*it).second < compareParams.getParameter((*it).first);
	} else {
		return false;
	}
}

ParameterObject::ParameterObject() {
	parameterObjects = new std::list<paramPair>();
}

ParameterObject::~ParameterObject() {
	delete parameterObjects;
}

bool ParameterObject::operator==(Parameters &parameterObject) {
	auto it = _parameters.begin();

	if(parameterObject.getNumParameters() != getNumParameters()) {
		return false;
	}

	for(int i = 0; it != _parameters.end(); it++, i++) {
		if(parameterObject.getParameterIndex((*it).first) >= 0) {
			if(fabs((*it).second - parameterObject.getParameter((*it).first))
			    > 0.00001) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

void ParameterObject::setParameter(const std::string& name, RealType value) {
	Parameters::setParameter(name, value);
	parametersChanged();
}

void ParameterObject::setParameters(Parameters *parameters_) {
	Parameters::setParameters(parameters_);
	parametersChanged();
}

void ParameterObject::parametersChanged() {
	std::list<paramPair>::iterator it = parameterObjects->begin();

	for(; it != parameterObjects->end(); it++) {
		std::string prefix = (*it).second;
		for(int i = 0; i < getNumParameters(); i++) {
			std::string paramName = getParameterName(i);
			RealType value = getParameter(paramName);

			if(prefix == paramName.substr(0, prefix.length())) {
				paramName = paramName.substr(prefix.length());
				if((*it).first->getParameterIndex(paramName) >= 0
				    && fabs((*it).first->getParameter(paramName) - value)
				        > 0.00001) {
					(*it).first->setParameter(paramName, value);
				}
			}
		}
	}
	onParametersChanged();
}

void ParameterObject::addParameters(
    ParameterObject *lparameters, const std::string& prefix) {
	auto it = _parameters.begin();

	for(int i = 0; i < lparameters->getNumParameters(); i++) {
		std::string name = prefix + lparameters->getParameterName(i);
		it = _parameters.find(name);

		if(it == _parameters.end()) {
			_parameters[name] = lparameters->getParameterFromIndex(i);
		}
	}
	parameterObjects->push_back(paramPair(lparameters, prefix));
}

RealType ParameterObject::getParameter(const std::string& name) {
	return Parameters::getParameter(name);
}

AdaptiveParameterCalculator::AdaptiveParameterCalculator(
    Parameters *l_targetObject, const std::string& l_targetParameter,
    int functionKind) :
	targetParameter(l_targetParameter) {
	this->functionKind = functionKind;

	this->targetObject = l_targetObject;

	addParameter("APFunction", (RealType) functionKind);

	targetObject->setAsAdaptiveParameter(targetParameter, true);

}

AdaptiveParameterCalculator::~AdaptiveParameterCalculator() {
}

void AdaptiveParameterCalculator::onParametersChanged() {
	ParameterObject::onParametersChanged();
	functionKind = my_round(getParameter("APFunction"));
}

void AdaptiveParameterCalculator::setParameterValue(RealType value) {
	targetObject->setParameter(targetParameter, value);
}

AdaptiveParameterBoundedValuesCalculator::AdaptiveParameterBoundedValuesCalculator(
    Parameters *l_targetObject, std::string l_targetParameter, int functionKind,
    RealType paramOffset, RealType paramScale,
    RealType targetMin, RealType targetMax) :
	AdaptiveParameterCalculator(l_targetObject, l_targetParameter, functionKind) {
	this->targetMin = targetMin;
	this->targetMax = targetMax;
	this->paramOffset = paramOffset;
	this->paramScale = paramScale;

	addParameter("APTargetMin", targetMin);
	addParameter("APTargetMax", targetMax);
	addParameter("APParamOffset", paramOffset);
	addParameter("APParamScale", paramScale);
	addParameter("APInvertTargetFunction", 0.0);
	addParameter("APTargetScale", 1.0);

	invertTarget = false;
	targetScale = 1.0;
}

AdaptiveParameterBoundedValuesCalculator::~AdaptiveParameterBoundedValuesCalculator() {
}

void AdaptiveParameterBoundedValuesCalculator::onParametersChanged() {
	AdaptiveParameterCalculator::onParametersChanged();

	targetMin = getParameter("APTargetMin");
	targetMax = getParameter("APTargetMax");
	targetScale = getParameter("APTargetScale");
	paramScale = getParameter("APParamScale");
	paramOffset = getParameter("APParamOffset");
	invertTarget = getParameter("APInvertTargetFunction") > 0.5;
}

void AdaptiveParameterBoundedValuesCalculator::setParameterValue(
RealType targetValue) {
	RealType functionValue = 0.0;
	RealType functionArgument = (targetValue - targetMin)
	    / (targetMax - targetMin);

	if(functionArgument < 0) {
		functionArgument = 0;
	} else {
		if(functionArgument > 1.0) {
			functionArgument = 1.0;
		}
	}

	switch(functionKind) {
	case LINEAR:
		functionValue = functionArgument;
	break;
	case SQUARE:
		functionValue = pow(functionArgument, 2);
	break;
	case LOG:
		assert(functionArgument >= 0);
		functionValue = log(functionArgument * targetScale + 1.0)
		    / log(1.0 + targetScale);
	break;
	case FRACT:
		assert(functionArgument > 0);
		functionValue = (1.0 / (functionArgument * targetScale + 1.0)
		    - 1.0 / (targetScale + 1.0)) * (1.0 + targetScale) / targetScale;
	break;
	case FRACTSQUARE: {
		assert(functionArgument > 0);
		RealType powScale = pow(targetScale, (RealType) 2.0);
		functionValue = (1.0 / (pow(functionArgument, 2) * powScale + 1.0)
		    - 1.0 / (powScale + 1.0)) * (1.0 + powScale) / powScale;
	break;}
	case FRACTLOG: {
		assert(functionArgument >= 0);
		RealType offset = 1.0 / (1.0 + log(1.0 + targetScale));
		functionValue = (1.0 / (1.0 + log(functionArgument * targetScale + 1.0))
		    - offset) / (1 - offset);
	break;}
	default:
		functionValue = functionArgument;
		printf(
		    "Unknown Adaptive Parameter Function Value %d, linear used instead\n",
		    functionKind);
	break;
	}

	if(invertTarget) {
		functionValue = 1 - functionValue;
	}

	AdaptiveParameterCalculator::setParameterValue(
	    paramOffset + paramScale * functionValue);
}

AdaptiveParameterUnBoundedValuesCalculator::AdaptiveParameterUnBoundedValuesCalculator(
    Parameters *l_targetObject, std::string l_targetParameter, int functionKind,
    RealType param0, RealType paramScale, RealType target0,
    RealType targetScale
):
	AdaptiveParameterCalculator(l_targetObject, l_targetParameter, functionKind),
	targetOffset(target0),
	targetScale(targetScale),
	paramOffset(0),
	paramScale(paramScale),
	paramLimit(0)
{
	addParameter("APTargetOffset", target0);
	addParameter("APTargetScale", targetScale);
	addParameter("APParamOffset", param0);
	addParameter("APParamScale", paramScale);
	addParameter("APParamLimit", 0.0);
}

AdaptiveParameterUnBoundedValuesCalculator::~AdaptiveParameterUnBoundedValuesCalculator() {}

void AdaptiveParameterUnBoundedValuesCalculator::onParametersChanged() {
	AdaptiveParameterCalculator::onParametersChanged();

	targetOffset = getParameter("APTargetOffset");
	targetScale = getParameter("APTargetScale");
	paramOffset = getParameter("APParamOffset");
	paramScale = getParameter("APParamScale");
	paramLimit = getParameter("APParamLimit");
}

void AdaptiveParameterUnBoundedValuesCalculator::setParameterValue(
RealType targetValue) {
	RealType functionValue = 0.0;
	RealType functionArgument = targetOffset + targetValue * targetScale;

	switch(functionKind) {
	case LINEAR:
		functionValue = functionArgument;
	break;
	case SQUARE:
		functionValue = pow(functionArgument, 2);
	break;
	case LOG:
		assert(functionArgument >= 0);
		functionValue = log(functionArgument + 1.0);
	break;
	case FRACT:
		assert(functionArgument > 0);
		functionValue = (1.0 / (functionArgument + 1.0));
	break;
	case FRACTSQUARE:
		assert(functionArgument > 0);
		functionValue = 1.0 / (pow(functionArgument, 2) + 1.0);
	break;
	case FRACTLOG:
		assert(functionArgument >= 0);
		functionValue = 1.0 / (1.0 + log(functionArgument + 1.0));
	break;
	case EXP:
		functionValue = exp(functionArgument);
	break;
	default:
		functionValue = functionArgument;
		printf(
		    "Unknown Adaptive Parameter Function Value %d, linear used instead\n",
		    functionKind);
	break;
	}

	RealType paramValue = paramOffset + paramScale * functionValue;
	AdaptiveParameterCalculator::setParameterValue(paramValue);
}

} //namespace rlearner
