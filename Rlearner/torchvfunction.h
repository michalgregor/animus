// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ctorchvfunction_H_
#define Rlearner_ctorchvfunction_H_

#include <Systematic/configure.h>

#ifdef USE_TORCH

#include <torch/ConnectedMachine.h>
#include <torch/MLP.h>

#include "vfunction.h"
#include "continuousactions.h"

namespace rlearner {

using Torch::Sequence;
using Torch::Parameters;
using Torch::Machine;
using Torch::GradientMachine;
using Torch::real;

/**
 * @class TorchFunction
 * Interface to integrate Torch Machines in the learning systems.
 *
 * This class only handles the output of the torch machines. It provides
 * a function for transforming a RIL toolbox state object in a torch input
 * sequence. Therefore the continuous and then the discrete state variables,
 * are written in the sequence. When the function getValue is called the state
 * is converted into the torch sequence and then passed to the torch machine,
 * and the output of the machine is returned. Learning (i.e. setting and
 * updating the Values) can only be used with the class
 * VFunctionFromGradientFunction.
 *
 * @see VFunctionFromGradientFunction
 */
class TorchFunction {
protected:
	//! Pointer to the torch machine.
	Machine *machine;
	//! The input sequence, needed to feed the torch machines.
	Sequence *input;

public:
	//! Returns the torch machine.
	virtual Machine *getMachine();
	virtual RealType getValueFromMachine(Sequence *state);

public:
	TorchFunction& operator=(const TorchFunction&) = delete;
	TorchFunction(const TorchFunction&) = delete;

	//! Initialises the input sequence with one frame of size
	//! |continuousStates| + |discreteStates| of the properties object.
	TorchFunction(Machine *machine);
	virtual ~TorchFunction();
};

/**
 * @class TorchGradientFunction
 * Class for learning with Torch-Gradient machines.
 *
 * Extends the ability from TorchVFunction, to learn with a torch gradient
 * machine. The parameters of the machine are updated by adding the current
 * gradient of the parameters multiplied with the difference given by
 * updateValue.
 */
class TorchGradientFunction: public TorchFunction, public GradientFunction {
protected:
	Sequence *alpha;
	//! Pointer to the gradient Machine
	GradientMachine *gradientMachine;
	AdaptiveEtaCalculator *localEtaCalc;

public:
	//! Resets the parameters of the gradient machine.
	virtual void resetData();

	virtual void updateWeights(FeatureList *gradientFeatures);

	virtual int getNumWeights();

	virtual void getInputDerivationPre(ColumnVector *input,
	        Matrix *targetVector);
	virtual void getFunctionValuePre(ColumnVector *input, ColumnVector *output);

	virtual void getWeights(RealType *parameters);
	virtual void setWeights(RealType *parameters);

	virtual void getGradientPre(ColumnVector *input, ColumnVector *outputErrors,
	        FeatureList *gradientFeatures);

	void setGradientMachine(GradientMachine *gradientMachine);
	GradientMachine *getGradientMachine();

public:
	TorchGradientFunction& operator=(const TorchGradientFunction&) = delete;
	TorchGradientFunction(const TorchGradientFunction&) = delete;

	//! Creates a new value function learning with a torch gradient machine.
	TorchGradientFunction(int numInputs, int numOutputs);
	TorchGradientFunction(GradientMachine *machine);
	virtual ~TorchGradientFunction();
};

class CTorchGradientEtaCalculator: public IndividualEtaCalculator {
public:
	CTorchGradientEtaCalculator(GradientMachine *gradientMachine);
	virtual ~CTorchGradientEtaCalculator() = default;
};

class TorchVFunction: public AbstractVFunction {
protected:
	TorchFunction *torchFunction;
	Sequence *input;

protected:
	/**
	 * Converts the state in an torch sequence.
	 *
	 * The sequence has to have the frame size
	 * |continuousStates| + |discreteStates| of the state.
	 */
	void getInputSequence(State *state, Sequence *input);

public:
	//! Converts the state into an input sequence, transfers the sequence
	//! to the machine and returns its output.
	virtual RealType getValue(State *state);

public:
	TorchVFunction& operator=(const TorchVFunction&) = delete;
	TorchVFunction(const TorchVFunction&) = delete;

	//! Initialises the input sequence with one frame of size
	//! |continuousStates| + |discreteStates| of the properties object.
	TorchVFunction(TorchFunction *torchFunction,
	        StateProperties *properties);
	virtual ~TorchVFunction();
};

/**
 * @class VFunctionFromGradientFunction
 * Class for learning with Torch-Gradient machines.
 *
 * Extends the ability from TorchVFunction, to learn with a torch gradient
 * machine. The parameters of the machine are updated by adding the current
 * gradient of the parameters multiplied with the difference given by
 * updateValue.
 */
class VFunctionFromGradientFunction: public GradientVFunction,
        public VFunctionInputDerivationCalculator {
protected:
	//! Pointer to the gradient Machine.
	GradientFunction *gradientFunction;

	ColumnVector *input;
	ColumnVector *outputError;
	Matrix *inputDerivation;

protected:
	virtual void updateWeights(FeatureList *gradientFeatures);
	void getInputSequence(State *state, ColumnVector *sequence);

public:
	/**
	 * Calls update value with "value" - currentValue as parameter.
	 *
	 * For learning only updateValue should be used.
	 */
	virtual void setValue(State *state, RealType value);

	/**
	 * Updates the parameters of the machine by adding the gradient
	 * to the parameters.
	 *
	 * Resets the parameters of the gradient machine.
	 */
	virtual void resetData();

	//! Converts the state into an input sequence, transfers the sequence
	//! to the machine and returns its output.
	virtual RealType getValue(State *state);

	virtual void getGradient(StateCollection *originalState,
	        FeatureList *modifiedState);

	virtual int getNumWeights();

	virtual AbstractVETraces *getStandardETraces();

	void getInputDerivation(StateCollection *originalState,
	        ColumnVector *targetVector);

	virtual void getWeights(RealType *parameters);
	virtual void setWeights(RealType *parameters);

public:
	VFunctionFromGradientFunction& operator=(const VFunctionFromGradientFunction&) = delete;
	VFunctionFromGradientFunction(const VFunctionFromGradientFunction&) = delete;

	//! Creates a new value function learning with a torch gradient machine.
	VFunctionFromGradientFunction(GradientFunction *gradientFunction,
	        StateProperties *properties);
	virtual ~VFunctionFromGradientFunction();
};

class QFunctionFromGradientFunction: public ContinuousActionQFunction,
        StateObject {
protected:
	GradientFunction *gradientFunction;
	ColumnVector *input;
	ColumnVector *outputError;
	ActionSet *staticActions;

protected:
	void getInputSequence(ColumnVector *input, State *state,
	        ContinuousActionData *data);
	virtual void updateWeights(FeatureList *gradientFeatures);

public:
	virtual void getBestContinuousAction(StateCollection *state,
	        ContinuousActionData *actionData);

	virtual void updateCAValue(StateCollection *state,
	        ContinuousActionData *data, RealType td);
	virtual void setCAValue(StateCollection *state,
	        ContinuousActionData *data, RealType qValue);
	virtual RealType getCAValue(StateCollection *state,
	        ContinuousActionData *data);

	virtual void getCAGradient(StateCollection *state,
	        ContinuousActionData *data, FeatureList *gradient);
	virtual int getNumWeights();

	virtual void getWeights(RealType *parameters);
	virtual void setWeights(RealType *parameters);

	virtual void resetData();

public:
	QFunctionFromGradientFunction& operator=(const QFunctionFromGradientFunction&) = delete;
	QFunctionFromGradientFunction(const QFunctionFromGradientFunction&) = delete;

	QFunctionFromGradientFunction(ContinuousAction *contAction,
	        GradientFunction *torchGradientFunction, ActionSet *actions,
	        StateProperties *properties);
	virtual ~QFunctionFromGradientFunction();
};

} //namespace rlearner

#endif // USE_TORCH

#endif //Rlearner_ctorchvfunction_H_

