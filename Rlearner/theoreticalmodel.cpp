// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "ril_debug.h"
#include "theoreticalmodel.h"
#include "episodehistory.h"
#include "utility.h"
#include "vfunction.h"
#include "qfunction.h"
#include "discretizer.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "action.h"

namespace rlearner {

Transition::Transition(int startState, int endState, RealType prop) {
	this->startState = startState;
	this->endState = endState;
	this->propability = prop;

	type = 1;
}

bool Transition::isType(int Type) {
	return (this->type & Type) > 0;
}

int Transition::getStartState() {
	return startState;
}

int Transition::getEndState() {
	return endState;
}

RealType Transition::getPropability() {
	return propability;
}

void Transition::setPropability(RealType prop) {
	propability = prop;
}

void Transition::save(std::ostream& stream, bool forward) {
	int state;

	if(forward) {
		state = getEndState();
	} else {
		state = getStartState();
	}

	fmt::fprintf(stream, "(%d %lf)", state, getPropability());
}

void Transition::load(std::istream& stream, int fixedState, bool forward) {
	int state;
	xscanf(stream, "(%d %lf)", state, propability);

	if(forward) {
		endState = state;
		startState = fixedState;
	} else {
		startState = state;
		endState = fixedState;
	}
}

SemiMDPTransition::SemiMDPTransition(int startState, int endState, RealType prop) :
	Transition(startState, endState, prop) {
	durations = new std::map<int, RealType>();
	type = type | SEMIMDPTRANSITION;
}

SemiMDPTransition::~SemiMDPTransition() {
	delete durations;
}

std::map<int, RealType> *SemiMDPTransition::getDurations() {
	return durations;
}

void SemiMDPTransition::addDuration(int duration, RealType factor) {
	RealType normalize = (1 - factor);
	std::map<int, RealType>::iterator it = durations->begin();

	for(; it != durations->end(); it++) {
		(*it).second *= normalize;
	}

	it = durations->find(duration);

	if(it != durations->end()) {
		(*it).second += factor;
	} else {
		(*durations)[duration] = factor;
	}
}

RealType SemiMDPTransition::getDurationFaktor(int duration) {
	return (*durations)[duration];
}

RealType SemiMDPTransition::getDurationPropability(int duration) {
	return getPropability() * getDurationFaktor(duration);
}

RealType SemiMDPTransition::getSemiMDPFaktor(RealType gamma) {
	RealType factor = 0.0;
	std::map<int, RealType>::iterator itDurations = getDurations()->begin();
	for(; itDurations != getDurations()->end(); itDurations++) {
		factor += pow(gamma, (*itDurations).first - 1) * (*itDurations).second;
	}
	return factor;
}

void SemiMDPTransition::load(std::istream& stream, int fixedState, bool forward) {
	int bufDuration;
	RealType bufFaktor;
	char buf;

	Transition::load(stream, fixedState, forward);

	xscanf(stream, "[");

	stream.get(buf);
	while(buf != ']') {
		xscanf(stream, "%d %lf)", bufDuration, bufFaktor);
		(*durations)[bufDuration] = bufFaktor;
		stream.get(buf);
	}
}

void SemiMDPTransition::save(std::ostream& stream, bool forward) {
	Transition::save(stream, forward);

	std::map<int, RealType>::iterator it = this->getDurations()->begin();

	fmt::fprintf(stream, "[");

	for(; it != getDurations()->end(); it++) {
		fmt::fprintf(stream, "(%d %f)", (*it).first, (*it).second);
	}

	fmt::fprintf(stream, "]");
}

TransitionList::TransitionList(bool forwardList) {
	this->forwardList = forwardList;
}

TransitionList::iterator TransitionList::getTransitionIterator(int index) {
	TransitionList::iterator it = begin();

	if(forwardList) {
		while(it != end() && (*it)->getEndState() < index) {
			it++;
		}
	} else {
		while(it != end() && (*it)->getStartState() < index) {
			it++;
		}
	}

	return it;
}

bool TransitionList::isMember(int featureIndex) {
	TransitionList::iterator it = getTransitionIterator(featureIndex);

	if(forwardList) {
		return it != end() && (*it)->getEndState() == featureIndex;
	} else {
		return it != end() && (*it)->getStartState() == featureIndex;
	}
}

bool TransitionList::isForwardList() {
	return forwardList;
}

void TransitionList::addTransition(Transition *transition) {
	TransitionList::iterator it;

	if(isForwardList()) {
		it = getTransitionIterator(transition->getEndState());
	} else {
		it = getTransitionIterator(transition->getStartState());
	}
	insert(it, transition);
}

Transition *TransitionList::getTransition(int featureIndex) {
	TransitionList::iterator it = getTransitionIterator(featureIndex);

	if(it != end()) {
		return (*it);
	} else return nullptr;
}

void TransitionList::clearAndDelete() {
	TransitionList::iterator it = begin();

	for(; it != end(); it++) {
		delete (*it);
	}

	clear();
}

StateActionTransitions::StateActionTransitions() {
	forwardList = new TransitionList(true);
	backwardList = new TransitionList(false);
}

StateActionTransitions::~StateActionTransitions() {
	forwardList->clearAndDelete();
	backwardList->clear();

	delete forwardList;
	delete backwardList;
}

TransitionList* StateActionTransitions::getForwardTransitions() {
	return forwardList;
}

TransitionList* StateActionTransitions::getBackwardTransitions() {
	return backwardList;
}

unsigned int AbstractFeatureStochasticModel::getNumFeatures() {
	return numFeatures;
}

AbstractFeatureStochasticModel::AbstractFeatureStochasticModel(
    ActionSet *actions, int numFeatures) :
	ActionObject(actions) {
	this->numFeatures = numFeatures;
	discretizer = nullptr;
	createdActions = false;
}

AbstractFeatureStochasticModel::AbstractFeatureStochasticModel(
    int numActions, int numFeatures) :
	ActionObject(new ActionSet()) {
	this->numFeatures = numFeatures;
	discretizer = nullptr;

	for(int i = 0; i < numActions; i++) {
		actions->add(new StochasticModelAction(this));
	}

	createdActions = true;
}

AbstractFeatureStochasticModel::AbstractFeatureStochasticModel(
    ActionSet *actions, StateModifier *l_discretizer) :
	ActionObject(actions) {
	this->numFeatures = l_discretizer->getDiscreteStateSize();
	discretizer = l_discretizer;

	createdActions = false;
}

AbstractFeatureStochasticModel::~AbstractFeatureStochasticModel() {
	if(createdActions) {
		ActionSet::iterator it = actions->begin();

		for(; it != actions->end(); it++) {
			delete *it;
		}

		delete actions;
	}
}

RealType AbstractFeatureStochasticModel::getPropability(
    int oldState, Action *action, int newState) {
	int index = getActions()->getIndex(action);
	return getPropability(oldState, index, newState);
}

RealType AbstractFeatureStochasticModel::getPropability(
    FeatureList *oldList, Action *action, FeatureList *newList) {
	RealType propability = 0.0;
	FeatureList::iterator itOld;
	FeatureList::iterator itNew = newList->begin();

	int actoinIndex = getActions()->getIndex(action);

	for(itOld = oldList->begin(); itOld != oldList->end(); itOld++) {
		for(itNew = newList->begin(); itNew != newList->end(); itNew++) {
			propability += getPropability((*itOld)->featureIndex, actoinIndex,
			    (*itNew)->featureIndex) * (*itNew)->factor * (*itOld)->factor;
		}
	}

	return propability;
}

RealType AbstractFeatureStochasticModel::getPropability(
    StateCollection *oldState, Action *action, StateCollection *newState) {
	return getPropability(oldState->getState(discretizer), action,
	    newState->getState(discretizer));
}

RealType AbstractFeatureStochasticModel::getPropability(
    State *oldState, Action *action, State *newState) {
	RealType propability = 0;
	int actoinIndex = getActions()->getIndex(action);

	for(unsigned int i = 0; i < oldState->getNumDiscreteStates(); i++) {
		for(unsigned int j = 0; j < newState->getNumDiscreteStates(); j++) {
			propability += getPropability(i, actoinIndex, j)
			    * newState->getContinuousState(j)
			    * oldState->getContinuousState(i);
		}
	}

	return propability;
}

TransitionList* AbstractFeatureStochasticModel::getForwardTransitions(
    Action *action, State *state) {
	return getForwardTransitions(actions->getIndex(action),
	    state->getDiscreteState(0));
}

TransitionList*
AbstractFeatureStochasticModel::getForwardTransitions(
    Action *action, StateCollection *state) {
	return getForwardTransitions(action, state->getState());
}

FeatureStochasticModel::FeatureStochasticModel(
    ActionSet *actions, int numFeatures, std::istream& stream
):
	AbstractFeatureStochasticModel(actions, numFeatures) {
	stateTransitions = new MyArray2D<StateActionTransitions *>(getNumActions(),
	    numFeatures);

	for(int i = 0; i < stateTransitions->getSize(); i++) {
		stateTransitions->set1D(i, new StateActionTransitions());
	}

	load(stream);
}

FeatureStochasticModel::FeatureStochasticModel(
    ActionSet *actions, int numFeatures
):
	AbstractFeatureStochasticModel(actions, numFeatures)
{
	stateTransitions = new MyArray2D<StateActionTransitions *>(getNumActions(),
	    numFeatures);

	for(int i = 0; i < stateTransitions->getSize(); i++) {
		stateTransitions->set1D(i, new StateActionTransitions());
	}
}
FeatureStochasticModel::FeatureStochasticModel(int numActions, int numFeatures) :
	AbstractFeatureStochasticModel(numActions, numFeatures) {

	stateTransitions = new MyArray2D<StateActionTransitions *>(getNumActions(),
	    numFeatures);

	for(int i = 0; i < stateTransitions->getSize(); i++) {
		stateTransitions->set1D(i, new StateActionTransitions());
	}
}

FeatureStochasticModel::~FeatureStochasticModel() {
	StateActionTransitions *saPair = nullptr;

	for(int i = 0; i < stateTransitions->getSize(); i++) {
		saPair = stateTransitions->get1D(i);
		delete saPair;
	}

	delete stateTransitions;
}

Transition *FeatureStochasticModel::getNewTransition(
    int startState, int endState, Action *action, RealType prop) {
	if(action->isType(AF_MultistepAction)) {
		return new SemiMDPTransition(startState, endState, prop);
	} else return new Transition(startState, endState, prop);
}

void FeatureStochasticModel::setPropability(
    RealType propability, int oldState, int action, int newState) {
	StateActionTransitions *saTrans = stateTransitions->get(action, oldState);

	if(saTrans->getForwardTransitions()->isMember(newState)) {
		saTrans->getForwardTransitions()->getTransition(newState)->setPropability(
		    propability);
	} else {
		Transition *trans = getNewTransition(oldState, newState,
		    actions->get(action), propability);

		saTrans->getForwardTransitions()->addTransition(trans);
		stateTransitions->get(action, newState)->getBackwardTransitions()->addTransition(
		    trans);
	}
}

RealType FeatureStochasticModel::getPropability(
    int oldState, int action, int newState) {
	assert(action > 0);

	StateActionTransitions *saTrans = stateTransitions->get(action, oldState);

	Transition *trans = saTrans->getForwardTransitions()->getTransition(
	    newState);

	if(trans == nullptr) {
		return 0.0;
	}

	return trans->getPropability();
}

RealType FeatureStochasticModel::getPropability(
    int oldFeature, int action, int duration, int newFeature) {
	if(actions->get(action)->isType(AF_MultistepAction)) {
		SemiMDPTransition *trans = (SemiMDPTransition *) stateTransitions->get(
		    action, oldFeature)->getForwardTransitions()->getTransition(
		    newFeature);

		if(trans != nullptr) {
			return trans->getPropability() * trans->getDurationFaktor(duration);
		}

		else {
			return 0.0;
		}
	} else return getPropability(oldFeature, action, newFeature);
}

void FeatureStochasticModel::setPropability(
    RealType propability, int oldFeature, int action, int duration,
    int newFeature) {
	if(actions->get(action)->isType(AF_MultistepAction)) {

		SemiMDPTransition *trans = (SemiMDPTransition *) stateTransitions->get(
		    action, oldFeature)->getForwardTransitions()->getTransition(
		    newFeature);
		if(trans != nullptr) {
			RealType durationProp = trans->getDurationFaktor(duration)
			    * trans->getPropability();
			trans->setPropability(
			    trans->getPropability() - durationProp + propability);
			trans->addDuration(duration,
			    (propability - durationProp) / trans->getPropability());
		} else {
			trans = (SemiMDPTransition *) getNewTransition(oldFeature,
			    newFeature, actions->get(action), propability);
			trans->addDuration(duration, 1.0);
		}
	} else setPropability(propability, oldFeature, action, newFeature);
}

TransitionList *FeatureStochasticModel::getForwardTransitions(
    int action, int oldState) {
	return stateTransitions->get(action, oldState)->getForwardTransitions();
}

TransitionList *FeatureStochasticModel::getBackwardTransitions(
    int action, int oldState) {
	return stateTransitions->get(action, oldState)->getBackwardTransitions();
}

void FeatureStochasticModel::save(std::ostream& stream) {
	TransitionList *transList;
	TransitionList::iterator it;

	for(unsigned int i = 0; i < getNumActions(); i++) {
		fmt::fprintf(stream, "Action %d\n", i);

		for(unsigned int startState = 0; startState < numFeatures;
		    startState++) {
			transList =
			    stateTransitions->get(i, startState)->getForwardTransitions();
			fmt::fprintf(stream, "Startstate %d [%d]: ", startState,
			    transList->size());

			for(it = transList->begin(); it != transList->end(); it++) {
				(*it)->save(stream, true);
			}
			fmt::fprintf(stream, "\n");
		}
	}
}

void FeatureStochasticModel::load(std::istream& stream) {
	int action = 0;
	int startState = 0;
	int numTransitions = 0;

	for(unsigned int i = 0; i < getNumActions(); i++) {
		xscanf(stream, "Action %d\n", action);

		for(unsigned int j = 0; j < numFeatures; j++) {
			assert(
			    xscanf(stream, "Startstate %d [%d]: ", startState,
			        numTransitions) == 2);

			for(int k = 0; k < numTransitions; k++) {
				Transition *newTrans = getNewTransition(0, 0, actions->get(i),
				    0.0);
				newTrans->load(stream, j, true);
				stateTransitions->get(i, j)->getForwardTransitions()->addTransition(
				    newTrans);
				stateTransitions->get(i, newTrans->getEndState())->getBackwardTransitions()->addTransition(
				    newTrans);
			}
			xscanf(stream, "\n");
		}
	}
}

StochasticModelAction::StochasticModelAction(
    AbstractFeatureStochasticModel *l_model
) {
	this->model = l_model;
}

bool StochasticModelAction::isAvailable(StateCollection *state) {
	return model->getForwardTransitions(this, state)->size() > 0;
}

AbstractFeatureStochasticEstimatedModel::AbstractFeatureStochasticEstimatedModel(
    StateProperties *properties, FeatureQFunction *stateActionVisits,
    ActionSet *actions, int numFeatures) :
	FeatureStochasticModel(actions, numFeatures), StateObject(properties) {
	this->stateActionVisits = stateActionVisits;

	addParameter("EstimatedModelForgetFactor", 1.0);
}

AbstractFeatureStochasticEstimatedModel::~AbstractFeatureStochasticEstimatedModel() {}

void AbstractFeatureStochasticEstimatedModel::saveData(std::ostream& stream) {
	FeatureStochasticModel::save(stream);
}

void AbstractFeatureStochasticEstimatedModel::loadData(std::istream& stream) {
	FeatureStochasticModel::load(stream);
}

void AbstractFeatureStochasticEstimatedModel::resetData() {
	StateActionTransitions *saPair = nullptr;

	for(int i = 0; i < stateTransitions->getSize(); i++) {
		saPair = stateTransitions->get1D(i);
		delete saPair;
		stateTransitions->set1D(i, new StateActionTransitions());
	}

	stateActionVisits->resetData();
}

RealType AbstractFeatureStochasticEstimatedModel::getStateActionVisits(
    int Feature, int action) {
	Action *actionObj = actions->get(action);
	return stateActionVisits->getValue(Feature, actionObj, nullptr);
}

RealType AbstractFeatureStochasticEstimatedModel::getStateVisits(int Feature) {
	RealType sum = 0;

	for(unsigned int i = 0; i < getNumActions(); i++) {
		sum += getStateActionVisits(i, Feature);
	}

	return sum;
}

void AbstractFeatureStochasticEstimatedModel::intermediateStep(
    StateCollection *oldState, Action *action, StateCollection *nextState
) {
	nextStep(oldState, action, nextState);
}

void AbstractFeatureStochasticEstimatedModel::updateStep(
    int oldFeature, Action *action, int newFeature, RealType factor) {
	RealType propability = 0.0;
//	RealType timeFactor = getParameter("EstimatedModelForgetFactor");

	bool found = false;
	int actionIndex = getActions()->getIndex(action);

	RealType newSAVisits = stateActionVisits->getValue(oldFeature, action,
	    nullptr);
	RealType oldSAVisits = newSAVisits - factor;

	if(newSAVisits < 0.0001) {
		return;
	}

	TransitionList *transList =
	    stateTransitions->get(actionIndex, oldFeature)->getForwardTransitions();

	TransitionList::iterator trans = transList->begin();

	for(; trans != transList->end(); trans++) {
		propability = (*trans)->getPropability() * oldSAVisits;

		if((*trans)->getEndState() == newFeature) {
			found = true;
			propability += factor;

			if(action->isType(AF_MultistepAction)) {
				int duration =
				    dynamic_cast<MultiStepAction *>(action)->getDuration();
				SemiMDPTransition *semiTrans = (SemiMDPTransition *) (*trans);
				semiTrans->addDuration(duration, factor / (propability));
			}
		}
		propability = propability / newSAVisits;

		assert(propability >= 0);
		(*trans)->setPropability(propability);
	}

	if(!found) {
		setPropability(factor / newSAVisits, oldFeature, actionIndex,
		    newFeature);
		if(action->isType(AF_MultistepAction)) {
			int duration =
			    dynamic_cast<MultiStepAction *>(action)->getDuration();
			SemiMDPTransition *semiTrans =
			    (SemiMDPTransition *) transList->getTransition(newFeature);
			semiTrans->addDuration(duration, 1.0);

		}
	}
}

DiscreteStochasticEstimatedModel::DiscreteStochasticEstimatedModel(
    AbstractStateDiscretizer *discState, FeatureQFunction *stateActionVisits,
    ActionSet *actions
):
	AbstractFeatureStochasticEstimatedModel(discState, stateActionVisits,
		actions, discState->getDiscreteStateSize()) {}

void DiscreteStochasticEstimatedModel::nextStep(
    StateCollection *oldState, Action *action, StateCollection *newState) {
	int oldStateNum = oldState->getState(properties)->getDiscreteState(0);
	int newStateNum = newState->getState(properties)->getDiscreteState(0);

	updateStep(oldStateNum, action, newStateNum, 1.0);
}

int DiscreteStochasticEstimatedModel::getStateActionVisits(
    int state, int action
) {
	return (int) floor(
	    AbstractFeatureStochasticEstimatedModel::getStateActionVisits(state,
	        action));
}

int DiscreteStochasticEstimatedModel::getStateVisits(int state) {
	int sum = 0;

	for(unsigned int i = 0; i < getNumActions(); i++) {
		sum += (int) floor(
		    AbstractFeatureStochasticEstimatedModel::getStateVisits(state));
	}

	return sum;
}

FeatureStochasticEstimatedModel::FeatureStochasticEstimatedModel(
    FeatureCalculator *featCalc_, FeatureQFunction *stateActionVisits,
    ActionSet *actions
):
	AbstractFeatureStochasticEstimatedModel(featCalc_, stateActionVisits,
		actions, featCalc->getNumFeatures()),
	featCalc(featCalc_)
{
	addParameter("EstimatedModelMinimumUpdateFactor", 0.005);
}

void FeatureStochasticEstimatedModel::nextStep(
    StateCollection *oldState, Action *action, StateCollection *newState
) {
	//int actionIndex = getModelProperties()->getActions()->index(action);

	State *oldS = oldState->getState(properties);
	State *newS = newState->getState(properties);

	RealType minimumUpdate = getParameter("EstimatedModelMinimumUpdateFactor");

	for(unsigned int i = 0; i < oldS->getNumContinuousStates(); i++) {
		for(unsigned int j = 0; j < oldS->getNumContinuousStates(); j++) {
			RealType factor = oldS->getContinuousState(i)
			    * newS->getContinuousState(j);
			if(factor > minimumUpdate) {
				updateStep(oldS->getDiscreteState(i), action,
				    newS->getDiscreteState(j), factor);
			}
		}
	}
}

} //namespace rlearner
