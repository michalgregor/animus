// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cqfunction_H_
#define Rlearner_cqfunction_H_

#include <cstdio>

#include "learndataobject.h"
#include "baseobjects.h"
#include "gradientfunction.h"

#define GRADIENTQFUNCTION 1
#define AF_ContinuousActionQFUNCTION 2

namespace rlearner {

class AbstractFeatureStochasticModel;
class AbstractQETraces;
class GradientQETraces;

class AbstractVFunction;
class FeatureVFunction;
class FeatureRewardFunction;
class ActionStatistics;
class Feature;

/**
 * @class AbstractQFunction
 * Interface for all Q-Functions.
 *
 * Q-Functions depend on the state and the action you can choose in the current
 * state, so the policy is able to decide which action is best (usually the
 * action with the highest Q-Value). AbstractQFunction is the base class
 * of all Q-Functions. It just provides the interface for getting, setting and
 * updating (adding a value to the current value) Q-Values. These functions are
 * again getValue, setValue and updateValue, now there is always an action
 * object as additional parameter. The class maintains an action set of all
 * actions which are stored in the Q-Function. In addition the class provides
 * one function to retrieve the action with the best Q-Value (getMax), one
 * function for retrieving the value of these best action (getMaxValue) and
 * there is also a function which writes all Q-Values of a state (i.e. for all
 * actions) in a RealType array (getActionValues).
 *
 * The class also maintains a gamma factor which should be the same as the
 * gamma factor of its value functions. In addition the interface provides
 * functions for storing and loading the Values of the QFunction (storeValues
 * and loadValues).
 *
 * @see QFunction
 */
class AbstractQFunction: public ActionObject, virtual public LearnDataObject {
protected:
	int type;

public:
	bool mayDiverge;

public:
	int getType();
	bool isType(int type);
	void addType(int Type);

	virtual void saveData(std::ostream& file);
	virtual void loadData(std::istream& file);
	virtual void printValues() {}

	virtual void resetData() {}

	/**
	 * Writes the Q-Values of the specified actions in the actionValues array.
	 *
	 * So the size of the array has to be at least the size of the action set.
	 **/
	void getActionValues(StateCollection *state, ActionSet *actions,
	        RealType *actionValues, ActionDataSet *data = nullptr);

	/**
	 * Calculates the best action from a given action set.
	 *
	 * Returns the best action from the availableActions action set. If several
	 * actions have the same best Q-Value, the first action which has this
	 * value in the action set is chosen.
	 **/
	virtual Action* getMax(StateCollection *state,
	        ActionSet *availableActions, ActionDataSet *data = nullptr);

	/**
	 * Returns the best action value from a given action set.
	 *
	 * Returns the best action value from the availableActions action set.
	 **/
	virtual RealType getMaxValue(StateCollection *state,
	        ActionSet *availableActions);

	//! Returns the statistics for a given action.
	virtual void getStatistics(StateCollection *state, Action *action,
	        ActionSet *actions, ActionStatistics* statistics);

	//! Interface for updating a Q-Value.
	virtual void updateValue(StateCollection *, Action *, RealType,
	        ActionData * = nullptr) {}

	//! Interface for setting a Q-Value.
	virtual void setValue(StateCollection *state, Action *action,
	        RealType qValue, ActionData *data = nullptr);
	//! Interface for getting a Q-Value.
	virtual RealType getValue(StateCollection *state, Action *action,
	        ActionData *data = nullptr) = 0;

	virtual AbstractQETraces *getStandardETraces() {
		return nullptr;
	}

public:
	//! Creates a QFunction, handling Q-Values for all actions in the actionset.
	AbstractQFunction(ActionSet *actions);
	virtual ~AbstractQFunction();
};

class QFunctionSum: public AbstractQFunction {
protected:
	std::map<AbstractQFunction *, RealType> *qFunctions;

public:
	//! Interface for getting a Q-Value.
	virtual RealType getValue(StateCollection *state, Action *action,
	        ActionData *data = nullptr);

	virtual AbstractQETraces *getStandardETraces() {
		return nullptr;
	}

	RealType getQFunctionFactor(AbstractQFunction *qFunction);
	void setQFunctionFactor(AbstractQFunction *qFunction, RealType factor);

	void addQFunction(AbstractQFunction *qFunction, RealType factor);
	void removeQFunction(AbstractQFunction *qFunction);

	void normFactors(RealType factor);

public:
	QFunctionSum& operator=(const QFunctionSum&) = delete;
	QFunctionSum(const QFunctionSum&) = delete;

	QFunctionSum(ActionSet *actions);
	virtual ~QFunctionSum();
};

/**
 * This exception is thrown if a value function has become divergent.
 *
 * There can be many reasons why a value function can become divergent, for
 * example the learning rate is too high.
 **/
class DivergentQFunctionException: public systematic::TracedError {
public:
	std::string qFunctionName;
	AbstractQFunction* qFunction;
	State* state;
	RealType value;

public:
	DivergentQFunctionException& operator=(const DivergentQFunctionException&) = default;
	DivergentQFunctionException(const DivergentQFunctionException&) = default;

	DivergentQFunctionException(std::string qFunctionName,
	        AbstractQFunction *qFunction, State *state, RealType value);

	virtual ~DivergentQFunctionException() = default;
};

class GradientQFunction: public AbstractQFunction,
        virtual public GradientUpdateFunction {
protected:
	FeatureList *localGradientQFunctionFeatures;

public:
	virtual int getWeightsOffset(Action *) {
		return 0;
	}

	virtual void getGradient(StateCollection *state, Action *action,
	        ActionData *data, FeatureList *gradient) = 0;

	//! Interface for updating a Q-Value.
	virtual void updateValue(StateCollection *state, Action *action,
	        RealType td, ActionData *data = nullptr);

	virtual void resetData() {
		AbstractQFunction::resetData();
	}

	virtual void loadData(std::istream& stream) {
		GradientUpdateFunction::loadData(stream);
	}

	virtual void saveData(std::ostream& stream) {
		GradientUpdateFunction::saveData(stream);
	}

	virtual AbstractQETraces *getStandardETraces();

	virtual void copy(LearnDataObject *qFunction) {
		GradientUpdateFunction::copy(qFunction);
	}

public:
	GradientQFunction& operator=(const GradientQFunction&) = delete;
	GradientQFunction(const GradientQFunction&) = delete;

	GradientQFunction(ActionSet *actions);
	virtual ~GradientQFunction();
};

/**
 * @class QFunction
 * Compounded Q-Function consisting of V-Functions.
 *
 * For Q-Functions there is one value function for each action of the
 * Q-Function, so its obvious to compose Q-Functions of value Functions. RIL
 * toolbox gives you the possibility to do this. This has the advantage that
 * you can choose an own value functions for each action, so its possible to
 * create own discretizations or even other kinds of value functions for an
 * action. This possibility is only available for certain algorithm, e.g. the
 * model based prioritized sweeping algorithm expects an Q-Function consisting
 * of feature value functions with the same feature calculator (at least with
 * the same number of features). The composition of value function is modelled
 * by the class QFunction. The class maintains a list of value functions,
 * which has the same size as the action set of the Q-Function. You can assign
 * specific value functions to specific actions. This is done by setVFunction.
 *
 * If one of the functions for accessing the Q-Values is called (getValue,
 * setValue, updateValue) the composed Q-Functions refers the call to the value
 * function of the specified action. There are 2 subclasses of QFunction,
 * one for feature Q-Functions and one for Q-Tables.
 */
class QFunction: public GradientQFunction {
protected:
	/**
	 * The list of V-Functions.
	 *
	 * Has the same order as the actionset, so the first qFunction corresponds
	 * to the first action.
	 */
	std::map<Action *, AbstractVFunction *> *vFunctions;

protected:
	virtual int getWeightsOffset(Action *action);
	virtual void updateWeights(FeatureList *features);

public:
	/**
	 * Calls the updateValue Function of the specified value function.
	 *
	 * Updates the Value of the value function assigned to the given action.
	 */
	virtual void updateValue(StateCollection *state, Action *action,
	        RealType td, ActionData *data = nullptr);

	/**
	 * Sets the Value of the value function assigned to the given action.
	 *
	 * Calls the setValue Function of the specified value function.
	 */
	virtual void setValue(StateCollection *state, Action *action,
	        RealType qValue, ActionData *data = nullptr);

	/**
	 * Returns the Value of the value function assigned to the given action.
	 *
	 * Returns the value of  the getValue Function of the specified value
	 * function.
	 */
	virtual RealType getValue(StateCollection *state, Action *action,
	        ActionData *data = nullptr);

/// Updates the Value of the value function assigned to the given action
	/** Calls the updateValue Function of the specified value function. The given state must be the state
	 used by the value function (at least a state that can be used by the value function)
	 */
	virtual void updateValue(State *state, Action *action, RealType td,
	        ActionData *data = nullptr);

	/**
	 * Sets the Value of the value function assigned to the given action.
	 *
	 * Calls the setValue Function of the specified value function. The given
	 * state must be the state used by the value function (at least a state
	 * that can be used by the value function.
	 */
	virtual void setValue(State *state, Action *action, RealType qValue,
	        ActionData *data = nullptr);

	/**
	 * Returns the Value of the value function assigned to the given action.
	 *
	 * Returns the value of  the getValue Function of the specified value
	 * function. The given state must be the state used by the value function
	 * (at least a state that can be used by the value function.
	 */
	virtual RealType getValue(State *state, Action *action, ActionData *data =
	        nullptr);

	/**
	 * Saves the Value Functions.
	 *
	 * Calls the saveValues method of all Value Functions.
	 */
	virtual void saveData(std::ostream& file);

	/**
	 * Loads the Value Functions.
	 *
	 * Calls the loadValues method of all Value Functions. So the value
	 * Function list has already to be initialized and the V-Functions has to
	 * be in the same order as they were when the Q-Function was save.
	 **/
	virtual void loadData(std::istream& file);

	//! Calls saveValues with stdout as outputstream.
	virtual void printValues();

	//! Returns the value function assigned to the given action.
	AbstractVFunction *getVFunction(Action *action);

	/**
	 * Returns the index-th value function (so the value function assigned
	 * to the index-th action).
	 */
	AbstractVFunction *getVFunction(int index);

	/**
	 * Sets the Value-Function of the specified action.
	 *
	 * If bDeleteOld is true (default) the old Value Function is deleted.
	 */
	void setVFunction(Action *action, AbstractVFunction *vfunction,
	        bool bDeleteOld = true);

	/**
	 * Sets the Value-Function of the index-th action.
	 *
	 * If bDeleteOld is true (default) the old Value Function is deleted.
	 */
	void setVFunction(int index, AbstractVFunction *vfunction,
	        bool bDeleteOld = true);

	//! Returns the number of V-Functions, which is always the number of Actions.
	int getNumVFunctions();

	virtual AbstractQETraces *getStandardETraces();

	//virtual StateProperties *getGradientCalculator(Action *action);
	virtual void getGradient(StateCollection *state, Action *action,
	        ActionData *data, FeatureList *gradient);

	virtual int getNumWeights();

	virtual void getWeights(RealType *weights);
	virtual void setWeights(RealType *weights);

	virtual void resetData();
	virtual void copy(LearnDataObject *qFunction);

public:
	QFunction& operator=(const QFunction&) = delete;
	QFunction(const QFunction&) = delete;

	/**
	 * Creates a composed Q-Function for the given actions.
	 *
	 * The Value-Functions list is initialized with nullptr with the same size
	 * as the action set, so the V-Functions have to be set by the user with
	 * the function setVFunction.
	 **/
	QFunction(ActionSet *actions);
	virtual ~QFunction();
};

/**
 * @class QFunctionFromStochasticModel
 * Converts a VFunction and a Model to a Q-Function.
 *
 * The class calculates the Q-Value by combining the information from a model
 * and a feature V-Function. So the class obviously only provides the functions
 * for getting a Q-Value. The Q-Value of an action is calculated the following
 * way: Q(s,a)=sum_{s'} P(s'|s,a)*(R(s,a,s') + gamma * V(s')).
 *
 * This class is used for the policies if you only have a V-Function (e.g. model
 * based learning), since policies can only handle Q-Functions.
 */
class QFunctionFromStochasticModel: public AbstractQFunction,
        public StateObject {
protected:
	//! The given V-Function.
	FeatureVFunction *vfunction;
	//! The model.
	AbstractFeatureStochasticModel *model;
	//! Discretizer used by the V-Function.
	StateProperties *discretizer;
	//! Feature Reward Function for the learning problem.
	FeatureRewardFunction *rewardfunction;

	//! State buffer.
	State *discState;

public:
	//! Does nothing.
	virtual void updateValue(StateCollection *, Action *, RealType,
	        ActionData * = nullptr) {
	}

	//! Does nothing.
	virtual void setValue(StateCollection *, Action *, RealType, ActionData * =
	        nullptr) {
	}

	/**
	 * The getValue function for state collections.
	 *
	 * Calls the getValue function for the specific state (retrieved from
	 * the collection by the discretizer)
	 **/
	virtual RealType getValue(StateCollection *state, Action *action,
	        ActionData *data = nullptr);

	/**
	 * The getValue function for states.
	 *
	 * Decomposes the feature state in its discrete state variables and calls
	 * the getValue(int, Action *) function. The results are weighted by the
	 * feature factors and summed up. For discrete states obviously just the
	 * getValue(int, Action *) with the discrete state number is called.
	 */
	virtual RealType getValue(State *featState, Action *action,
	        ActionData *data = nullptr);

	/**
	 * Calculates the Action-Value for a specific discrete state number.
	 *
	 * The Q-Value of an action is calculated the following way:
	 * Q(s,a)= sum_{s'} P(s'|s,a)*(R(s,a,s') + gamma * V(s')).
	 */
	virtual RealType getValue(int feature, Action *action, ActionData *data =
	        nullptr);

	virtual AbstractQETraces *getStandardETraces() {
		return nullptr;
	}

public:
	QFunctionFromStochasticModel& operator=(const QFunctionFromStochasticModel&) = delete;
	QFunctionFromStochasticModel(const QFunctionFromStochasticModel&) = delete;

	/**
	 * Creates a new QFunction from VFunction object for the given V-Function
	 * and the given model, the discretizer is taken from the V-Function.
	 */
	QFunctionFromStochasticModel(FeatureVFunction *vfunction,
	        AbstractFeatureStochasticModel *model,
	        FeatureRewardFunction *rewardfunction);

	virtual ~QFunctionFromStochasticModel();
};

/**
 * @class FeatureQFunction
 * Composed feature Q-Function.
 *
 * The class FeatureQFunction is a composed Q-Function which consists of
 * feature value function with the same feature calculator (or the same
 * discretizer). Very often this is all you need for learning, and
 * FeatureQFunction objects are easier to create. Its also needed because
 * some learning algorithms expect the value functions to be all of the same
 * kind (prioritized sweeping).
 *
 * The class also provides methods for manipulating the features directly,
 * without accessing the value functions explicitly.
 *
 * For creation of an feature Q-Function object you only need a state
 * properties object of a feature calculator or discretizer.
 *
 * In addition you have the possibility to initialise you Q-Function with
 * the values from a V-Function combined with a theoretical model
 * (see QFunctionFromStochasticModel). The difference to
 * QFunctionFromStochasticModel is, that all Action-Values for all states get
 * calculated and stored in the value Function. With
 * QFunctionFromStochasticModel the value of the state gets calculated
 * directly by the model and V-Function. So you can convert a Value Function
 * to a Q-Function.
 */
class FeatureQFunction: public QFunction {
protected:
	//! Discretizer used for retrieving the state from the state collection.
	StateProperties* _discretizer;
	//! Number of features from the Value-Functions.
	unsigned int _features;

	std::list<FeatureVFunction*> _featureVFunctions;

protected:
	//! Initializes the V-Function list with FeatureVFunction objects.
	virtual void init();

	/**
	 * Initializes the V-Functions with the Values calculated by a V-Function,
	 * a theoretical model and a reward function.
	 *
	 * The action values are calculated for each action in each state by the
	 * function CDynmaicProgramming::getActionValue and then they are stored
	 * in the V-Functions.
	 */
	void initVFunctions(FeatureVFunction *vfunction,
	        AbstractFeatureStochasticModel *model,
	        FeatureRewardFunction *rewardFunction, RealType gamma);

public:
	std::list<FeatureVFunction*>& getVFunctionList() {
		return _featureVFunctions;
	}

	const std::list<FeatureVFunction*>& getVFunctionList() const {
		return _featureVFunctions;
	}

public:
	/**
	 * Calls updateValue from the specified V-Function.
	 *
	 * Allows direct feature manipulation without the need of state objects.
	 */
	void updateValue(Feature *state, Action *action, RealType td,
	        ActionData *data = nullptr);

	/**
	 * Calls setValue from the specified V-Function.
	 *
	 * Allows direct feature manipulation without the need of state objects.
	 */
	void setValue(int state, Action *action, RealType qValue, ActionData *data =
	        nullptr);

	/**
	 * Returns the Value of a feature for a specific action.
	 *
	 * Allows direct feature manipulation without the need of state objects.
	 */
	RealType getValue(int feature, Action *action, ActionData *data = nullptr);

	void setFeatureCalculator(StateModifier *discretizer);
	StateProperties *getFeatureCalculator();

	int getNumFeatures();

	/**
	 * Saves the Values of the actions for each state in a readable tabular form.
	 *
	 * Tool for debugging and tracing the learning results
	 **/
	void saveFeatureActionValueTable(std::ostream& stream);

	/**
	 * Saves the index of the best action for each state in a readable
	 * tabular form.
	 *
	 * Tool for debugging and tracing the learning results
	 **/
	void saveFeatureActionTable(std::ostream& stream);

public:
	FeatureQFunction& operator=(const FeatureQFunction&) = delete;
	FeatureQFunction(const FeatureQFunction&) = delete;

	//! Creates an Q-Function with the specified discretizer.
	FeatureQFunction(ActionSet *actions, StateProperties *discretizer);

	/**
	 * Initializes the Value Functions with the values coming from a V-Function
	 * combined with a model and a reward function.
	 *
	 * The actionset is taken from the model, the discretizer from the feature
	 * V-Function. The V-Functions are initialized by the Function
	 * initVFunctions.
	 */
	FeatureQFunction(FeatureVFunction *vfunction,
	        AbstractFeatureStochasticModel *model,
	        FeatureRewardFunction *rewardFunction, RealType gamma);

	virtual ~FeatureQFunction();
};

class ComposedQFunction: public GradientQFunction {
protected:
	std::list<AbstractQFunction *> *qFunctions;

protected:
	virtual int getWeightsOffset(Action *action);
	virtual void updateWeights(FeatureList *features);

public:
	virtual void saveData(std::ostream& file);
	virtual void loadData(std::istream& file);
	virtual void printValues();

	virtual void getStatistics(StateCollection *state, Action *action,
	        ActionSet *actions, ActionStatistics* statistics);

	//! Interface for updating a Q-Value.
	virtual void updateValue(StateCollection *state, Action *action,
	        RealType td, ActionData *data = nullptr);
	//! Interface for setting a Q-Value.
	virtual void setValue(StateCollection *state, Action *action,
	        RealType qValue, ActionData *data = nullptr);
	//! Interface for getting a Q-Value.
	virtual RealType getValue(StateCollection *state, Action *action,
	        ActionData *data = nullptr);

	void addQFunction(AbstractQFunction *qFunction);

	std::list<AbstractQFunction *> *getQFunctions();
	int getNumQFunctions();

	virtual AbstractQETraces *getStandardETraces();

	virtual void getGradient(StateCollection *state, Action *action,
	        ActionData *data, FeatureList *gradient);

	virtual int getNumWeights();
	virtual void getWeights(RealType *weights);
	virtual void setWeights(RealType *weights);

	virtual void resetData();

public:
	ComposedQFunction& operator=(const ComposedQFunction&) = delete;
	ComposedQFunction(const ComposedQFunction&) = delete;

	ComposedQFunction();
	virtual ~ComposedQFunction();
};

} //namespace rlearner

#endif //Rlearner_cqfunction_H_
