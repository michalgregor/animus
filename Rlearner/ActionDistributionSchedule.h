#ifndef Rlearner_ActionDistributionSchedule_H_
#define Rlearner_ActionDistributionSchedule_H_

#include <utility>
#include <vector>
#include "policies.h"

namespace rlearner {

/**
 * @class ActionDistributionSchedule
 * A distribution schedule: contains a list of distributions over which it
 * keeps cycling, using each for the specified number of time steps.
 */
class ActionDistributionSchedule: public ActionDistribution {
public:
	typedef std::vector<std::pair<shared_ptr<ActionDistribution>, unsigned int> > distro_vector;

private:
	//! The vector of distributions.
	distro_vector _distributions;
	//! Number of the current step.
	unsigned int _counter = 0;
	//! Index of the current distribution.
	unsigned int _currentDistribution = 0;

public:
	typedef distro_vector::iterator iterator;
	typedef distro_vector::const_iterator const_iterator;

	/**
	 * Adds the specified distribution into the schedule. The distribution
	 * is used for numSteps steps.
	 */
	void push_back(const shared_ptr<ActionDistribution>& distribution, unsigned int numSteps) {
		_distributions.push_back({distribution, numSteps});
	}

	void erase(iterator pos) {
		_distributions.erase(pos);
	}

	iterator begin() {
		return _distributions.begin();
	}

	const_iterator begin() const {
		return _distributions.begin();
	}

	iterator end() {
		return _distributions.end();
	}

	const_iterator end() const {
		return _distributions.end();
	}

	void clear() {
		_distributions.clear();
	}

	void swap(ActionDistributionSchedule& obj);

public:
	virtual void getDistribution(StateCollection* state, ActionSet* availableActions, RealType* values);

public:
	ActionDistributionSchedule& operator=(const ActionDistributionSchedule&) = delete;
	ActionDistributionSchedule(const ActionDistributionSchedule&) = delete;

	ActionDistributionSchedule(ActionDistributionSchedule&& obj):
		_distributions(std::move(obj._distributions)), _counter(obj._counter),
		_currentDistribution(obj._currentDistribution) {}

	ActionDistributionSchedule() = default;
	ActionDistributionSchedule(const distro_vector& distributions);
	ActionDistributionSchedule(distro_vector&& distributions);

	virtual ~ActionDistributionSchedule() = default;
};

} //namespace rlearner

#endif //Rlearner_ActionDistributionSchedule_H_
