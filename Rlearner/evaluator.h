// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cevaluator_H_
#define Rlearner_cevaluator_H_

#include "agentlistener.h"

namespace rlearner {

class StateList;
class TransitionFunctionEnvironment;
class Agent;
class AgentSimulator;
class SemiMDPSender;
class AgentController;
class ActionDataSet;
class AgentController;
class DeterministicController;

class Evaluator {
public:
	virtual ~Evaluator() {}
	virtual RealType evaluate() = 0;
};

class PolicyEvaluator: public SemiMDPRewardListener, public Evaluator {
protected:
	AgentSimulator* agentSimulator;
	AgentController* controller;
	DeterministicController* detController;

	RealType policyValue;
	int nEpisodes;
	int nStepsPerEpisode;

protected:
	virtual RealType getEpisodeValue() = 0;

public:
	virtual RealType evaluatePolicy();
	virtual RealType evaluate() {
		return evaluatePolicy();
	}

	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState) = 0;

	virtual void setStepsPerEpisode(int steps) {
		nStepsPerEpisode = steps;
	}

	virtual void setAgentController(AgentController *controller);
	virtual void setDeterministicController(DeterministicController *detController);

public:
	PolicyEvaluator(AgentSimulator *agentSimulator, RewardFunction *rewardFunction, int nEpisodes,
	        int nStepsPerEpisode);
	virtual ~PolicyEvaluator() {}
};

class AverageRewardCalculator: public PolicyEvaluator {
protected:
	int nSteps;
	RealType averageReward;
	RealType minReward;

	virtual RealType getEpisodeValue();

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);
	virtual void newEpisode();

public:
	AverageRewardCalculator(AgentSimulator* agentSimulator, RewardFunction *rewardFunction,
	        int nEpisodes, int nStepsPerEpisode, RealType minReward = -2.0);
	virtual ~AverageRewardCalculator() {}
};

class RewardPerEpisodeCalculator: public PolicyEvaluator {
protected:
	RealType reward;

protected:
	virtual RealType getEpisodeValue();

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);
	virtual void newEpisode();

public:
	RewardPerEpisodeCalculator(AgentSimulator* agentSimulator,
		RewardFunction *rewardFunction, int nEpisodes, int nStepsPerEpisode);
};

class ValueCalculator: public PolicyEvaluator {
protected:
	int nSteps;
	RealType value;

	virtual RealType getEpisodeValue();

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);
	virtual void newEpisode();

public:
	ValueCalculator(AgentSimulator* agentSimulator, RewardFunction *rewardFunction, int nEpisodes,
		int nStepsPerEpisode, RealType gamma);
};

class PolicySameStateEvaluator: public PolicyEvaluator {
protected:
	StateList *startStates;
	TransitionFunctionEnvironment *environment;
	SemiMDPSender *sender;

protected:
	virtual RealType getEpisodeValue() = 0;

public:
	void setSemiMDPSender(SemiMDPSender *l_sender) {
		sender = l_sender;
	}

	virtual RealType evaluatePolicy();

	virtual RealType getValueForState(State *state, int nSteps);
	virtual RealType getActionValueForState(State *state, Action *action,
	        int nSteps);

	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState) = 0;

	virtual StateList *getStartStates() {
		return startStates;
	}

	virtual void setStartStates(StateList *newList);
	void getNewStartStates(int numStartStates);

public:
	PolicySameStateEvaluator(AgentSimulator *agentSimulator, RewardFunction *rewardFunction,
	        TransitionFunctionEnvironment *environment, StateList *startStates,
	        int nStepsPerEpisode);
	PolicySameStateEvaluator(AgentSimulator *agentSimulator, RewardFunction *rewardFunction,
	        TransitionFunctionEnvironment *environment, int numStartStates,
	        int nStepsPerEpisode);
	virtual ~PolicySameStateEvaluator();
};

class AverageRewardSameStateCalculator: public PolicySameStateEvaluator {
protected:
	int nSteps;
	RealType averageReward;
	RealType minReward;

protected:
	virtual RealType getEpisodeValue();

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);
	virtual void newEpisode();

public:
	AverageRewardSameStateCalculator(AgentSimulator* agentSimulator,
		RewardFunction *rewardFunction,
		TransitionFunctionEnvironment *environment, StateList *startStates,
		int nStepsPerEpisode, RealType minReward = -2.0);
};

class ValueSameStateCalculator: public PolicySameStateEvaluator {
protected:
	int nSteps;
	RealType value;

protected:
	virtual RealType getEpisodeValue();

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);
	virtual void newEpisode();

public:
	ValueSameStateCalculator(AgentSimulator* agentSimulator, RewardFunction *rewardFunction,
		TransitionFunctionEnvironment *environment, StateList *startStates,
		int nStepsPerEpisode, RealType gamma);
};

class PolicyGreedynessEvaluator: public PolicyEvaluator {
protected:
	AgentController *greedyPolicy;
	ActionDataSet *actionDataSet;

	int nGreedyActions;

protected:
	virtual RealType getEpisodeValue();

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);
	virtual void newEpisode();

public:
	PolicyGreedynessEvaluator(AgentSimulator* agentSimulator, RewardFunction *reward,
	        int nEpisodes, int nStepsPerEpsiode,
	        AgentController *l_greedyPolicy);
	virtual ~PolicyGreedynessEvaluator();
};

} //namespace rlearner

#endif //Rlearner_cevaluator_H_
