// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "algebra.h"
#include "lstd.h"

#include "vfunction.h"
#include "vetraces.h"

#include "vfunction.h"
#include "stateproperties.h"
#include "statemodifier.h"
#include "state.h"
#include "statecollection.h"

#include "action.h"
#include "agentcontroller.h"
#include "qetraces.h"
#include "qfunction.h"
#include "rewardfunction.h"

namespace rlearner {

LSTDLambda::LSTDLambda(
    RewardFunction *rewardFunction, GradientUpdateFunction *l_vFunction,
    int l_nUpdatePerEpisode) :
	    SemiMDPRewardListener(rewardFunction),
	    LeastSquaresLearner(l_vFunction, l_vFunction->getNumWeights()) {
	//vFunction = l_vFunction;
	nEpisode = 1;
	nUpdateEpisode = l_nUpdatePerEpisode;

	//featureCalc = vFunction->getStateProperties();
	newStateGradient = new FeatureList();
	oldStateGradient = new FeatureList();
}

LSTDLambda::~LSTDLambda() {
	delete newStateGradient;
	delete oldStateGradient;
}

void LSTDLambda::nextStep(
    StateCollection *oldStateCol, Action *action, RealType reward,
    StateCollection *newStateCol) {
	//vETraces->updateETraces(action->getDuration());
	//vETraces->addETrace(oldStateCol);
	RealType gamma = getParameter("DiscountFactor");

	updateETraces(oldStateCol, action);

	oldStateGradient->clear();
	newStateGradient->clear();

	if(!newStateCol->getState()->isResetState()) {
		getNewGradient(newStateCol, newStateGradient);
		newStateGradient->multFactor(-gamma);
	}
	getOldGradient(oldStateCol, action, oldStateGradient);
	newStateGradient->add(oldStateGradient);

	FeatureList *eTraceList = getGradientETraces();

	FeatureList::iterator it = newStateGradient->begin();

	for(; it != newStateGradient->end(); it++) {
		FeatureList::iterator it2 = eTraceList->begin();
		for(; it2 != eTraceList->end(); it2++) {
			int featx = (*it)->featureIndex;
			int featy = (*it2)->featureIndex;

			A->operator()(featy, featx) = A->operator()(featy, featx)
			    + (*it)->factor * (*it2)->factor;
		}
	}

//	for (; it != stateDiffList->end(); it ++)
//	{
//		FeatureList::iterator it2 = eTraceList->begin();
//		for (int i = 0; i < oldState->getNumDiscreteStates(); i ++)
//		{
//			int featx = (*it)->featureIndex;
//			int featy = oldState->getDiscreteState(i);
//
//			A->operator()(featy, featx) = A->operator()(featy, featx) + (*it)->factor * oldState->getContinuousState(i);
//		}
//	}

	FeatureList::iterator it2 = eTraceList->begin();
	for(; it2 != eTraceList->end(); it2++) {
		int featy = (*it2)->featureIndex;
		b->operator[](featy) = b->operator[](featy) + (*it2)->factor * reward;
	}

	/*	for (int i = 0; i < oldState->getNumDiscreteStates(); i ++)
	 {
	 int featy = oldState->getDiscreteState(i);
	 b->operator[](featy) = b->operator[](featy) + oldState->getContinuousState(i) * reward;
	 }*/

}

void LSTDLambda::newEpisode() {
	if(nEpisode > 0 && nUpdateEpisode > 0 && nEpisode % nUpdateEpisode == 0) {
		RealType error = doOptimization();

		printf("Error in LSTD optimization: %f\n", error);
	}
	nEpisode++;

	resetETraces();
}

void LSTDLambda::resetData() {
	A->setZero();
	b->setZero();
	nEpisode = 1;
}

void LSTDLambda::loadData(std::istream& stream) {
	xscanf(stream, "LSTD Data\n");
	xscanf(stream, "A - Matrix\n");

	RealType dBuf;
	for(int i = 0; i < A->rows(); i++) {
		for(int j = 0; j < A->cols(); j++) {
			xscanf(stream, "%lf ", dBuf);
			A->operator()(i, j) = dBuf;
		}
		xscanf(stream, "\n");
	}

	xscanf(stream, "b - Vector\n");
	for(int j = 0; j < b->rows(); j++) {
		xscanf(stream, "%lf ", dBuf);
		b->operator[](j) = dBuf;
	}
	xscanf(stream, "\n");
}

void LSTDLambda::saveData(std::ostream& stream) {
	fmt::fprintf(stream, "LSTD Data\n");
	fmt::fprintf(stream, "A - Matrix\n");

	for(int i = 0; i < A->rows(); i++) {
		for(int j = 0; j < A->cols(); j++) {
			fmt::fprintf(stream, "%f ", A->operator()(i, j));
		}
		fmt::fprintf(stream, "\n");
	}

	fmt::fprintf(stream, "b - Vector\n");
	for(int j = 0; j < b->rows(); j++) {
		fmt::fprintf(stream, "%f ", b->operator[](j));
	}
	fmt::fprintf(stream, "\n");
}

VLSTDLambda::VLSTDLambda(
    RewardFunction *rewardFunction, FeatureVFunction *updateFunction,
    int nUpdatePerEpisode) :
	LSTDLambda(rewardFunction, updateFunction, nUpdatePerEpisode) {
	vFunction = updateFunction;

	vETraces = new FeatureVETraces(vFunction);

	addParameters(vETraces);

}

VLSTDLambda::~VLSTDLambda() {
	delete vETraces;
}

void VLSTDLambda::getOldGradient(
    StateCollection *stateCol, Action *, FeatureList *gradient) {
	vFunction->getGradient(stateCol, gradient);
}

void VLSTDLambda::getNewGradient(
    StateCollection *stateCol, FeatureList *gradient) {
	vFunction->getGradient(stateCol, gradient);
}

void VLSTDLambda::updateETraces(StateCollection *stateCol, Action *action) {
	vETraces->updateETraces(action->getDuration());
	vETraces->addETrace(stateCol);
}

FeatureList *VLSTDLambda::getGradientETraces() {
	return vETraces->getGradientETraces();
}

void VLSTDLambda::resetETraces() {
	vETraces->resetETraces();
}

QLSTDLambda::QLSTDLambda(
    RewardFunction *rewardFunction, FeatureQFunction *updateFunction,
    AgentController *l_policy, int nUpdatePerEpisode) :
	LSTDLambda(rewardFunction, updateFunction, nUpdatePerEpisode) {
	qFunction = updateFunction;
	policy = l_policy;

	actionDataSet = new ActionDataSet(policy->getActions());

	qETraces = new GradientQETraces(qFunction);

	addParameters(qETraces);

}

QLSTDLambda::~QLSTDLambda() {
	delete qETraces;
	delete actionDataSet;
}

void QLSTDLambda::getOldGradient(
    StateCollection *stateCol, Action *action, FeatureList *gradient) {
	qFunction->getGradient(stateCol, action, action->getActionData(), gradient);
}

void QLSTDLambda::getNewGradient(
    StateCollection *stateCol, FeatureList *gradient) {
	Action *action = policy->getNextAction(stateCol, actionDataSet);

	qFunction->getGradient(stateCol, action,
	    actionDataSet->getActionData(action), gradient);
}

void QLSTDLambda::updateETraces(StateCollection *stateCol, Action *action) {
	qETraces->updateETraces(action);
	qETraces->addETrace(stateCol, action);
}

FeatureList *QLSTDLambda::getGradientETraces() {
	return qETraces->getGradientETraces();
}

void QLSTDLambda::resetETraces() {
	qETraces->resetETraces();
}

} //namespace rlearner
