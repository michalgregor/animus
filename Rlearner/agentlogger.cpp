// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cstdlib>
#include <cassert>
#include <cstring>
#include <fstream>

#include "agentlogger.h"
#include "continuousactions.h"
#include "episode.h"
#include "history.h"
#include "rewardfunction.h"
#include "stateproperties.h"
#include "statecollection.h"
#include "action.h"
#include "ril_debug.h"

namespace rlearner {

AgentLogger::AgentLogger(
    StateProperties *properties, ActionSet *actions,
    const std::string& autoSavefile, int holdMemory
): 	StateModifiersObject(properties), EpisodeHistory(properties, actions),
	_filename(),_file(), _loadFileName(), _loadModifiers(),
	_holdMemory(holdMemory), _episodes(),
	_currentEpisode(new Episode(getStateProperties(), getActions()))
{
	setAutoSaveFile(autoSavefile);
}

AgentLogger::AgentLogger(
    const std::string& loadFileName, StateProperties* properties,
    ActionSet* actions, const std::set<StateModifierList*>& modifierLists
):
	StateModifiersObject(properties), EpisodeHistory(properties, actions),
	_filename(),_file(), _loadFileName(loadFileName), _loadModifiers(modifierLists),
	_holdMemory(-1), _episodes(),
	_currentEpisode(new Episode(getStateProperties(), getActions()))
{
	std::ifstream loadFile(_loadFileName);

	for(auto& list: _loadModifiers) {
		addModifierList(list);
	}

	loadData(loadFile, _loadModifiers);
}

AgentLogger::AgentLogger(StateProperties *properties, ActionSet *actions) :
	StateModifiersObject(properties), EpisodeHistory(properties, actions),
	_filename(),_file(), _loadFileName(), _loadModifiers(),
	_holdMemory(-1), _episodes(),
	_currentEpisode(new Episode(getStateProperties(), getActions())) {}

void AgentLogger::setAutoSaveFile(const std::string& filename) {
	_filename = filename;
	_file = make_unique<std::ofstream>(filename, std::ofstream::app);
}

AgentLogger::~AgentLogger() {
	for(auto& episode: _episodes) {
		delete episode;
	}
}

void AgentLogger::resetData() {
	for(auto& episode: _episodes) {
		delete episode;
	}

	_episodes.clear();

	if(_currentEpisode) {
		delete _currentEpisode;
		_currentEpisode = nullptr;
	}

	if(_loadFileName.size()) {
		std::ifstream loadFile(_loadFileName);
		loadData(loadFile, _loadModifiers);
	}
}

void AgentLogger::setLoadDataFile(
    const std::string& loadData,
    const std::set<StateModifierList*>& modifierLists
) {
	_loadFileName = loadData;
	_loadModifiers = modifierLists;
}

void AgentLogger::nextStep(
    StateCollection *oldState, Action *action, StateCollection *nextState
) {
	_currentEpisode->nextStep(oldState, action, nextState);
}

void AgentLogger::newEpisode() {
	if(_currentEpisode) {
		if(_currentEpisode->getNumSteps() > 0) {
			if(_file) {
				_currentEpisode->saveData(*_file);
				_file->flush();
			}

			_episodes.push_back(_currentEpisode);
			_currentEpisode = new Episode(getStateProperties(), getActions(),
			    getModifierLists());
		}
	} else {
		_currentEpisode = new Episode(getStateProperties(), getActions(),
			getModifierLists());
	}

	if(_holdMemory > 0 && (int) _episodes.size() > _holdMemory) {
		Episode* firstEpisode = *_episodes.begin();
		_episodes.pop_front();
		delete firstEpisode;
	}
}

void AgentLogger::saveData(std::ostream& stream) {
	int i = 0;
	for(auto it = _episodes.begin(); it != _episodes.end(); it++, i++) {
		fmt::fprintf(stream, "\nEpisode: %d\n", i);
		(*it)->saveData(stream);
	}
}

void AgentLogger::loadData(
	std::istream& stream, std::set<StateModifierList*>& modifierLists,
	int numEpisodes
) {
	int nEpisodes = numEpisodes;
	int buf;

	if(_holdMemory >= 0 && _holdMemory < numEpisodes) {
		_holdMemory = numEpisodes;
	}

	if(!modifierLists.size()) {
		modifierLists = getModifierLists();
	}

	while(stream.good() && (nEpisodes > 0 || nEpisodes < 0)) {
		Episode* tmp = new Episode(
			getStateProperties(), getActions(), modifierLists
		);

		xscanf(stream, "\nEpisode: %d\n", buf);
		tmp->loadData(stream);
		_episodes.push_back(tmp);

		nEpisodes--;
	}

	newEpisode();
}

void AgentLogger::loadData(std::istream& stream, int numEpisodes) {
	int nEpisodes = numEpisodes;
	int buf;

	if(_holdMemory >= 0 && _holdMemory < numEpisodes) {
		_holdMemory = numEpisodes;
	}

	while(stream.good() && (nEpisodes > 0 || nEpisodes < 0)) {
		Episode* tmp = new Episode(
			getStateProperties(), getActions(), getModifierLists()
		);

		xscanf(stream, "\nEpisode: %d\n", buf);
		tmp->loadData(stream);
		_episodes.push_back(tmp);

		nEpisodes--;
	}

	newEpisode();
}

void AgentLogger::loadData(std::istream& stream) {
	loadData(stream, -1);
}

int AgentLogger::getNumEpisodes() {
	return _episodes.size();
}

Episode* AgentLogger::getEpisode(int index) {
	assert(index < getNumEpisodes());
	auto it = _episodes.begin();
	std::advance(it, index);
	return *it;
}

void AgentLogger::addModifierList(StateModifierList* modifierList) {
	StateModifiersObject::addModifierList(modifierList);

	if(_currentEpisode) {
		_currentEpisode->addModifierList(modifierList);
	}
}

void AgentLogger::removeModifierList(StateModifierList* modifierList) {
	StateModifiersObject::removeModifierList(modifierList);

	if(_currentEpisode) {
		_currentEpisode->removeModifierList(modifierList);
	}
}

Episode* AgentLogger::getCurrentEpisode() {
	return _currentEpisode;
}

void AgentLogger::clearAutoSaveFile() {
	_file = make_unique<std::ofstream>(_filename);
}

EpisodeOutput::EpisodeOutput(
    StateProperties* featCalc, RewardFunction* rewardFunction,
    ActionSet* actions, const shared_ptr<std::ostream>& output
):
	SemiMDPRewardListener(rewardFunction), ActionObject(actions),
	StateObject(featCalc), _stream(output), _nEpisodes(0), _nSteps(0)
{}

EpisodeOutput::~EpisodeOutput() {}

void EpisodeOutput::nextStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState
) {
	ActionData *actionData = action->getActionData();

	fmt::fprintf(*_stream, "Episode: %d, Step %d: ", _nEpisodes, _nSteps);
	fmt::fprintf(*_stream, "old State: ");
	oldState->getState(properties)->save(*_stream);
	fmt::fprintf(*_stream, "\nAction %d ", actions->getIndex(action));
	if(actionData != nullptr) {
		actionData->save(*_stream);
	}
	fmt::fprintf(*_stream, " Reward %lf\n", reward);

	fmt::fprintf(*_stream, "newState: ");
	nextState->getState(properties)->save(*_stream);
	fmt::fprintf(*_stream, "\n");
	_nSteps++;
	_stream->flush();
}

void EpisodeOutput::newEpisode() {
	fmt::fprintf(*_stream, "Start Episode %d\n", _nEpisodes);
	_nEpisodes++;
	_nSteps = 0;
}

void EpisodeOutput::intermediateStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState
) {
	fmt::fprintf(*_stream, "Intermediate Step (%d)\n", _nEpisodes);
	nextStep(oldState, action, reward, nextState);
}

EpisodeMatlabOutput::EpisodeMatlabOutput(
    StateProperties *featCalc, RewardFunction *rewardFunction,
    ActionSet *actions, const shared_ptr<std::ostream>& output
): 	SemiMDPRewardListener(rewardFunction), ActionObject(actions),
	StateObject(featCalc), _stream(output), _nEpisodes(0), _nSteps(0) {}

EpisodeMatlabOutput::~EpisodeMatlabOutput() {}

void EpisodeMatlabOutput::nextStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState
) {
	ActionData *actionData = action->getActionData();

	fmt::fprintf(*_stream, "%d %d ", _nEpisodes, _nSteps);
	State *state = oldState->getState(properties);
	unsigned int i = 0;

	for(i = 0; i < properties->getNumContinuousStates(); i++) {
		fmt::fprintf(*_stream, "%lf ", state->getContinuousState(i));
	}

	for(i = 0; i < properties->getNumDiscreteStates(); i++) {
		fmt::fprintf(*_stream, "%d ", (int) state->getDiscreteState(i));
	}

	state = nextState->getState(properties);

	for(i = 0; i < properties->getNumContinuousStates(); i++) {
		fmt::fprintf(*_stream, "%lf ", state->getContinuousState(i));
	}

	for(i = 0; i < properties->getNumDiscreteStates(); i++) {
		fmt::fprintf(*_stream, "%d ", (int) state->getDiscreteState(i));
	}

	fmt::fprintf(*_stream, "%d ", actions->getIndex(action));

	if(actionData != nullptr) {
		ContinuousActionData *contData =
		    dynamic_cast<ContinuousActionData *>(actionData);
		for(int j = 0; j < contData->rows(); j++) {
			fmt::fprintf(*_stream, "%lf ", contData->operator[](j));
		}
	}

	fmt::fprintf(*_stream, "%lf ", reward);

	fmt::fprintf(*_stream, "\n");

	_nSteps++;
	_stream->flush();
}

void EpisodeMatlabOutput::newEpisode() {
	_nEpisodes++;
	_nSteps = 0;
}

void EpisodeMatlabOutput::setOutputFile(const shared_ptr<std::ostream>& stream) {
	_stream = stream;
}

EpisodeOutputStateChanged::EpisodeOutputStateChanged(
    StateProperties *featCalc, RewardFunction *rewardFunction,
    ActionSet *actions, const shared_ptr<std::ostream>& output
):
	EpisodeOutput(featCalc, rewardFunction, actions, output) {}

void EpisodeOutputStateChanged::nextStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState
) {
	State *old = oldState->getState(properties);
	State *next = nextState->getState(properties);

	if(!old->equals(next)) {
		EpisodeOutput::nextStep(oldState, action, reward, nextState);
	} else {
		_nSteps++;
	}
}

StateOutput::StateOutput(
	StateProperties *featCalc, const shared_ptr<std::ostream>& output
): StateObject(featCalc), _stream(output) {}

StateOutput::~StateOutput() {}

void StateOutput::nextStep(
    StateCollection *, Action *, StateCollection *nextState
) {
	unsigned int i = 0;

	State *state = nextState->getState(properties);

	for(i = 0; i < properties->getNumContinuousStates(); i++) {
		fmt::fprintf(*_stream, "%lf ", state->getContinuousState(i));
	}

	for(i = 0; i < properties->getNumDiscreteStates(); i++) {
		fmt::fprintf(*_stream, "%d ", (int) state->getDiscreteState(i));
	}

	fmt::fprintf(*_stream, "\n");
	_stream->flush();
}

ActionOutput::ActionOutput(
	ActionSet *actions, const shared_ptr<std::ostream>& output
): ActionObject(actions), _stream(output) {}

ActionOutput::~ActionOutput() {}

void ActionOutput::nextStep(
    StateCollection *, Action *action, StateCollection *
) {
	int index = actions->getIndex(action);

	fmt::fprintf(*_stream, "%d ", index);
	if(action->getActionData() != nullptr) {
		action->getActionData()->save(*_stream);
	}
	fmt::fprintf(*_stream, "\n");
}

} //namespace rlearner
