// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "montecarlo.h"
#include "agent.h"
#include "transitionfunction.h"
#include "rewardfunction.h"
#include "ril_debug.h"
#include "episodehistory.h"
#include "episode.h"
#include "rewardmodel.h"
#include "supervisedlearner.h"
#include "state.h"
#include "statecollection.h"
#include "stateproperties.h"
#include "episode.h"
#include "vfunction.h"
#include "AgentSimulator.h"

namespace rlearner {

MonteCarloError::MonteCarloError(
	AgentSimulator *l_agentSimulator, RewardFunction *reward, StateProperties *modelState,
    ActionSet *actions, const std::set<StateModifierList*>& modifierLists,
    int l_nEpisodes, int l_nStepsPerEpisode, RealType discountFactor
):
	agentSimulator(l_agentSimulator),
	episode(new Episode(modelState, actions, modifierLists, true)),
	rewardFunction(reward),
	oldState(new StateCollectionImpl(modelState, modifierLists)),
	newState(new StateCollectionImpl(modelState, modifierLists)),
	nEpisodes(l_nEpisodes),
	nStepsPerEpisode(l_nStepsPerEpisode),
	semiMDPSender(l_agentSimulator->getAgent().get()),
	rewardLogger(nullptr),
	episodeHistory(nullptr),
	useRewardEpisode(true),
	errorFunction(MC_MSE)
{
	addParameter("DiscountFactor", discountFactor);
}

MonteCarloError::~MonteCarloError() {
	delete episode;
	delete oldState;
	delete newState;
}

void MonteCarloError::setSemiMDPSender(SemiMDPSender *sender) {
	semiMDPSender = sender;
}

void MonteCarloError::setEpisodeHistory(
    EpisodeHistory *l_episodeHistory, RewardHistory *l_rewardLogger) {
	rewardLogger = l_rewardLogger;
	this->episodeHistory = l_episodeHistory;
}

RealType MonteCarloError::getMeanMonteCarloError(
    EpisodeHistory *episodeHistory, RewardHistory *rewardLogger) {
	RealType mean = 0;
	for(int i = 0; i < episodeHistory->getNumEpisodes(); i++) {
		if(rewardLogger) {
			mean += getMonteCarloError(episodeHistory->getEpisode(i),
			    rewardLogger->getEpisode(i));
		} else {
			mean += getMonteCarloError(episodeHistory->getEpisode(i), nullptr);
		}
	}

	return mean / episodeHistory->getNumSteps();
}

RealType MonteCarloError::getMonteCarloError(
    Episode *episode, RewardEpisode *rewardEpisode) {
	RealType error = 0;
	RealType cum_reward = 0;
	RealType gamma = getParameter("DiscountFactor");
	int step = episode->getNumSteps();
	episode->getStateCollection(step, oldState);

	if(!oldState->getState()->isResetState()) {
		cum_reward = getValue(oldState, nullptr);
	}

	for(step = step - 1; step >= 0; step--) {
		StateCollectionImpl *temp = oldState;
		oldState = newState;
		newState = temp;
		episode->getStateCollection(step, oldState);
		Action *action = episode->getAction(step);

		RealType l_reward = 0;

		if(rewardEpisode == nullptr) {
			l_reward = rewardFunction->getReward(oldState, action, newState);
		} else {
			l_reward = rewardEpisode->getReward(step);
		}

		cum_reward = l_reward + gamma * cum_reward;

		RealType value = getValue(oldState, action);

		//printf("MC-Error %d : %f %f %f %f\n", step, cum_reward, l_reward, value, fabs(cum_reward - getValue(oldState, action)));

		switch(errorFunction) {
		case MC_MAE:
			error = error + fabs(cum_reward - value);
		break;
		case MC_MSE:
			error = error + pow(cum_reward - value, 2.0);
		break;
		default:
			error = error + pow(cum_reward - value, 2.0);
		break;
		}
	}

	return error;
}

RealType MonteCarloError::evaluate() {
	if(episodeHistory != nullptr) {
		return -getMeanMonteCarloError(episodeHistory, rewardLogger);
	}

	semiMDPSender->addSemiMDPListener(episode);
	RewardEpisode *rewardEpisode = nullptr;

	if(useRewardEpisode) {
		rewardEpisode = new RewardEpisode(rewardFunction);
		semiMDPSender->addSemiMDPListener(rewardEpisode);
	}

	RealType error = 0;
	for(int i = 0; i < nEpisodes; i++) {
		agentSimulator->startNewEpisode();
		agentSimulator->doControllerEpisode(nStepsPerEpisode);
		error += getMonteCarloError(episode, rewardEpisode);
	}
	error = error / nEpisodes;

	semiMDPSender->removeSemiMDPListener(episode);

	if(useRewardEpisode) {
		semiMDPSender->removeSemiMDPListener(rewardEpisode);
		delete rewardEpisode;
	}

	return -error;
}

RealType MonteCarloVError::getValue(StateCollection *state, Action *) {
	return vFunction->getValue(state);
}

MonteCarloVError::MonteCarloVError(
    AbstractVFunction *vFunction, AgentSimulator* agentSimulator, RewardFunction *reward,
    StateProperties *modelState, ActionSet *actions,
    const std::set<StateModifierList*>& modifierLists, int numEpisodes, int numSteps,
    RealType discountFactor) :
	    MonteCarloError(agentSimulator, reward, modelState, actions, modifierLists,
	        numEpisodes, numSteps, discountFactor) {
	this->vFunction = vFunction;
}

MonteCarloVError::~MonteCarloVError() {}

RealType MonteCarloQError::getValue(StateCollection *state, Action *action) {
	if(action != nullptr) {
		return qFunction->getValue(state, action);
	} else {
		ActionSet availAbleActions;
		episode->getActions()->getAvailableActions(&availAbleActions, state);

		if(availAbleActions.size() > 0) {
			return qFunction->getMaxValue(state, &availAbleActions);
		} else {
			printf("MC Q-Error: No Actions available!!\n");
			return 0;
		}
	}
}

MonteCarloQError::MonteCarloQError(
    AbstractQFunction *l_qFunction, AgentSimulator* agentSimulator, RewardFunction *reward,
    StateProperties *modelState, ActionSet *actions,
    const std::set<StateModifierList*>& modifierLists, int numEpisodes, int numSteps,
    RealType discountFactor) :
	    MonteCarloError(agentSimulator, reward, modelState, actions, modifierLists,
	        numEpisodes, numSteps, discountFactor) {
	this->qFunction = l_qFunction;
}

MonteCarloQError::~MonteCarloQError() {}

MonteCarloSupervisedLearner::MonteCarloSupervisedLearner(
    EpisodeHistory *l_episodeHistory, RewardHistory *l_rewardLogger,
    BatchDataGenerator *l_dataGenerator) {
	episodeHistory = l_episodeHistory;
	rewardLogger = l_rewardLogger;
	dataGenerator = l_dataGenerator;

	addParameters(dataGenerator);
	addParameter("DiscountFactor", 0.95);
}

MonteCarloSupervisedLearner::~MonteCarloSupervisedLearner() {}

void MonteCarloSupervisedLearner::evaluatePolicy(int) {
	dataGenerator->resetPolicyEvaluation();

	ActionDataSet dataSet(episodeHistory->getActions());

	StateCollectionImpl *oldState = new StateCollectionImpl(
	    episodeHistory->getStateProperties(),
	    episodeHistory->getModifierLists());

	for(int i = 0; i < episodeHistory->getNumEpisodes(); i++) {
		RealType cum_reward = 0;
		RealType gamma = getParameter("DiscountFactor");

		Episode *episode = episodeHistory->getEpisode(i);
		RewardEpisode *rewardEpisode = rewardLogger->getEpisode(i);

		int step = episode->getNumSteps();

		episode->getStateCollection(step, oldState);

		if(!oldState->getState()->isResetState()) {
			cum_reward = dataGenerator->getValue(oldState, nullptr);
		}

		for(step = step - 1; step >= 0; step--) {
			episode->getStateCollection(step, oldState);
			Action *action = episode->getAction(step, &dataSet);

			RealType l_reward = 0;

			l_reward = rewardEpisode->getReward(step);

			cum_reward = l_reward + gamma * cum_reward;

			dataGenerator->addInput(oldState, action, cum_reward);
		}
	}

	delete oldState;
	dataGenerator->trainFA();
}

MonteCarloVLearner::MonteCarloVLearner(
    AbstractVFunction *l_vFunction, EpisodeHistory *episodeHistory,
    RewardHistory *rewardLogger, SupervisedLearner *l_learner) :
	    MonteCarloSupervisedLearner(episodeHistory, rewardLogger,
	        new BatchVDataGenerator(l_vFunction, l_learner)) {}

MonteCarloVLearner::~MonteCarloVLearner() {
	delete dataGenerator;
}

MonteCarloCAQLearner::MonteCarloCAQLearner(
    StateProperties *properties, ContinuousActionQFunction *l_qFunction,
    EpisodeHistory *episodeHistory, RewardHistory *rewardLogger,
    SupervisedLearner *learner) :
	    MonteCarloSupervisedLearner(episodeHistory, rewardLogger,
	        new BatchCAQDataGenerator(properties, l_qFunction, learner)) {}

MonteCarloCAQLearner::~MonteCarloCAQLearner() {
	delete dataGenerator;
}

MonteCarloQLearner::MonteCarloQLearner(
    QFunction *qFunction, EpisodeHistory *episodeHistory,
    RewardHistory *rewardLogger, SupervisedQFunctionLearner *learner) :
	    MonteCarloSupervisedLearner(episodeHistory, rewardLogger,
	        new BatchQDataGenerator(qFunction, learner)) {}

MonteCarloQLearner::~MonteCarloQLearner() {
	delete dataGenerator;
}

} //namespace rlearner
