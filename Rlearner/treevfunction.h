// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ctreevfunction_H_
#define Rlearner_ctreevfunction_H_

#include "trees.h"
#include "forest.h"
#include "inputdata.h"
#include "vfunction.h"
#include "continuousactions.h"
#include "statemodifier.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "action.h"

namespace rlearner {

class RegressionTreeFunction {
protected:
	Mapping<RealType> *tree;
	int numDim;

public:
	void setTree(Mapping<RealType> *tree);
	Mapping<RealType> *getTree();

	int getNumDimensions();
	virtual void getInputData(
	    StateCollection *state,
	    Action *action,
	    ColumnVector *data
	) = 0;

public:
	RegressionTreeFunction(Mapping<RealType> *tree, int numDim);
	virtual ~RegressionTreeFunction() {}
};

class RegressionTreeVFunction:
    public AbstractVFunction, public RegressionTreeFunction {
public:
	virtual RealType getValue(State *state);

	virtual void getInputData(
	    StateCollection *state,
	    Action *action,
	    ColumnVector *data
	);

	virtual void resetData();
	virtual void saveData(std::ostream& stream);

public:
	RegressionTreeVFunction(StateProperties *properties, Mapping<RealType> *tree);
	virtual ~RegressionTreeVFunction() {}
};

class RegressionTreeQFunction:
    public ContinuousActionQFunction, public StateObject,
    public RegressionTreeFunction {
protected:
	ColumnVector *buffVector;

public:
	virtual RealType getCAValue(
	    StateCollection *state,
	    ContinuousActionData *data
	);

	virtual void getInputData(
	    StateCollection *state,
	    Action *action,
	    ColumnVector *data
	);

	virtual void resetData();

public:
	RegressionTreeQFunction(
	    ContinuousAction *action,
	    StateProperties *properties,
	    Mapping<RealType> *tree
	);

	virtual ~RegressionTreeQFunction();
};

template<typename TreeData> class ForestFeatureCalculator:
    public FeatureCalculator {
protected:
	Forest<TreeData> *forest;
	Leaf<TreeData> **activeLeaves;

protected:
	RealType getLeafActivationFactor(
	    State *stateCol,
	    Leaf<TreeData> *targetState
	);

public:
	void getModifiedState(StateCollection *stateCol, State *targetState);
	void setForest(Forest<TreeData> *forest);

public:
	ForestFeatureCalculator(Forest<TreeData> *forest, int offsetNumLeaves = 0);
	ForestFeatureCalculator(int numFeatures, int numActiveFeatures);
	virtual ~ForestFeatureCalculator();
};

template<typename TreeData>
ForestFeatureCalculator<TreeData>::ForestFeatureCalculator(
    Forest<TreeData> *l_forest,
    int offsetNumTrees) :
	    FeatureCalculator(l_forest->getNumLeaves() + offsetNumTrees,
	        l_forest->getNumTrees()) {
	forest = l_forest;
	activeLeaves = new Leaf<TreeData>*[forest->getNumTrees()];
}

template<typename TreeData>
ForestFeatureCalculator<TreeData>::ForestFeatureCalculator(
    int numFeatures,
    int numActiveFeatures
):
	FeatureCalculator(numFeatures, numActiveFeatures)
{
	forest = nullptr;
	activeLeaves = new Leaf<TreeData>*[getNumActiveFeatures()];
}

template<typename TreeData>
ForestFeatureCalculator<TreeData>::~ForestFeatureCalculator() {
	delete[] activeLeaves;

	if(forest) {
		delete forest;
	}
}

template<typename TreeData>
void ForestFeatureCalculator<TreeData>::setForest(Forest<TreeData> *l_forest) {
	forest = l_forest;
}

template<typename TreeData>
void ForestFeatureCalculator<TreeData>::getModifiedState(
    StateCollection *stateCol,
    State *targetState
) {
	if(forest == nullptr) {
		targetState->resetState();
		targetState->setNumActiveContinuousStates(0);
		targetState->setNumActiveDiscreteStates(0);
	} else {
		State *state = stateCol->getState(originalState);

		forest->getActiveLeaves(state, activeLeaves);

		int leafSum = 0;

		targetState->setNumActiveContinuousStates(numActiveFeatures);
		targetState->setNumActiveDiscreteStates(numActiveFeatures);

		for(unsigned int i = 0; i < numActiveFeatures; i++) {
			targetState->setDiscreteState(i,
			    activeLeaves[i]->getLeafNumber() + leafSum);
			targetState->setContinuousState(i,
			    getLeafActivationFactor(state, activeLeaves[i]));

			leafSum += forest->getTree(i)->getNumLeaves();
		}
	}
}

template<typename TreeData>
RealType ForestFeatureCalculator<TreeData>::getLeafActivationFactor(
    State *,
    Leaf<TreeData> *
) {
	return 1.0 / numActiveFeatures;
}

class FeatureVRegressionTreeFunction: public FeatureVFunction {
public:
	void setForest(RegressionForest *regTree, FeatureCalculator *featCalc);
	virtual RealType getValue(State *state);
	virtual void copy(LearnDataObject *vFunction);

public:
	FeatureVRegressionTreeFunction(
	    RegressionForest *regTree,
	    FeatureCalculator *featCalc
	);

	FeatureVRegressionTreeFunction(int numFeatures);
};

} //namespace rlearner

#endif //Rlearner_ctreevfunction_H_
