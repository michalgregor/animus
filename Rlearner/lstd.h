// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_clstd_H_
#define Rlearner_clstd_H_

#include <cstdlib>
#include <cstdio>

#include "learndataobject.h"
#include "agentlistener.h"
#include "supervisedlearner.h"
#include "algebra.h"

namespace rlearner {

class FeatureVFunction;
class FeatureQFunction;
class StateProperties;
class FeatureVETraces;
class FeatureQETraces;
class FeatureList;

class GradientQETraces;
class AgentController;
class ActionDataSet;

class LSTDLambda:
	public SemiMDPRewardListener,
	public LearnDataObject,
	public LeastSquaresLearner
{
protected:
	//FeatureVFunction *vFunction;
	//FeatureVETraces *vETraces;
	FeatureList *oldStateGradient;
	FeatureList *newStateGradient;

	int nEpisode;

protected:
	virtual void getOldGradient(StateCollection *stateCol, Action *action,
	        FeatureList *gradient) = 0;
	virtual void getNewGradient(StateCollection *stateCol,
	        FeatureList *gradient) = 0;

	virtual void updateETraces(StateCollection *stateCol, Action *action) = 0;
	virtual FeatureList *getGradientETraces() = 0;
	virtual void resetETraces() = 0;

public:
	int nUpdateEpisode;

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *newState);
	virtual void newEpisode();

	virtual void resetData();
	virtual void loadData(std::istream& stream);
	virtual void saveData(std::ostream& stream);

public:
	LSTDLambda(
		RewardFunction *rewardFunction,
		GradientUpdateFunction *updateFunction,
		int nUpdatePerEpisode
	);

	virtual ~LSTDLambda();
};

class VLSTDLambda: public LSTDLambda {
protected:
	FeatureVFunction *vFunction;
	FeatureVETraces *vETraces;

protected:
	virtual void getOldGradient(StateCollection *stateCol, Action *action,
	        FeatureList *gradient);
	virtual void getNewGradient(StateCollection *stateCol,
	        FeatureList *gradient);

	virtual void updateETraces(StateCollection *stateCol, Action *action);
	virtual FeatureList *getGradientETraces();
	virtual void resetETraces();

public:
	VLSTDLambda(RewardFunction *rewardFunction,
	        FeatureVFunction *updateFunction, int nUpdatePerEpisode);
	virtual ~VLSTDLambda();
};

class QLSTDLambda: public LSTDLambda {
protected:
	FeatureQFunction *qFunction;
	GradientQETraces *qETraces;

	AgentController *policy;
	ActionDataSet *actionDataSet;

protected:
	virtual void getOldGradient(StateCollection *stateCol, Action *action,
	        FeatureList *gradient);
	virtual void getNewGradient(StateCollection *stateCol,
	        FeatureList *gradient);

	virtual void updateETraces(StateCollection *stateCol, Action *action);
	virtual FeatureList * getGradientETraces();
	virtual void resetETraces();

public:
	QLSTDLambda(RewardFunction *rewardFunction,
	        FeatureQFunction *updateFunction, AgentController *policy,
	        int nUpdatePerEpisode);
	virtual ~QLSTDLambda();
};

} //namespace rlearner

#endif //Rlearner_clstd_H_
