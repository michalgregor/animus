// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>

#include "system.h"
#include "algebra.h"
#include "ril_debug.h"
#include "utility.h"

#include "system.h"
#include <Systematic/math/Compare.h>

namespace rlearner {

int my_round(RealType value) {
	int intvalue = (int) floor(value);
	if (value - intvalue > 0.5) {
		intvalue++;
	}
	return intvalue;
}

RealType bounded_exp(RealType value) {
	if (value > MAX_EXP) {
		value = MAX_EXP;
	} else {
		if (value < MIN_EXP) {
			value = MIN_EXP;
		}
	}
	return exp(value);
}

void getPseudoInverse(Matrix *J, Matrix *pinv, RealType lambda) {
	bool transpose = false;
	AlgebraIndex n, m;
	n = m = 0;

	if (J->rows() < J->cols()) {
		n = J->rows();
		m = J->cols();

		transpose = true;
	} else {
		m = J->rows();
		n = J->cols();
	}

	Matrix U(m, n);
	Matrix V(n, n);
	ColumnVector D(n);

	if (transpose) {
		auto svd = J->transpose().jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
		D = svd.singularValues();
		U = svd.matrixU();
		V = svd.matrixV();
	} else {
		auto svd = J->jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
		D = svd.singularValues();
		U = svd.matrixU();
		V = svd.matrixV();
	}

	pinv->setZero();

	for (int i = 0; i < n; i++) {
		Matrix::Scalar s = D[i];

		if (transpose) {
			(*pinv) = (*pinv)
			        + s / (s * s + lambda * lambda) * U.col(i + 1)
			                * V.col(i + 1).transpose();
		} else {
			(*pinv) = (*pinv)
			        + s / (s * s + lambda * lambda) * V.col(i + 1)
			                * U.col(i + 1).transpose();
		}
	}
}

void Distributions::getGibbsDistribution(RealType beta, RealType *values,
        unsigned int numValues) {
	RealType sum = 0.0;
	unsigned int i;
	for (i = 0; i < numValues; i++) {
		values[i] = bounded_exp(beta * values[i]);
		sum += values[i];
	}
	for (i = 0; i < numValues; i++) {
		values[i] = values[i] / sum;
	}
}

void Distributions::getS1L0Distribution(RealType *values,
        unsigned int numValues) {
	RealType smallest, largest, sum;
	unsigned int i;

	//transform: smallest Value = 0, Valuesum = 1;
	smallest = sum = largest = values[0];
	for (i = 1; i < numValues; i++) {
		if (smallest > values[i]) smallest = values[i];
		if (largest < values[i]) largest = values[i];
		sum += values[i];
	}
	sum -= numValues * smallest;

	// alle Werte gleich ==> Werte so setzen, das alle Values das
	//Gewicht 1 / numValues bekommen)
	if(Compare(largest, smallest)) {
		sum = numValues;
		smallest = smallest - 1;
	}

	for (i = 0; i < numValues; i++) {
		values[i] = (values[i] - smallest) / sum;
	}
}

RealType Distributions::getNormalDistributionSample(RealType mu, RealType sigma) {
	auto rand = make_rand();

	RealType x1 = (RealType) (rand() + 1) / ((RealType) RAND_MAX + 1);
	RealType x2 = (RealType) rand() / (RealType) RAND_MAX;

	RealType z = sqrt(-2 * log(x1)) * cos(2 * Constant::pi * x2);

	return z * sqrt(sigma) + mu;
}

int Distributions::getSampledIndex(RealType *distribution, int numValues) {
	auto rand = make_rand();

	RealType z = (RealType) (rand()) / RAND_MAX;
	RealType sum = distribution[0];

	int index = 0;

	while (sum <= z && index < numValues - 1) {
		index++;
		sum += distribution[index];
	}
	return index;
}

FeatureMap::FeatureMap(RealType stdValue_):
	stdValue(stdValue_) {}

RealType FeatureMap::getValue(unsigned int featureIndex) {
	FeatureMap::iterator reward = find(featureIndex);

	if (reward != end()) {
		return (*reward).second;
	} else {
		return stdValue;
	}
}

} //namespace rlearner
