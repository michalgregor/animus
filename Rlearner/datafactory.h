// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cdatafactory_H_
#define Rlearner_cdatafactory_H_

#include "trees.h"
#include "algebra.h"

namespace rlearner {

class DataSet1D;
class DataSet;

class RegressionFactory: public TreeDataFactory<RealType> {
protected:
	DataSet1D *outputData;
	DataSet1D *weightData;

public:
	virtual RealType createTreeData(DataSubset *dataSubset, int numLeaves);

public:
	RegressionFactory(DataSet1D *outputData, DataSet1D *weightData);
	virtual ~RegressionFactory();
};

class SubsetFactory: public TreeDataFactory<DataSubset *> {
public:
	virtual DataSubset *createTreeData(DataSubset *dataSubset, int numLeaves);
	virtual void deleteData(DataSubset *dataSet);

public:
	virtual ~SubsetFactory() {}
};

class VectorQuantizationFactory: public TreeDataFactory<ColumnVector *> {
protected:
	DataSet *inputData;

public:
	virtual ColumnVector *createTreeData(DataSubset *dataSubset, int numLeaves);
	virtual void deleteData(ColumnVector *dataSet);

public:
	VectorQuantizationFactory(DataSet *inputData);
	virtual ~VectorQuantizationFactory();
};

class LeafIndexFactory: public TreeDataFactory<int> {
	virtual int createTreeData(DataSubset *dataSubset, int numLeaves);
};

} //namespace rlearner

#endif //Rlearner_cdatafactory_H_
