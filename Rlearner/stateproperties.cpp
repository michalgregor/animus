// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <limits>

#include <Systematic/math/Compare.h>
#include <boost/serialization/vector.hpp>

#include "stateproperties.h"
#include "ril_debug.h"

namespace rlearner {

void StateProperties::serialize(IArchive& ar, const unsigned int) {
	ar & _numContinuousStates;
	ar & _numDiscreteStates;
	ar & _type;
	ar & _discreteStateSize;
	ar & _minValues;
	ar & _maxValues;
	ar & _isPeriodic;
	ar & _bInit;
}

void StateProperties::serialize(OArchive& ar, const unsigned int) {
	ar & _numContinuousStates;
	ar & _numDiscreteStates;
	ar & _type;
	ar & _discreteStateSize;
	ar & _minValues;
	ar & _maxValues;
	ar & _isPeriodic;
	ar & _bInit;
}

int StateProperties::getType() {
	return _type;
}

void StateProperties::addType(int newType) {
	_type |= newType;
}

bool StateProperties::isType(int type) {
	return (_type & type) > 0;
}

void StateProperties::setMinValue(unsigned int dim, RealType value) {
	assert(dim < _numContinuousStates);
	_minValues[dim] = value;
}

RealType StateProperties::getMinValue(unsigned int dim) {
	assert(dim < _numContinuousStates);
	return _minValues[dim];
}

void StateProperties::setMaxValue(unsigned int dim, RealType value) {
	assert(dim < _numContinuousStates);
	_maxValues[dim] = value;
}

RealType StateProperties::getMaxValue(unsigned int dim) {
	assert(dim < _numContinuousStates);
	return _maxValues[dim];
}

unsigned int StateProperties::getNumContinuousStates() {
	return _numContinuousStates;
}

bool StateProperties::equals(StateProperties *object) {
	if(object->getNumContinuousStates() != getNumContinuousStates()) return false;

	for(unsigned int i = 0; i < getNumDiscreteStates(); i++) {
		if(getDiscreteStateSize(i) != object->getDiscreteStateSize(i)) return false;
	}

	for(unsigned int i = 0; i < getNumContinuousStates(); i++) {
		if(!systematic::CompareSimply(getMinValue(i), object->getMinValue(i))) return false;
		if(!systematic::CompareSimply(getMaxValue(i), object->getMaxValue(i))) return false;
	}

	return true;
}

unsigned int StateProperties::getNumDiscreteStates() {
	return _numDiscreteStates;
}

void StateProperties::setDiscreteStateSize(unsigned int dim,
        unsigned int size) {
	assert(dim < _numDiscreteStates);
	_discreteStateSize[dim] = size;
}

unsigned int StateProperties::getDiscreteStateSize(unsigned int dim) {
	assert(dim < _numDiscreteStates);
	return _discreteStateSize[dim];
}

unsigned int StateProperties::getDiscreteStateSize() {
	unsigned int dim = 1;

	for (unsigned int i = 0; i < getNumDiscreteStates(); i++) {
		dim *= getDiscreteStateSize(i);
	}
	return dim;
}

void StateProperties::setPeriodicity(unsigned int index, bool isPeriodic_) {
	assert(index < _numContinuousStates);
	_isPeriodic[index] = isPeriodic_;
}

bool StateProperties::getPeriodicity(unsigned int index) {
	assert(index < _numContinuousStates);
	return _isPeriodic[index];
}

RealType StateProperties::getMirroredStateValue(unsigned int index,
        RealType value) {
	RealType period = getMaxValue(index) - getMinValue(index);
	return value - floor((value - getMinValue(index)) / period) * period;
}

/**
 * Sets the number of continuous states.
 * @note Be careful with this - will not resize already created
 * State objects.
 */
void StateProperties::setNumContinuousStates(unsigned int num) {
	_numContinuousStates = num;
	_minValues.resize(_numContinuousStates, 0);
	_maxValues.resize(_numContinuousStates, 1.0);
	_isPeriodic.resize(_numContinuousStates, false);
}

/**
 * Sets the number of discrete states.
 * @note Be careful with this - will not resize already created
 * State objects.
 */
void StateProperties::setNumDiscreteStates(unsigned int num) {
	_numDiscreteStates = num;
	_discreteStateSize.resize(_numDiscreteStates, 0);
}

void StateProperties::initProperties(unsigned int continuousStates,
        unsigned int discreteStates, int type) {
	_numContinuousStates = continuousStates;
	_numDiscreteStates = discreteStates;

	_discreteStateSize.resize(_numDiscreteStates, 0);
	_minValues.resize(_numContinuousStates, -std::numeric_limits<RealType>::infinity());
	_maxValues.resize(_numContinuousStates, std::numeric_limits<RealType>::infinity());
	_isPeriodic.resize(_numContinuousStates, false);

	_type = type;
	_bInit = true;
}

StateProperties::StateProperties(unsigned int continuousStates,
        unsigned int discreteStates, int type) :
		_numContinuousStates(0), _numDiscreteStates(0), _type(0),
		_discreteStateSize(), _minValues(), _maxValues(),
		_isPeriodic(), _bInit(false)
{
	initProperties(continuousStates, discreteStates, type);
}

StateProperties::StateProperties(StateProperties *properties_) :
	_numContinuousStates(0), _numDiscreteStates(0), _type(0),
	_discreteStateSize(), _minValues(), _maxValues(),
	_isPeriodic(), _bInit(false)
{
	unsigned int i;

	initProperties(properties_->getNumContinuousStates(),
	        properties_->getNumDiscreteStates(), properties_->getType());

	for (i = 0; i < _numDiscreteStates; i++) {
		_discreteStateSize[i] = properties_->getDiscreteStateSize(i);
	}

	for (i = 0; i < _numContinuousStates; i++) {
		_minValues[i] = properties_->getMinValue(i);
		_maxValues[i] = properties_->getMaxValue(i);
		_isPeriodic[i] = false;
	}
}

StateProperties::StateProperties() :
	_numContinuousStates(0), _numDiscreteStates(0), _type(0),
	_discreteStateSize(), _minValues(), _maxValues(),
	_isPeriodic(), _bInit(false) {}

} //namespace rlearner
