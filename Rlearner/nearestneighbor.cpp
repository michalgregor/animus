// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <limits>
#include <cassert>
#include <cmath>

#include "nearestneighbor.h"

namespace rlearner {

KDRectangle::KDRectangle(int numDim) {
	minValues = new ColumnVector(numDim);
	maxValues = new ColumnVector(numDim);

	minValues->setConstant(-std::numeric_limits<RealType>::max());
	maxValues->setConstant(std::numeric_limits<RealType>::max());
}

KDRectangle::KDRectangle(KDRectangle &rectangle) {
	minValues = new ColumnVector(*rectangle.getMinVector());
	maxValues = new ColumnVector(*rectangle.getMaxVector());
}

KDRectangle::~KDRectangle() {
	delete minValues;
	delete maxValues;
}

ColumnVector *KDRectangle::getMinVector() {
	return minValues;
}

ColumnVector *KDRectangle::getMaxVector() {
	return maxValues;
}

bool KDRectangle::intersects(KDRectangle *rectangle) {
	for(int i = 0; i < minValues->rows(); i++) {
		if((rectangle->getMaxValue(i) < minValues->operator[](i))
		    || (rectangle->getMinValue(i) > maxValues->operator[](i))) {
			/*			printf("No Intersection %d:", i);
			 cout << getMinVector()->t() << "; " << getMaxVector()->t() << endl;

			 cout << rectangle->getMinVector()->t() << "; " << rectangle->getMaxVector()->t() << endl;
			 */

			return false;
		}
	}
	return true;
}

void KDRectangle::setMaxValue(int dim, RealType value) {
	maxValues->operator[](dim) = value;
}

void KDRectangle::setMinValue(int dim, RealType value) {
	minValues->operator[](dim) = value;
}

RealType KDRectangle::getMinValue(int dim) {
	return minValues->operator[](dim);
}

RealType KDRectangle::getMaxValue(int dim) {
	return maxValues->operator[](dim);
}

RealType KDRectangle::getDistanceToPoint(ColumnVector *point) {
	RealType distance = 0;
	for(int i = 0; i < point->rows(); i++) {
		if(point->operator[](i) < minValues->operator[](i)) {
			distance += pow(point->operator[](i) - minValues->operator[](i),
			    2.0);
//			printf("min : %d %f %f %f, ", i, point->operator[](i) - minValues->operator[](i), point->operator[](i), minValues->operator[](i));
		} else {
			if(point->operator[](i) > maxValues->operator[](i)) {
				distance += pow(point->operator[](i) - maxValues->operator[](i),
				    2.0);

//				printf("max : %d %f %f %f, ", i, point->operator[](i) - minValues->operator[](i), point->operator[](i), minValues->operator[](i));
			}
		}
	}
	return sqrt(distance);
}

void KNearestNeighbors::addDataElements(
    ColumnVector *point, Leaf<DataSubset *> *leaf, KDRectangle *) {
	DataSubset *subset = leaf->getTreeData();
	DataSubset::iterator it = subset->begin();
	for(; it != subset->end(); it++) {
		*buffVector = *point - (*(*inputSet)[*it]);

		addAndSortDataElements(*it, buffVector->norm());
	}
}

KNearestNeighbors::KNearestNeighbors(
    Tree<DataSubset *> *tree, DataSet *l_inputSet, int K) :
	KNearestNeighborsTreeData<int, DataSubset *>(tree, K) {
	inputSet = l_inputSet;
	buffVector = new ColumnVector(inputSet->getNumDimensions());
}

KNearestNeighbors::~KNearestNeighbors() {
	delete buffVector;
}

RangeSearch::RangeSearch(Tree<DataSubset *> *l_tree, DataSet *l_inputSet) {
	tree = l_tree;
	inputSet = l_inputSet;
	elementList = new DataSubset;
}

RangeSearch::~RangeSearch() {
	delete elementList;
}

void RangeSearch::getSamplesInRangeElements(
    KDRectangle *range, TreeElement<DataSubset *> *element,
    KDRectangle *rectangle) {
	if(element->isLeaf()) {
		Leaf<DataSubset *> *leaf = dynamic_cast<Leaf<DataSubset *> *>(element);
		addDataElements(range, leaf, rectangle);
	} else {
		Node<DataSubset *> *node = dynamic_cast<Node<DataSubset *> *>(element);
		SplittingCondition1D *split =
		    dynamic_cast<SplittingCondition1D *>(node->getSplittingCondition());

		RealType minValue = rectangle->getMinValue(split->getDimension());
		RealType maxValue = rectangle->getMaxValue(split->getDimension());

		if(split->getThreshold() < minValue || split->getThreshold() > maxValue) {
			printf("Split Not in Rectangle: %f: %f %f\n", split->getThreshold(),
			    minValue, maxValue);
			assert(false);
		}

		// set value for left branch
		rectangle->setMaxValue(split->getDimension(), split->getThreshold());

		bool leftIntersect = rectangle->intersects(range);

		if(leftIntersect) {
			getSamplesInRangeElements(range, node->getLeftElement(), rectangle);
		}

		rectangle->setMaxValue(split->getDimension(), maxValue);
		rectangle->setMinValue(split->getDimension(), split->getThreshold());

		bool rightIntersect = rectangle->intersects(range);

		if(rightIntersect) {
			getSamplesInRangeElements(range, node->getRightElement(),
			    rectangle);
		}

		//printf("LeftDist: %f, RightDist: %f...", leftDist, rightDist);	
		/*
		 for (int i = 0; i < K; i ++)
		 {
		 if (distList[i] < numeric_limits<RealType>::max())
		 {
		 printf("%f ", distList[i]);
		 }
		 }
		 printf("\n");*/

		rectangle->setMaxValue(split->getDimension(), maxValue);
		rectangle->setMinValue(split->getDimension(), minValue);
	}
}

void RangeSearch::addAndSortDataElements(int data) {

	elementList->insert(data);

	/*printf("Adding Element with distance %f\n", distance);
	 for (int i = 0; i < K; i ++)
	 {
	 if (distList[i] < numeric_limits<RealType>::max())
	 {
	 printf("%f ", distList[i]);
	 }
	 }
	 printf("\n");*/
}

void RangeSearch::getSamplesInRange(
    KDRectangle *range, DataSubset *l_elementList) {
	elementList->clear();

	KDRectangle rectangle(tree->getNumDimensions());

	KDRectangle pre_range(*range);

	if(tree->getPreprocessor()) {
		tree->getPreprocessor()->preprocessInput(pre_range.getMinVector(),
		    pre_range.getMinVector());
		tree->getPreprocessor()->preprocessInput(pre_range.getMaxVector(),
		    pre_range.getMaxVector());
	}

	/*	printf("Starting Range search for area:\n");
	 cout << pre_range.getMinVector()->t() << endl;
	 cout << pre_range.getMaxVector()->t() << endl;
	 */
	getSamplesInRangeElements(&pre_range, tree->getRoot(), &rectangle);

	l_elementList->clear();

	DataSubset::iterator it = elementList->begin();

//	printf("Points in range %d: \n", elementList->size());
	for(; it != elementList->end(); it++) {
		l_elementList->insert(*it);
//		cout << (*inputSet)[*it]->t() << endl;
	}

	/*	DataSet::iterator it2  = inputSet->begin();

	 int numElements = 0;
	 for (;it2 != inputSet->end(); it2 ++)
	 {
	 if (pre_range.getDistanceToPoint(*it2) == 0)
	 {
	 numElements ++;
	 }
	 }
	 printf("Real Points in Range: %d, %d\n", numElements, elementList->size());*/
}

void RangeSearch::addDataElements(
    KDRectangle *range, Leaf<DataSubset *> *leaf, KDRectangle *leafRectangle) {
	DataSubset *subset = leaf->getTreeData();
	DataSubset::iterator it = subset->begin();

	for(; it != subset->end(); it++) {
		RealType distance = range->getDistanceToPoint((*inputSet)[*it]);

		if(distance == 0) {
			addAndSortDataElements(*it);
		}
	}
}

} //namespace rlearner
