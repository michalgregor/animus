// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cstateproperties_H_
#define Rlearner_cstateproperties_H_

#define FEATURESTATE 1
#define DISCRETESTATE 2
#define STATEDERIVATIONX 4
#define FEATURESTATEDERIVATIONX 5

#include "system.h"
#include <vector>

namespace rlearner {

/**
 * @class StateProperties
 * Class defining the Properties of a State.
 *
 * The class contains all the Properties a single State can have. Each State
 * (actually even each StateObject) has a pointer to his state properties.
 * The class contains the number of discrete and continuous states-variables,
 * the discrete state size for each discrete state variable and the minimum
 * and maximum values for the continuous states.
 *
 * The member type describes the type of the state. There are three different
 * types by now:
 * - 0: "Normal" Modelstate: Can have an arbitrary number of discrete and
 *   continuous states, can't be used for the most qFunctions.
 * - DISCRETESTATE: Can only have one discrete state. These states are usually
 *   calculated by a AbstractStateDiscretizer, not by the model.
 * - FEATURESTATE: Has to have the same number of discrete and continuous
 *   states. The i-th continuous state corresponds to the i-th discrete state,
 *   the discrete states determine the feature index and the continuous states
 *   the factor of that feature. All discrete state sizes have to be the same,
 *   all factors have to sum up to one, their minimum value is 0.0 and their
 *   max value is 1.0.
 *
 * The state properties object are also very important for the
 * StateCollection class. Here the state properties object pointer
 * serves as Id to retrieve the state with the specific properties from
 * the statecollection.
 *
 * @see StateObject
 * @see State
 * @see StateCollection
**/
class StateProperties {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	//! Number of continuous states.
	unsigned int _numContinuousStates;
	//! Number of discrete states.
	unsigned int _numDiscreteStates;

	//! Type of the state.
	int _type;

	//! An array containing the discrete state sizes.
	std::vector<unsigned int> _discreteStateSize;
	//! A vector containing the minimum values of the continuous states.
	std::vector<RealType> _minValues;
	//! A vector containing the maximum values of the continuous states.
	std::vector<RealType> _maxValues;
	//! Stores whether the corresponding continuous states are periodic or not.
	std::vector<bool> _isPeriodic;

	bool _bInit;

	virtual void initProperties(unsigned int continuousStates, unsigned int discreteStates, int type = 0);

public:
	int getType();
	bool isType(int type);

	/**
	 * Adds a specific type to the type field bitmap.
	 *
	 * So the parameter should be a power of 2, because all bits in the "Type"
	 * parameter gets set with an OR mask to the internal type.
	**/
	void addType(int Type);

	//! Sets the discrete state size of the dim-th state.
	void setDiscreteStateSize(unsigned int dim, unsigned int size);
	//! Returns the discrete state size of the dim-th state.
	virtual unsigned int getDiscreteStateSize(unsigned int dim);

	//! Returns the number of continuous state variables.
	unsigned int getNumContinuousStates();
	//! Returns the number of discrete state variables.
	unsigned int getNumDiscreteStates();

	//! Returns the discrete state size of all discrete states together
	//! (i.e the product of all sizes).
	virtual unsigned int getDiscreteStateSize();

	//! Sets the min-value of the dim-th continuous state.
	void setMinValue(unsigned int dim, RealType value);
	//! Returns the min-value of the dim-th continuous state.
	RealType getMinValue(unsigned int dim);

	//! Sets the max-value of the dim-th continuous state.
	void setMaxValue(unsigned int dim, RealType value);
	//! Returns the max-value of the dim-th continuous state.
	RealType getMaxValue(unsigned int dim);

	void setPeriodicity(unsigned int index, bool isPeriodic);
	bool getPeriodicity(unsigned int index);

	RealType getMirroredStateValue(unsigned int index, RealType value);

	//! Compares two properties in all their attributes, even discrete state
	//! sizes min and max values.
	bool equals(StateProperties *object);

public:
	void setNumContinuousStates(unsigned int num);
	void setNumDiscreteStates(unsigned int num);

protected:
	StateProperties();

public:
	StateProperties& operator=(const StateProperties&) = delete;
	StateProperties(const StateProperties&) = delete;

	/**
	 * Creates a properties object with continuousStates continuous states and
	 * discreteStates discrete states.
	 *
	 * The discrete state sizes, minimum and maximum values cant't be given to
	 * the constructor, these values have to be set explicitly.
	 **/
	StateProperties(unsigned int continuousStates, unsigned int discreteStates, int type = 0);

	/**
	 * Creates a properties object with the same properties as the given
	 * properties object.
	 */
	StateProperties(StateProperties *properties);

	virtual ~StateProperties() = default;
};

} //namespace rlearner

#endif //Rlearner_cstateproperties_H_
