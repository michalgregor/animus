// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "continuousactiongradientpolicy.h"
#include "vfunction.h"
#include "linearfafeaturecalculator.h"
#include "torchvfunction.h"
#include "state.h"
#include "statecollection.h"
#include "stateproperties.h"
#include "action.h"
#include "continuousactions.h"
#include "ril_debug.h"

namespace rlearner {

ContinuousActionGradientPolicy::ContinuousActionGradientPolicy(
    ContinuousAction *contAction, StateProperties *modelState) :
	    ContinuousActionController(contAction),
	    GradientFunction(modelState->getNumContinuousStates(),
	        contAction->getNumDimensions()),
	    StateObject(modelState) {
	this->modelState = modelState;
}

ContinuousActionGradientPolicy::~ContinuousActionGradientPolicy() {
}

void ContinuousActionGradientPolicy::getFunctionValuePre(
    ColumnVector *input, ColumnVector *output) {
	State *state = new State(modelState);
	ContinuousActionData *data =
	    dynamic_cast<ContinuousActionData *>(contAction->getNewActionData());

	for(int i = 0; i < getNumInputs(); i++) {
		state->setContinuousState(i, input->operator[](i));
	}

	getNextContinuousAction(state, data);
	for(int i = 0; i < getNumOutputs(); i++) {
		output->operator[](i) = state->getContinuousState(i);
	}

	delete state;
	delete data;
}

void ContinuousActionGradientPolicy::getGradientPre(
    ColumnVector *input, ColumnVector *outputErrors,
    FeatureList *gradientFeatures) {
	FeatureList *featureList = new FeatureList();
	State *state = new State(modelState);
	for(int i = 0; i < getNumInputs(); i++) {
		state->setContinuousState(i, input->operator[](i));
	}

	for(int i = 0; i < getNumOutputs(); i++) {
		getGradient(state, i, featureList);
		gradientFeatures->add(featureList, outputErrors->operator[](i));
	}
	delete featureList;
	delete state;
}

ContinuousActionPolicyFromGradientFunction::ContinuousActionPolicyFromGradientFunction(
    ContinuousAction *contAction, GradientFunction *gradientFunction,
    StateProperties *modelState) :
	ContinuousActionGradientPolicy(contAction, modelState) {
	assert(
	    contAction->getNumDimensions()
	        == (unsigned int ) gradientFunction->getNumOutputs());
	this->gradientFunction = gradientFunction;

	outputError = new ColumnVector(gradientFunction->getNumOutputs());
}

ContinuousActionPolicyFromGradientFunction::~ContinuousActionPolicyFromGradientFunction() {
	delete outputError;
}

void ContinuousActionPolicyFromGradientFunction::updateWeights(
    FeatureList *dParams) {
	gradientFunction->updateGradient(dParams, 1.0);
}

void ContinuousActionPolicyFromGradientFunction::getNextContinuousAction(
    StateCollection *inputState, ContinuousActionData *action) {
	State *state = inputState->getState(modelState);

	gradientFunction->getFunctionValue(state, action);
}

int ContinuousActionPolicyFromGradientFunction::getNumWeights() {
	return gradientFunction->getNumWeights();
}

void ContinuousActionPolicyFromGradientFunction::getWeights(
    RealType *parameters) {
	gradientFunction->getWeights(parameters);
}

void ContinuousActionPolicyFromGradientFunction::setWeights(
    RealType *parameters) {
	gradientFunction->setWeights(parameters);
}

void ContinuousActionPolicyFromGradientFunction::getGradient(
    StateCollection *inputState, int outputDimension,
    FeatureList *gradientFeatures) {
	outputError->setZero();
	outputError->operator[](outputDimension) = 1.0;
	ColumnVector input(getNumInputs());

	State *state = inputState->getState(modelState);
	for(int i = 0; i < getNumInputs(); i++) {
		input.operator[](i) = state->getContinuousState(i);
	}

	gradientFunction->getGradient(&input, outputError, gradientFeatures);
}

void ContinuousActionPolicyFromGradientFunction::getInputDerivation(
    StateCollection *, Matrix *) {
	//gradientFunction->getInputDerivation(inputState->getState(modelState), targetVector);
}

void ContinuousActionPolicyFromGradientFunction::resetData() {
	gradientFunction->resetData();
}

ContinuousActionFeaturePolicy::ContinuousActionFeaturePolicy(
    ContinuousAction *contAction, StateProperties *modelState,
    std::list<FeatureCalculator *> *l_featureCalculators) :
	ContinuousActionGradientPolicy(contAction, modelState), _modifierList() {
	this->featureCalculators = new std::list<FeatureCalculator *>();

	localGradient = new FeatureList();

	numWeights = 0;

	std::list<FeatureCalculator*>::iterator it = l_featureCalculators->begin();

	for(it = l_featureCalculators->begin(); it != l_featureCalculators->end();
	    it++) {
		featureCalculators->push_back(*it);
		_modifierList.addStateModifier(*it);
	}

	featureFunctions = new std::list<FeatureVFunction *>();

	inputDerivationFunctions = new std::map<FeatureVFunction *,
	    VFunctionInputDerivationCalculator *>();

	for(it = featureCalculators->begin(); it != featureCalculators->end();
	    it++) {
		FeatureVFunction *featureFunction = new FeatureVFunction(*it);
		featureFunctions->push_back(featureFunction);
		(*inputDerivationFunctions)[featureFunction] =
		    new VFunctionNumericInputDerivationCalculator(modelState,
		        featureFunction, 0.005, {&_modifierList});
		numWeights += (*it)->getNumFeatures();
	}

	inputDerivation = new ColumnVector(modelState->getNumContinuousStates());
}

ContinuousActionFeaturePolicy::~ContinuousActionFeaturePolicy() {
	delete localGradient;

	std::list<FeatureVFunction *>::iterator it = featureFunctions->begin();

	for(; it != featureFunctions->end(); it++) {
		(*inputDerivationFunctions)[*it]->removeModifierList(&_modifierList);
		delete (*inputDerivationFunctions)[*it];
		delete (*it);
	}

	delete featureFunctions;
	delete featureCalculators;

	delete inputDerivation;
	delete inputDerivationFunctions;
}

void ContinuousActionFeaturePolicy::updateWeights(FeatureList *dParams) {
	unsigned int weightIndexStart = 0;
	unsigned int weightIndexStop = 0;

	std::list<FeatureVFunction *>::iterator it = featureFunctions->begin();

	for(; it != featureFunctions->end(); it++) {
		weightIndexStop += (*it)->getNumFeatures();
		FeatureList::iterator itFeat = dParams->begin();
		localGradient->clear();
		for(; itFeat != dParams->end(); itFeat++) {
			if((*itFeat)->featureIndex >= weightIndexStart
			    && (*itFeat)->featureIndex < weightIndexStop) {
				localGradient->update((*itFeat)->featureIndex,
				    (*itFeat)->factor);
			}
		}
		(*it)->updateFeatureList(localGradient, 1.0);
	}
}

void ContinuousActionFeaturePolicy::getNextContinuousAction(
    StateCollection *stateCol, ContinuousActionData *action) {
	std::list<FeatureVFunction *>::iterator itFunc = featureFunctions->begin();
	std::list<FeatureCalculator *>::iterator itCalc =
	    featureCalculators->begin();

	for(int i = 0; itFunc != featureFunctions->end(); itFunc++, itCalc++, i++) {
		action->operator[](i) = (*itFunc)->getValue(
		    stateCol->getState((*itFunc)->getStateProperties()));
	}
}

int ContinuousActionFeaturePolicy::getNumWeights() {
	return numWeights;
}

void ContinuousActionFeaturePolicy::getWeights(RealType *parameters) {
	std::list<FeatureVFunction *>::iterator it = featureFunctions->begin();

	int weightIndex = 0;
	for(; it != featureFunctions->end(); it++) {
		(*it)->getWeights(parameters + weightIndex);
		weightIndex += (*it)->getNumWeights();
	}
}

void ContinuousActionFeaturePolicy::setWeights(RealType *parameters) {
	std::list<FeatureVFunction *>::iterator it = featureFunctions->begin();

	int weightIndex = 0;
	for(; it != featureFunctions->end(); it++) {
		(*it)->setWeights(parameters + weightIndex);
		weightIndex += (*it)->getNumWeights();
	}
}

void ContinuousActionFeaturePolicy::getGradient(
    StateCollection *inputState, int outputDimension,
    FeatureList *gradientFeatures) {
	std::list<FeatureVFunction *>::iterator it = featureFunctions->begin();

	int weightIndex = 0;
	for(int i = 0; it != featureFunctions->end(); it++, i++) {
		if(outputDimension == i) {
			localGradient->clear();
			(*it)->getGradient(inputState, localGradient);
			localGradient->addIndexOffset(weightIndex);
			gradientFeatures->add(localGradient, 1.0);
			weightIndex += (*it)->getNumWeights();
		}
	}
}

void ContinuousActionFeaturePolicy::getInputDerivation(
    StateCollection *, Matrix *) {
	// NOT WORKING, not in use
	assert(false);
	std::list<FeatureVFunction *>::iterator it = featureFunctions->begin();

	for(unsigned int row = 0; it != featureFunctions->end(); it++, row++) {
//		(*inputDerivationFunctions)[(*it)]->getInputDerivation(inputState, inputDerivation);

//		for (unsigned int col = 0; col < inputDerivation->rows(); col ++)
//		{
//			targetVector->operator()(row, col) = inputDerivation->operator[](col);
//		}
	}
}

void ContinuousActionFeaturePolicy::resetData() {
	std::list<FeatureVFunction *>::iterator it = featureFunctions->begin();

	for(; it != featureFunctions->end(); it++) {
		(*it)->resetData();
	}
}

ContinuousActionSigmoidPolicy::ContinuousActionSigmoidPolicy(
    ContinuousActionGradientPolicy *policy,
    CAGradientPolicyInputDerivationCalculator *inputDerivation) :
	    ContinuousActionGradientPolicy(policy->getContinuousAction(),
	        policy->getStateProperties()) {
	this->policy = policy;
	this->inputDerivation = inputDerivation;

	contData = new ContinuousActionData(
	    policy->getContinuousActionProperties());

	randomControllerMode = RandomController_Intern;
}

ContinuousActionSigmoidPolicy::~ContinuousActionSigmoidPolicy() {
	delete contData;
}

void ContinuousActionSigmoidPolicy::updateWeights(FeatureList *dParams) {
	policy->updateGradient(dParams, 1.0);
}

int ContinuousActionSigmoidPolicy::getNumWeights() {
	return policy->getNumWeights();
}

void ContinuousActionSigmoidPolicy::getWeights(RealType *parameters) {
	policy->getWeights(parameters);
}

void ContinuousActionSigmoidPolicy::setWeights(RealType *parameters) {
	policy->setWeights(parameters);
}

void ContinuousActionSigmoidPolicy::resetData() {
	policy->resetData();
}

void ContinuousActionSigmoidPolicy::getNoise(
    StateCollection *state, ContinuousActionData *action,
    ContinuousActionData *l_noise) {
	if(randomControllerMode == RandomController_Intern) {
		ColumnVector tempVector(this->contAction->getNumDimensions());

		policy->getNextContinuousAction(state, l_noise);

		tempVector = *action;

		for(int i = 0; i < tempVector.rows(); i++) {
			RealType umax = getContinuousActionProperties()->getMaxActionValue(i);
			RealType umin = getContinuousActionProperties()->getMinActionValue(i);
			RealType width = umax - umin;

			RealType actionValue = tempVector.operator[](i);
			actionValue = (actionValue - umin) / (umax - umin);

			if(actionValue <= 0.0001) {
				actionValue = 0.0001;
			} else {
				if(actionValue >= 0.9999) {
					actionValue = 0.9999;
				}
			}

			actionValue = (-log(1 / actionValue - 1) + 2) * width / 4 + umin;

			tempVector.operator[](i) = actionValue;
		}

		*l_noise *= -1;
		*l_noise += tempVector;
	} else {
		ContinuousActionController::getNoise(state, action, l_noise);
	}
}

void ContinuousActionSigmoidPolicy::getNextContinuousAction(
    StateCollection *state, ContinuousActionData *action) {
	policy->getNextContinuousAction(state, action);

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "Sigmoid Policy, Action Values:");
		action->save(DebugGetFileHandle('p'));
		DebugPrint('p', "\n");
	}

	noise->initData(0.0);

	if(randomController
	    && this->randomControllerMode == RandomController_Intern) {
		randomController->getNextContinuousAction(state, noise);
	}

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "Sigmoid Policy, Noise Values:");
		noise->save(DebugGetFileHandle('p'));
		DebugPrint('p', "\n");
	}

	(*action) << *action + *noise;

	for(int i = 0; i < action->rows(); i++) {
		RealType min =
		    contAction->getContinuousActionProperties()->getMinActionValue(i);
		RealType width =
		    contAction->getContinuousActionProperties()->getMaxActionValue(i)
		        - min;

		action->operator[](i) = -2 + (action->operator[](i) - min) / width * 4;

		action->operator[](i) = min
		    + width * (1.0 / (1.0 + bounded_exp(-action->operator[](i))));
	}
}

void ContinuousActionSigmoidPolicy::getGradient(
    StateCollection *inputState, int outputDimension,
    FeatureList *gradientFeatures) {
	policy->getNextContinuousAction(inputState, contData);
	policy->getGradient(inputState, outputDimension, gradientFeatures);

	RealType min = contAction->getContinuousActionProperties()->getMinActionValue(
	    outputDimension);
	RealType width =
	    contAction->getContinuousActionProperties()->getMaxActionValue(
	        outputDimension) - min;

	DebugPrint('p', "Sigmoid Gradient Calculation: Action Value %f\n",
	    contData->getActionValue(outputDimension));
	contData->operator[](outputDimension) = -2
	    + (contData->operator[](outputDimension) - min) / width * 4;

	RealType dSig = 1 / pow(1 + bounded_exp(-contData->operator[](outputDimension)), 2)
	    * bounded_exp(-contData->operator[](outputDimension));

	if(fabs(dSig) > 10000000) {
		printf("Infintity gradient!! : %f, %f, %f,%f \n", dSig,
		    contData->operator[](outputDimension), min, width);
		assert(false);
	}

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "ContinuousActionPolicyGradient: ");
		gradientFeatures->save(DebugGetFileHandle('p'));
		DebugPrint('p', "\nSaturationFactor; %f\n", 4 * dSig);
	}

	gradientFeatures->multFactor(4 * dSig);
}

void ContinuousActionSigmoidPolicy::getInputDerivation(
    StateCollection *, Matrix *) {
	// Not used an not working

	assert(false);
	/*	policy->getNextContinuousAction(inputState, contData);
	 inputDerivation->getInputDerivation(inputState, targetVector);

	 if (DebugIsEnabled('p'))
	 {
	 DebugPrint('p', "Inner Policy Input Derivation: ");
	 //targetVector->saveASCII(DebugGetFileHandle('p'));
	 DebugPrint('p', "Action Values: ");
	 contData->saveASCII(DebugGetFileHandle('p'));

	 }

	 for (unsigned int i = 0; i < contData->rows(); i ++)
	 {
	 RealType min = contAction->getContinuousActionProperties()->getMinActionValue(i);
	 RealType width = contAction->getContinuousActionProperties()->getMaxActionValue(i) - min;
	 contData->setElement(i, - 2 + (contData->operator[](i) - min) / width * 4);
	 RealType dSig = 1 / pow(1 + bounded_exp(- contData->operator[](i)), 2) * bounded_exp(- contData->operator[](i));

	 if (DebugIsEnabled('p'))
	 {
	 DebugPrint('p', "SaturationFactor for dimension %d: %f (actionValue %f)\n", i, dSig);
	 }

	 for (unsigned int j = 0; j < targetVector->cols(); j++)
	 {
	 targetVector->setElement(i, j,4 * dSig * targetVector->operator()(i,j));
	 }
	 }*/
}

CAGradientPolicyNumericInputDerivationCalculator::CAGradientPolicyNumericInputDerivationCalculator(
    ContinuousActionGradientPolicy *policy, RealType stepSize,
    const std::set<StateModifierList*>& modifierLists
) {
	this->policy = policy;
	contDataPlus = new ContinuousActionData(
	    policy->getContinuousActionProperties());

	contDataMinus = new ContinuousActionData(
	    policy->getContinuousActionProperties());

	this->stateBuffer = new StateCollectionImpl(policy->getStateProperties(),
		modifierLists);

	addParameter("NumericInputDerivationStepSize", stepSize);
}

CAGradientPolicyNumericInputDerivationCalculator::~CAGradientPolicyNumericInputDerivationCalculator() {
	delete contDataPlus;
	delete contDataMinus;
	delete stateBuffer;
}

void CAGradientPolicyNumericInputDerivationCalculator::getInputDerivation(
    StateCollection *inputStateCol, Matrix *targetVector) {
	StateProperties *modelState = policy->getStateProperties();
	State *inputState = stateBuffer->getState(modelState);
	inputState->setState(inputStateCol->getState(modelState));

	RealType stepSize = getParameter("NumericInputDerivationStepSize");

	DebugPrint('p', "Calculating Numeric Policy Input Derivation\n");
	;
	for(unsigned int col = 0; col < modelState->getNumContinuousStates();
	    col++) {
		RealType stepSize_i = (modelState->getMaxValue(col)
		    - modelState->getMinValue(col)) * stepSize;
		inputState->setContinuousState(col,
		    inputState->getContinuousState(col) + stepSize_i);
		stateBuffer->newModelState();
		policy->getNextContinuousAction(stateBuffer, contDataPlus);

		if(DebugIsEnabled('p')) {
			DebugPrint('p', "State : ");
			inputState->save(DebugGetFileHandle('p'));

			DebugPrint('p', "Action : ");
			contDataPlus->save(DebugGetFileHandle('p'));
		}

		inputState->setContinuousState(col,
		    inputState->getContinuousState(col) - 2 * stepSize_i);
		stateBuffer->newModelState();
		policy->getNextContinuousAction(stateBuffer, contDataMinus);

		if(DebugIsEnabled('p')) {
			DebugPrint('p', "State : ");
			inputState->save(DebugGetFileHandle('p'));

			DebugPrint('p', "Action : ");
			contDataMinus->save(DebugGetFileHandle('p'));
		}

		inputState->setContinuousState(col,
		    inputState->getContinuousState(col) + stepSize_i);
		for(int row = 0; row < policy->getNumOutputs(); row++) {
			targetVector->operator()(row, col) = (contDataPlus->getActionValue(
			    row) - contDataMinus->getActionValue(row)) / (2 * stepSize_i);
		}
	}
}

} //namespace rlearner
