// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cpegasus_H_
#define Rlearner_cpegasus_H_

#include "parameters.h"
#include "policygradient.h"
#include "agentlistener.h"

namespace rlearner {

class ContinuousTimeAndActionTransitionFunction;
class State;
class ContinuousActionData;

class ContinuousActionGradientPolicy;
class StateList;
class TransitionFunctionEnvironment;
class PolicySameStateEvaluator;
class AgentSimulator;
class RewardFunction;
class CAGradientPolicyInputDerivationCalculator;
class FeatureList;
class StateReward;

class TransitionFunctionInputDerivationCalculator: virtual public ParameterObject {
protected:
	ContinuousTimeAndActionTransitionFunction *dynModel;
	State *nextState;
	ContinuousActionData *buffData;

public:
	virtual void getInputDerivation(State *currentState,
	        ContinuousActionData *data, Matrix *dModelInput) = 0;

public:
	TransitionFunctionInputDerivationCalculator(
		ContinuousTimeAndActionTransitionFunction *dynModel
	);

	virtual ~TransitionFunctionInputDerivationCalculator();
};

class TransitionFunctionNumericalInputDerivationCalculator:
	public TransitionFunctionInputDerivationCalculator
{
protected:
	State *buffState;
	State *nextState1;
	State *nextState2;

public:
	virtual void getInputDerivation(
		State *currentState,
	    ContinuousActionData *data,
	    Matrix *dModelInput
	);

public:
	TransitionFunctionNumericalInputDerivationCalculator(
	        ContinuousTimeAndActionTransitionFunction *dynModel,
	        RealType stepsize);
	virtual ~TransitionFunctionNumericalInputDerivationCalculator();
};

class PEGASUSPolicyGradientCalculator: public PolicyGradientCalculator {
protected:
	ContinuousActionGradientPolicy *policy;
	StateList *startStates;
	TransitionFunctionEnvironment *dynModel;
	PolicySameStateEvaluator *sameStateEvaluator;

public:
	virtual void getGradient(FeatureList *gradient);
	virtual void getPEGASUSGradient(FeatureList *gradient,
	        StateList *startStates) = 0;

	virtual StateList* getStartStates();
	virtual void setStartStates(StateList *startStates);
	virtual void setRandomStartStates();

public:
	PEGASUSPolicyGradientCalculator(
		AgentSimulator *agentSimulator, RewardFunction *reward,
	    ContinuousActionGradientPolicy *policy,
	    TransitionFunctionEnvironment *dynModel,
	    int numStartStates, int horizon, RealType gamma
	);

	virtual ~PEGASUSPolicyGradientCalculator();
};

class PEGASUSAnalyticalPolicyGradientCalculator:
	public PEGASUSPolicyGradientCalculator,
	public SemiMDPListener
{
protected:
	ColumnVector *dReward;
	Matrix *dPolicy;
	Matrix *dModelInput;
	std::list<FeatureList *> *stateGradient1;
	std::list<FeatureList *> *stateGradient2;
	std::list<FeatureList *> *dModelGradient;

	FeatureList *episodeGradient;
	StateReward *rewardFunction;
	TransitionFunctionInputDerivationCalculator *dynModeldInput;
	CAGradientPolicyInputDerivationCalculator *policydInput;

	int steps;
	AgentSimulator* agentSimulator;

protected:
	void multMatrixFeatureList(
		Matrix *matrix, FeatureList *features, int index,
		std::list<FeatureList *> *newFeatures
	);

public:
	virtual void getPEGASUSGradient(FeatureList *gradientFeatures,
	        StateList *startStates);
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *newState);
	virtual void newEpisode();

public:
	PEGASUSAnalyticalPolicyGradientCalculator(AgentSimulator* agentSimulator,
		ContinuousActionGradientPolicy *policy,
		CAGradientPolicyInputDerivationCalculator *policyInputDerivation,
		TransitionFunctionEnvironment *dynModel,
		TransitionFunctionInputDerivationCalculator *dynModeldInput,
		StateReward *reward, int numStartStates, int horizon, RealType gamma
	);

	virtual ~PEGASUSAnalyticalPolicyGradientCalculator();
};

class PEGASUSNumericPolicyGradientCalculator: public PEGASUSPolicyGradientCalculator {
protected:
	FeatureList *gradientFeatures;
	RealType *weights;
	RewardFunction *rewardFunction;
	AgentSimulator* agentSimulator;

public:
	virtual void getPEGASUSGradient(FeatureList *gradientFeatures,
	        StateList *startStates);

public:
	PEGASUSNumericPolicyGradientCalculator(AgentSimulator* agentSimulator,
	        ContinuousActionGradientPolicy *policy,
	        TransitionFunctionEnvironment *dynModel, RewardFunction *reward,
	        RealType stepSize, int startStates, int horizon, RealType gamma);
	virtual ~PEGASUSNumericPolicyGradientCalculator();
};

} //namespace rlearner

#endif //Rlearner_cpegasus_H_

