// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cstatemodifier_H_
#define Rlearner_cstatemodifier_H_

#include <list>

#include "algebra.h"
#include "system.h"

#include "stateproperties.h"
#include "StateModifierList.h"

namespace rlearner {

class StateCollection;
class StateCollectionImpl;
class State;

class DataSet;
class DataSet1D;
class FeatureVFunction;

/**
 * @class StateModifier
 * Abstract class for calculating modified states from the original Model state.
 *
 * State modifier take the original model state or any other modified state
 * and transform the state in a new state. The new state can be any type of
 * state. Feature states are created by all sub classes of FeatureCalculator
 * and discrete states are calculated by all subclasses of
 * AbstractStateDiscretizer. The only function of StateModifier is
 * getModifiedState(StateCollection *originalState, State *targetState).
 * All a state modifier has to do is to take the information of the actual
 * state from the "originalState" collection and writes the new, modified state
 * into the target state. To use an instantiated state modifier you have to add
 * the modifier to the state modifier list of the agent. The agent again adds
 * the modifier to its state collections, so the modified state gets available
 * to all listeners.
 *
 * StateModifier is also subclass of StateProperties. This has basically 2
 * reasons. First of all this is an easy way to specify the properties of the
 * modified states calculated by this modifier, and the other important reason
 * is that the modifier itself serves as the id for a state
 * in a state collection.
 */
class StateModifier: public StateProperties {
protected:
	bool changeState;
	std::list<StateCollectionImpl *> *stateCollections;

protected:
	virtual void registerStateCollection(StateCollectionImpl *stateCollection);
	virtual void removeStateCollection(StateCollectionImpl *stateCollection);

	virtual void stateChanged();

	friend class StateCollectionImpl;

protected:
	StateModifier();

public:
	//! Virtual function for calculating the modified state from the original
	//! state (usually the model state).
	virtual void getModifiedState(StateCollection *originalState,
	        State *modifiedState) = 0;

public:
	StateModifier& operator=(const StateModifier&) = delete;
	StateModifier(const StateModifier&) = delete;

	//! Constructor for state modifiers, sets the number of continuous and
	//! discrete states of the resulting modified state objects.
	StateModifier(unsigned int numContinuousStates,
	        unsigned int numDiscreteStates);

	virtual ~StateModifier();
};

/**
 * @class NeuralNetworkStateModifier
 * State Modifier used for Neural Networks input states.
 *
 * This state modifier does the input data preprocessing for neural networks.
 * For Non-Periodic continuous state variables, the current state value gets
 * scaled into the interval [-1,1], periodic state variables get a special
 * treatment. In order to model the periodicity more accurate, the periodic
 * state variable gets split into two state variables, one representing the
 * sine and one the cosine of the normalized periodic state. So a neural
 * network state has the number of periodic state variables more state
 * variables than the original state. The discrete state variables remain
 * the same. The original state can be set in the constructor and is usually
 * the model state.
 */
class NeuralNetworkStateModifier: public StateModifier {
protected:
	StateProperties *originalState;
	unsigned int *dimensions;
	unsigned int numDim;

	ColumnVector *input_mean;
	ColumnVector *input_std;

	ColumnVector *buffVector;

	virtual void preprocessInput(ColumnVector *input, ColumnVector *norm_input);

	//! Returns the number of continuous states for the NN-state.
	static int getNumInitContinuousStates(StateProperties *properties,
	        unsigned int *dimensions, unsigned int numDim);

public:
	bool normValues;

	/**
	 * Data preprocessing for NN's.
	 *
	 * For Non-Periodic continuous state variables, the current state value
	 * gets scaled into the interval [-1,1], periodic state variables get
	 * a special treatment. In order to model the periodicity more accurate,
	 * the periodic state variable gets split into two state variables, one
	 * representing the sine and one the cosine of the normalized periodic
	 * state. So a neural network state has the number of periodic state
	 * variables more state variables than the original state. The discrete
	 * state variables remain the same. The original state can be set in the
	 * constructor and is usually the model state.
	 */
	virtual void getModifiedState(StateCollection *originalState,
	        State *modifiedState);

	void setPreprocessing(ColumnVector *input_mean, ColumnVector *input_std);

public:
	NeuralNetworkStateModifier& operator=(const NeuralNetworkStateModifier&) = delete;
	NeuralNetworkStateModifier(const NeuralNetworkStateModifier&) = delete;

	NeuralNetworkStateModifier(StateProperties *originalState);
	NeuralNetworkStateModifier(StateProperties *originalState,
	        unsigned int *dimensions, unsigned int numDim);
	virtual ~NeuralNetworkStateModifier();
};

/**
 * @class FeatureCalculator
 * Base class for all Feature Calculators.
 *
 * Features always have a unique feature index and a feature activation factor,
 * they are used for linear approximators. Feature calculators determine the
 * feature activation factors of a feature state. A feature state consists of
 * N discrete and continuous state variables, each pair represents a feature.
 * All features which are not listed in a feature state have an activation
 * factor of 0.0, so N is the maximum number of active features in a feature
 * state.
 *
 * The FeatureCalculator class is the interface of all other feature
 * calculators, it sets the state properties (numActiveFeatures continuous +
 * discrete states, min = 0, max = 1.0 of all continuous states, and
 * numFeatures is the discrete state size of all discrete state variables).
 *
 * It also provides functions for setting the original state. The original
 * state is used by all feature calculators as the "working" state. This is
 * per default the model state, but can be set differently if needed.
 *
 * The feature calculation itself is implemented by the subclasses in the
 * function getModifiedState.
 */
class FeatureCalculator: public StateModifier {
protected:
	unsigned int numFeatures;
	unsigned int numActiveFeatures;

	StateProperties *originalState;

protected:
	/**
	 * Normalizes all active features of the given feature state.
	 *
	 * Normalizes the feature factors so that the sum of all factors equals 1.0.
	 */
	void normalizeFeatures(State *featState);

	//! Sets the state properties.
	void initFeatureCalculator(unsigned int numFeatures,
	        unsigned int numActiveFeatures);

public:
	virtual unsigned int getNumFeatures();
	virtual unsigned int getNumActiveFeatures();

	virtual unsigned int getDiscreteStateSize(unsigned int i);
	virtual unsigned int getDiscreteStateSize();

	virtual RealType getMin(unsigned int i);
	virtual RealType getMax(unsigned int i);

	/**
	 * Sets the original state.
	 *
	 * The original state is used by all feature calculators as the "working"
	 * state. This is per default the model state, but can be set differently
	 * if needed.
	 */
	StateProperties *getOriginalState() {
		return originalState;
	}

	void setOriginalState(StateProperties *originalState_) {
		originalState = originalState_;
	}

protected:
	FeatureCalculator();

public:
	FeatureCalculator& operator=(const FeatureCalculator&) = delete;
	FeatureCalculator(const FeatureCalculator&) = delete;

	FeatureCalculator(unsigned int numFeatures,
	        unsigned int numActiveFeatures);
	virtual ~FeatureCalculator() {}
};

class NewFeatureCalculator {
public:
	virtual FeatureCalculator * getFeatureCalculator(
	        FeatureVFunction *vFunction, DataSet *inputData,
	        DataSet1D *outputData) = 0;

public:
	virtual ~NewFeatureCalculator() {}
};

/**
 * An interface for all state modifiers which have access to several other
 * state modifiers for state calculation.
 *
 * Base class for feature operators (FeatureOperatorOr, FeatureOperatorAnd)
 * and discrete state operators (DiscreteStateOperatorAnd).
 */
class StateMultiModifier {
protected:
	std::list<State*> _states;
	StateModifierList _modifierList;

public:
	//! Add a state Modifier to the StateCollections.
	void addStateModifier(StateModifier* featCalc);

	StateModifierList* getModifierList() {return &_modifierList;}

public:
	StateMultiModifier& operator=(const StateMultiModifier&) = delete;
	StateMultiModifier(const StateMultiModifier&) = delete;

	StateMultiModifier();
	virtual ~StateMultiModifier();
};

class StateVariablesChooser: public StateModifier {
protected:
	unsigned int *contStatesInd;
	unsigned int *discStatesInd;

	StateProperties *originalState;

public:
	virtual void getModifiedState(StateCollection *originalState,
	        State *modifiedState);

public:
	StateVariablesChooser& operator=(const StateVariablesChooser&) = delete;
	StateVariablesChooser(const StateVariablesChooser&) = delete;

	StateVariablesChooser(unsigned int numContStates,
	        unsigned int *contStatesInd, unsigned int numDiscStates,
	        unsigned int *discStatesInd,
	        StateProperties *originalState = nullptr);

	virtual ~StateVariablesChooser();
};

} //namespace rlearner

#endif //Rlearner_cstatemodifier_H_
