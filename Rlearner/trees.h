// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ctrees_H_
#define Rlearner_ctrees_H_

#include <list>
#include <vector>
#include <cstdio>

#include "algebra.h"
#include "inputdata.h"

namespace rlearner {

class SplittingCondition {
public:
	virtual bool isLeftNode(ColumnVector *input) = 0;

public:
	virtual ~SplittingCondition() {}
};

class SplittingCondition1D: public SplittingCondition {
protected:
	int dimension;
	RealType treshold;

public:
	virtual bool isLeftNode(ColumnVector *input);

	int getDimension();
	RealType getThreshold();

public:
	SplittingCondition1D(int dimension, RealType treshold);
	virtual ~SplittingCondition1D();
};

class SplittingConditionFactory {
public:
	virtual SplittingCondition *createSplittingCondition(
	        DataSubset *dataSubset) = 0;

	virtual bool isLeaf(DataSubset *dataSubset) = 0;

public:
	virtual ~SplittingConditionFactory() {}
};

template<typename TreeData> class TreeDataFactory {
public:
	virtual TreeData createTreeData(DataSubset *dataSubset, int numLeaves) = 0;
	virtual void deleteData(TreeData) {}

public:
	virtual ~TreeDataFactory() {}
};

template<typename TreeData> class Leaf;

template<typename TreeData> class TreeElement {
protected:
	TreeElement *parent;

public:
	virtual Leaf<TreeData> *getLeaf(ColumnVector *input) = 0;

	TreeElement<TreeData> *getParent() {
		return parent;
	}

	virtual int getDepth() {
		return 0;
	}

	virtual bool isLeaf() {
		return false;
	}

public:
	TreeElement& operator=(const TreeElement&) = delete;
	TreeElement(const TreeElement&) = delete;

	TreeElement(TreeElement<TreeData> *l_parent) {
		parent = l_parent;
	}

	virtual ~TreeElement() {}
};

template<typename TreeData> class Node: public TreeElement<TreeData> {
protected:
	SplittingCondition *split;
	TreeElement<TreeData> *leftElement;
	TreeElement<TreeData> *rightElement;

public:
	virtual Leaf<TreeData> *getLeaf(ColumnVector *input);
	virtual int getDepth();

	TreeElement<TreeData> *getLeftElement() {
		return leftElement;
	}

	TreeElement<TreeData> *getRightElement() {
		return rightElement;
	}

	void setLeftElement(TreeElement<TreeData> *l_leftElement) {
		leftElement = l_leftElement;
	}

	void setRightElement(TreeElement<TreeData> *l_rightElement) {
		rightElement = l_rightElement;
	}

	SplittingCondition *getSplittingCondition();

public:
	Node& operator=(const Node&) = delete;
	Node(const Node&) = delete;

	Node(TreeElement<TreeData> *parent, SplittingCondition *l_condition,
	        TreeElement<TreeData> *l_leftElement,
	        TreeElement<TreeData> *l_rightElement);

	virtual ~Node();
};

template<typename TreeData>
Node<TreeData>::Node(
	TreeElement<TreeData> *parent, SplittingCondition *l_condition,
	TreeElement<TreeData> *l_leftElement,
	TreeElement<TreeData> *l_rightElement
):
	TreeElement<TreeData>(parent), split(l_condition),
	leftElement(l_leftElement), rightElement(l_rightElement) {}

template<typename TreeData>
Node<TreeData>::~Node() {
	delete split;
	delete rightElement;
	delete leftElement;
}

template<typename TreeData>
SplittingCondition *Node<TreeData>::getSplittingCondition() {
	return split;
}

template<typename TreeData>
Leaf<TreeData> *Node<TreeData>::getLeaf(
        ColumnVector *input) {
	bool isLeft = split->isLeftNode(input);
	if (isLeft) {
		return leftElement->getLeaf(input);
	} else {
		return rightElement->getLeaf(input);
	}
}

template<typename TreeData>
int Node<TreeData>::getDepth() {
	return std::max(leftElement->getDepth(), rightElement->getDepth()) + 1;
}

template<typename TreeData>
class Leaf: public TreeElement<TreeData> {
protected:
	TreeData data;
	TreeDataFactory<TreeData> *dataFactory;

	int numLeaf;
	DataSubset *subset;

public:
	virtual Leaf<TreeData> *getLeaf(ColumnVector *);
	virtual TreeData getTreeData();

	virtual bool isLeaf() {
		return true;
	}

	int getLeafNumber() {
		return numLeaf;
	}

	int getNumSamples() {
		return subset->size();
	}

	DataSubset *getDataSet() {
		return subset;
	}

public:
	Leaf& operator=(const Leaf&) = delete;
	Leaf(const Leaf&) = delete;

	Leaf(TreeElement<TreeData> *parent, TreeData l_data, DataSubset *subset,
		        int numLeaf, TreeDataFactory<TreeData> *l_dataFactory);
	virtual ~Leaf();
};

template<typename TreeData>
Leaf<TreeData>::Leaf(
	TreeElement<TreeData> *parent, TreeData l_data, DataSubset *l_subset,
	int l_numLeaf, TreeDataFactory<TreeData> *l_dataFactory):
	TreeElement<TreeData>(parent), data(l_data),
	dataFactory(l_dataFactory), numLeaf(l_numLeaf),
	subset(new DataSubset(*l_subset)) {}

template<typename TreeData>
Leaf<TreeData>::~Leaf() {
	dataFactory->deleteData(data);
	delete subset;
}

template<typename TreeData>
TreeData Leaf<TreeData>::getTreeData() {
	//printf("Returning Data from Leave %d\n", numLeaf);
	return data;
}

template<typename TreeData>
Leaf<TreeData> *Leaf<TreeData>::getLeaf(
        ColumnVector *
) {
	//printf("Returning Data from Leave %d\n", numLeaf);
	return this;
}

template<typename TreeData>
class Tree: public Mapping<TreeData> {
protected:
	TreeElement<TreeData> *root;
	TreeDataFactory<TreeData> *dataFactory;
	int numLeaves;

	Leaf<TreeData> **leaves;
	DataSet *inputData;

protected:
	virtual void createTree(DataSet *inputData,
	        SplittingConditionFactory *splittingFactory,
	        TreeDataFactory<TreeData> *l_dataFactory,
	        bool createLeaves = true);

	virtual TreeElement<TreeData> *createNode(TreeElement<TreeData> *parent,
	        DataSet *inputData, DataSubset *inputDataSubset,
	        SplittingConditionFactory *splittingFactory,
	        TreeDataFactory<TreeData> *dataFactory);

	virtual int setLeaves(TreeElement<TreeData> *element, int numLeaf);
	virtual TreeData doGetOutputValue(ColumnVector *input);

public:
	Leaf<TreeData> *getLeaf(int index);

	int getNumLeaves();
	int getDepth();
	int getNumSamples();

	TreeElement<TreeData> *getRoot() {
		return root;
	}

	DataSet *getInputData() {
		return inputData;
	}

	virtual Leaf<TreeData> *getLeaf(ColumnVector *input);
	TreeDataFactory<TreeData> *getDataFactory() {
		return dataFactory;
	}

	virtual void addNewInput(int index, SplittingConditionFactory *splitting);
	void createLeavesArray();

protected:
	Tree(int numDim);

public:
	Tree& operator=(const Tree&) = delete;
	Tree(const Tree&) = delete;

	Tree(DataSet *inputData, SplittingConditionFactory *splittingFactory,
	        TreeDataFactory<TreeData> *l_dataFactory);
	virtual ~Tree();
};

template<typename TreeData>
Tree<TreeData>::Tree(int numDim):
	Mapping<TreeData>(numDim), root(nullptr), dataFactory(nullptr),
	numLeaves(0), leaves(nullptr), inputData(nullptr) {}

template<typename TreeData>
Tree<TreeData>::~Tree() {
	if (root != nullptr) {
		delete root;
	}
	if (leaves != nullptr) {
		delete[] leaves;
	}
}

template<typename TreeData>
Leaf<TreeData> * Tree<TreeData>::getLeaf(int index) {
	return leaves[index];
}

template<typename TreeData>
void Tree<TreeData>::addNewInput(
	int index,
	SplittingConditionFactory *splittingFactory
) {
	ColumnVector *newInput = (*inputData)[index];
	Leaf<TreeData> *leaf = getLeaf(newInput);

	if (leaf != nullptr && leaf->getParent() != nullptr) {
		Node<TreeData> *parent =
		        dynamic_cast<Node<TreeData> *>(leaf->getParent());

		bool isLeft = parent->getLeftElement() == leaf;
		DataSubset *subset = leaf->getDataSet();
		subset->insert(index);

		TreeElement<TreeData> *newNode = createNode(parent, getInputData(),
		        subset, splittingFactory, getDataFactory());

		if (isLeft) {
			parent->setLeftElement(newNode);
		} else {
			parent->setRightElement(newNode);
		}
		delete leaf;
		numLeaves--;

	} else {
		if (root != nullptr) {
			delete root;
		}

		createTree(getInputData(), splittingFactory, getDataFactory(), false);
	}
	//printf("DataSetSize %d, Leaves: %d\n", inputData->size(), numLeaves);
}

template<typename TreeData>
int Tree<TreeData>::getNumLeaves() {
	return numLeaves;
}

template<typename TreeData>
int Tree<TreeData>::getDepth() {
	return root->getDepth();
}

template<typename TreeData>
Tree<TreeData>::Tree(
	DataSet *inputData_,
	SplittingConditionFactory *splittingFactory,
	TreeDataFactory<TreeData> *l_dataFactory
):
	Mapping<TreeData>(inputData_->getNumDimensions()), root(nullptr),
	dataFactory(nullptr), numLeaves(0), leaves(nullptr), inputData(nullptr)
{
	createTree(inputData_, splittingFactory, l_dataFactory);
}

template<typename TreeData> TreeData
Tree<TreeData>::doGetOutputValue(ColumnVector *input) {
	return root->getLeaf(input)->getTreeData();
}

template<typename TreeData>
Leaf<TreeData> * Tree<TreeData>::getLeaf(ColumnVector *input) {
	if (root) {
		ColumnVector *l_input = Mapping<TreeData>::getPreprocessedInput(input);

		return root->getLeaf(l_input);
	} else {
		return nullptr;
	}
}

template<typename TreeData>
void Tree<TreeData>::createTree(
	DataSet *l_inputData,
	SplittingConditionFactory *splittingFactory,
    TreeDataFactory<TreeData> *l_dataFactory,
    bool setLeaves_
) {
	inputData = l_inputData;
	dataFactory = l_dataFactory;

	DataSubset subset;
	numLeaves = 0;

	if (l_inputData->size() > 0) {
		for (unsigned int i = 0; i < l_inputData->size(); i++) {
			subset.insert(i);
		}
		dataFactory = l_dataFactory;
		root = createNode(nullptr, l_inputData, &subset, splittingFactory,
		        l_dataFactory);

		if (setLeaves_) {
			createLeavesArray();
		}
	}
}

template<typename TreeData>
void Tree<TreeData>::createLeavesArray() {
	if (leaves) {
		delete leaves;
	}

	leaves = new Leaf<TreeData>*[numLeaves];
	setLeaves(root, 0);
}

template<typename TreeData>
int Tree<TreeData>::getNumSamples() {
	int samples = 0;
	for (int i = 0; i < numLeaves; i++) {
		samples += leaves[i]->getDataSet()->size();
	}
	return samples;
}

template<typename TreeData>
int Tree<TreeData>::setLeaves(TreeElement<TreeData> *element, int numLeaf) {
	if (element->isLeaf()) {
		Leaf<TreeData> *leaf = dynamic_cast<Leaf<TreeData> *>(element);
		leaves[numLeaf] = leaf;
		return 1;
	} else {
		int newLeaves = 0;
		Node<TreeData> *node = dynamic_cast<Node<TreeData> *>(element);

		newLeaves += setLeaves(node->getLeftElement(), numLeaf);

		newLeaves += setLeaves(node->getRightElement(), numLeaf + newLeaves);

		return newLeaves;
	}

}

template<typename TreeData>
TreeElement<TreeData> *Tree<TreeData>::createNode(
	TreeElement<TreeData> *parent,
	DataSet *inputData_,
	DataSubset *inputDataSubset,
	SplittingConditionFactory *splittingFactory,
	TreeDataFactory<TreeData> *dataFactory_
) {
	//printf("Creating Node with %d Inputs\n", inputDataSubset->size());

	bool isLeaf = splittingFactory->isLeaf(inputDataSubset);
	TreeElement<TreeData> *newNode = nullptr;

	if (isLeaf) {
		TreeData data = dataFactory_->createTreeData(inputDataSubset, numLeaves);
		newNode = new Leaf<TreeData>(parent, data, inputDataSubset, numLeaves,
				dataFactory_);
		numLeaves++;
	} else {
		SplittingCondition *split = splittingFactory->createSplittingCondition(
		        inputDataSubset);

		DataSubset leftSet;
		DataSubset rightSet;

		DataSubset::iterator it = inputDataSubset->begin();

		for (; it != inputDataSubset->end(); it++) {
			ColumnVector *input = (*inputData_)[*it];

			if (split->isLeftNode(input)) {
				leftSet.insert(*it);
			} else {
				rightSet.insert(*it);
			}
		}
		//printf("LeftSet : %d, Right Set : %d\n", leftSet.size(), rightSet.size());

		Node<TreeData> *newNode1 = new Node<TreeData>(parent, split, nullptr,
		        nullptr);

		TreeElement<TreeData> *leftElement = createNode(newNode1, inputData_,
		        &leftSet, splittingFactory, dataFactory_);

		TreeElement<TreeData> *rightElement = createNode(newNode1, inputData_,
		        &rightSet, splittingFactory, dataFactory_);

		newNode1->setLeftElement(leftElement);
		newNode1->setRightElement(rightElement);

		newNode = newNode1;

	}
	return newNode;
}

} //namespace rlearner

#endif //Rlearner_ctrees_H_
