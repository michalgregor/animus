// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "evaluator.h"
#include "agent.h"
#include "agentcontroller.h"
#include "rewardfunction.h"
#include "statecollection.h"
#include "environmentmodel.h"
#include "transitionfunction.h"
#include "AgentSimulator.h"

namespace rlearner {

PolicyEvaluator::PolicyEvaluator(
	AgentSimulator* agentSimulator, RewardFunction *rewardFunction, int nEpisodes,
    int nStepsPerEpisode
):	SemiMDPRewardListener(rewardFunction),
	agentSimulator(agentSimulator),
	controller(nullptr),
	detController(agentSimulator->getAgent().get()),
	policyValue(0),
	nEpisodes(nEpisodes),
	nStepsPerEpisode(nStepsPerEpisode) {}

void PolicyEvaluator::setAgentController(AgentController *l_controller) {
	controller = l_controller;
}

void PolicyEvaluator::setDeterministicController(
    DeterministicController *l_detController) {
	detController = l_detController;
}

RealType PolicyEvaluator::evaluatePolicy() {
	RealType value = 0;
	agentSimulator->getAgent()->addSemiMDPListener(this);

	AgentController *tempController = nullptr;
	if(controller) {
		tempController = detController->getController();
		detController->setController(controller);
	}

	for(int i = 0; i < nEpisodes; i++) {
		agentSimulator->startNewEpisode();
		agentSimulator->doControllerEpisode(nStepsPerEpisode);
		value += this->getEpisodeValue();
	}
	value /= nEpisodes;
	agentSimulator->getAgent()->removeSemiMDPListener(this);

	if(tempController) {
		detController->setController(tempController);
	}

	return value;
}

AverageRewardCalculator::AverageRewardCalculator(
	AgentSimulator* agentSimulator, RewardFunction *rewardFunction, int nEpisodes,
    int nStepsPerEpisode, RealType minReward) :
	PolicyEvaluator(agentSimulator, rewardFunction, nEpisodes, nStepsPerEpisode) {
	nSteps = 0;
	averageReward = 0;
	this->minReward = minReward;
}

RealType AverageRewardCalculator::getEpisodeValue() {
	if(agentSimulator->getEnvironmentModel()->isReset()) {
		averageReward += minReward * (nStepsPerEpisode - nSteps);
		nSteps = nStepsPerEpisode;
	}
	return averageReward / nSteps;
}

void AverageRewardCalculator::nextStep(
    StateCollection *, Action *, RealType reward, StateCollection *) {
	averageReward += reward;
	nSteps++;
}

void AverageRewardCalculator::newEpisode() {
	averageReward = 0;
	nSteps = 0;
}

RewardPerEpisodeCalculator::RewardPerEpisodeCalculator(
	AgentSimulator* agentSimulator, RewardFunction *rewardFunction, int nEpisodes,
    int nStepsPerEpisode) :
	PolicyEvaluator(agentSimulator, rewardFunction, nEpisodes, nStepsPerEpisode) {
	reward = 0;
}

RealType RewardPerEpisodeCalculator::getEpisodeValue() {
	return reward;
}

void RewardPerEpisodeCalculator::nextStep(
    StateCollection *, Action *, RealType l_reward, StateCollection *) {
	reward += l_reward;
}

void RewardPerEpisodeCalculator::newEpisode() {
	reward = 0;
}

ValueCalculator::ValueCalculator(
	AgentSimulator* agentSimulator, RewardFunction *rewardFunction, int nEpisodes,
    int nStepsPerEpisode, RealType gamma) :
	PolicyEvaluator(agentSimulator, rewardFunction, nEpisodes, nStepsPerEpisode) {
	nSteps = 0;
	value = 0;
	addParameter("DiscountFactor", gamma);
}

RealType ValueCalculator::getEpisodeValue() {
	return value;
}

void ValueCalculator::nextStep(
    StateCollection *, Action *action, RealType reward, StateCollection *) {
	value += pow(getParameter("DiscountFactor"), nSteps) * reward;
	nSteps += action->getDuration();
}

void ValueCalculator::newEpisode() {
	value = 0;
	nSteps = 0;
}

PolicySameStateEvaluator::PolicySameStateEvaluator(
	AgentSimulator* agentSimulator, RewardFunction *rewardFunction,
    TransitionFunctionEnvironment *environment, StateList *l_startStates,
    int nStepsPerEpisode) :
	    PolicyEvaluator(agentSimulator, rewardFunction, l_startStates->getNumStates(),
	        nStepsPerEpisode) {
	this->startStates = new StateList(environment->getStateProperties());

	this->environment = environment;

	setStartStates(l_startStates);

	sender = agentSimulator->getAgent().get();
}

PolicySameStateEvaluator::PolicySameStateEvaluator(
	AgentSimulator* agentSimulator, RewardFunction *rewardFunction,
    TransitionFunctionEnvironment *environment, int numStartStates,
    int nStepsPerEpisode) :
	PolicyEvaluator(agentSimulator, rewardFunction, numStartStates, nStepsPerEpisode) {
	this->startStates = new StateList(environment->getStateProperties());

	this->environment = environment;

	getNewStartStates(numStartStates);

	sender = agentSimulator->getAgent().get();
}

PolicySameStateEvaluator::~PolicySameStateEvaluator() {
	delete startStates;
}

RealType PolicySameStateEvaluator::getValueForState(
    State *startState, int nSteps) {
	if(nSteps > 0) {
		//printf("Evaluator Start State: \n");		
		//startState->saveASCII(stdout);

		AgentController *tempController = nullptr;
		if(controller) {
			tempController = detController->getController();
			detController->setController(controller);
		}

		sender->addSemiMDPListener(this);
		agentSimulator->startNewEpisode();
		environment->setState(startState);
		agentSimulator->doControllerEpisode(nSteps);
		sender->removeSemiMDPListener(this);
		if(tempController) {
			detController->setController(tempController);
		}

		return this->getEpisodeValue();
	} else {
		return 0;
	}
}

RealType PolicySameStateEvaluator::getActionValueForState(
    State *startState, Action *action, int nSteps) {
	if(nSteps > 0) {
		AgentController *tempController = nullptr;
		if(controller) {
			tempController = detController->getController();
			detController->setController(controller);
		}

		sender->addSemiMDPListener(this);

		agentSimulator->startNewEpisode();

		environment->setState(startState);

		agentSimulator->doAction(action);
		if(nSteps > 1 && !agentSimulator->getEnvironmentModel()->isReset()) {
			agentSimulator->doControllerEpisode(nSteps - 1);
		}
		sender->removeSemiMDPListener(this);

		if(tempController) {
			detController->setController(tempController);
		}

	}
	return this->getEpisodeValue();
}

RealType PolicySameStateEvaluator::evaluatePolicy() {
	RealType value = 0;

	State *startState = new State(environment->getStateProperties());
	for(int i = 0; i < nEpisodes; i++) {

		startStates->getState(i, startState);
		value += getValueForState(startState, nStepsPerEpisode);
	}
	value /= nEpisodes;

	delete startState;
	return value;
}

void PolicySameStateEvaluator::getNewStartStates(int numStartStates) {
	State *startState = new State(environment->getStateProperties());

	startStates->clear();

	for(int i = 0; i < numStartStates; i++) {
		environment->resetModel();
		environment->getState(startState);
		startStates->addState(startState);
	}
	nEpisodes = startStates->getNumStates();
	delete startState;
}

void PolicySameStateEvaluator::setStartStates(StateList *newList) {
	int numStartStates = newList->getNumStates();

	State *startState = new State(environment->getStateProperties());

	startStates->clear();

	for(int i = 0; i < numStartStates; i++) {
		newList->getState(i, startState);
		startStates->addState(startState);
	}
	nEpisodes = startStates->getNumStates();
	delete startState;
}

AverageRewardSameStateCalculator::AverageRewardSameStateCalculator(
    AgentSimulator* agentSimulator, RewardFunction *rewardFunction,
    TransitionFunctionEnvironment *environment, StateList *startStates,
    int nStepsPerEpisode, RealType minReward) :
	    PolicySameStateEvaluator(agentSimulator, rewardFunction, environment,
	        startStates, nStepsPerEpisode) {
	nSteps = 0;
	averageReward = 0;

	this->minReward = minReward;

}

RealType AverageRewardSameStateCalculator::getEpisodeValue() {
	if(agentSimulator->getEnvironmentModel()->isFailed()) {
		averageReward += minReward * (nStepsPerEpisode - nSteps);
		nSteps = nStepsPerEpisode;
	}
	return averageReward / nSteps;
}

void AverageRewardSameStateCalculator::nextStep(
    StateCollection *, Action *, RealType reward, StateCollection *) {
	averageReward += reward;
	nSteps++;
}

void AverageRewardSameStateCalculator::newEpisode() {
	averageReward = 0;
	nSteps = 0;
}

ValueSameStateCalculator::ValueSameStateCalculator(
	AgentSimulator* agentSimulator, RewardFunction *rewardFunction,
    TransitionFunctionEnvironment *environment, StateList *startStates,
    int nStepsPerEpisode, RealType gamma) :
	    PolicySameStateEvaluator(agentSimulator, rewardFunction, environment,
	        startStates, nStepsPerEpisode) {
	nSteps = 0;
	value = 0;
	addParameter("DiscountFactor", gamma);
}

RealType ValueSameStateCalculator::getEpisodeValue() {
//	printf("Value %f\n", value);
	return value;
}

void ValueSameStateCalculator::nextStep(
    StateCollection *, Action *action, RealType reward, StateCollection *) {
	value += pow(getParameter("DiscountFactor"), nSteps) * reward;
	nSteps += action->getDuration();
	//printf("Steps %d, Reward %f\n", nSteps, reward);
}

void ValueSameStateCalculator::newEpisode() {
//	printf("Value reset %f\n", value); 
	value = 0;
	nSteps = 0;
}

PolicyGreedynessEvaluator::PolicyGreedynessEvaluator(
	AgentSimulator *agentSimulator, RewardFunction *reward, int nEpisodes, int nStepsPerEpsiode,
    AgentController *l_greedyPolicy) :
	PolicyEvaluator(agentSimulator, reward, nEpisodes, nStepsPerEpsiode) {
	nGreedyActions = 0;
	greedyPolicy = l_greedyPolicy;

	actionDataSet = new ActionDataSet(greedyPolicy->getActions());
}

PolicyGreedynessEvaluator::~PolicyGreedynessEvaluator() {
	delete actionDataSet;
}

RealType PolicyGreedynessEvaluator::getEpisodeValue() {
	return (RealType) nGreedyActions / (RealType) nStepsPerEpisode;
}

void PolicyGreedynessEvaluator::nextStep(
    StateCollection *oldState, Action *action, RealType, StateCollection *) {
	Action *greedyAction = greedyPolicy->getNextAction(oldState);
	if(action->isSameAction(greedyAction,
	    actionDataSet->getActionData(greedyAction))) {
		nGreedyActions++;
	}
}

void PolicyGreedynessEvaluator::newEpisode() {
	nGreedyActions = 0;
}

} //namespace rlearner
