// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ctestsuit_H_
#define Rlearner_ctestsuit_H_

#include <ctime>
#include <cstdio>
#include <map>
#include <string>
#include <iostream>

#include "parameters.h"

#define ARCF_IDENTITY 1
#define ARCF_LINEAR 2
#define ARCF_AVERAGE 3

namespace rlearner {

class Evaluator;
class VisitStateCounter;
class VisitStateActionCounter;

class AgentPlayground;
class StateProperties;
class ActionSet;
class RewardFunction;

class AbstractVFunction;
class StateModifier;
class ErrorSender;
class FeatureVFunction;
class FeatureQFunction;

class StateList;

class VAverageTDErrorLearner;
class VAverageTDVarianceLearner;

class AgentSimulator;
class AgentController;
class LearnDataObject;
class AdaptiveParameterCalculator;
class SemiMDPListener;
class SemiMarkovDecisionProcess;

class PolicyEvaluation;
class PolicyIteration;
class GradientLearner;

class GraphDynamicProgramming;

class TestSuiteEvaluatorLogger {
protected:
	std::string outputDirectory;

public:
	int nEpisodesBeforeEvaluate;

public:
	void setOutputDirectory(std::string outputDirectory);

	virtual void evaluate(
	    std::string evaluationDirectory,
	    int trial,
	    int numEpisodes
	) = 0;

	virtual void startNewEvaluation(
	    std::string evaluationDirectory,
	    Parameters *parameters,
	    int trial
	);

	virtual void endEvaluation() {}

public:
	TestSuiteEvaluatorLogger(std::string outputDirectory);
	virtual ~TestSuiteEvaluatorLogger() {}
};

class TestSuiteLoggerFromEvaluator: public TestSuiteEvaluatorLogger {
protected:
	Evaluator *evaluator;
	std::string outputFileName;

public:
	virtual void evaluate(
	    std::string evaluationDirectory,
	    int trial,
	    int numEpisodes
	);

	virtual RealType evaluateValue(
	    std::string evaluationDirectory,
	    int trial,
	    int numEpisodes
	);

	virtual void startNewEvaluation(
	    std::string evaluationDirectory,
	    Parameters *parameters,
	    int trial
	);

public:
	TestSuiteLoggerFromEvaluator(
	    std::string outputDirectory,
	    std::string outputFileName,
	    Evaluator *evaluator
	);

	virtual ~TestSuiteLoggerFromEvaluator() {}
};

class GraphLogger: public TestSuiteEvaluatorLogger {
protected:
	StateList *states;
	GraphDynamicProgramming *graph;

public:
	virtual void evaluate(
	    std::string evaluationDirectory,
	    int trial,
	    int numEpisodes
	);

	virtual void startNewEvaluation(
	    std::string evaluationDirectory,
	    Parameters *parameters,
	    int trial
	);

public:
	GraphLogger(StateList *states, GraphDynamicProgramming *graph);
	virtual ~GraphLogger();
};

class MatlabEpisodeOutputLogger: public TestSuiteEvaluatorLogger {
protected:
	AgentSimulator* agentSimulator;
	int nEpisodes;
	int nSteps;

	StateProperties *modifier;
	ActionSet *actions;
	RewardFunction *rewardFunction;

public:
	virtual void evaluate(
	    std::string evaluationDirectory,
	    int trial,
	    int numEpisodes
	);

	virtual void startNewEvaluation(
	    std::string evaluationDirectory,
	    Parameters *parameters,
	    int trial
	);

public:
	MatlabEpisodeOutputLogger(
		AgentSimulator* agentSimulator,
	    RewardFunction *rewardFunction,
	    StateProperties *modifier,
	    ActionSet *actions,
	    int nEpisodes,
	    int nSteps
	);

	virtual ~MatlabEpisodeOutputLogger();
};

class MatlabVAnalyzerLogger: public TestSuiteEvaluatorLogger {
protected:
	AbstractVFunction *vFunction;
	std::set<StateModifierList*> _modifierLists;
	ErrorSender *vLearner;

	FeatureVFunction *visitCounter;
	FeatureVFunction *averageError;
	FeatureVFunction *averageVariance;

	int dim1;
	int dim2;

	int part1;
	int part2;

	StateList *states;

public:
	int nTrialEvaluate;

	VisitStateCounter *visitCounterLearner;
	VAverageTDErrorLearner *errorLearner;
	VAverageTDVarianceLearner *varianceLearner;

public:
	virtual void evaluate(
	    std::string evaluationDirectory,
	    int trial,
	    int numEpisodes
	);

	virtual void startNewEvaluation(
	    std::string evaluationDirectory,
	    Parameters *parameters,
	    int trial
	);

	virtual void addListenersToAgent(SemiMDPSender *agent);
	virtual void removeListenersToAgent(SemiMDPSender *agent);

public:
	MatlabVAnalyzerLogger(
	    AbstractVFunction *l_vFunction,
	    FeatureCalculator *featCalc,
	    ErrorSender *l_vLearner,
	    StateList *l_States,
	    int l_dim1,
	    int l_dim2,
	    int l_part1,
	    int l_part2,
	    const std::set<StateModifierList*>& modifierLists
	);

	virtual ~MatlabVAnalyzerLogger();
};

class MatlabQAnalyzerLogger: public MatlabVAnalyzerLogger {
protected:
	FeatureQFunction *qFunction;
	FeatureQFunction *saVisits;
	bool delVFunction;

public:
	VisitStateActionCounter *visitStateActionCounterLearner;

public:
	virtual void evaluate(
	    std::string evaluationDirectory,
	    int trial,
	    int numEpisodes
	);

	virtual void startNewEvaluation(
	    std::string evaluationDirectory,
	    Parameters *parameters,
	    int trial
	);

	virtual void addListenersToAgent(SemiMDPSender *agent);
	virtual void removeListenersToAgent(SemiMDPSender *agent);

public:
	MatlabQAnalyzerLogger(
	    FeatureQFunction *l_qFunction,
	    FeatureCalculator *featCalc,
	    ErrorSender *l_vLearner,
	    StateList *l_States,
	    int l_dim1,
	    int l_dim2,
	    int l_part1,
	    int l_part2,
	    const std::set<StateModifierList*>& modifierLists
	);

	MatlabQAnalyzerLogger(
	    FeatureVFunction *vFunction,
	    FeatureQFunction *l_qFunction,
	    FeatureCalculator *featCalc,
	    ErrorSender *l_vLearner,
	    StateList *l_States,
	    int l_dim1,
	    int l_dim2,
	    int l_part1,
	    int l_part2,
	    const std::set<StateModifierList*>& modifierLists
	);

	virtual ~MatlabQAnalyzerLogger();
};

class TestSuite: virtual public ParameterObject {
protected:
	AgentController *controller;
	AgentController *evaluationController;
	std::list<LearnDataObject *> *learnDataObjects;

	std::map<LearnDataObject *, bool> *saveLearnData;
	std::list<AdaptiveParameterCalculator *> *paramCalculators;

	AgentSimulator* agentSimulator;
	std::string testSuiteName;
	std::string learnDataFileName;

public:
	virtual void addParamCalculator(
	    AdaptiveParameterCalculator *paramCalculator
	);

	virtual void saveLearnedData(std::ostream& stream);
	virtual void loadLearnedData(std::istream& stream);

	virtual void resetParamCalculators();
	virtual void resetLearnedData();

	void addLearnDataObject(
	    LearnDataObject *learnDataObject,
	    bool saveLearnData = true
	);

	virtual void learn(int nEpisodes, int nStepsPerEpisode) = 0;
	virtual AgentController *getController();
	virtual void setController(AgentController *controller);
	virtual AgentController *getEvaluationController();
	virtual void setEvaluationController(AgentController *evaluationController);

	virtual void deleteObjects();
	std::string getTestSuiteName();
	void setTestSuiteName(std::string name);

public:
	TestSuite(
		AgentSimulator* agentSimulator,
	    AgentController *controller,
	    LearnDataObject *vFunction,
	    char *testSuiteName
	);

	TestSuite(
		AgentSimulator* agentSimulator,
	    AgentController *controller,
	    AgentController *evaluationController,
	    LearnDataObject *vFunction,
	    char *testSuiteName
	);

	virtual ~TestSuite();
};

class ListenerTestSuite: public TestSuite {
protected:
	std::list<SemiMDPListener *> *learnerObjects;
	std::map<SemiMDPListener *, SemiMarkovDecisionProcess *> *addToAgent;

public:
	virtual void addLearnersToAgent();
	virtual void removeLearnersFromAgent();

	void addLearnerObject(
	    SemiMDPListener *listener,
	    bool addParams = true,
	    bool addBack = true,
	    SemiMarkovDecisionProcess *remove = nullptr
	);

	virtual void learn(int nEpisodes, int nStepsPerEpisode);
	virtual void deleteObjects();

	std::list<SemiMDPListener *> *getLearnerList() {
		return learnerObjects;
	}

public:
	ListenerTestSuite(
		AgentSimulator* agentSimulator,
	    SemiMDPListener *learner,
	    AgentController *controller,
	    LearnDataObject *vFunction,
	    char *testSuiteName
	);

	ListenerTestSuite(
		AgentSimulator* agentSimulator,
	    SemiMDPListener *learner,
	    AgentController *controller,
	    AgentController *evaluationController,
	    LearnDataObject *vFunction,
	    char *testSuiteName
	);

	virtual ~ListenerTestSuite();
};

class PolicyEvaluation;

class PolicyEvaluationTestSuite: public TestSuite {
protected:
	PolicyEvaluation *evaluation;

public:
	virtual void learn(int nEpisodes, int nStepsPerEpisode);
	virtual void resetLearnedData();

public:
	PolicyEvaluationTestSuite(
		AgentSimulator* agentSimulator,
	    PolicyEvaluation *learner,
	    AgentController *controller,
	    LearnDataObject *vFunction,
	    char *testSuiteName
	);

	virtual ~PolicyEvaluationTestSuite();
};

class PolicyIteration;

class PolicyIterationTestSuite: public TestSuite {
protected:
	PolicyIteration *policyIteration;

public:
	virtual void learn(int nEpisodes, int nStepsPerEpisode);
	virtual void resetLearnedData();

public:
	PolicyIterationTestSuite(
		AgentSimulator* agentSimulator,
	    PolicyIteration *policyIteration,
	    AgentController *controller,
	    LearnDataObject *vFunction,
	    char *testSuiteName
	);

	virtual ~PolicyIterationTestSuite();
};

class PolicyGradientTestSuite: public TestSuite {
protected:
	GradientLearner *learner;

public:
	virtual void deleteObjects();
	virtual void learn(int nEpisodes, int nStepsPerEpisode);
	virtual void resetLearnedData();

public:
	PolicyGradientTestSuite(
		AgentSimulator* agentSimulator,
	    GradientLearner *learner,
	    AgentController *controller,
	    LearnDataObject *vFunction,
	    char *testSuiteName,
	    int nMaxGradientUpdates = 1
	);

	PolicyGradientTestSuite(
		AgentSimulator* agentSimulator,
	    GradientLearner *learner,
	    AgentController *controller,
	    AgentController *evaluationController,
	    LearnDataObject *vFunction,
	    char *testSuiteName,
	    int nMaxGradientUpdates = 1
	);

	virtual ~PolicyGradientTestSuite();
};

class TestSuiteCollection {
protected:
	std::map<std::string, TestSuite *> *testSuiteMap;
	std::list<void *> *objectsToDelete;

public:
	void addTestSuite(TestSuite *testSuite);
	void removeTestSuite(TestSuite *testSuite);
	void removeAllTestSuites();

	int getNumTestSuites();
	TestSuite *getTestSuite(std::string testSuiteName);
	TestSuite *getTestSuite(int index);

	void addObjectToDelete(void *object);
	void deleteObjects();

public:
	TestSuiteCollection();
	virtual ~TestSuiteCollection();
};

typedef struct {
	RealType averageValue;
	RealType bestValue;
	unsigned int trialNumber;
	RealType evaluationTime;
	std::string evaluationDate;
} EvaluationValue;

typedef std::list<EvaluationValue> EvaluationValues;

class TestSuiteEvaluator: virtual public ParameterObject {
protected:
	AgentSimulator* agentSimulator;
	std::list<TestSuiteEvaluatorLogger *> *evaluators;

	std::string baseDirectory;
	TestSuite *testSuite;

	unsigned int nTrials;
	unsigned int trialNumber;

	bool exception;

	std::list<Parameters *> *parameterList;
	std::map<Parameters *, EvaluationValues *> *evaluations;

protected:
	virtual void newEvaluationTrial(
	    TestSuite *testSuite,
	    EvaluationValue *evaluationData
	) = 0;

	virtual void doEpisode(TestSuite *testSuite, int nEpisode) = 0;
	virtual void getEvaluationValue(EvaluationValue *evaluationData) = 0;
	virtual bool isFinished(int unsigned nEpisode) = 0;

	Parameters *getParametersObject(Parameters *);

public:
	std::string getEvaluationDirectory();
	int getNewTrialNumber();
	std::string getLearnDataFileName(int trialNumber);

	void checkDirectories();

	virtual void saveEvaluationDataMatlab(std::string filename);

	virtual void doEvaluationTrial(
	    Parameters *testSuite,
	    EvaluationValue *evaluationData
	);

	virtual void evaluateParameters(Parameters *testSuite);
	virtual RealType getAverageValue(Parameters *testSuite);
	virtual RealType getBestValue(Parameters *testSuite);

	virtual EvaluationValues *getEvaluationValues();
	virtual void addPolicyEvaluator(TestSuiteEvaluatorLogger *evaluator);

public:
	TestSuiteEvaluator(
		AgentSimulator* agentSimulator,
	    std::string baseDirectory,
	    TestSuite *testSuite,
	    int nTrials
	);

	virtual ~TestSuiteEvaluator();
};

class AverageRewardTestSuiteEvaluator: public TestSuiteEvaluator {
protected:
	int numEvals;
	RealType bestValue;
	RealType averageValue;

	Evaluator *evaluator;
	shared_ptr<std::ostream> evaluationFile;

protected:
	virtual void newEvaluationTrial(
	    TestSuite *testSuite,
	    EvaluationValue *evaluationData
	);

	virtual void doEpisode(TestSuite *testSuite, int nEpisode);
	virtual void getEvaluationValue(EvaluationValue *evaluationData);
	virtual bool isFinished(unsigned int nEpisode);

public:
	unsigned int episodesBeforeEvaluate;
	unsigned int totalLearnEpisodes;
	unsigned int stepsLearnEpisode;

public:
	AverageRewardTestSuiteEvaluator(
		AgentSimulator* agentSimulator,
	    std::string baseDirectory,
	    TestSuite *testSuite,
	    Evaluator *evaluator,
	    int totalLearnEpisodes,
	    int episodesBeforeEvaluate,
	    int stepsLearnEpisode,
	    int nTrials
	);

	virtual ~AverageRewardTestSuiteEvaluator();
};

} //namespace rlearner

#endif //Rlearner_ctestsuit_H_
