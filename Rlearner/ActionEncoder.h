#ifndef Motivator_ActionEncoder_H_
#define Motivator_ActionEncoder_H_

#include "system.h"
#include <vector>
#include "action.h"

namespace rlearner {

/**
* @class ActionEncoder
* @author Michal Gregor
*
* ActionEncoders are used to encode a Action using an std::vector of RealType
* numbers.
**/
class RLEARNER_API ActionEncoder {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	/**
	 * Returns the size of the vector used to represent the action.
	 */
	virtual size_t size() const = 0;

	/**
	 * Encodes the provided Action object using a vector of RealType numbers.
	 */
	virtual std::vector<RealType> operator()(const Action* action) = 0;

public:
	virtual ~ActionEncoder() = 0;
};

}//namespace rlearner

SYS_EXPORT_CLASS(rlearner::ActionEncoder)

#endif //Motivator_ActionEncoder_H_
