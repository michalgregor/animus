// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>
#include <fstream>
#include "system.h"

#include "ril_debug.h"
#include "rewardmodel.h"
#include "agentlistener.h"
#include "theoreticalmodel.h"
#include "stateproperties.h"
#include "statecollection.h"
#include "state.h"
#include "statemodifier.h"
#include "action.h"
#include "utility.h"

namespace rlearner {

FeatureStateRewardFunction::FeatureStateRewardFunction(
        StateProperties *discretizer) :
		FeatureRewardFunction(discretizer) {
	rewards = new std::map<int, RealType>;
}

FeatureStateRewardFunction::~FeatureStateRewardFunction() {
	delete rewards;
}

RealType FeatureStateRewardFunction::getReward(int, Action *, int newState) {
	return getReward(newState);
}

RealType FeatureStateRewardFunction::getReward(int state) {
	return (*rewards)[state];
}

void FeatureStateRewardFunction::setReward(int state, RealType reward) {
	(*rewards)[state] = reward;
}

FeatureRewardModel::FeatureRewardModel(ActionSet *actions,
        RewardFunction *function,
        AbstractFeatureStochasticEstimatedModel *model,
        StateModifier *discretizer
):
	FeatureRewardFunction(discretizer), SemiMDPRewardListener(function), ActionObject(
		actions),
	rewardTable(new MyArray2D<FeatureMap *>(getNumActions(),
        discretizer->getDiscreteStateSize())),
	visitTable(nullptr),
	model(model),
	bExternVisitSparse(true)
{
	for (int i = 0; i < rewardTable->getSize(); i++) {
		rewardTable->set1D(i, new FeatureMap());
	}
}

FeatureRewardModel::FeatureRewardModel(ActionSet *actions,
	RewardFunction *function, StateModifier *discretizer
):
	FeatureRewardFunction(discretizer), SemiMDPRewardListener(function), ActionObject(
		actions),
	rewardTable(new MyArray2D<FeatureMap *>(getNumActions(),
        discretizer->getDiscreteStateSize())),
	visitTable(new MyArray2D<FeatureMap *>(getNumActions(),
        discretizer->getDiscreteStateSize())),
	model(nullptr),
	bExternVisitSparse(false)
{
	int i;

	for (i = 0; i < rewardTable->getSize(); i++) {
		rewardTable->set1D(i, new FeatureMap());
	}

	for (i = 0; i < visitTable->getSize(); i++) {
		visitTable->set1D(i, new FeatureMap());
	}
}

FeatureRewardModel::~FeatureRewardModel() {
	for (int i = 0; i < rewardTable->getSize(); i++) {
		delete rewardTable->get1D(i);
	}

	delete rewardTable;

	if (!bExternVisitSparse) {
		for (int i = 0; i < visitTable->getSize(); i++) {
			delete visitTable->get1D(i);
		}

		delete visitTable;
	}
}

RealType FeatureRewardModel::getTransitionVisits(int oldState, int action,
        int newState) {
	RealType visits = 0.0;

	if (!this->bExternVisitSparse) {
		visits = visitTable->get(action, oldState)->getValue(newState);
	} else {
		Transition *trans =
		        model->getForwardTransitions(action, oldState)->getTransition(
		                newState);
		if (trans == nullptr) {
			visits = 0;
		} else {
			visits = trans->getPropability()
			        * model->getStateActionVisits(oldState, action);
		}
	}

	return visits;
}

RealType FeatureRewardModel::getReward(int oldState, Action *action,
        int newState)
{
	int actionIndex = getActions()->getIndex(action);
	RealType transVisits = getTransitionVisits(oldState, actionIndex, newState);

	//assert(visitSparse->getFaktor(oldState, actionIndex, newState) > 0);

	if (transVisits > 0) {
		return rewardTable->get(actionIndex, oldState)->getValue(newState)
		        / transVisits;
	} else {
		return 0.0;
	}
}

void FeatureRewardModel::nextStep(StateCollection *oldState, Action *action,
        RealType reward, StateCollection *newState)
{
	FeatureMap *featMap;

	State *oldS = oldState->getState(properties);
	State *newS = newState->getState(properties);

	RealType oldreward = 0.0;
	RealType visits = 0.0;

	int actionIndex = getActions()->getIndex(action);

	int type = oldS->getStateProperties()->getType()
	        & (DISCRETESTATE | FEATURESTATE);
	switch (type) {
	case FEATURESTATE: {
		for (unsigned int oldIndex = 0; oldIndex < oldS->getNumDiscreteStates();
		        oldIndex++) {
			int oldFeature = oldS->getDiscreteState(oldIndex);
			featMap = rewardTable->get(actionIndex, oldFeature);

			for (unsigned int newIndex = 0;
			        newIndex < newS->getNumDiscreteStates(); newIndex++) {
				int newFeature = newS->getDiscreteState(newIndex);

				oldreward = featMap->getValue(newFeature);

				(*featMap)[newFeature] = oldreward
				        + reward * newS->getContinuousState(newIndex)
				                * oldS->getContinuousState(oldIndex);

				if (!bExternVisitSparse) {
					visits = visitTable->get(actionIndex, oldFeature)->getValue(
					        newFeature);

					(*visitTable->get(actionIndex, oldFeature))[newFeature] =
					        visits
					                + newS->getContinuousState(newIndex)
					                        * oldS->getContinuousState(
					                                oldIndex);
				}
			}
		}
	break;}
	case DISCRETESTATE:
	default: {
		featMap = rewardTable->get(actionIndex, oldS->getDiscreteStateNumber());

		oldreward = featMap->getValue(newS->getDiscreteStateNumber());

		int feata = oldS->getDiscreteStateNumber();
		int featb = newS->getDiscreteStateNumber();

		(*featMap)[featb] = oldreward + reward;

		if (!bExternVisitSparse) {
			visits = visitTable->get(actionIndex, feata)->getValue(featb);

			(*visitTable->get(actionIndex, feata))[featb] = visits + 1.0;
		}
	}
	break;}
}

void FeatureRewardModel::saveData(std::ostream& stream) {
	FeatureMap::iterator mapIt;
	FeatureMap *featMap;
	fmt::fprintf(stream, "Reward Table\n");

	for (unsigned int action = 0; action < getNumActions(); action++) {
		fmt::fprintf(stream, "Action %d:\n", action);
		for (unsigned int startState = 0;
		        startState < discretizer->getDiscreteStateSize();
		        startState++) {
			featMap = rewardTable->get(action, startState);

			fmt::fprintf(stream, "Startstate %d [%d]: ", startState,
			        featMap->size());

			for (mapIt = featMap->begin(); mapIt != featMap->end(); mapIt++) {
				fmt::fprintf(stream, "(%d %f)", (*mapIt).first, (*mapIt).second);
			}
			fmt::fprintf(stream, "\n");
		}
		fmt::fprintf(stream, "\n");
	}

	if (!this->bExternVisitSparse) {
		fmt::fprintf(stream, "Visit Table\n");

		for (unsigned int action = 0; action < getNumActions(); action++) {
			fmt::fprintf(stream, "Action %d:\n", action);
			for (unsigned int startState = 0;
			        startState < discretizer->getDiscreteStateSize();
			        startState++) {
				featMap = visitTable->get(action, startState);

				fmt::fprintf(stream, "Startstate %d [%d]: ", startState,
				        featMap->size());

				for (mapIt = featMap->begin(); mapIt != featMap->end();
				        mapIt++) {
					fmt::fprintf(stream, "(%d %f)", (*mapIt).first, (*mapIt).second);
				}
				fmt::fprintf(stream, "\n");
			}
			fmt::fprintf(stream, "\n");
		}
	}
}

void FeatureRewardModel::loadData(std::istream& stream) {
	FeatureMap *featMap;
	xscanf(stream, "Reward Table\n");

	int buf, numVal = 0, endState;
	RealType reward;

	for (unsigned int action = 0; action < getNumActions(); action++) {
		xscanf(stream, "Action %d:\n", buf);
		for (unsigned int startState = 0;
		        startState < discretizer->getDiscreteStateSize();
		        startState++) {
			featMap = rewardTable->get(action, startState);

			featMap->clear();

			xscanf(stream, "Startstate %d [%d]: ", buf, numVal);

			for (int i = 0; i < numVal; i++) {
				xscanf(stream, "(%d %lf)", endState, reward);
				(*featMap)[endState] = reward;
			}
			xscanf(stream, "\n");
		}
		xscanf(stream, "\n");
	}

	if (!this->bExternVisitSparse) {
		xscanf(stream, "Visit Table\n");

		for (unsigned int action = 0; action < getNumActions(); action++) {
			xscanf(stream, "Action %d:\n", buf);
			for (unsigned int startState = 0;
			        startState < discretizer->getDiscreteStateSize();
			        startState++) {
				featMap = visitTable->get(action, startState);

				featMap->clear();

				xscanf(stream, "Startstate %d [%d]: ", buf, numVal);

				for (int i = 0; i < numVal; i++) {
					xscanf(stream, "(%d %lf)", endState, reward);
					(*featMap)[endState] = reward;
				}
				xscanf(stream, "\n");
			}
			xscanf(stream, "\n");
		}
	}
}

void FeatureRewardModel::resetData() {
	FeatureMap *featMap;

	for (unsigned int action = 0; action < getNumActions(); action++) {
		for (unsigned int startState = 0;
		        startState < discretizer->getDiscreteStateSize();
		        startState++) {
			featMap = rewardTable->get(action, startState);

			featMap->clear();
		}
	}

	if (!this->bExternVisitSparse) {
		for (unsigned int action = 0; action < getNumActions(); action++) {
			for (unsigned int startState = 0;
			        startState < discretizer->getDiscreteStateSize();
			        startState++) {
				featMap = visitTable->get(action, startState);

				featMap->clear();
			}
		}
	}
}

FeatureStateRewardModel::FeatureStateRewardModel(RewardFunction *function,
        StateModifier *discretizer) :
		FeatureRewardFunction(discretizer), SemiMDPRewardListener(function) {
	rewards = new RealType[discretizer->getDiscreteStateSize(0)];
	visits = new RealType[discretizer->getDiscreteStateSize(0)];

	for (unsigned int i = 0; i < discretizer->getDiscreteStateSize(0); i++) {
		rewards[i] = 0.0;
		visits[i] = 0.0;
	}

	rewardMean = 0.0;
	numRewards = 0;
}

FeatureStateRewardModel::~FeatureStateRewardModel() {
	delete[] rewards;
	delete[] visits;
}

RealType FeatureStateRewardModel::getReward(State *, Action *,
        State *newState) {
	RealType reward = 0.0;

	if (newState->getStateProperties()->isType(FEATURESTATE)) {
		for (unsigned int i = 0; i < newState->getNumContinuousStates(); i++) {
			reward += newState->getContinuousState(i)
			        * getReward(newState->getDiscreteState(i));
		}
	} else {
		if (newState->getStateProperties()->isType(DISCRETESTATE)) {
			reward = getReward(newState->getDiscreteState(0));
		}
	}
	return reward;
}

RealType FeatureStateRewardModel::getReward(int, Action *, int newState) {
	return getReward(newState);
}

RealType FeatureStateRewardModel::getReward(int newState) {
	RealType numVisits = visits[newState];
	RealType reward = 0.0;

	if (numVisits > 0) {
		reward = rewards[newState] / numVisits;
	} else {
		if (numRewards > 0) {
			reward = rewardMean / numRewards;
		}
	}
	return reward;
}

void FeatureStateRewardModel::nextStep(StateCollection *, Action *,
        RealType reward, StateCollection *newStateCol) {
	State *newState = newStateCol->getState(discretizer);

	if (newState->getStateProperties()->isType(FEATURESTATE)) {
		for (unsigned int i = 0; i < newState->getNumContinuousStates(); i++) {
			rewards[newState->getDiscreteState(i)] += reward
			        * newState->getContinuousState(i);
			visits[newState->getDiscreteState(i)] +=
			        newState->getContinuousState(i);
		}
	} else {
		if (newState->getStateProperties()->isType(DISCRETESTATE)) {
			rewards[newState->getDiscreteState(0)] += reward;
			visits[newState->getDiscreteState(0)] += 1.0;
		}
	}
}

void FeatureStateRewardModel::saveData(std::ostream& stream) {
	fmt::fprintf(stream, "State-Reward Table (%d Features):\n",
	        discretizer->getDiscreteStateSize(0));
	for (unsigned int i = 0; i < discretizer->getDiscreteStateSize(0); i++) {
		fmt::fprintf(stream, "%f ", rewards[i]);
	}
	fmt::fprintf(stream, "\n");
	fmt::fprintf(stream, "State-Reward Visit Table (%d Features):\n",
	        discretizer->getDiscreteStateSize(0));
	for (unsigned int i = 0; i < discretizer->getDiscreteStateSize(0); i++) {
		fmt::fprintf(stream, "%f ", visits[i]);
	}
	fmt::fprintf(stream, "\n");
}

void FeatureStateRewardModel::loadData(std::istream& stream) {
	RealType bufNumRewards = 0.0;
	rewardMean = 0.0;
	int buffer;

	xscanf(stream, "State-Reward Table (%d Features):\n", buffer);
	for (unsigned int i = 0; i < discretizer->getDiscreteStateSize(0); i++) {
		xscanf(stream, "%lf ", rewards[i]);
		rewardMean += rewards[i];
	}

	xscanf(stream, "\n");
	xscanf(stream, "State-Reward Visit Table (%d Features):\n", buffer);

	for (unsigned int i = 0; i < discretizer->getDiscreteStateSize(0); i++) {
		xscanf(stream, "%lf ", visits[i]);

		bufNumRewards += visits[i];
	}

	xscanf(stream, "\n");
	numRewards = (int) floor(bufNumRewards);
}

void FeatureStateRewardModel::resetData() {
	for (unsigned int i = 0; i < discretizer->getDiscreteStateSize(0); i++) {
		rewards[i] = 0.0;
		visits[i] = 0.0;
	}

	rewardMean = 0.0;
	numRewards = 0;
}

SemiMDPLastNRewardFunction::SemiMDPLastNRewardFunction(
        RewardFunction *rewardFunction, RealType l_gamma) :
		RewardEpisode(rewardFunction) {
	this->gamma = l_gamma;
}

SemiMDPLastNRewardFunction::~SemiMDPLastNRewardFunction() {}

RealType SemiMDPLastNRewardFunction::getReward(StateCollection *state,
        Action *action, StateCollection *
) {
	RealType reward = 0;
	unsigned int duration = action->getDuration();

	for (unsigned int index = 0; index < duration; index++) {
		if (_rewards.size() < duration) {
			printf("Semi MDP Reward Function: %d < %d!!!\n", _rewards.size(),
			        duration);
		}

		assert(_rewards.size() >= duration);
		reward = reward
		        + pow(gamma, (int) index)
		                * _rewards[_rewards.size() - duration + index];
	}

	return reward;
}

RewardEpisode::RewardEpisode(RewardFunction *rewardFunction) :
		SemiMDPRewardListener(rewardFunction), _rewards() {
}

RewardEpisode::~RewardEpisode() {
}

void RewardEpisode::nextStep(StateCollection *, Action *, RealType reward,
        StateCollection *) {
	_rewards.push_back(reward);
}

void RewardEpisode::newEpisode() {
	_rewards.clear();
}

int RewardEpisode::getNumRewards() {
	return _rewards.size();
}

RealType RewardEpisode::getReward(int index) {
	return _rewards[index];
}

RealType RewardEpisode::getMeanReward() {
	assert(getNumRewards() > 0);
	RealType mean = 0;
	for (int i = 0; i < getNumRewards(); i++) {
		mean += getReward(i);
	}
	return mean / getNumRewards();
}

RealType RewardEpisode::getSummedReward(RealType gamma) {
	RealType sum = 0;
	RealType l_gamma = 1.0;
	for (int i = 0; i < getNumRewards(); i++) {
		sum += l_gamma * getReward(i);
		l_gamma = gamma * l_gamma;
	}
	return sum;
}

RealType RewardEpisode::getLastStepsMeanReward(int Steps) {
	assert(getNumRewards() > 0);
	RealType mean = 0;
	for (int i = getNumRewards() - Steps; i < getNumRewards(); i++) {
		mean += getReward(i);
	}
	return mean / (getNumRewards() - Steps);
}

void RewardEpisode::saveData(std::ostream& stream) {
	fmt::fprintf(stream, "NumRewards: %d\n", getNumRewards());

	for (int i = 0; i < getNumRewards(); i++) {
		RealType dBuf = getReward(i);
		fmt::fprintf(stream, "%lf ", dBuf);
	}
	fmt::fprintf(stream, "\n");

}

void RewardEpisode::loadData(std::istream& stream) {
	int buf;
	size_t result = xscanf(stream, "NumRewards: %d\n", buf);

	if (result > 0) {
		for (int i = 0; i < buf; i++) {
			RealType dBuf;
			xscanf(stream, "%lf ", dBuf);
			_rewards.push_back(dBuf);
		}
	}

	xscanf(stream, "\n");
}

RewardHistorySubset::RewardHistorySubset(RewardHistory *l_episodes,
        std::vector<int> *l_indices) {
	episodes = l_episodes;
	indices = l_indices;
}

RewardHistorySubset::~RewardHistorySubset() {
}

int RewardHistorySubset::getNumEpisodes() {
	return indices->size();
}

RewardEpisode* RewardHistorySubset::getEpisode(int index) {
	return episodes->getEpisode((*indices)[index]);
}

RewardLogger::RewardLogger(
	RewardFunction *reward, const std::string& autoSavefile, int holdMemory
): SemiMDPRewardListener(reward)
{
	init();
	this->holdMemory = holdMemory;
	setAutoSaveFile(autoSavefile);
}

RewardLogger::RewardLogger(const std::string& loadFileName, RewardFunction *reward):
		SemiMDPRewardListener(reward)
{
	init();

	_loadFileName = loadFileName;
	std::ifstream loadFile(_loadFileName);

	this->holdMemory = -1;
	loadData(loadFile);
}

RewardLogger::RewardLogger(RewardFunction *reward) :
		SemiMDPRewardListener(reward)
{
	init();
}

/**
 * Goes over rewards in all episodes (including the current one), and returns
 * the rewards in format: vector[episode][reward].
 *
 * \note This function copies all the rewards -- not necessarily good for
 * efficiency.
 */
std::vector<std::vector<RealType> > RewardLogger::getRewards() {
	auto sz = getNumEpisodes();
	std::vector<std::vector<RealType> > rewards(sz + 1);

	// regular episodes
	for(decltype(sz) i = 0; i < sz; i++) {
		rewards[i] = getEpisode(i)->getRewards();
	}

	// current episode
	rewards.back() = getCurrentEpisode()->getRewards();

	return rewards;
}

void RewardLogger::init() {
	episodes = new std::list<RewardEpisode *>();
	currentEpisode = new RewardEpisode(semiMDPRewardFunction);
	holdMemory = -1;
}

void RewardLogger::setAutoSaveFile(const std::string& filename) {
	if(filename.size()) {
		_filename = filename;
		_file = make_shared<std::ofstream>(filename, std::ofstream::app);
	} else _file.reset();
}

RewardLogger::~RewardLogger() {
	for (std::list<RewardEpisode *>::iterator it = episodes->begin();
	        it != episodes->end(); it++) {
		delete *it;
	}

	if (currentEpisode != nullptr) {
		delete currentEpisode;
	}

	delete episodes;

}

void RewardLogger::resetData() {
	for (std::list<RewardEpisode *>::iterator it = episodes->begin();
	        it != episodes->end(); it++) {
		delete *it;
	}
	if (currentEpisode != nullptr) {
		delete currentEpisode;
		currentEpisode = nullptr;
	}
	episodes->clear();

	if(_loadFileName.size()) {
		std::ifstream loadFile(_loadFileName);
		loadData(loadFile);
	}
}

void RewardLogger::nextStep(StateCollection *oldState, Action *action,
        RealType reward, StateCollection *nextState) {
	currentEpisode->nextStep(oldState, action, reward, nextState);
}

void RewardLogger::newEpisode() {
	if (currentEpisode != nullptr) {
		if (currentEpisode->getNumRewards() > 0) {
			if(_file) {
				currentEpisode->saveData(*_file);
				_file->flush();
			}
			episodes->push_back(currentEpisode);
			currentEpisode = new RewardEpisode(semiMDPRewardFunction);
		}
	} else {
		currentEpisode = new RewardEpisode(semiMDPRewardFunction);
	}

	if (holdMemory > 0 && (int) episodes->size() > holdMemory) {
		RewardEpisode *firstEpisode = *episodes->begin();
		episodes->pop_front();
		delete firstEpisode;
	}
}

void RewardLogger::saveData(std::ostream& stream) {
	int i = 0;

	for (auto it = episodes->begin(); it != episodes->end(); it++, i++) {
		fmt::fprintf(stream, "\nRewardEpisode: %d\n", i);
		(*it)->saveData(stream);
	}
}

void RewardLogger::loadData(std::istream& stream, int numEpisodes) {
	int nEpisodes = numEpisodes;
	int buf;

	if (holdMemory >= 0 && holdMemory < numEpisodes) {
		holdMemory = numEpisodes;
	}

	while(!stream.eof() && (nEpisodes > 0 || nEpisodes < 0)) {
		RewardEpisode *tmp = new RewardEpisode(semiMDPRewardFunction);
		xscanf(stream, "\nRewardEpisode: %d\n", buf);
		tmp->loadData(stream);

		episodes->push_back(tmp);
		nEpisodes--;
	}

	newEpisode();
}

int RewardLogger::getNumEpisodes() {
	return episodes->size();
}

RewardEpisode* RewardLogger::getEpisode(int index) {
	int i = 0;

	std::list<RewardEpisode *>::iterator it;

	assert(index < getNumEpisodes());

	for (it = episodes->begin(); it != episodes->end(), i < index; it++, i++) {

	}

	return *it;
}

RewardEpisode* RewardLogger::getCurrentEpisode() {
	return currentEpisode;
}

void RewardLogger::clearAutoSaveFile() {
	_file.reset();
}

void RewardLogger::setLoadDataFile(const std::string& loadData) {
	_loadFileName = loadData;
}

} //namespace rlearner
