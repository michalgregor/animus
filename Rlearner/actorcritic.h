// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cactorcritic_H_
#define Rlearner_cactorcritic_H_

#include "errorlistener.h"
#include "agentlistener.h"
#include "agentcontroller.h"

#include <Systematic/generator/BoundaryGenerator.h>

namespace rlearner {

class State;
class StateCollection;
class StateProperties;

class Action;
class ActionData;
class ActionDataSet;

class AbstractQFunction;
class AbstractQETraces;

class AbstractVFunction;
class AbstractVETraces;

class StochasticPolicy;

class ContinuousActionGradientPolicy;
class GradientVETraces;
class FeatureList;
class ContinuousActionData;

/**
 * @class Actor
 * Interface for all Actors.
 *
 * The actors have to adopt their policies according to the critics they get.
 * The class Actor provides an interface for sending a critic to the actor
 * (receiveError(StateCollection *currentState, Action *action). This is
 * all the Actor classes have to implement. How to get the policy from
 * the actor is controlled to the Actor classes itself. The class also
 * maintains a learning rate beta, which can be used by the subclasses.
 */
class Actor: public ErrorListener {
public:
	/**
	 * Interface function for the actors.
	 *
	 * The actor gets a critic for a given state action pair. Then he has
	 * to adopt his policy according to the critic.
	 */
	virtual void receiveError(RealType critic, StateCollection *oldState,
	        Action *Action, ActionData *data = nullptr) = 0;

	RealType getLearningRate();
	void setLearningRate(RealType learningRate);

public:
	Actor();
	virtual ~Actor() = default;
};

/**
 * @class ActorFromQFunction
 * Actor who creates his Policy on a Q Function.
 *
 * The ActorFromQFunction updates it's Q-Function on the particular state
 * action pair according to the critic he got for that state action pair.
 * Since we are using a Q-Function the actor from Q-Function uses QETraces
 * to boost learning. The policy from the actor is usually a Softmax Policy
 * using the Q-Function, this Policy must be created by the user exclusively.
 *
 * The Q-Function update for this actor is Q(s,a)_new = Q(s,a)_old + beta * td,
 * where td is the value coming from the critic.
 *
 * @see ActorFromQFunctionAndPolicy.
 */
class ActorFromQFunction: public Actor, public SemiMDPListener {
protected:
	/// The Q Function of the actor
	AbstractQFunction *qFunction;
	/// The Etraces used for the QFunction
	AbstractQETraces *eTraces;

public:
	/**
	 * Updates the Q-Function.
	 *
	 * The actor first updates the Etraces (i.e. multiply all ETraces with
	 * gamma*lambda and then adds the state to the ETraces). Then the
	 * Q-Function is updated by the Etraces Object with the value beta * critic.
	 *
	 * @see QETraces
	 */
	virtual void receiveError(RealType critic, StateCollection *oldState,
	        Action *Action, ActionData *data = nullptr);
	//! Returns the used Q-Function.
	AbstractQFunction *getQFunction();
	//! Returns the used ETraces.
	AbstractQETraces *getETraces();

	//! Resets etraces object.
	virtual void newEpisode();

public:
	ActorFromQFunction& operator=(const ActorFromQFunction&) = delete;
	ActorFromQFunction(const ActorFromQFunction&) = delete;

	//! Creates an Actor using the specified Q-Function to adopt his Policy.
	ActorFromQFunction(AbstractQFunction *qFunction);
	virtual ~ActorFromQFunction();
};

/**
 * Actor which uses a QFunction and his Policy for the update.
 *
 * The only difference to ActorFromQFunction is the update of the Q-Function.
 * The update is Q(s_t,a_t)_new = Q(s_t,a_t)_old + beta * td *
 * (1 - pi_(s_t, a_t)), where pi(s_t, a_t) is the softmax-policy from
 * the actor. This method is recommended by Sutton and Barto.
 */
class ActorFromQFunctionAndPolicy: public ActorFromQFunction {
protected:
	StochasticPolicy *policy;
	RealType *actionValues;

public:
	/**
	 * Updates the Q-Function.
	 *
	 * Does the following update: Q(s_t,a_t)_new = Q(s_t,a_t)_old +
	 * beta * td * (1 - pi(s_t, a_t)).
	 */
	virtual void receiveError(RealType critic, StateCollection *state,
	        Action *Action, ActionData *data = nullptr);

	StochasticPolicy *getPolicy();

public:
	ActorFromQFunctionAndPolicy& operator=(const ActorFromQFunctionAndPolicy&) = delete;
	ActorFromQFunctionAndPolicy(const ActorFromQFunctionAndPolicy&) = delete;

	//! Creates the actor object, the policy has to choose the actions
	//! using the specified Q-Function.
	ActorFromQFunctionAndPolicy(AbstractQFunction *qFunction,
	        StochasticPolicy *policy);
	virtual ~ActorFromQFunctionAndPolicy();
};

/**
 * @class ActorFromActionValue
 * Actor class which can only decide between 2 different action, depending
 * on the action value of the current state.
 *
 * This is the implementation of the simple Actor-Critic Algorithm used by
 * Barto, Sutton, and Anderson in their cart pole example. The actor can only
 * decide between 2 actions. Which action is taken depends on the action value
 * of the current state. If this value is negative, the first action is more
 * likely to be chosen and vice versa. The probability of choosing the first
 * action is calculated the following way : 1.0 / (1.0 + exp(actionvalue(s))).
 * The action weight value is represented by an V-Function, for updating
 * the V-Function an etrace object is used. The current state is added to
 * the etrace with a positive factor if the second action was chosen,
 * otherwise with a negative factor. When a new episode begins, the etraces
 * are reset. This kind of algorithm usually need a very high learning rate,
 * for this class 1000.0 is the standard value for the "ActorLearningRate"
 * Parameter.
 *
 * This class directly implements the AgentController interface, so it can
 * be used as a controller.
 */
class ActorFromActionValue: public AgentController,
        public Actor,
        public SemiMDPListener
{
protected:
	AbstractVFunction *vFunction;
	AbstractVETraces *eTraces;

	//! A generator giving RealTypes from range [0, 1).
	BoundaryGenerator<RealType> _rand = {0, 1};

public:
	//! Adopt the action values according to the critic.
	virtual void receiveError(RealType critic, StateCollection *oldState,
	        Action *Action, ActionData *data = nullptr);

	virtual Action *getNextAction(StateCollection *state,
	        ActionDataSet *data = nullptr);
	//! Resets etraces object.
	virtual void newEpisode();

public:
	ActorFromActionValue& operator=(const ActorFromActionValue&) = delete;
	ActorFromActionValue(const ActorFromActionValue&) = delete;

	ActorFromActionValue(AbstractVFunction *vFunction, Action *action1,
	        Action *action2);
	virtual ~ActorFromActionValue();
};

class ActorFromContinuousActionGradientPolicy: public Actor,
        public SemiMDPListener {
protected:
	ContinuousActionGradientPolicy *gradientPolicy;
	GradientVETraces *gradientETraces;
	FeatureList *gradientFeatureList;

	ContinuousActionData *policyDifference;

public:
	virtual void receiveError(RealType critic, StateCollection *oldState,
	        Action *Action, ActionData *data = nullptr);
	virtual void newEpisode();

public:
	ActorFromContinuousActionGradientPolicy& operator=(const ActorFromContinuousActionGradientPolicy&) = delete;
	ActorFromContinuousActionGradientPolicy(const ActorFromContinuousActionGradientPolicy&) = delete;

	ActorFromContinuousActionGradientPolicy(
	        ContinuousActionGradientPolicy *gradientPolicy);
	virtual ~ActorFromContinuousActionGradientPolicy();
};

class ActorForMultipleAgents: public Actor, public AgentController {
protected:
	std::list<Actor *> *actors;
	std::list<AgentController *> *actionSets;
	unsigned int numActions;

public:
	void addActor(Actor *actor, AgentController *policy);

	virtual void receiveError(RealType critic, StateCollection *state,
	        Action *action, ActionData *data);

	virtual Action* getNextAction(StateCollection *state,
	        ActionDataSet *dataset);

public:
	ActorForMultipleAgents& operator=(const ActorForMultipleAgents&) = delete;
	ActorForMultipleAgents(const ActorForMultipleAgents&) = delete;

	ActorForMultipleAgents(ActionSet *actions);
	virtual ~ActorForMultipleAgents();
};

} //namespace rlearner

#endif //Rlearner_cactorcritic_H_
