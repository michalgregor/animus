// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "treebatchlearning.h"
#include "forest.h"
#include "rbftrees.h"
#include "localregression.h"
#include "modeltree.h"
#include "batchlearning.h"
#include "statecollection.h"
#include "treevfunction.h"
#include "rewardmodel.h"
#include "kdtrees.h"
#include "nearestneighbor.h"

namespace rlearner {

ExtraRegressionForestTrainer::ExtraRegressionForestTrainer(
    int numTrees, int K, int n_min, RealType treshold
) {
	addParameter("NumRegressionTrees", numTrees);
	addParameter("ExtraTreesK", K);
	addParameter("ExtraTreesMaxSamplesPerLeaf", n_min);
	addParameter("ExtraTreesMaxOutputVarianceThreshold", treshold);
}

ExtraRegressionForestTrainer::~ExtraRegressionForestTrainer() {}

RegressionForest *ExtraRegressionForestTrainer::getNewTree(
    DataSet *input, DataSet1D *output, DataSet1D *weightData
) {
	int numTrees = (int) getParameter("NumRegressionTrees");
	int K = (int) getParameter("ExtraTreesK");
	int n_min = (int) getParameter("ExtraTreesMaxSamplesPerLeaf");
	RealType treshold = getParameter("ExtraTreesMaxOutputVarianceThreshold");

	printf("Training Forest (%d trees, %f, %f) with %d (%d) Inputs...\n",
	    numTrees, output->getMean(nullptr), output->getVariance(nullptr),
	    input->size(), output->size());
	ExtraTreeRegressionForest *extraTree = new ExtraTreeRegressionForest(
	    numTrees, input, output, K, n_min, treshold, weightData);

	printf("Average Depth : %f, NumLeaves: %f\n", extraTree->getAverageDepth(),
	    extraTree->getAverageNumLeaves());

	return extraTree;
}

ExtraRegressionForestLearner::ExtraRegressionForestLearner(
    RegressionTreeFunction *l_treeFunction, int numTrees, int K, int n_min,
    RealType treshold
):
	ExtraRegressionForestTrainer(numTrees, K, n_min, treshold)
{
	treeFunction = l_treeFunction;
}

ExtraRegressionForestLearner::~ExtraRegressionForestLearner() {}

void ExtraRegressionForestLearner::learnFA(
    DataSet *inputData, DataSet1D *outputData
) {
	learnWeightedFA(inputData, outputData, nullptr);
}

void ExtraRegressionForestLearner::learnWeightedFA(
    DataSet *inputData, DataSet1D *outputData, DataSet1D *weightData
) {
	Mapping<RealType> *tree = treeFunction->getTree();

	if(tree != nullptr) {
		delete tree;
	}

	RegressionForest *extraTree = getNewTree(inputData, outputData, weightData);
	treeFunction->setTree(extraTree);
}

void ExtraRegressionForestLearner::resetLearner() {
	/*	Mapping<RealType> *tree = treeFunction->getTree();

	 if (tree != nullptr)
	 {
	 delete tree;
	 }
	 treeFunction->setTree(nullptr);*/
}

/*
 CExtraRegressionForestQFunctionLearner::CExtraRegressionForestQFunctionLearner(std::map<Action *, RegressionTreeFunction *> *l_functionMap, int numTrees, int K, int n_min, RealType treshold) : ExtraRegressionForestTrainer( numTrees, K, n_min, treshold)
 {
 functionMap = l_functionMap;
 }

 CExtraRegressionForestQFunctionLearner::~CExtraRegressionForestQFunctionLearner()
 {
 }

 void CExtraRegressionForestQFunctionLearner::learnQFunction(Action *action, DataSet *inputData, DataSet1D *outputData)
 {
 RegressionTreeFunction *treeFunction = (*functionMap)[action];
 Mapping<RealType> *tree = treeFunction->getTree();

 if (tree != nullptr)
 {
 delete tree;
 }
 RegressionForest *extraTree = getNewTree(inputData, outputData);
 treeFunction->setTree( extraTree);
 }

 ExtraRegressionForestFeatureLearner::ExtraRegressionForestFeatureLearner(StateProperties *l_originalState, int numTrees, int K, int n_min, RealType treshold) : ExtraRegressionForestTrainer(numTrees, K, n_min, treshold)
 {
 originalState = l_originalState;
 }
 */

ExtraRegressionForestFeatureLearner::~ExtraRegressionForestFeatureLearner() {}

FeatureCalculator *ExtraRegressionForestFeatureLearner::getFeatureCalculator(
    FeatureVFunction *vFunction, DataSet *inputData, DataSet1D *outputData
) {
	RegressionForest *forest = getNewTree(inputData, outputData, nullptr);

	ForestFeatureCalculator<RealType> *featCalc = new ForestFeatureCalculator<
	    RealType>(forest);

	featCalc->setOriginalState(originalState);

	FeatureVRegressionTreeFunction *vRegFunction =
	    dynamic_cast<FeatureVRegressionTreeFunction *>(vFunction);

	vRegFunction->setForest(forest, featCalc);

	return featCalc;
}

RBFForestLearner::RBFForestLearner(
    RegressionTreeFunction *l_treeFunction, int numTrees, int kNN, int K,
    int n_min, RealType treshold, RealType varMult, RealType minVar
) {
	treeFunction = l_treeFunction;

	addParameter("NumRegressionTrees", numTrees);
	addParameter("NumNearestNeighbors", kNN);
	addParameter("ExtraTreesK", K);
	addParameter("ExtraTreesMaxSamplesPerLeaf", n_min);
	addParameter("ExtraTreesMaxOutputVarianceThreshold", treshold);
	addParameter("RBFTreeVarianceMultiplier", varMult);
	addParameter("RBFTreeMinVariance", minVar);
}

RBFForestLearner::~RBFForestLearner() {}

void RBFForestLearner::learnFA(DataSet *input, DataSet1D *output) {
	Mapping<RealType> *tree = treeFunction->getTree();

	if(tree != nullptr) {
		delete tree;
	}

	int numTrees = (int) getParameter("NumRegressionTrees");
	int K = (int) getParameter("ExtraTreesK");
	int kNN = (int) getParameter("NumNearestNeighbors");
	int n_min = (int) getParameter("ExtraTreesMaxSamplesPerLeaf");
	RealType treshold = getParameter("ExtraTreesMaxOutputVarianceThreshold");

	RealType varMult = getParameter("RBFTreeVarianceMultiplier");
	RealType minVar = getParameter("RBFTreeMinVariance");

	printf("Training Tree with %d Inputs...", input->size());

	ColumnVector varMultiplier(treeFunction->getNumDimensions());
	varMultiplier.setConstant(varMult);

	ColumnVector minVariance(treeFunction->getNumDimensions());
	minVariance.setConstant(minVar);

	RBFExtraRegressionForest *forest = new RBFExtraRegressionForest(numTrees,
	    kNN, input, output, K, n_min, treshold, &varMultiplier, &minVariance);

	treeFunction->setTree(forest);
	printf("Average Depth : %f, NumLeaves: %f\n", forest->getAverageDepth(),
	    forest->getAverageNumLeaves());
}

ExtraLinearRegressionModelForestLearner::ExtraLinearRegressionModelForestLearner(
    RegressionTreeFunction *l_treeFunction, int numTrees, int K, int n_min,
    RealType treshold, int t1, int t2, int t3
) {
	addParameter("NumRegressionTrees", numTrees);
	addParameter("ExtraTreesK", K);
	addParameter("ExtraTreesMaxSamplesPerLeaf", n_min);
	addParameter("ExtraTreesMaxOutputVarianceThreshold", treshold);

	addParameter("LinearRegressionModelThreshold1", t1);
	addParameter("LinearRegressionModelThreshold2", t2);
	addParameter("LinearRegressionModelThreshold3", t3);
	addParameter("LinearRegressionModelLambda", 0.01);

	treeFunction = l_treeFunction;
}

ExtraLinearRegressionModelForestLearner::~ExtraLinearRegressionModelForestLearner() {}

void ExtraLinearRegressionModelForestLearner::learnFA(
    DataSet *input, DataSet1D *output
) {
	Mapping<RealType> *tree = treeFunction->getTree();

	if(tree) {
		delete tree;
	}

	int numTrees = (int) getParameter("NumRegressionTrees");
	int K = (int) getParameter("ExtraTreesK");
	int n_min = (int) getParameter("ExtraTreesMaxSamplesPerLeaf");
	RealType treshold = getParameter("ExtraTreesMaxOutputVarianceThreshold");
	RealType lambda = getParameter("LinearRegressionModelLambda");

	int t1 = (int) getParameter("LinearRegressionModelThreshold1");
	int t2 = (int) getParameter("LinearRegressionModelThreshold2");
	int t3 = (int) getParameter("LinearRegressionModelThreshold3");

	printf("Training Model Tree with %d Inputs...", input->size());

	RegressionMultiMapping *multiMapping = new RegressionMultiMapping(numTrees,
	    input->getNumDimensions());
	multiMapping->deleteMappings = true;

	for(int i = 0; i < numTrees; i++) {
		multiMapping->addMapping(i,
		    new ExtraLinearRegressionModelTree(input, output, K, n_min,
		        treshold, t1, t2, t3, lambda));
	}

	printf("done\n");
	treeFunction->setTree(multiMapping);
}

LocalRBFLearner::LocalRBFLearner(
    RegressionTreeFunction *l_treeFunction, int kNN, RealType varMult
):	treeFunction(l_treeFunction),
	inputData(nullptr),
	outputData(nullptr),
	preprocessor(nullptr)
{
	addParameter("NumNearestNeighbors", kNN);
	addParameter("LocalRBFVarianceMultiplier", varMult);
}

LocalRBFLearner::~LocalRBFLearner() {
	Mapping<RealType> *tree = treeFunction->getTree();

	if(tree != nullptr) {
		delete tree;
	}

	if(inputData != nullptr) {
		delete inputData;
		delete outputData;
	}
}

void LocalRBFLearner::learnFA(DataSet *input, DataSet1D *output) {
	Mapping<RealType> *tree = treeFunction->getTree();

	if(tree != nullptr) {
		delete tree;
	}

	if(inputData != nullptr) {
		delete inputData;
		delete outputData;

		delete preprocessor;
	}

	inputData = new DataSet(*input);
	outputData = new DataSet1D(*output);
	preprocessor = new MeanStdPreprocessor(inputData);
	preprocessor->preprocessDataSet(inputData);

	int kNN = (int) getParameter("NumNearestNeighbors");
	RealType varMult = getParameter("LocalRBFVarianceMultiplier");

	ColumnVector sigma(treeFunction->getNumDimensions());
	sigma.setConstant(varMult);

	LocalRBFRegression *regression = new LocalRBFRegression(inputData,
	    outputData, kNN, &sigma);
	regression->setPreprocessor(preprocessor);

	printf("Finished Building KD Tree\n");
	treeFunction->setTree(regression);
}

LocalLinearLearner::LocalLinearLearner(
    RegressionTreeFunction *l_treeFunction, int kNN, int degree
):	treeFunction(l_treeFunction),
	inputData(nullptr),
	outputData(nullptr),
	preprocessor(nullptr)
{
	addParameter("NumNearestNeighbors", kNN);
	addParameter("LocalLinearRegressionDegree", degree);
	addParameter("LocalLinearRegressionLambda", 0.01);
}

LocalLinearLearner::~LocalLinearLearner() {
	Mapping<RealType> *tree = treeFunction->getTree();

	if(tree != nullptr) {
		delete tree;
	}

	if(inputData != nullptr) {
		delete inputData;
		delete outputData;
	}
}

void LocalLinearLearner::learnFA(DataSet *input, DataSet1D *output) {
	Mapping<RealType> *tree = treeFunction->getTree();

	if(tree != nullptr) {
		delete tree;
	}

	if(inputData != nullptr) {
		delete inputData;
		delete outputData;

		delete preprocessor;
	}

	inputData = new DataSet(*input);
	outputData = new DataSet1D(*output);
	preprocessor = new MeanStdPreprocessor(inputData);
	preprocessor->preprocessDataSet(inputData);

	int kNN = (int) getParameter("NumNearestNeighbors");

	int degree = (int) getParameter("LocalLinearRegressionDegree");
	RealType lambda = getParameter("LocalLinearRegressionLambda");

	LocalLinearRegression *regression = new LocalLinearRegression(inputData,
	    outputData, kNN, degree, lambda);
	regression->setPreprocessor(preprocessor);

	printf("Finished Building KD Tree\n");
	treeFunction->setTree(regression);
}

UnknownDataQFunction::UnknownDataQFunction(
    ActionSet *actions, EpisodeHistory *l_logger, StateProperties *l_properties,
    RealType factor) :
	AbstractQFunction(actions) {
	int kNN = 3;

	addParameter("UnknownDataFactor", factor);
	addParameter("UnknownDataNearestNeighbors", kNN);
	addParameter("UnknownDataVarianceMultiplier", 0.05);

	logger = l_logger;
	properties = l_properties;

	dataGenerator = nullptr;

	treeMap = new std::map<Action *, KDTree *>;
	nnMap = new std::map<Action *, KNearestNeighbors *>;
	preMap = new std::map<Action *, DataPreprocessor *>;

	distVector = new ColumnVector(kNN);

	recalculateTrees();
}

UnknownDataQFunction::~UnknownDataQFunction() {
	clearMaps();
	delete treeMap;
	delete nnMap;
	delete preMap;
	delete distVector;
}

void UnknownDataQFunction::clearMaps() {
	if(dataGenerator) {
		delete dataGenerator;

		ActionSet::iterator it = actions->begin();
		for(; it != actions->end(); it++) {
			delete (*treeMap)[*it];
			delete (*nnMap)[*it];
			delete (*preMap)[*it];
		}

		treeMap->clear();
		nnMap->clear();
		preMap->clear();
	}
}

void UnknownDataQFunction::onParametersChanged() {
	AbstractQFunction::onParametersChanged();

	delete distVector;
	distVector = new ColumnVector(
	    (int) getParameter("UnknownDataNearestNeighbors"));
}

RealType UnknownDataQFunction::getValue(
    StateCollection *stateCol, Action *action, ActionData *) {
	RealType varMult = getParameter("UnknownDataVarianceMultiplier");

	RealType punishment = getParameter("UnknownDataFactor");

	if(fabs(punishment) <= 0.0001) {
		return 0;
	}

	if(varMult < 0.000001) {
		return 0;
	}

	KNearestNeighbors *nearestN = (*nnMap)[action];
	DataSet *dataSet = dataGenerator->getInputData(action);

	State *state = stateCol->getState(properties);

	//cout << "State: " << state->t();	

	std::list<int> subset;

	int kNN = (int) getParameter("UnknownDataNearestNeighbors");

	ColumnVector distToState(dataSet->getNumDimensions());

	nearestN->getNearestNeighbors(state, &subset, kNN, distVector);

	if(distVector->operator[](0) > varMult) {
		return punishment;
	}

	return 0;

	/*RealType value = getUnknownDataValue(distVector);

	 return value * getParameter("UnknownDataFactor");*/
}

RealType UnknownDataQFunction::getUnknownDataValue(ColumnVector *distVector) {
	RealType varMult = getParameter("UnknownDataVarianceMultiplier");
	RealType value = 0.0;
	RealType factor = 1.0;

	for(int i = 0; i < distVector->rows(); i++) {
		if(distVector->operator[](i) < varMult / 2) {
			factor = factor / 4;
		}

		if(distVector->operator[](i) == 0.0) {
			break;
		}

		if(distVector->operator[](i) > varMult) {
			value += pow((distVector->operator[](i) - varMult) / varMult, 2.0)
			    * factor;
		} else {
			value += (distVector->operator[](i) - varMult) * factor * 0.2;
		}

		factor = factor * 0.25;
	}

	return value;
}

void UnknownDataQFunction::recalculateTrees() {
	clearMaps();

	dataGenerator = new BatchQDataGenerator(actions, properties);
	dataGenerator->generateInputData(logger);

	int kNN = (int) getParameter("UnknownDataNearestNeighbors");

	ActionSet::iterator it = actions->begin();
	for(; it != actions->end(); it++) {
		DataSet *dataSet = dataGenerator->getInputData(*it);

		DataPreprocessor *preProc = new MeanStdPreprocessor(dataSet);

		(*preMap)[*it] = preProc;
		preProc->preprocessDataSet(dataSet);

		(*treeMap)[*it] = new KDTree(dataSet, 1);
		(*treeMap)[*it]->setPreprocessor(preProc);

		(*nnMap)[*it] = new KNearestNeighbors((*treeMap)[*it], dataSet, kNN);
	}
}

void UnknownDataQFunction::resetData() {
	recalculateTrees();
}

UnknownDataQFunctionFromLocalRBFRegression::UnknownDataQFunctionFromLocalRBFRegression(
    ActionSet *actions,
    std::map<Action *, RegressionTreeVFunction *> *l_regressionMap,
    RealType factor) :
	AbstractQFunction(actions) {
	regressionMap = l_regressionMap;
	addParameter("UnknownDataFactor", factor);
	addParameter("UnknownDataNearestNeighbors", 3);

	recalculateFactors = false;
}

UnknownDataQFunctionFromLocalRBFRegression::~UnknownDataQFunctionFromLocalRBFRegression() {}

RealType UnknownDataQFunctionFromLocalRBFRegression::getValue(
    StateCollection *stateCol, Action *action, ActionData *) {
	RegressionTreeVFunction *vFunction = (*regressionMap)[action];
	State *state = stateCol->getState(vFunction->getStateProperties());

	RealType unknownDataValue = 0;
	int unknownNNs = (int) getParameter("UnknownDataNearestNeighbors");
	RealType factor = getParameter("UnknownDataFactor");

	if(vFunction->getTree() != nullptr) {
		LocalRBFRegression *localRegression =
		    dynamic_cast<LocalRBFRegression *>(vFunction->getTree());

		if(recalculateFactors) {
			localRegression->getOutputValue(state);
		}
		ColumnVector *factors = localRegression->getLastRBFFactors();

		for(int i = 0; i < unknownNNs; i++) {
			unknownDataValue += (1 - factors->operator[](i));
		}
	}
	return unknownDataValue * factor;
}

} //namespace rlearner
