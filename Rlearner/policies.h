// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cpolicies_H_
#define Rlearner_cpolicies_H_

#include "algebra.h"
#include "agentcontroller.h"
#include "parameters.h"

namespace rlearner {

class AbstractFeatureStochasticEstimatedModel;
class TransitionFunction;
class AbstractQFunction;
class ActionSet;
class FeatureList;
class ActionStatistics;
class AbstractVFunction;
class QFunctionFromTransitionFunction;
class StateCollectionImpl;

/**
 * @class QGreedyPolicy
 * Greedy Policy based on a Q-Function.
 *
 * This policy always takes the greedy action (action with the highest Q-Value).
 * The policy can't be used as stochastic policy, if a stochastic greedy policy
 * is needed take QStochasticPolicy with a greedy distribution.
 */
class QGreedyPolicy: public AgentController {
protected:
	AbstractQFunction *qFunction;
	ActionSet *availableActions;

public:
	//! Always returns the greedy action
	virtual Action *getNextAction(StateCollection *state,
	        ActionDataSet *data = nullptr);

public:
	QGreedyPolicy& operator=(const QGreedyPolicy&) = delete;
	QGreedyPolicy(const QGreedyPolicy&) = delete;

	QGreedyPolicy(ActionSet *actions, AbstractQFunction *qFunction);
	virtual ~QGreedyPolicy();
};

/**
 * @class ActionDistribution
 * Action Distribution classes define the distributions of stochastic Policies.
 *
 * Action Distribution calculate the distribution for sampling an action, which
 * is done by the class StochasticPolicy. The distribution calculation usually
 * depends on some kind of Q-Value of the actions. This is done in the function
 * getDistribution. The function gets as input the current state, all available
 * actions, and the Q-Values (actually it can be any kind of value, rating
 * an action) of the actions as a RealType array. Usually only this Q-Values are
 * used for the distribution (the state is only used for special exploration
 * policies). The function has to overwrite the Q-Values RealType array with
 * the distribution values. Additionally some algorithm need a differentiable
 * distribution. Therefore the interface provides the function isDifferentiable
 * (since not all distributions are differentiable) and the function
 * getGradientFactors. The function calculates the gradient
 * dP(usedaction|actionFactors)/ (d_actionfactors). The actionfactors
 * are again some kind of rating for the actions. The result has to be written
 * in the output vector gradientfactors. This vector has always the same size
 * as the actionfactors array (so the number of actions). Only the SoftMax
 * Distribution supports calculating this gradient.
 */
class ActionDistribution: virtual public ParameterObject {
public:
	/**
	 * Returns the distribution of the actions that is sampled by a
	 * stochastic policy.
	 *
	 * The function gets as input the current state, all available actions,
	 * and the Q-Values (actually it can be any kind of value, rating
	 * an action) of the actions as a RealType array. Usually only this Q-Values
	 * are used for the distribution (the state is only used for special
	 * exploration policies). The function has to overwrite the Q-Values
	 * in RealType array with the distribution values.
	 */
	virtual void getDistribution(StateCollection *state,
	        ActionSet *availableActions, RealType *actionFactors) = 0;
	virtual bool isDifferentiable() {
		return false;
	}

	/**
	 * Calculates the derivation of the probability of choosing
	 * the specified action.
	 *
	 * The function calculates the gradient
	 * dP(usedaction|actionFactors)/ (d_actionfactors). The actionfactors are
	 * again some kind of rating for the actions. The result has to be written
	 * in the output vector gradientfactors. This vector has always the same
	 * size as the actionfactors array (so the number of actions). Only the
	 * SoftMax Distribution supports calculating this gradient.
	 */
	virtual void getGradientFactors(StateCollection *state,
	        Action *usedAction, ActionSet *actions, RealType *actionFactors,
	        ColumnVector *gradientFactors);

public:
	virtual ~ActionDistribution() = default;
};

/**
 * @class SoftMaxDistribution
 * Soft Max Distribution for Stochastic Policies.
 *
 * This class implements the well known softmax distribution (sometimes called
 * Gibbs distribution). The Softmax Distribution is differentiable and
 * therefore can be used for policy gradient algorithms. The Distribution
 * depends on the parameter "SoftMaxBeta" which specifies you the "greediness"
 * of your distribution.
 *
 * The class SoftMaxDistribution has the following Parameters:
 * - "SoftMaxBeta": Greediness of the distribution.
 */
class SoftMaxDistribution: public ActionDistribution {
public:
	virtual void getDistribution(StateCollection *state,
	        ActionSet *availableActions, RealType *values);

	virtual bool isDifferentiable() {
		return true;
	}

	virtual void getGradientFactors(StateCollection *state,
	        Action *usedAction, ActionSet *actions, RealType *actionFactors,
	        ColumnVector *gradientFactors);

public:
	SoftMaxDistribution(RealType beta);
	virtual ~SoftMaxDistribution() = default;
};

class AbsoluteSoftMaxDistribution: public ActionDistribution {
public:
	virtual void getDistribution(StateCollection *state,
	        ActionSet *availableActions, RealType *values);

	virtual bool isDifferentiable() {
		return false;
	}

public:
	AbsoluteSoftMaxDistribution(RealType maxAbsValue);
	virtual ~AbsoluteSoftMaxDistribution() = default;
};

/**
 * @class GreedyDistribution
 * Class for a greedy action distribution.
 *
 * This class implements a greedy action distribution, so the probability for
 * the best rated action is always 1, and for the rest 0. It's
 * understood that this distribution is not differentiable.
 *
 * If there are more than one greedy action, the first action will be taken.
 * If you want to assign all of these equal probabilities instead, use
 * setMultiGreedy(true).
 */
class GreedyDistribution: public ActionDistribution {
private:
	/**
	 * If set to false (default), when there are more than one greedy action,
	 * the first one is taken. If set to true, the actions are assigned equal
	 * probabilities instead.
	 */
	bool _multiGreedy = false;

public:
	virtual void getDistribution(StateCollection *state,
	        ActionSet *availableActions, RealType *values);

public:
	/**
	 * Returns the multi-greedy flag.
	 *
	 * \copydoc _multiGreedy
	 */
	bool getMultiGreedy() const {
		return _multiGreedy;
	}

	/**
	 * Sets the multi-greedy flag.
	 *
	 * \copydoc _multiGreedy
	 */
	void setMultiGreedy(bool multiGreedy) {
		_multiGreedy = multiGreedy;
	}

public:
	GreedyDistribution(bool multiGreedy = false): _multiGreedy(multiGreedy) {}
	virtual ~GreedyDistribution() = default;
};

/**
 * @class EpsilonGreedyDistribution
 * Class for the epsilon greedy action distribution.
 *
 * This class implements the epsilon greedy action distribution. Epsilon greedy
 * policies take the greedy (best rated) action with probability (1 - epsilon)
 * and a random action with probability epsilon. To set epsilon please
 * use the parameter "EpsilonGreedy" or the constructor of the class. Its
 * understood that this distribution is not differentiable.
 *
 * If there are more than one greedy action, the probability will be distributed
 * among them uniformly. If you want to assign it to the first action instead,
 * use setMultiGreedy(false).
 */
class EpsilonGreedyDistribution: public ActionDistribution {
private:
	/**
	 * If set to true (default), the actions are assigned equal probabilities
	 * instead. If set to false, when there are more than one greedy action,
	 * the first one is taken.
	 */
	bool _multiGreedy = true;

public:
	virtual void getDistribution(StateCollection *state,
	        ActionSet *availableActions, RealType *values);

public:
	/**
	 * Returns the multi-greedy flag.
	 *
	 * \copydoc _multiGreedy
	 */
	bool getMultiGreedy() const {
		return _multiGreedy;
	}

	/**
	 * Sets the multi-greedy flag.
	 *
	 * \copydoc _multiGreedy
	 */
	void setMultiGreedy(bool multiGreedy) {
		_multiGreedy = multiGreedy;
	}

public:
	EpsilonGreedyDistribution(RealType epsilon, bool multiGreedy = true);
	virtual ~EpsilonGreedyDistribution() = default;
};

/**
 * @class StochasticPolicy
 * Class modelling a stochastic policy.
 *
 * Many algorithms need more than just a specific action for a specific state,
 * especially when the policy is a stochastic policy very often the distribution
 * for choosing an action is needed. This is modelled by StochasticPolicy.
 * The Policy chooses an action according to a given probability distribution,
 * you can specify this distribution in the constructor with the
 * ActionDistribution object. In the getNextAction Method an action is chosen
 * according the distribution returned by getActionProbabilities.
 * The getActionProbabilities method has to call the getDistribution method
 * from the ActionDistribution object with the action rating as input. How
 * this action rating is calculated has to be implemented by the subclasses,
 * usually the values comes from a Q-Function (see QStochasticPolicy). Some
 * algorithms like the policy gradient algorithm need a differentiable action
 * distribution. StochasticPolicy also provides an interface for differentiate
 * your distribution with respect to the policy weights
 * (weights of the Q-Function).
 *
 * The gradient calculation of the policy is already implemented. You have the
 * possibility to calculate dP(action| state)/ dweights or the logarithmic
 * gradient which is the same as
 * dP(action| state)/ dweights * 1 / P(action | state). Calculating
 * the gradient of the action ratings (e.g. dQ(a,s)/dw for QFunctions)
 * has to be implemented in the function getActionGradient if the stochastic
 * policy is supposed to be differentiable. Differentiable policies also have
 * to overwrite the function isDifferentiable, which always returns false for
 * the base class. Wether the policy is differentiable or not depends on the
 * kind of action ratings and on the distribution. Both of them have to be
 * differentiable. The class also provides the possibility to get a statistics
 * object for the action which was chosen. This is done by the virtual function
 * getActionStatistics, which is called by the getNextAction Function if an
 * statistics object is requested.
 */
class StochasticPolicy: public AgentStatisticController {
protected:
	//! Array to store the current action pronabilites.
	RealType *actionValues;
	ActionDistribution *distribution;

	ColumnVector *gradientFactors;
	FeatureList *actionGradientFeatures;
	ActionSet *availableActions;

	/**
	 * Virtual function for getting the action statistic for the chosen action.
	 *
	 * The class also provides the possibility to get a statistics object for
	 * the action which was chosen. This is done by the virtual function
	 * getActionStatistics, which is called by the getNextAction Function if
	 * an statistics object is requested.
	 */
	virtual void getActionStatistics(StateCollection *, Action *,
	        ActionStatistics *) {}

public:
	/**
	 * Virtual function for retrieving the action probability distribution.
	 *
	 * For each action in the availableActions action set, the function has
	 * to calculate the probability and write it in the RealType array
	 * actionValues. The function first calculates the action ratings with
	 * the function getNextAction and then calculates the action distribution
	 * with the action distribution object.
	 */
	virtual void getActionProbabilities(StateCollection *state,
	        ActionSet *availableActions, RealType *actionValues,
	        ActionDataSet *actionDataSet = nullptr);

	/**
	 * Chooses an action according the distribution from getActionPropability.
	 *
	 * First of all the available actions for the current state are calculated,
	 * and then the probabilities for this available actions. Then an action
	 * is chosen from the available actions set according the distribution.
	 */
	virtual Action *getNextAction(StateCollection *state,
	        ActionDataSet *dataset, ActionStatistics *stat);

	//! Interface function for calculating the action ratings, has to be
	//! implemented by the subclasses.
	virtual void getActionValues(StateCollection *state,
	        ActionSet *availableActions, RealType *actionValues,
	        ActionDataSet *actionDataSet = nullptr) = 0;

	virtual bool isDifferentiable() {
		return false;
	}

	virtual void getActionProbabilityGradient(StateCollection *state,
	        Action *action, ActionData *data, FeatureList *gradientState);
	virtual void getActionProbabilityLnGradient(StateCollection *state,
	        Action *action, ActionData *data, FeatureList *gradientState);

	/**
	 * Interface function for calculating the derivative of an action factor.
	 *
	 * The function has to calculate d_actionratings(action)/dw, which is for
	 * example dQ(s,a)/dw.
	 */
	virtual void getActionGradient(StateCollection *state, Action *action,
	        ActionData *data, FeatureList *gradientState);

public:
	StochasticPolicy& operator=(const StochasticPolicy&) = delete;
	StochasticPolicy(const StochasticPolicy&) = delete;

	//! Creates a stochastic policy which can choose from the actions
	//! in "actions".
	StochasticPolicy(ActionSet *actions, ActionDistribution *distribution);
	virtual ~StochasticPolicy();
};

/**
 * @class QStochasticPolicy
 * Stochastic Policy which computes its probabilities from the Q-Values of
 *
 * This stochastic policy calculates its action ratings according to the given
 * Q-Function. The getActionValues function writes the Q-Values in the
 * actionFactors array. The Q-Stochastic Policies also support gradient
 * calculation. The policy is differentiable, if the distribution and the
 * Q-Function are differentiable. The gradient d_actionratings(action) / dw
 * calculated in the function getActionGradient is the same as dQ(s,a)/dw.
 */
class QStochasticPolicy: public StochasticPolicy {
protected:
	//! QFunction of the policy, needed for action decision.
	AbstractQFunction *qfunction;

protected:
	//! Returns the action statistics object from the q-function.
	virtual void getActionStatistics(StateCollection *state, Action *action,
	        ActionStatistics *stat);

public:
	virtual void getActionValues(StateCollection *state,
	        ActionSet *availableActions, RealType *actionValues,
	        ActionDataSet *actionDataSet = nullptr);

	virtual void getActionGradient(StateCollection *state, Action *action,
	        ActionData *data, FeatureList *gradientState);
	virtual bool isDifferentiable();

	virtual AbstractQFunction *getQFunction() {
		return qfunction;
	}

public:
	QStochasticPolicy& operator=(const QStochasticPolicy&) = delete;
	QStochasticPolicy(const QStochasticPolicy&) = delete;

	QStochasticPolicy(ActionSet *actions, ActionDistribution *distribution,
	        AbstractQFunction *qfunction);
	virtual ~QStochasticPolicy();
};

class QFunctionFromTransitionFunction;

/**
 * @class VMStochasticPolicy
 * Stochastic Policy which calculates its action from a Dynamic Model
 * and a V-Function.
 *
 * The policy calculates its action ratings with a 1 or more step forward view
 * using the dynamic model. For every action the successor state s' is
 * calculated and then the value of that state is determined. The action rating
 * of that action is then: Q(s,a) = R(s,a,s') + gamma * V(s'). This calculation
 * is done by an own Q-Function class QFunctionFromTransitionFunction. This
 * Q-Function also supports a larger search deep than 1, the search deep can be
 * set with the parameter "SearchDepth". Be aware that large search deeps ( > 3)
 * have large (exponentially growing) computational costs.
 *
 * The policy is differentiable if the distribution is differentiable and the
 * V-Function is differentiable. The action rating gradient can be calculated
 * easily since the reward doesn't depend on the weights, the derivative of
 * action a is just dV(s')/dw, where s'is the successor state when taking
 * action a.
 *
 * VMStochasticPolicy has the following parameters:
 * - Inherits all Parameters from the action distribution.
 * - "SearchDepth": number of forward search steps in the value function.
 * - "DiscountFactor": gamma.
 */
class VMStochasticPolicy: public QStochasticPolicy {
protected:
	StateCollectionImpl* _nextState;
	StateCollectionImpl* _intermediateState;

	AbstractVFunction* _vFunction;
	TransitionFunction* _model;
	RewardFunction* _reward;

public:
	virtual void getActionGradient(StateCollection *state, Action *action,
	        ActionData *data, FeatureList *gradientState);
	virtual bool isDifferentiable();

public:
	VMStochasticPolicy& operator=(const VMStochasticPolicy&) = delete;
	VMStochasticPolicy(const VMStochasticPolicy&) = delete;

	VMStochasticPolicy(ActionSet *actions, ActionDistribution *distribution,
	        AbstractVFunction *vFunction, TransitionFunction *model,
	        RewardFunction *reward,
	        const std::set<StateModifierList*>& modifierLists
	);

	virtual ~VMStochasticPolicy();
};

} //namespace rlearner

#endif //Rlearner_cpolicies_H__
