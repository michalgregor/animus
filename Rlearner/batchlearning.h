// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cbatchlearning_H_
#define Rlearner_cbatchlearning_H_

#include "agentcontroller.h"
#include "parameters.h"
#include "agent.h"
#include "algebra.h"

namespace rlearner {

class RewardHistory;
class EpisodeHistory;
class RewardLogger;
class AgentLogger;
class AgentSimulator;

class ContinuousActionQFunction;
class PolicySameStateEvaluator;
class GradientUpdateFunction;
class SemiMDPListener;
class LSTDLambda;
class SemiMDPRewardListener;

class FeatureCalculator;
class FeatureVFunction;
class DataSet;
class DataSet1D;
class DataSubset;

class AbstractVFunction;
class GradientVFunction;
class QFunction;

class SupervisedLearner;
class SupervisedWeightedLearner;
class SupervisedQFunctionLearner;
class SupervisedQFunctionWeightedLearner;

class StochasticPolicy;

class LearnDataObject;

class ContinuousActionQFunction;
class StateProperties;

class KDTree;
class KNearestNeighbors;
class DataPreprocessor;

class SamplingBasedTransitionModel;
class SamplingBasedGraph;
class ActionDistribution;

class Action;
class ActionSet;

class State;
class StateCollection;
class StateProperties;

class Step;
class NewFeatureCalculator;
class AbstractQFunction;

class BatchLearningPolicy: public DeterministicController {
protected:
	ActionDataSet *actionDataSet;
	Action *nextAction;

public:
	virtual Action *getNextAction(StateCollection *state,
	        ActionDataSet *actionData = nullptr);

	virtual void setAction(Action *action, ActionData *data);

public:
	BatchLearningPolicy(ActionSet *actions);
	virtual ~BatchLearningPolicy();
};

class PolicyEvaluation: public ParameterObject {
public:
	virtual void evaluatePolicy();
	virtual void evaluatePolicy(int numEvaluations) = 0;

	virtual void resetLearnData() {}

public:
	PolicyEvaluation(int maxEvaluations = 100);
	virtual ~PolicyEvaluation();
};

class PolicyEvaluationGradientFunction: public PolicyEvaluation {
protected:
	GradientUpdateFunction *learnData;
	RealType *oldWeights;

	virtual RealType getWeightDifference(RealType *oldWeights);

public:
	bool resetData;

	virtual void evaluatePolicy(int numEvaluations) = 0;
	virtual void resetLearnData();

public:
	PolicyEvaluationGradientFunction(GradientUpdateFunction *learnData,
	        RealType treshold = 0.1, int maxEvaluations = 100);

	virtual ~PolicyEvaluationGradientFunction();
};

class OnlinePolicyEvaluation: public PolicyEvaluationGradientFunction {
protected:
	AgentSimulator* agentSimulator;
	SemiMDPListener *learner;
	SemiMDPSender *semiMDPSender;

public:
	virtual void evaluatePolicy(int numEvaluations);
	void setSemiMDPSender(SemiMDPSender *semiMDPSender);

public:
	OnlinePolicyEvaluation(AgentSimulator* agentSimulator, SemiMDPListener *learner,
	        GradientUpdateFunction *learnData, int maxEvaluationEpisodes,
	        int numSteps, int checkWeightsPerEpisode);

	virtual ~OnlinePolicyEvaluation();
};

class LSTDOnlinePolicyEvaluation: public OnlinePolicyEvaluation {
protected:
	LSTDLambda *lstdLearner;

	virtual RealType getWeightDifference(RealType *oldWeights);

public:
	virtual void resetLearnData();

public:
	LSTDOnlinePolicyEvaluation(AgentSimulator* agentSimulator, LSTDLambda *learner,
	        GradientUpdateFunction *learnData, int maxEvaluationSteps,
	        int nSteps);

	virtual ~LSTDOnlinePolicyEvaluation();
};

class OfflineEpisodePolicyEvaluation:
	public PolicyEvaluationGradientFunction,
	public SemiMDPSender
{
protected:
	SemiMDPRewardListener *learner;
	EpisodeHistory *episodeHistory;
	RewardHistory *rewardLogger;

	std::list<StateModifier *> *modifiers;

	BatchLearningPolicy *policy;

public:
	void setBatchLearningPolicy(BatchLearningPolicy *l_policy);
	virtual void evaluatePolicy(int numEvaluations);

public:
	OfflineEpisodePolicyEvaluation(EpisodeHistory *episodeHistory,
	        SemiMDPRewardListener *learner, GradientUpdateFunction *learnData,
	        std::list<StateModifier *> *l_modifiers,
	        int maxEvaluationEpisodes);

	OfflineEpisodePolicyEvaluation(EpisodeHistory *episodeHistory,
	        RewardHistory *rewardLogger, SemiMDPRewardListener *learner,
	        GradientUpdateFunction *learnData,
	        std::list<StateModifier *> *l_modifiers,
	        int maxEvaluationEpisodes);

	virtual ~OfflineEpisodePolicyEvaluation();
};

class LSTDOfflineEpisodePolicyEvaluation: public OfflineEpisodePolicyEvaluation {
protected:
	LSTDLambda *lstdLearner;

	virtual RealType getWeightDifference(RealType *oldWeights);

public:
	virtual void resetLearnData();

public:
	LSTDOfflineEpisodePolicyEvaluation(EpisodeHistory *episodeHistory,
	        RewardHistory *rewardLogger, LSTDLambda *learner,
	        GradientVFunction *learnData,
	        std::list<StateModifier *> *l_modifiers, int episodes);

	virtual ~LSTDOfflineEpisodePolicyEvaluation();
};

class DataCollector: public ParameterObject {
public:
	virtual void collectData() = 0;

public:
	DataCollector();
	virtual ~DataCollector();
};

class UnknownDataQFunction;

class DataCollectorFromAgentLogger: public DataCollector {
protected:
	AgentLogger *logger;
	RewardLogger *rewardLogger;
	AgentSimulator* agentSimulator;

	int numCollections;
	SemiMarkovDecisionProcess *sender;

	std::list<UnknownDataQFunction *> *unknownDataQFunctions;
	AgentController *controller;

public:
	virtual void collectData();

	virtual void setController(AgentController *controller);
	virtual void addUnknownDataFunction(
	        UnknownDataQFunction *unknownDataQFunctions);

	void setSemiMDPSender(SemiMarkovDecisionProcess *sender);

public:
	DataCollectorFromAgentLogger(AgentSimulator* agentSimulator, AgentLogger *logger,
	        RewardLogger *rewardLogger, int numEpisodes, int numSteps);

	virtual ~DataCollectorFromAgentLogger();
};

class PolicyIteration: virtual public ParameterObject {
protected:
	LearnDataObject *policyFunction;
	LearnDataObject *evaluationFunction;

	PolicyEvaluation *evaluation;
	DataCollector *collector;

public:
	virtual void doPolicyIterationStep();
	virtual void initPolicyIteration();

public:
	PolicyIteration(LearnDataObject *policyFunction,
	        LearnDataObject *evaluationFunction, PolicyEvaluation *evaluation,
	        DataCollector *collector = nullptr);

	virtual ~PolicyIteration();
};

class NewFeatureCalculatorDataGenerator {
public:
	virtual void initFeatures() {
		calculateNewFeatures();
	}

	virtual void calculateNewFeatures() = 0;

	virtual void swapValueFunctions() = 0;

	virtual void resetData() {}

public:
	virtual ~NewFeatureCalculatorDataGenerator() {}
};

class PolicyIterationNewFeatures: public PolicyIteration {
protected:
	NewFeatureCalculatorDataGenerator *newFeatureCalculator;

public:
	virtual void doPolicyIterationStep();
	virtual void initPolicyIteration();

public:
	PolicyIterationNewFeatures(
		LearnDataObject *policyFunction,
	    LearnDataObject *evaluationFunction,
	    PolicyEvaluation *evaluation,
	    NewFeatureCalculatorDataGenerator *newFeatureCalculator,
	    DataCollector *collector = nullptr
	);

	virtual ~PolicyIterationNewFeatures();
};

class BatchDataGenerator: public ParameterObject {
public:
	virtual void addInput(StateCollection *state, Action *action,
	        RealType output, RealType weighting = 1.0) = 0;

	virtual void trainFA() = 0;
	virtual void resetPolicyEvaluation() = 0;

	virtual RealType getValue(StateCollection *state, Action *action) = 0;

	void generateInputData(EpisodeHistory *logger);

public:
	BatchDataGenerator() {}
	virtual ~BatchDataGenerator() {}
};

class BatchVDataGenerator: public BatchDataGenerator {
protected:
	DataSet *inputData;
	DataSet1D *outputData;
	DataSet1D *weightingData;
	ColumnVector *buffVector;

	AbstractVFunction *vFunction;

	SupervisedLearner *learner;
	SupervisedWeightedLearner *weightedLearner;

	BatchVDataGenerator(SupervisedLearner *learner, int inputDim);

public:
	virtual void init(int numDim);

	virtual void addInput(StateCollection *state, Action *action,
	        RealType output, RealType weighting = 1.0);

	virtual void trainFA();
	virtual void resetPolicyEvaluation();

	virtual RealType getValue(StateCollection *state, Action *action);

	virtual DataSet *getInputData();
	virtual DataSet1D *getOutputData();
	virtual DataSet1D *getWeighting();

public:
	BatchVDataGenerator(AbstractVFunction *vFunction,
	        SupervisedLearner *learner);

	BatchVDataGenerator(AbstractVFunction *vFunction,
	        SupervisedWeightedLearner *learner);

	virtual ~BatchVDataGenerator();
};

class BatchCAQDataGenerator: public BatchVDataGenerator {
protected:
	ContinuousActionQFunction *qFunction;
	StateProperties *properties;

public:
	virtual void addInput(StateCollection *state, Action *action,
	        RealType output);

	virtual RealType getValue(StateCollection *state, Action *action);

public:
	BatchCAQDataGenerator(StateProperties *properties,
	        ContinuousActionQFunction *qFunction, SupervisedLearner *learner);

	virtual ~BatchCAQDataGenerator();
};

class BatchQDataGenerator: public BatchDataGenerator {
protected:
	std::map<Action *, DataSet *> *inputMap;
	std::map<Action *, DataSet1D *> *outputMap;
	std::map<Action *, ColumnVector *> *buffVectorMap;
	std::map<Action *, DataSet1D *> *weightedMap;

	QFunction *qFunction;
	StateProperties *properties;
	ActionSet *actions;

	SupervisedQFunctionLearner *learner;
	SupervisedQFunctionWeightedLearner *weightedLearner;

public:
	void init(QFunction *qFunction, ActionSet *actions,
	        StateProperties *properties);

	virtual void addInput(StateCollection *state, Action *action,
	        RealType output, RealType weighting = 1.0);

	virtual void trainFA();
	virtual void resetPolicyEvaluation();

	virtual RealType getValue(StateCollection *state, Action *action);

	DataSet *getInputData(Action *action);
	DataSet1D *getOutputData(Action *action);

	StateProperties *getStateProperties(Action *action);

public:
	BatchQDataGenerator(
		QFunction *qFunction,
	    SupervisedQFunctionLearner *learner,
	    StateProperties *inputState = nullptr
	);

	BatchQDataGenerator(
		QFunction *qFunction,
		SupervisedQFunctionWeightedLearner *learner,
		StateProperties *inputState = nullptr
	);

	BatchQDataGenerator(ActionSet *actions, StateProperties *properties);

	virtual ~BatchQDataGenerator();
};

class FittedIteration: public PolicyEvaluation {
protected:
	AgentController *estimationPolicy;
	BatchDataGenerator *dataGenerator;

	EpisodeHistory *episodeHistory;
	RewardHistory *rewardLogger;

	DataCollector *dataCollector;
	PolicyEvaluation *actorLearner;

	PolicyEvaluation *initialPolicyEvaluation;
	int useResidualAlgorithm;

protected:
	virtual void addResidualInput(Step *step, Action *action, RealType oldV,
	        RealType newV, RealType nearestNeighborDistance,
	        Action *nextHistoryAction = nullptr, RealType nextReward = 0.0);

	virtual RealType getWeighting(StateCollection *state, Action *action);
	virtual RealType getValue(StateCollection *state, Action *action);
	virtual void onParametersChanged();

public:
	virtual void doEvaluationTrial();
	virtual void evaluatePolicy(int trials);

	virtual BatchDataGenerator *createTrainingsData();

	virtual void setDataCollector(DataCollector *dataCollector);

	virtual void setInitialPolicyEvaluation(
	        PolicyEvaluation *initialPolicyEvaluation);

	virtual void resetLearnData();

	void setActorLearner(PolicyEvaluation *actorLearner);
	virtual void evaluatePolicy();

public:
	FittedIteration(
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		BatchDataGenerator *dataGenerator
	);

	virtual ~FittedIteration();
};

class FittedCAQIteration: public FittedIteration {
public:
	FittedCAQIteration(
		ContinuousActionQFunction *qFunction,
		StateProperties *properties,
		AgentController *estimationPolicy,
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		SupervisedLearner *learner
	);

	virtual ~FittedCAQIteration();
};

class FittedVIteration: public FittedIteration {
protected:
	StochasticPolicy *estimationPolicy;
	RealType *actionProbabilities;
	ActionSet *availableActions;

protected:
	virtual RealType getWeighting(StateCollection *state, Action *action);

public:
	FittedVIteration(
		AbstractVFunction *vFunction,
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		SupervisedLearner *learner
	);

	FittedVIteration(
		AbstractVFunction *vFunction,
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		SupervisedWeightedLearner *learner,
		StochasticPolicy *estimationPolicy
	);

	virtual ~FittedVIteration();

};

class FittedQIteration: public FittedIteration {
protected:
	std::map<Action *, DataSet *> *inputDatas;
	std::map<Action *, DataSet1D *> *outputDatas;
	std::map<Action *, KDTree *> *kdTrees;
	std::map<Action *, KNearestNeighbors *> *nearestNeighbors;
	std::map<Action *, DataPreprocessor *> *dataPreProc;

	std::list<int> *neighborsList;
	StateProperties *residualProperties;
	int kNN;
	State *buffState;

protected:
	virtual void addResidualInput(Step *step, Action *action, RealType oldV,
	        RealType newV, RealType nearestNeighborDistance,
	        Action *nextHistoryActon = nullptr, RealType nextReward = 0.0);

public:
	virtual void doEvaluationTrial();

public:
	FittedQIteration(
		QFunction *qFunction,
		AgentController *estimationPolicy,
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		SupervisedQFunctionLearner *learner,
		StateProperties *residualProperties = nullptr
	);

	FittedQIteration(
		QFunction *qFunction,
		StateProperties *inputState,
		AgentController *estimationPolicy,
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		SupervisedQFunctionLearner *learner,
		StateProperties *residualProperties = nullptr
	);

	virtual ~FittedQIteration();
};

class FittedQNewFeatureCalculator:
	public FittedQIteration,
	public NewFeatureCalculatorDataGenerator
{
protected:
	QFunction *qFunction;
	QFunction *qFunctionPolicy;

	std::map<Action *, FeatureCalculator *> *policyCalculator;
	std::map<Action *, FeatureCalculator *> *estimationCalculator;

	NewFeatureCalculator *newFeatureCalc;
	Agent *agent;

public:
	virtual void calculateNewFeatures();
	virtual void swapValueFunctions();

	void clearCalculators();
	virtual void resetData();

	void setStateModifiersObject(Agent *agent);

public:
	FittedQNewFeatureCalculator(
		QFunction *qFunction,
		QFunction *qFunctionPolicy,
		StateProperties *inputState,
		AgentController *estimationPolicy,
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		NewFeatureCalculator *newFeatCalc
	);

	virtual ~FittedQNewFeatureCalculator();
};

class ActionDistribution;

class ContinuousDynamicProgramming: public PolicyEvaluation {
protected:
	SamplingBasedTransitionModel *transModel;

	ActionSet *availableActions;

	RealType *actionValues;
	RealType *actionProbabilities;

	int numIteration;

public:
	virtual void evaluatePolicy(int numEvaluations);

	virtual RealType getValueFromDistribution(ActionSet *availableActions,
	        RealType *actionValues, ActionDistribution *distribution);

	virtual RealType getValue(State *state, ActionSet *availableActions) = 0;
	virtual void updateOutputs(int index, ActionSet *availableActions,
	        RealType *actionValues) = 0;

	virtual void learn() = 0;

	virtual void resetLearnData();
	virtual void resetDynamicProgramming() = 0;

public:
	ContinuousDynamicProgramming(
		ActionSet *allActions,
		SamplingBasedTransitionModel *transModel
	);

	virtual ~ContinuousDynamicProgramming();
};

class ContinuousDynamicVProgramming: public ContinuousDynamicProgramming {
protected:
	DataSet1D *outputValues;
	ActionDistribution *distribution;

	SupervisedLearner *learner;
	AbstractVFunction *vFunction;

public:
	virtual RealType getValue(State *state, ActionSet *availableActions);

	virtual void updateOutputs(
		int index,
		ActionSet *availableActions,
		RealType *actionValues
	);

	virtual void learn();
	virtual void resetDynamicProgramming();

public:
	ContinuousDynamicVProgramming(
		ActionSet *allActions,
		ActionDistribution *distribution,
		SamplingBasedTransitionModel *transModel,
		AbstractVFunction *vFunction,
		SupervisedLearner *learner
	);

	virtual ~ContinuousDynamicVProgramming();
};

class ContinuousDynamicQProgramming: public ContinuousDynamicProgramming {
protected:
	std::map<Action *, DataSet1D *> *outputValues;
	std::map<Action *, DataSet *> *inputValues;

	SupervisedQFunctionLearner *learner;
	AbstractQFunction *qFunction;

	ActionDistribution *distribution;
	RealType *actionValues2;

public:
	virtual RealType getValue(State *state, ActionSet *availableActions);
	virtual void updateOutputs(int index, ActionSet *availableActions,
	        RealType *actionValues);

	virtual void learn();
	virtual void resetDynamicProgramming();

public:
	ContinuousDynamicQProgramming(
		ActionSet *allActions,
		ActionDistribution *distribution,
		SamplingBasedTransitionModel *transModel,
		AbstractQFunction *vFunction,
		SupervisedQFunctionLearner *learner
	);

	virtual ~ContinuousDynamicQProgramming();
};

class ContinuousMCQEvaluation: public ContinuousDynamicQProgramming {
protected:
	PolicySameStateEvaluator *evaluator;

public:
	virtual RealType getValue(State *state, ActionSet *availableActions);

public:
	ContinuousMCQEvaluation(
		ActionSet *allActions,
		ActionDistribution *distribution,
		SamplingBasedTransitionModel *transModel,
		PolicySameStateEvaluator *evaluator,
		SupervisedQFunctionLearner *learner
	);

	virtual ~ContinuousMCQEvaluation();

};

class GraphTransition;
class DataSubset;
class KDRectangle;

class GraphDynamicProgramming: public PolicyEvaluation {
protected:
	SamplingBasedGraph *transModel;
	DataSet1D *outputValues;

public:
	bool resetGraph;

public:
	virtual void evaluatePolicy(int numEvaluations);
	virtual void resetLearnData();

	virtual RealType getValue(int node);
	virtual RealType getValue(ColumnVector *input);

	virtual GraphTransition *getMaxTransition(int index, RealType &maxValue,
	        KDRectangle *range = nullptr);
	virtual void getNearestNode(ColumnVector *input, int &node,
	        RealType &distance);

	SamplingBasedGraph *getGraph();
	DataSet1D *getOutputValues();
	virtual void saveCSV(std::string filename, DataSubset *nodeSubset);

public:
	GraphDynamicProgramming(SamplingBasedGraph *transModel);
	virtual ~GraphDynamicProgramming();
};

class GraphTarget;
class AdaptiveTargetGraph;

class GraphAdaptiveTargetDynamicProgramming: public GraphDynamicProgramming {
protected:
	std::map<GraphTarget *, DataSet1D *> *targetMap;
	std::list<GraphTarget *> *targets;

	GraphTarget *currentTarget;
	AdaptiveTargetGraph *adaptiveTargetGraph;

public:
	virtual GraphTransition *getMaxTransition(int index, RealType &maxValue,
	        KDRectangle *range = nullptr);

	virtual void addTarget(GraphTarget *target);
	virtual GraphTarget *getTargetForState(StateCollection *state);

	virtual void setCurrentTarget(GraphTarget *target);

	int getNumTargets();
	GraphTarget *getTarget(int index);

	virtual void resetLearnData();

public:
	GraphAdaptiveTargetDynamicProgramming(AdaptiveTargetGraph *graph);
	virtual ~GraphAdaptiveTargetDynamicProgramming();
};

} //namespace rlearner

#endif //Rlearner_cbatchlearning_H_
