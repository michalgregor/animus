// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cagentlogger_H_
#define Rlearner_cagentlogger_H_

#include <iostream>
#include <list>

#include "system.h"
#include "agentlistener.h"
#include "episodehistory.h"
#include "baseobjects.h"

namespace rlearner {

class Episode;
class StateModifier;

/**
 * @class AgentLogger
 * Class for logging a whole training trial.
 *
 * The agent logger (AgentLogger) objects can store whole training trials.
 * AgentLogger is a subclass of SemiMDPListener in order to get the data from
 * the agent. The agent logger uses a list of episode objects, so you can
 * retrieve whole episodes from the logger. The current episode isn't in that
 * list and can only be referenced by the method getCurrentEpisode. From
 * the episodes you can again retrieve the single states. You can set
 * an auto-save file where every Episode gets saved automatically when it
 * is finished. You can also set the number of episodes the logger should hold
 * in memory (both parameters are set in the constructor). If the episodes
 * exceeds that number, the oldest episodes get discarded. holdMemory of
 * -1 means that all episodes are held in the memory.
 *
 * The agent logger stores only those states which's state modifier objects
 * (i.e. the properties) are in the state modifier list of the logger.
 @see Episode
 */
class AgentLogger: public SemiMDPListener, public EpisodeHistory {
protected:
	//! Autosave file name.
	std::string _filename;
	//! Autosave file.
	std::unique_ptr<std::ostream> _file;

	std::string _loadFileName;
	std::set<StateModifierList*> _loadModifiers;
	//! Number of episodes to hold in memory, if set to -1 all episodes
	//! are held in memory.
	int _holdMemory;

	//! List of the episodes.
	std::list<Episode*> _episodes;
	//! Pointer to the current episode.
	Episode* _currentEpisode;

	void init();

public:
	//! Stores the step in the current episode.
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *nextState);
	//! Stores the current Episode in the episode list and creates a new
	//! current Episode object.
	virtual void newEpisode();

	//! Sets auto save file, if set to nullptr no auto save file is used.
	void setAutoSaveFile(const std::string& filename);

	//! Saves the whole trial to a text file.
	virtual void saveData(std::ostream& stream);

	/**
	 * Loads a training trial from a text file.
	 *
	 * If episodes is set to a value greater 0, only the first "episodes"
	 * episodes are loaded.
	 */
	virtual void loadData(std::istream& stream, int episodes);

	virtual void loadData(std::istream& stream);

	virtual void loadData(
		std::istream& stream,
		std::set<StateModifierList*>& modifierLists,
		int episodes = -1
	);

	/// Returns number of Episodes in the episode list (so exclusive
	//! the current Episode).
	virtual int getNumEpisodes();

	//! Adds a state modifier to the current episode object.
	virtual void addModifierList(StateModifierList* modifierList);
	//! Removes a state modifier to the current episode object.
	virtual void removeModifierList(StateModifierList* modifierList);

	//! Returns a pointer to the current episode.
	virtual Episode* getCurrentEpisode();

	//! Returns the index-th episode.
	virtual Episode* getEpisode(int index);

	//! Clears the autosave file.
	void clearAutoSaveFile();

	void setLoadDataFile(
		const std::string& loadData,
		const std::set<StateModifierList*>& modifierLists = {}
	);

	//! Removes all episodes from memory.
	virtual void resetData();

public:
	//! Creates an agent logger and sets the autosave file.
	AgentLogger(StateProperties *model, ActionSet *actions,
			const std::string& autoSavefile, int holdMemory = -1);
	//! Creates an agent logger with no autosave file and all episodes
	//! held in memory.
	AgentLogger(StateProperties *model, ActionSet *actions);
	//! Loads an training trial from a binary file. The modifiers must be
	//! the same as the modifiers used when the trial was saved.
	AgentLogger(const std::string& loadFileName, StateProperties *model,
		ActionSet *actions, const std::set<StateModifierList*>& modifierLists);

	virtual ~AgentLogger();
};

/**
 * @class EpisodeOutput
 * This class writes each step and start of a new episode in readable
 * form to a file.
 *
 * For each state the old state, action, reward and newstate is written
 * to the specified file. For the states only the specified state is chosen
 * from the state collection. This class can be used for error tracking
 * of the model or reward model and debugging.
 */
class EpisodeOutput: public SemiMDPRewardListener,
        public ActionObject,
        public StateObject {
protected:
	shared_ptr<std::ostream> _stream;

	int _nEpisodes;
	int _nSteps;

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);
	virtual void intermediateStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);
	virtual void newEpisode();

public:
	EpisodeOutput(StateProperties *featCalc, RewardFunction *rewardFunction,
	        ActionSet *actions, const shared_ptr<std::ostream>& output);
	virtual ~EpisodeOutput();
};

class EpisodeMatlabOutput: public SemiMDPRewardListener,
        public ActionObject,
        public StateObject {
protected:
	shared_ptr<std::ostream> _stream;

	int _nEpisodes;
	int _nSteps;

public:
	int getNumEpisodes() const {return _nEpisodes;}
	int getNumSteps() const {return _nSteps;}

	void setNumEpisodes(int numEpisodes) {_nEpisodes = numEpisodes;}
	void setNumSteps(int numSteps) {_nSteps = numSteps;}

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);
	virtual void newEpisode();

	void setOutputFile(const shared_ptr<std::ostream>& stream);

public:
	EpisodeMatlabOutput(
		StateProperties *featCalc, RewardFunction *rewardFunction,
		ActionSet *actions, const shared_ptr<std::ostream>& output
	);

	virtual ~EpisodeMatlabOutput();
};

/**
 * @class EpisodeOutputStateChanged
 * Class writes only the steps in which the specified state changes in readable
 * form to a file.
 *
 * Does the same as EpisodeOutput, but only for steps in which
 * the state changes.
 */
class EpisodeOutputStateChanged: public EpisodeOutput {
public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *nextState);

public:
	EpisodeOutputStateChanged(
		StateProperties *featCalc, RewardFunction *rewardFunction,
		ActionSet *actions, const shared_ptr<std::ostream>& output
	);

	virtual ~EpisodeOutputStateChanged() {}
};

class StateOutput: public SemiMDPListener, public StateObject {
protected:
	shared_ptr<std::ostream> _stream;

public:
	virtual void nextStep(
		StateCollection *oldState, Action *action, StateCollection *nextState
	);

public:
	StateOutput(StateProperties *featCalc, const shared_ptr<std::ostream>& _stream);
	virtual ~StateOutput();
};

class ActionOutput: public SemiMDPListener, public ActionObject {
protected:
	shared_ptr<std::ostream> _stream;

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *nextState);

public:
	ActionOutput(ActionSet *actions, const shared_ptr<std::ostream>& output);
	virtual ~ActionOutput();
};

} //namespace rlearner

#endif //Rlearner_cagentlogger_H_
