#include "StateInitializer.h"

namespace rlearner {

void StateInitializer::serialize(IArchive&, const unsigned int) {}
void StateInitializer::serialize(OArchive&, const unsigned int) {}

void StateInitializer_Constant::serialize(
    IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<StateInitializer>(*this);
	ar & _discreteDimensions;
	ar & _continuousDimensions;
}

void StateInitializer_Constant::serialize(
    OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<StateInitializer>(*this);
	ar & _discreteDimensions;
	ar & _continuousDimensions;
}

void StateInitializer_Constant::setDiscreteValue(unsigned int dim, int value) {
	_discreteDimensions.at(dim) = value;
}

void StateInitializer_Constant::setContinuousValue(
    unsigned int dim, RealType value) {
	_continuousDimensions.at(dim) = value;
}

void StateInitializer_Constant::setDiscreteValues(
    const std::vector<int>& values) {
	_discreteDimensions = values;
}

void StateInitializer_Constant::setContinuousValues(
    const std::vector<RealType>& values) {
	_continuousDimensions = values;
}

void StateInitializer_Constant::initialize(State* state) {
	auto sz = _discreteDimensions.size();

	for(decltype(sz) i = 0; i < sz; i++) {
		state->setDiscreteState(i, _discreteDimensions[i]);
	}

	sz = _continuousDimensions.size();

	for(decltype(sz) i = 0; i < sz; i++) {
		state->setContinuousState(i, _continuousDimensions[i]);
	}
}

StateInitializer_Constant::StateInitializer_Constant() :
	_discreteDimensions(), _continuousDimensions() {
}

StateInitializer_Constant::StateInitializer_Constant(
    const std::vector<int>& discreteValues,
    const std::vector<RealType> continuousValues) :
	_discreteDimensions(), _continuousDimensions() {
	setDiscreteValues(discreteValues);
	setContinuousValues(continuousValues);
}

void StateInitializer_Uniform::serialize(
    IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<StateInitializer>(*this);
	ar & _discreteDimensions;
	ar & _continuousDimensions;
}

void StateInitializer_Uniform::serialize(
    OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<StateInitializer>(*this);
	ar & _discreteDimensions;
	ar & _continuousDimensions;
}

void StateInitializer_Uniform::setDiscreteBound(
    unsigned int dim, int lbound, int ubound) {
	_discreteDimensions.at(dim).createSpace(lbound, ubound);
}

void StateInitializer_Uniform::setContinuousBound(
    unsigned int dim, RealType lbound, RealType ubound) {
	_continuousDimensions.at(dim).createSpace(lbound, ubound);
}

void StateInitializer_Uniform::setDiscreteBounds(
    const std::vector<int>& lbounds, const std::vector<int>& ubounds) {
	SYS_THROW_ASSERT(lbounds.size() == ubounds.size(),
	    TracedError_InvalidArgument);

	auto sz = lbounds.size();
	_discreteDimensions.resize(sz);

	for(decltype(sz) i = 0; i < sz; i++) {
		_discreteDimensions[i].createSpace(lbounds[i], ubounds[i]);
	}
}

void StateInitializer_Uniform::setContinuousBounds(
    const std::vector<RealType>& lbounds, const std::vector<RealType>& ubounds) {
	SYS_THROW_ASSERT(lbounds.size() == ubounds.size(),
	    TracedError_InvalidArgument);

	auto sz = lbounds.size();
	_continuousDimensions.resize(sz);

	for(decltype(sz) i = 0; i < sz; i++) {
		_continuousDimensions[i].createSpace(lbounds[i], ubounds[i]);
	}
}

void StateInitializer_Uniform::initialize(State* state) {
	auto sz = _discreteDimensions.size();

	for(decltype(sz) i = 0; i < sz; i++) {
		state->setDiscreteState(i, _discreteDimensions[i]());
	}

	sz = _continuousDimensions.size();

	for(decltype(sz) i = 0; i < sz; i++) {
		state->setContinuousState(i, _continuousDimensions[i]());
	}
}

StateInitializer_Uniform::StateInitializer_Uniform() :
	_discreteDimensions(), _continuousDimensions() {
}

StateInitializer_Uniform::StateInitializer_Uniform(
    const std::vector<int>& lboundsDiscrete,
    const std::vector<int>& uboundsDiscrete,
    const std::vector<RealType>& lboundsContinuous,
    const std::vector<RealType>& uboundsContinuous) :
	_discreteDimensions(), _continuousDimensions() {
	setDiscreteBounds(lboundsDiscrete, uboundsDiscrete);
	setContinuousBounds(lboundsContinuous, uboundsContinuous);
}

} //namespace rlearner
