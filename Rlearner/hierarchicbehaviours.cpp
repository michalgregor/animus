// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "hierarchicbehaviours.h"
#include "transitionfunction.h"
#include "regions.h"
#include "state.h"
#include "statecollection.h"
#include "stateproperties.h"

#include "utility.h"

#include "ril_debug.h"

namespace rlearner {

SubGoalBehaviour::SubGoalBehaviour(
    StateProperties *l_modelProperties, Region *l_avialableRegion,
    char *l_subgoalName) :
	    HierarchicalSemiMarkovDecisionProcess(l_modelProperties),
	    StateReward(l_modelProperties),
	    subgoalName(l_subgoalName) {
	standardReward = -1.0;

	availableRegion = l_avialableRegion;

	rewardFactors = new std::map<Region *, std::pair<RealType, RealType> >;

	targetRegions = new std::list<Region *>;
	failRegions = new std::list<Region *>;

	this->modelProperties = l_modelProperties;
}

SubGoalBehaviour::~SubGoalBehaviour() {
	delete rewardFactors;
	delete targetRegions;
	delete failRegions;
}

void SubGoalBehaviour::sendNextStep(Action *action) {
	bool debugPrint = false;
	HierarchicalSemiMarkovDecisionProcess::sendNextStep(action);
}

bool SubGoalBehaviour::isFinished(
    StateCollection *, StateCollection *newState) {
	bool finished = isInFailRegion(newState->getState(modelProperties))
	    || isInGoalRegion(newState->getState(modelProperties));

	return finished;
}

bool SubGoalBehaviour::isInGoalRegion(State *state) {
	std::list<Region *>::iterator it = targetRegions->begin();

	bool finished = false;

	for(; it != targetRegions->end(); it++) {
		if((*it)->isStateInRegion(state)) {
			finished = true;
			break;
		}
	}
	return finished;
}

bool SubGoalBehaviour::isInFailRegion(State *state) {
	std::list<Region *>::iterator it = failRegions->begin();

	bool finished = false;

	for(; it != failRegions->end(); it++) {
		if((*it)->isStateInRegion(state)) {
			finished = true;
			break;
		}
	}
	return finished;
}

bool SubGoalBehaviour::isAvailable(StateCollection *currentState) {
	bool availAble = availableRegion->isStateInRegion(
	    currentState->getState(modelProperties));
	availAble = availAble && !isFinished(nullptr, currentState);

	if(availAble) {
		DebugPrint('h', "Subgoal %s is availAble\n", subgoalName.c_str());
	} else {
		DebugPrint('h', "Subgoal %s is not availAble\n", subgoalName.c_str());
	}

	return availAble;
}

RealType SubGoalBehaviour::getStateReward(State *modelState) {
	RealType reward = standardReward;

	std::list<Region *>::iterator it = targetRegions->begin();

	for(; it != targetRegions->end(); it++) {
		RealType rewardFactor = (*rewardFactors)[*it].first;
		RealType rewardTau = (*rewardFactors)[*it].second;

		RealType distance = (*it)->getDistance(modelState);
		RealType dReward = rewardFactor * bounded_exp(-rewardTau * distance);
		reward += dReward;
	}

	it = failRegions->begin();

	for(; it != failRegions->end(); it++) {
		RealType rewardFactor = (*rewardFactors)[*it].first;
		RealType rewardTau = (*rewardFactors)[*it].second;

		RealType dReward = rewardFactor
		    * bounded_exp(-rewardTau * (*it)->getDistance(modelState));

		reward += dReward;
	}
	return reward;
}

void SubGoalBehaviour::getInputDerivation(State *, ColumnVector *) {

}

void SubGoalBehaviour::addTargetRegion(
    Region *target, RealType rewardFactor, RealType rewardTau) {
	targetRegions->push_back(target);

	(*rewardFactors)[target] = std::pair<RealType, RealType>(rewardFactor,
	    rewardTau);
}

void SubGoalBehaviour::addFailRegion(
    Region *target, RealType rewardFactor, RealType rewardTau) {
	failRegions->push_back(target);

	(*rewardFactors)[target] = std::pair<RealType, RealType>(rewardFactor,
	    rewardTau);
}

void SubGoalBehaviour::setRewardFactor(Region *region, RealType rewardFactor) {
	(*rewardFactors)[region].first = rewardFactor;
}

void SubGoalBehaviour::setRewardTau(Region *region, RealType rewardTau) {
	(*rewardFactors)[region].second = rewardTau;
}

/*
 SubGoalTrainer::SubGoalTrainer(TransitionFunction *transitionFunction, SubGoalBehaviour *l_subGoal) : TransitionFunctionEnvironment(transitionFunction)
 {
 this->subGoal = l_subGoal;
 if (subGoal)
 {
 this->sampleRegion = l_subGoal->getAvailAbleRegion();
 }
 }

 void SubGoalTrainer::doNextState(PrimitiveAction *action)
 {
 TransitionFunctionEnvironment::doNextState(action);

 if (subGoal)
 {
 failed = failed | subGoal->isInFailRegion(this->modelState);
 reset = reset | failed | subGoal->isInGoalRegion(this->modelState);
 }
 }

 void SubGoalTrainer::doResetModel()
 {
 if (subGoal)
 {
 sampleRegion->getRandomStateSample(modelState);
 }
 else
 {
 TransitionFunctionEnvironment::doResetModel();
 }
 }

 void SubGoalTrainer::setSubGoal(SubGoalBehaviour *subGoal)
 {
 this->subGoal = subGoal;
 this->sampleRegion =  subGoal->getAvailAbleRegion();
 }

 void SubGoalTrainer::setSampleRegion(Region *l_sampleRegion)
 {
 sampleRegion = l_sampleRegion;
 }
 */

SubGoalOutput::SubGoalOutput(AgentController *policy) {
	lastAction = nullptr;
	this->policy = policy;
}

void SubGoalOutput::nextStep(
    StateCollection *, Action *action, StateCollection *newState) {
	lastAction = (SubGoalBehaviour *) action;
	printf("Subgoal %s ended after %d steps in State\n",
	    lastAction->getSubGoalName().c_str(), lastAction->getDuration());
	newState->getState()->save(std::cout);
	printf("Subgoal %s begins\n",
	    ((SubGoalBehaviour *) policy->getNextAction(newState))->getSubGoalName().c_str());
}

void SubGoalOutput::newEpisode() {}

SubGoalController::SubGoalController(ActionSet *hierarchicActions) :
	AgentController(hierarchicActions) {}

Action *SubGoalController::getNextAction(
    StateCollection *state, ActionDataSet *) {
	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		if((*it)->isAvailable(state)) {
			return *it;
		}
	}
	printf("SubGoal Controller, fatal error: No Subgoal available!!, State: ");
	state->getState()->save(std::cout);
	printf("\n");
	assert(false);
	return nullptr;
}

ExtendedPrimitiveAction::ExtendedPrimitiveAction(
    Action *l_primitiveAction, int l_extendedActionDuration) {
	this->primitiveAction = l_primitiveAction;
	this->extendedActionDuration = l_extendedActionDuration;
	this->sendIntermediateSteps = false;
}

bool ExtendedPrimitiveAction::isFinished(StateCollection *, StateCollection *) {
	return getDuration() >= extendedActionDuration;
}

Action* ExtendedPrimitiveAction::getNextHierarchyLevel(
    StateCollection *, ActionDataSet *) {
	return primitiveAction;
}

PrimitiveActionStateChange::PrimitiveActionStateChange(
    PrimitiveAction *action, StateProperties *stateToChange) {
	this->primitiveAction = action;
	this->stateToChange = stateToChange;
}

Action* PrimitiveActionStateChange::getNextHierarchyLevel(
    StateCollection *, ActionDataSet *) {
	return primitiveAction;
}

bool PrimitiveActionStateChange::isFinished(
    StateCollection *oldStateCol, StateCollection *newStateCol) {
	State *oldState = oldStateCol->getState(stateToChange);
	State *newState = newStateCol->getState(stateToChange);
	return oldState->equals(newState);
}

void PrimitiveActionStateChange::setStateToChange(
    StateProperties *stateToChange) {
	this->stateToChange = stateToChange;
}

} //namespace rlearner
