#include "AgentSimulator.h"
#include "ril_debug.h"
#include "statecollection.h"

namespace rlearner {

void AgentSimulator::addStateModifier(StateModifier* modifier) {
	_modifierList.addStateModifier(modifier);
}

void AgentSimulator::removeStateModifier(StateModifier* modifier) {
	_modifierList.removeStateModifier(modifier);
}

void AgentSimulator::addModifierList(StateModifierList* modifierList) {
	_lastState->addModifierList(modifierList);
	_currentState->addModifierList(modifierList);
	_agent->addModifierList(modifierList);
}

void AgentSimulator::removeModifierList(StateModifierList* modifierList) {
	_lastState->removeModifierList(modifierList);
	_currentState->removeModifierList(modifierList);
	_agent->removeModifierList(modifierList);
}

void AgentSimulator::initializeState() {
	_model->getState(_currentState.get());
	_currentState->newModelState();
	_currentState->setResetState(_model->isReset());

	if(DebugIsEnabled()) {
		DebugPrint('+', "\nNew Episode (%d): ",
			this->getTotalEpisodes());
		DebugPrint('+', "start State: ");
		_currentState->getState()->save(DebugGetFileHandle('+'));
		DebugPrint('+', "\n");
	}
}

void AgentSimulator::doAction(Action *l_action) {
	Action *action = l_action;

	int index = _agent->getActions()->getIndex(action);

	assert(
	    action != nullptr && action->isType(AF_PrimitiveAction) && index >= 0);

	if(_model->isReset()) {
		startNewEpisode();
	}

	std::swap(_lastState, _currentState);

	PrimitiveAction* primAction = dynamic_cast<PrimitiveAction*>(action);

	_model->nextState(primAction);
	_model->getState(_currentState.get());

	_currentState->setResetState(_model->isReset());

	if(DebugIsEnabled()) {
		DebugPrint('+', "\nNew Step: ");
		DebugPrint('+', "oldState: ");
		_lastState->getState()->save(DebugGetFileHandle('+'));
		DebugPrint('+', "action: %d ", _agent->getActions()->getIndex(action));
		if(action->getActionData()) {
			action->getActionData()->save(DebugGetFileHandle('+'));
		}
		DebugPrint('+', "currentState: ");
		_currentState->getState()->save(DebugGetFileHandle('+'));
		DebugPrint('+', "\n");
	}

	_totalSteps++;
	_agent->sendNextStep(_lastState.get(), action, _currentState.get());
}

void AgentSimulator::startNewEpisode() {
	DebugPrint('a', "Starting new Episode\n");
	_agent->startNewEpisode();
	_totalEpisodes++;
	_model->resetModel();
	initializeState();
}

bool AgentSimulator::isEpisodeEnded() const {
	return _model->isReset();
}

StateCollection* AgentSimulator::getCurrentState() {
	return _currentState.get();
}

int AgentSimulator::doControllerEpisode(int maxSteps) {
	if(_model->isReset()) startNewEpisode();
	int currentSteps = 0;

	while(!_model->isReset() && currentSteps < maxSteps) {
		// if the episode has ended, we stop and do not increment the counter
		if(!doControllerStep()) break;
		currentSteps++;
	}

	return currentSteps;
}

std::pair<unsigned int, unsigned int> AgentSimulator::run(unsigned int maxEpisodes, unsigned int maxSteps) {
	unsigned int currentSteps = 0;
	unsigned int currentEpisodes = 0;

	while(currentEpisodes < maxEpisodes) {
		currentSteps += doControllerEpisode(maxSteps);
		currentEpisodes++;
	}

	return {currentSteps, currentEpisodes};
}

bool AgentSimulator::doControllerStep() {
	if(_model->isReset()) return false;

	Action* action = _agent->computeAction(_currentState.get());
	assert(action != nullptr);

	action->loadActionData(_agent->getActionDataSet()->getActionData(action));
	doAction(action);

	return true;
}

AgentSimulator::AgentSimulator(
	const shared_ptr<EnvironmentModel>& model,
	const shared_ptr<Agent>& agent): _model(model), _agent(agent),
	_lastState(), _currentState(), _modifierList()
{
	assert(_model->getStateProperties());

	_currentState = std::unique_ptr<StateCollectionImpl>(new StateCollectionImpl(_model->getStateProperties(), {&_modifierList, _agent->getModifierList()}));
	_lastState = std::unique_ptr<StateCollectionImpl>(new StateCollectionImpl(_model->getStateProperties(), {&_modifierList, _agent->getModifierList()}));
	_agent->addModifierList(&_modifierList);
	startNewEpisode();
	_totalEpisodes = 0;
}

AgentSimulator::~AgentSimulator() {
	_currentState->removeModifierList(&_modifierList);
	_currentState->removeModifierList(_agent->getModifierList());
	_lastState->removeModifierList(&_modifierList);
	_lastState->removeModifierList(_agent->getModifierList());
	_agent->removeModifierList(&_modifierList);
}

} // namespace rlearner
