// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_crewardmodel_H_
#define Rlearner_crewardmodel_H_

#include <map>
#include <vector>

#include "rewardfunction.h"
#include "learndataobject.h"
#include "baseobjects.h"
#include "utility.h"
#include "agentlistener.h"

namespace rlearner {

class AbstractFeatureStochasticEstimatedModel;

class FeatureStateRewardFunction: public FeatureRewardFunction {
protected:
	std::map<int, RealType> *rewards;

public:
	virtual RealType getReward(int oldState, Action *action, int newState);
	virtual RealType getReward(int state);

	virtual void setReward(int state, RealType reward);

public:
	FeatureStateRewardFunction& operator=(const FeatureStateRewardFunction&) = delete;
	FeatureStateRewardFunction(const FeatureStateRewardFunction&) = delete;

	FeatureStateRewardFunction(StateProperties *discretizer);
	virtual ~FeatureStateRewardFunction();
};

/**
 * @class FeatureRewardModel
 *
 * For model based learning you need an reward function which assigns a reward
 * for transitions of feature indices, not for state objects. But what happens
 * if you don't have a reward function for features to yours disposal, if you
 * just have normal reward function (e.g. for the model state)? You can also
 * estimate the reward you will get for a transition, this is done by
 * FeatureRewardModel. Therefore it stores the reward already got when the
 * same transition occurred and the visits of the transition. Thus it can
 * calculate the mean reward. For the visits of the transition, an estimated
 * model can also be used to spare memory.
 *
 * Since the reward model must learn from the training trial it has to be added
 * to the agent's listener list. Since the reward model implements the
 * FeatureRewardFunction interface it can also be used as normal reward
 * function. Semi-MDP support hans't been added yet.
 *
 * @see FeatureRewardFunction
 */
class FeatureRewardModel:
    public FeatureRewardFunction, public SemiMDPRewardListener,
    public ActionObject, public LearnDataObject {
protected:
	/**
	 * Table of the rewards, summed up during the whole training trial,
	 * for a transition.
	 */
	MyArray2D<FeatureMap *> *rewardTable;

	/**
	 * Only used if no external estimated model is assigned.
	 *
	 * Table of the Transition visits, so the reward can be calculated by
	 * the mean (sum rewards/sum visits).
	 */
	MyArray2D<FeatureMap *> *visitTable;

	//! Used for calculating the visits, so no visit table is needed if used.
	AbstractFeatureStochasticEstimatedModel *model;

	bool bExternVisitSparse;

	/**
	 * Returns the transition visits of the specified state.
	 *
	 * Returns either the visits from the visit table, or, if an estimated
	 * model is assigned, the visits can also be retrieved by the model
	 */
	RealType getTransitionVisits(int oldState, int action, int newState);

public:
	/**
	 * Returns the reward for a specific discrete state transition.
	 *
	 * Calculates the mean reward from that transition, i.e. sum rewards/sum
	 * visits.
	 */
	virtual RealType getReward(int oldState, Action *action, int newState);
	virtual void nextStep(StateCollection *oldState, Action *action,
	RealType reward, StateCollection *newState);

	/**
	 * Saves the reward model.
	 *
	 * Saves the reward Table and the visit table if it is used
	 */
	virtual void saveData(std::ostream& stream);

	/**
	 * Loads the reward Table and the visit table if no estimated model was
	 * assigned to the constructor
	 */
	virtual void loadData(std::istream& stream);

	virtual void resetData();

public:
	FeatureRewardModel& operator=(const FeatureRewardModel&) = delete;
	FeatureRewardModel(const FeatureRewardModel&) = delete;

	/**
	 * Creates a reward model, which uses an estimated model for the calculation
	 * of the transition visits.
	 *
	 * It is very important that the estimated model contains the same
	 * transitions as the reward table, because otherwise a division by zero
	 * would occur. So the estimated model has to be added before the reward
	 * model to the listener.
	 */
	FeatureRewardModel(
	    ActionSet *actions,
	    RewardFunction *function,
	    AbstractFeatureStochasticEstimatedModel *model,
	    StateModifier *discretizer);

	/**
	 * Creates a reward model, which has to use an own visit table
	 * for the visits.
	 */
	FeatureRewardModel(
	    ActionSet *actions,
	    RewardFunction *function,
	    StateModifier *discretizer);

	virtual ~FeatureRewardModel();
};

class FeatureStateRewardModel:
    public FeatureRewardFunction, public SemiMDPRewardListener,
    public LearnDataObject {
protected:
	RealType *rewards;RealType *visits;

	RealType rewardMean;
	int numRewards;

public:
	/**
	 * Calls getReward(int oldState, Action *action, int newState) with
	 * the features from the state as argument.
	 *
	 * The state has to be a feature state, this state gets decomposed into
	 * his features, the reward for the features is calculated and summed up
	 * weighted with the feature factors.
	 */
	virtual RealType getReward(State *oldState, Action *action, State *newState);

	/**
	 * Returns the reward for a specific discrete state transition.
	 *
	 * Calculates the mean reward from that transition, i.e. sum
	 * rewards/sum visits.
	 */
	virtual RealType getReward(int oldState, Action *action, int newState);
	virtual RealType getReward(int newState);

	virtual void nextStep(StateCollection *oldState, Action *action,
	RealType reward, StateCollection *newState);

	/**
	 * Saves the reward model.
	 *
	 * Saves the reward Table and the visit table if it is used
	 */
	virtual void saveData(std::ostream& stream);

	/**
	 * Loads the reward Table and the visit table if no estimated model
	 * was assigned to the constructor
	 */
	virtual void loadData(std::istream& stream);

	virtual void resetData();

public:
	FeatureStateRewardModel& operator=(const FeatureStateRewardModel&) = delete;
	FeatureStateRewardModel(const FeatureStateRewardModel&) = delete;

	/**
	 * Creates a reward model, which uses an estimated model for the
	 * calculation of the transition visits.
	 *
	 * It is very important that the estimated model contains the same
	 * transitions as the reward table, because otherwise a division by zero
	 * would occur. So the estimated model has to be added before the reward
	 * model to the listener.
	 */
	FeatureStateRewardModel(
	    RewardFunction *function,
	    StateModifier *discretizer);

	/**
	 * Creates a reward model, which has to use an own visit table
	 * for the visits.
	 */
	virtual ~FeatureStateRewardModel();
};

/**
 * @class RewardEpisode
 * Logs the rewards during an Episode.
 *
 * Maintains a RealType array for logging the reward of each step. Can be used
 * in combination with a normal episode to log the whole
 * State-Action-Reward-State Tuples.
 */
class RewardEpisode: public SemiMDPRewardListener {
protected:
	//! Array for the rewards.
	std::vector<RealType> _rewards;

public:
	//! Stores the current reward at the and of the rewards vector.
	virtual void nextStep(StateCollection *oldState, Action *action,
	RealType reward, StateCollection *newState);
	//! Clears the reward vector.
	virtual void newEpisode();

	int getNumRewards();

	//! Returns the requested reward from the rewards vector.
	RealType getReward(int index);

	//! Returns reference to the reward vector.
	std::vector<RealType>& getRewards() {
		return _rewards;
	}

	//! Returns reference to the reward vector.
	const std::vector<RealType>& getRewards() const {
		return _rewards;
	}

	RealType getMeanReward();RealType getLastStepsMeanReward(int Steps);

	RealType getSummedReward(RealType gamma = 1.0);

	virtual void saveData(std::ostream& stream);
	virtual void loadData(std::istream& stream);

public:
	RewardEpisode& operator=(const RewardEpisode&) = delete;
	RewardEpisode(const RewardEpisode&) = delete;

	RewardEpisode(RewardFunction *rewardFunction);
	virtual ~RewardEpisode();
};

class RewardHistory {
public:
	virtual RewardEpisode* getEpisode(int index) = 0;
	virtual int getNumEpisodes() = 0;

public:
	RewardHistory() {
	}
	virtual ~RewardHistory() {
	}
};

class RewardHistorySubset: public RewardHistory {
protected:
	RewardHistory *episodes;
	std::vector<int> *indices;

public:
	//! Returns the number of episodes.
	virtual int getNumEpisodes();
	//! Returns a specific episode.
	virtual RewardEpisode* getEpisode(int index);

public:
	RewardHistorySubset& operator=(const RewardHistorySubset&) = delete;
	RewardHistorySubset(const RewardHistorySubset&) = delete;

	RewardHistorySubset(RewardHistory *episodes, std::vector<int> *indices);
	virtual ~RewardHistorySubset();
};

class RewardLogger:
    public SemiMDPRewardListener, public LearnDataObject, public RewardHistory {
protected:
	//! Autosave file name.
	std::string _filename;
	//! Autosave file.
	shared_ptr<std::ostream> _file;
	std::string _loadFileName;

	/**
	 * The number of episodes to hold in memory, if set to -1 all episodes are
	 * held in memory.
	 */
	int holdMemory;

	//! List of the episodes.
	std::list<RewardEpisode *> *episodes;
	//! Pointer to the current episode.
	RewardEpisode *currentEpisode;

	void init();

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	RealType reward, StateCollection *nextState);
	virtual void newEpisode();

	void setAutoSaveFile(const std::string& filename);

	virtual void saveData(std::ostream& stream);
	virtual void loadData(std::istream& stream) {loadData(stream, -1);}
	virtual void loadData(std::istream& stream, int episodes);

	virtual int getNumEpisodes();

	virtual RewardEpisode* getCurrentEpisode();
	virtual RewardEpisode* getEpisode(int index);

	std::vector<std::vector<RealType> > getRewards();

	void clearAutoSaveFile();
	void setLoadDataFile(const std::string& loadData);

	virtual void resetData();

public:
	RewardLogger& operator=(const RewardLogger&) = delete;
	RewardLogger(const RewardLogger&) = delete;

	RewardLogger(
	    RewardFunction *rewardFunction,
	    const std::string& autoSavefile,
	    int holdMemory);

	RewardLogger(RewardFunction *rewardFunction);
	RewardLogger(const std::string& loadFileName, RewardFunction *rewardFunction);

	virtual ~RewardLogger();
};

/**
 * @class SemiMDPLastNRewardFunction
 * Reward Function for Behaviours
 *
 * Very often the reward of a behaviour consists of the summed up rewards from
 * the primitive actions which were executed during the behaviour was active.
 * The class SemiMDPLastNRewardFunction does this reward calculation. It is
 * a subclass of RewardEpisode, so it maintains a reward array containing all
 * rewards from the past. The function getReward(StateCollection *oldState,
 * Action *action, StateCollection *newState). For an action of duration d,
 * the function calculates the reward of the transition by
 * sum_{i=0}^{d-1} gamma^i * r(N-i). The discount factor is needed since the
 * reward from the past has to be weakened by this factor.
 *
 * Since the object has to have access to the past "primitive" rewards, it has
 * to be added to the listener list of the agent.
 */
class SemiMDPLastNRewardFunction: public RewardFunction, public RewardEpisode {
protected:
	//! The discount factor.
	RealType gamma;

public:
	/**
	 * Calculates the reward for extended actions.
	 *
	 * For an action of duration d, the function the reward is calculated by
	 * sum_{i=0}^{d-1} gamma^i * r(N-i). The discount factor is needed
	 * since the reward from the past has to be weakened by this factor.
	 */
	virtual RealType getReward(
	    StateCollection *oldState,
	    Action *action,
	    StateCollection *newState);

public:
	//! Creates the reward function with the discount factor gamma.
	SemiMDPLastNRewardFunction(RewardFunction *rewardFunction, RealType gamma);
	virtual ~SemiMDPLastNRewardFunction();
};

} //namespace rlearner

#endif //Rlearner_crewardmodel_H_
