// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifdef USE_TORCH

#include <torch/ConnectedMachine.h>
#include <torch/MLP.h>
#include <torch/StochasticGradient.h>
#include <torch/WeightedMSECriterion.h>
#include <torch/MSECriterion.h>
#include <torch/MemoryDataSet.h>
#include <torch/DataSet.h>

#endif // USE_TORCH

#include "algebra.h"
#include "supervisedlearner.h"
#include "utility.h"
#include "torchvfunction.h"
#include "gradientfunction.h"
#include "inputdata.h"
#include "featurefunction.h"

#include <fstream>

namespace rlearner {

#ifdef USE_TORCH
using namespace Torch;
using Torch::MLP;
#endif // USE_TORCH

LeastSquaresLearner::LeastSquaresLearner(
    GradientUpdateFunction *l_featureFunc, int numData) {
	this->featureFunc = l_featureFunc;

	int numFeatures = featureFunc->getNumWeights();
	A = new Matrix(numData, numFeatures);
	b = new ColumnVector(numData);

	A->setZero();
	b->setZero();

	A_pinv = new Matrix(numFeatures, numData);
	A_pinv->setZero();

	addParameter("SVD_Damping", 0.01);
}

LeastSquaresLearner::~LeastSquaresLearner() {
	delete A;
	delete b;
	delete A_pinv;
}

RealType LeastSquaresLearner::doOptimization() {
	int numFeatures = featureFunc->getNumWeights();
	ColumnVector w(numFeatures);
	RealType error = doOptimization(A, A_pinv, b, &w,
	    getParameter("SVD_Damping"));

	RealType weights[numFeatures];

	for(int i = 0; i < numFeatures; i++) {
		weights[i] = w(i);
	}

	featureFunc->setWeights(weights);

	return error;
}

RealType LeastSquaresLearner::doOptimization(
    Matrix *A, Matrix *A_pinv, ColumnVector *b, ColumnVector *w,
    RealType lambda) {
	int numFeatures = w->rows();
//	RealType lambda = getParameter("SVD_Damping");

	assert(A->cols() == numFeatures && A->rows() == b->rows());

	getPseudoInverse(A, A_pinv, lambda);

	(*w) = (*A_pinv) * (*b);

	RealType error = 0;

	ColumnVector b_error(b->rows());
	b_error = (*A) * (*w);
	b_error = b_error - *b;

	error = b_error.norm() / b_error.rows();

	return error;
}

SupervisedGradientCalculator::SupervisedGradientCalculator(
    GradientFunction *l_gradientFunction, DataSet *l_inputData,
    DataSet *l_outputData) {
	this->inputData = l_inputData;
	this->outputData = l_outputData;

	outputData1D = nullptr;

	this->gradientFunction = l_gradientFunction;

	//assert(gradientFunction->getNumInputs() == inputData->getNumDimensions());
	//assert(gradientFunction->getNumOutputs() == inputData->rows());
}

SupervisedGradientCalculator::~SupervisedGradientCalculator() {
}

void SupervisedGradientCalculator::getGradient(FeatureList *gradient) {
	ColumnVector *input;
	ColumnVector output(gradientFunction->getNumOutputs());
	gradient->clear();

	for(unsigned int i = 0; i < inputData->size(); i++) {
		input = (*inputData)[i];

		gradientFunction->getFunctionValue(input, &output);

		if(outputData1D != nullptr) {
			output(0) = output(0) - (*outputData1D)[i];
		} else {
			output = output - *(*outputData)[i];
		}

		gradientFunction->getGradient(input, &output, gradient);
	}

	gradient->multFactor(1.0 / inputData->size());
}

RealType SupervisedGradientCalculator::getFunctionValue() {
	RealType squaredE = 0;
	ColumnVector *input;
	ColumnVector output(gradientFunction->getNumOutputs());

	for(unsigned int i = 0; i < inputData->size(); i++) {
		input = (*inputData)[i];

		gradientFunction->getFunctionValue(input, &output);

		if(outputData1D != nullptr) {
			output(0) = output(0) - (*outputData1D)[i];
		} else {
			output = output - *(*outputData)[i];
		}

		squaredE += output.dot(output);
	}

	return squaredE / inputData->size() / 2;
}

SupervisedFeatureGradientCalculator::SupervisedFeatureGradientCalculator(
    FeatureFunction *l_featureFunction) :
	SupervisedGradientCalculator(nullptr, nullptr, nullptr) {
	featureFunction = l_featureFunction;
	featureList = new FeatureList();
}

SupervisedFeatureGradientCalculator::~SupervisedFeatureGradientCalculator() {
	delete featureList;
}

FeatureList *SupervisedFeatureGradientCalculator::getFeatureList(
    ColumnVector *input) {
	featureList->clear();

	for(int i = 0; i < input->rows() / 2; i++) {
		featureList->set((int) input->operator[](i),
		    input->operator[](i + input->rows() / 2));
	}

	return featureList;
}

void SupervisedFeatureGradientCalculator::getGradient(FeatureList *gradient) {
	ColumnVector *input;
	gradient->clear();

	for(unsigned int i = 0; i < inputData->size(); i++) {
		input = (*inputData)[i];

		FeatureList *featureList = getFeatureList(input);

		RealType output = featureFunction->getFeatureList(featureList);
		RealType error = 0;
		if(outputData1D != nullptr) {
			error = output - (*outputData1D)[i];
		} else {
			assert(false);
		}
		gradient->add(featureList, error);
	}

	gradient->multFactor(1.0 / inputData->size());
}

RealType SupervisedFeatureGradientCalculator::getFunctionValue() {
	ColumnVector *input;
	RealType squaredE = 0;

	for(unsigned int i = 0; i < inputData->size(); i++) {
		input = (*inputData)[i];

		FeatureList *featureList = getFeatureList(input);

		RealType output = featureFunction->getFeatureList(featureList);
		RealType error = 0;
		if(outputData1D != nullptr) {
			error = output - (*outputData1D)[i];
		} else {
			assert(false);
		}
		squaredE += pow(error, 2.0);
	}

	return squaredE / 2.0 / inputData->size();
}

void SupervisedGradientCalculator::setData(
    DataSet *l_inputData, DataSet1D *l_outputData1D) {
	inputData = l_inputData;
	outputData = nullptr;
	outputData1D = l_outputData1D;
}

void SupervisedGradientCalculator::setData(
    DataSet *l_inputData, DataSet *l_outputData) {
	inputData = l_inputData;
	outputData = l_outputData;
	outputData1D = nullptr;
}

SupervisedGradientLearner::SupervisedGradientLearner(
    GradientLearner *l_gradientLearner,
    SupervisedGradientCalculator *l_gradientCalculator, int episodes) {
	addParameter("SupervisedGradientLearnEpisodes", episodes);

	gradientLearner = l_gradientLearner;
	gradientCalculator = l_gradientCalculator;
}

SupervisedGradientLearner::~SupervisedGradientLearner() {
}

void SupervisedGradientLearner::resetLearner() {
	gradientLearner->resetOptimization();
}

void SupervisedGradientLearner::learnFA(
    DataSet *inputData, DataSet1D *outputData) {
	gradientCalculator->setData(inputData, outputData);

	int numEpisodes = (int) getParameter("SupervisedGradientLearnEpisodes");
	gradientLearner->doOptimization(numEpisodes);
}

SupervisedQFunctionLearnerFromLearners::SupervisedQFunctionLearnerFromLearners(
    const ActionLearnerMap& learnerMap
): _learnerMap(learnerMap)
{
	for(auto lpair: _learnerMap) {
		addParameters(lpair.second.get());
	}
}

SupervisedQFunctionLearnerFromLearners::SupervisedQFunctionLearnerFromLearners(
	ActionLearnerMap&& learnerMap
): _learnerMap(std::move(learnerMap)) {
	for(auto lpair: _learnerMap) {
		addParameters(lpair.second.get());
	}
}

SupervisedQFunctionLearnerFromLearners::~SupervisedQFunctionLearnerFromLearners() {
}

void SupervisedQFunctionLearnerFromLearners::resetLearner() {
	for(auto lpair: _learnerMap) {
		lpair.second->resetLearner();
	}
}

void SupervisedQFunctionLearnerFromLearners::learnQFunction(
    Action* action, DataSet* inputData, DataSet1D* outputData
) {
	auto iter = _learnerMap.find(action);
	SYS_ASSERT(iter != _learnerMap.end());
	iter->second->learnFA(inputData, outputData);
}

SupervisedQFunctionWeightedLearnerFromLearners::SupervisedQFunctionWeightedLearnerFromLearners(
    std::map<Action *, SupervisedWeightedLearner *> *l_learnerMap) {
	learnerMap = l_learnerMap;
	std::map<Action *, SupervisedWeightedLearner *>::iterator it =
	    learnerMap->begin();
	for(; it != learnerMap->end(); it++) {
		addParameters((*it).second);
	}
}

SupervisedQFunctionWeightedLearnerFromLearners::~SupervisedQFunctionWeightedLearnerFromLearners() {
}

void SupervisedQFunctionWeightedLearnerFromLearners::resetLearner() {
	std::map<Action *, SupervisedWeightedLearner *>::iterator it =
	    learnerMap->begin();
	for(; it != learnerMap->end(); it++) {
		(*it).second->resetLearner();
	}
}

void SupervisedQFunctionWeightedLearnerFromLearners::learnQFunction(
    Action *action, DataSet *inputData, DataSet1D *outputData,
    DataSet1D *weightData) {
	(*learnerMap)[action]->learnWeightedFA(inputData, outputData, weightData);
}

GradientFunctionUpdater::GradientFunctionUpdater(
    GradientUpdateFunction *updateFunction) {
	this->updateFunction = updateFunction;
}

void GradientFunctionUpdater::addRandomParams(RealType randSize) {
	RealType *weights = new RealType[updateFunction->getNumWeights()];
	updateFunction->getWeights(weights);

	RealType normWeights = 0;
	for(int i = 0; i < updateFunction->getNumWeights(); i++) {
		normWeights += pow(weights[i], 2);
	}
	normWeights = sqrt(normWeights);

	for(int i = 0; i < updateFunction->getNumWeights(); i++) {
		weights[i] += Distributions::getNormalDistributionSample(0,
		    normWeights * randSize / 2);
	}
	updateFunction->setWeights(weights);
	delete weights;
}

ConstantGradientFunctionUpdater::ConstantGradientFunctionUpdater(
    GradientUpdateFunction *updateFunction, RealType learningRate) :
	GradientFunctionUpdater(updateFunction) {
	addParameter("GradientLearningRate", learningRate);
}

void ConstantGradientFunctionUpdater::updateWeights(FeatureList *gradient) {
	updateFunction->updateGradient(gradient,
	    getParameter("GradientLearningRate"));
}

void LineSearchGradientFunctionUpdater::setWorkingParamters(
    FeatureList *gradient, RealType stepSize, RealType *startParameters,
    RealType *workParameters) {
	DebugPrint('l', "Applying StepSize: %f\n", stepSize);

	memcpy(workParameters, startParameters,
	    sizeof(RealType) * updateFunction->getNumWeights());

	FeatureList::iterator it = gradient->begin();
	for(; it != gradient->end(); it++) {
		workParameters[(*it)->featureIndex] += stepSize * (*it)->factor;
	}
}

LineSearchGradientFunctionUpdater::LineSearchGradientFunctionUpdater(
    GradientCalculator *l_gradientCalculator,
    GradientUpdateFunction *updateFunction, int maxSteps) :
	GradientFunctionUpdater(updateFunction) {
	gradientCalculator = l_gradientCalculator;

	startParameters = new RealType[updateFunction->getNumWeights()];
	workParameters = new RealType[updateFunction->getNumWeights()];

	this->maxSteps = maxSteps;

	addParameter("LineSearchStepSizeScale", 1.0);
	precision_treshold = 0.0001;
}

LineSearchGradientFunctionUpdater::~LineSearchGradientFunctionUpdater() {
	delete startParameters;
	delete workParameters;
}

RealType LineSearchGradientFunctionUpdater::getFunctionValue(
RealType *startParameters, FeatureList *gradient, RealType stepSize) {
	setWorkingParamters(gradient, stepSize, startParameters, workParameters);
	updateFunction->setWeights(workParameters);

	RealType value = gradientCalculator->getFunctionValue();
	return value;
}

void LineSearchGradientFunctionUpdater::bracketMinimum(
RealType *startParameters, FeatureList *gradient, RealType fa, RealType &a,
RealType &b, RealType &c) {
	// Value of golden section (1 + sqrt(5))/2.0
	const RealType phi = 1.6180339887;

	// Initialise count of number of function evaluations
	int num_evals = 0;

	// A small non-zero number to avoid dividing by zero in quadratic interpolation
	const RealType TINY = 1.e-10;

	RealType max_step = 10.0;

	RealType fb = getFunctionValue(startParameters, gradient, b);

	num_evals++;

	bool bracket_found = false;
	// Assume that we know going from a to b is downhill initially 
	// (usually because gradf(a) < 0).

	RealType fu = 0;
	RealType fc = 0;
	RealType u = 0;

	if(fb > fa) {
		// Minimum must lie between a and b: do golden section until we find point
		// low enough to be middle of bracket
		do {
			c = b;
			b = a + (c - a) / phi;
			fb = getFunctionValue(startParameters, gradient, b);
			num_evals++;
			printf("Bracketing1: (%f %f) (%f, %f) (%f %f)\n", a, fa, b, fb, c,
			    fc);
		} while(fb > fa && b > a + TINY);
	} else {
		// There is a valid bracket upper bound greater than b
		c = b + phi * (b - a);
		fc = getFunctionValue(startParameters, gradient, c);
		num_evals = num_evals + 1;

		while(fb > fc) {
			//Do a quadratic interpolation (i.e. to minimum of quadratic)
			RealType r = (b - a) * (fb - fc);
			RealType q = (b - c) * (fb - fa);
			RealType sign_qr = 1.0;
			if(q - r < 0) {
				sign_qr = -1.0;
			}

			u = b
			    - ((b - c) * q - (b - a) * r)
			        / (2.0 * (sign_qr * std::max(std::abs(q - r), TINY)));
			RealType ulimit = b + max_step * (c - b);
			if((b - u) * (u - c) > 0.0) {
				// Interpolant lies between b and c
				fu = getFunctionValue(startParameters, gradient, u);
				num_evals = num_evals + 1;
				if(fu < fc) {
					//Have a minimum between b and c
					a = b;
					b = u;
					c = c;
					return;
				} else {
					if(fu > fb) {
						// Have a minimum between a and u
						a = a;
						b = c;
						c = u;
						return;
					}
				}
				// Quadratic interpolation didn't give a bracket, so take a golden step
				u = c + phi * (c - b);
			} else {
				if((c - u) * (u - ulimit) > 0.0) {
					// Interpolant lies between c and limit
					fu = getFunctionValue(startParameters, gradient, u);
					num_evals = num_evals + 1;
					if(fu < fc) {
						// Move bracket along, and then take a golden section step
						b = c;
						c = u;
						u = c + phi * (c - b);
					} else {
						bracket_found = 1;
					}
				} else {
					if((u - ulimit) * (ulimit - c) >= 0.0) {
						// Limit parabolic u to maximum value
						u = ulimit;
					} else {
						// Reject parabolic u and use golden section step
						u = c + phi * (c - b);
					}
				}
			}
			if(!bracket_found) {
				fu = getFunctionValue(startParameters, gradient, u);
				printf("1\n");
				num_evals = num_evals + 1;
			}
			a = b;
			b = c;
			c = u;
			fa = fb;
			fb = fc;
			fc = fu;

			printf("Bracketing: (%f, %f) (%f, %f) (%f, %f)\n", a, fa, b, fb, c,
			    fc);

		} // while loop
	} // bracket found

	printf("Bracketing finished: (%e %f) (%e, %f) (%e %f)\n", a, fa, b, fb, c,
	    fc);
	if(a > c) {
		RealType temp = c;
		c = a;
		a = temp;
	}
}

void LineSearchGradientFunctionUpdater::updateWeights(FeatureList *gradient) {
	updateFunction->getWeights(startParameters);

	RealType fpt = getFunctionValue(startParameters, gradient, 0);
	RealType lmin = 0.0;
	updateWeights(gradient, fpt, lmin);
}

RealType LineSearchGradientFunctionUpdater::updateWeights(
    FeatureList *gradient, RealType fpt, RealType &lmin) {
	// Value of golden section (1 + sqrt(5))/2.0
	const RealType phi = 1.6180339887499;
	const RealType cphi = 1 - 1 / phi;
	const RealType TOL = 1.0e-10; // Maximal fractional precision
	const RealType TINY = 1.0e-10; // Can't use fractional precision when minimum is at 0

	// Bracket the minimum

	RealType br_min = 0.0;
	RealType br_max = getParameter("LineSearchStepSizeScale");
	RealType br_mid = br_max;

	updateFunction->getWeights(startParameters);

	printf("Bracketing Minimum\n");
	bracketMinimum(startParameters, gradient, fpt, br_min, br_mid, br_max);
	printf("Done...\n");

// Use Brent's algorithm to find minimum
// Initialise the points and function values
	RealType w = br_mid; // Where second from minimum is
	RealType v = br_mid; // Previous value of w
	RealType x = v; // Where current minimum is
	RealType e = 0.0; // Distance moved on step before last
	RealType fx = getFunctionValue(startParameters, gradient, x);

	RealType fv = fx;
	RealType fw = fx;
	RealType fu = 0.0;

	for(int n = 1; n < maxSteps; n++) {
		RealType xm = 0.5 * (br_min + br_max); // Middle of bracket
		// Make sure that tolerance is big enough
		RealType tol1 = TOL * (fabs(x)) + TINY;
		// Decide termination on absolute precision required by options(2)
		if(fabs(x - xm) <= precision_treshold
		    && br_max - br_min < 4 * precision_treshold) {
			printf("Exiting Line Search x = %f - PrecisionThreshold\n", x);
			break;
		}

		// Check if step before last was big enough to try a parabolic step.
		// Note that this will fail on first iteration, which must be a golden
		// section step.

		RealType r = 0.0;
		RealType q = 0.0;
		RealType p = 0.0;
		RealType d = 0.0;
		RealType u = 0.0;

		if(fabs(e) > tol1) {
			// Construct a trial parabolic fit through x, v and w
			r = (fx - fv) * (x - w);
			q = (fx - fw) * (x - v);
			p = (x - v) * q - (x - w) * r;
			q = 2.0 * (q - r);
			if(q > 0.0) {
				p = -p;
			}
			q = fabs(q);
			// Test if the parabolic fit is OK
			if(fabs(p) >= fabs(0.5 * q * e) || p <= q * (br_min - x)
			    || p >= q * (br_max - x)) {
				// No it isn't, so take a golden section step
				if(x >= xm) {
					e = br_min - x;
				} else {
					e = br_max - x;
				}
				d = cphi * e;
			} else {
				// Yes it is, so take the parabolic step
				e = d;
				d = p / q;
				u = x + d;
				if(u - br_min < 2 * tol1 || br_max - u < 2 * tol1) {
					if(xm - x < 0) {
						d = -tol1;
					} else {
						d = tol1;
					}
				}
			}
		} else {
			// Step before last not big enough, so take a golden section step
			if(x >= xm) {
				e = br_min - x;
			} else {
				e = br_max - x;
			}

			d = cphi * e;
		}

		// Make sure that step is big enough
		if(fabs(d) >= tol1) {
			u = x + d;
		} else {
			if(d > 0) {
				u = x + tol1;
			} else {
				u = x - tol1;
			}
		}

		// Evaluate function at u
		fu = getFunctionValue(startParameters, gradient, u);

		// Reorganise bracket
		if(fu <= fx) {
			if(u >= x) {
				br_min = x;
			} else {
				br_max = x;
			}
			v = w;
			w = x;
			x = u;

			fv = fw;
			fw = fx;
			fx = fu;
		} else {
			if(u < x) {
				br_min = u;
			} else {
				br_max = u;
			}

			if(fu <= fw || w == x) {
				v = w;
				w = u;
				fv = fw;
				fw = fu;
			} else {
				if(fu <= fv || v == x || v == w) {
					v = u;
					fv = fu;
				}
			}
		}

		printf("Cycle %d  Error %f, br_min: %f, br_max: %f, x: %f\n", n, fx,
		    br_min, br_max, x);
	}

	setWorkingParamters(gradient, x, startParameters, workParameters);
	updateFunction->setWeights(workParameters);
	lmin = x;
	return fx;
}

GradientLearner::GradientLearner(GradientCalculator *l_gradientCalculator) {
	gradientCalculator = l_gradientCalculator;
	addParameters(gradientCalculator);
}

BatchGradientLearner::BatchGradientLearner(
    GradientCalculator *gradientCalculator, GradientFunctionUpdater *l_updater) :
	GradientLearner(gradientCalculator) {
	updater = l_updater;
	addParameters(updater);
	treshold_f = 0.0001;
	gradient = new FeatureList();
}

BatchGradientLearner::~BatchGradientLearner() {
	delete gradient;
}

RealType BatchGradientLearner::doOptimization(int maxSteps) {
	RealType fold = gradientCalculator->getFunctionValue();
	RealType fnew = fold;
	for(int i = 0; i < maxSteps; i++) {
		gradient->clear();

		printf("Getting gradient...\n");
		gradientCalculator->getGradient(gradient);
		printf("done... multiplying with -1 \n");
		gradient->multFactor(-1.0);
		printf("Updating weights...");
		updater->updateWeights(gradient);
		printf("done");

		fnew = gradientCalculator->getFunctionValue();
		printf("New Function Value (%d): %f\n", i, fnew);

		if(fabs(fnew - fold) < treshold_f) {
			printf("Change in Functionvalue below treshold - Exiting\n");
			break;
		}
		fold = fnew;
	}
	return fold;
}

ConjugateGradientLearner::ConjugateGradientLearner(
    GradientCalculator *gradientCalculator,
    LineSearchGradientFunctionUpdater *updater) :
	GradientLearner(gradientCalculator) {
	this->gradientUpdater = updater;

	addParameters(gradientUpdater);

	gradnew = new FeatureList();
	gradold = new FeatureList();
	d = new FeatureList();

	treshold_x = 0.0001;
	treshold_f = 0.0001;

	fnew = 0.0;
	exiting = 0;
}

ConjugateGradientLearner::~ConjugateGradientLearner() {
	delete gradnew;
	delete gradold;
	delete d;
}

RealType ConjugateGradientLearner::doOptimization(int maxGradientUpdates) {
	int niters = maxGradientUpdates;
	int maxExits = 10;
	RealType fold = 0.0;

//	RealType br_min = 0;
//	RealType br_max = 1.0;	// Initial value for maximum distance to search along

//	const RealType tol = 1.0e-10;;

	int j = 1;

//	RealType *parameters = new RealType[gradientFunction->getNumWeights()];
//	RealType *oldParameters = new RealType[gradientFunction->getNumWeights()];

	if(exiting > maxExits) {
		printf("Finished Optimization\n");
		return fnew;
	}

	while(j <= niters) {
//		memcpy(oldParameters, parameters, sizeof(RealType) * gradientFunction->getNumWeights());

		gradnew->clear();
		gradientCalculator->getGradient(gradnew);

//	    gradnew = feval(gradf, x, varargin{:});
//  options(11) = options(11) + 1;

		// Use Polak-Ribiere formula to update search direction
		RealType gg = gradnew->multFeatureList(gradnew);
		if(gg == 0.0) {
			printf("Gradient is 0 - Exiting\n");
			// If the gradient is zero then we are done.
			break;
		}

		if(d->size() == 0 || gradold->size() == 0) {

			fnew = gradientCalculator->getFunctionValue();
			printf("Calculated new value: %f \n", fnew);

			d->clear();
			d->add(gradnew, -1.0); // Initial search direction		
		} else {
			//gamma = ((gradnew - gradold)*(gradnew)')/gg;
			gradold->multFactor(-1.0);
			gradold->add(gradnew);

			RealType gamma = gradold->multFeatureList(gradnew) / gg;
			d->multFactor(gamma);
			d->add(gradnew, -1.0);
		}

		fold = fnew;
		gradold->clear();
		gradold->add(gradnew);

		// This shouldn't occur, but rest of code depends on d being downhill
		RealType prod_d = gradnew->multFeatureList(d);
		if(prod_d > 0) {
			d->clear();
			d->add(gradnew, -1.0);
			printf("search direction uphill in conjgrad\n");
		}
		RealType norm_d = sqrt(d->multFeatureList(d));

		RealType lmin = 0.0;

		d->multFactor(1.0 / norm_d);

		printf("Starting LineSearch\n");

		fnew = gradientUpdater->updateWeights(d, fold, lmin);

		printf("result: %e %f %f\n", lmin, fnew, fold);
		// Set x and fnew to be the actual search point we have found
		// x = xold + lmin * line_sd;
		// fnew = line_options(8);

		d->multFactor(norm_d);
		// Check for termination
		if(lmin * norm_d < treshold_x && fabs(fnew - fold) < treshold_f) {
			printf("Updates are smaller than treshold - Exiting\n");
			break;
		}

//	    d = (d .* gamma) - gradnew;
		printf("Cycle %d  Function %f\n", j, fnew);

		j = j + 1;
	}

// If we get here, then we haven't terminated in the given number of 
// iterations.
	if(j > niters) {
		printf("Maximum number of iterations reached\n");
		exiting = 0;
	} else {
		printf("Finished Optimization\n");
		d->clear();
		exiting++;
	}
	return fnew;
}

void ConjugateGradientLearner::resetOptimization() {
	GradientLearner::resetOptimization();
	exiting = 0;
	d->clear();
	gradold->clear();
}

#ifdef USE_TORCH

SupervisedNeuralNetworkMatlabLearner::SupervisedNeuralNetworkMatlabLearner(
    TorchGradientFunction *l_mlpFunction, int numHidden) {
	mlpFunction = l_mlpFunction;
	addParameter("MatlabNeuralNetNumNeurons", numHidden);
}

SupervisedNeuralNetworkMatlabLearner::~SupervisedNeuralNetworkMatlabLearner() {
	if(mlpFunction->getMachine() != nullptr) {
		delete mlpFunction->getMachine();
	}
}

void SupervisedNeuralNetworkMatlabLearner::learnFA(
    DataSet *inputData, DataSet1D *outputData
) {
	std::ofstream inputFile("InputData.csv");
	inputData->saveCSV(inputFile);

	std::ofstream targetFile("TargetData.csv");
	outputData->saveCSV(targetFile);

	int numHidden = (int) getParameter("MatlabNeuralNetNumNeurons");

	char syscall[250];
	sprintf(syscall, "matlab -r \"learnNeuralNet(%d)\"", numHidden);

	printf("System Call: %s\n", syscall);
	system(syscall);

	if(mlpFunction->getMachine() != nullptr) {
		delete mlpFunction->getMachine();
	}

	MLP *mlp = new MLP(3, inputData->getNumDimensions(), "linear", numHidden,
	    "tanh", numHidden, "linear", 1);

	mlpFunction->setGradientMachine(mlp);
	mlpFunction->loadDataFromFile("NetworkWeights.csv");

	char *systemCall = "rm NetworkWeights.csv";

	system(systemCall);

	char *filename = "NetworkWeights_config.data";

	std::ifstream configFile(filename);

	if(!configFile.good()) {
		std::cout << "Configuration file " << filename << " not found." << std::endl;
		exit(-1);
	}

	ColumnVector inputMean(3);
	ColumnVector inputStd(3);
	ColumnVector outputMean(1);
	ColumnVector outputStd(1);

	inputMean.setZero();
	inputStd.setConstant(1);

	outputMean.setZero();
	outputStd.setConstant(1);

	// load configuration
	while(!configFile.eof()) {
		char buffer[100];
		xscanf(configFile, "%s ", buffer);

		if(strcmp(buffer, "NUMHIDDEN:") == 0) {
			xscanf(configFile, "%d", numHidden);
			//		printf("NumHidden: %d\n", numHidden);
		}

		if(strcmp(buffer, "PREPROCESSING_INPUTMEAN:") == 0) {
			RealType buf1, buf2, buf3;
			xscanf(configFile, "%lf, %lf, %lf", buf1, buf2, buf3);
			inputMean(0) = buf1;
			inputMean(1) = buf2;
			inputMean(2) = buf3;

			//		printf("Input Mean : %lf, %lf, %lf\n", buf1, buf2, buf3);
		}
		if(strcmp(buffer, "PREPROCESSING_INPUTSTD:") == 0) {
			RealType buf1, buf2, buf3;
			xscanf(configFile, "%lf, %lf, %lf", buf1, buf2, buf3);
			inputStd(0) = buf1;
			inputStd(1) = buf2;
			inputStd(2) = buf3;

			//		printf("Input Std : %lf, %lf, %lf\n", buf1, buf2, buf3);
		}
		if(strcmp(buffer, "PREPROCESSING_OUTPUTMEAN:") == 0) {
			RealType buf1;
			xscanf(configFile, "%lf ", buf1);
			outputMean(0) = buf1;

			//		printf("Output Mean : %lf\n", buf1);
		}
		if(strcmp(buffer, "PREPROCESSING_OUTPUTSTD:") == 0) {
			RealType buf1;
			xscanf(configFile, "%lf ", buf1);
			outputStd(0) = buf1;
			//		printf("Output Std : %lf\n", buf1);
		}
	}

	systemCall = "rm NetworkWeights_config.data";
	system(systemCall);

	mlpFunction->setInputMean(&inputMean);
	mlpFunction->setOutputMean(&outputMean);
	mlpFunction->setInputStd(&inputStd);
	mlpFunction->setOutputStd(&outputStd);
}

void SupervisedNeuralNetworkMatlabLearner::resetLearner() {
	if(mlpFunction->getMachine() != nullptr) {
		delete mlpFunction->getMachine();
		mlpFunction->setGradientMachine(nullptr);
	}
}

SupervisedNeuralNetworkTorchLearner::SupervisedNeuralNetworkTorchLearner(
    TorchGradientFunction *l_mlpFunction) {
	mlpFunction = l_mlpFunction;
	addParameter("TorchLearningRate", 0.01);
	addParameter("TorchLearningRateDecay", 0.0);
	addParameter("TorchMaxIterations", -1);
	addParameter("TorchAccuracy", 0.0001);
}

SupervisedNeuralNetworkTorchLearner::~SupervisedNeuralNetworkTorchLearner() {
}

void SupervisedNeuralNetworkTorchLearner::learnFA(
    DataSet *inputData, DataSet1D *outputData) {
	learnWeightedFA(inputData, outputData, nullptr);
}

void SupervisedNeuralNetworkTorchLearner::learnWeightedFA(
    DataSet *inputData, DataSet1D *outputData, DataSet1D *weightingSet) {
	float *weighting = nullptr;

	ColumnVector inputMean(inputData->getNumDimensions());
	ColumnVector inputStd(inputData->getNumDimensions());

	inputData->getMean(nullptr, &inputMean);
	inputData->getVariance(nullptr, &inputStd);

	for(int i = 0; i < inputStd.rows(); i++) {
		inputStd(i) = sqrt(inputStd(i));
	}

	mlpFunction->setInputMean(&inputMean);
	mlpFunction->setInputStd(&inputStd);

	MeanStdPreprocessor preProc(inputData);

	preProc.preprocessDataSet(inputData);

	if(weightingSet) {
		weighting = new float[inputData->size()];

		for(int i = 0; i < weightingSet->size(); i++) {
			weighting[i] = (*weightingSet)[i];
		}
	}

	//Sequence *inputSequence = new Sequence(inputData->size(), inputData->getNumDimensions());

	//Sequence *outputSequence = new Sequence(inputData->size(), 1);
	Sequence **inputSequence = new Sequence *[inputData->size()];
	Sequence **outputSequence = new Sequence *[inputData->size()];

	for(int i = 0; i < inputData->size(); i++) {
		ColumnVector *inputVector = (*inputData)[i];
		inputSequence[i] = new Sequence(1, inputVector->rows());

		for(int j = 0; j < inputData->getNumDimensions(); j++) {
			inputSequence[i]->frames[0][j] = inputVector->operator[](j);
		}

		outputSequence[i] = new Sequence(1, 1);
		outputSequence[i]->frames[0][0] = (*outputData)[i];
	}

	MemoryDataSet *torchDataSet = new MemoryDataSet();

	torchDataSet->setInputs(inputSequence, inputData->size());
	torchDataSet->setTargets(outputSequence, inputData->size());

	Criterion *torchCriterion;

	if(weighting) {
		torchCriterion = new WeightedMSECriterion(torchDataSet, weighting);
	} else {
		torchCriterion = new WeightedMSECriterion(torchDataSet);
	}

	mlpFunction->getGradientMachine()->reset();
	StochasticGradient stochGrad(mlpFunction->getGradientMachine(),
	    torchCriterion);

	stochGrad.setROption("learning rate", getParameter("TorchLearningRate"));
	stochGrad.setROption("learning rate decay",
	    getParameter("TorchLearningRateDecay"));

	stochGrad.setIOption("max iter", getParameter("TorchMaxIterations"));
	stochGrad.setROption("end accuracy", getParameter("TorchAccuracy"));

	stochGrad.train(torchDataSet, nullptr);

	//mlpFunction->saveData(stdout);

	delete torchCriterion;
	delete torchDataSet;

	for(int i = 0; i < inputData->size(); i++) {
		delete inputSequence[i];
		delete outputSequence[i];
	}

	delete[] inputSequence;
	delete[] outputSequence;

	if(weighting) {
		delete weighting;
	}
}

void SupervisedNeuralNetworkTorchLearner::resetLearner() {}

#endif // USE_TORCH

}
//namespace rlearner

