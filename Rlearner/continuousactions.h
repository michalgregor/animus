// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ccontinuousactions_H_
#define Rlearner_ccontinuousactions_H_

#include "action.h"
#include "utility.h"
#include "qfunction.h"
#include "qetraces.h"
#include "agentcontroller.h"
#include "agentlistener.h"

#include "system.h"

namespace rlearner {

class AbstractQFunction;
class QFunction;

/**
 * Class for saving the continuous Values from a ContinuosAction.
 *
 * @see ActionData
 */
SYS_WNONVIRT_DTOR_OFF
class ContinuousActionData: public MultiStepActionData, public ColumnVector {
SYS_WNONVIRT_DTOR_ON
public:
	ContinuousActionProperties *properties;

public:
	virtual void setActionValue(int dim, RealType value);RealType getActionValue(
	        int dim);

	void normalizeAction();
	RealType getDistance(ColumnVector *vector);

	virtual void save(std::ostream& stream);
	virtual void load(std::istream& stream);

	virtual void setData(ActionData *actionData);
	void initData(RealType initVal);

public:
	ContinuousActionData& operator=(const ContinuousActionData& obj) {
		MultiStepActionData::operator=(obj);
		ColumnVector::operator=(obj);
		properties = obj.properties;
		return *this;
	}

	ContinuousActionData(const ContinuousActionData& obj):
		MultiStepActionData(obj), ColumnVector(obj),
		properties(obj.properties) {}

	ContinuousActionData(ContinuousActionProperties *properties);
	virtual ~ContinuousActionData();
};

class ContinuousActionProperties {
protected:
	unsigned int numActionValues;
	RealType *minValues;
	RealType *maxValues;

public:
	unsigned int getNumActionValues();

	RealType getMinActionValue(int dim);RealType getMaxActionValue(int dim);

	void setMinActionValue(int dim, RealType value);
	void setMaxActionValue(int dim, RealType value);

public:
	ContinuousActionProperties& operator=(const ContinuousActionProperties&) = delete;
	ContinuousActionProperties(const ContinuousActionProperties&) = delete;

	ContinuousActionProperties(int numActionValues);
	virtual ~ContinuousActionProperties();
};

class ContinuousAction: public PrimitiveAction {
protected:
	ContinuousActionData *continuousActionData;
	ContinuousActionProperties *properties;

public:
	ContinuousActionProperties *getContinuousActionProperties();

	virtual ContinuousActionData *getContinuousActionData() {
		return continuousActionData;
	}

	virtual ActionData *getNewActionData();

	RealType getActionValue(int dim);
	unsigned int getNumDimensions();

	virtual void loadActionData(ActionData *data);

	virtual bool equals(Action *action);
	virtual bool isSameAction(Action *action, ActionData *data);

protected:
	ContinuousAction(ContinuousActionProperties *properties,
	        ContinuousActionData *actionData);

public:
	ContinuousAction& operator=(const ContinuousAction&) = delete;
	ContinuousAction(const ContinuousAction&) = delete;

	ContinuousAction(ContinuousActionProperties *properties);
	virtual ~ContinuousAction();
};

/**
 * @enum RandomControllerType
 */
enum RandomControllerType {
	RandomController_NoRandom = 0, //!< RandomController_NoRandom
	RandomController_Extern = 1, //!< RandomController_Extern
	RandomController_Intern = 2 //!< RandomController_Intern
};

class ContinuousActionRandomPolicy;

class ContinuousActionController: public AgentController {
protected:
	ContinuousAction *contAction;

	ContinuousActionRandomPolicy *randomController;
	ContinuousActionData *noise;

	int randomControllerMode;

public:
	virtual Action *getNextAction(StateCollection *state,
	        ActionDataSet *data = nullptr);
	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *action) = 0;

	virtual ContinuousActionProperties *getContinuousActionProperties() {
		return contAction->getContinuousActionProperties();
	}

	virtual ContinuousAction *getContinuousAction() {
		return contAction;
	}

	virtual void setRandomController(
	        ContinuousActionRandomPolicy *randomController);
	virtual ContinuousActionRandomPolicy *getRandomController();

	void setRandomControllerMode(int randomControllerMode);
	int getRandomControllerMode();

	virtual void getNoise(StateCollection *state,
	        ContinuousActionData *action, ContinuousActionData *noise);

public:
	ContinuousActionController& operator=(const ContinuousActionController&) = delete;
	ContinuousActionController(const ContinuousActionController&) = delete;

	ContinuousActionController(ContinuousAction *contAction,
	        int randomControllerMode = 1);
	virtual ~ContinuousActionController();
};

class StaticContinuousAction: public ContinuousAction {
protected:
	ContinuousAction *contAction;
	RealType maximumDistance;

public:
	virtual void setContinuousAction(ContinuousActionData *contAction);
	virtual void addToContinuousAction(ContinuousActionData *contAction,
	        RealType factor);

	ContinuousAction *getContinuousAction();

	virtual void loadActionData(ActionData *) {}

	virtual void setData(ActionData *) {
		assert(false);
	}

	virtual bool equals(Action *action);
	virtual bool isSameAction(Action *action, ActionData *data);

	virtual RealType getMaximumDistance();

public:
	StaticContinuousAction& operator=(const StaticContinuousAction&) = delete;
	StaticContinuousAction(const StaticContinuousAction&) = delete;

	StaticContinuousAction(ContinuousAction *properties, RealType *actionValues,
		        RealType maximumDistance = 0.0);
	virtual ~StaticContinuousAction();
};

class LinearFAContinuousAction: public StaticContinuousAction {
public:
	virtual RealType getActionFactor(ContinuousActionData *contAction) = 0;

public:
	LinearFAContinuousAction(ContinuousAction *properties,
	        RealType *actionValues);
	virtual ~LinearFAContinuousAction() = default;
};

class ContinuousRBFAction: public LinearFAContinuousAction {
protected:
	RealType *rbfSigma;

public:
	virtual RealType getActionFactor(ContinuousActionData *contAction);

public:
	ContinuousRBFAction& operator=(const ContinuousRBFAction&) = delete;
	ContinuousRBFAction(const ContinuousRBFAction&) = delete;

	ContinuousRBFAction(ContinuousAction *properties, RealType *rbfCenter,
	        RealType *rbfSigma);
	virtual ~ContinuousRBFAction();
};

class ContinuousActionLinearFA {
protected:
	ActionSet *contActions;
	ContinuousActionProperties *actionProperties;

public:
	void getActionFactors(ContinuousActionData *action, RealType *actionFactors);

	void getContinuousAction(unsigned int index, ContinuousActionData *action);
	void getContinuousAction(ContinuousActionData *action,
	        RealType *actionFactors);

	int getNumContinuousActionFA();

public:
	ContinuousActionLinearFA& operator=(const ContinuousActionLinearFA&) = delete;
	ContinuousActionLinearFA(const ContinuousActionLinearFA&) = delete;

	ContinuousActionLinearFA(ActionSet *contActions,
	        ContinuousActionProperties *properties);
	virtual ~ContinuousActionLinearFA();
};

class CALinearFAQETraces;

class ContinuousActionQFunction: public GradientQFunction {
protected:
	ContinuousAction *contAction;

public:
	virtual Action *getMax(StateCollection *, ActionSet *availableActions,
	        ActionDataSet *actionDatas);

	virtual void getBestContinuousAction(StateCollection *state,
	        ContinuousActionData *actionData) = 0;

	virtual void updateValue(StateCollection *state, Action *action,
	        RealType td, ActionData *data = nullptr);

	/**
	 * Sets the Value of the value function assigned to the given action.
	 *
	 * Calls the setValue Function of the specified value function.
	 */
	virtual void setValue(StateCollection *state, Action *action,
	        RealType qValue, ActionData *data = nullptr);

	/**
	 * Returns the Value of the value function assigned to the given action.
	 *
	 * Returns the value of  the getValue Function of the specified value
	 * function.
	 */
	virtual RealType getValue(StateCollection *state, Action *action,
	        ActionData *data = nullptr);

	virtual void updateCAValue(StateCollection *state,
	        ContinuousActionData *data, RealType td);
	virtual void setCAValue(StateCollection *state,
	        ContinuousActionData *data, RealType qValue);
	virtual RealType getCAValue(StateCollection *state,
	        ContinuousActionData *data) = 0;

	virtual void getGradient(StateCollection *state, Action *action,
	        ActionData *data, FeatureList *gradient);
	virtual void getCAGradient(StateCollection *state,
	        ContinuousActionData *data, FeatureList *gradient);

	ContinuousAction *getContinuousActionObject() {
		return contAction;
	}

	virtual int getNumWeights() {
		return 0;
	}

	virtual void getWeights(RealType *parameters);
	virtual void setWeights(RealType *parameters);

public:
	ContinuousActionQFunction& operator=(const ContinuousActionQFunction&) = delete;
	ContinuousActionQFunction(const ContinuousActionQFunction&) = delete;

	ContinuousActionQFunction(ContinuousAction *contAction);
	virtual ~ContinuousActionQFunction();
};

class CALinearFAQFunction: public ContinuousActionQFunction,
        public ContinuousActionLinearFA {
protected:
	RealType *actionFactors;RealType *CAactionValues;
	QFunction *qFunction;

	FeatureList *tempGradient;

protected:
	virtual void updateWeights(FeatureList *features);

public:
	virtual void getBestContinuousAction(StateCollection *state,
	        ContinuousActionData *actionData);

	virtual void updateCAValue(StateCollection *state,
	        ContinuousActionData *data, RealType td);
	virtual void setCAValue(StateCollection *state,
	        ContinuousActionData *data, RealType qValue);
	virtual RealType getCAValue(StateCollection *state,
	        ContinuousActionData *data);

	QFunction *getQFunctionForCA();

	virtual AbstractQETraces* getStandardETraces();
	virtual void getCAGradient(StateCollection *state,
	        ContinuousActionData *action, FeatureList *gradient);

	virtual int getNumWeights();

	virtual void getWeights(RealType *weights);
	virtual void setWeights(RealType *weights);

	virtual int getWeightsOffset(Action *) {
		return 0;
	}

public:
	CALinearFAQFunction& operator=(const CALinearFAQFunction&) = delete;
	CALinearFAQFunction(const CALinearFAQFunction&) = delete;

	CALinearFAQFunction(QFunction *qFunction,
	        ContinuousAction *returnAction);
	virtual ~CALinearFAQFunction();
};

class CALinearFAQETraces: public QETraces {
protected:
	RealType *actionFactors;
	CALinearFAQFunction *contQFunc;

public:
	virtual void addETrace(StateCollection *State, Action *action,
	        RealType factor = 1.0, ActionData *data = nullptr);

public:
	CALinearFAQETraces& operator=(const CALinearFAQETraces&) = delete;
	CALinearFAQETraces(const CALinearFAQETraces&) = delete;

	CALinearFAQETraces(CALinearFAQFunction *qfunction);
	virtual ~CALinearFAQETraces();
};

class ActionDistribution;

class ContinuousActionPolicy: public ContinuousActionController {
protected:
	ActionDistribution *distribution;
	RealType *actionValues;
	AbstractQFunction *continuousActionQFunc;

	ActionSet *continuousStaticActions;

public:
	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *action);

public:
	ContinuousActionPolicy& operator=(const ContinuousActionPolicy&) = delete;
	ContinuousActionPolicy(const ContinuousActionPolicy&) = delete;

	ContinuousActionPolicy(ContinuousAction *contAction,
	        ActionDistribution *distribution,
	        AbstractQFunction *continuousActionQFunc,
	        ActionSet *continuousStaticActions);
	virtual ~ContinuousActionPolicy();
};

class ContinuousActionRandomPolicy: public ContinuousActionController,
        public SemiMDPListener {
protected:
	ColumnVector *lastNoise;
	ColumnVector *currentNoise;

	RealType sigma;
	RealType alpha;

public:
	virtual void newEpisode();
	virtual void nextStep(StateCollection *, Action *, StateCollection *);

	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *action);

	virtual void onParametersChanged();

	ColumnVector *getCurrentNoise();
	ColumnVector *getLastNoise();

public:
	ContinuousActionRandomPolicy& operator=(const ContinuousActionRandomPolicy&) = delete;
	ContinuousActionRandomPolicy(const ContinuousActionRandomPolicy&) = delete;

	ContinuousActionRandomPolicy(ContinuousAction *action, RealType sigma,
	        RealType alpha);
	virtual ~ContinuousActionRandomPolicy();
};

class ContinuousActionAddController: public ContinuousActionController {
protected:
	std::list<ContinuousActionController *> *controllers;
	std::map<ContinuousActionController *, RealType> *controllerWeights;
	ColumnVector *actionValues;

public:
	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *action);

	void addContinuousActionController(ContinuousActionController *controller,
	        RealType weight = 1.0);
	void setControllerWeight(ContinuousActionController *controller,
	        RealType weight);RealType getControllerWeight(
	        ContinuousActionController *controller);

public:
	ContinuousActionAddController& operator=(const ContinuousActionAddController&) = delete;
	ContinuousActionAddController(const ContinuousActionAddController&) = delete;

	ContinuousActionAddController(ContinuousAction *action);
	virtual ~ContinuousActionAddController();
};

} //namespace rlearner

#endif //Rlearner_ccontinuousactions_H_
