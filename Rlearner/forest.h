// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cforest_H_
#define Rlearner_cforest_H_

#include "trees.h"
#include "inputdata.h"

namespace rlearner {

//! Forest.
template<typename TreeData> class Forest {
protected:
	Tree<TreeData> **forest;
	int numTrees;

public:
	virtual void getTreeDatas(ColumnVector *vector, TreeData *outputs);
	virtual void addTree(int index, Tree<TreeData> *tree);
	virtual void removeTree(int index);

	Tree<TreeData> *getTree(int index);
	int getNumTrees();
	int getNumLeaves();

	RealType getAverageDepth();
	RealType getAverageNumLeaves();

	virtual void getActiveLeafNumbers(ColumnVector *vector, int *leafNumbers);
	virtual void getActiveLeaves(ColumnVector *vector,
	        Leaf<TreeData> **leafNumbers);

public:
	Forest(int numTrees);
	virtual ~Forest();
};

template<typename TreeData>
Forest<TreeData>::Forest(int l_numTrees) {
	numTrees = l_numTrees;
	forest = new Tree<TreeData> *[numTrees];

	for (int i = 0; i < numTrees; i++) {
		forest[i] = nullptr;
	}
}

template<typename TreeData>
Forest<TreeData>::~Forest() {
	delete[] forest;
}

template<typename TreeData>
void Forest<TreeData>::getActiveLeafNumbers(ColumnVector *vector, int *leafNumbers) {
	int leaveSum = 0;

	for (int i = 0; i < numTrees; i++) {
		leafNumbers[i] = forest[i]->getLeaf(vector)->getLeafNumber() + leaveSum;
		leaveSum += forest[i]->getNumLeaves();
	}
}

template<typename TreeData>
void Forest<TreeData>::getActiveLeaves(ColumnVector *vector, Leaf<TreeData> **leaves) {
	for (int i = 0; i < numTrees; i++) {
		leaves[i] = forest[i]->getLeaf(vector);
	}
}

template<typename TreeData>
int Forest<TreeData>::getNumLeaves() {
	int numLeaves = 0;
	for (int i = 0; i < numTrees; i++) {
		numLeaves += forest[i]->getNumLeaves();
	}
	return numLeaves;
}

template<typename TreeData>
RealType Forest<TreeData>::getAverageDepth() {
	RealType depth = 0;
	for (int i = 0; i < numTrees; i++) {
		depth += forest[i]->getDepth();
	}
	return depth / numTrees;
}

template<typename TreeData>
RealType Forest<TreeData>::getAverageNumLeaves() {

	RealType leaves = 0;
	for (int i = 0; i < numTrees; i++) {
		leaves += forest[i]->getNumLeaves();
	}
	return leaves / numTrees;
}

template<typename TreeData>
Tree<TreeData> *Forest<TreeData>::getTree(int index) {
	return forest[index];
}

template<typename TreeData>
void Forest<TreeData>::getTreeDatas(ColumnVector *vector, TreeData *outputs) {
	for (int i = 0; i < numTrees; i++) {
		if (forest[i] != nullptr) {
			TreeData element = forest[i]->getOutputValue(vector);
			outputs[i] = element;
		}
	}
}

template<typename TreeData>
void Forest<TreeData>::addTree(int index, Tree<TreeData> *tree) {
	forest[index] = tree;
}

template<typename TreeData>
void Forest<TreeData>::removeTree(int index) {
	forest[index] = nullptr;
}

template<typename TreeData>
int Forest<TreeData>::getNumTrees() {
	return numTrees;
}

//! RegressionForest.

class RegressionForest: public Forest<RealType>, public Mapping<RealType> {
protected:
	virtual RealType doGetOutputValue(ColumnVector *vector);

public:
	virtual void save(std::ostream& stream);

public:
	RegressionForest(int numTrees, int numDim);
	virtual ~RegressionForest();
};

class ExtraTreeRegressionForest: public RegressionForest {
public:
	ExtraTreeRegressionForest(int numTrees, DataSet *inputData,
		DataSet1D *outputData, unsigned int K, unsigned int n_min,
		RealType treshold, DataSet1D *weightData = nullptr
	);

	virtual ~ExtraTreeRegressionForest();
};

class RegressionMultiMapping: public Mapping<RealType> {
protected:
	Mapping<RealType> **mappings;
	int numMappings;

protected:
	RealType doGetOutputValue(ColumnVector *inputVector);

public:
	bool deleteMappings;

public:
	void addMapping(int index, Mapping<RealType> *mapping);

public:
	RegressionMultiMapping(int numMappings, int numDimensions);
	virtual ~RegressionMultiMapping();
};

} //namespace rlearner

#endif //Rlearner_cforest_H_
