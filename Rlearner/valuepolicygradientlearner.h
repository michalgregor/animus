// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cvaluepolicygradientlearner_H_
#define Rlearner_cvaluepolicygradientlearner_H_

#include "algebra.h"
#include "agentlistener.h"
#include <set>

namespace rlearner {

class FeatureList;
class AbstractVFunction;
class VFunctionInputDerivationCalculator;
class ContinuousActionGradientPolicy;
class CAGradientPolicyInputDerivationCalculator;
class ContinuousActionData;
class StateGradient;
class StateReward;
class TransitionFunction;
class TransitionFunctionInputDerivationCalculator;
class StateCollectionImpl;
class FeatureList;
class StateModifier;
class StateCollection;
class StateModifierList;

class VPolicyLearner: public SemiMDPRewardListener {
protected:
	typedef std::list<FeatureList *> StateGradient;

	AbstractVFunction *vFunction;
	VFunctionInputDerivationCalculator *vFunctionInputDerivation;
	ContinuousActionGradientPolicy *gradientPolicy;
	CAGradientPolicyInputDerivationCalculator *policydInput;

	ColumnVector *dReward;
	ColumnVector *dVFunction;
	Matrix *dPolicy;
	Matrix *dModelInput;

	ContinuousActionData *data;
	std::list<StateGradient *> *stateGradients;

	StateGradient *stateGradient1;
	StateGradient *stateGradient2;
	StateGradient *dModelGradient;

	StateReward *rewardFunction;
	TransitionFunction *dynModel;
	TransitionFunctionInputDerivationCalculator *dynModeldInput;

	StateCollectionImpl *tempStateCol;
	FeatureList *policyGradient;

	std::list<StateCollectionImpl *> *pastStates;
	std::list<ColumnVector *> *pastDRewards;
	std::list<ContinuousActionData *> *pastActions;
	std::list<StateCollectionImpl *> *statesResource;
	std::list<ColumnVector *> *rewardsResource;
	std::list<ContinuousActionData *> *actionsResource;
	std::set<StateModifierList*> _modifierLists;

protected:
	void getDNextState(
	    StateGradient *stateGradient1, StateGradient *stateGradient2,
	    StateCollection *currentState, ContinuousActionData *data
	);

	void multMatrixFeatureList(
	    Matrix *matrix, FeatureList *features, int index,
	    std::list<FeatureList *> *newFeatures
	);

public:
	virtual void nextStep(
	    StateCollection *oldState, Action *action, RealType reward,
	    StateCollection *nextState
	);

	virtual void newEpisode();

	void calculateGradient(
	    std::list<StateCollectionImpl *> *states,
	    std::list<ColumnVector *> *Drewards,
	    std::list<ContinuousActionData *> *actionDatas,
	    FeatureList *policyGradient
	);

public:
	VPolicyLearner(
	    StateReward *rewardFunction, TransitionFunction *dynModel,
	    TransitionFunctionInputDerivationCalculator *dynModeldInput,
	    AbstractVFunction *vFunction,
	    VFunctionInputDerivationCalculator *vFunctionInputDerivation,
	    ContinuousActionGradientPolicy *gradientPolicy,
	    CAGradientPolicyInputDerivationCalculator *policydInput,
	    const std::set<StateModifierList*>& modifierLists,
	    int nForwardView
	);

	virtual ~VPolicyLearner();
};

} //namespace rlearner

#endif //Rlearner_cvaluepolicygradientlearner_H_
