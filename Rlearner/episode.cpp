// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>

#include "ril_debug.h"
#include "episode.h"
#include "statecollection.h"
#include "action.h"
#include "ril_debug.h"
#include "statemodifier.h"

namespace rlearner {

Episode::Episode(
    StateProperties *properties, ActionSet *actions, bool autoNewEpisode) :
	StateModifiersObject(properties), StepHistory(properties, actions) {
	this->autoNewEpisode = autoNewEpisode;

	stateCollectionList = new StateCollectionList(properties);
	actionList = new ActionList(actions);
}

Episode::Episode(
    StateProperties *properties, ActionSet *actions,
    const std::set<StateModifierList*>& modifierLists, bool autoNewEpisode) :
	StateModifiersObject(properties), StepHistory(properties, actions) {
	this->autoNewEpisode = autoNewEpisode;

	stateCollectionList = new StateCollectionList(properties);
	actionList = new ActionList(actions);

	for(auto& list: modifierLists) {
		addModifierList(list);
	}
}

Episode::~Episode() {
	newEpisode();

	delete actionList;
	delete stateCollectionList;
}

void Episode::resetData() {
	stateCollectionList->clearStateLists();
	actionList->clear();
}

void Episode::nextStep(
    StateCollection *oldState, Action *action, StateCollection *newState) {
	if(getNumSteps() == 0) {
		stateCollectionList->addStateCollection(oldState);
	}

	stateCollectionList->addStateCollection(newState);

	actionList->addAction(action);
}

void Episode::newEpisode() {
	stateCollectionList->clearStateLists();
	actionList->clear();
}

int Episode::getNumSteps() {
	return actionList->getSize();
}

void Episode::saveData(std::ostream& stream) {
	stateCollectionList->save(stream);
	actionList->save(stream);
}

void Episode::loadData(std::istream& stream) {
	stateCollectionList->load(stream);
	actionList->load(stream);
}

void Episode::getStep(int num, Step *step) {
	assert(num < getNumSteps());

	stateCollectionList->getStateCollection(num, step->oldState);
	stateCollectionList->getStateCollection(num + 1, step->newState);

	step->action = actionList->getAction(num, step->actionData);
}

void Episode::getStateCollection(
    int index, StateCollectionImpl *stateCollection) {
	stateCollectionList->getStateCollection(index, stateCollection);
}

void Episode::getState(int index, State *state) {
	stateCollectionList->getState(index, state);
}

StateList *Episode::getStateList(StateProperties *properties) {
	return stateCollectionList->getStateList(properties);
}

void Episode::addModifierList(StateModifierList* modifierList) {
	stateCollectionList->addModifierList(modifierList);
	StateModifiersObject::addModifierList(modifierList);
}

void Episode::removeModifierList(StateModifierList* modifierList) {
	stateCollectionList->removeModifierList(modifierList);
	StateModifiersObject::removeModifierList(modifierList);
}

Action *Episode::getAction(unsigned int num, ActionDataSet *dataSet) {
	return actionList->getAction(num, dataSet);
}

int Episode::getNumStateCollections() {
	return stateCollectionList->getNumStateCollections();
}

} //namespace rlearner
