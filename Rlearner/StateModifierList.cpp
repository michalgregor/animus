#include "StateModifierList.h"

namespace rlearner {

void StateModifierList::addStateModifier(StateModifier* modifier) {
	iterator first, last;
	first = last = _modifiers.insert(modifier).first; last++;

	for(auto& reporter: _reporters) {
		reporter->modifiersAdded(first, last);
	}
}

void StateModifierList::removeStateModifier(StateModifier* modifier) {
	iterator first, last;
	first = last = _modifiers.find(modifier); last++;

	if(first == _modifiers.end()) return;

	for(auto& reporter: _reporters) {
		reporter->modifiersRemoved(first, last);
	}
}

void StateModifierList::addReporter(ModifierListReporter* reporter) {
	_reporters.insert(reporter);
}

void StateModifierList::removeReporter(ModifierListReporter* reporter) {
	auto iter = _reporters.find(reporter);
	if(iter != _reporters.end()) _reporters.erase(iter);
}

void StateModifierList::clear() {
	for(auto& reporter: _reporters) {
		reporter->modifiersRemoved(_modifiers.begin(), _modifiers.end());
	}

	_modifiers.clear();
}

ModifierListReporter::~ModifierListReporter() {}

} // namespace rlearner
