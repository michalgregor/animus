// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>
#include <cassert>
#include <sstream>
#include <string>
#include <fstream>

#include "ril_debug.h"
#include "analyzer.h"
#include "qfunction.h"
#include "statecollection.h"
#include "action.h"
#include "stateproperties.h"
#include "state.h"
#include "vfunction.h"
#include "history.h"
#include "episodehistory.h"
#include "inputdata.h"
#include "kdtrees.h"
#include "nearestneighbor.h"
#include "evaluator.h"
#include "system.h"

namespace rlearner {

VFunctionAnalyzer::VFunctionAnalyzer(
    AbstractVFunction *vFunction, StateProperties *modelState,
    const std::set<StateModifierList*>& modifierLists) {
	this->vFunction = vFunction;
	this->modelStateProperties = modelState;

	stateCollection = new StateCollectionImpl(modelState, modifierLists);
}

VFunctionAnalyzer::~VFunctionAnalyzer() {
	delete stateCollection;
}

void VFunctionAnalyzer::save1DValues(
    std::ostream& stream, State *initState, int dim1, int part1
) {
	RealType width = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);
	fmt::fprintf(stream, "VFunction Analyzer 1D-Table for Dimension %d\n", dim1);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	for(int i = 0; i <= part1; i++) {
		RealType x = modelStateProperties->getMinValue(dim1) + i * width / part1;
		modelState->setContinuousState(dim1, x);
		stateCollection->newModelState();
		RealType value = vFunction->getValue(stateCollection);
		fmt::fprintf(stream, "%f %f\n", x, value);
	}
}

void VFunctionAnalyzer::save1DMatlab(
	std::ostream& stream, State *initState, int dim1, int part1
) {
	RealType width = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	for(int i = 0; i <= part1; i++) {
		RealType x = modelStateProperties->getMinValue(dim1) + i * width / part1;
		modelState->setContinuousState(dim1, x);
		stateCollection->newModelState();
		RealType value = vFunction->getValue(stateCollection);
		fmt::fprintf(stream, "%f %f\n", x, value);
	}
}

void VFunctionAnalyzer::saveDiscrete1DValues(
	std::ostream& stream, State *initState, int dim1
) {
	int numDiscStates = modelStateProperties->getDiscreteStateSize(dim1);

	State *modelState = stateCollection->getState(modelStateProperties);

	fmt::fprintf(stream, "VFunction Analyzer 1D-Table for Dimension %d\n", dim1);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	modelState->setState(initState);

	for(int i = 0; i < numDiscStates; i++) {
		modelState->setDiscreteState(dim1, i);
		stateCollection->newModelState();
		RealType value = vFunction->getValue(stateCollection);
		fmt::fprintf(stream, "%d %f\n", i, value);
	}
}

void VFunctionAnalyzer::saveDiscrete1DMatlab(
	std::ostream& stream, State *initState, int dim1
) {
	int numDiscStates = modelStateProperties->getDiscreteStateSize(dim1);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	for(int i = 0; i < numDiscStates; i++) {
		modelState->setDiscreteState(dim1, i);
		stateCollection->newModelState();
		RealType value = vFunction->getValue(stateCollection);
		fmt::fprintf(stream, "%d %f\n", i, value);
	}
}

void VFunctionAnalyzer::save2DValues(
	std::ostream& stream, State *initState, int dim1, int part1, int dim2, int part2
) {
	RealType width1 = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	RealType width2 = modelStateProperties->getMaxValue(dim2)
	    - modelStateProperties->getMinValue(dim2);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);
	fmt::fprintf(stream, "VFunction Analyzer 2D-Table for Dimensions %d and %d\n",
	    dim1, dim2);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	for(int i = 0; i <= part1; i++) {

		RealType x = modelStateProperties->getMinValue(dim1) + i * width1 / part1;

		modelState->setContinuousState(dim1, x);

		for(int j = 0; j <= part2; j++) {

			RealType y = modelStateProperties->getMinValue(dim2)
			    + j * width2 / part2;

			modelState->setContinuousState(dim2, y);

			stateCollection->newModelState();

			RealType value = vFunction->getValue(stateCollection);

			fmt::fprintf(stream, "%f ", value);
		}
		fmt::fprintf(stream, "\n");
	}
}

void VFunctionAnalyzer::save2DMatlab(
	std::ostream& stream, State *initState, int dim1, int part1, int dim2, int part2
) {
	RealType width1 = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	RealType width2 = modelStateProperties->getMaxValue(dim2)
	    - modelStateProperties->getMinValue(dim2);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	fmt::fprintf(stream, "VFunction Analyzer 2D-Table for Dimensions %d and %d\n",
	    dim1, dim2);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	for(int i = 0; i <= part1; i++) {
		RealType x = modelStateProperties->getMinValue(dim1) + i * width1 / part1;

		modelState->setContinuousState(dim1, x);

		for(int j = 0; j <= part2; j++) {

			RealType y = modelStateProperties->getMinValue(dim2)
			    + j * width2 / part2;

			modelState->setContinuousState(dim2, y);

			stateCollection->newModelState();

			RealType value = vFunction->getValue(stateCollection);

			fmt::fprintf(stream, "%f %f %f\n", x, y, value);
		}
	}
}

void VFunctionAnalyzer::saveDiscrete2DValues(
	std::ostream& stream, State *initState, int row, int col
) {
	int numDiscStates1 = modelStateProperties->getDiscreteStateSize(row);
	int numDiscStates2 = modelStateProperties->getDiscreteStateSize(col);

	State *modelState = stateCollection->getState(modelStateProperties);
	modelState->setState(initState);

	for(int i = 0; i < numDiscStates1; i++) {

		modelState->setDiscreteState(row, i);

		for(int j = 0; j < numDiscStates2; j++) {

			modelState->setDiscreteState(col, j);

			stateCollection->newModelState();

			RealType value = vFunction->getValue(stateCollection);

			if(j < numDiscStates2 - 1) fmt::fprintf(stream, "%f, ", value);
			else fmt::fprintf(stream, "%f\n", value);
		}
	}
}

void VFunctionAnalyzer::saveDiscrete2DMatlab(
	std::ostream& stream, State *initState, int row, int col
) {
	int numDiscStates1 = modelStateProperties->getDiscreteStateSize(row);
	int numDiscStates2 = modelStateProperties->getDiscreteStateSize(col);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	for(int i = 0; i < numDiscStates1; i++) {

		modelState->setDiscreteState(row, i);

		for(int j = 0; j < numDiscStates2; j++) {

			modelState->setDiscreteState(col, j);

			stateCollection->newModelState();

			RealType value = vFunction->getValue(stateCollection);
			fmt::fprintf(stream, "%d %d %f\n", i, j, value);
		}
	}
}

void VFunctionAnalyzer::saveStateValues(
	std::ostream& stream, StateList *states
) {
	State *modelState = stateCollection->getState(modelStateProperties);

	fmt::fprintf(stream, "VFunction Analyzer State Value-Table\n");

	for(unsigned int i = 0; i < states->getNumStates(); i++) {
		states->getState(i, modelState);
		stateCollection->newModelState();

		RealType value = vFunction->getValue(stateCollection);
		fmt::fprintf(stream, "(");
		modelState->save(stream);
		fmt::fprintf(stream, ", %f)\n", value);
	}
}

void VFunctionAnalyzer::save1DValues(
    const std::string& filename, State *initstate, int dim1, int part1
) {
	std::ofstream stream(filename);

	if(!stream.good()) {
		std::cerr << "VFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save1DValues(stream, initstate, dim1, part1);
}

void VFunctionAnalyzer::saveDiscrete1DValues(
	const std::string& filename, State *initstate, int dim1
) {
	std::ofstream stream(filename);

	if(!stream.good()) {
		std::cerr << "VFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete1DValues(stream, initstate, dim1);
}

void VFunctionAnalyzer::save2DValues(
	const std::string& filename, State *initstate, int dim1, int part1,
	int dim2, int part2
) {
	std::ofstream stream(filename);

	if(!stream.good()) {
		std::cerr << "VFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save2DValues(stream, initstate, dim1, part1, dim2, part2);
}

void VFunctionAnalyzer::saveDiscrete2DValues(
	const std::string& filename, State *initstate, int row, int col
) {
	std::ofstream stream(filename);

	if(!stream.good()) {
		std::cerr << "VFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete2DValues(stream, initstate, row, col);
}

void VFunctionAnalyzer::save1DMatlab(
	const std::string& filename, State *initstate, int dim1, int part1
) {
	std::ofstream stream(filename);

	if(!stream.good()) {
		std::cerr << "VFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save1DMatlab(stream, initstate, dim1, part1);
}

void VFunctionAnalyzer::saveDiscrete1DMatlab(
	const std::string& filename, State *initstate, int dim1
) {
	std::ofstream stream(filename);

	if(!stream.good()) {
		std::cerr << "VFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete1DMatlab(stream, initstate, dim1);
}

void VFunctionAnalyzer::save2DMatlab(
	const std::string& filename, State *initstate, int dim1, int part1,
	int dim2, int part2
) {
	std::ofstream stream(filename);

	if(!stream.good()) {
		std::cerr << "VFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save2DMatlab(stream, initstate, dim1, part1, dim2, part2);
}

void VFunctionAnalyzer::saveDiscrete2DMatlab(
	const std::string& filename, State *initstate, int row, int col
) {
	std::ofstream stream(filename);

	if(!stream.good()) {
		std::cerr << "VFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete2DMatlab(stream, initstate, row, col);
}

void VFunctionAnalyzer::saveStateValues(
	const std::string& filename, StateList *states
) {
	std::ofstream stream(filename);

	if(!stream.good()) {
		std::cerr << "VFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveStateValues(stream, states);
}

void VFunctionAnalyzer::setVFunction(AbstractVFunction *l_vFunction) {
	this->vFunction = l_vFunction;
}

QFunctionAnalyzer::QFunctionAnalyzer(
    AbstractQFunction *qFunction, StateProperties *modelState,
    const std::set<StateModifierList*>& modifierLists) {
	this->qFunction = qFunction;
	this->modelStateProperties = modelState;

	stateCollection = new StateCollectionImpl(modelState, modifierLists);
}

QFunctionAnalyzer::~QFunctionAnalyzer() {
	delete stateCollection;
}

void QFunctionAnalyzer::setQFunction(AbstractQFunction *l_qFunction) {
	this->qFunction = l_qFunction;
}

void QFunctionAnalyzer::save1DValues(
    std::ostream& stream, ActionSet *actions, State *initState,
    int dim1, int part1
) {
	RealType width = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);
	fmt::fprintf(stream, "QFunction Analyzer 1D-Table for Dimension %d\n", dim1);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	fmt::fprintf(stream, "Actions: %lu\\n", actions->size());

	int index = 0;
	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		fmt::fprintf(stream, "Action %d\n", index++);

		for(int i = 0; i <= part1; i++) {
			RealType x = modelStateProperties->getMinValue(dim1)
			    + i * width / part1;
			modelState->setContinuousState(dim1, x);
			stateCollection->newModelState();
			fmt::fprintf(stream, "%f ", x);

			RealType value = qFunction->getValue(stateCollection, *it);
			fmt::fprintf(stream, "%f ", value);

			fmt::fprintf(stream, "\n");
		}
		fmt::fprintf(stream, "\n\n");
	}

}

void QFunctionAnalyzer::save1DValues(
    const std::string& filename, ActionSet *actions, State *initState,
    int dim1, int part1
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save1DValues(file, actions, initState, dim1, part1);
}

void QFunctionAnalyzer::save1DMaxAction(
    std::ostream& stream, ActionSet *actions, State *initState, int dim1,
    int part1, char *actionSymbols
) {
	RealType width = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);
	fmt::fprintf(stream, "QFunction Analyzer 1D-Table for Dimension %d\n", dim1);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	fmt::fprintf(stream, "Actions: %lu\\n", actions->size());

	int index = 0;

	ActionSet availableActions;

	for(int i = 0; i <= part1; i++) {
		RealType x = modelStateProperties->getMinValue(dim1) + i * width / part1;
		modelState->setContinuousState(dim1, x);
		stateCollection->newModelState();
		fmt::fprintf(stream, "%f ", x);

		availableActions.clear();
		actions->getAvailableActions(&availableActions, stateCollection);
		Action *action = qFunction->getMax(stateCollection, &availableActions);

		if(actionSymbols == nullptr) {
			fmt::fprintf(stream, "%d", actions->getIndex(action));
		} else {
			fmt::fprintf(stream, "%c", actionSymbols[actions->getIndex(action)]);
		}

		fmt::fprintf(stream, "\n");
	}
}

void QFunctionAnalyzer::save1DMaxAction(
    const std::string& filename, ActionSet *actions, State *initState,
    int dim1, int part1, char *actionSymbols
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save1DMaxAction(file, actions, initState, dim1, part1, actionSymbols);
}

void QFunctionAnalyzer::save1DMatlab(
	std::ostream& stream, ActionSet *actions, State *initState,
	int dim1, int part1
) {
	RealType width = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	for(int i = 0; i <= part1; i++) {
		RealType x = modelStateProperties->getMinValue(dim1) + i * width / part1;
		modelState->setContinuousState(dim1, x);
		stateCollection->newModelState();
		fmt::fprintf(stream, "%f ", x);
		ActionSet::iterator it = actions->begin();

		for(; it != actions->end(); it++) {
			RealType value = qFunction->getValue(stateCollection, *it);
			fmt::fprintf(stream, "%f ", value);
		}
		fmt::fprintf(stream, "\n");
	}
}

void QFunctionAnalyzer::save1DMatlab(
	const std::string& filename, ActionSet *actions, State *initState,
	int dim1, int part1
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save1DMatlab(file, actions, initState, dim1, part1);
}

void QFunctionAnalyzer::saveDiscrete1DValues(
	std::ostream& stream, ActionSet *actions, State *initState, int dim1) {
	int numDiscStates = modelStateProperties->getDiscreteStateSize(dim1);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	fmt::fprintf(stream, "QFunction Analyzer 1D-Table for Dimension %d\n", dim1);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	fmt::fprintf(stream, "Actions: %lu\\n", actions->size());

	int index = 0;
	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {

		fmt::fprintf(stream, "Action %d\n", index++);

		for(int i = 0; i < numDiscStates; i++) {
			modelState->setDiscreteState(dim1, i);
			stateCollection->newModelState();

			RealType value = qFunction->getValue(stateCollection, *it);
			fmt::fprintf(stream, "%d %f ", i, value);
		}
		fmt::fprintf(stream, "\n");
	}
}

void QFunctionAnalyzer::saveDiscrete1DValues(
	const std::string& filename, ActionSet *actions, State *initState, int dim1
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete1DValues(file, actions, initState, dim1);
}

void QFunctionAnalyzer::saveDiscrete1DMaxAction(
	std::ostream& stream, ActionSet *actions, State *initState, int dim1,
    char *actionSymbols) {
	int numDiscStates = modelStateProperties->getDiscreteStateSize(dim1);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	fmt::fprintf(stream, "QFunction Analyzer 1D-Table for Dimension %d\n", dim1);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	fmt::fprintf(stream, "Actions: %lu\\n", actions->size());

	int index = 0;
	ActionSet availableActions;

	for(int i = 0; i < numDiscStates; i++) {
		modelState->setDiscreteState(dim1, i);
		stateCollection->newModelState();

		availableActions.clear();
		actions->getAvailableActions(&availableActions, stateCollection);
		Action *action = qFunction->getMax(stateCollection, &availableActions);

		if(actionSymbols == nullptr) {
			fmt::fprintf(stream, "%d", actions->getIndex(action));
		} else {
			fmt::fprintf(stream, "%c", actionSymbols[actions->getIndex(action)]);
		}
	}
	fmt::fprintf(stream, "\n");
}

void QFunctionAnalyzer::saveDiscrete1DMaxAction(
	const std::string& filename, ActionSet *actions, State *initState,
	int dim1, char *actionSymbols
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete1DMaxAction(file, actions, initState, dim1, actionSymbols);
}

void QFunctionAnalyzer::saveDiscrete1DMatlab(
	std::ostream& stream, ActionSet *actions, State *initState, int dim1) {
	int numDiscStates = modelStateProperties->getDiscreteStateSize(dim1);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	int index = 0;
	for(int i = 0; i < numDiscStates; i++) {
		modelState->setDiscreteState(dim1, i);
		stateCollection->newModelState();

		fmt::fprintf(stream, "%d ", i);
		ActionSet::iterator it = actions->begin();

		for(; it != actions->end(); it++) {
			RealType value = qFunction->getValue(stateCollection, *it);
			fmt::fprintf(stream, "%f ", value);
		}
		fmt::fprintf(stream, "\n");
	}

}

void QFunctionAnalyzer::saveDiscrete1DMatlab(
	const std::string& filename, ActionSet *actions, State *initState, int dim1
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete1DMatlab(file, actions, initState, dim1);
}

void QFunctionAnalyzer::save2DValues(
	const std::string& filename, ActionSet *actions, State *initstate,
	int dim1, int part1, int dim2, int part2
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save2DValues(file, actions, initstate, dim1, part1, dim2, part2);
}

void QFunctionAnalyzer::save2DValues(
	std::ostream& stream, ActionSet *actions, State *initState, int dim1, int part1,
    int dim2, int part2) {
	RealType width1 = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	RealType width2 = modelStateProperties->getMaxValue(dim2)
	    - modelStateProperties->getMinValue(dim2);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	fmt::fprintf(stream, "QFunction Analyzer 2D-Table for Dimensions %d and %d\n",
	    dim1, dim2);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	fmt::fprintf(stream, "Actions: %lu\n", actions->size());

	ActionSet::iterator it = actions->begin();

	for(int index = 0; it != actions->end(); it++, index++) {
		fmt::fprintf(stream, "Action %d\n", index);
		for(int i = 0; i <= part1; i++) {
			RealType x = modelStateProperties->getMinValue(dim1)
			    + i * width1 / part1;
			modelState->setContinuousState(dim1, x);

			for(int j = 0; j <= part2; j++) {
				RealType y = modelStateProperties->getMinValue(dim2)
				    + j * width2 / part2;
				modelState->setContinuousState(dim2, y);
				stateCollection->newModelState();

				RealType value = qFunction->getValue(stateCollection, *it);
				fmt::fprintf(stream, "%f ", value);
			}

			fmt::fprintf(stream, "\n");
		}
	}
}

void QFunctionAnalyzer::save2DMaxAction(
	const std::string& filename, ActionSet *actions, State *initstate,
	int dim1, int part1, int dim2, int part2, char *actionSymbols
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save2DMaxAction(file, actions, initstate, dim1, part1, dim2, part2,
	    actionSymbols);
}

void QFunctionAnalyzer::save2DMaxAction(
	std::ostream& stream, ActionSet *actions, State *initState, int dim1,
	int part1, int dim2, int part2, char *actionSymbols
) {
	RealType width1 = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	RealType width2 = modelStateProperties->getMaxValue(dim2)
	    - modelStateProperties->getMinValue(dim2);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	fmt::fprintf(stream, "QFunction Analyzer 2D-Table for Dimensions %d and %d\n",
	    dim1, dim2);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	fmt::fprintf(stream, "Actions: %lu\\n", actions->size());

	ActionSet availableActions;

	for(int i = 0; i <= part1; i++) {
		RealType x = modelStateProperties->getMinValue(dim1) + i * width1 / part1;
		modelState->setContinuousState(dim1, x);

		for(int j = 0; j <= part2; j++) {
			RealType y = modelStateProperties->getMinValue(dim2)
			    + j * width2 / part2;
			modelState->setContinuousState(dim2, y);
			stateCollection->newModelState();

			availableActions.clear();
			actions->getAvailableActions(&availableActions, stateCollection);
			Action *action = qFunction->getMax(stateCollection,
			    &availableActions);

			if(actionSymbols == nullptr) {
				fmt::fprintf(stream, "%d", actions->getIndex(action));
			} else {
				fmt::fprintf(stream, "%c", actionSymbols[actions->getIndex(action)]);
			}
		}

		fmt::fprintf(stream, "\n");
	}
}

void QFunctionAnalyzer::save2DMatlab(
	const std::string& filename, ActionSet *actions, State *initstate,
	int dim1, int part1, int dim2, int part2
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	save2DMatlab(file, actions, initstate, dim1, part1, dim2, part2);
}

void QFunctionAnalyzer::save2DMatlab(
	std::ostream& stream, ActionSet *actions, State *initState, int dim1, int part1,
    int dim2, int part2) {
	RealType width1 = modelStateProperties->getMaxValue(dim1)
	    - modelStateProperties->getMinValue(dim1);
	RealType width2 = modelStateProperties->getMaxValue(dim2)
	    - modelStateProperties->getMinValue(dim2);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	for(int i = 0; i <= part1; i++) {
		RealType x = modelStateProperties->getMinValue(dim1) + i * width1 / part1;
		modelState->setContinuousState(dim1, x);

		for(int j = 0; j <= part2; j++) {
			RealType y = modelStateProperties->getMinValue(dim2)
			    + j * width2 / part2;
			modelState->setContinuousState(dim2, y);
			stateCollection->newModelState();
			fmt::fprintf(stream, "%f %f ", x, y);

			ActionSet::iterator it = actions->begin();

			for(; it != actions->end(); it++) {
				RealType value = qFunction->getValue(stateCollection, *it);
				fmt::fprintf(stream, "%f ", value);
			}

			fmt::fprintf(stream, "\n");
		}
	}
}

void QFunctionAnalyzer::saveDiscrete2DValues(
	const std::string& filename, ActionSet *actions, State *initstate,
	int row_dim, int col_dim
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete2DValues(file, actions, initstate, row_dim, col_dim);
}

void QFunctionAnalyzer::saveDiscrete2DValues(
	std::ostream& stream, ActionSet *actions, State *initState,
	int row_dim, int col_dim
) {

	int numDiscStates1 = modelStateProperties->getDiscreteStateSize(row_dim);
	int numDiscStates2 = modelStateProperties->getDiscreteStateSize(col_dim);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	fmt::fprintf(stream, "QFunction Analyzer 2D-Table for Dimensions [%d, %d]\n",
	    row_dim, col_dim);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");

	fmt::fprintf(stream, "Actions: %lu\n\n\n", actions->size());

	ActionSet::iterator it = actions->begin();

	int index = 0;

	for(; it != actions->end(); it++) {
		fmt::fprintf(stream, "Action %d\n", index++);

		for(int i = 0; i < numDiscStates1; i++) {

			modelState->setDiscreteState(row_dim, i);

			for(int j = 0; j < numDiscStates2; j++) {

				modelState->setDiscreteState(col_dim, j);

				stateCollection->newModelState();

				RealType value = qFunction->getValue(stateCollection, *it);
				fmt::fprintf(stream, "%f ", value);

			}
			fmt::fprintf(stream, "\n");

		}
		fmt::fprintf(stream, "\n\n");
	}
}

void QFunctionAnalyzer::saveDiscrete2DMaxAction(
	const std::string& filename, ActionSet *actions, State *initstate,
	int row_dim, int col_dim, char *actionSymbols
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete2DMaxAction(file, actions, initstate, row_dim, col_dim,
	    actionSymbols);
}

void QFunctionAnalyzer::saveDiscrete2DMaxAction(
	std::ostream& stream, ActionSet *actions, State *initState, int row_dim,
    int col_dim, char *actionSymbols
) {
	int numDiscStates1 = modelStateProperties->getDiscreteStateSize(row_dim);
	int numDiscStates2 = modelStateProperties->getDiscreteStateSize(col_dim);

	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	fmt::fprintf(stream, "QFunction Analyzer 2D-Table for Dimensions [%d, %d]\n",
	    row_dim, col_dim);
	fmt::fprintf(stream, "InitialState: ");
	initState->save(stream);
	fmt::fprintf(stream, "\n");
	fmt::fprintf(stream, "Actions: %lu\n\n\n", actions->size());

	ActionSet availableActions;
	int index = 0;

	fmt::fprintf(stream, "Action %d\n", index++);

	for(int i = 0; i < numDiscStates1; i++) {

		modelState->setDiscreteState(row_dim, i);

		for(int j = 0; j < numDiscStates2; j++) {

			modelState->setDiscreteState(col_dim, j);

			stateCollection->newModelState();

			availableActions.clear();
			actions->getAvailableActions(&availableActions, stateCollection);
			Action *action = qFunction->getMax(stateCollection,
			    &availableActions);

			if(actionSymbols == nullptr) {
				fmt::fprintf(stream, "%d", actions->getIndex(action));
			} else {
				fmt::fprintf(stream, "%c", actionSymbols[actions->getIndex(action)]);
			}

		}

		fmt::fprintf(stream, "\n");

	}
}

void QFunctionAnalyzer::saveDiscrete2DMatlab(
	const std::string& filename, ActionSet *actions, State *initstate,
	int dim1, int dim2
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveDiscrete2DMatlab(file, actions, initstate, dim1, dim2);
}

void QFunctionAnalyzer::saveDiscrete2DMatlab(
	std::ostream& stream, ActionSet *actions, State *initState, int dim1, int dim2) {
	State *modelState = stateCollection->getState(modelStateProperties);

	modelState->setState(initState);

	for(int i = 0; i < modelStateProperties->getDiscreteStateSize(dim1); i++) {

		modelState->setDiscreteState(dim1, i);

		for(int j = 0; j < modelStateProperties->getDiscreteStateSize(dim2);
		    j++) {
			modelState->setDiscreteState(dim2, j);
			stateCollection->newModelState();
			fmt::fprintf(stream, "%d %d ", i, j);

			ActionSet::iterator it = actions->begin();

			for(; it != actions->end(); it++) {
				RealType value = qFunction->getValue(stateCollection, *it);
				fmt::fprintf(stream, "%f ", value);
			}

			fmt::fprintf(stream, "\n");
		}
	}
}

void QFunctionAnalyzer::saveStateValues(
	std::ostream& stream, ActionSet *actions, StateList *states
) {
	State *modelState = stateCollection->getState(modelStateProperties);

	fmt::fprintf(stream, "QFunction Analyzer State Value-Table\n");

	for(unsigned int i = 0; i < states->getNumStates(); i++) {
		states->getState(i, modelState);
		stateCollection->newModelState();

		fmt::fprintf(stream, "(");
		modelState->save(stream);
		fmt::fprintf(stream, ": ");

		ActionSet::iterator it = actions->begin();

		for(; it != actions->end(); it++) {
			RealType value = qFunction->getValue(stateCollection, *it);
			fmt::fprintf(stream, "%f ", value);
		}
		fmt::fprintf(stream, ")\n");
	}
}

void QFunctionAnalyzer::saveStateValues(
	const std::string& filename, ActionSet *actions, StateList *states
) {
	std::ofstream file(filename);

	if(!file.good()) {
		std::cerr << "QFunctionAnalyzer: File not found " << filename << "!!\n" << std::endl;
		return;
	}

	saveStateValues(file, actions, states);
}

CFunctionComparator::CFunctionComparator(
    StateProperties *modelState,
    const std::set<StateModifierList*>& modifierLists
) {
	this->modelStateProperties = modelState;
	stateCollection = new StateCollectionImpl(modelStateProperties, modifierLists);
}

CFunctionComparator::~CFunctionComparator() {
	delete stateCollection;
}

RealType CFunctionComparator::getDifference(
    StateCollection *state, int errorFunction) {
	RealType value1 = getValue(1, state);
	RealType value2 = getValue(2, state);

	RealType error = 0;

	switch(errorFunction) {
	case AT_MSE: {
		error = pow(value1 - value2, 2);
		break;
	}
	case AT_MAE: {
		error = fabs((RealType) (value1 - value2));
	}
	}
	return error;
}

void CFunctionComparator::getRandomState(State *state) {
	auto rand = make_rand();

	for(unsigned int i = 0; i < state->getNumContinuousStates(); i++) {
		RealType width = state->getStateProperties()->getMaxValue(i)
		    - state->getStateProperties()->getMinValue(i);
		RealType randVal = ((RealType) rand() / (RealType) RAND_MAX) * width
		    + state->getStateProperties()->getMaxValue(i);

		state->setContinuousState(i, randVal);
	}
}

RealType CFunctionComparator::compareFunctionsRandom(
    int nSamples, int errorFunction) {
	State *modelState = stateCollection->getState(modelStateProperties);
	RealType error = 0.0;
	for(int i = 0; i < nSamples; i++) {
		getRandomState(modelState);
		stateCollection->newModelState();

		switch(errorFunction) {
		case AT_MSE:
		case AT_MAE: {
			RealType value = getDifference(stateCollection, errorFunction);
			error += value;
			break;
		}
		case AT_MAXERROR: {
			RealType value = getDifference(stateCollection, AT_MAE);
			if(value > error) {
				error = value;
			}
			break;
		}

		}
	}
	if(errorFunction == AT_MSE || errorFunction == AT_MAE) {
		error /= nSamples;
	}
	return error;
}

RealType CFunctionComparator::compareFunctionsStates(
    StateList *states, int errorFunction) {
	State *modelState = stateCollection->getState(modelStateProperties);
	RealType error = 0.0;
	for(unsigned int i = 0; i < states->getNumStates(); i++) {
		states->getState(i, modelState);
		stateCollection->newModelState();
		switch(errorFunction) {
		case AT_MSE:
		case AT_MAE: {
			RealType value = getDifference(stateCollection, errorFunction);
			error += value;
			break;
		}
		case AT_MAXERROR: {
			RealType value = getDifference(stateCollection, AT_MAE);
			if(value > error) {
				error = value;
			}
			break;
		}

		}
	}
	if(errorFunction == AT_MSE || errorFunction == AT_MAE) {
		error /= states->getNumStates();
	}
	return error;
}

RealType CFunctionComparator::getReward(
    StateCollection *oldState, Action *, StateCollection *) {
	return getDifference(oldState, AT_MAE);
}

RealType VFunctionComparator::getValue(int numFunc, StateCollection *state) {
	if(numFunc == 1) {
		return vFunction1->getValue(state);
	} else {
		return vFunction2->getValue(state);
	}
}

VFunctionComparator::VFunctionComparator(
    StateProperties *modelState, const std::set<StateModifierList*>& modifierLists,
    AbstractVFunction *vFunction1, AbstractVFunction *vFunction2) :
	CFunctionComparator(modelState, modifierLists)
{
	this->vFunction1 = vFunction1;
	this->vFunction2 = vFunction2;
}

RealType QFunctionComparator::getValue(int numFunc, StateCollection *state) {
	if(numFunc == 1) {
		return qFunction1->getValue(state, action);
	} else {
		return qFunction2->getValue(state, action);
	}
}

QFunctionComparator::QFunctionComparator(
    StateProperties *modelState, const std::set<StateModifierList*>& modifierLists,
    AbstractQFunction *qFunction1, AbstractQFunction *qFunction2,
    Action *action) :
	CFunctionComparator(modelState, modifierLists) {
	this->qFunction1 = qFunction1;
	this->qFunction2 = qFunction2;

	this->action = action;
}

ControllerAnalyzer::ControllerAnalyzer(
    StateList *states, AgentController *controller, ActionSet *actions) :
	ActionObject(actions) {
	this->states = states;
	this->controller = controller;
}

ControllerAnalyzer::~ControllerAnalyzer() {
}

StateList *ControllerAnalyzer::getStateList() {
	return this->states;
}

void ControllerAnalyzer::setStateList(StateList *states) {
	this->states = states;
}

AgentController *ControllerAnalyzer::getController() {
	return this->controller;
}

void ControllerAnalyzer::setController(AgentController *controller) {
	this->controller = controller;
}

void ControllerAnalyzer::saveActions(
    std::ostream& stream, const std::set<StateModifierList*>& modifierLists
) {
	fmt::fprintf(stream, "State - selected Action Table\n");
	State *modelState = new State(states->getStateProperties());
	Action *action;
	unsigned int i = 1;
	StateCollectionImpl *stateCollection = new StateCollectionImpl(
	    states->getStateProperties(), modifierLists);

	for(unsigned int stateNum = 0; stateNum < states->getNumStates();
	    stateNum++) {
		states->getState(stateNum, modelState);
		stateCollection->setState(modelState);
		stateCollection->calculateModifiedStates();

		action = controller->getNextAction(stateCollection);
		assert(action != nullptr);
		fmt::fprintf(stream, "State# %d: Action %d\n", i++,
		    actions->getIndex(action));
	}
	delete modelState;
	delete stateCollection;
}

FittedQIterationAnalyzer::FittedQIterationAnalyzer(
    QFunction *qFunction, AgentController *estimationPolicy,
    EpisodeHistory *episodeHistory, RewardHistory *rewardLogger,
    SupervisedQFunctionLearner *learner, StateProperties *residualProperties,
    PolicySameStateEvaluator *l_evaluator) :
	    FittedQIteration(qFunction, estimationPolicy, episodeHistory,
	        rewardLogger, learner, residualProperties),
	    TestSuiteEvaluatorLogger("Analyzer") {
	evaluator = l_evaluator;
	analyzerFile = nullptr;

	kNN = 5;

	buffState2 = new State(residualProperties);

	addParameter("UseRealQValues", 0.0);

	useQValues = false;

	lastQValue = 0;
	lastEstimatedQValue = 0;
}

FittedQIterationAnalyzer::~FittedQIterationAnalyzer() {
	delete buffState2;
}

void FittedQIterationAnalyzer::addResidualInput(
    Step *step, Action *nextAction, RealType V, RealType newV,
    RealType nearestNeighborDistance, Action *, RealType) {
	if(!useQValues) {
		FittedQIteration::addResidualInput(step, nextAction, V, newV,
		    nearestNeighborDistance);

	}

	/*if (nextAction == nullptr)
	 {
	 printf("%d reward: %f\n", episodeHistory->getActions()->getIndex(step->action), nextReward);

	 printf("Real States:\n");
	 step->oldState->getState()->saveASCII( stdout);
	 printf("\n");

	 step->newState->getState()->saveASCII( stdout);
	 printf("\n");

	 RealType estR = evaluator->getActionValueForState(step->oldState->getState(), step->action, numEvaluations);

	 printf("Estimated Reward: %f\n", estR);
	 }*/

	if(nextAction != nullptr && analyzerFile
	    && (*nearestNeighbors)[nextAction] != nullptr) {
		StateProperties *l_prop = residualProperties;

		if(l_prop == nullptr) {
			BatchQDataGenerator *qGenerator =
			    dynamic_cast<BatchQDataGenerator *>(dataGenerator);

			l_prop = qGenerator->getStateProperties(nextAction);
		}

		/*State *state1 = step->oldState->getState(l_prop);

		 State newState(state1);

		 (*dataPreProc)[step->action]->preprocessInput(state1, &newState);

		 neighborsList->clear();
		 (*nearestNeighbors)[step->action]->getNearestNeighbors(state1, neighborsList);

		 std::list<int>::iterator it1 = neighborsList->begin();
		 for (; it1 != neighborsList->end(); it1 ++)
		 {
		 ColumnVector *nearestNeighbor = (*(*inputDatas)[step->action])[*(it1)];

		 RealType distance = newState.getDistance(nearestNeighbor);

		 printf("%f ", distance);
		 }
		 printf("\n");*/

		/*RealType realV = 

		 if (useQValues)
		 {
		 dataGenerator->addInput(step->newState, nextAction, realV);
		 }*/

		int actionIndex1 = episodeHistory->getActions()->getIndex(step->action);
		int actionIndex2 = episodeHistory->getActions()->getIndex(nextAction);
		fmt::fprintf(*analyzerFile, "%d %d %f %f ", actionIndex1, actionIndex2,
		    lastEstimatedQValue, lastQValue);

		/*
		 if (nextHistoryAction != nullptr)
		 {
		 fprintf(analyzerFile, "%d %f ", episodeHistory->getActions()->getIndex(nextHistoryAction), nextReward);
		 }
		 else
		 {
		 fprintf(analyzerFile, "-1 0.0 ");
		 }*/

		State *state = step->newState->getState(l_prop);
		(*dataPreProc)[nextAction]->preprocessInput(state, buffState);

		neighborsList->clear();
		(*nearestNeighbors)[nextAction]->getNearestNeighbors(state,
		    neighborsList);

		std::list<int>::iterator it = neighborsList->begin();
		for(; it != neighborsList->end(); it++) {
			ColumnVector *nearestNeighbor = (*(*inputDatas)[nextAction])[*(it)];

			RealType distance = buffState->getDistance(nearestNeighbor);

			fmt::fprintf(*analyzerFile, "%f %f ", distance,
			    (*(*outputDatas)[nextAction])[*it]);
		}

		State *oldState = step->oldState->getState(l_prop);
		(*dataPreProc)[nextAction]->preprocessInput(oldState, buffState2);

		it = neighborsList->begin();
		for(; it != neighborsList->end(); it++) {
			ColumnVector *nearestNeighbor = (*(*inputDatas)[nextAction])[*(it)];

			RealType distance = buffState2->getDistance(nearestNeighbor);

			fmt::fprintf(*analyzerFile, "%f %f ", distance,
			    (*(*outputDatas)[nextAction])[*it]);
		}

		RealType distance = buffState2->getDistance(buffState);
		fmt::fprintf(*analyzerFile, "%f %f", distance, V);

		fmt::fprintf(*analyzerFile, "\n");
	}
}

void FittedQIterationAnalyzer::evaluate(
    std::string evaluationDirectory, int trial, int numEpisodes) {
	std::stringstream filename;

	filename << evaluationDirectory << outputDirectory << "/" << "Trial"
	    << trial << "/FittedQAnalyzer_Eval" << numEpisodes << ".csv";

	analyzerFile = make_shared<std::ofstream>(filename.str());
	numEvaluations = numEpisodes;

	int useRealQValues = (int) getParameter("UseRealQValues");

	useQValues = (useRealQValues > 0 && (numEvaluations % useRealQValues == 0));
}

void FittedQIterationAnalyzer::startNewEvaluation(
    std::string evaluationDirectory, Parameters *, int trial) {
	std::stringstream sstream;

	sstream << "mkdir " << evaluationDirectory << outputDirectory << "/"
	    << "Trial" << trial;
	std::string scall = sstream.str();
	system(scall.c_str());

	analyzerFile.reset();
	numEvaluations = 0;
}

RealType FittedQIterationAnalyzer::getValue(
    StateCollection *stateCollection, Action *action) {
	lastEstimatedQValue = FittedQIteration::getValue(stateCollection, action);

	lastQValue = evaluator->getActionValueForState(stateCollection->getState(),
	    action, numEvaluations);

	if(useQValues) {
		return lastQValue;
	} else {
		return lastEstimatedQValue;
	}
}

} //namespace rlearner
