// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_chistory_H_
#define Rlearner_chistory_H_

#include "baseobjects.h"
#include "learndataobject.h"
#include "agentlistener.h"

namespace rlearner {

class StateCollectionImpl;
class StateModifier;
class StateProperties;
class Action;
class ActionDataSet;

/**
 * @class Step
 * Class storing a single step (State-Action-State Tuple).
 */
class Step: public StateObject, public ActionObject {
public:
	StateCollectionImpl *oldState;
	StateCollectionImpl *newState;

	Action *action;
	ActionDataSet *actionData;

public:
	Step& operator=(const Step&) = delete;
	Step(const Step&) = delete;

	Step(
		StateProperties *properties,
		const std::set<StateModifierList*>& modifierLists,
		ActionSet *actions
	);

	virtual ~Step();
};

/**
 * @class StepHistory
 * Class maintaining an unordered set of steps, which can be used for Learning.
 *
 * This class provides an interface from which you can retrieve any step from
 * the step history. This is done by getStep(int index, Step *step). This
 * function must be implemented by the subclasses.
 *
 * Further the class has 2 functions for showing a semiMDP listener the single,
 * not related steps. simulateSteps(..) simulates  "num" random steps to the
 * given listener, simulateAllSteps simulates the number of steps in
 * the history to the current listener. This method can only be used for
 * learning algorithms without etraces, since the steps shown to the listener
 * are not related, so you have to set the Parameter "Lambda" to zero if you
 * want to use the single steps updates for normal learners.
 */
class StepHistory: virtual public StateModifiersObject,
        virtual public LearnDataObject,
        public ActionObject {
public:
	virtual int getNumSteps() = 0;

	//! Virtual function; should retrieve the index-th step and write it into
	//! the listener.
	virtual void getStep(int index, Step *step) = 0;

public:
	StepHistory(StateProperties *properties, ActionSet *actions);
	virtual ~StepHistory() {}
};

/**
 * @class BatchStepUpdate
 * Class for doing batch updates.
 *
 * Another possibility to improve performance is the batch update. In
 * BatchStepUpdate after each episode the steps from a logger can be showed to
 * the assigned listener again. This can improve learning specially for
 * TD-Learning algorithms, but be careful, it can also falsify the state
 * transition distributions, especially if the problem doesn't have exactly
 * the Markov property. The class needs obviously a listener and an assigned
 * step history. Additionally you have to determine the number of steps which
 * are shown to the listener.
 */
class BatchStepUpdate: public SemiMDPListener {
protected:
	int numUpdates;
	SemiMDPListener *listener;
	StepHistory *steps;

	Step *step;
	ActionDataSet *dataSet;

public:
	virtual void newEpisode();
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *nextState);

	/**
	 * Simulates num random steps to the listener.
	 *
	 * Since the sequenced states shown to the listener are not related,
	 * a newEpisode event is send to the listener after each step.
	 */
	virtual void simulateSteps(SemiMDPListener *listener, int num);

	/**
	 * Simulates "getNumSteps" random steps to the listener.
	 * Calls simulateSteps(listener, getNumSteps())
	 */
	virtual void simulateAllSteps(SemiMDPListener *listener);

public:
	BatchStepUpdate& operator=(const BatchStepUpdate&) = delete;
	BatchStepUpdate(const BatchStepUpdate&) = delete;

	BatchStepUpdate(SemiMDPListener* listener, StepHistory* logger,
		int numUpdatesPerStep, int numUpdatesPerEpisode,
		const std::set<StateModifierList*>& modifierLists
	);

	virtual ~BatchStepUpdate();
};

} //namespace rlearner

#endif //Rlearner_chistory_H_
