// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>

#include "agentcontroller.h"
#include "ril_debug.h"
#include "actionstatistics.h"
#include "action.h"

namespace rlearner {

AgentController::AgentController(ActionSet *actions) :
	ActionObject(actions) {
}

AgentController::~AgentController() {
}

AgentStatisticController::AgentStatisticController(ActionSet *actions) :
	AgentController(actions) {
}

Action* AgentStatisticController::getNextAction(
    StateCollection *state, ActionDataSet *data) {
	return getNextAction(state, data, nullptr);
}

DeterministicController::DeterministicController(ActionSet *actions) :
	AgentController(actions) {
	this->controller = nullptr;
	this->statisticController = nullptr;
	useStatisticController = false;
	statistics = new ActionStatistics();
	initStatistics();
	nextAction = nullptr;

	actionDataSet = new ActionDataSet(getActions());
}

DeterministicController::DeterministicController(AgentController *controller) :
	AgentController(controller->getActions()) {
	this->controller = controller;
	this->statisticController = nullptr;
	useStatisticController = false;
	statistics = new ActionStatistics();
	initStatistics();
	nextAction = nullptr;

	actionDataSet = new ActionDataSet(getActions());
}

DeterministicController::DeterministicController(
    AgentStatisticController *controller) :
	AgentController(controller->getActions()) {
	this->statisticController = controller;
	this->controller = controller;
	useStatisticController = false;
	statistics = new ActionStatistics();
	initStatistics();
	nextAction = nullptr;

	actionDataSet = new ActionDataSet(getActions());
}

DeterministicController::~DeterministicController() {
	delete statistics;
	delete actionDataSet;
}

Action* DeterministicController::getNextAction(
    StateCollection *state, ActionDataSet *data) {
	if(nextAction == nullptr) {
		if(useStatisticController) {
			nextAction = statisticController->getNextAction(state,
			    actionDataSet, statistics);
		} else {
			if(controller != nullptr) {
				nextAction = controller->getNextAction(state, actionDataSet);
			} else {
				nextAction = nullptr;
			}
		}
	}
	if(data && nextAction) {
		data->setActionData(nextAction,
		    actionDataSet->getActionData(nextAction));
	}
	return nextAction;
}

void DeterministicController::nextStep(
    StateCollection *, Action *, StateCollection *) {
	nextAction = nullptr;
}

void DeterministicController::newEpisode() {
	nextAction = nullptr;
}

void DeterministicController::initStatistics() {
	assert(statistics != nullptr);
	statistics->owner = this;
	statistics->action = nullptr;
	statistics->equal = 0;
	statistics->superior = 0;
	statistics->probability = 0.0;
}

ActionStatistics *DeterministicController::getLastActionStatistics() {
	return statistics;
}

bool DeterministicController::isUsingStatisticController() {
	return useStatisticController;
}

void DeterministicController::setController(AgentController *controller) {
	this->controller = controller;
	this->statisticController = nullptr;
	useStatisticController = false;
	initStatistics();
}

void DeterministicController::setController(
    AgentStatisticController *controller) {
	this->statisticController = controller;
	this->controller = controller;
	useStatisticController = false;
}

void DeterministicController::setNextAction(Action *action, ActionData *data) {
	nextAction = action;

	if(data) {
		actionDataSet->setActionData(nextAction, data);
	}
}

} //namespace rlearner
