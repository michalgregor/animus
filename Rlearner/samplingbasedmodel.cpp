// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "samplingbasedmodel.h"
#include "action.h"
#include "episodehistory.h"
#include "transitionfunction.h"
#include "rewardmodel.h"
#include "statemodifier.h"
#include "state.h"
#include "statecollection.h"
#include "episode.h"
#include "nearestneighbor.h"
#include "kdtrees.h"
#include "batchlearning.h"
#include "evaluator.h"
#include "supervisedlearner.h"
#include "continuousactions.h"
#include "inputdata.h"

namespace rlearner {

CSampleTransition::CSampleTransition(
    State *l_state, ActionSet *l_availableActions, RealType l_reward,
    ActionData *l_actionData) {
	state = new State(l_state);
	reward = l_reward;

	availableActions = new ActionSet();
	availableActions->add(l_availableActions);
	actionData = l_actionData;
}

CSampleTransition::~CSampleTransition() {
	delete state;
	delete availableActions;

	if(actionData) {
		delete actionData;
	}
}

ContinuousStateList::ContinuousStateList(StateProperties *properties) :
	DataSet(properties->getNumContinuousStates()), StateObject(properties) {
	initLogger = nullptr;

	kdTree = nullptr;
	nearestNeighbor = nullptr;
	rangeSearch = nullptr;

	initNearestNeighborSearch();
	addParameter("StateListMinimumDistance", 0.0);
}

ContinuousStateList::~ContinuousStateList() {
	if(kdTree) {
		delete kdTree;
		delete nearestNeighbor;
		delete rangeSearch;
	}
}

void ContinuousStateList::disableNearestNeighborSearch() {
	if(kdTree) {
		delete kdTree;
		delete nearestNeighbor;
		delete rangeSearch;
	}

	kdTree = nullptr;
	nearestNeighbor = nullptr;
	rangeSearch = nullptr;
}

void ContinuousStateList::initNearestNeighborSearch() {
	if(kdTree) {
		delete kdTree;
		delete nearestNeighbor;
		delete rangeSearch;
	}

	kdTree = new KDTree(this, 1);
	nearestNeighbor = new KNearestNeighbors(kdTree, this, 1);
	rangeSearch = new RangeSearch(kdTree, this);
}

bool ContinuousStateList::isMember(ColumnVector *point) {
	int nN = 0;
	RealType distance = 2;
	nearestNeighbor->getNearestNeighborDistance(point, nN, distance);

	return distance == 0;
}

void ContinuousStateList::getNearestNeighbor(
    ColumnVector *point, int &index, RealType &distance) {
	nearestNeighbor->getNearestNeighborDistance(point, index, distance);
}

void ContinuousStateList::getSamplesInRange(
    KDRectangle *rectangle, DataSubset *subset) {
	rangeSearch->getSamplesInRange(rectangle, subset);
}

void ContinuousStateList::nextStep(
    StateCollection *oldState, Action *, StateCollection *) {
	State *state = oldState->getState(properties);

	addState(state, getParameter("StateListMinimumDistance"));
}

void ContinuousStateList::loadData(std::istream& stream) {
	int states = 0;
	int dimensions = 0;
	xscanf(stream, "ContinuousStateList: %d States, %d Dimensions\n",
		states, dimensions);

	for(int i = 0; i < states; i++) {
		ColumnVector state(dimensions);

		for(int j = 0; j < state.rows(); j++) {
			RealType buf = 0.0;
			xscanf(stream, "%lf ", buf);

			state.operator[](j) = buf;
		}
		xscanf(stream, "\n");
		addInput(&state);
	}

	printf("Continuous State List: Loaded %d States\n", size());
}

void ContinuousStateList::saveData(std::ostream& stream) {
	fmt::fprintf(stream, "ContinuousStateList: %d States, %d Dimensions\n", size(),
	    getNumDimensions());

	for(int i = 0; i < size(); i++) {
		ColumnVector *state = (*this)[i];

		for(int j = 0; j < state->rows(); j++) {
			fmt::fprintf(stream, "%f ", state->operator[](j));
		}
		fmt::fprintf(stream, "\n");
	}
}

int ContinuousStateList::addState(State *state, RealType minDist) {
	int nN = 0;
	RealType distance = 2;

	if(minDist < 0) {
		minDist = getParameter("StateListMinimumDistance");
	}

	if(size() > 0 && kdTree) {
		nearestNeighbor->getNearestNeighborDistance(state, nN, distance);
	}

	if(distance <= minDist) {
		return nN;
	} else {
		addInput(state);
		if(kdTree) {
			kdTree->addNewInput(size() - 1);
			//printf("Growing KD Tree: %d Examples %d Depth %d Leaves %d Samples\n", size(), kdTree->getDepth(), kdTree->getNumLeaves());

		}
		return size() - 1;
	}
}

void ContinuousStateList::createStateList(
    EpisodeHistory *history, bool useInitLogger)
{
	if(useInitLogger) {
		initLogger = history;
	} else {
		Step step(history->getStateProperties(), history->getModifierLists(),
		    history->getActions());

		printf("Creating Sample Transition Model ...\n");

		for(int i = 0; i < history->getNumEpisodes(); i++) {
			Episode *episode = history->getEpisode(i);

			for(int j = 0; j < episode->getNumSteps(); j++) {
				episode->getStep(j, &step);

				nextStep(step.oldState, step.action, step.newState);
			}

		}

		printf("Finished : %d States\n", size());
	}
}

void ContinuousStateList::resetData() {
	DataSet::clear();

	if(kdTree) {
		initNearestNeighborSearch();
	}

	if(initLogger) {
		createStateList(initLogger);
	}
}

void SamplingBasedTransitionModel::clearTransitions() {
	std::map<int, Transitions *>::iterator it = transitions->begin();

	for(; it != transitions->end(); it++) {
		Transitions::iterator it2 = (*it).second->begin();

		for(; it2 != (*it).second->end(); it2++) {
			delete (*it2).second;
		}
		delete (*it).second;
	}

	transitions->clear();
}

SamplingBasedTransitionModel::SamplingBasedTransitionModel(
    StateProperties *properties, StateProperties *l_targetProperties,
    ActionSet *action, RewardFunction *reward
):
	ActionObject(action),
	SemiMDPRewardListener(reward),
	StateObject(properties),
	initLogger(nullptr),
	initRewardLogger(nullptr),
	rewardFunction(nullptr),
	targetProperties(l_targetProperties),
	transitions(new std::map<int, Transitions *>),
	stateList(new ContinuousStateList(properties))
{}

SamplingBasedTransitionModel::~SamplingBasedTransitionModel() {
	clearTransitions();
	delete transitions;
	delete stateList;
}

int SamplingBasedTransitionModel::getNumStates() {
	return stateList->size();
}

void SamplingBasedTransitionModel::createStateList(
    EpisodeHistory *history, RewardLogger *rewardLogger, bool useInitLogger) {
	if(useInitLogger) {
		initLogger = history;
		initRewardLogger = rewardLogger;
	} else {
		Step step(history->getStateProperties(), history->getModifierLists(),
		    history->getActions());

		printf("Creating Sample Transition Model ...\n");

		for(int i = 0; i < history->getNumEpisodes(); i++) {
			Episode *episode = history->getEpisode(i);

			RewardEpisode *rewardEpisode = nullptr;

			if(rewardLogger) {
				rewardEpisode = rewardLogger->getEpisode(i);
			}

			for(int j = 0; j < episode->getNumSteps(); j++) {
				episode->getStep(j, &step);

				if(rewardEpisode) {
					nextStep(step.oldState, step.action,
					    rewardEpisode->getReward(j), step.newState);

				} else {
					nextStep(step.oldState, step.action, step.newState);
				}
			}

		}
		printf("Finished : %d States\n", getNumStates());
	}
}

void SamplingBasedTransitionModel::addTransition(
    int index, Action *action, StateCollection *state, RealType reward) {
	assert((unsigned int ) index <= transitions->size());

	ActionSet availableActions;
	actions->getAvailableActions(&availableActions, state);

	Transitions *transSamples = nullptr;
	if((unsigned int) index < transitions->size()) {
		transSamples = (*transitions)[index];
	} else {
		transSamples = new Transitions();
		(*transitions)[index] = transSamples;
	}
	CSampleTransition *trans = new CSampleTransition(
	    state->getState(targetProperties), &availableActions, reward);

	if((*transSamples)[action] != nullptr) {
		delete (*transSamples)[action];
	}
	(*transSamples)[action] = trans;

}

void SamplingBasedTransitionModel::nextStep(
    StateCollection *oldState, Action *action, StateCollection *newState) {
	RealType reward = rewardFunction->getReward(oldState, action, newState);
	nextStep(oldState, action, reward, newState);
}

void SamplingBasedTransitionModel::nextStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *newState) {
	int index = stateList->addState(oldState->getState(properties));

	addTransition(index, action, newState, reward);
}

void SamplingBasedTransitionModel::loadData(std::istream&) {}

void SamplingBasedTransitionModel::saveData(std::ostream&) {}

void SamplingBasedTransitionModel::resetData() {
	clearTransitions();
	stateList->resetData();
}

std::map<Action *, CSampleTransition *> *SamplingBasedTransitionModel::getTransitions(
    int index) {
	return (*transitions)[index];
}

ContinuousStateList *SamplingBasedTransitionModel::getStateList() {
	return stateList;
}

SamplingBasedTransitionModelFromTransitionFunction::SamplingBasedTransitionModelFromTransitionFunction(
    StateProperties *properties, StateProperties *targetProperties,
    ActionSet *l_allActions, TransitionFunction *l_transitionFunction,
    RewardFunction *rewardFunction,
    const std::set<StateModifierList*>& modifierLists,
    RewardFunction *l_rewardPrediction) :
	    SamplingBasedTransitionModel(properties, targetProperties, l_allActions,
	        rewardFunction) {
	transitionFunction = l_transitionFunction;
	predictState = new StateCollectionImpl(
	    transitionFunction->getStateProperties(), modifierLists);

	rewardPrediction = l_rewardPrediction;
	availableActions = new ActionSet();

	allActions = l_allActions;
}

SamplingBasedTransitionModelFromTransitionFunction::~SamplingBasedTransitionModelFromTransitionFunction() {
	delete predictState;
	delete availableActions;
}

void SamplingBasedTransitionModelFromTransitionFunction::nextStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *newState) {
	int index = stateList->addState(oldState->getState(properties));

	addTransition(index, action, newState, reward);

	Transitions *sampleTransitions = (*transitions)[index];

	availableActions->clear();
	allActions->getAvailableActions(availableActions, oldState);

	ActionSet::iterator it = availableActions->begin();

	for(; it != availableActions->end(); it++) {
		if((*sampleTransitions)[*it] == nullptr) {
			transitionFunction->transitionFunction(
			    oldState->getState(transitionFunction->getStateProperties()),
			    *it, predictState->getState());

			predictState->newModelState();
			predictState->setResetState(
			    transitionFunction->isResetState(predictState->getState()));

			RealType reward = 0;
			if(rewardPrediction) {
				reward = rewardPrediction->getReward(oldState, *it,
				    predictState);
			} else {
				reward = rewardFunction->getReward(oldState, *it, predictState);
			}

			addTransition(index, *it, predictState, reward);
		}
	}
}

GraphTransition::GraphTransition(
    int l_newStateIndex, RealType l_reward, RealType l_discountFactor,
    Action *l_action, ActionData *l_actionData) {
	newStateIndex = l_newStateIndex;
	reward = l_reward;

	action = l_action;
	actionData = l_actionData;

	discountFactor = l_discountFactor;
}

GraphTransition::~GraphTransition() {
	if(actionData) {
		delete actionData;
	}
}

RealType GraphTransition::getReward() {
	return reward;
}

void SamplingBasedGraph::clearTransitions() {
	std::map<int, Transitions *>::iterator it = transitions->begin();

	for(; it != transitions->end(); it++) {
		Transitions::iterator it2 = (*it).second->begin();

		for(; it2 != (*it).second->end(); it2++) {
			delete (*it2);
		}
		delete (*it).second;
	}
	transitions->clear();

	for(unsigned int i = 0; i < stateList->size(); i++) {
		(*transitions)[i] = new Transitions();
	}

	numTransitions = 0;
}

void SamplingBasedGraph::addTransition(
    int index, int newIndex, Action *action, ActionData *actionData,
    RealType reward, RealType discountFactor) {
	Transitions *transSamples = nullptr;
	transSamples = (*transitions)[index];

	GraphTransition *trans = new GraphTransition(newIndex, reward,
	    discountFactor, action, actionData);

	transSamples->push_back(trans);

	numTransitions++;
}

SamplingBasedGraph::SamplingBasedGraph(
    ContinuousStateList *l_stateList, ActionSet *actions) :
	ActionObject(actions), StateObject(l_stateList->getStateProperties()) {
	transitions = new std::map<int, Transitions *>;
	stateList = l_stateList;

	clearTransitions();
	addParameters(stateList);

	numTransitions = 0;
}

SamplingBasedGraph::~SamplingBasedGraph() {
	std::map<int, Transitions *>::iterator it = transitions->begin();

	for(; it != transitions->end(); it++) {
		Transitions::iterator it2 = (*it).second->begin();

		for(; it2 != (*it).second->end(); it2++) {
			delete (*it2);
		}

		delete (*it).second;
	}

	delete transitions;
}

void SamplingBasedGraph::loadData(std::istream& stream) {
	stateList->loadData(stream);
	clearTransitions();

	xscanf(stream, "Continuous Graph\n");
	int gesTransitions = 0;

	for(unsigned int i = 0; i < stateList->size(); i++) {
		Transitions *transitions = getTransitions(i);
		int buf = 0;
		int numTransitions = 0;

		xscanf(stream, "Transitions for State %d: %d\n", buf, numTransitions);

		for(int j = 0; j < numTransitions; j++) {
			int newStateIndex = 0;
			RealType reward = 0.0;
			RealType discountFactor = 0.0;

			Action *action = nullptr;
			ActionData *actionData = nullptr;
			int actionIndex = -1;

			xscanf(stream, "%d %lf %lf %d ", newStateIndex, reward,
			    discountFactor, actionIndex);

			if(actionIndex >= 0) {
				action = actions->get(actionIndex);

				actionData = action->getNewActionData();

				if(actionData != nullptr) {
					actionData->load(stream);
				}
			}

			addTransition(i, newStateIndex, action, actionData, reward,
			    discountFactor);

			gesTransitions++;
		}

		xscanf(stream, "\n");
		assert(numTransitions == getTransitions(i)->size());
	}
}

void SamplingBasedGraph::saveData(std::ostream& stream) {
	stateList->saveData(stream);

	fmt::fprintf(stream, "Continuous Graph\n");
	for(unsigned int i = 0; i < stateList->size(); i++) {
		Transitions *transitions = getTransitions(i);
		fmt::fprintf(stream, "Transitions for State %d: %d\n", i,
		    transitions->size());
		Transitions::iterator it = transitions->begin();

		for(; it != transitions->end(); it++) {
			int actionIndex = -1;

			fmt::fprintf(stream, "%d %f %f ", (*it)->newStateIndex, (*it)->reward,
			    (*it)->discountFactor);
			if((*it)->action != nullptr) {
				fmt::fprintf(stream, "%d ", actions->getIndex((*it)->action));
			} else {
				fmt::fprintf(stream, "-1 ");
			}

			if((*it)->actionData) {
				(*it)->actionData->save(stream);
			}
		}

		fmt::fprintf(stream, "\n");
	}
}

void SamplingBasedGraph::resetData() {
	clearTransitions();
}

int SamplingBasedGraph::getNumStates() {
	return stateList->size();
}

std::list<GraphTransition *> *SamplingBasedGraph::getTransitions(int index) {
	return (*transitions)[index];
}

ContinuousStateList *SamplingBasedGraph::getStateList() {
	return stateList;
}

void SamplingBasedGraph::getConnectedNodes(int node, DataSubset *subset) {
	subset->insert(node);

	Transitions *transitions = getTransitions(node);
	Transitions::iterator it = transitions->begin();

	for(; it != transitions->end(); it++) {
		int newNode = (*it)->newStateIndex;

		if(newNode >= 0) {
			if(subset->find(newNode) == subset->end()) {
				getConnectedNodes(newNode, subset);
			}
		}
	}
}

void SamplingBasedGraph::createTransitions() {
	clearTransitions();

	for(int i = 0; i < stateList->size(); i++) {
		addTransitions(i, false);
	}

	printf("Added %d Transitions to the graph\n", numTransitions);
}

void SamplingBasedGraph::addState(State *state) {
	int oldSize = stateList->size();
	int index = stateList->addState(state);

	if(oldSize < stateList->size()) {
		(*transitions)[oldSize] = new Transitions();
		addTransitions(oldSize, true);
	}
}

void SamplingBasedGraph::addTransitions(int node, bool newNode) {
	DataSubset elementList;

	if(isFinalNode(node)) {
		addFinalTransition(node);
	} else {
		elementList.clear();
		getNeighboredNodes(node, &elementList);

		DataSubset::iterator it = elementList.begin();

		/*if (elementList.size() > 0)
		 {
		 printf("Found %d States for State %d ", elementList.size(), i);
		 }
		 else
		 {
		 printf("Error: NO States found in the neighborhood of %d!!\n", i);
		 }*/

		for(int m = 0; it != elementList.end(); it++, m++) {
			if(*it != node) {
				calculateTransition(node, *it);
				if(newNode) {
					calculateTransition(*it, node);
				}
			}
		}

		if(getTransitions(node)->size() == 0) {
			printf("No transitions found for node %d\n", node);
			ColumnVector *nodeVector = (*stateList)[node];
			std::cout << nodeVector->transpose();
		}
	}
}

GraphTarget::GraphTarget(GraphTarget *l_nextTarget) {
	nextTarget = l_nextTarget;
}

GraphTarget::~GraphTarget() {}

GraphTransitionAdaptiveTarget::GraphTransitionAdaptiveTarget(
    int newStateIndex, RealType reward, RealType discountFactor, Action *action,
    ActionData *actionData, GraphTarget **l_currentTarget) :
	GraphTransition(newStateIndex, reward, discountFactor, action, actionData) {
	targetReward = new std::map<GraphTarget *, RealType>();
	targetReached = new std::map<GraphTarget *, bool>();
	currentTarget = l_currentTarget;
}

GraphTransitionAdaptiveTarget::~GraphTransitionAdaptiveTarget() {
	delete targetReward;
	delete targetReached;
}

RealType GraphTransitionAdaptiveTarget::getReward() {
	return getReward(*currentTarget);
}

RealType GraphTransitionAdaptiveTarget::getReward(GraphTarget *target) {
	std::map<GraphTarget *, RealType>::iterator it = targetReward->find(target);

	if(it != targetReward->end()) {
		//printf("Reward: %f, Target Reward: %f\n", reward, (*it).second);
		return reward + (*it).second;
	} else {
		//printf("Reward: %f\n", reward);
		return reward;
	}
}

bool GraphTransitionAdaptiveTarget::isFinished(GraphTarget *target) {
	std::map<GraphTarget *, bool>::iterator it = targetReached->find(target);

	if(it != targetReached->end()) {
		return (*it).second;
	} else {
		return false;
	}
}

void GraphTransitionAdaptiveTarget::addTarget(
    GraphTarget *target, RealType reward, bool isFinished) {
	(*targetReward)[target] = reward;
	(*targetReached)[target] = isFinished;
}

AdaptiveTargetGraph::AdaptiveTargetGraph(
    ContinuousStateList *stateList, ActionSet *actions) :
	SamplingBasedGraph(stateList, actions) {
	targetList = new std::list<GraphTarget *>();
	currentTarget = nullptr;
}

AdaptiveTargetGraph::~AdaptiveTargetGraph() {
	delete targetList;
}

void AdaptiveTargetGraph::setCurrentTarget(GraphTarget *target) {
	currentTarget = target;
}

void AdaptiveTargetGraph::addTransition(
    int index, int newIndex, Action *action, ActionData *actionData,
    RealType reward, RealType discountFactor) {
	Transitions *transSamples = nullptr;
	transSamples = (*transitions)[index];

	GraphTransition *trans = new GraphTransitionAdaptiveTarget(newIndex, reward,
	    discountFactor, action, actionData, &currentTarget);

	transSamples->push_back(trans);

	numTransitions++;
}

void AdaptiveTargetGraph::addTarget(GraphTarget *target) {
	targetList->push_back(target);

	for(int i = 0; i < stateList->size(); i++) {
		addTargetForNode(target, i);
	}
}

void AdaptiveTargetGraph::addTargetForNode(GraphTarget *target, int i) {
	ColumnVector *node = (*stateList)[i];

	int numFinished = 0;
	int numCanditates = 0;

	if(target->isFinishedCanditate(node)) {
		Transitions *transitions = getTransitions(i);

		Transitions::iterator it = transitions->begin();

		numCanditates++;

		for(; it != transitions->end(); it++) {
			GraphTransitionAdaptiveTarget *targetTrans =
			    dynamic_cast<GraphTransitionAdaptiveTarget *>(*it);

			ColumnVector *newNode = (*stateList)[targetTrans->newStateIndex];

			RealType reward = 0.0;
			bool finished = target->isFinished(node, newNode, reward);

			if(finished || fabs(reward) > 0.001) {
				targetTrans->addTarget(target, reward, finished);

				if(finished) {
					numFinished++;
//					printf("End Node %d: %f %f\n", targetTrans->newStateIndex, newNode->operator[](0), newNode->operator[](1));
				}
			}
		}
		//printf("%d Canditates, %d Final Transitions\n", numCanditates, numFinished);
	}
}

void AdaptiveTargetGraph::addState(State *addState) {
	int oldSize = stateList->size();

	SamplingBasedGraph::addState(addState);

	if(oldSize < stateList->size()) {
		std::list<GraphTarget *>::iterator it = targetList->begin();

		for(; it != targetList->end(); it++) {
			addTargetForNode(*it, oldSize);
		}
	}
}

GraphController::GraphController(
    ActionSet *actionSet, GraphDynamicProgramming *l_graph) :
	AgentController(actionSet) {
	graph = l_graph;
}

GraphController::~GraphController() {}

Action *GraphController::getNextAction(
    StateCollection *state, ActionDataSet *dataSet) {
	State *inputState = state->getState(
	    graph->getGraph()->getStateProperties());

	int nearestNeighbor = -1;
	RealType distance = -1.0;
	RealType maxValue = 0.0;

	graph->getNearestNode(inputState, nearestNeighbor, distance);

	GraphTransition *transition = graph->getMaxTransition(nearestNeighbor,
	    maxValue);

	//assert(transition != nullptr);

	if(transition == nullptr) {
		return actions->get(0);
	}

	if(dataSet && transition->actionData) {
		dataSet->setActionData(transition->action, transition->actionData);
	}
	return transition->action;
}

AdaptiveTargetGraphController::AdaptiveTargetGraphController(
    ActionSet *actionSet,
    GraphAdaptiveTargetDynamicProgramming *l_adaptiveGraph) :
	AgentController(actionSet) {
	adaptiveGraph = l_adaptiveGraph;
}

AdaptiveTargetGraphController::~AdaptiveTargetGraphController() {}

Action *AdaptiveTargetGraphController::getNextAction(
    StateCollection *state, ActionDataSet *dataSet) {

	GraphTarget *target = adaptiveGraph->getTargetForState(state);

	adaptiveGraph->setCurrentTarget(target);

	int index = -1;
	for(int i = 0; i < adaptiveGraph->getNumTargets(); i++) {
		if(target == adaptiveGraph->getTarget(i)) {
			index = i;
			break;
		}
	}
	printf("Controller Target: %d\n", index);

	State *inputState = state->getState(
	    adaptiveGraph->getGraph()->getStateProperties());

	if(target == nullptr) {
		printf("No target found for state ");
		state->getState()->save(std::cout);
		printf("\n");

	}

	int nearestNeighbor = -1;
	RealType distance = -1.0;
	RealType maxValue = 0.0;

	adaptiveGraph->getNearestNode(inputState, nearestNeighbor, distance);

	GraphTransition *transition = adaptiveGraph->getMaxTransition(
	    nearestNeighbor, maxValue);

	//assert(transition != nullptr);

	if(transition == nullptr) {
		printf("No graph transition found... Using random action: %d\n",
		    nearestNeighbor);
		ColumnVector *node =
		    (*adaptiveGraph->getGraph()->getStateList())[nearestNeighbor];

		std::cout << node->transpose();

		inputState->save(std::cout);
		printf("\n");
		return actions->get(0);
	}

	if(dataSet && transition->actionData) {
		dataSet->setActionData(transition->action, transition->actionData);
	}
	return transition->action;
}

GraphDebugger::GraphDebugger(
    GraphDynamicProgramming *l_graph,
    RewardFunction *reward,
    StateModifier *l_hcState
):
	SemiMDPRewardListener(reward)
{
	graph = l_graph;

	realRewardSum = 0.0;
	graphRewardSum = 0.0;

	hcState = l_hcState;
	step = 0;
}

void GraphDebugger::nextStep(
    StateCollection *oldState,
    Action *action,
    RealType reward,
    StateCollection *newState
) {
	State *inputState = oldState->getState(
	    graph->getGraph()->getStateProperties());

	int nearestNeighbor = -1;
	RealType distance = -1.0;
	RealType maxValue = 0.0;

	graph->getNearestNode(inputState, nearestNeighbor, distance);

	KDRectangle range(4);

	/*RealType nextTarget = oldState->getState(hcState)->getContinuousState(14) * 100;
	 range.setMinValue(3, nextTarget - 0.0001);
	 range.setMaxValue(3, nextTarget + 0.0001);
	 */

	GraphTransition *transition = graph->getMaxTransition(nearestNeighbor,
	    maxValue); //, &range);

	realRewardSum += reward;

	if(transition == nullptr) {
		printf("No transition found\n");
		return;
	}

	RealType graphReward = transition->getReward();
	graphRewardSum += graphReward;

	State *newGraphState = newState->getState(
	    graph->getGraph()->getStateProperties());

	ColumnVector *newNode =
	    (*graph->getGraph()->getStateList())[transition->newStateIndex];

	ColumnVector distVec = *newGraphState - *newNode;

	if(fabs(graphReward - reward) > 0.001 || distVec.norm() > 0.0001
	    || (*graph->getOutputValues())[nearestNeighbor] < -10 || true) {
		printf("%d : Transition from Node %d to %d, target: %d\n", step,
		    nearestNeighbor, transition->newStateIndex,
		    oldState->getState()->getDiscreteState(0));
		printf("Real reward: %f, Graph reward: %f, Graph Value: %f (%f %f)\n",
		    reward, graphReward, (*graph->getOutputValues())[nearestNeighbor],
		    realRewardSum, graphRewardSum);

		ContinuousActionData *data =
		    dynamic_cast<ContinuousActionData *>(action->getActionData());

		ContinuousActionData *graphData =
		    dynamic_cast<ContinuousActionData *>(transition->actionData);

		if(graphData) {
			printf("RealAction: %f %f, Graph Action: %f %f\n",
			    data->operator[](0), data->operator[](3),
			    graphData->operator[](0), graphData->operator[](3));
		}

		oldState->getState(graph->getGraph()->getStateProperties())->save(std::cout);
		printf("\n");

		newState->getState(graph->getGraph()->getStateProperties())->save(std::cout);
		printf("\n");

		std::cout
		    << (*graph->getGraph()->getStateList())[nearestNeighbor]->transpose();

		if(transition->newStateIndex >= 0) {
			std::cout
			    << (*graph->getGraph()->getStateList())[transition->newStateIndex]->transpose();
		} else {
			std::cout << "End State\n";
		}
	}

	step++;
}

void GraphDebugger::newEpisode() {
	printf("Real reward Sum %f, Graph %f\n", realRewardSum, graphRewardSum);

	realRewardSum = 0.0;
	graphRewardSum = 0.0;
	step = 0;
}

GraphValueFromValueFunctionCalculator::GraphValueFromValueFunctionCalculator(
    GraphDynamicProgramming *l_graph, SupervisedLearner *l_learner,
    PolicyEvaluator *l_evaluator) {
	graph = l_graph;
	learner = l_learner;
	evaluator = l_evaluator;
}

GraphValueFromValueFunctionCalculator::~GraphValueFromValueFunctionCalculator() {}

RealType GraphValueFromValueFunctionCalculator::evaluate() {
	learner->learnFA(graph->getGraph()->getStateList(),
	    graph->getOutputValues());

	RealType value = evaluator->evaluatePolicy();
	printf("Learned Value Function: %f\n", value);

	return value;
}

} //namespace rlearner
