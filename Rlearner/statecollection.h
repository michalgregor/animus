// 	Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cstatecollection_H_
#define Rlearner_cstatecollection_H_

#include <list>
#include <map>
#include <memory>
#include <iostream>

#include "baseobjects.h"
#include "StateModifierList.h"
#include "state.h"

namespace rlearner {

class StateCollectionImpl;
class StateCollectionList;

class StateCollectionReporter: public ModifierListReporter {
private:
	StateCollectionImpl* _stateCollection;

public:
	virtual void modifiersAdded(StateModifierList::iterator first,
		StateModifierList::iterator last);

	virtual void modifiersRemoved(StateModifierList::iterator first,
		StateModifierList::iterator last);

public:
	StateCollectionReporter(StateCollectionImpl* stateCollection):
		_stateCollection(stateCollection) {}
	virtual ~StateCollectionReporter() = default;
};

/**
 * @class StateCollectionImpl
 * Implementation of the StateCollection interface.
 *
 * A state collection contains the "basic" state, usually the model state,
 * and a list of modified states with their state modifiers. Any component
 * which has access to a state collection (usually a listener), can retrieve
 * a state from the state collection as long as he maintains the pointer to
 * the properties of the desired state, which serves as the id of the state
 * in the state collection. When you add a state modifier, a state with the
 * properties of the modifier is created and stored in a map (modifiedStates).
 * Each time you change the model state you have to call the function
 * newModelState(), the collection sets then all modified states as deprecated.
 * The modified state gets recalculated when it is requested for the first time
 * and the deprecated flag of the state gets cleared.
 *
 * You can also set the modified states directly without calculation, this is
 * useful if the modified state is already available by a logger.
 */
class StateCollectionImpl: public StateModifiersObject,
        public StateCollection {
public:
	friend class StateCollectionReporter;

	class StateEntry {
	public:
		std::unique_ptr<State> state;
		bool calculated;
		size_t refcount = 0;

	public:
		StateEntry(std::unique_ptr<State>&& state_, bool calculated_ = false):
			state(std::move(state_)), calculated(calculated_) {}
	};

protected:
	//! Basic model state of the collection.
	std::unique_ptr<State> _modelState;
	//! Stores all state modifiers with their states.
	std::map<StateModifier*, StateEntry> _modifiedStates;

	//! A reporter that manages addition/removal of state modifiers.
	StateCollectionReporter _reporter{this};

protected:
	//! Adds a state modifier to the modified states list and increments its
	//! reference counter, if already present. If already present, only the
	//! reference count is increased.
	void addStateModifier(StateModifier* modifier);

	//! Decrements the reference counter for the modifier and erases it if
	//! the counter reaches zero.
	//!
	//! If the modifier is not present, this has no effect.
	void removeStateModifier(StateModifier* modifier);

public:
	//! Calculate all modified states which are deprecated.
	void calculateModifiedStates();
	//! Set the state in the state collection with the same properties
	//! as the given state.
	void setState(State *state);

	void setStateCollection(StateCollection *stateCollection);

	//! Get state with the given properties.
	virtual State *getState(StateProperties *properties);
	//! Get basic state.
	virtual State *getState();
	//! Add a list of state modifiers.
	virtual void addModifierList(StateModifierList* modifierList);
	//! Remove a list of state modifiers.
	virtual void removeModifierList(StateModifierList* modifierList);

	//! Returns whether the state has already been calculated.
	virtual bool isStateCalculated(StateModifier *);

	//! Returns whether the state has already been calculated.
	virtual void setIsStateCalculated(StateModifier *modifier,
	        bool isCalculated);

	//! Returns whether the modifier states or the basic state have
	//! the specified properties.
	virtual bool isMember(StateProperties *stateModifier);

	//! Marks all modified states as deprecated, forces them to recalculate.
	void newModelState();

	virtual void setResetState(bool reset);

public:
	StateCollectionImpl& operator=(const StateCollectionImpl&) = delete;
	StateCollectionImpl(const StateCollectionImpl&) = delete;

	//! Create state collection with the given state properties as basic state.
	StateCollectionImpl(StateProperties* modelProperties);
	//! Copy state collection.
	StateCollectionImpl(StateCollectionImpl* stateCollection);
	//! Create state collection with the given state properties as basic state
	//! and the given modifiers already added.
	StateCollectionImpl(StateProperties* properties,
	        const std::set<StateModifierList*>& modifierLists);

	virtual ~StateCollectionImpl();
};

class StateCollectionListReporter: public ModifierListReporter {
private:
	StateCollectionList* _stateCollectionList;

public:
	virtual void modifiersAdded(StateModifierList::iterator first,
		StateModifierList::iterator last);

	virtual void modifiersRemoved(StateModifierList::iterator first,
		StateModifierList::iterator last);

public:
	StateCollectionListReporter(StateCollectionList* stateCollectionList):
		_stateCollectionList(stateCollectionList) {}
	virtual ~StateCollectionListReporter() = default;
};

/**
 * @class StateCollectionList
 * Class for storing a sequence of state collections.
 *
 * This class is able to store a sequence of state collections, all state
 * collections added to the list are supposed to have the same state modifiers.
 * For storing the collection it uses a list of StateList objects, for each
 * state in the collection there is a state list. When a state collection is
 * added, the collection is split into its states and the states are added
 * to the state lists.
 *
 * When retrieving a state collection from the list, only the states which are
 * member of the given state collection get set in the collection, no new state
 * modifiers get added.
 *
 * @see StateList
 */
class StateCollectionList: public StateModifiersObject {
public:
	friend class StateCollectionListReporter;

	class StateEntry {
	public:
		std::unique_ptr<StateList> stateList;
		size_t refcount = 0;

	public:
		StateEntry(std::unique_ptr<StateList>&& stateList_):
			stateList(std::move(stateList_)) {}
	};

protected:
	//! A map from StateProperties* to the contained state lists.
	std::map<StateProperties*, StateEntry> _stateLists;
	//! A reporter that manages addition/removal of state modifiers.
	StateCollectionListReporter _reporter{this};

protected:
	/**
	 * Add a state modifier to the collection list.
	 *
	 * A new state list is created for the modifier.
	 */
	void addStateModifier(StateModifier* modifier);

	//! Remove the state modifier from the list, the corresponding state list
	//! is deleted.
	void removeStateModifier(StateModifier* modifier);

public:
	//! Clears all state lists.
	void clearStateLists();

	/**
	 * Add state collection to the list.
	 *
	 * The collection is split into its states and the states are added
	 * to the state lists.
	 */
	void addStateCollection(StateCollection *stateCollection);

	/**
	 * Retrieve a collection from the list.
	 *
	 * When retrieving a state collection from the list, only the states which
	 * are members of the given state collection get set in the collection,
	 * no new state modifiers are added.
	 */
	void getStateCollection(int index, StateCollectionImpl* stateCollection);

	void removeLastStateCollection();

	StateList* getStateList(StateProperties* properties);

	//! Get a State directly from the collection list, without the need
	//! of an state collection.
	void getState(int index, State *state);

	/**
	 * Add a list of state modifiers.
	 *
	 * A new state list is created for every modifier in the list. State lists
	 * are also automatically created/erased upon modifications in
	 * the modifierList.
	 */
	virtual void addModifierList(StateModifierList* modifierList);

	//! Remove a list of state modifiers.
	virtual void removeModifierList(StateModifierList* modifierList);

	void load(std::istream& stream);
	void save(std::ostream& stream);

	int getNumStateCollections();

public:
	StateCollectionList& operator=(const StateCollectionList&) = delete;
	StateCollectionList(const StateCollectionList&) = delete;

	//! Create a state collection list with "model" as the basic state for
	//! the collections.
	StateCollectionList(StateProperties *model);
	//! Create a state collection list with "model" as the basic state for
	//! the collections and the modifiers already added.
	StateCollectionList(StateProperties *model,
		const std::set<StateModifierList*>& modifierLists);
	virtual ~StateCollectionList();
};

} //namespace rlearner

#endif //Rlearner_cstatecollection_H_
