// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "ril_debug.h"
#include "valuepolicygradientlearner.h"

#include "qfunction.h"
#include "vfunction.h"
#include "qetraces.h"
#include "residuals.h"
#include "policygradient.h"
#include "transitionfunction.h"
#include "pegasus.h"
#include "continuousactiongradientpolicy.h"

#include "state.h"
#include "statecollection.h"
#include "action.h"
#include "stateproperties.h"
#include "statemodifier.h"

namespace rlearner {

VPolicyLearner::VPolicyLearner(
    StateReward *rewardFunction, TransitionFunction *dynModel,
    TransitionFunctionInputDerivationCalculator *dynModeldInput,
    AbstractVFunction *vFunction,
    VFunctionInputDerivationCalculator *vFunctionInputDerivation,
    ContinuousActionGradientPolicy *gradientPolicy,
    CAGradientPolicyInputDerivationCalculator *policydInput,
    const std::set<StateModifierList*>& modifierLists,
    int nForwardView
):
	SemiMDPRewardListener(rewardFunction), _modifierLists(modifierLists)
{
	this->rewardFunction = rewardFunction;
	this->vFunction = vFunction;
	this->gradientPolicy = gradientPolicy;
	this->dynModeldInput = dynModeldInput;
	this->policydInput = policydInput;
	this->vFunctionInputDerivation = vFunctionInputDerivation;
	this->dynModel = dynModel;

	dPolicy = new Matrix(gradientPolicy->getNumOutputs(),
	    dynModel->getNumContinuousStates());
	dModelInput = new Matrix(dynModel->getNumContinuousStates(),
	    gradientPolicy->getNumOutputs() + dynModel->getNumContinuousStates());
	dReward = new ColumnVector(dynModel->getNumContinuousStates());
	dVFunction = new ColumnVector(dynModel->getNumContinuousStates());
	data = new ContinuousActionData(
	    gradientPolicy->getContinuousActionProperties());

//	states = new std::list<State *>();

	addParameter("DiscountFactor", 0.95);
	addParameter("PolicyLearningRate", 1.0);
	addParameter("PolicyLearningFowardView", nForwardView);
	addParameter("PolicyLearningBackwardView", 0.0);
	addParameters(policydInput, "DPolicy");
	addParameters(dynModeldInput, "DModel");
	addParameters(vFunctionInputDerivation, "DVFunction");

	tempStateCol = new StateCollectionImpl(dynModel->getStateProperties(),
		_modifierLists);

	stateGradient1 = new StateGradient();

	for(unsigned int i = 0; i < dynModel->getNumContinuousStates(); i++) {
		stateGradient1->push_back(new FeatureList());
	}

	stateGradient2 = new StateGradient();

	for(unsigned int i = 0; i < dynModel->getNumContinuousStates(); i++) {
		stateGradient2->push_back(new FeatureList());
	}

	dModelGradient = new StateGradient();

	for(int i = 0; i < gradientPolicy->getNumOutputs(); i++) {
		dModelGradient->push_back(new FeatureList());
	}

	policyGradient = new FeatureList();

	pastStates = new std::list<StateCollectionImpl *>();
	pastDRewards = new std::list<ColumnVector *>();
	pastActions = new std::list<ContinuousActionData *>();

	statesResource = new std::list<StateCollectionImpl *>();
	rewardsResource = new std::list<ColumnVector *>();
	actionsResource = new std::list<ContinuousActionData *>();
}

VPolicyLearner::~VPolicyLearner() {
	newEpisode();
//	delete states;
	delete dPolicy;
	delete dModelInput;
	delete dReward;
	delete stateGradient1;
	delete stateGradient2;
	delete dModelGradient;
	delete tempStateCol;
	delete data;
	delete dVFunction;
	delete policyGradient;
	delete pastStates;
	delete pastDRewards;
	delete pastActions;

	newEpisode();

	std::list<StateCollectionImpl *>::iterator itStates =
	    statesResource->begin();
	for(; itStates != statesResource->end(); itStates++) {
		delete *itStates;
	}

	std::list<ColumnVector *>::iterator itRewards = rewardsResource->begin();
	for(; itRewards != rewardsResource->end(); itRewards++) {
		delete *itRewards;
	}

	std::list<ContinuousActionData *>::iterator itActions =
	    actionsResource->begin();
	for(; itActions != actionsResource->end(); itActions++) {
		delete *itActions;
	}

	delete statesResource;
	delete rewardsResource;
	delete actionsResource;
}

void VPolicyLearner::newEpisode() {
	std::list<StateCollectionImpl *>::iterator itStates = pastStates->begin();
	for(; itStates != pastStates->end(); itStates++) {
		statesResource->push_back(*itStates);
	}
	pastStates->clear();

	std::list<ColumnVector *>::iterator itRewards = pastDRewards->begin();
	for(; itRewards != pastDRewards->end(); itRewards++) {
		rewardsResource->push_back(*itRewards);
	}
	pastDRewards->clear();

	std::list<ContinuousActionData *>::iterator itActions =
	    pastActions->begin();
	for(; itActions != pastActions->end(); itActions++) {
		actionsResource->push_back(*itActions);
	}
	pastActions->clear();
}

void VPolicyLearner::multMatrixFeatureList(
    Matrix *matrix, FeatureList *features, int index,
    std::list<FeatureList *> *newFeatures) {
	FeatureList::iterator itFeat = features->begin();

	for(; itFeat != features->end(); itFeat++) {
		std::list<FeatureList *>::iterator itList = newFeatures->begin();
		for(int row = 0; itList != newFeatures->end(); itList++, row++) {
			(*itList)->update((*itFeat)->featureIndex,
			    (*itFeat)->factor * matrix->operator()(row, index));
		}
	}
}

void VPolicyLearner::getDNextState(
    StateGradient *stateGradient1, StateGradient *stateGradient2,
    StateCollection *currentState, ContinuousActionData *data) {
	// Clear 2nd StateGradient list
	StateGradient::iterator it = stateGradient2->begin();

	for(; it != stateGradient2->end(); it++) {
		(*it)->clear();
	}

	//Clear Model Gradient
	it = dModelGradient->begin();

	for(; it != dModelGradient->end(); it++) {
		(*it)->clear();
	}

	// Derivation of the Model
	dynModeldInput->getInputDerivation(
	    currentState->getState(dynModel->getStateProperties()), data,
	    dModelInput);

	it = stateGradient1->begin();
	for(unsigned int i = 0; i < dynModel->getNumContinuousStates(); i++, it++) {
		multMatrixFeatureList(dModelInput, *it, i, stateGradient2);
	}

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "Pegasus Gradient Calculation:\n ");
		DebugPrint('p', "State Gradient:\n ");
		for(it = stateGradient1->begin(); it != stateGradient1->end(); it++) {
			(*it)->save(DebugGetFileHandle('p'));
			DebugPrint('p', "\n");
		}

		DebugPrint('p', "\n");
		DebugPrint('p', "dModel: ");
		//dModelInput->saveASCII(DebugGetFileHandle('p'));
	}

	// Input-Derivation of the policy
	policydInput->getInputDerivation(currentState, dPolicy);

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "dPolicy: ");
		//dPolicy->saveASCII(DebugGetFileHandle('p'));
		DebugPrint('p', "\n");
	}

	// Derivation with respect to the weights
	it = dModelGradient->begin();

	// Gradient = d_Pi(s)/dw
	for(int i = 0; it != dModelGradient->end(); it++, i++) {
		gradientPolicy->getGradient(currentState, i, *it);
	}

	it = stateGradient1->begin();
	//Pi'(s) * s'
	for(int i = 0; it != stateGradient1->end(); i++, it++) {
		multMatrixFeatureList(dPolicy, *it, i, dModelGradient);
	}

	it = dModelGradient->begin();

	for(int i = 0; it != dModelGradient->end(); it++, i++) {
		multMatrixFeatureList(dModelInput, *it,
		    i + dynModel->getNumContinuousStates(), stateGradient2);
	}

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "Model Gradients:\n ");
		for(it = dModelGradient->begin(); it != dModelGradient->end(); it++) {
			(*it)->save(DebugGetFileHandle('p'));
			DebugPrint('p', "\n");

		}
		DebugPrint('p', "New State Gradient:\n ");

		for(it = stateGradient2->begin(); it != stateGradient2->end(); it++) {
			(*it)->save(DebugGetFileHandle('p'));
			DebugPrint('p', "\n");

		}

	}
}

void VPolicyLearner::calculateGradient(
    std::list<StateCollectionImpl *> *states,
    std::list<ColumnVector *> *Drewards,
    std::list<ContinuousActionData *> *actionDatas,
    FeatureList *policyGradient
) {
//	RealType gamma = getParameter("DiscountFactor");

	policyGradient->clear();

	std::list<StateCollectionImpl *>::iterator itStates = states->begin();
	std::list<ColumnVector *>::iterator itRewards = Drewards->begin();
	std::list<ContinuousActionData *>::iterator itActions =
	    actionDatas->begin();

	StateGradient::iterator it = stateGradient1->begin();

	for(; it != stateGradient1->end(); it++) {
		(*it)->clear();
	}

	it = stateGradient2->begin();

	for(; it != stateGradient2->end(); it++) {
		(*it)->clear();
	}

	for(unsigned int i = 0; itStates != states->end();
	    itStates++, itRewards++, itActions++, i++) {
		getDNextState(stateGradient1, stateGradient2, *itStates, *itActions);

		StateGradient *tempStateGradient = stateGradient1;
		stateGradient1 = stateGradient2;
		stateGradient2 = tempStateGradient;

		if(i < states->size() - 1) {
			StateGradient::iterator itGradient = stateGradient1->begin();
			for(int j = 0; itGradient != stateGradient1->end();
			    itGradient++, j++) {
				policyGradient->add(*itGradient, (*itRewards)->operator[](j));
			}
		} else {
			vFunctionInputDerivation->getInputDerivation(*itStates, dVFunction);

			DebugPrint('p', "dVFunction : ");

			if(DebugIsEnabled('p')) {
				//dVFunction->saveASCII(DebugGetFileHandle('p'));
			}

			StateGradient::iterator itGradient = stateGradient1->begin();
			for(int j = 0; itGradient != stateGradient1->end();
			    itGradient++, j++) {
				policyGradient->add(*itGradient, dVFunction->operator[](j));
			}
		}
	}
}

void VPolicyLearner::nextStep(
    StateCollection *, Action *action, RealType, StateCollection *nextState
) {
	int nForwardView = (int) getParameter("PolicyLearningFowardView");
	int nBackwardView = (int) getParameter("PolicyLearningBackwardView");

	StateCollectionImpl *currentState;
	ContinuousActionData *currentAction;

	if(statesResource->size() > 0) {
		currentAction = *actionsResource->begin();
		actionsResource->pop_front();

		currentState = *statesResource->begin();
		statesResource->pop_front();
	} else {
		currentAction =
		    new ContinuousActionData(
		        gradientPolicy->getContinuousAction()->getContinuousActionProperties());
		currentState = new StateCollectionImpl(dynModel->getStateProperties(),
			_modifierLists);
	}

	currentAction->setData(action->getActionData());
	currentState->setStateCollection(nextState);

	pastStates->push_back(currentState);
	pastActions->push_back(currentAction);

	// FORWARD View
	int gradientPolicyRandomMode = gradientPolicy->getRandomControllerMode();
	gradientPolicy->setRandomControllerMode(RandomController_NoRandom);

	ColumnVector *currentReward;

	for(int i = 0; i < nForwardView - 1; i++) {
		StateCollection *lastState = *pastStates->rbegin();
		if(statesResource->size() > 0) {
			currentAction = *actionsResource->begin();
			actionsResource->pop_front();

			currentState = *statesResource->begin();
			statesResource->pop_front();
		} else {
			currentAction =
			    new ContinuousActionData(
			        gradientPolicy->getContinuousAction()->getContinuousActionProperties());
			currentState = new StateCollectionImpl(
			    dynModel->getStateProperties(), _modifierLists);
		}

		if(rewardsResource->size() > 0) {
			currentReward = *rewardsResource->begin();
			rewardsResource->pop_front();
		} else {
			currentReward = new ColumnVector(
			    dynModel->getNumContinuousStates());
		}

		gradientPolicy->getNextContinuousAction(lastState, currentAction);
		dynModel->transitionFunction(
		    lastState->getState(dynModel->getStateProperties()),
		    gradientPolicy->getContinuousAction(),
		    currentState->getState(dynModel->getStateProperties()),
		    currentAction);
		currentState->newModelState();

		rewardFunction->getInputDerivation(
		    lastState->getState(dynModel->getStateProperties()), currentReward);

		pastStates->push_back(currentState);
		pastDRewards->push_back(currentReward);
		pastActions->push_back(currentAction);
	}

	calculateGradient(pastStates, pastDRewards, pastActions, policyGradient);
	DebugPrint('p', "policyGradient for Update : ");

	if(DebugIsEnabled('p')) {
		policyGradient->save(DebugGetFileHandle('p'));
	}

	gradientPolicy->updateGradient(policyGradient,
	    getParameter("PolicyLearningRate"));
	gradientPolicy->setRandomControllerMode(gradientPolicyRandomMode);

	for(int i = 0; i < nForwardView - 1; i++) {
		statesResource->push_back(*pastStates->rbegin());
		actionsResource->push_back(*pastActions->rbegin());

		pastStates->pop_back();
		pastActions->pop_back();

		rewardsResource->push_back(*pastDRewards->rbegin());
		pastDRewards->pop_back();
	}

	if(nBackwardView > 0) {
		if(rewardsResource->size() > 0) {
			currentReward = *rewardsResource->begin();
			rewardsResource->pop_front();
		} else {
			currentReward = new ColumnVector(
			    dynModel->getNumContinuousStates());
		}
		rewardFunction->getInputDerivation(
		    (*pastStates->rbegin())->getState(dynModel->getStateProperties()),
		    currentReward);

		pastDRewards->push_back(currentReward);
	}

	if(pastStates->size() > (unsigned int) nBackwardView) {
		statesResource->push_back(*pastStates->begin());
		actionsResource->push_back(*pastActions->begin());

		pastStates->pop_front();
		pastActions->pop_front();

		if(nBackwardView > 0) {
			rewardsResource->push_back(*pastDRewards->begin());
			pastDRewards->pop_front();
		}
	}
}

} //namespace rlearner
