// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>
#include <ctime>
#include <cstring>
#include <fstream>

#include "batchlearning.h"

#include "rewardmodel.h"
#include "residuals.h"
#include "continuousactions.h"
#include "treebatchlearning.h"
#include "montecarlo.h"
#include "history.h"
#include "lstd.h"
#include "agentlogger.h"
#include "kdtrees.h"
#include "nearestneighbor.h"
#include "samplingbasedmodel.h"

#include "action.h"
#include "state.h"
#include "statecollection.h"
#include "stateproperties.h"
#include "episode.h"
#include "episodehistory.h"
#include "vfunction.h"
#include "qfunction.h"
#include "policies.h"
#include "gradientfunction.h"
#include "AgentSimulator.h"

namespace rlearner {

BatchLearningPolicy::BatchLearningPolicy(ActionSet *actions) :
	DeterministicController(actions) {
	actionDataSet = new ActionDataSet(actions);
	nextAction = nullptr;
}

BatchLearningPolicy::~BatchLearningPolicy() {
	delete actionDataSet;
}

Action *BatchLearningPolicy::getNextAction(
    StateCollection *, ActionDataSet *actionData) {
	assert(nextAction != nullptr);
	if(actionData) {
		actionData->setActionData(nextAction,
		    actionDataSet->getActionData(nextAction));
	}
	return nextAction;
}

void BatchLearningPolicy::setAction(Action *action, ActionData *data) {
	nextAction = action;
	if(data) {
		actionDataSet->setActionData(nextAction, data);
	}
}

PolicyEvaluation::PolicyEvaluation(int maxEpisodes) {
	addParameter("PolicyEvaluationMaxEpisodes", maxEpisodes);
}

PolicyEvaluation::~PolicyEvaluation() {

}

void PolicyEvaluation::evaluatePolicy() {
	int maxEvaluationEpisodes = (int) getParameter(
	    "PolicyEvaluationMaxEpisodes");
	evaluatePolicy(maxEvaluationEpisodes);
}

RealType PolicyEvaluationGradientFunction::getWeightDifference(
    RealType *oldWeights) {
	RealType sum = 0;
	RealType *newWeights = new RealType[learnData->getNumWeights()];

	learnData->getWeights(newWeights);

	for(int i = 0; i < learnData->getNumWeights(); i++) {
		sum += pow(oldWeights[i] - newWeights[i], 2);
	}

	delete[] newWeights;

	return sqrt(sum);
}

PolicyEvaluationGradientFunction::PolicyEvaluationGradientFunction(
    GradientUpdateFunction *l_learnData, RealType l_treshold, int maxEpisodes) {
	addParameter("PolicyEvaluationThreshold", l_treshold);
	addParameter("PolicyEvaluationMaxEpisodes", maxEpisodes);
	learnData = l_learnData;

	oldWeights = new RealType[learnData->getNumWeights()];
}

PolicyEvaluationGradientFunction::~PolicyEvaluationGradientFunction() {
	delete[] oldWeights;
}

void PolicyEvaluationGradientFunction::resetLearnData() {
	if(resetData) {
		learnData->resetLearnData();
	}
}

OnlinePolicyEvaluation::OnlinePolicyEvaluation(
	AgentSimulator* l_agentSimulator, SemiMDPListener *l_learner,
    GradientUpdateFunction *learnData, int l_maxEvaluationEpisodes,
    int l_numSteps, int l_checkWeightsPerEpisode) :
	PolicyEvaluationGradientFunction(learnData, 0.01, l_maxEvaluationEpisodes) {
	resetData = false;

	addParameter("PolicyEvaluationCheckWeights", l_checkWeightsPerEpisode);
	addParameter("PolicyEvaluationMaxSteps", l_numSteps);

	learner = l_learner;
	agentSimulator = l_agentSimulator;
	semiMDPSender = agentSimulator->getAgent().get();

	addParameters(learner);
}

OnlinePolicyEvaluation::~OnlinePolicyEvaluation() {
}

void OnlinePolicyEvaluation::setSemiMDPSender(SemiMDPSender *sender) {
	semiMDPSender = sender;
}

void OnlinePolicyEvaluation::evaluatePolicy(int maxEvaluationEpisodes) {
	resetLearnData();
	semiMDPSender->addSemiMDPListener(learner);
	learnData->getWeights(oldWeights);

	int checkWeightsEpisode = (int) getParameter(
	    "PolicyEvaluationCheckWeights");
	int numSteps = (int) getParameter("PolicyEvaluationMaxSteps");

	RealType treshold = getParameter("PolicyEvaluationThreshold");

	for(int i = 0; i < maxEvaluationEpisodes; i++) {
		agentSimulator->doControllerEpisode(numSteps);

		if((i + 1) % checkWeightsEpisode == 0) {
			RealType difference = getWeightDifference(oldWeights);
			if(difference < treshold) {
				printf(
				    "PE: FINISHED Update Difference in Weight Vector after %d Episodes: %f\n",
				    i, difference);
//				updatePolicy();
				break;
			}
			printf(
			    "PE: Update Difference in Weight Vector after %d  Episode: %f\n",
			    i + 1, difference);

			learnData->getWeights(oldWeights);

		}
	}

	semiMDPSender->removeSemiMDPListener(learner);
}

LSTDOnlinePolicyEvaluation::LSTDOnlinePolicyEvaluation(
	AgentSimulator* agentSimulator, LSTDLambda *learner, GradientUpdateFunction *learnData,
    int maxEvaluationSteps, int numSteps) :
	    OnlinePolicyEvaluation(agentSimulator, learner, learnData, maxEvaluationSteps,
	        numSteps, maxEvaluationSteps / 5) {
	lstdLearner = learner;

	resetData = true;
}

LSTDOnlinePolicyEvaluation::~LSTDOnlinePolicyEvaluation() {
}

void LSTDOnlinePolicyEvaluation::resetLearnData() {
	OnlinePolicyEvaluation::resetLearnData();
	if(resetData) {
		lstdLearner->resetData();
	}
}

RealType LSTDOnlinePolicyEvaluation::getWeightDifference(RealType *oldWeights) {
	lstdLearner->doOptimization();
	return OnlinePolicyEvaluation::getWeightDifference(oldWeights);
}

OfflineEpisodePolicyEvaluation::OfflineEpisodePolicyEvaluation(
    EpisodeHistory *l_episodeHistory, SemiMDPRewardListener *l_learner,
    GradientUpdateFunction *learnData, std::list<StateModifier *> *l_modifiers,
    int l_maxEvaluationEpisodes) :
	PolicyEvaluationGradientFunction(learnData, 0.01, l_maxEvaluationEpisodes) {
	resetData = false;
	learner = l_learner;

	episodeHistory = l_episodeHistory;
	rewardLogger = nullptr;

	modifiers = l_modifiers;

	addParameters(learner);

	policy = nullptr;
}

OfflineEpisodePolicyEvaluation::OfflineEpisodePolicyEvaluation(
    EpisodeHistory *l_episodeHistory, RewardHistory *l_rewardLogger,
    SemiMDPRewardListener *l_learner, GradientUpdateFunction *learnData,
    std::list<StateModifier *> *l_modifiers, int l_maxEvaluationEpisodes) :
	PolicyEvaluationGradientFunction(learnData, 0.01, l_maxEvaluationEpisodes) {
	resetData = false;
	learner = l_learner;

	episodeHistory = l_episodeHistory;
	rewardLogger = l_rewardLogger;

	modifiers = l_modifiers;

	addParameters(learner);

	policy = nullptr;
}

OfflineEpisodePolicyEvaluation::~OfflineEpisodePolicyEvaluation() {

}

void OfflineEpisodePolicyEvaluation::setBatchLearningPolicy(
    BatchLearningPolicy *l_policy) {
	policy = l_policy;
}

void OfflineEpisodePolicyEvaluation::evaluatePolicy(int maxEvaluationEpisodes) {
	ActionSet *actions = episodeHistory->getActions();
	Step *step = new Step(episodeHistory->getStateProperties(),
	    episodeHistory->getModifierLists(), actions);
	ActionDataSet *dataSet = new ActionDataSet(actions);
	ActionDataSet *nextDataSet = new ActionDataSet(actions);

	resetLearnData();

	learner->newEpisode();
	startNewEpisode();

	RealType treshold = getParameter("PolicyEvaluationThreshold");

	static int count = episodeHistory->getNumEpisodes() - 1;

	printf(
	    "Offline Policy Evaluation, Episodes %d (%d), beginning with Episode %d\n",
	    episodeHistory->getNumEpisodes(), rewardLogger->getNumEpisodes(),
	    count);

	printf(
	    "OfflineEvaluation: Episode has %d (%d), State Modifiers, Episodes: %d\n",
	    episodeHistory->getModifierLists().size(),
	    step->oldState->getModifierLists().size(), maxEvaluationEpisodes);

	int resetCount = 0;

	for(int j = 0; j < maxEvaluationEpisodes; j++) {
		learnData->getWeights(oldWeights);

		if(count < 0 || count >= episodeHistory->getNumEpisodes()) {
			count = episodeHistory->getNumEpisodes() - 1;
			resetCount++;

			if(resetCount > 10) {
				break;
			}
		}

		if(count < 0) {
			break;
		}

		Episode *episode = episodeHistory->getEpisode(count);

		RewardEpisode *rewardEpisode = nullptr;
		if(rewardLogger) {
			rewardEpisode = rewardLogger->getEpisode(count);
		}

		learner->newEpisode();
		startNewEpisode();

		for(int i = 0; i < episode->getNumSteps(); i++) {
			episode->getStep(i, step);

			ActionData *data = step->action->getActionData();

			if(policy != nullptr) {
				Action *nextAction;
				if(i < episode->getNumSteps() - 1) {
					nextAction = episode->getAction(i + 1, nextDataSet);
				} else {
					nextAction = nullptr;
				}
				policy->setAction(nextAction,
				    nextDataSet->getActionData(nextAction));
			}

			if(data != nullptr) {
				data->setData(step->actionData->getActionData(step->action));
			}
			if(rewardLogger) {
				RealType reward = rewardEpisode->getReward(i);

				if(std::isnan(reward)) {
					printf("Reward is nan !! %d %d %d %d %d\n", count, i,
					    episode->getNumSteps(), rewardEpisode->getNumRewards(),
					    rewardLogger->getNumEpisodes());
					reward = rewardEpisode->getReward(i);
					assert(false);
				}

				learner->nextStep(step->oldState, step->action,
				    rewardEpisode->getReward(i), step->newState);
			} else {
				learner->nextStep(step->oldState, step->action, step->newState);
			}
			sendNextStep(step->oldState, step->action, step->newState);

		}

		count--;

		if(count % 100 == 0) {
			RealType difference = getWeightDifference(oldWeights);

			if(difference < treshold) {
				printf(
				    "PE: FINISHED Update Difference in Weight Vector after %d Episodes: %f\n",
				    count, difference);
				break;
			}
			printf(
			    "PE: Update Difference in Weight Vector after %d Episode: %f\n",
			    count, difference);
		}
	}

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		(*it)->getActionData()->setData(dataSet->getActionData(*it));
	}
	delete step;
	delete dataSet;
	delete nextDataSet;
}

LSTDOfflineEpisodePolicyEvaluation::LSTDOfflineEpisodePolicyEvaluation(
    EpisodeHistory *episodeHistory, RewardHistory *rewardLogger,
    LSTDLambda *learner, GradientVFunction *learnData,
    std::list<StateModifier *> *l_modifiers, int episodes) :
	    OfflineEpisodePolicyEvaluation(episodeHistory, rewardLogger, learner,
	        learnData, l_modifiers, episodes) {
	lstdLearner = learner;
}

LSTDOfflineEpisodePolicyEvaluation::~LSTDOfflineEpisodePolicyEvaluation() {
}

void LSTDOfflineEpisodePolicyEvaluation::resetLearnData() {
	OfflineEpisodePolicyEvaluation::resetLearnData();
	if(resetData) {
		lstdLearner->resetData();
	}
}

RealType LSTDOfflineEpisodePolicyEvaluation::getWeightDifference(
    RealType *oldWeights) {
	lstdLearner->doOptimization();
	return OfflineEpisodePolicyEvaluation::getWeightDifference(oldWeights);
}

DataCollector::DataCollector() {
}

DataCollector::~DataCollector() {
}

DataCollectorFromAgentLogger::DataCollectorFromAgentLogger(
	AgentSimulator *l_agentSimulator, AgentLogger *l_logger, RewardLogger *l_rewardLogger,
    int numEpisodes, int numSteps) {
	logger = l_logger;
	agentSimulator = l_agentSimulator;
	sender = agentSimulator->getAgent().get();

	rewardLogger = l_rewardLogger;

	addParameter("DataCollectorNumEpisodes", numEpisodes);
	addParameter("DataCollectorNumCollections", 1);

	addParameter("DataCollectorNumSteps", numSteps);
	addParameter("DataCollectorResetEpisodes", 0.0);

	numCollections = 0;

	unknownDataQFunctions = new std::list<UnknownDataQFunction *>;

}

DataCollectorFromAgentLogger::~DataCollectorFromAgentLogger() {
	delete unknownDataQFunctions;
}

void DataCollectorFromAgentLogger::setController(
    AgentController *l_controller) {
	controller = l_controller;
}

void DataCollectorFromAgentLogger::addUnknownDataFunction(
    UnknownDataQFunction *unknownDataQFunction) {
	unknownDataQFunctions->push_back(unknownDataQFunction);
}

void DataCollectorFromAgentLogger::collectData() {
	bool bReset = getParameter("DataCollectorResetEpisodes") > 0.5;
	int numEpisodes = (int) getParameter("DataCollectorNumEpisodes");
	int numSteps = (int) getParameter("DataCollectorNumSteps");
	int numColl = (int) getParameter("DataCollectorNumCollections");

	numCollections++;

	if(numCollections % numColl == 0) {
		if(bReset) {
			logger->resetData();
			rewardLogger->resetData();
		}

		sender->addSemiMDPListener(logger);
		sender->addSemiMDPListener(rewardLogger);

		AgentController *tempController = sender->getController();

		if(controller) {
			sender->setController(controller);
		}

		for(int i = 0; i < numEpisodes; i++) {
			agentSimulator->startNewEpisode();
			agentSimulator->doControllerEpisode(numSteps);
		}
		agentSimulator->startNewEpisode();
		sender->removeSemiMDPListener(logger);
		sender->removeSemiMDPListener(rewardLogger);

		sender->setController(tempController);

		std::list<UnknownDataQFunction *>::iterator it =
		    unknownDataQFunctions->begin();

		for(; it != unknownDataQFunctions->end(); it++) {
			(*it)->recalculateTrees();
		}
	}

}

void DataCollectorFromAgentLogger::setSemiMDPSender(
    SemiMarkovDecisionProcess *l_sender) {
	sender = l_sender;
}

PolicyIteration::PolicyIteration(
    LearnDataObject *l_policyFunction, LearnDataObject *l_evaluationFunction,
    PolicyEvaluation *l_evaluation, DataCollector *l_collector) {
	policyFunction = l_policyFunction;
	evaluationFunction = l_evaluationFunction;

	evaluation = l_evaluation;
	collector = l_collector;

	addParameters(evaluation);
	addParameters(collector);

	addParameter("PolicyIterationInitSteps", 0.0);
}

PolicyIteration::~PolicyIteration() {
}

void PolicyIteration::doPolicyIterationStep() {
	evaluation->evaluatePolicy();

	evaluationFunction->copy(policyFunction);

	if(collector) {
		collector->collectData();
	}

}

void PolicyIteration::initPolicyIteration() {

	evaluation->resetLearnData();
	int numInitSteps = (int) getParameter("PolicyIterationInitSteps");

	printf("Initializing Policy Iteration\n");
	evaluation->evaluatePolicy(numInitSteps);
}

PolicyIterationNewFeatures::PolicyIterationNewFeatures(
    LearnDataObject *policyFunction, LearnDataObject *evaluationFunction,
    PolicyEvaluation *evaluation,
    NewFeatureCalculatorDataGenerator *l_newFeatureCalculator,
    DataCollector *collector) :
	PolicyIteration(policyFunction, evaluationFunction, evaluation, collector) {
	newFeatureCalculator = l_newFeatureCalculator;
}

PolicyIterationNewFeatures::~PolicyIterationNewFeatures() {
}

void PolicyIterationNewFeatures::doPolicyIterationStep() {
	newFeatureCalculator->calculateNewFeatures();

	PolicyIteration::doPolicyIterationStep();
}

void PolicyIterationNewFeatures::initPolicyIteration() {
	newFeatureCalculator->calculateNewFeatures();

	PolicyIteration::doPolicyIterationStep();
}

void BatchDataGenerator::generateInputData(EpisodeHistory *episodeHistory) {
	ActionSet *actions = episodeHistory->getActions();
	Step *step = new Step(episodeHistory->getStateProperties(),
	    episodeHistory->getModifierLists(), episodeHistory->getActions());

	ActionDataSet *dataSet = new ActionDataSet(actions);

	for(int j = 0; j < episodeHistory->getNumEpisodes(); j++) {
		Episode *episode = episodeHistory->getEpisode(j);

		for(int i = 0; i < episode->getNumSteps(); i++) {
			episode->getStep(i, step);

			addInput(step->oldState, step->action, 0.0);
		}
	}

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		(*it)->getActionData()->setData(dataSet->getActionData(*it));
	}
	delete step;
	delete dataSet;
}

BatchVDataGenerator::BatchVDataGenerator(
    SupervisedLearner *l_learner, int numDim) {

	learner = l_learner;
	vFunction = nullptr;

	addParameters(learner);

	weightedLearner = nullptr;

	init(numDim);
}

BatchVDataGenerator::BatchVDataGenerator(
    AbstractVFunction *l_vFunction, SupervisedLearner *l_learner) {
	vFunction = l_vFunction;
	learner = l_learner;
	weightedLearner = nullptr;

	int numDim = vFunction->getStateProperties()->getNumContinuousStates()
	    + vFunction->getStateProperties()->getNumDiscreteStates();

	addParameters(learner);

	init(numDim);
}

BatchVDataGenerator::BatchVDataGenerator(
    AbstractVFunction *l_vFunction, SupervisedWeightedLearner *l_learner) {
	vFunction = l_vFunction;
	learner = nullptr;
	weightedLearner = l_learner;

	int numDim = vFunction->getStateProperties()->getNumContinuousStates()
	    + vFunction->getStateProperties()->getNumDiscreteStates();

	addParameters(weightedLearner);

	init(numDim);
}

void BatchVDataGenerator::init(int numDim) {
	inputData = new DataSet(numDim);
	outputData = new DataSet1D();
	weightingData = new DataSet1D();

	buffVector = new ColumnVector(numDim);
}

BatchVDataGenerator::~BatchVDataGenerator() {
	delete inputData;
	delete outputData;
	delete weightingData;

	delete buffVector;
}

void BatchVDataGenerator::addInput(
    StateCollection *stateCol, Action *, RealType output, RealType weighting) {
	if(weighting > 0.001) {
		StateProperties *properties = vFunction->getStateProperties();
		State *state = stateCol->getState(properties);

		int offset = 0;

		for(unsigned int i = 0; i < properties->getNumDiscreteStates(); i++) {
			buffVector->operator[](i + offset) = state->getDiscreteState(i);
		}

		offset += properties->getNumDiscreteStates();
		for(unsigned int i = 0; i < properties->getNumContinuousStates(); i++) {
			buffVector->operator[](i + offset) = state->getContinuousState(i);
		}

		offset += properties->getNumContinuousStates();

		inputData->addInput(buffVector);
		outputData->push_back(output);
		weightingData->push_back(weighting);

	} else {
	}
}

void BatchVDataGenerator::trainFA() {
	if(learner) {
		learner->learnFA(inputData, outputData);
	} else {
		weightedLearner->learnWeightedFA(inputData, outputData, weightingData);
	}
}

void BatchVDataGenerator::resetPolicyEvaluation() {
	inputData->clear();
	outputData->clear();
	weightingData->clear();
	if(learner) {
		learner->resetLearner();
	}
	if(weightedLearner) {
		weightedLearner->resetLearner();
	}
}

RealType BatchVDataGenerator::getValue(StateCollection *state, Action *) {
	return vFunction->getValue(state);
}

DataSet *BatchVDataGenerator::getInputData() {
	return inputData;
}

DataSet1D *BatchVDataGenerator::getOutputData() {
	return outputData;
}

DataSet1D *BatchVDataGenerator::getWeighting() {
	return weightingData;
}

BatchCAQDataGenerator::BatchCAQDataGenerator(
    StateProperties *l_properties, ContinuousActionQFunction *l_qFunction,
    SupervisedLearner *learner) :
	    BatchVDataGenerator(learner,
	        properties->getNumContinuousStates()
	            + l_qFunction->getContinuousActionObject()->getNumDimensions()
	            + properties->getNumDiscreteStates()) {
	qFunction = l_qFunction;
	properties = l_properties;

}

BatchCAQDataGenerator::~BatchCAQDataGenerator() {

}

void BatchCAQDataGenerator::addInput(
    StateCollection *stateCol, Action *action, RealType output) {
	ContinuousActionData *data =
	    dynamic_cast<ContinuousAction *>(action)->getContinuousActionData();

	int offset = 0;
	State *state = stateCol->getState(properties);
	for(unsigned int i = 0; i < properties->getNumDiscreteStates(); i++) {
		buffVector->operator[](i + offset) = state->getDiscreteState(i);
	}

	offset += properties->getNumDiscreteStates();
	for(unsigned int i = 0; i < properties->getNumContinuousStates(); i++) {
		buffVector->operator[](i + offset) = state->getContinuousState(i);
	}

	offset += properties->getNumContinuousStates();

	for(int i = 0; i < data->rows(); i++) {
		buffVector->operator[](i + offset) = data->operator[](i);
	}

	inputData->addInput(buffVector);
	outputData->push_back(output);
}

RealType BatchCAQDataGenerator::getValue(StateCollection *state, Action *action) {
	return qFunction->getValue(state, action);
}

BatchQDataGenerator::BatchQDataGenerator(
    QFunction *l_qFunction, SupervisedQFunctionLearner *l_learner,
    StateProperties *inputState) {
	init(l_qFunction, nullptr, inputState);

	qFunction = l_qFunction;
	learner = l_learner;

	if(learner) {
		addParameters(learner);
	}
	weightedLearner = nullptr;
}

BatchQDataGenerator::BatchQDataGenerator(
    QFunction *l_qFunction, SupervisedQFunctionWeightedLearner *l_learner,
    StateProperties *inputState) {
	init(l_qFunction, nullptr, inputState);

	qFunction = l_qFunction;
	weightedLearner = l_learner;

	if(weightedLearner) {
		addParameters(weightedLearner);
	}
	learner = nullptr;
}

BatchQDataGenerator::BatchQDataGenerator(
    ActionSet *l_actions, StateProperties *l_properties) {
	init(nullptr, l_actions, l_properties);

	learner = nullptr;
	weightedLearner = nullptr;
	qFunction = nullptr;
}

void BatchQDataGenerator::init(
    QFunction *l_qFunction, ActionSet *l_actions,
    StateProperties *l_properties) {
	inputMap = new std::map<Action *, DataSet *>;
	outputMap = new std::map<Action *, DataSet1D *>;

	weightedMap = new std::map<Action *, DataSet1D *>;
	buffVectorMap = new std::map<Action *, ColumnVector *>;

	properties = l_properties;

	actions = l_actions;
	qFunction = l_qFunction;

	if(actions == nullptr) {
		actions = l_qFunction->getActions();
	}

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		int dim = 0;
		if(properties == nullptr) {
			StateProperties *properties =
			    qFunction->getVFunction(*it)->getStateProperties();
			dim = properties->getNumContinuousStates()
			    + properties->getNumDiscreteStates();
		} else {
			dim = properties->getNumContinuousStates()
			    + properties->getNumDiscreteStates();
		}
		(*inputMap)[(*it)] = new DataSet(dim);
		(*outputMap)[(*it)] = new DataSet1D();
		(*weightedMap)[*it] = new DataSet1D();
		(*buffVectorMap)[(*it)] = new ColumnVector(dim);
	}

}

BatchQDataGenerator::~BatchQDataGenerator() {
	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		delete (*inputMap)[(*it)];
		delete (*outputMap)[(*it)];
		delete (*weightedMap)[*it];
		delete (*buffVectorMap)[(*it)];

	}

	delete inputMap;
	delete outputMap;
	delete weightedMap;
	delete buffVectorMap;
}

void BatchQDataGenerator::addInput(
    StateCollection *stateCol, Action *action, RealType output,
    RealType weighting) {
	if(weighting > 0.001) {

		StateProperties *inputState = properties;
		if(properties == nullptr) {
			AbstractVFunction *vFunction = qFunction->getVFunction(action);
			inputState = vFunction->getStateProperties();
		}

		State *state = stateCol->getState(inputState);

		int offset = 0;

		ColumnVector *buffVector = (*buffVectorMap)[action];

		for(unsigned int i = 0; i < inputState->getNumDiscreteStates(); i++) {
			buffVector->operator[](i + offset) = state->getDiscreteState(i);
		}

		offset += inputState->getNumDiscreteStates();
		for(unsigned int i = 0; i < inputState->getNumContinuousStates(); i++) {
			buffVector->operator[](i + offset) = state->getContinuousState(i);
		}

		offset += inputState->getNumContinuousStates();

		DataSet *inputData = (*inputMap)[action];
		DataSet1D *outputData = (*outputMap)[action];
		DataSet1D *weightData = (*weightedMap)[action];

		inputData->addInput(buffVector);
		outputData->push_back(output);
		weightData->push_back(weighting);

		//printf("Added Input to action ");
	}
}

void BatchQDataGenerator::trainFA() {
	ActionSet::iterator it = actions->begin();

	for(int i = 0; it != actions->end(); it++, i++) {
		printf("Action %d : %d Examples...", i, (*inputMap)[*it]->size());
		if(learner) {
			learner->learnQFunction(*it, (*inputMap)[*it], (*outputMap)[*it]);
		}
		if(weightedLearner) {
			weightedLearner->learnQFunction(*it, (*inputMap)[*it],
			    (*outputMap)[*it], (*weightedMap)[*it]);

		}

	}
}

StateProperties *BatchQDataGenerator::getStateProperties(Action *action) {
	if(properties == nullptr) {
		AbstractVFunction *vFunction = qFunction->getVFunction(action);
		properties = vFunction->getStateProperties();
		return properties;
	} else {
		return properties;
	}
}

void BatchQDataGenerator::resetPolicyEvaluation() {
//
	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		(*inputMap)[(*it)]->clear();
		(*outputMap)[(*it)]->clear();
		(*weightedMap)[(*it)]->clear();
	}
	if(learner) {
		learner->resetLearner();
	}
	if(weightedLearner) {
		weightedLearner->resetLearner();
	}
}

RealType BatchQDataGenerator::getValue(StateCollection *state, Action *action) {
	return qFunction->getValue(state, action);
}

DataSet *BatchQDataGenerator::getInputData(Action *action) {
	return (*inputMap)[action];
}

DataSet1D *BatchQDataGenerator::getOutputData(Action *action) {
	return (*outputMap)[action];
}

FittedIteration::FittedIteration(
    EpisodeHistory *l_episodeHistory, RewardHistory *l_rewardLogger,
    BatchDataGenerator *l_generator) :
	PolicyEvaluation() {
	rewardLogger = l_rewardLogger;
	episodeHistory = l_episodeHistory;

	estimationPolicy = nullptr;

	addParameter("DiscountFactor", 0.95);
	addParameter("ResidualNearestNeighborDistance", 0.0);
	addParameter("UseResidualAlgorithm", 0.0);

	dataCollector = nullptr;
	dataGenerator = l_generator;

	addParameters(dataGenerator);

	useResidualAlgorithm = 0;
	initialPolicyEvaluation = nullptr;

	actorLearner = nullptr;
}

FittedIteration::~FittedIteration() {
}

void FittedIteration::setDataCollector(DataCollector *l_dataCollector) {
	dataCollector = l_dataCollector;
	addParameters(dataCollector);
}

void FittedIteration::setActorLearner(PolicyEvaluation *l_actorLearner) {
	actorLearner = l_actorLearner;
}

void FittedIteration::evaluatePolicy() {
	PolicyEvaluation::evaluatePolicy();

	if(actorLearner) {
		actorLearner->evaluatePolicy();
	}
}

void FittedIteration::evaluatePolicy(int trials) {
	for(int i = 0; i < trials; i++) {
		doEvaluationTrial();
	}
}

void FittedIteration::addResidualInput(
    Step *, Action *, RealType, RealType, RealType, Action *, RealType) {
}

BatchDataGenerator *FittedIteration::createTrainingsData() {
	ActionSet *actions = episodeHistory->getActions();
	Step *step = new Step(episodeHistory->getStateProperties(),
	    episodeHistory->getModifierLists(), episodeHistory->getActions());

	printf("FittetIteration: Episode has %d State Modifiers\n",
	    episodeHistory->getModifierLists().size());

	ActionDataSet *dataSet = new ActionDataSet(actions);
	ActionDataSet *nextDataSet = new ActionDataSet(actions);

	printf("Tree Regression Value Calculation, Episodes %d\n",
	    episodeHistory->getNumEpisodes());

	int steps = 0;

	RealType discountFactor = getParameter("DiscountFactor");
	RealType nearestNeighborDistance = getParameter(
	    "ResidualNearestNeighborDistance");

	dataGenerator->resetPolicyEvaluation();

	for(int j = 0; j < episodeHistory->getNumEpisodes(); j++) {
		Episode *episode = episodeHistory->getEpisode(j);

		RewardEpisode *rewardEpisode = nullptr;
		if(rewardLogger) {
			rewardEpisode = rewardLogger->getEpisode(j);
		}

		for(int i = 0; i < episode->getNumSteps(); i++) {
			steps++;

			episode->getStep(i, step);

			RealType reward = 0;
			reward = rewardEpisode->getReward(i);

			RealType newV = 0;

			Action *nextAction = nullptr;

			Action *nextEpisodeAction = nullptr;
			RealType nextReward = 0.0;

			if(!step->newState->isResetState()) {

				if(i < episode->getNumSteps() - 1) {
					nextEpisodeAction = episode->getAction(i + 1, nextDataSet);
					nextReward = rewardEpisode->getReward(i + 1);
				}
				if(estimationPolicy) {
					nextAction = estimationPolicy->getNextAction(step->newState,
					    nextDataSet);
				} else {
					nextAction = nextEpisodeAction;
				}

				if(nextAction != nullptr) {
					ActionData *data = nextAction->getActionData();

					if(data != nullptr) {
						data->setData(nextDataSet->getActionData(nextAction));
					}
				}

				newV = getValue(step->newState, nextAction);
			}
			RealType V = reward + discountFactor * newV;
			if(nextAction == nullptr) {
				addResidualInput(step, nextAction, V, newV,
				    nearestNeighborDistance, nextEpisodeAction, reward);
			} else {
				addResidualInput(step, nextAction, V, newV,
				    nearestNeighborDistance, nextEpisodeAction, nextReward);

			}

			RealType weighting = getWeighting(step->oldState, step->action);

			dataGenerator->addInput(step->oldState, step->action, V, weighting);

		}
	}
	printf("Finished Creating Training-set\n");

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		(*it)->getActionData()->setData(dataSet->getActionData(*it));
	}
	delete step;
	delete dataSet;
	delete nextDataSet;

	return dataGenerator;
}

void FittedIteration::onParametersChanged() {
	PolicyEvaluation::onParametersChanged();

	useResidualAlgorithm = (int) getParameter("UseResidualAlgorithm");
}

RealType FittedIteration::getWeighting(StateCollection *, Action *) {
	return 1.0;
}

RealType FittedIteration::getValue(StateCollection *state, Action *action) {
	return dataGenerator->getValue(state, action);
}

void FittedIteration::doEvaluationTrial() {
	createTrainingsData();

	dataGenerator->trainFA();

	if(dataCollector != nullptr) {
		dataCollector->collectData();
	}
}

void FittedIteration::setInitialPolicyEvaluation(
    PolicyEvaluation *l_initialPolicyEvaluation) {
	initialPolicyEvaluation = l_initialPolicyEvaluation;
}

void FittedIteration::resetLearnData() {
	if(initialPolicyEvaluation) {
		initialPolicyEvaluation->evaluatePolicy();
	}

	if(actorLearner) {
		actorLearner->resetLearnData();
	}
}

FittedCAQIteration::FittedCAQIteration(
    ContinuousActionQFunction *l_qFunction, StateProperties *l_properties,
    AgentController *l_estimationPolicy, EpisodeHistory *episodeHistory,
    RewardHistory *rewardLogger, SupervisedLearner *l_learner) :
	    FittedIteration(episodeHistory, rewardLogger,
	        new BatchCAQDataGenerator(l_properties, l_qFunction, l_learner)) {
	estimationPolicy = l_estimationPolicy;
}

FittedCAQIteration::~FittedCAQIteration() {
	delete dataGenerator;
}

FittedVIteration::FittedVIteration(
    AbstractVFunction *l_vFunction, EpisodeHistory *episodeHistory,
    RewardHistory *rewardLogger, SupervisedLearner *l_learner) :
	    FittedIteration(episodeHistory, rewardLogger,
	        new BatchVDataGenerator(l_vFunction, l_learner)) {
	estimationPolicy = nullptr;

	actionProbabilities = nullptr;
	availableActions = nullptr;
}

FittedVIteration::FittedVIteration(
    AbstractVFunction *l_vFunction, EpisodeHistory *episodeHistory,
    RewardHistory *rewardLogger, SupervisedWeightedLearner *l_learner,
    StochasticPolicy *l_estimationPolicy) :
	    FittedIteration(episodeHistory, rewardLogger,
	        new BatchVDataGenerator(l_vFunction, l_learner)) {
	estimationPolicy = l_estimationPolicy;

	actionProbabilities = new RealType[estimationPolicy->getActions()->size()];
	availableActions = new ActionSet();
}

FittedVIteration::~FittedVIteration() {
	delete dataGenerator;

	if(actionProbabilities) {
		delete[] actionProbabilities;
		delete availableActions;
	}
}

RealType FittedVIteration::getWeighting(StateCollection *state, Action *action) {
	if(estimationPolicy == nullptr) {
		return 1.0;
	} else {
		availableActions->clear();

		estimationPolicy->getActions()->getAvailableActions(availableActions,
		    state);

		estimationPolicy->getActionProbabilities(state, availableActions,
		    actionProbabilities);

		int index = availableActions->getIndex(action);
		assert(index >= 0);

		return actionProbabilities[index];
	}
}

FittedQIteration::FittedQIteration(
    QFunction *l_qFunction, AgentController *l_estimationPolicy,
    EpisodeHistory *episodeHistory, RewardHistory *rewardLogger,
    SupervisedQFunctionLearner *l_learner,
    StateProperties *l_residualProperties) :
	    FittedIteration(episodeHistory, rewardLogger,
	        new BatchQDataGenerator(l_qFunction, l_learner)) {
	inputDatas = new std::map<Action *, DataSet *>;
	outputDatas = new std::map<Action *, DataSet1D *>;
	kdTrees = new std::map<Action *, KDTree *>;
	nearestNeighbors = new std::map<Action *, KNearestNeighbors *>;
	dataPreProc = new std::map<Action *, DataPreprocessor *>;
	neighborsList = new std::list<int>;

	estimationPolicy = l_estimationPolicy;
	residualProperties = l_residualProperties;

	StateProperties *l_prop = residualProperties;

	if(l_prop == nullptr) {
		l_prop = l_qFunction->getVFunction(0)->getStateProperties();
	}

	buffState = new State(l_prop);

	kNN = 1;
}

FittedQIteration::FittedQIteration(
    QFunction *l_qFunction, StateProperties *inputState,
    AgentController *l_estimationPolicy, EpisodeHistory *episodeHistory,
    RewardHistory *rewardLogger, SupervisedQFunctionLearner *l_learner,
    StateProperties *l_residualProperties) :
	    FittedIteration(episodeHistory, rewardLogger,
	        new BatchQDataGenerator(l_qFunction, l_learner, inputState)) {
	inputDatas = new std::map<Action *, DataSet *>;
	outputDatas = new std::map<Action *, DataSet1D *>;

	kdTrees = new std::map<Action *, KDTree *>;
	nearestNeighbors = new std::map<Action *, KNearestNeighbors *>;
	dataPreProc = new std::map<Action *, DataPreprocessor *>;
	neighborsList = new std::list<int>;
	residualProperties = l_residualProperties;

	estimationPolicy = l_estimationPolicy;

	StateProperties *l_prop = residualProperties;

	if(l_prop == nullptr) {
		l_prop = l_qFunction->getVFunction(0)->getStateProperties();
	}

	buffState = new State(l_prop);

	kNN = 1;
}

FittedQIteration::~FittedQIteration() {
	delete dataGenerator;
	ActionSet *actions = episodeHistory->getActions();

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		if((*inputDatas)[*it] != nullptr) {
			delete (*inputDatas)[*it];
			delete (*outputDatas)[*it];

			delete (*kdTrees)[*it];
			delete (*nearestNeighbors)[*it];
			delete (*dataPreProc)[*it];

		}
	}
	delete inputDatas;
	delete outputDatas;
	delete kdTrees;
	delete nearestNeighbors;
	delete dataPreProc;

	delete neighborsList;

	delete buffState;
}

void FittedQIteration::addResidualInput(
    Step *step, Action *nextAction, RealType, RealType newV,
    RealType nearestNeighborDistance, Action *, RealType) {

	if(useResidualAlgorithm > 0 && nextAction != nullptr
	    && (*nearestNeighbors)[nextAction] != nullptr) {
		StateProperties *l_prop = residualProperties;

		if(l_prop == nullptr) {
			BatchQDataGenerator *qGenerator =
			    dynamic_cast<BatchQDataGenerator *>(dataGenerator);

			l_prop = qGenerator->getStateProperties(nextAction);
		}

		State *state = step->newState->getState(l_prop);

		(*dataPreProc)[nextAction]->preprocessInput(state, buffState);

		neighborsList->clear();
		(*nearestNeighbors)[nextAction]->getNearestNeighbors(state,
		    neighborsList);

		ColumnVector *nearestNeighbor =
		    (*(*inputDatas)[nextAction])[*(neighborsList->begin())];

		RealType distance = buffState->getDistance(nearestNeighbor);

		if(distance > nearestNeighborDistance
		    && (useResidualAlgorithm == 1 || useResidualAlgorithm == 3)) {
			dataGenerator->addInput(step->newState, nextAction, newV);
		} else {
			if(nextAction == step->action && useResidualAlgorithm < 3) {
				State *oldState = step->oldState->getState(l_prop);

				(*dataPreProc)[nextAction]->preprocessInput(oldState,
				    buffState);
				RealType distanceOldState = buffState->getDistance(
				    nearestNeighbor);

				if(distanceOldState < distance + nearestNeighborDistance / 3) {
					dataGenerator->addInput(step->newState, nextAction, newV);
				}
			}
		}
	}
}

void FittedQIteration::doEvaluationTrial() {
	ActionSet *actions = episodeHistory->getActions();

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		if((*inputDatas)[*it] != nullptr) {
			delete (*inputDatas)[*it];
			delete (*outputDatas)[*it];
			delete (*kdTrees)[*it];
			delete (*nearestNeighbors)[*it];
			delete (*dataPreProc)[*it];
			(*inputDatas)[*it] = nullptr;
		}
		DataSet *dataSet =
		    dynamic_cast<BatchQDataGenerator *>(dataGenerator)->getInputData(
		        *it);
		DataSet1D *outputSet =
		    dynamic_cast<BatchQDataGenerator *>(dataGenerator)->getOutputData(
		        *it);

		if(dataSet != nullptr && dataSet->size() > 0) {
			(*inputDatas)[*it] = new DataSet(*dataSet);
			(*outputDatas)[*it] = new DataSet1D(*outputSet);

			(*dataPreProc)[*it] = new MeanStdPreprocessor(dataSet);
			(*dataPreProc)[*it]->preprocessDataSet((*inputDatas)[*it]);

			(*kdTrees)[*it] = new KDTree((*inputDatas)[*it], 1);
			(*kdTrees)[*it]->setPreprocessor((*dataPreProc)[*it]);
			(*nearestNeighbors)[*it] = new KNearestNeighbors((*kdTrees)[*it],
			    (*inputDatas)[*it], kNN);
		}
	}
	FittedIteration::doEvaluationTrial();
}

FittedQNewFeatureCalculator::FittedQNewFeatureCalculator(
    QFunction *l_qFunction, QFunction *l_qFunctionPolicy,
    StateProperties *inputState, AgentController *estimationPolicy,
    EpisodeHistory *episodeHistory, RewardHistory *rewardLogger,
    NewFeatureCalculator *l_newFeatCalc) :
	    FittedQIteration(l_qFunction, inputState, estimationPolicy,
	        episodeHistory, rewardLogger, nullptr) {
	qFunction = l_qFunction;
	qFunctionPolicy = l_qFunctionPolicy;

	estimationCalculator = new std::map<Action *, FeatureCalculator *>;
	policyCalculator = new std::map<Action *, FeatureCalculator *>;

	newFeatureCalc = l_newFeatCalc;
	agent = nullptr;
}

FittedQNewFeatureCalculator::~FittedQNewFeatureCalculator() {
	clearCalculators();

	delete estimationCalculator;
	delete policyCalculator;
}

void FittedQNewFeatureCalculator::clearCalculators() {

	ActionSet *actions = estimationPolicy->getActions();

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		FeatureVFunction *vFunction =
		    dynamic_cast<FeatureVFunction *>(qFunction->getVFunction(*it));
		vFunction->setFeatureCalculator(nullptr);

		vFunction =
		    dynamic_cast<FeatureVFunction *>(qFunctionPolicy->getVFunction(*it));
		vFunction->setFeatureCalculator(nullptr);

		if((*estimationCalculator)[*it]) {
			episodeHistory->removeStateModifier((*estimationCalculator)[*it]);

			if(agent) {
				agent->removeStateModifier((*estimationCalculator)[*it]);
			}

			delete (*estimationCalculator)[*it];
		}
		episodeHistory->removeStateModifier((*policyCalculator)[*it]);

		if(agent) {
			agent->removeStateModifier((*policyCalculator)[*it]);
		}

		delete (*policyCalculator)[*it];
	}
	estimationCalculator->clear();
	policyCalculator->clear();
}

void FittedQNewFeatureCalculator::resetData() {
	clearCalculators();
}

void FittedQNewFeatureCalculator::calculateNewFeatures() {
	createTrainingsData();

	ActionSet *actions = estimationPolicy->getActions();

	ActionSet::iterator it = actions->begin();

	BatchQDataGenerator *qDataGenerator =
	    dynamic_cast<BatchQDataGenerator *>(dataGenerator);

	for(; it != actions->end(); it++) {
		DataSet *dataSet = qDataGenerator->getInputData(*it);

		DataSet1D *outputDataSet = qDataGenerator->getOutputData(*it);

		FeatureVFunction *vFunction =
		    dynamic_cast<FeatureVFunction *>(qFunction->getVFunction(*it));

		(*estimationCalculator)[*it] = newFeatureCalc->getFeatureCalculator(
		    vFunction, dataSet, outputDataSet);

		vFunction->setFeatureCalculator((*estimationCalculator)[*it]);

		episodeHistory->addStateModifier((*estimationCalculator)[*it]);

		if(agent) {
			agent->addStateModifier((*estimationCalculator)[*it]);
		}
	}
	printf("Episode has %d State Modifiers\n",
	    episodeHistory->getModifierLists().size());
}

void FittedQNewFeatureCalculator::swapValueFunctions() {

	ActionSet *actions = estimationPolicy->getActions();

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		printf("Swap V-Func: %d Modifier\n",
		    episodeHistory->getModifierLists().size());
		if((*policyCalculator)[*it] != nullptr) {
			printf("Removing State Modifier\n");
			episodeHistory->removeStateModifier((*policyCalculator)[*it]);

			if(agent) {
				agent->removeStateModifier((*policyCalculator)[*it]);
			}
		}
		printf("Swap V-Func 2: %d Modifier\n",
		    episodeHistory->getModifierLists().size());

		(*policyCalculator)[*it] = (*estimationCalculator)[*it];

		FeatureVFunction *vFunction =
		    dynamic_cast<FeatureVFunction *>(qFunctionPolicy->getVFunction(*it));

		vFunction->setFeatureCalculator((*policyCalculator)[*it]);

		(*estimationCalculator)[*it] = nullptr;
	}
}

void FittedQNewFeatureCalculator::setStateModifiersObject(
	Agent* l_agent) {
	agent = l_agent;
}

ContinuousDynamicProgramming::ContinuousDynamicProgramming(
    ActionSet *allActions, SamplingBasedTransitionModel *l_transModel) {
	transModel = l_transModel;

	availableActions = new ActionSet();
	actionValues = new RealType[allActions->size()];
	actionProbabilities = new RealType[allActions->size()];

	addParameter("DiscountFactor", 0.95);

	addParameters(transModel);

	numIteration = 0;
}

ContinuousDynamicProgramming::~ContinuousDynamicProgramming() {
	delete availableActions;
	delete[] actionValues;
	delete[] actionProbabilities;
}

void ContinuousDynamicProgramming::evaluatePolicy(int numEvaluations) {
	for(int k = 0; k < numEvaluations; k++) {
		numIteration++;
		RealType gamma = getParameter("DiscountFactor");

		printf("DynamicProgramming Evaluation %d (%d)\n", numIteration, k);
		resetDynamicProgramming();
		for(int i = 0; i < transModel->getNumStates(); i++) {
			availableActions->clear();

			std::map<Action *, CSampleTransition *> * transitions =
			    transModel->getTransitions(i);

			std::map<Action *, CSampleTransition *>::iterator it =
			    transitions->begin();

			for(int j = 0; it != transitions->end(); it++, j++) {
				availableActions->push_back((*it).first);

				CSampleTransition *sampleTrans = (*it).second;

				State *nextState = sampleTrans->state;

				RealType value = sampleTrans->reward;

				if(!nextState->isResetState()) {
					value += gamma
					    * getValue(nextState, sampleTrans->availableActions);
				}
				actionValues[j] = value;
			}
			updateOutputs(i, availableActions, actionValues);

			if(i % 5000 == 0) {
				printf("Finished State %d\n", i);
			}
		}
		learn();
	}
}

RealType ContinuousDynamicProgramming::getValueFromDistribution(
    ActionSet *availableActions, RealType *actionValues,
    ActionDistribution *distribution) {
	memcpy(actionProbabilities, actionValues,
	    sizeof(RealType) * availableActions->size());

	distribution->getDistribution(nullptr, availableActions,
	    actionProbabilities);

	RealType sum = 0;
	for(unsigned int i = 0; i < availableActions->size(); i++) {
		sum += actionValues[i] * actionProbabilities[i];
	}
	return sum;
}

void ContinuousDynamicProgramming::resetLearnData() {
	numIteration = 0;
	resetDynamicProgramming();
}

ContinuousDynamicVProgramming::ContinuousDynamicVProgramming(
    ActionSet *allActions, ActionDistribution *l_distribution,
    SamplingBasedTransitionModel *transModel, AbstractVFunction *l_vFunction,
    SupervisedLearner *l_learner) :
	ContinuousDynamicProgramming(allActions, transModel) {
	distribution = l_distribution;

	vFunction = l_vFunction;
	learner = l_learner;

	outputValues = new DataSet1D();

	addParameters(learner);
}

ContinuousDynamicVProgramming::~ContinuousDynamicVProgramming() {
	delete outputValues;
}

RealType ContinuousDynamicVProgramming::getValue(State *state, ActionSet *) {
	return vFunction->getValue(state);
}

void ContinuousDynamicVProgramming::updateOutputs(
    int, ActionSet *availableActions, RealType *actionValues) {
	RealType value = getValueFromDistribution(availableActions, actionValues,
	    distribution);

	outputValues->push_back(value);
}

void ContinuousDynamicVProgramming::learn() {
	learner->learnFA(transModel->getStateList(), outputValues);
}

void ContinuousDynamicVProgramming::resetDynamicProgramming() {
	outputValues->clear();
}

ContinuousDynamicQProgramming::ContinuousDynamicQProgramming(
    ActionSet *allActions, ActionDistribution *l_distribution,
    SamplingBasedTransitionModel *transModel, AbstractQFunction *l_qFunction,
    SupervisedQFunctionLearner *l_learner) :
	ContinuousDynamicProgramming(allActions, transModel) {
	qFunction = l_qFunction;
	learner = l_learner;

	actionValues2 = new RealType[allActions->size()];

	distribution = l_distribution;

	outputValues = new std::map<Action *, DataSet1D *>;

	inputValues = new std::map<Action *, DataSet *>;

	ActionSet::iterator it = allActions->begin();
	for(; it != allActions->end(); it++) {
		(*outputValues)[*it] = new DataSet1D();
		(*inputValues)[*it] = new DataSet(
		    transModel->getStateList()->getNumDimensions());
	}

	addParameters(learner);
}

ContinuousDynamicQProgramming::~ContinuousDynamicQProgramming() {
	std::map<Action *, DataSet1D *>::iterator it = outputValues->begin();
	for(; it != outputValues->end(); it++) {
		delete (*it).second;
		delete (*inputValues)[(*it).first];
	}

	delete outputValues;
	delete inputValues;

	delete[] actionValues2;
}

RealType ContinuousDynamicQProgramming::getValue(
    State *state, ActionSet *availableActions) {
	qFunction->getActionValues(state, availableActions, actionValues2);
	return getValueFromDistribution(availableActions, actionValues2,
	    distribution);
}

void ContinuousDynamicQProgramming::updateOutputs(
    int index, ActionSet *availableActions, RealType *actionValues) {
	ActionSet::iterator it = availableActions->begin();

	for(int i = 0; it != availableActions->end(); it++, i++) {
		(*outputValues)[*it]->push_back(actionValues[i]);
		(*inputValues)[*it]->addInput((*transModel->getStateList())[index]);
		;
	}
}

void ContinuousDynamicQProgramming::learn() {
	std::map<Action *, DataSet1D *>::iterator it = outputValues->begin();
	for(int i = 0; it != outputValues->end(); it++, i++) {
		printf("%d\n", i);
		learner->learnQFunction((*it).first, (*inputValues)[(*it).first],
		    (*outputValues)[(*it).first]);
	}
}

void ContinuousDynamicQProgramming::resetDynamicProgramming() {
	std::map<Action *, DataSet1D *>::iterator it = outputValues->begin();
	for(; it != outputValues->end(); it++) {
		(*inputValues)[(*it).first]->clear();
		(*outputValues)[(*it).first]->clear();
	}
}

ContinuousMCQEvaluation::ContinuousMCQEvaluation(
    ActionSet *allActions, ActionDistribution *distribution,
    SamplingBasedTransitionModel *transModel,
    PolicySameStateEvaluator *l_evaluator, SupervisedQFunctionLearner *learner) :
	    ContinuousDynamicQProgramming(allActions, distribution, transModel,
	        nullptr, learner) {
	evaluator = l_evaluator;
}

ContinuousMCQEvaluation::~ContinuousMCQEvaluation() {

}

RealType ContinuousMCQEvaluation::getValue(State *state, ActionSet *) {
	return evaluator->getValueForState(state, numIteration + 1);
}

GraphDynamicProgramming::GraphDynamicProgramming(
    SamplingBasedGraph *l_transModel) {
	transModel = l_transModel;

	outputValues = new DataSet1D();

	addParameter("InitNodeValue", -50);

	addParameters(transModel);

	resetGraph = true;
}

GraphDynamicProgramming::~GraphDynamicProgramming() {
	delete outputValues;
}

void GraphDynamicProgramming::evaluatePolicy(int numEvaluations) {
	RealType init = getParameter("InitNodeValue");

	if(numEvaluations > 0) {
		while(outputValues->size() < transModel->getNumStates()) {
			outputValues->push_back(init);
		}
	}

	for(int i = 0; i < numEvaluations; i++) {
		printf("Beginning %d Iteration: Output Mean %f\n", i,
		    outputValues->getMean(nullptr));
		for(int j = 0; j < transModel->getNumStates(); j++) {
//			RealType oldVal = (*outputValues)[j];
			RealType newVal = 0.0;

			getMaxTransition(j, newVal);

			(*outputValues)[j] = newVal;
		}
		printf("Finished %d Iteration: Output Mean %f\n", i,
		    outputValues->getMean(nullptr));
	}
}

void GraphDynamicProgramming::resetLearnData() {
	outputValues->clear();
	RealType init = getParameter("InitNodeValue");

	printf("Init Graph Iteration\n");

	if(resetGraph) {
		ContinuousStateList *stateList = transModel->getStateList();
		stateList->resetData();
		printf("Creating Graph...(%lu Nodes)\n",
		    transModel->getStateList()->size());
		transModel->createTransitions();
		printf("Done\n");
		transModel->getStateList()->initNearestNeighborSearch();
		transModel->getStateList()->getKDTree()->createLeavesArray();
	}

	for(int i = 0; i < transModel->getNumStates(); i++) {
		outputValues->push_back(init);
	}

}

RealType GraphDynamicProgramming::getValue(int node) {
	return (*outputValues)[node];
}

RealType GraphDynamicProgramming::getValue(ColumnVector *input) {
	int node = 0;
	RealType distance = 0.0;

	transModel->getStateList()->getNearestNeighbor(input, node, distance);

	return (*outputValues)[node];
}

void GraphDynamicProgramming::getNearestNode(
    ColumnVector *input, int &node, RealType &distance) {
	transModel->getStateList()->getNearestNeighbor(input, node, distance);
}

DataSet1D *GraphDynamicProgramming::getOutputValues() {
	return outputValues;
}

GraphTransition *GraphDynamicProgramming::getMaxTransition(
    int index, RealType &maxValue, KDRectangle *range) {

	std::list<GraphTransition *> *transitions = transModel->getTransitions(
	    index);
	std::list<GraphTransition *>::iterator it = transitions->begin();

	maxValue = 0;
	GraphTransition *edge = nullptr;

	for(int k = 0; it != transitions->end(); it++, k++) {
		ContinuousStateList *stateList = transModel->getStateList();

		RealType distance = 0.0;

		bool newPoint =
		    ((*it)->newStateIndex > 0)
		        && (fabs(
		            (*stateList)[index]->operator[](2)
		                - (*stateList)[(*it)->newStateIndex]->operator[](2))
		            > 0.001);

		if(range != nullptr && newPoint) {
			//printf("TargetRange: %f\n", range->getMinValue(3));

			//cout << (*stateList)[(*it)->newStateIndex]->t();

			distance = range->getDistanceToPoint(
			    (*stateList)[(*it)->newStateIndex]);
			//printf("Distance: %f\n", distance);
		}

		if(range == nullptr || distance <= 0.00001) {

			RealType tempVal = (*it)->getReward();

			if((*it)->newStateIndex >= 0) {
				tempVal += (*it)->discountFactor
				    * (*outputValues)[(*it)->newStateIndex];
			}

			if(k == 0 || tempVal > maxValue) {
				maxValue = tempVal;
				edge = *it;
			}
		}
	}
	if(transitions->size() == 0) {
		maxValue = (*outputValues)[index];
	}
	return edge;
}

SamplingBasedGraph *GraphDynamicProgramming::getGraph() {
	return transModel;
}

void GraphDynamicProgramming::saveCSV(
    std::string filename, DataSubset *nodeSubset
) {
	std::string nodeFilename = filename + "_Nodes.data";
	std::string edgeFilename = filename + "_Edges.data";


	std::ofstream nodeFILE(nodeFilename);
	std::ofstream edgeFILE(edgeFilename);

	DataSubset::iterator it = nodeSubset->begin();
	DataSet *dataSet = transModel->getStateList();

	std::map<int, int> indexMap;

	for(int j = 0; it != nodeSubset->end(); it++, j++) {
		indexMap[*it] = j;
	}

	it = nodeSubset->begin();
	for(int j = 0; it != nodeSubset->end(); it++, j++) {
		// Save node
		ColumnVector *node = (*dataSet)[*it];
		for(int i = 0; i < node->rows(); i++) {
			fmt::fprintf(nodeFILE, "%f ", node->operator[](i));
		}
		fmt::fprintf(nodeFILE, "%f\n", (*outputValues)[*it]);

		//Save edges

		std::list<GraphTransition *> *transitions = transModel->getTransitions(
		    *it);

		std::list<GraphTransition *>::iterator itTrans = transitions->begin();

		for(; itTrans != transitions->end(); itTrans++) {
			int newStateIndex = (*itTrans)->newStateIndex;
			if(newStateIndex >= 0) {
				newStateIndex = indexMap[newStateIndex];
			}
			if((*itTrans)->action->isType(AF_ContinuousAction)) {
				ContinuousActionData *data =
				    dynamic_cast<ContinuousActionData *>((*itTrans)->actionData);
				fmt::fprintf(edgeFILE, "%d %d %f %f %f\n", j, newStateIndex,
				    (*itTrans)->getReward(), (*itTrans)->discountFactor,
				    data->operator[](0));
			} else {
				fmt::fprintf(edgeFILE, "%d %d %f %f 0\n", j, newStateIndex,
				    (*itTrans)->getReward(), (*itTrans)->discountFactor);
			}
		}
	}
}

GraphAdaptiveTargetDynamicProgramming::GraphAdaptiveTargetDynamicProgramming(
    AdaptiveTargetGraph *graph) :
	GraphDynamicProgramming(graph) {
	adaptiveTargetGraph = graph;
	currentTarget = nullptr;

	targetMap = new std::map<GraphTarget *, DataSet1D *>();
	targets = new std::list<GraphTarget *>();

	delete outputValues;
	outputValues = nullptr;
}

GraphAdaptiveTargetDynamicProgramming::~GraphAdaptiveTargetDynamicProgramming() {
	delete targets;

	std::map<GraphTarget *, DataSet1D *>::iterator it = targetMap->begin();

	for(; it != targetMap->end(); it++) {
		delete (*it).second;
	}
	delete targetMap;
}

GraphTransition *GraphAdaptiveTargetDynamicProgramming::getMaxTransition(
    int index, RealType &maxValue, KDRectangle *range) {
	std::list<GraphTransition *> *transitions = transModel->getTransitions(
	    index);
	std::list<GraphTransition *>::iterator it = transitions->begin();

	maxValue = 0;
	GraphTransition *edge = nullptr;

	for(int k = 0; it != transitions->end(); it++, k++) {
		ContinuousStateList *stateList = transModel->getStateList();

		GraphTransitionAdaptiveTarget *adaptiveTrans =
		    dynamic_cast<GraphTransitionAdaptiveTarget *>(*it);

		RealType tempVal = adaptiveTrans->getReward(currentTarget);

		if((*it)->newStateIndex >= 0) {

			DataSet1D *targetNodeValues = nullptr;
			if(adaptiveTrans->isFinished(currentTarget)) {
				GraphTarget *nextTarget = currentTarget->getNextTarget();

				if(nextTarget != nullptr) {
					targetNodeValues = (*targetMap)[nextTarget];
				}
			} else {
				targetNodeValues = (*targetMap)[currentTarget];
			}

			if(targetNodeValues) {
				tempVal += (*it)->discountFactor
				    * (*targetNodeValues)[(*it)->newStateIndex];
			}
		}

		if(k == 0 || tempVal > maxValue) {
			maxValue = tempVal;
			edge = *it;
		}
	}

	if(transitions->size() == 0) {
		DataSet1D *targetNodeValues = (*targetMap)[currentTarget];
		maxValue = (*targetNodeValues)[index];
	}
	return edge;
}

void GraphAdaptiveTargetDynamicProgramming::addTarget(GraphTarget *target) {
	currentTarget = target;
	targets->push_back(target);

	outputValues = new DataSet1D();

	adaptiveTargetGraph->addTarget(target);

	(*targetMap)[target] = getOutputValues();
	evaluatePolicy(getParameter("PolicyEvaluationMaxEpisodes"));

//	outputValues = new DataSet1D();
}

GraphTarget * GraphAdaptiveTargetDynamicProgramming::getTargetForState(
    StateCollection *state) {
	std::list<GraphTarget *>::iterator it = targets->begin();
	for(; it != targets->end(); it++) {
		GraphTarget *target = *it;

		if(target->isTargetForState(state)) {
			return target;
		}
	}
	return nullptr;
}

void GraphAdaptiveTargetDynamicProgramming::setCurrentTarget(
    GraphTarget *target) {
	currentTarget = target;
	adaptiveTargetGraph->setCurrentTarget(target);

	outputValues = (*targetMap)[target];
}

int GraphAdaptiveTargetDynamicProgramming::getNumTargets() {
	return targets->size();
}

GraphTarget *GraphAdaptiveTargetDynamicProgramming::getTarget(int index) {
	std::list<GraphTarget *>::iterator it = targets->begin();

	for(int i = 0; it != targets->end() && i < index; i++, it++)
		;

	return (*it);
}

void GraphAdaptiveTargetDynamicProgramming::resetLearnData() {
	RealType init = getParameter("InitNodeValue");

	if(resetGraph) {
		printf("Init Graph Iteration\n");

		ContinuousStateList *stateList = transModel->getStateList();
		stateList->resetData();
		printf("Creating Graph...(%d Nodes)\n",
		    transModel->getStateList()->size());
		transModel->createTransitions();
		printf("Done\n");
		transModel->getStateList()->initNearestNeighborSearch();
		transModel->getStateList()->getKDTree()->createLeavesArray();
	}
}

} //namespace rlearner
