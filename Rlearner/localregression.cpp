// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "localregression.h"
#include "utility.h"
#include "trees.h"
#include "kdtrees.h"
#include "nearestneighbor.h"

namespace rlearner {

LocalRegression::LocalRegression(DataSet *l_input, DataSet1D *l_output, int l_K) :
	Mapping<RealType>(l_input->getNumDimensions()) {
	input = l_input;
	output = l_output;

	printf("Building Kd-Tree with %d inputs\n", input->size());
	kdTree = new KDTree(input, 1);

	printf("Tree: %d %d\n", kdTree->getDepth(), kdTree->getNumLeaves());

	nearestNeighbors = new KNearestNeighbors(kdTree, input, l_K);

	subsetList = new std::list<int>();
	subset = new DataSubset();

	K = l_K;

	if(buffVector->size() == 0) {
		printf("%d %d %d\n", buffVector->size(), l_input->getNumDimensions(),
		    l_input->size());
		assert(buffVector->size() > 0);
	}
}

LocalRegression::~LocalRegression() {
	delete kdTree;
	delete nearestNeighbors;

	delete subset;
	delete subsetList;
}

RealType LocalRegression::doGetOutputValue(ColumnVector *vector) {
	subset->clear();
	subsetList->clear();

	nearestNeighbors->getNearestNeighbors(vector, subsetList);

	subset->addElements(subsetList);

	return doRegression(vector, subset);
}

DataSubset *LocalRegression::getLastNearestNeighbors() {
	return subset;
}

int LocalRegression::getNumNearestNeighbors() {
	return K;
}

LinearRegression::LinearRegression(
    int degree, int numDataPoints, int numDimensions) :
	Mapping<RealType>(numDimensions) {
	init(degree, numDataPoints, numDimensions);
}

LinearRegression::LinearRegression(
    int degree, DataSet *dataSet, DataSet1D *outputValues, DataSubset *subset) :
	Mapping<RealType>(dataSet->getNumDimensions()) {
	if(subset) {
		init(degree, subset->size(), dataSet->getNumDimensions());
	} else {
		init(degree, dataSet->size(), dataSet->getNumDimensions());
	}
	calculateRegressionMatrix(dataSet, outputValues, subset);
}

void LinearRegression::init(
    int l_degree, int numDataPoints, int l_numDimensions) {
	degree = l_degree;
	numDimensions = l_numDimensions;

	switch(degree) {
	case 0: {
		xDim = 1;
		break;
	}
	case 1: {
		xDim = 1 + numDimensions;
		break;
	}
	case 2: {
		xDim = 1 + numDimensions * numDimensions;
		break;
	}
	case 3: {
		xDim = 1 + numDimensions * (numDimensions + 1);
		break;
	}
	};

	X = new Matrix(numDataPoints, xDim);
	X_pinv = new Matrix(xDim, numDataPoints);

	xVector = new ColumnVector(xDim);
	yVector = new ColumnVector(numDataPoints);

	w = new ColumnVector(xDim);

	lambda = 0.01;
}

LinearRegression::~LinearRegression() {
	delete X;
	delete xVector;
	delete yVector;

	delete w;
	delete X_pinv;
}

void LinearRegression::getXVector(ColumnVector *input, ColumnVector *xVector) {
	xVector->operator[](0) = 1;

	// Linear part
	if(degree >= 1) {
		for(int i = 0; i < numDimensions; i++) {
			xVector->operator[](i + 1) = input->operator[](i);
		}
	}

	// Quadratic Part
	if(degree >= 2) {
		int count = 0;

		for(int i = 0; i < numDimensions; i++) {
			for(int j = 0; j < numDimensions; j++) {
				if(degree > 2 || i != j) {
					xVector->operator[](1 + numDimensions + count) =
					    input->operator[](i) * input->operator[](j);
					count++;
				}
			}
		}
	}
}

void LinearRegression::calculateRegressionMatrix(
    DataSet *dataSet, DataSet1D *outputValues, DataSubset *subset) {
	if(subset) {
		assert((signed int ) subset->size() == X->rows());
		DataSubset::iterator it = subset->begin();
		for(int i = 0; it != subset->end(); it++, i++) {
			yVector->operator[](i) = (*outputValues)[*it];

			getXVector((*dataSet)[*it], xVector);

			for(int j = 0; j < xVector->size(); j++) {
				X->operator()(i, j) = xVector->operator[](j);
			}
		}
	} else {
		assert(dataSet->size() == (unsigned int ) X->rows());

		for(unsigned int i = 0; i < dataSet->size(); i++) {
			yVector->operator[](i) = (*outputValues)[i];

			getXVector((*dataSet)[i], xVector);

			for(int j = 0; j < xVector->size(); j++) {
				X->operator()(i, j) = xVector->operator[](j);
			}
		}

	}

	getPseudoInverse(X, X_pinv, lambda);
	*w = (*X_pinv) * (*yVector);
}

RealType LinearRegression::doGetOutputValue(ColumnVector *input) {
	getXVector(input, xVector);
	return w->dot(*xVector);
}

LocalLinearRegression::LocalLinearRegression(
    DataSet *input, DataSet1D *output, int K, int degree, RealType lambda) :
	LocalRegression(input, output, K) {
	regression = new LinearRegression(degree, K, input->getNumDimensions());

	regression->lambda = lambda;
}

LocalLinearRegression::~LocalLinearRegression() {
	delete regression;
}

RealType LocalLinearRegression::doRegression(
    ColumnVector *vector, DataSubset *subset) {
	regression->calculateRegressionMatrix(input, output, subset);

	return regression->getOutputValue(vector);
}

LocalRBFRegression::LocalRBFRegression(
    DataSet *input, DataSet1D *output, int K, ColumnVector *l_sigma) :
	LocalRegression(input, output, K) {
	rbfFactors = new ColumnVector(K);
	sigma = new ColumnVector(*l_sigma);
}

LocalRBFRegression::~LocalRBFRegression() {
	delete sigma;
	delete rbfFactors;
}

RealType LocalRBFRegression::doRegression(
    ColumnVector *vector, DataSubset *subset) {
	DataSubset::iterator it = subset->begin();

	ColumnVector *rbfFactors = getRBFFactors(vector, subset);
	it = subset->begin();
	RealType value = 0;
	for(int i = 0; it != subset->end(); it++, i++) {
		value += rbfFactors->operator[](i) * (*output)[*it];
	}
	RealType sum = rbfFactors->sum();
	if(sum > 0) {
		value = value / sum;
	}

	return value;
}

ColumnVector *LocalRBFRegression::getRBFFactors(
    ColumnVector *vector, DataSubset *subset) {
	DataSubset::iterator it = subset->begin();

	for(int i = 0; it != subset->end(); it++, i++) {
		RealType malDist = 0;
		for(int j = 0; j < vector->rows(); j++) {
			ColumnVector *data = (*input)[*it];
			malDist += pow(
			    (vector->operator[](j) - data->operator[](j))
			        / sigma->operator[](j), 2.0);
		}

		rbfFactors->operator[](i) = exp(-malDist / 2.0);
	}

	return rbfFactors;
}

ColumnVector *LocalRBFRegression::getLastRBFFactors() {
	return rbfFactors;
}

} //namespace rlearner
