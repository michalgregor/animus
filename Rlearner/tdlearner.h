// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ctdlearner_H_
#define Rlearner_ctdlearner_H_

#include "errorlistener.h"
#include "agentlistener.h"
#include "baseobjects.h"

namespace rlearner {

class AgentController;
class DeterministicController;
class AbstractQFunction;
class AbstractQETraces;
class ActionDataSet;
class GradientQFunction;
class GradientQETraces;
class ResidualFunction;
class ResidualGradientFunction;
class FeatureList;
class AbstractBetaCalculator;
class FeatureQFunction;

/**
 * @class TDLearner
 * Class for temporal Difference Learning.
 *
 * Temporal Difference (TD) Q-Value Learner are the common model-free
 * reinforcement learning algorithms. They make their update according to the
 * difference of the current Q-Value to the calculated Q Value
 * Q(s_t, a_t)=R(s_t, a_t, s_{t+1})+gamma * Q(s_{t+1}, a_{t+1}) for each
 * step sample. So the TD update for the Q-Values is
 * Q_new(s_t,a_t)=(1-alpha)*Q_old(s_t,a_t)+alpha*(R(s_t,a_t,s_{t+1})+gamma*Q(s_{t+1},a_{t+1}))
 * is further \f$Q_old(s_t,a_t) + alpha*(R(s_t,a_t, s_{t+1})+gamma*Q(s_{t+1},a_{t+1})-Q(s_t,a_t)),
 * where R(s_t, a_t, s_{t+1}) + gamma * Q(s_{t+1}, a_{t+1})- Q(s_t,a_t)) is
 * the temporal difference.
 *
 * Respectively for the semi Markov case, the temporal difference is
 * R(s_t, a_t, s_{t+1}) + gamma^N * Q(s_{t+1}, a_{t+1})- Q(s_t,a_t)). This
 * temporal difference update is usually done for states from the past too,
 * using ETraces. This Method is called TD-Lambda.
 *
 * In the RIL toolbox TD-Learner are represented by the class TDLearner and
 * provides an implementation of the TD-Lambda algorithm. The class maintains
 * a Q-Function, an ETraces Object, a Reward Function and a Policy as estimation
 * policy needed for the calculation of a_{t+1} The Q-Function, the Reward
 * Function and the Policy have to be passed from the user. The ETraces object
 * is usually initialized with the standard etraces object for the Q-Function,
 * but can also be specified.
 *
 * The learnStep Function updates the Q-Function according the step sample.
 * The function is called by the nextStep event. First of all the last
 * estimated action ($a_{t+1}$) is compared to the action doubly executed.
 * If these two actions are not equal, the ETraces have to be reset, because
 * the agent didn't follow the policy to learn. If you don't want to reset
 * the etraces you can set the parameter "ResetETracesOnWrongEstimate" to
 * false (0.0). If the 2 actions are equal the Etraces gets multiplied by
 * lambda*gamma. After that, the Etrace of the current state-action pair is
 * added to the ETraces object, then the next estimated action is calculated
 * by the given policy and stored. Now the temporal difference error can be
 * calculated by R(s_t, a_t, s_{t+1}) + gamma * Q(s_{t+1}, a_{t+1})- Q(s_t,a_t)) or R(s_t, a_t, s_{t+1}) + gamma^N * Q(s_{t+1}, a_{t+1})- Q(s_t,a_t))
 * for multi-step actions. Having the temporal difference error all the states
 * in the ETraces are updated by the updateQFunction method from the Q-Etraces
 * object. Before the update, the temporal difference error gets multiplied with
 * the learning rate (Parameter: "QLearningRate").
 *
 * The getTemporalDifference function calculates the old Q-Value and the new
 * Q-Value and then calls the getResidual function, which does the actual
 * temporal difference error computation.
 *
 * For hierarchic MDP's Intermediate steps get a special treatment in the
 * TD-Algorithm. Since the intermediate steps aren't doubly member of the
 * episode they need special treatment for etraces. The state of the
 * intermediate step is normally added to the ETraces object, but the
 * multiplication of all other ETraces is cancelled and the Q-Function isn't
 * updated with the whole ETraces object, only the Q-Value of the intermediate
 * state is updated. This is done because the intermediate step isn't directly
 * reachable for the past states and update all intermediate steps via etraces
 * would falsify the Q-Values since the same step gets updates several times.
 *
 * TDLearner has following Parameters:
 * - inherits all Parameters from the Q-Function;
 * - inherits all Parameters from the ETraces;
 * - "QLearningRate", 0.2 : learning rate of the algorithm;
 * - "DiscountFactor", 0.95 : discount factor of the learning problem;
 * - "ResetETracesOnWrongEstimate", 1.0 : reset etraces when the estimated
 * action wasn't the RealType executed.
 * @see QLearner
 * @see SarsaLearner
 **/
class TDLearner: public SemiMDPRewardListener, public ErrorSender {
protected:
	//! Use extern eTraces.
	bool externETraces;
	//! Estimation Policy - policy which is learned.
	AgentController *estimationPolicy;
	//! The last action estimated by the policy.
	Action *lastEstimatedAction;

	AbstractQFunction *qfunction;
	AbstractQETraces *etraces;
	ActionDataSet *actionDataSet;

protected:
	/**
	 * Updates the Q-Function and manages the Etraces.
	 *
	 * The learnStep Function updates the Q-Function according the step sample.
	 * The function is called by the nextStep event. First of all the last
	 * estimated action (a_{t+1}) is compared to the action doubly executed.
	 * If these two actions are not equal, the ETraces have to be reset,
	 * because the agent didn't follow the policy to learn, using the etraces
	 * of older states would falsify the Q-Values. If the 2 actions are equal
	 * the Etraces gets multiplied by lambda*gamma.
	 *
	 * After that, the Etrace of the current state-action pair is added to the
	 * ETraces object, then the next estimated action is calculated by the
	 * given policy. Now the temporal difference can be calculated by
	 * R(s_t, a_t, s_{t+1}) + gamma * Q(s_{t+1}, a_{t+1})- Q(s_t,a_t)) or
	 * R(s_t, a_t, s_{t+1}) + gamma^N * Q(s_{t+1}, a_{t+1})- Q(s_t,a_t))
	 * for multi-step actions. Having the temporal difference all the states
	 * in the ETraces are updated by the updateQFunction method from the
	 * Q-Etraces object.
	 **/
	virtual void learnStep(
	    StateCollection *oldState,
	    Action *action,
	    RealType reward,
	    StateCollection *nextState
	);

	//! Calculates the temporal difference.
	virtual RealType getTemporalDifference(
	    StateCollection *oldState,
	    Action *action,
	    RealType reward,
	    StateCollection *nextState
	);

	//! Returns the temporal difference error residual.
	virtual RealType getResidual(
	    RealType oldQ,
	    RealType reward,
	    int duration,
	    RealType newQ
	);

	//! Adds the current state to the etraces.
	virtual void addETraces(
	    StateCollection *oldState,
	    StateCollection *newState,
	    Action *action
	);

public:
	virtual void loadValues(const std::string& filename);
	virtual void saveValues(const std::string& filename);

	virtual void loadValues(std::istream& stream);
	virtual void saveValues(std::ostream& stream);

	//! Calls the update function learnStep.
	virtual void nextStep(
	    StateCollection *oldState,
	    Action *action,
	    RealType reward,
	    StateCollection *nextState
	);

	/**
	 * Updates the Q-Function for a intermediate step.
	 *
	 * Since the intermediate steps aren't doubly member of the episode they
	 * need special treatment for etraces. The state of the intermediate step
	 * is normally added to the ETraces object, but the multiplication of all
	 * other ETraces is cancelled and the Q-Function isn't updated with
	 * the whole ETraces object, only the Q-Value of the intermediate state is
	 * updated. This is done because the intermediate step isn't directly
	 * reachable for the past states and update all intermediate steps via
	 * etraces would falsify the Q-Values since the same step gets updates
	 * several times.
	 **/
	virtual void intermediateStep(
	    StateCollection *oldState,
	    Action *action,
	    RealType reward,
	    StateCollection *nextState
	);

	//! Resets the Etraces.
	virtual void newEpisode();

//	//! Sets the gamma value of the Q-Function (discount factor).
//	void setGamma(RealType gamma);

	//! Sets the learning rate.
	void setAlpha(RealType alpha);
	//! Sets the lambda parameter of the etraces.
	void setLambda(RealType lambda);

	AgentController* getEstimationPolicy();
	void setEstimationPolicy(AgentController * estimationPolicy);

	AbstractQFunction* getQFunction();
	AbstractQETraces *getETraces();

public:
	//! Creates a TD Learner with the given abstract Q-Function and Q-ETraces.
	TDLearner(
	    RewardFunction *rewardFunction,
	    AbstractQFunction *qfunction,
	    AbstractQETraces *etraces,
	    AgentController *estimationPolicy
	);

	/**
	 * Creates a TD Learner with the given composed Q-Function and a new
	 * composed Q-Etraces object.
	 *
	 * The etraces get initialised by the standard V-Etraces of the Q-Functions
	 * V-Functions. If you want to access the VEtraces you have to cast
	 * the result from getQETraces() from (AbstractQETraces *) to (QETraces *).
	 **/
	TDLearner(
	    RewardFunction *rewardFunction,
	    AbstractQFunction *qfunction,
	    AgentController *estimationPolicy
	);

	virtual ~TDLearner();
};

///
/**
 * @class QLearner
 * Class for Q-Learning.
 *
 * Q-Learning chooses always the best action for the state s_{t+1}, which
 * doesn't have to be the action executed in the state s_{t+1}, since
 * exploration policies might choose another action. So Q-Learning is
 * Off-Policy learning, it doesn't learn a the values for the agent's policy,
 * but for the optimal policy.
 *
 * The class is just a normal TD-Learner, initializing the estimation policy
 * with a QGreedyPolicy object.
 **/
class QLearner: public TDLearner {
public:
	QLearner(RewardFunction *rewardFunction, AbstractQFunction *qfunction);
	~QLearner();
};

/**
 * @class
 * Class for Sarsa Learning.
 *
 * The other possibility for choosing the action a_{t+1} is to choose always
 * the action which is doubly executed by the agent. This Method is called
 * SARSA learning (you have a (S)tate-(A)ction-(R)eward-(S)tate-(A)ction tuple
 * for update). This method learns the policy of the agent directly. Which
 * method (Q or Sarsa Learning) works better depends on the learning problem,
 * generally SARSA learning is more save if you have some states with high
 * negative reward, since SARSA learning takes the exploration policy of
 * the agent into account.
 *
 * Since the sarsa algorithm needs to know what the agent will do in the next
 * step, it gets a pointer to the agent. The agent serves as deterministic
 * controller, saving the action coming from his controller. The learner can
 * use the agen't getNextAction method to get the next estimated action.
 * The advantage that the estimation policy is the policy of the agent is that
 * the ETraces of the Sarsa Learner only have to be reset when a new Episode
 * begins. This can lead to better performance as the Q-Learning Algorithm.
 *
 * The Sarsa learner supposes a deterministic controller as estimation policy,
 * which is usually the agent or a hierarchic MDP.
 **/
class SarsaLearner: public TDLearner {
public:
	SarsaLearner(
	    RewardFunction *rewardFunction,
	    AbstractQFunction *qfunction,
	    DeterministicController *agent
	);

	~SarsaLearner();
};

class TDGradientLearner: public TDLearner {
protected:
	ResidualFunction *residual;
	ResidualGradientFunction *residualGradient;
	GradientQFunction *gradientQFunction;
	GradientQETraces *gradientQETraces;

	FeatureList *oldGradient;
	FeatureList *newGradient;
	FeatureList *residualGradientFeatures;

protected:
	virtual RealType getResidual(
	    RealType oldQ,
	    RealType reward,
	    int duration,
	    RealType newQ
	);

	virtual void addETraces(
	    StateCollection *oldState,
	    StateCollection *newState,
	    Action *action
	);

public:
	TDGradientLearner(
	    RewardFunction *rewardFunction,
	    GradientQFunction *qfunction,
	    AgentController *agent,
	    ResidualFunction *residual,
	    ResidualGradientFunction *residualGradient
	);

	virtual ~TDGradientLearner();
};

class TDResidualLearner: public TDGradientLearner {
protected:
	GradientQETraces *residualGradientTraces;
	GradientQETraces *directGradientTraces;
	GradientQETraces *residualETraces;
	AbstractBetaCalculator *betaCalculator;

	virtual void learnStep(
	    StateCollection *oldState,
	    Action *action,
	    RealType reward,
	    StateCollection *nextState
	);

public:
	void newEpisode();

	virtual void addETraces(
	    StateCollection *oldState,
	    StateCollection *newState,
	    Action *action,
	    RealType td
	);

	GradientQETraces *getResidualETraces() {
		return residualETraces;
	}

public:
	TDResidualLearner(
	    RewardFunction *rewardFunction,
	    GradientQFunction *qfunction,
	    AgentController *agent,
	    ResidualFunction *residual,
	    ResidualGradientFunction *residualGradient,
	    AbstractBetaCalculator *betaCalc);

	virtual ~TDResidualLearner();
};

class QAverageTDErrorLearner: public ErrorListener, public StateObject {
protected:
	RealType updateRate;
	FeatureQFunction *averageErrorFunction;

public:
	virtual void onParametersChanged();

	virtual void receiveError(
	    RealType error,
	    StateCollection *state,
	    Action *action,
	    ActionData *data = nullptr
	);

public:
	QAverageTDErrorLearner(
	    FeatureQFunction *averageErrorFunction,
	    RealType updateRate
	);

	virtual ~QAverageTDErrorLearner();
};

class QAverageTDVarianceLearner: public QAverageTDErrorLearner {
public:
	virtual void receiveError(
	    RealType error,
	    StateCollection *state,
	    Action *action,
	    ActionData *data = nullptr
	);

public:
	QAverageTDVarianceLearner(
	    FeatureQFunction *averageErrorFunction,
	    RealType updateRate
	);

	virtual ~QAverageTDVarianceLearner();
};

} //namespace rlearner

#endif //Rlearner_ctdlearner_H_
