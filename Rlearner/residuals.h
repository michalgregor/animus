// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cresiduals_H_
#define Rlearner_cresiduals_H_

#include "parameters.h"

namespace rlearner {

class FeatureList;
class StateCollection;

class ResidualGradientFunction: virtual public ParameterObject {
public:
	virtual void getResidualGradient(
	    FeatureList *oldGradient,
	    FeatureList *newGradient,
	    RealType duration,
	    FeatureList *residualGradientFeatures
	) = 0;

public:
	virtual ~ResidualGradientFunction() = default;
};

class ResidualFunction: public ResidualGradientFunction {
public:
	virtual RealType getResidual(
	    RealType oldV,
	    RealType reward,
	    RealType duration,
	    RealType newV
	) = 0;

public:
	virtual ~ResidualFunction() = default;
};

class DiscreteResidual: public ResidualFunction {
public:
	virtual RealType getResidual(
	    RealType oldV,
	    RealType reward,
	    RealType duration,
	    RealType newV
	);

	virtual void getResidualGradient(
	    FeatureList *oldGradient,
	    FeatureList *newGradient,
	    RealType duration,
	    FeatureList *residualGradientFeatures
	);

public:
	DiscreteResidual(RealType gamma);
	virtual ~DiscreteResidual() = default;
};

class ContinuousEulerResidual: public ResidualFunction {
public:
	virtual RealType getResidual(
	    RealType oldV,
	    RealType reward,
	    RealType duration,
	    RealType newV
	);

	virtual void getResidualGradient(
	    FeatureList *oldGradient,
	    FeatureList *newGradient,
	    RealType duration,
	    FeatureList *residualGradientFeatures
	);

public:
	ContinuousEulerResidual(RealType dt, RealType sgamma);
	virtual ~ContinuousEulerResidual() = default;
};

class ContinuousCoulomResidual: public ResidualFunction {
public:
	virtual RealType getResidual(
	    RealType oldV,
	    RealType reward,
	    RealType duration,
	    RealType newV
	);

	virtual void getResidualGradient(
	    FeatureList *oldGradient,
	    FeatureList *newGradient,
	    RealType duration,
	    FeatureList *residualGradientFeatures
	);

public:
	ContinuousCoulomResidual(RealType dt, RealType sgamma);
	virtual ~ContinuousCoulomResidual() = default;
};

class AbstractBetaCalculator: virtual public ParameterObject {
public:
	virtual RealType getBeta(
	    FeatureList *directGradient,
	    FeatureList *residualGradient
	) = 0;

public:
	virtual ~AbstractBetaCalculator() = default;
};

class CConstantBetaCalculator: public AbstractBetaCalculator {
public:
	virtual RealType getBeta(
	    FeatureList *directGradient,
	    FeatureList *residualGradient
	);

public:
	CConstantBetaCalculator(RealType beta);
	virtual ~CConstantBetaCalculator() = default;
};

class VariableBetaCalculator: public AbstractBetaCalculator {
public:
	virtual RealType getBeta(
	    FeatureList *directGradient,
	    FeatureList *residualGradient
	);

public:
	VariableBetaCalculator(RealType mu, RealType maxBeta);
	virtual ~VariableBetaCalculator() = default;
};

class ResidualBetaFunction: public ResidualGradientFunction {
protected:
	AbstractBetaCalculator *betaCalculator;
	ResidualGradientFunction *residualGradient;
	FeatureList *tempResidual;

public:
	virtual void getResidualGradient(
	    FeatureList *oldGradient,
	    FeatureList *newGradient,
	    RealType duration,
	    FeatureList *residualGradientFeatures
	);

public:
	ResidualBetaFunction& operator=(const ResidualBetaFunction&) = delete;
	ResidualBetaFunction(const ResidualBetaFunction&) = delete;

	ResidualBetaFunction(
	    AbstractBetaCalculator *betaCalculator,
	    ResidualGradientFunction *residualGradient
	);

	virtual ~ResidualBetaFunction() = default;
};

class DirectGradient: public ResidualGradientFunction {
public:
	virtual void getResidualGradient(
	    FeatureList *oldGradient,
	    FeatureList *newGradient,
	    RealType duration,
	    FeatureList *residualGradientFeatures
	);

public:
	virtual ~DirectGradient() = default;
};

} //namespace rlearner

#endif //Rlearner_cresiduals_H_
