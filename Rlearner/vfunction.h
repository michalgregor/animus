// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cvfunction_H_
#define Rlearner_cvfunction_H_

#include <cstdio>

#include "learndataobject.h"
#include "baseobjects.h"
#include "gradientfunction.h"
#include "featurefunction.h"

#define DIVERGENTVFUNCTIONVALUE 1000000
#define CONTINUOUSVFUNCTION 1
#define GRADIENTVFUNCTION 2

namespace rlearner {

class AbstractVETraces;
class FeatureQFunction;
class StochasticPolicy;
class RewardFunction;
class StateCollectionImpl;
class FeatureCalculator;
class AbstractStateDiscretizer;
class StateReward;

/**
 * @class AbstractVFunction
 * Interface representing a Value Function.
 *
 * Value functions return for each state the total discount reward which they
 * expect to get from that state following a specific policy. Usually this is
 * a Greedy Policy, greedy on the value function. In the RIL toolbox the
 * Q-Functions are composed of v-functions, one v-function for each action,
 * so the Value Function is the essential part part of each learning algorithm.
 * The kind of the value function is for the most learning algorithm
 * independent, so it does'nt matter what value functions you use for the
 * Q-Function, the algorithm just works with the interface of its v-function.
 *
 * The class AbstractVFunction is the interface for all value functions.
 * The interface provides a gamma-value for each value function, serving
 * as discount factor. The value functions have to implement functions for
 * getting V-Values, setting V-Values and updating V-Values for an specific
 * state. These three functions are:
 * - getValue(State *). The function returns the expected total discount
 *   reward for the given state.
 * - setState(State *, RealType value). This function is usually used for
 *   initialisation of th value function. It has to set the total discount
 *   reward of the state to the specified value as good as it is possible
 *   (for function  approximators).
 * - updateValue(State *, RealType td) is the function usually used for
 *   learning. Adds the td value to the current value of the function.
 *
 * All these function have an companion piece with state collections, which
 * are used by the learning algorithm. So each value function maintains its own
 * state properties object to retrieve the required state from the given state
 * collection and call the demanded function with a state as parameter.
 *
 * In the RIL toolbox there are 3 different kinds of value functions,
 * V-Tables, V-FeatureFunctions and V-Functions using Neural Networks
 * (The Torch toolbox is used for the neural networks). All these
 * value functions support the possibility to save and load the learned values.
 * The class is subclass of StateObject, with the consequent state properties
 * object the desired state is fetches from the state collection.
 *
 * The class also has a function getStandardETraces to determine which E-Traces
 * should be used. The function has to return a newly instantiated
 * AbstractVETraces object for the V-Function, which is used to compose
 * the CQETtraces object. The function returns StateVETraces as standard.
 */
class AbstractVFunction: public StateObject, virtual public LearnDataObject {
protected:
	int type;

	void addType(int newType);

public:
	bool mayDiverge;

	virtual void resetData() {
	}

	//! Calls updateValue(State *state, RealType td) with the state assigned
	//! to the value function
	virtual void updateValue(StateCollection *state, RealType td);
	//! Calls setValue(State *state, RealType qValue) with the state assigned
	//! to the value function.
	virtual void setValue(StateCollection *state, RealType qValue);
	//! Calls setValue(State *state, RealType qValue) with the state assigned
	//! to the value function.
	virtual RealType getValue(StateCollection *state);

	//! Sets the value of the state to the current value + td.
	virtual void updateValue(State *state, RealType td);
	//! Sets the value of the state, has to be implemented by
	//! the other V-Functions.
	virtual void setValue(State *, RealType) {
	}
	//! Returns the value of the state, has to be implemented by
	//! the other V-Functions.
	virtual RealType getValue(State *state) = 0;

	//! Saves the Parameters of the Value Function.
	virtual void saveData(std::ostream& file);
	//! Loads the Parameters of the Value Function.
	virtual void loadData(std::istream& file);
	//! Prints the Parameters of the Value Function.
	virtual void printValues() {
	}

	int getType();
	bool isType(int isT);

	/**
	 * Returns a standard VETraces object.
	 *
	 * The function has to return a new instantiated AbstractVETraces object,
	 * which is used to compose the CQETtraces object. The function returns
	 * StateVETraces as standard.
	 */
	virtual AbstractVETraces *getStandardETraces();

public:
	//! Constructor, the properties are needed to fetch the state from
	//! the state collection.
	AbstractVFunction(StateProperties *properties);

	virtual ~AbstractVFunction();
};

/**
 * @class ZeroVFunction
 * Value Function always returning zero
 */
class ZeroVFunction: public AbstractVFunction {
public:
	virtual RealType getValue(State *state);

public:
	ZeroVFunction();
	virtual ~ZeroVFunction() {
	}
};

class VFunctionSum: public AbstractVFunction {
protected:
	std::map<AbstractVFunction *, RealType> *vFunctions;

public:
	//! Interface for getting a Q-Value.
	virtual RealType getValue(StateCollection *state);
	virtual RealType getValue(State *state) {
		return getValue((StateCollection *) state);
	}

	virtual AbstractVETraces *getStandardETraces() {
		return nullptr;
	}

	RealType getVFunctionFactor(AbstractVFunction *vFunction);
	void setVFunctionFactor(AbstractVFunction *vFunction, RealType factor);

	void addVFunction(AbstractVFunction *vFunction, RealType factor = 1.0);
	void removeVFunction(AbstractVFunction *vFunction);

	void normFactors(RealType factor);

public:
	VFunctionSum& operator=(const VFunctionSum&) = delete;
	VFunctionSum(const VFunctionSum&) = delete;

	VFunctionSum();
	virtual ~VFunctionSum();
};

/**
 * @class DivergentVFunctionException
 * This exception is thrown if a value function has become divergent
 *
 * There can be many reasons why a value function can become divergent, for
 * example the learning rate is too high. The exception is thrown if the
 * absolute value of a state gets higher than 100000. If your value function
 * is doubly that high, please scale your reward function.
 */
class DivergentVFunctionException: public TracedError {
public:
	std::string vFunctionName;
	AbstractVFunction *vFunction;
	State *state;
	RealType value;

public:
	DivergentVFunctionException& operator=(const DivergentVFunctionException&) = default;
	DivergentVFunctionException(const DivergentVFunctionException&) = default;

	DivergentVFunctionException(std::string vFunctionName,
	        AbstractVFunction *vFunction, State *state, RealType value);
	virtual ~DivergentVFunctionException() = default;
};

/**
 * @class GradientVFunction
 * Interface for all classes that can use gradients for updating.
 *
 * Gradient V-Functions are able to calculate the gradient of the V-Function
 * with respect to the weights in the current state and can be also updated
 * by a gradient object (represented as a FeatureList object). In the toolbox
 * all gradients are represented as feature lists, where the feature index is
 * the weight index and the feature factor represents the gradient value of
 * that weights. All weights that are not listed in the feature list have
 * a zero gradient.
 *
 * For the gradient calculation all subclasses have to implement the function
 * getGradient(StateCollection *state, FeatureList *gradientFeatures), where
 * the gradient in the current state is calculated and written in the given
 * feature list. The feature list is supposed to be empty.
 *
 * All gradient-VFunctions implement the interface GradientUpdateFunction as
 * the interface for updating a gradient function, so additionally
 * the subclasses have to implement the functions:
 * - updateWeights(FeatureList *gradient): Update the weights according
 *   to the gradient.
 * - getWeights(RealType *parameters): Write all weights in the RealType array.
 * - setWeights(RealType *parameters): Set the weights according to the
 *   RealType array.
 * - resetData(): Reset all weights, needed when a new learning
 *   process is started.
 * - getNumWeights(): Return the number of weights.
 *
 * As the V-Functions implement the gradient update interface, they can use
 * variable learning rates for different weights (see AdaptiveEtaCalculator).
 */
class GradientVFunction: public AbstractVFunction,
        virtual public GradientUpdateFunction {
public:
	//! Calls updateValue(State *state, RealType td) with the state assigned
	//! to the value function.
	virtual void updateValue(StateCollection *state, RealType td);
	//! Sets the value of the state to the current value + td.
	virtual void updateValue(State *state, RealType td);

	virtual void getGradient(StateCollection *state,
	        FeatureList *gradientFeatures) = 0;

	virtual void resetData() = 0;
	virtual void loadData(std::istream& stream) {
		GradientUpdateFunction::loadData(stream);
	}

	virtual void saveData(std::ostream& stream) {
		GradientUpdateFunction::saveData(stream);
	}

	virtual AbstractVETraces *getStandardETraces();

	virtual void copy(LearnDataObject *vFunction) {
		GradientUpdateFunction::copy(vFunction);
	}

public:
	//! Constructor, the properties are needed to fetch the state from
	//! the state collection.
	GradientVFunction(StateProperties *properties);
	virtual ~GradientVFunction();
};

/**
 * @class VFunctionInputDerivationCalculator
 * Interface class for calculating the gradient dV(x)/dx.
 *
 * Interface for calculating the input derivation of a feature function.
 * The input derivation is calculated in the function getInputDerivation
 * and written in the given targetVector, which always has the dimension
 * of the model state (only for continuous state variables).
 *
 * By now there is only the numeric input derivation calculator, calculating
 * the derivation analytically is supported by feature v-functions and
 * torch-vfunctions but its not tested, so its recommended to use the
 * numeric derivation.
 */
class VFunctionInputDerivationCalculator: virtual public ParameterObject {
protected:
	StateProperties *modelState;

public:
	virtual void getInputDerivation(StateCollection *state,
	        ColumnVector *targetVector) = 0;
	unsigned int getNumInputs();

	virtual void addModifierList(StateModifierList* modifierList);
	virtual void removeModifierList(StateModifierList* modifierList);

public:
	VFunctionInputDerivationCalculator& operator=(const VFunctionInputDerivationCalculator&) = delete;
	VFunctionInputDerivationCalculator(const VFunctionInputDerivationCalculator&) = delete;

	VFunctionInputDerivationCalculator(StateProperties *modelState);
	virtual ~VFunctionInputDerivationCalculator() {}
};

/**
 * @class VFunctionNumericInputDerivationCalculator
 * Calculating the input derivation of a V-Function numerically.
 *
 * The derivation is calculated by the three point rule for each input state
 * variable, so the form
 * $f'(x) = (f(x + stepSize) - f(x - stepSize))/ 2 * stepSize$ is used,
 * stepSize is set in the constructor and also can be set by the Parameter
 * "NumericInputDerivationStepSize". For each input state variable the stepsize
 * is scaled with the size of the intervall of the state variable, so the
 * "NumericInputDerivationStepSize" parameter is given in percent,
 * and not an absolute value.
 *
 * The class "VFunctionNumericInputDerivationCalculator" has the following
 * Parameters:
 - "NumericInputDerivationStepSize": stepSize of the numeric differentiation.
 */
class VFunctionNumericInputDerivationCalculator: public VFunctionInputDerivationCalculator {
protected:
	AbstractVFunction *vFunction;
	StateCollectionImpl *stateBuffer;

public:
	virtual void getInputDerivation(StateCollection *state,
	        ColumnVector *targetVector);

	virtual void addModifierList(StateModifierList* modifierList);
	virtual void removeModifierList(StateModifierList* modifierList);

public:
	VFunctionNumericInputDerivationCalculator& operator=(const VFunctionNumericInputDerivationCalculator&) = delete;
	VFunctionNumericInputDerivationCalculator(const VFunctionNumericInputDerivationCalculator&) = delete;

	VFunctionNumericInputDerivationCalculator(StateProperties *modelState,
	        AbstractVFunction *vFunction, RealType stepSize,
	        const std::set<StateModifierList*>& modifierLists);
	virtual ~VFunctionNumericInputDerivationCalculator();
};

/**
 * @class FeatureVFunction
 *
 * Feature Functions can be used as linear approximators, like tile-coding and
 * RBF-networks, the exact usage of a feature function depends on the feature
 * state it uses. Feature states are a very common possibility to discretize
 * continuous states. A feature consists of its index and a feature activation
 * factor. The sum of all these factors should sum up to 1.
 *
 * Feature value function are modelled by the class FeatureVFunction. For each
 * feature it stores a feature value, so its just a table of features, the only
 * difference to the tabular case is the calculation of the values (the sum of
 * feature value * feature factor). FeatureVFunctions are supposed to get a
 * feature calculator or discretizer as state properties object. With this
 * state property object the value function is able retrieve its feature state
 * from the state collections. FeatureVFunction inherits all access functions
 * for the features from FeatureFunction. Additionally it implements the
 * functions for setting and getting the values with states. These functions
 * decompose the feature state into its discrete states and call the companion
 * pieces of the functions for features (e.g. integer values instead of states)
 * and multiply the values by the feature factors.
 *
 * To create a feature value function you have to pass a state properties
 * object to the constructor. The number of features for the function is
 * calculated by the discrete state size of the properties object. Of course
 * the properties object is passed to the the super class AbstractVFunction
 * and is used to retrieve the wanted state from the state collection. You also
 * have an additional constructor at yours disposal, which can be used to
 * calculate the value function from a stochastic policy given a Q-Function.
 * The Values of the features are calculated as the expectation of the action
 * probabilities multiplied with the Q-Values.
 *
 * The standard VETraces object of feature value functions are CVFeatureETraces,
 * which store the features in a feature list.
 */
class FeatureVFunction: public GradientVFunction, public FeatureFunction {
public:
	/**
	 * Can be used to calculate the value function from a stochastic policy
	 * given a Q-Function. The Values of the features are calculated as the
	 * expectation of the action probabilities multiplied with the Q-Values
	 **/
	virtual void setVFunctionFromQFunction(FeatureQFunction *qfunction,
	        StochasticPolicy *policy);

	virtual void updateWeights(FeatureList *gradientFeatures);

	/**
	 * Updates the value function given a feature or discrete state.
	 *
	 * Decomposes the feature state in its discrete state variable and adds
	 * the "td" value to the values of the features. Each update is multiplied
	 * with the corresponding feature factor.
	 */
	virtual void updateValue(State *state, RealType td);

	/**
	 * Sets the value given a feature or discrete state
	 *
	 * Decomposes the feature state in its discrete state variable and sets the
	 * values of the features to the "qValue" value. Each value is multiplied
	 * with the corresponding feature factor.
	 */
	virtual void setValue(State *state, RealType qValue);

	/**
	 * Returns the value given a feature or discrete state.
	 *
	 * Decomposes the feature state in its discrete state variable and
	 * calculates the value by summing up the feature values of the active
	 * features multiplied with their factor.
	 */
	virtual RealType getValue(State *state);

	//! Calls the saveFeatures function of FeatureFunction.
	virtual void saveData(std::ostream& file);
	//! Calls the loadFeatures function of FeatureFunction.
	virtual void loadData(std::istream& file);
	virtual void printValues();

	//! Returns a new FeatureVETraces object.
	virtual AbstractVETraces *getStandardETraces();

	virtual void getGradient(StateCollection *state,
	        FeatureList *gradientFeatures);

	virtual int getNumWeights();

	virtual void resetData();

	virtual void getWeights(RealType *parameters);
	virtual void setWeights(RealType *parameters);

	void setFeatureCalculator(FeatureCalculator *featCalc);

public:
	FeatureVFunction(int numFeatures);

	/**
	 * Creates a feature v-function with the specific feature state.
	 *
	 * The number of features for the function is calculated by the discrete
	 * state size of the properties object. Of course the properties object
	 * is passed to the the super calls AbstractVFunction and is used to
	 * retrieve the wanted state from the state collection. The properties
	 * are supposed to be from a feature or a discrete state.
	 **/
	FeatureVFunction(StateProperties *featureFact);

	/**
	 * Can be used to calculate the value function from a stochastic policy
	 * given a Q-Function. The Values of the features are calculated as the
	 * expectation of the action probabilities multiplied with the Q-Values.
	 * The constructor calls setVFunctionFromQFunction to do this. The state
	 * properties and so the number of features are taken from the Q-Function.
	 */
	FeatureVFunction(FeatureQFunction *qfunction, StochasticPolicy *policy);

	~FeatureVFunction();
};

/**
 * @class VTable
 * Value Function as a table.
 *
 * Tables are just the same as feature functions. The only difference is the
 * kind of states for the value-table, it uses discrete states. The class
 * VTable represents tabular value functions, the class is subclass of
 * FeatureVFunction. Value-Tables can only be used with
 * AbstractStateDiscretizer.
 */
class VTable: public FeatureVFunction {
public:
	void setDiscretizer(AbstractStateDiscretizer *discretizer);
	AbstractStateDiscretizer *getDiscretizer();

	int getNumStates();

public:
	VTable(AbstractStateDiscretizer *state);
	~VTable();
};

class RewardAsVFunction: public AbstractVFunction {
protected:
	StateReward *reward;

public:
	virtual RealType getValue(State *state);

public:
	RewardAsVFunction& operator=(const RewardAsVFunction&) = delete;
	RewardAsVFunction(const RewardAsVFunction&) = delete;

	RewardAsVFunction(StateReward *reward);
	virtual ~RewardAsVFunction() {}
};

} //namespace rlearner

#endif //Rlearner_cvfunction_H_
