// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cagentcontroller_H_
#define Rlearner_cagentcontroller_H_

#include <list>

#include "baseobjects.h"
#include "agentlistener.h"
#include "parameters.h"

namespace rlearner {

class StateCollection;
class ActionStatistics;

/**
 * @class AgentController
 * Class for navigating the agent.
 *
 * The agent has an own agent controller, which tells the agent what to do.
 * Agent controllers return an action given a specific state collection.
 * This is done by the interface
 * Action* getNextAction(StateCollection *state), this function must be
 * implemented by all subclasses. Agent controllers have an own action set
 * they can choose from, but the agent controllers must return one action
 * which is member of the allocated semi-MDP's action set. So the controller
 * navigating the agent can only choose from primitive actions.
 */
class AgentController: public ActionObject, virtual public ParameterObject {
public:
	//! Virtual function for returning the action for the specified state,
	//! must be implemented by all subclasses.
	virtual Action* getNextAction(StateCollection *state,
	        ActionDataSet *data = nullptr) = 0;

public:
	//! Constructor for the controller, sets the agent controllers action set.
	AgentController(ActionSet *actions);
	virtual ~AgentController();
};

/**
 * @class AgentStatisticController
 * Agent controller which returns additionally an statistic object for
 * the action.
 *
 * The statistic object gives information about how good the controller thinks
 * the chosen action is. This is used by CMultiController, to choose the best
 * controller in the specified state. Almost all implemented policies are
 * statistic controllers, returning the goodness of the q-value in comparison
 * to the other actions.
 *
 * @see ActionStatistics
 */
class AgentStatisticController: public AgentController {
public:
	virtual Action* getNextAction(StateCollection *state,
	        ActionDataSet *data = nullptr);
	virtual Action* getNextAction(StateCollection *, ActionDataSet *,
	        ActionStatistics*) {
		return nullptr;
	}

public:
	AgentStatisticController(ActionSet *actions);
	virtual ~AgentStatisticController() {}
};

/**
 * @class DeterministicController
 * Controller class makes a given controller deterministic.
 *
 * Many other controllers are stochastic controllers, which means that they
 * don't return the same action for the same state (even in the same step).
 * So if getNextAction is called more than once during a step, the resulting
 * action don't have to be the same. For some algorithm a deterministic
 * behaviour (at least for one step) is needed, so they can ask for the action
 * which has been chosen in the current step (e.g. SarsaLearner needs the
 * exact policy of the agent). The DeterministicController isn't deterministic
 * for states (same state -> same action) but for steps
 * (same step -> same action), it stores the action computed by the given
 * controller each time the getNextAction is called and a newStep begins.
 * It always returns the stored action until a new step begins again.
 *
 * The class DeterministicController makes the stochastic controller returning
 * the same action while being in the same step. Therefore it stores the
 * calculated action in the nextAction field, returning that action when further
 * requests for the action occur.  When the step is finished (nextStep event
 * occurs) the nextAction is cleared and with the first request newly
 * calculated. To gather the nextStep and newEpisode events the controller
 * HAS TO BE ADDED to the agent's listeners. The deterministic controller
 * can handle statistic controllers and normal controllers. When using
 * a statistic controller the statistic object is stored in the statistics
 * field and can be obtained by getLastActionStatistics().
 *
 * For actions with changeable action data, the action data is stored in
 * actionDataSet, so the changeable action data is calculated only once too.
 *
 * @see SemiMarkovDecisionProcess
 */
class DeterministicController: public AgentController, public SemiMDPListener {
protected:
	//! Field for storing the calculated action.
	Action *nextAction;

	/**
	 * The stochastic controller.
	 *
	 * Only one of controller and statisticController is used.
	 */
	AgentController *controller;

	/**
	 * The stochastic statistic controller.
	 *
	 * Only one of controller and statisticController is used.
	 */
	AgentStatisticController *statisticController;

	//! Last statistics returned by the statistic controller, not used when
	//! using a normal controller.
	ActionStatistics *statistics;
	bool useStatisticController;

	//! Action Data set of all actions, the controller uses this data set to
	//! store the changeable actoin data until the nextstep.
	ActionDataSet* actionDataSet;

	void initStatistics();

public:
	ActionDataSet* getActionDataSet() const {
		return actionDataSet;
	}

	/**
	 * Returns the action stored in nextAction.
	 *
	 * If the nextAction was cleared, a new action is calculated and stored
	 * in the nextAction field.
	 */
	virtual Action* getNextAction(StateCollection *state,
	        ActionDataSet *data = nullptr);

	//! Clears the nextAction, forcing a new calculation of the action.
	virtual void nextStep(StateCollection *state1, Action *action,
	        StateCollection *state2);
	//! Clears the nextAction, forcing a new calculation of the action.
	virtual void newEpisode();

	/**
	 * Returns the last statistics got from the statistic controller.
	 *
	 * Object is empty or uninitialized when no statistic controller is used.
	 **/
	ActionStatistics *getLastActionStatistics();
	bool isUsingStatisticController();

	//! Set the controller, no statistic controller is used.
	void setController(AgentController *controller);
	/**
	 * Set the controller.
	 *
	 * So a statistic controller is used and the statistics of the controller
	 * can be obtained by getLastStatistics
	 **/
	void setController(AgentStatisticController *controller);

	AgentController *getController() {
		return controller;
	}

	void setNextAction(Action *action, ActionData *data = nullptr);

public:
	DeterministicController& operator=(const DeterministicController&) = delete;
	DeterministicController(const DeterministicController&) = delete;

	//! Creates a new deterministic controller.
	DeterministicController(ActionSet *actions);
	//! Creates a new deterministic controller, with the given controller
	//! to make deterministic.
	DeterministicController(AgentController *controller);
	//! Creates a new deterministic controller, with the given statistic
	//! controller to make deterministic.
	DeterministicController(AgentStatisticController *controller);
	virtual ~DeterministicController();
};

} //namespace rlearner

#endif //Rlearner_cagentcontroller_H_
