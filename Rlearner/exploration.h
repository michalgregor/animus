// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cexploration_H_
#define Rlearner_cexploration_H_

#include "agentlistener.h"
#include "qfunction.h"
#include "policies.h"

namespace rlearner {

class GradientVFunction;
class FeatureVFunction;
class StateCollection;
class Action;
class ActionData;

class AbstractVFunction;
class AbstractQFunction;

class VisitStateCounter: public SemiMDPListener {
protected:
	GradientVFunction *visits;RealType *weights;
	int steps;

protected:
	virtual void doDecay(RealType decay);

public:
	virtual void nextStep(StateCollection *state, Action *action,
	        StateCollection *nextState);
	virtual void newEpisode();

public:
	VisitStateCounter(FeatureVFunction *visits, RealType decay = 1.0);
	virtual ~VisitStateCounter();
};

class VisitStateActionCounter: public SemiMDPListener {
protected:
	GradientQFunction *visits;RealType *weights;
	int steps;

protected:
	virtual void doDecay(RealType decay);

public:
	virtual void nextStep(StateCollection *state, Action *action,
	        StateCollection *nextState);
	virtual void newEpisode();

public:
	VisitStateActionCounter(FeatureQFunction *visits, RealType decay = 1.0);
	virtual ~VisitStateActionCounter();
};

class VisitStateActionEstimator: public VisitStateCounter {
protected:
	GradientQFunction *actionVisits;

protected:
	virtual void doDecay(RealType decay);

public:
	virtual void nextStep(StateCollection *state, Action *action,
	        StateCollection *nextState);
	virtual void newEpisode();

public:
	VisitStateActionEstimator(FeatureVFunction *stateVisits,
	        FeatureQFunction *actionVisits, RealType decay = 1.0);
	virtual ~VisitStateActionEstimator();
};

class ExplorationQFunction: public AbstractQFunction {
protected:
	AbstractVFunction *stateVisitCounter;
	AbstractQFunction *actionVisitCounter;

public:
	virtual void updateValue(StateCollection *state, Action *action, RealType td,
	        ActionData *data = nullptr);
	virtual void setValue(StateCollection *state, Action *action, RealType qValue,
	        ActionData *data = nullptr);
	virtual RealType getValue(StateCollection *state, Action *action,
	        ActionData *data = nullptr);

	virtual AbstractQETraces *getStandardETraces();

public:
	ExplorationQFunction(AbstractVFunction *stateVisitCounter,
	        AbstractQFunction *actionVisitCounter);
	virtual ~ExplorationQFunction();
};

class QStochasticExplorationPolicy: public QStochasticPolicy {
protected:
	AbstractQFunction *explorationFunction;
	RealType *explorationValues;

public:
	virtual void getActionValues(StateCollection *state,
	        ActionSet *availableActions, RealType *actionValues,
	        ActionDataSet *actionDataSet = nullptr);

	virtual AbstractQFunction *getExplorationQFunction() {
		return explorationFunction;
	}

public:
	QStochasticExplorationPolicy(ActionSet *actions,
	        ActionDistribution *distribution, AbstractQFunction *qFunctoin,
	        AbstractQFunction *explorationFunction, RealType explorationFactor);
	virtual ~QStochasticExplorationPolicy();
};

class SelectiveExplorationCalculator: public SemiMDPListener {
protected:
	QStochasticExplorationPolicy *explorationPolicy;
	RealType attention;

public:
	virtual void nextStep(StateCollection *state, Action *action,
	        StateCollection *nextState);
	virtual void newEpisode();

public:
	SelectiveExplorationCalculator(
	        QStochasticExplorationPolicy *explorationFunction);
	virtual ~SelectiveExplorationCalculator();
};

} //namespace rlearner

#endif //Rlearner_cexploration_H_
