// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_csamplingbasedmodel_H_
#define Rlearner_csamplingbasedmodel_H_

#include "agentlistener.h"
#include "agentcontroller.h"
#include "learndataobject.h"
#include "inputdata.h"
#include "baseobjects.h"
#include "parameters.h"
#include "evaluator.h"

namespace rlearner {

class EpisodeHistory;
class Action;
class TransitionFunction;
class RewardLogger;
class ActionSet;
class StateModifier;
class KDTree;
class KNearestNeighbors;
class RangeSearch;
class KDRectangle;

class ContinuousStateList:
    public DataSet, public SemiMDPListener, public LearnDataObject,
    public StateObject {
protected:
	EpisodeHistory *initLogger;
	KNearestNeighbors *nearestNeighbor;
	RangeSearch *rangeSearch;
	KDTree *kdTree;

public:
	virtual void nextStep(
	    StateCollection *oldState,
	    Action *action,
	    StateCollection *newState
	);

	virtual int addState(State *state, RealType minDist = -1.0);

	virtual void createStateList(EpisodeHistory *history, bool useInitLogger =
	    false);

	virtual void resetData();
	virtual void loadData(std::istream&);
	virtual void saveData(std::ostream&);

	void initNearestNeighborSearch();
	void disableNearestNeighborSearch();

	virtual bool isMember(ColumnVector *point);
	virtual void getNearestNeighbor(
	    ColumnVector *point,
	    int &index,
	    RealType &distance);

	virtual void getSamplesInRange(KDRectangle *rectangle, DataSubset *subset);

	virtual KDTree *getKDTree() {
		return kdTree;
	}

public:
	ContinuousStateList(StateProperties *properties);
	virtual ~ContinuousStateList();
};

class CSampleTransition {
public:
	State *state;
	ActionSet *availableActions;
	RealType reward;
	ActionData *actionData;

public:
	CSampleTransition(
	    State *state,
	    ActionSet *availableActions,
	    RealType reward,
	    ActionData *actionData = nullptr);

	virtual ~CSampleTransition();
};

class SamplingBasedTransitionModel:
    public ActionObject, SemiMDPRewardListener, public LearnDataObject,
    public StateObject {
protected:
	EpisodeHistory *initLogger;
	RewardLogger *initRewardLogger;

	typedef std::map<Action *, CSampleTransition *> Transitions;

	RewardFunction *rewardFunction;
	StateProperties *targetProperties;

	std::map<int, Transitions *> *transitions;
	ContinuousStateList *stateList;

protected:
	void clearTransitions();
	void addTransition(
	    int index,
	    Action *action,
	    StateCollection *state,
	    RealType reward);

public:
	virtual void nextStep(
	    StateCollection *oldState,
	    Action *action,
	    StateCollection *newState);

	virtual void nextStep(
	    StateCollection *oldState,
	    Action *action,
	    RealType reward,
	    StateCollection *newState);

	virtual void resetData();
	virtual void loadData(std::istream&);
	virtual void saveData(std::ostream&);

	int getNumStates();

	std::map<Action *, CSampleTransition *> *getTransitions(int index);

	virtual void createStateList(
	    EpisodeHistory *history,
	    RewardLogger *logger,
	    bool useInitLogger = false);

	ContinuousStateList *getStateList();

public:
	SamplingBasedTransitionModel(
	    StateProperties *properties,
	    StateProperties *targetProperties,
	    ActionSet *actions,
	    RewardFunction *rewardFunction);

	virtual ~SamplingBasedTransitionModel();
};

class GraphTransition {
public:
	int newStateIndex;RealType reward;
	ActionData *actionData;
	Action *action;
	RealType discountFactor;

public:
	virtual RealType getReward();

public:
	GraphTransition(
	    int newStateIndex,
	    RealType reward,
	    RealType discountFactor,
	    Action *action,
	    ActionData *actionData = nullptr);

	virtual ~GraphTransition();
};

class SamplingBasedGraph:
    public ActionObject, public StateObject, public LearnDataObject {
protected:
	typedef std::list<GraphTransition *> Transitions;
	std::map<int, Transitions *> *transitions;
	ContinuousStateList *stateList;
	int numTransitions;

protected:
	void clearTransitions();
	virtual void addTransition(
	    int index,
	    int newIndex,
	    Action *action,
	    ActionData *actionData,
	    RealType reward,
	    RealType discountFactor);

public:
	virtual void resetData();
	int getNumStates();

	std::list<GraphTransition *> *getTransitions(int index);
	ContinuousStateList *getStateList();

	virtual void loadData(std::istream&);
	virtual void saveData(std::ostream&);

	virtual void getConnectedNodes(int node, DataSubset *subset);
	virtual void createTransitions();

	virtual bool calculateTransition(int startNode, int endNode) = 0;
	virtual void getNeighboredNodes(int node, DataSubset *elementList) = 0;

	virtual bool isFinalNode(int node) = 0;
	virtual void addFinalTransition(int node) = 0;

	virtual void addState(State *addState);
	virtual void addTransitions(int node, bool newNode = false);

public:
	SamplingBasedGraph(ContinuousStateList *stateList, ActionSet *actions);
	virtual ~SamplingBasedGraph();
};

class GraphTarget {
protected:
	GraphTarget *nextTarget;

public:
	virtual bool isFinishedCanditate(ColumnVector *node) = 0;
	virtual bool isFinished(
	    ColumnVector *oldNode,
	    ColumnVector *newNode,
	    RealType &reward) = 0;

	virtual bool isTargetForState(StateCollection *state) = 0;

	GraphTarget *getNextTarget() {
		return nextTarget;
	}

public:
	GraphTarget(GraphTarget *nextTarget);
	virtual ~GraphTarget();
};

class GraphTransitionAdaptiveTarget: public GraphTransition {
protected:
	std::map<GraphTarget *, RealType> *targetReward;
	std::map<GraphTarget *, bool> *targetReached;
	GraphTarget **currentTarget;

public:
	virtual RealType getReward();
	virtual RealType getReward(GraphTarget *target);
	virtual bool isFinished(GraphTarget *target);
	virtual void addTarget(GraphTarget *target, RealType reward, bool isFinished);

public:
	GraphTransitionAdaptiveTarget(
	    int newStateIndex,
	    RealType reward,
	    RealType discountFactor,
	    Action *action,
	    ActionData *actionData,
	    GraphTarget **currentTarget);

	virtual ~GraphTransitionAdaptiveTarget();
};

class AdaptiveTargetGraph: public SamplingBasedGraph {
protected:
	std::list<GraphTarget *> *targetList;
	GraphTarget *currentTarget;

protected:
	virtual void addTransition(
	    int index,
	    int newIndex,
	    Action *action,
	    ActionData *actionData,
	    RealType reward,
	    RealType discountFactor);

	virtual void addTargetForNode(GraphTarget *target, int node);

public:
	void setCurrentTarget(GraphTarget *target);
	virtual void addTarget(GraphTarget *target);
	virtual void addState(State *addState);

public:
	AdaptiveTargetGraph(ContinuousStateList *stateList, ActionSet *actions);
	virtual ~AdaptiveTargetGraph();
};

class GraphDynamicProgramming;

class GraphController: public AgentController {
protected:
	GraphDynamicProgramming *graph;

public:
	virtual Action *getNextAction(
	    StateCollection *state,
	    ActionDataSet *dataSet);

public:
	GraphController(ActionSet *actionSet, GraphDynamicProgramming *graph);
	virtual ~GraphController();
};

class GraphAdaptiveTargetDynamicProgramming;

class AdaptiveTargetGraphController: public AgentController {
protected:
	GraphAdaptiveTargetDynamicProgramming *adaptiveGraph;

public:
	virtual Action *getNextAction(
	    StateCollection *state,
	    ActionDataSet *dataSet);

public:
	AdaptiveTargetGraphController(
	    ActionSet *actionSet,
	    GraphAdaptiveTargetDynamicProgramming *adaptiveGraph);

	virtual ~AdaptiveTargetGraphController();
};

class PolicyEvaluator;
class SupervisedLearner;

class GraphValueFromValueFunctionCalculator: public Evaluator {
protected:
	SupervisedLearner *learner;
	GraphDynamicProgramming *graph;
	PolicyEvaluator *evaluator;

public:
	virtual RealType evaluate();

public:
	GraphValueFromValueFunctionCalculator(
	    GraphDynamicProgramming *l_graph,
	    SupervisedLearner *learner,
	    PolicyEvaluator *evaluator);

	virtual ~GraphValueFromValueFunctionCalculator();
};

class StateCollectionImpl;

class SamplingBasedTransitionModelFromTransitionFunction:
    public SamplingBasedTransitionModel {
protected:
	TransitionFunction *transitionFunction;
	ActionSet *availableActions;
	ActionSet *allActions;
	RewardFunction *rewardPrediction;
	StateCollectionImpl *predictState;

public:
	virtual void nextStep(
	    StateCollection *oldState,
	    Action *action,
	    RealType reward,
	    StateCollection *newState);

public:
	SamplingBasedTransitionModelFromTransitionFunction(
	    StateProperties *properties,
	    StateProperties *targetProperties,
	    ActionSet *allActions,
	    TransitionFunction *transitionFunction,
	    RewardFunction *rewardFunction,
	    const std::set<StateModifierList*>& modifierLists,
	    RewardFunction *predictReward);

	virtual ~SamplingBasedTransitionModelFromTransitionFunction();
};

class GraphDebugger: public SemiMDPRewardListener {
protected:
	GraphDynamicProgramming *graph;
	RealType realRewardSum;
	RealType graphRewardSum;
	StateModifier *hcState;
	int step;

public:
	virtual void nextStep(
	    StateCollection *oldState,
	    Action *action,
	    RealType reward,
	    StateCollection *newState);

	virtual void newEpisode();

public:
	GraphDebugger(
	    GraphDynamicProgramming *graph,
	    RewardFunction *reward,
	    StateModifier *hcState);
};

} //namespace rlearner

#endif //Rlearner_csamplingbasedmodel_H_
