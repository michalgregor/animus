// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cdiscretizer_H_
#define Rlearner_cdiscretizer_H_

#include <map>

#include "statemodifier.h"

namespace rlearner {

/**
 * @class AbstractStateDiscretizer
 *
 * Interface for all state discretizers.
 *
 * Normal state discretization is done by all subclasses of
 * AbstractStateDiscretizer. AbstractStateDiscretizer is a subclass of
 * StateModifier. Normal state discretization assigns a single discrete state
 * index to the current model state.
 *
 * This number is calculated by the function
 * int getDiscreteStateNumber(StateCollection *), which has to be implemented
 * by all subclasses. This function is called by the getModifiedState method
 * and registered in the target state.
 *
 * AbstractStateDiscretizer also offers you the possibility to make state
 * substitutions, which is done by addStateSubstitution. With state
 * substitutions you can replace a special discrete state number with another
 * state from a specified modifier. This is needed if you want a more precise
 * resolution of the model state only for some specific discrete state numbers.
 * It is also possible to add a feature state as state substitution. The state
 * discretizer produces then feature states instead of discrete states.
 **/
class AbstractStateDiscretizer: public StateModifier {
protected:
	//! List of state substitutions.
	std::map<int, std::pair<StateModifier *, State *>*>* stateSubstitutions;

public:
	//! Returns the discrete State size of discretizer, this is the discrete
	//! state of the 1st state variable.
	virtual unsigned int getDiscreteStateSize();
	//! Function used to determine the State index from a state collection
	//! (usually from the model state).
	virtual unsigned int getDiscreteStateNumber(StateCollection *state) = 0;

	/**
	 * Registers the discrete state number into the modified state object.
	 *
	 * The state number is calculated by the interface function
	 * getDiscreteStateNumber. The getModifiedState method passes through all
	 * state substitutions until the calculated state number is reached, summing
	 * up all discrete state sizes of the modifiers from the substitutions.
	 * This sum is than added to the calculated discrete state number to make
	 * the state index unique again.
	 *
	 *  Whenever a substitution has been assigned to the current state number,
	 *  the state to substitute is calculated (or taken from the state
	 *  collection). Then the state is stored in the target state, and the
	 *  calculated discrete state number + the sum of the state sizes is added
	 *  to all discrete state variables of the state substitutions.
	 */
	virtual void getModifiedState(StateCollection *originalState,
	        State *modifiedState);

	//! Adds a state substitution for a discrete state index.
	virtual void addStateSubstitution(int discState, StateModifier *modifier);
	//! Removes the state substitution for the specified state.
	virtual void removeStateSubstitution(int discState);

public:
	AbstractStateDiscretizer& operator=(const AbstractStateDiscretizer&) = delete;
	AbstractStateDiscretizer(const AbstractStateDiscretizer&) = delete;

	//! Creates a state discretizer which with numState different states.
	AbstractStateDiscretizer(unsigned int numStates);
	virtual ~AbstractStateDiscretizer();
};

/**
 * @class ModelStateDiscretizer
 *
 * Class calculating a single discrete state from several state variables.
 *
 * The class calculates from several discrete state variables of the original
 * state (defined by the given state properties in the constructor) a single
 * discrete state number. This is done by an "and" combination of the discrete
 * state variables.
 *
 * You can also choose which discrete state variables you want
 * in the constructor.
 *
 * @see DiscreteStateOperatorAnd
 */
class ModelStateDiscretizer: public AbstractStateDiscretizer {
protected:
	StateProperties* originalState = nullptr;
	std::vector<int> _discreteStates;

	unsigned int calcDiscreteStateSize(StateProperties* prop,
		const std::vector<int>& discreteStates);

public:
	//! Calculates the discrete State Number using the and operator for all
	//! discrete state variable of the original state.
	virtual unsigned int getDiscreteStateNumber(StateCollection *state);

public:
	ModelStateDiscretizer& operator=(const ModelStateDiscretizer&) = delete;
	ModelStateDiscretizer(const ModelStateDiscretizer&) = delete;

	/**
	 * Creates an ModelStateDiscretizer which use the state defined by the
	 * properties as original state.
	 *
	 * With the parameter discreteStates you can choose the discrete state
	 * variables you want for your new discrete state. If you omit this
	 * parameter all discrete state from the original state are chosen.
	 * The properties object are the properties of the original state
	 * (most time the model state)
	 */
	ModelStateDiscretizer(
		StateProperties* properties,
		const std::vector<int>& discreteStates
	);

	ModelStateDiscretizer(
		StateProperties* properties,
		std::vector<int>&& discreteStates
	);

	virtual ~ModelStateDiscretizer();
};

/**
 * @class SingleStateDiscretizer
 *
 * Discretizes the original states' i-th continuous state into the specified
 * partitions.
 *
 * The class serves as tools so that you don't have to create your own state
 * discretizer classes, you can build your discrete state with already available
 * classes.
 *
 * This class only discretizes a single continuous state of the model state into
 * given partitions. You can specify a RealType array as partition, and which
 * continuous state variable should be used of the model state.
 *
 * The discretizer then calculates the partition in which the continuous state
 * variable is located and returns its number. Since the values in the partition
 * array are the limits of the partitions, the discrete state size is the
 * partition array's size + 1.
 *
 * The discrete states created by SingleStateDiscretizer can then be combined
 * by DiscreteStateOperatorAnd.
 */
class SingleStateDiscretizer: public AbstractStateDiscretizer {
protected:
	//! The index of the continuous state variable to discretize.
	int _dimension;
	//! The partitions array must be sorted in ascending order.
	std::vector<RealType> _partitions;

	//! The original state which's state variable is discretized.
	StateProperties* originalState = nullptr;

public:
	//! Discretizes the specified continuous state variable with
	//! the partitions array.
	virtual unsigned int getDiscreteStateNumber(StateCollection *state);
	//! Set the original State.
	virtual void setOriginalState(StateProperties *originalState);

public:
	SingleStateDiscretizer& operator=(const SingleStateDiscretizer&) = delete;
	SingleStateDiscretizer(const SingleStateDiscretizer&) = delete;

	/**
	 * Constructor.
	 *
	 * @param dimension The continuous state dimension to be discretized.
	 * @param partitions The partition vector. MUST be sorted in ascending
	 * order.
	 */
	SingleStateDiscretizer(int dimension, const std::vector<RealType>& partitions);

	/**
	 * Constructor.
	 *
	 * @param dimension The continuous state dimension to be discretized.
	 * @param partitions The partition vector. MUST be sorted in ascending
	 * order.
	 */
	SingleStateDiscretizer(int dimension, std::vector<RealType>&& partitions);

	virtual ~SingleStateDiscretizer() = default;
};

///**
// * Similar to SingleStateDiscretizer, but the partition is not predefined,
// * but rather a regular partitioning computed on the fly is used.
// */
//class SingleStateParitioner: public AbstractStateDiscretizer {
//protected:
//	//! The index of the continuous state variable to discretize.
//	int _dimension;
//	std::set<RealType> _partitions;
//
//	//! The original state which's state variable is discretized.
//	StateProperties* originalState = nullptr;
//
//public:
//	//! Discretizes the specified continuous state variable with
//	//! the partitions array.
//	virtual unsigned int getDiscreteStateNumber(StateCollection *state);
//	//! Set the original State.
//	virtual void setOriginalState(StateProperties *originalState);
//
//public:
//	SingleStateParitioner& operator=(const SingleStateParitioner&) = delete;
//	SingleStateParitioner(const SingleStateParitioner&) = delete;
//
//	SingleStateParitioner(int dimension, );
//	SingleStateParitioner(int dimension, );
//
//	virtual ~SingleStateParitioner() = default;
//};

/**
 * @class DiscreteStateOperatorAnd
 *
 * The and operator combines several discrete states to one discrete state.
 *
 * Combination the discrete states by the and operator. Several discrete state
 * variables are transformed to one non-ambiguous discrete state variable.
 * The state size of the new state is the product of all state sizes.
 *
 * When the operator is created, it is empty and has no discrete states
 * to combine, you can add as many discrete states as you want with the function
 * addStateModifier.
 *
 * The class is often use in combination with SingleStateDiscretizer because
 * here you have several discrete states variables coming from the single state
 * discretizer (usually you will have 1 discretizer for each dimension of your
 * model state) and you need to combine these discrete state numbers to one
 * unique discrete state number for your learners.
 **/
class DiscreteStateOperatorAnd: public StateMultiModifier,
        public AbstractStateDiscretizer {
public:
	State *stateBuf;

public:
	//! Calculates the discrete state index as a "and" combination of all added discrete states.
	virtual unsigned int getDiscreteStateNumber(StateCollection *state);

	//! Add the discrete state to the operator
	virtual void addStateModifier(AbstractStateDiscretizer *featCalc);

public:
	DiscreteStateOperatorAnd& operator=(const DiscreteStateOperatorAnd&) = delete;
	DiscreteStateOperatorAnd(const DiscreteStateOperatorAnd&) = delete;

	DiscreteStateOperatorAnd();
	virtual ~DiscreteStateOperatorAnd();
};

} //namespace rlearner

#endif //Rlearner_cdiscretizer_H_
