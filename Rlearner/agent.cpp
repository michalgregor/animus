// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "agent.h"
#include "episode.h"
#include "ril_debug.h"
#include "continuousactions.h"
#include "environmentmodel.h"
#include "agentlistener.h"
#include "state.h"
#include "statecollection.h"
#include "ril_debug.h"
#include <cassert>

namespace rlearner {

/** Creates a new Agent. A Episode Object is also instantiated automatically and added to the ListenerList, logging can be turned of by
 setLoggedEpisode(bool)*/
Agent::Agent(StateProperties* properties) :
	    SemiMarkovDecisionProcess(),
	    StateModifiersObject(properties), _modifierList(),
	    _currentEpisode(new Episode(properties, actions, &_modifierList))
{
	StateModifiersObject::addModifierList(&_modifierList);

	lastAction = nullptr;
	assert(properties);

	addSemiMDPListener(_currentEpisode.get());
	bLogEpisode = true;

	startNewEpisode();
}

Agent::~Agent() {
	StateModifiersObject::removeModifierList(&_modifierList);
}

void Agent::setLogEpisode(bool bLogEpisode) {
	if(this->bLogEpisode != bLogEpisode) {
		this->bLogEpisode = bLogEpisode;
		if(bLogEpisode) {
			addSemiMDPListener(_currentEpisode.get());
		} else {
			removeSemiMDPListener(_currentEpisode.get());
		}
	}
}

void Agent::addAction(PrimitiveAction *action) {
	SemiMarkovDecisionProcess::addAction(action);
}

void Agent::addStateModifier(StateModifier* modifier) {
	_modifierList.addStateModifier(modifier);
}

void Agent::removeStateModifier(StateModifier* modifier) {
	_modifierList.removeStateModifier(modifier);
}

Episode* Agent::getCurrentEpisode() {
	return _currentEpisode.get();
}

} //namespace rlearner
