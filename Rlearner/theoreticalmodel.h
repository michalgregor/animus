// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ctheoreticalmodel_H_
#define Rlearner_ctheoreticalmodel_H_

#include <map>
#include <list>

#include "learndataobject.h"
#include "action.h"
#include "baseobjects.h"
#include "agentlistener.h"
#include "utility.h"

#include "system.h"

#define TRANSITION 1
#define SEMIMDPTRANSITION 2

namespace rlearner {

class FeatureQFunction;
class AbstractStateDiscretizer;
class FeatureCalculator;
class TransitionFunction;
class FeatureList;
class StateCollection;
class State;

/**
 * @class Transition
 * Transition for the Markov Case.
 *
 * Transition represents a transition for the Markov case. Stores the
 * start-state, the end-state and the probability of the transition.
 * The type of the transition is TRANSITION. With the type field you can
 * determine whether its a Transition or a SemiMDPTransition.
 */

class Transition {
protected:
	int startState;
	int endState;
	RealType propability;
	int type;

public:
	int getStartState();
	int getEndState();

	virtual RealType getPropability();
	virtual void setPropability(RealType prop);

	virtual void load(std::istream& stream, int fixedState, bool forward);
	virtual void save(std::ostream& stream, bool forward);

	virtual bool isType(int Type);

public:
	Transition(int startState, int endState, RealType prop);
	virtual ~Transition() {	}
};

/**
 * @class SemiMDPTransition
 * Transition for the semiMDP case.
 *
 * SemiMDPTranstion stores a Transition for the Semi-Markov case. Additionally
 * there is a list of possible durations and the probabilities of the duration,
 * so the probability of the duration multiplied with the transition
 * probability is the probability of coming from state A to state B in
 * "duration" steps, executing the given action.
 *
 * The type field of SemiMDP transition is set to SEMIMDPTRANSITION. When
 * adding a duration with a specific factor, all other duration factors get
 * multiplied by 1 - factor, and then the given duration's factor is added or
 * a nes duration is added to the list if the duration wasn't member. When
 * setting a duration all other durations factors are multiplied by
 * 1 - (factor - factor_{old}).
 */
class SemiMDPTransition: public Transition {
protected:
	//! The list of the durations.
	std::map<int, RealType> *durations;

public:
	std::map<int, RealType>* getDurations();

	/**
	 * Adds the given factor the  duration's factor.
	 *
	 * When adding a duration with a specific factor, all other duration
	 * factors get multiplied by 1 - factor, and then the given duration's
	 * factor is added or a nes duration is added to the list if the duration
	 * wasn't member.
	 */
	void addDuration(int duration, RealType factor);

	//! Adds the duration's factor to the given factor.
	void setDuration(int duration, RealType factor);
	RealType getDurationFaktor(int duration);

	/**
	 * Returns the probability of transition with the specified duration.
	 *
	 * The probability is getPropability() * getDurationFaktor(duration).
	 */
	RealType getDurationPropability(int duration);

	virtual void load(std::istream& stream, int fixedState, bool forward);
	virtual void save(std::ostream& stream, bool forward);

	//! Returns the Faktor sum_N gamma^N.
	RealType getSemiMDPFaktor(RealType gamma);

public:
	SemiMDPTransition& operator=(const SemiMDPTransition&) = delete;
	SemiMDPTransition(const SemiMDPTransition&) = delete;
	SemiMDPTransition(int startState, int endState, RealType prop);

	virtual ~SemiMDPTransition();
};

/**
 * @class TransitionList
 * Class for storing Transitions.
 *
 * The transitions are all stored in a TransitionList object. The transition
 * list stores whether it is a forward or a backward list. The Transitions are
 * stored in a ordered list, the list is ordered by end-states for forward
 * lists and by start-states for backward lists. It provides functions for
 * adding a specific transition, getting the transition given a feature index
 * and determining whether a feature index is member of the transition list.
 * If the list is a forward list the search criteria for get and isMember is
 * obviously the end-State otherwise the start-state of the transitions.
 */
SYS_WNONVIRT_DTOR_OFF
class TransitionList: public std::list<Transition *> {
SYS_WNONVIRT_DTOR_ON
protected:
	//! Flag if forward or backward List.
	bool forwardList;

public:
	/**
	 * Returns whether the feature is Member of the Transition.
	 *
	 * I.e. if the list is a forward list it returns whether the featureIndex
	 * can be reached, fi the list is a backward list it returns whether the
	 * state "featuerIndex" can reach the assigned state from the List.
	 */
	bool isMember(int featureIndex);
	bool isForwardList();

	/**
	 * Adds a transition.
	 *
	 * Adds a Transition to the sorted list in the right position.
	 */
	void addTransition(Transition *transition);

	//! Returns the Transition with the specified feature as end (forward list)
	//! resp. start (backward list) state.
	Transition *getTransition(int featureIndex);

	TransitionList::iterator getTransitionIterator(int featureIndex);

	//! Clears the list and deletes all Transition objects.
	void clearAndDelete();

public:
	TransitionList& operator=(const TransitionList&) = delete;
	TransitionList(const TransitionList&) = delete;

	TransitionList(bool forwardList);
	virtual ~TransitionList() = default;
};

/**
 * @class StateActionTransitions
 * Class for storing the Backward and Forward Transitions for a given state
 * action pair.
 *
 * Saves the forward and the backward Transitions in 2 different Transition
 * Lists.
 */
class StateActionTransitions {
protected:
	TransitionList *forwardList;
	TransitionList *backwardList;

public:
	TransitionList* getForwardTransitions();
	TransitionList* getBackwardTransitions();

public:
	StateActionTransitions& operator=(const StateActionTransitions&) = delete;
	StateActionTransitions(const StateActionTransitions&) = delete;
	StateActionTransitions();

	~StateActionTransitions();
};

/**
 * Interface for all model classes.
 *
 * The models are only designed for feature and discrete states
 * (i.e. discretized states). The class defines the functions for getting the
 * Probabilities of a specific state transition (so a state-action-state, resp.
 * feature-action-feature tuple is given). In Addition you can retrieve the
 * forward and the backward transitions for a specific state action pair.
 *
 * The interface provides 4 Functions, which the subclasses have to implement:
 * - getPropability(int oldFeature, int action, int newFeature) has to return
 *   the propability P(s'|s,a);
 * - getPropability(int oldFeature, int action, int duration, int newFeature)
 *   has to return the propability for the semi MDP case, i.e. P(s',N|s,a);
 * - TransitionList* getForwardTransitions(int action, int state) has to
 *   return a list of all Transitions containing the states which can be reach
 *   from the state executing the action.
 * - TransitionList* getBackwardTransitions(int action, int state) has to
 *   return a list of all Transitions containing the states which can reach
 *   the state given state executing the action.
 */
class AbstractFeatureStochasticModel: public ActionObject {
protected:
	unsigned int numFeatures;
	StateModifier *discretizer;

	bool createdActions;

public:
	//! Calls the getPropability function with the action index as argument.
	virtual RealType getPropability(int oldFeature, Action *action,
	        int newFeature);

	/**
	 * Interface function.
	 *
	 * Has to return the probability P(s'|s,a).
	 */
	virtual RealType getPropability(int oldFeature, int action,
	        int newFeature) = 0;

	/**
	 * Interface function.
	 *
	 * Has to return the probability for the semi-MDP case, i.e. P(s',N|s,a)
	 */
	virtual RealType getPropability(int oldFeature, int action, int duration,
	        int newFeature) = 0;

	//! Calculates the probabilities for a list of features.
	virtual RealType getPropability(FeatureList *oldList, Action *action,
	        FeatureList *newList);

	virtual RealType getPropability(StateCollection *oldState, Action *action,
	        StateCollection *newState);
	virtual RealType getPropability(State *oldState, Action *action,
	        State *newState);

	/**
	 * Interface function.
	 *
	 * Has to return a list of all Transitions containing the states which
	 * can be reach from the state executing the action.
	 */
	virtual TransitionList* getForwardTransitions(int action, int state) = 0;
	virtual TransitionList* getForwardTransitions(Action *action,
	        State *state);
	virtual TransitionList* getForwardTransitions(Action *action,
	        StateCollection *state);

	/**
	 * Interface function.
	 *
	 * Has to return a list of all Transitions containing the states which can
	 * reach the state given state executing the action.
	 */
	virtual TransitionList* getBackwardTransitions(int action, int state) = 0;
	virtual unsigned int getNumFeatures();

public:
	AbstractFeatureStochasticModel& operator=(const AbstractFeatureStochasticModel&) = delete;
	AbstractFeatureStochasticModel(const AbstractFeatureStochasticModel&) = delete;

	//! To create the model you have to provide the Models actions and
	//! the number of different states
	AbstractFeatureStochasticModel(ActionSet *actions, int numStates);
	AbstractFeatureStochasticModel(ActionSet *actions,
	        StateModifier *discretizer);
	AbstractFeatureStochasticModel(int numActions, int numFeatures);
	virtual ~AbstractFeatureStochasticModel();
};

/**
 * @class FeatureStochasticModel
 * Class for loading and storing a fixed Model.
 *
 * The class FeatureStochasticModel implements all functions from the
 * interface AbstractFeatureStochasticModel, therefore it maintains a
 * StateActionTransitions for every state-action pair. For storing the
 * StateActionTransitions object a 2 dimensional array is used. The class
 * provides additional functions for setting the probability of a transition.
 * If the transition doesn't exist, a new transition object is created with
 * the specified probability, otherwise the probability is just set. For the
 * semi-MDP case, the duration of the transition can be specified as well.
 * The given probability is then added to the existing transition probability
 * and the duration factors are all adopted so that there sum is again one.
 * The model can only be used for loading, saving and directly setting the
 * probabilities, its not able to learn anything from a learning trial.
 */
class FeatureStochasticModel: public AbstractFeatureStochasticModel {
protected:
	//! The array of state-action Transitions.
	MyArray2D<StateActionTransitions *> *stateTransitions;
	//! Load the model from file, can only be used at the constructor
	//! of the class.
	void load(std::istream& stream);

	/**
	 * Returns a new Transition object for the specified action.
	 *
	 * If the action is a multistep action, a SemiMDPTransition is returned,
	 * otherwise a Transition object. The transition gets initialised with
	 * the given values.
	 */
	Transition *getNewTransition(int startState, int endState, Action *action,
	RealType propability);

public:
	/**
	 * Returns the Propability of the transition.
	 *
	 * Looks in the forward transitions of <oldFeature, action> whether
	 * a Transition to newFeature exists, if not 0 is returned, otherwise
	 * the probability of the transition.
	 */
	virtual RealType getPropability(int oldFeature, int action, int newFeature);

	/**
	 * Returns the probability of the transition.
	 *
	 * Looks in the forward transitions of <oldFeature, action> whether a
	 * Transition to newFeature exists with the specified duration, if not
	 * 0 is returned, otherwise the probability of the transition.
	 */
	virtual RealType getPropability(int oldFeature, int action, int duration,
	        int newFeature);
	void setPropability(RealType propability, int oldFeature, int action,
	        int newFeature);
	void setPropability(RealType propability, int oldFeature, int action,
	        int duration, int newFeature);

	//! Just returns the forward transition list for the given
	//! state-action Pair.
	virtual TransitionList* getForwardTransitions(int action, int state);
	//! Just returns the forward transition list for the given
	//! state-action Pair.
	virtual TransitionList* getBackwardTransitions(int action, int state);

	virtual void save(std::ostream& stream);

public:
	FeatureStochasticModel& operator=(const FeatureStochasticModel&) = delete;
	FeatureStochasticModel(const FeatureStochasticModel&) = delete;

	//! Loads the model from file.
	FeatureStochasticModel(ActionSet *actions, int numFeatures, std::istream& file);
	//! Creates a new model.
	FeatureStochasticModel(ActionSet *actions, int numFeatures);
	FeatureStochasticModel(int numActions, int numFeatures);

	virtual ~FeatureStochasticModel();
};

class StochasticModelAction: public PrimitiveAction {
protected:
	AbstractFeatureStochasticModel *model;
public:
	virtual bool isAvailable(StateCollection *state);

public:
	StochasticModelAction& operator=(const StochasticModelAction&) = delete;
	StochasticModelAction(const StochasticModelAction&) = delete;

	StochasticModelAction(AbstractFeatureStochasticModel *model);
	virtual ~StochasticModelAction() {}
};

/**
 * @class AbstractFeatureStochasticEstimatedModel
 * Base class for all estimated models.
 *
 * Estimated Models estimate the probability of the state transition by
 * counting the number of Transitions from  a specific state action pair to a
 * specific state and the number of visits from of the specific state-action
 * pair. So the estimated model is build on the fly, during learning.
 * This is done by the class AbstractFeatureStochasticEstimatedModel. The
 * class is subclass of FeatureStochasticModel so it stores the transition
 * probabilities in the Transition list. In addition it has an RealType array
 * which stores the visits of the state action pair (RealType is needed because
 * feature visits can be RealType valued). The Transition-visits are not stored
 * explicitly but can be recovered by multiplying the probability with the
 * visits of the state action pair.
 *
 * The class AbstractFeatureStochasticEstimatedModel provides the function
 * doUpdateStep for updating the transitions and the visit table when
 * a specific feature is visited (with an given factor). The function first
 * calculates the visits of the Transitions (multiplying state-action visits
 * with transition probability), updates the visits of the state-action pair
 * (the factor of the feature is added), and then recalculates the new
 * probabilities of the transitions (by dividing the transition visits
 * through the state action visits). Before this is done the feature factor
 * is added to the specified transition's visits or a new Transition object
 * is created if the transition has not existed by now.
 *
 * The class also has the possibility to forget transitions from the past,
 * so the probabilities can adapt to changing models more quickly. This is done
 * by the timeFaktor. Each time an update occurs, the state-action visits are
 * multiplied by the timeFaktor before updating. By default the time factor
 * is 1.0, so nothing is forgotten.
 *
 * There are additional functions for retrieving the transition and the state
 * action and the state visits.
 *
 * The subclasses of AbstractFeatureStochasticEstimatedModel only have to
 * implement the function nextStep(...) from the SemiMDPListener interface.
 * Intermediate steps don't need a special treatment, and are updated like
 * normal step.
 */
class AbstractFeatureStochasticEstimatedModel: public FeatureStochasticModel,
        public SemiMDPListener,
        public StateObject,
        public LearnDataObject
{
protected:
	FeatureQFunction *stateActionVisits;

	/**
	 * Updates the probabilities of the transitions from oldFeature
	 * and the given actions.
	 *
	 * The function first calculates the visits of the Transitions
	 * (multiplying state-action visits with transition probability), updates
	 * the visits of the state-action pair (the factor of the feature is
	 * added), and then recalculates the new probabilities of the transitions
	 * (by dividing the transition visits through the state action visits).
	 * Before this is done the feature factor is added to the specified
	 * transition's visits or a new Transition object is created if the
	 * transition has not existed by now. For the SemiMDP case the duration
	 * is added to the transition after the updates.
	 */
	virtual void updateStep(int oldFeature, Action *action, int newFeature,
	RealType Faktor);

public:
	//! The nextStep method, must be implemented by the subclasses.
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *nextState) = 0;
	//! Intermediate Steps can be treated as normal steps in the model
	//! based case.
	virtual void intermediateStep(StateCollection *oldState, Action *action,
	        StateCollection *nextState);

	virtual void saveData(std::ostream& stream);
	virtual void loadData(std::istream& stream);

	virtual void resetData();

	/**
	 * Returns the Transition visits of the specified Transition.
	 *
	 * The transition visits show how often a specific transition has been
	 * occurred and they are calculated by multiplying the probability of the
	 * transition with the visits of the state action pair.
	 */
	RealType getTransitionsVisits(int oldFeature, Action *action,
	        int newFeature);

	/**
	 * Returns the State Action Visits.
	 *
	 * Returns how often the given action was chosen in the given state.
	 * The State Action visits are stored in saVisits.
	 */
	RealType getStateActionVisits(int Feature, int action);

	/**
	 * Returns how often the agent visited the given state.
	 *
	 * This is calculated by summing up the state action visits.
	 */
	RealType getStateVisits(int Feature);

public:
	AbstractFeatureStochasticEstimatedModel& operator=(const AbstractFeatureStochasticEstimatedModel&) = delete;
	AbstractFeatureStochasticEstimatedModel(const AbstractFeatureStochasticEstimatedModel&) = delete;

	//! Creates a new estimated model.
	AbstractFeatureStochasticEstimatedModel(StateProperties *properties,
	        FeatureQFunction *stateActionVisits, ActionSet *actions,
	        int numFeatures);

	//! Loads an estimated model from a file.
	AbstractFeatureStochasticEstimatedModel(StateProperties *properties,
	        FeatureQFunction *stateActionVisits, ActionSet *actions,
	        int numFeatures, std::istream& file);

	virtual ~AbstractFeatureStochasticEstimatedModel();
};

/**
 * @class DiscreteStochasticEstimatedModel
 * Estimated Model for Discrete States.
 *
 * Implements the function nextStep for updating.
 * DiscreteStochasticEstimatedModel updates the transitions of the specified
 * discrete state number with visit factor 1.0. To retrieve the state from the
 * statecollection the in the constructor given discretizer is used.
 *
 * @see AbstractFeatureStochasticEstimatedModel
 */
class DiscreteStochasticEstimatedModel: public AbstractFeatureStochasticEstimatedModel {
protected:
	AbstractStateDiscretizer *discretizer;

public:
	//! Updates the discrete state transition.
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *nextState);

	int getStateActionVisits(int Feature, int action);
	int getStateVisits(int Feature);

public:
	DiscreteStochasticEstimatedModel& operator=(const DiscreteStochasticEstimatedModel&) = delete;
	DiscreteStochasticEstimatedModel(const DiscreteStochasticEstimatedModel&) = delete;

	DiscreteStochasticEstimatedModel(AbstractStateDiscretizer *discState,
	        FeatureQFunction *stateActionVisits, ActionSet *actions);
	virtual ~DiscreteStochasticEstimatedModel() {}
};

/**
 * Estimated Model for feature States.
 *
 * Implements the function nextStep for updating.
 * FeatureStochasticEstimatedModel updates all transitions for all
 * combinations of start and end-states. The visit factor is calculated by
 * multiplying the 2 feature factors of the specific start and end-features.
 * To retrieve the state from the state collection the in the constructor
 * given feature calculator is used.
 */
class FeatureStochasticEstimatedModel: public AbstractFeatureStochasticEstimatedModel {
protected:
	FeatureCalculator *featCalc;

public:
	/**
	 * Updates the Transitions for feature states.
	 *
	 * Updates all transitions for all combinations of start and end-states.
	 * The visit factor used by doUpdateStep is calculated by multiplying
	 * the 2 feature factors of the specific start and end-features. To
	 * retrieve the state from the state collection the in the constructor
	 * given feature calculator is used.
	 */
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *nextState);

public:
	FeatureStochasticEstimatedModel& operator=(const FeatureStochasticEstimatedModel&) = delete;
	FeatureStochasticEstimatedModel(const FeatureStochasticEstimatedModel&) = delete;

	FeatureStochasticEstimatedModel(FeatureCalculator *properties,
		        FeatureQFunction *stateActionVisits, ActionSet *actions);
	virtual ~FeatureStochasticEstimatedModel() {
	}
};

} //namespace rlearner

#endif //Rlearner_ctheoreticalmodel_H_
