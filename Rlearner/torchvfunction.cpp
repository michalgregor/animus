// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>

#include "ril_debug.h"
#include "torchvfunction.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "action.h"
#include "vetraces.h"

#ifdef USE_TORCH

namespace rlearner {

TorchFunction::TorchFunction(Machine *machine) {
	this->machine = machine;
}

TorchFunction::~TorchFunction() {
}

RealType TorchFunction::getValueFromMachine(Sequence *input) {
	machine->forward(input);
	return (**machine->outputs->frames);
}

Machine *TorchFunction::getMachine() {
	return machine;
}

/// Creates a new value function learning with a torch gradient machine
TorchGradientFunction::TorchGradientFunction(GradientMachine *machine) :
	    TorchFunction(machine),
	    GradientFunction(machine->n_inputs, machine->n_outputs) {
	localEtaCalc = nullptr;

	input = new Sequence(1, machine->n_inputs);
	alpha = new Sequence(1, machine->n_outputs);

	setGradientMachine(machine);

	//addParameter("InitWeightVarianceFactor", 1.0);
	//addParameter("TorchNormalizeWeights", 0.0);
}

TorchGradientFunction::TorchGradientFunction(int numInputs, int numOutputs) :
	TorchFunction(nullptr), GradientFunction(numInputs, numOutputs) {

	localEtaCalc = nullptr;

	input = new Sequence(1, numInputs);
	alpha = new Sequence(1, numOutputs);

	gradientMachine = nullptr;
}

TorchGradientFunction::~TorchGradientFunction() {
	delete alpha;
	delete input;
	delete localEtaCalc;
}

void TorchGradientFunction::getFunctionValuePre(
    ColumnVector *inputVector, ColumnVector *output
) {

	if(gradientMachine == nullptr) {
		output->setZero();
		return;
	}

	for(int i = 0; i < getNumInputs(); i++) {
		input->frames[0][i] = inputVector->operator[](i);
	}

	gradientMachine->forward(input);

	for(int i = 0; i < getNumOutputs(); i++) {
		output->operator[](i) = gradientMachine->outputs->frames[0][i];
	}
}

void TorchGradientFunction::updateWeights(FeatureList *gradientFeatures) {
	if(gradientMachine == nullptr) {
		return;
	}

	Parameters *params = (gradientMachine)->params;

	if(DebugIsEnabled('v')) {
		DebugPrint('v', "Updating Torch Function, Gradient: ");
		gradientFeatures->saveASCII(DebugGetFileHandle('v'));
		DebugPrint('v', "\n");
	}

	if(params) {
		FeatureList::iterator it = gradientFeatures->begin();

		for(; it != gradientFeatures->end(); it++) {
			assert((*it)->featureIndex < (unsigned int ) params->n_params);

			int param_index = 0;
			int param_offset = 0;

			while((unsigned int) ((*it)->featureIndex - param_offset)
			    > (unsigned int) (params->size[param_index])) {
				assert(param_index + 1 < params->n_data);
				param_offset += params->size[param_index];
				param_index += 1;
			}

			//DebugPrint('v', "\n Parameter: %d, Params Number: %d Params Size: %d\n", (*it)->featureIndex, param_index, params->size[param_index]);

			//DebugPrint('v', "(%f %f)", params->data[param_index][(*it)->featureIndex - param_offset], (*it)->factor);

			params->data[param_index][(*it)->featureIndex - param_offset] +=
			    (*it)->factor;

			//DebugPrint('v', "\n");
		}
	}

	/*if (getParameter("TorchNormalizeWeights") > 0.5)
	 {
	 for (int i = 0; i < params->n_data - 1; i ++)
	 {
	 RealType sum = 0;

	 for (int j = 0; j < params->size[i]; j++)
	 {
	 sum += pow(params->data[i][j], 2);
	 }
	 sum = sqrt(sum);

	 for (int j = 0; j < params->size[i]; j++)
	 {
	 params->data[i][j] /= 2.0;
	 }
	 }
	 }*/
}

void TorchGradientFunction::resetData() {
	if(gradientMachine == nullptr) {
		return;
	}

	Parameters *params = gradientMachine->params;

	int inputs = getNumInputs() + 1;
	DebugPrint('v', "Torch Gradient Function InitValues:\n");

	RealType sigma = 1.0; // getParameter("InitWeightVarianceFactor");

	if(params) {
		for(int i = 0; i < params->n_data; i++) {
			for(int j = 0; j < params->size[i]; j++) {
				params->data[i][j] = Distributions::getNormalDistributionSample(
				    0.0, (1.0 / inputs) * sigma);
				DebugPrint('v', "%f ", params->data[i][j]);

			}
			DebugPrint('v', "\n");

			inputs = params->size[i] / inputs + 1;
		}
	}
}

void TorchGradientFunction::setGradientMachine(GradientMachine *machine) {
	this->gradientMachine = machine;

	if(localEtaCalc) {
		delete localEtaCalc;
		localEtaCalc = nullptr;
	}

	if(gradientMachine) {
		localEtaCalc = new CTorchGradientEtaCalculator(gradientMachine);
	}

}

GradientMachine *TorchGradientFunction::getGradientMachine() {
	return gradientMachine;
}

void TorchGradientFunction::getGradientPre(
    ColumnVector *inputVector, ColumnVector *outputErrors,
    FeatureList *gradientFeatures) {
	if(gradientMachine == nullptr) {
		return;
	}

	Parameters *params = (gradientMachine)->params;
	Parameters *der_params = (gradientMachine)->der_params;

	for(int i = 0; i < getNumInputs(); i++) {
		input->frames[0][i] = inputVector->operator[](i);
	}

	for(int i = 0; i < getNumOutputs(); i++) {
		alpha->frames[0][i] = outputErrors->operator[](i);
	}

	gradientMachine->iterInitialize();
	if(der_params) {
		for(int i = 0; i < der_params->n_data; i++) {
			for(int j = 0; j < params->size[i]; j++) {
				der_params->data[i][j] = 0.0;
			}
		}
		//memset(der_params->data[i], 0, sizeof(real)*der_params->size[i]);
	}

	gradientMachine->forward(input);
	gradientMachine->backward(input, alpha);

	DebugPrint('v', "\n Getting Torch Gradient Params Size: %d\n",
	    getNumWeights());

	if(params) {
		int param_offset = 0;
		for(int i = 0; i < params->n_data; i++) {
			real *ptr_der_params = der_params->data[i];
			real *ptr_params = params->data[i];

			for(int j = 0; j < params->size[i]; j++) {
				gradientFeatures->set(param_offset + j, ptr_der_params[j]);
				DebugPrint('v', "%f (%f)", ptr_der_params[j], ptr_params[j]);
			}
			DebugPrint('v', "\n");

			param_offset += params->size[i];
		}
	}
}

int TorchGradientFunction::getNumWeights() {
	if(gradientMachine == nullptr) {
		return 0;
	}

	return gradientMachine->params->n_params;
}

void TorchGradientFunction::getInputDerivationPre(
    ColumnVector *inputVector, Matrix *targetMatrix) {
	if(gradientMachine == nullptr) {
		return;
	}

	targetMatrix->setZero();

	Sequence *beta = (gradientMachine)->beta;

	for(int i = 0; i < getNumInputs(); i++) {
		input->frames[0][i] = inputVector->operator[](i);
	}

	for(int nout = 0; nout < getNumOutputs(); nout++) {
		for(int i = 0; i < getNumOutputs(); i++) {
			alpha->frames[0][i] = 0.0;
		}

		alpha->frames[0][nout] = 1.0;

		gradientMachine->iterInitialize();

		gradientMachine->forward(input);
		gradientMachine->backward(input, alpha);

		if(beta) {
			for(int i = 0; i < getNumInputs(); i++) {
				targetMatrix->operator()(nout, i) = beta->frames[0][i];
			}
		}
	}

}

void TorchGradientFunction::getWeights(RealType *parameters) {
	if(gradientMachine == nullptr) {
		return;
	}

	Parameters *params = (gradientMachine)->params;

	if(params) {
		int paramIndex = 0;
		for(int i = 0; i < params->n_data; i++) {
			real *ptr_params = params->data[i];

			for(int j = 0; j < params->size[i]; j++) {
				parameters[paramIndex] = ptr_params[j];
				paramIndex++;
			}
		}
	}
}

void TorchGradientFunction::setWeights(RealType *parameters) {
	if(gradientMachine == nullptr) {
		return;
	}

	Parameters *params = (gradientMachine)->params;

	if(params) {
		int paramIndex = 0;
		for(int i = 0; i < params->n_data; i++) {
			real *ptr_params = params->data[i];

			for(int j = 0; j < params->size[i]; j++) {
				ptr_params[j] = parameters[paramIndex];
				paramIndex++;
			}
		}
	}
	if(DebugIsEnabled('t')) {
		DebugPrint('t', "Setting Torch Weights: ");

		saveData(DebugGetFileHandle('t'));
	}
}

CTorchGradientEtaCalculator::CTorchGradientEtaCalculator(
    GradientMachine *gradientMachine) :
	IndividualEtaCalculator(gradientMachine->params->n_params) {
	Parameters *params = gradientMachine->params;

	int inputs = gradientMachine->n_inputs + 1;
	int neurons = 1;
	int parameterIndex = 0;
	RealType factor = 1.0;
	if(params) {
		for(int i = 0; i < params->n_data; i++) {
			for(int j = 0; j < params->size[i]; j++) {
				this->etas[parameterIndex] = factor;
				parameterIndex++;
			}
			inputs = params->size[i] / inputs + 1;
			neurons = inputs - 1;
			factor = 1 / sqrt((RealType) neurons);
		}
	}
}

TorchVFunction::TorchVFunction(
    TorchFunction *torchFunction, StateProperties *properties) :
	AbstractVFunction(properties) {
	input = new Sequence(1,
	    properties->getNumContinuousStates()
	        + properties->getNumDiscreteStates());

	this->torchFunction = torchFunction;
}

TorchVFunction::~TorchVFunction() {
	delete input;
}

void TorchVFunction::getInputSequence(State *state, Sequence *sequence) {
	for(unsigned int i = 0; i < state->getNumContinuousStates(); i++) {
		sequence->frames[0][i] = state->getContinuousState(i);
	}
	for(unsigned int i = 0; i < state->getNumActiveDiscreteStates(); i++) {
		sequence->frames[0][i + state->getNumContinuousStates()] =
		    state->getDiscreteState(i);
	}
}

RealType TorchVFunction::getValue(State *state) {
	getInputSequence(state, input);
	RealType value = torchFunction->getValueFromMachine(input);

	state->saveASCII( stdout);

	if(!mayDiverge
	    && (value < -DIVERGENTVFUNCTIONVALUE || value > DIVERGENTVFUNCTIONVALUE)) {
		throw DivergentVFunctionException("Torch VFunction", this, state, value);
	}

	return value;
}

VFunctionFromGradientFunction::VFunctionFromGradientFunction(
    GradientFunction *l_gradientFunction, StateProperties *properties) :
	    GradientVFunction(properties),
	    VFunctionInputDerivationCalculator(properties) {
	this->gradientFunction = l_gradientFunction;

	printf("%d %d %d %d\n", properties->getNumContinuousStates(),
	    properties->getNumDiscreteStates(), gradientFunction->getNumInputs(),
	    gradientFunction->getNumOutputs());
	assert(
	    properties->getNumContinuousStates()
	        + properties->getNumDiscreteStates()
	        == (unsigned int ) gradientFunction->getNumInputs()
	        && gradientFunction->getNumOutputs() == 1);

	input = new ColumnVector(
	    properties->getNumContinuousStates()
	        + properties->getNumDiscreteStates());
	outputError = new ColumnVector(1);
	outputError->operator[](0) = 1.0;

	this->inputDerivation = new Matrix(1,
	    properties->getNumContinuousStates()
	        + properties->getNumDiscreteStates());

	addParameters(l_gradientFunction);
}

VFunctionFromGradientFunction::~VFunctionFromGradientFunction() {
	delete input;
	delete outputError;
	delete inputDerivation;
}

void VFunctionFromGradientFunction::getInputSequence(
    State *state, ColumnVector *sequence) {
	for(unsigned int i = 0; i < state->getNumActiveContinuousStates(); i++) {
		sequence->operator[](i) = state->getContinuousState(i);
	}
	for(unsigned int i = 0; i < state->getNumActiveDiscreteStates(); i++) {
		sequence->operator[](i + state->getNumContinuousStates()) =
		    state->getDiscreteState(i);
	}
}

void VFunctionFromGradientFunction::setValue(State *state, RealType value) {
	updateValue(state, value - getValue(state));
}

void VFunctionFromGradientFunction::resetData() {
	gradientFunction->resetData();
}

RealType VFunctionFromGradientFunction::getValue(State *state) {
	getInputSequence(state, input);

	gradientFunction->getFunctionValue(input, outputError);

	RealType value = outputError->operator[](0);

	if(!mayDiverge
	    && (value < -DIVERGENTVFUNCTIONVALUE || value > DIVERGENTVFUNCTIONVALUE)) {
		throw DivergentVFunctionException("Torch VFunction", this, state,
		    value);
	}

	return value;
}

void VFunctionFromGradientFunction::updateWeights(
    FeatureList *gradientFeatures) {
	gradientFunction->updateWeights(gradientFeatures);
}

int VFunctionFromGradientFunction::getNumWeights() {
	return gradientFunction->getNumWeights();
}

void VFunctionFromGradientFunction::getGradient(
    StateCollection *originalState, FeatureList *modifiedState) {
	State *state = originalState->getState(this->getStateProperties());

	getInputSequence(state, input);
	outputError->operator[](0) = 1.0;

	gradientFunction->getGradient(input, outputError, modifiedState);
}

void VFunctionFromGradientFunction::getInputDerivation(
    StateCollection *originalState, ColumnVector *targetVector) {
	State *state = originalState->getState(this->getStateProperties());

	getInputSequence(state, input);

	gradientFunction->getInputDerivation(input, targetVector);

//	memcpy(targetVector->getData(), inputDerivation->getRow(0), sizeof(RealType) * gradientFunction->getNumInputs());
}

AbstractVETraces *VFunctionFromGradientFunction::getStandardETraces() {
	return new GradientVETraces(this);
}

void VFunctionFromGradientFunction::getWeights(RealType *parameters) {
	gradientFunction->getWeights(parameters);
}

void VFunctionFromGradientFunction::setWeights(RealType *parameters) {
	gradientFunction->setWeights(parameters);
}

QFunctionFromGradientFunction::QFunctionFromGradientFunction(
    ContinuousAction *contAction, GradientFunction *gradientFunction,
    ActionSet *actions, StateProperties *properties) :
	ContinuousActionQFunction(contAction), StateObject(properties) {
	assert(
	    properties->getNumContinuousStates()
	        + properties->getNumDiscreteStates()
	        + contAction->getNumDimensions()
	        == (unsigned int ) gradientFunction->getNumInputs()
	        && gradientFunction->getNumOutputs() == 1);

	input = new ColumnVector(
	    properties->getNumContinuousStates()
	        + properties->getNumDiscreteStates()
	        + contAction->getNumDimensions());
	outputError = new ColumnVector(1);
	outputError->operator[](0) = 1.0;

	this->gradientFunction = gradientFunction;

	staticActions = actions;
}

QFunctionFromGradientFunction::~QFunctionFromGradientFunction() {
	delete input;
	delete outputError;
}

void QFunctionFromGradientFunction::getInputSequence(
    ColumnVector *sequence, State *state, ContinuousActionData *data) {
	for(unsigned int i = 0; i < state->getNumContinuousStates(); i++) {
		sequence->operator[](i) = state->getContinuousState(i);
	}
	for(unsigned int i = 0; i < state->getNumDiscreteStates(); i++) {
		sequence->operator[](i + state->getNumContinuousStates()) =
		    state->getDiscreteState(i);
	}
	for(int i = 0; i < data->rows(); i++) {
		RealType min =
		    contAction->getContinuousActionProperties()->getMinActionValue(i);
		RealType width =
		    contAction->getContinuousActionProperties()->getMaxActionValue(i)
		        - min;

		sequence->operator[](
		    i + state->getNumContinuousStates() + state->getNumDiscreteStates()) =
		    ((data->getActionValue(i) - min) / width) * 2 - 1.0;
	}
}

void QFunctionFromGradientFunction::getBestContinuousAction(
    StateCollection *state, ContinuousActionData *actionData) {
	Action *staticAction = AbstractQFunction::getMax(state, staticActions);
	actionData->setData(staticAction->getActionData());
}

void QFunctionFromGradientFunction::updateCAValue(
    StateCollection *state, ContinuousActionData *data, RealType td) {
	this->localGradientFeatureBuffer->clear();

	getCAGradient(state, data, localGradientFeatureBuffer);

	updateGradient(localGradientFeatureBuffer, td);
}

void QFunctionFromGradientFunction::setCAValue(
    StateCollection *state, ContinuousActionData *data, RealType qValue) {
	updateCAValue(state, data, qValue - getCAValue(state, data));
}

RealType QFunctionFromGradientFunction::getCAValue(
    StateCollection *state, ContinuousActionData *data) {
	getInputSequence(input, state->getState(properties), data);

	gradientFunction->getFunctionValue(input, outputError);

	return outputError->operator[](0);
}

void QFunctionFromGradientFunction::getCAGradient(
    StateCollection *state, ContinuousActionData *data, FeatureList *gradient) {
	getInputSequence(input, state->getState(properties), data);
	outputError->operator[](0) = 1.0;

	gradientFunction->getGradient(input, outputError, gradient);
}

void QFunctionFromGradientFunction::updateWeights(
    FeatureList *gradientFeatures) {
	gradientFunction->updateWeights(gradientFeatures);
}

int QFunctionFromGradientFunction::getNumWeights() {
	return gradientFunction->getNumWeights();
}

void QFunctionFromGradientFunction::resetData() {
	gradientFunction->resetData();
}

void QFunctionFromGradientFunction::getWeights(RealType *weights) {
	gradientFunction->getWeights(weights);
}

void QFunctionFromGradientFunction::setWeights(RealType *parameters) {
	gradientFunction->setWeights(parameters);
}

} //namespace rlearner

#endif // USE_TORCH

