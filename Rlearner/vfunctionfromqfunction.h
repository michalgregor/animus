// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cvfunctionfromqfunction_H_
#define Rlearner_cvfunctionfromqfunction_H_

#include "vfunction.h"

namespace rlearner {

class AbstractQFunction;
class ActionSet;
class StateProperties;
class StateCollection;
class State;
class AbstractVETraces;

class OptimalVFunctionFromQFunction: public AbstractVFunction {
protected:
	AbstractQFunction *qFunction;
	ActionSet *availableActions;

public:
	virtual RealType getValue(StateCollection *state);
	virtual RealType getValue(State *state);

	virtual AbstractVETraces *getStandardETraces();

public:
	OptimalVFunctionFromQFunction& operator=(const OptimalVFunctionFromQFunction&) = delete;
	OptimalVFunctionFromQFunction(const OptimalVFunctionFromQFunction&) = delete;

	OptimalVFunctionFromQFunction(AbstractQFunction *qfunction,
	        StateProperties *properties);
	virtual ~OptimalVFunctionFromQFunction();
};

class VFunctionFromQFunction: public OptimalVFunctionFromQFunction {
protected:
	RealType *actionValues;
	StochasticPolicy *stochPolicy;

public:
	virtual RealType getValue(StateCollection *state);
	virtual RealType getValue(State *state);

	virtual StochasticPolicy *getPolicy();
	virtual void setPolicy(StochasticPolicy *policy);

public:
	VFunctionFromQFunction& operator=(const VFunctionFromQFunction&) = delete;
	VFunctionFromQFunction(const VFunctionFromQFunction&) = delete;

	VFunctionFromQFunction(AbstractQFunction *qfunction,
	        StochasticPolicy *stochPolicy, StateProperties *properties);
	virtual ~VFunctionFromQFunction();
};

} //namespace rlearner

#endif //Rlearner_cvfunctionfromqfunction_H_
