#include "mdp.h"
#include "ril_debug.h"
#include "episode.h"
#include "statecollection.h"

namespace rlearner {

SemiMDPSender::SemiMDPSender() :
	_listeners(), _index() {
}

SemiMDPSender::~SemiMDPSender() {}

SemiMDPSender::const_iterator SemiMDPSender::find(
    SemiMDPListener* listener) const {
	auto iter = _index.find(listener);
	if(iter == _index.end()) return end();
	else return iter->second;
}

SemiMDPSender::iterator SemiMDPSender::find(SemiMDPListener* listener) {
	auto iter = _index.find(listener);
	if(iter == _index.end()) return end();
	else return iter->second;
}

SemiMDPSender::iterator SemiMDPSender::insert(
    iterator position, const value_type& val) {
	if(_index.find(val) != _index.end()) throw TracedError_AlreadyExists(
	    "The specified listener is already added to the sender.");

	auto iter = _listeners.insert(position, val);
	_index.insert(std::make_pair(val, iter));

	return iter;
}

SemiMDPSender::iterator SemiMDPSender::erase(iterator position) {
	auto iter = _index.find(*position);
	if(iter != _index.end()) _index.erase(iter);

	return _listeners.erase(position);
}

SemiMDPSender::iterator SemiMDPSender::erase(iterator first, iterator last) {
	for(auto iter = first; iter != last; iter++) {
		auto pos = _index.find(*iter);
		if(pos != _index.end()) _index.erase(pos);
	}

	return _listeners.erase(first, last);
}

void SemiMDPSender::addSemiMDPListener(SemiMDPListener* listener) {
	insert(_listeners.end(), listener);
}

void SemiMDPSender::removeSemiMDPListener(SemiMDPListener* listener) {
	auto iter = find(listener);
	if(iter != end()) erase(iter);
}

bool SemiMDPSender::isListenerAdded(SemiMDPListener* listener) {
	return _index.find(listener) != _index.end();
}

void SemiMDPSender::startNewEpisode() {
	for(auto& listener : _listeners) {
		if(listener->enabled) {
			listener->newEpisode();
		}
	}
}

void SemiMDPSender::sendNextStep(
    StateCollection *lastState, Action *action, StateCollection *currentState) {
	int i = 0;
	clock_t ticks1, ticks2;

	for(auto it = _listeners.begin(); it != _listeners.end(); it++, i++) {
		if((*it)->enabled) {
			ticks1 = clock();
			(*it)->nextStep(lastState, action, currentState);
			ticks2 = clock();
			DebugPrint('t', "Time needed for listener %d: %d\n", i,
			    ticks2 - ticks1);
		}
	}
}

void SemiMDPSender::sendIntermediateStep(
    StateCollection *lastState, Action *action, StateCollection *currentState) {
	for(auto it = _listeners.begin(); it != _listeners.end(); it++) {
		(*it)->intermediateStep(lastState, action, currentState);
	}
}

SemiMarkovDecisionProcess::SemiMarkovDecisionProcess():
	DeterministicController(new ActionSet()),
	_currentSteps(0), _isFirstStep(true)
{
	this->lastAction = nullptr;
}

SemiMarkovDecisionProcess::~SemiMarkovDecisionProcess() {
	delete actions;
}

/**
 * For the intermediate steps within an extended action, all the states that
 * occurred while the extended action hasn't finished, are also sent as the
 * tuple Intermediate_State-Action-current_State. The duration of the extended
 * action gets also reduced in the intermediate steps. When the given action
 * is finished (only MultiStepAction has the ability not to be finished)
 * the step is sent to all listeners. The method also updates currentSteps.
 * @see SemiMDPListener
 **/
void SemiMarkovDecisionProcess::sendNextStep(
    StateCollection *lastState, Action *action, StateCollection *currentState
) {
	_currentSteps++;

	bool finished = true;
	int duration = 1;
	lastAction = action;

	// Action has finished ?
	if(action->isType(AF_MultistepAction)) {
		MultiStepActionData *multiAction =
		    dynamic_cast<MultiStepAction *>(action)->getMultiStepActionData();
		finished = multiAction->finished;
		// get Duration
		duration = multiAction->duration;

		if(action->isType(AF_PrimitiveAction)) {
			// if there was a multistep-primitiv action, the intermediate steps hasn't been
			// recognized, so update currentSteps
			_currentSteps += duration - 1;
		}
	}

	if(finished) {
		DeterministicController::nextStep(lastState, action, currentState);

		// No ExtendedAction, send normal Step
		SemiMDPSender::sendNextStep(lastState, action, currentState);
	}
}

Action* SemiMarkovDecisionProcess::getLastAction() {
	return lastAction;
}

void SemiMarkovDecisionProcess::addActions(ActionSet *actions) {
	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		addAction(*it);
	}
}

void SemiMarkovDecisionProcess::startNewEpisode() {
	DeterministicController::newEpisode();
	SemiMDPSender::startNewEpisode();
	_currentSteps = 0;
	_isFirstStep = true;
}

/**
 * Sends the next Step if the Hierarchical SMDP has executed an action (i.e.
 * it is in the hierarchical ActoinStack). Before it sends the step, the
 * executed Action from the SMDP is calculated and the new Tuple S-A-S is
 * sent to the Listeners.
 */
void HierarchicalSemiMarkovDecisionProcess::nextStep(
    StateCollection *oldState, HierarchicalStack *actionStack,
    StateCollection *newState) {
	Action *currentAction = getExecutedAction(actionStack);

	if(currentAction != nullptr) {
		if(_isFirstStep) {
			pastState->setStateCollection(oldState);
			_isFirstStep = false;
		}
		bool sendStep = !currentAction->isType(AF_ExtendedAction);
		if(!sendStep) {
			ExtendedAction *eAction =
			    dynamic_cast<ExtendedAction *>(currentAction);

			sendStep = eAction->getMultiStepActionData()->finished;
		}
		if(sendStep) {
			currentState->setStateCollection(newState);
			sendNextStep(currentAction);
			StateCollectionImpl *buffer = pastState;
			pastState = currentState;
			currentState = buffer;
		}
	}
	if(multiStepData->finished && _currentSteps > 0) {
		startNewEpisode();
	}
}

void SemiMarkovDecisionProcess::addAction(Action *action) {
	actions->add(action);
	actionDataSet->addActionData(action);
}

HierarchicalSemiMarkovDecisionProcess::HierarchicalSemiMarkovDecisionProcess(
    Episode *loggedEpisode) :
	    SemiMarkovDecisionProcess(),
	    StateModifiersObject(loggedEpisode->getStateProperties()) {
	this->currentEpisode = loggedEpisode;
	pastState = new StateCollectionImpl(currentEpisode->getStateProperties());
	currentState = new StateCollectionImpl(
	    currentEpisode->getStateProperties());

	addModifierList(&_modifierList);

	for(auto& list: currentEpisode->getModifierLists()) {
		addModifierList(list);
	}
}

HierarchicalSemiMarkovDecisionProcess::HierarchicalSemiMarkovDecisionProcess(
    StateProperties *modelProperties, const std::set<StateModifierList*>& modifierLists) :
	SemiMarkovDecisionProcess(), StateModifiersObject(modelProperties) {
	this->currentEpisode = nullptr;

	pastState = new StateCollectionImpl(modelProperties);
	currentState = new StateCollectionImpl(modelProperties);

	for(auto& list: modifierLists) {
		addModifierList(list);
	}
}

HierarchicalSemiMarkovDecisionProcess::~HierarchicalSemiMarkovDecisionProcess() {
	removeModifierList(&_modifierList);

	delete pastState;
	delete currentState;
}

void HierarchicalSemiMarkovDecisionProcess::addModifierList(
	StateModifierList* modifierList
) {
	pastState->addModifierList(modifierList);
	currentState->addModifierList(modifierList);
	StateModifiersObject::addModifierList(modifierList);
}

void HierarchicalSemiMarkovDecisionProcess::removeModifierList(
	StateModifierList* modifierList
) {
	pastState->removeModifierList(modifierList);
	currentState->removeModifierList(modifierList);
	StateModifiersObject::removeModifierList(modifierList);
}

void HierarchicalSemiMarkovDecisionProcess::addStateModifier(StateModifier* modifier) {
	_modifierList.addStateModifier(modifier);
}

void HierarchicalSemiMarkovDecisionProcess::removeStateModifier(StateModifier* modifier) {
	_modifierList.removeStateModifier(modifier);
}

void HierarchicalSemiMarkovDecisionProcess::sendNextStep(Action *action) {

	DeterministicController::nextStep(pastState, action, currentState);
	SemiMarkovDecisionProcess::sendNextStep(pastState, action, currentState);

	if(action->isType(AF_ExtendedAction)) {
		ExtendedAction *mAction = dynamic_cast<ExtendedAction *>(action);
		if(mAction->getMultiStepActionData()->finished
		    && mAction->sendIntermediateSteps && currentEpisode != nullptr) {
			// send the Intermediate Steps and the "RealType" Step of the ExtendedAction

			int oldDuration = mAction->getDuration();
			int episodeIndex = currentEpisode->getNumSteps() - 1;

			Action *interAction = currentEpisode->getAction(episodeIndex);

			// set new duration of the extendedAction
			mAction->getMultiStepActionData()->duration =
			    interAction->getDuration();

			// Send intermediate Steps
			if(mAction->sendIntermediateSteps) {

				interAction = currentEpisode->getAction(episodeIndex);

				// set new duration of the extendedAction
				mAction->getMultiStepActionData()->duration =
				    interAction->getDuration();

				while(mAction->getMultiStepActionData()->duration < oldDuration) {
					assert(episodeIndex > 0);

					currentEpisode->getStateCollection(episodeIndex, pastState);
					SemiMDPSender::sendIntermediateStep(pastState, mAction,
					    currentState);

					episodeIndex--;

					// set new duration of the extendedAction
					interAction = currentEpisode->getAction(episodeIndex);
					mAction->getMultiStepActionData()->duration +=
					    interAction->getDuration();
				}
			}

			assert(mAction->getDuration() == oldDuration);
		}
	}
}

void HierarchicalSemiMarkovDecisionProcess::setLoggedEpisode(
    Episode *loggedEpisode) {
	currentEpisode = loggedEpisode;
}

void HierarchicalSemiMarkovDecisionProcess::newEpisode() {
	startNewEpisode();
}

Action* HierarchicalSemiMarkovDecisionProcess::getNextHierarchyLevel(
    StateCollection *state, ActionDataSet *actionDataSet) {
	return getNextAction(state, actionDataSet);
}

/**
 * Returns the action following the SMDP in the hierarchical Action Stack.
 **/
Action *HierarchicalSemiMarkovDecisionProcess::getExecutedAction(
    HierarchicalStack *actionStack) {
	HierarchicalStack::iterator it = actionStack->begin();
	while(it != actionStack->end() && (*it) != this) {
		it++;
	}
	if(it == actionStack->end()) {
		return nullptr;
	} else {
		it++;
		assert(it != actionStack->end());
		return *it;
	}
}

} //namespace rlearner
