// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <ctime>
#include <cstdio>
#include <map>
#include <cmath>
#include <cstring>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

#include "policies.h"
#include "agent.h"
#include "ril_debug.h"
#include "tdlearner.h"
#include "linearfafeaturecalculator.h"
#include "vfunctionlearner.h"
#include "rewardmodel.h"
#include "continuoustime.h"
#include "testsuit.h"
#include "vfunction.h"
#include "parameters.h"
#include "agentlogger.h"
#include "exploration.h"
#include "batchlearning.h"
#include "residuals.h"
#include "analyzer.h"
#include "evaluator.h"
#include "policygradient.h"
#include "vfunctionfromqfunction.h"
#include "samplingbasedmodel.h"

#include "state.h"
#include "stateproperties.h"
#include "statecollection.h"
#include "AgentSimulator.h"

namespace rlearner {

TestSuiteEvaluatorLogger::TestSuiteEvaluatorLogger(
    std::string l_outputDirectory
):
	outputDirectory(l_outputDirectory)
{
	nEpisodesBeforeEvaluate = 1;
}

void TestSuiteEvaluatorLogger::setOutputDirectory(
    std::string l_outputDirectory
) {
	outputDirectory = std::string(l_outputDirectory);
}

void TestSuiteEvaluatorLogger::startNewEvaluation(
    std::string, Parameters *, int) {}

TestSuiteLoggerFromEvaluator::TestSuiteLoggerFromEvaluator(
    std::string outputDirectory, std::string l_outputFileName,
    Evaluator *l_evaluator) :
	TestSuiteEvaluatorLogger(outputDirectory), outputFileName(l_outputFileName) {
	evaluator = l_evaluator;
}

void TestSuiteLoggerFromEvaluator::startNewEvaluation(
    std::string evaluationDirectory, Parameters *, int trial
) {
	std::stringstream sstream;

	sstream << evaluationDirectory << outputDirectory << "/" << outputFileName
	    << "_" << trial << ".data";
	std::string filename = sstream.str();

	std::ofstream file(filename);

	if(!file.good()) {
		std::cout << "Couldn't create file " << filename <<
			" ... please create the directory !!" << std::endl;
	}

	file << std::endl;
}

RealType TestSuiteLoggerFromEvaluator::evaluateValue(
    std::string evaluationDir, int trial, int numEpisode
) {
	std::stringstream sstream;

	sstream << evaluationDir << outputDirectory << "/" << outputFileName << "_"
	    << trial << ".data";
	std::string filename = sstream.str();

	RealType value = evaluator->evaluate();

	std::ofstream file(filename, std::ofstream::app);
	fmt::fprintf(file, "%d %f\n", numEpisode, value);

	return value;
}

void TestSuiteLoggerFromEvaluator::evaluate(
    std::string evaluationDir, int trial, int numEpisodes) {
	evaluateValue(evaluationDir, trial, numEpisodes);
}

MatlabEpisodeOutputLogger::MatlabEpisodeOutputLogger(
	AgentSimulator* l_agentSimulator, RewardFunction *l_rewardFunction,
    StateProperties *l_modifier, ActionSet *l_actions, int nEpisodes,
    int nSteps) :
	TestSuiteEvaluatorLogger("Episodes") {
	agentSimulator = l_agentSimulator;

	actions = l_actions;
	modifier = l_modifier;

	this->nEpisodes = nEpisodes;
	this->nSteps = nSteps;

	rewardFunction = l_rewardFunction;
}

MatlabEpisodeOutputLogger::~MatlabEpisodeOutputLogger() {}

void MatlabEpisodeOutputLogger::evaluate(
    std::string evaluationDir, int trial, int numEpisode) {

	int numEval = numEpisode / nEpisodesBeforeEvaluate;
	printf("Episode Ouput numEval %d\n", numEval);

	std::stringstream sstream;

	sstream << evaluationDir << outputDirectory << "/" << "Trial" << trial
	    << "/Episodes" << "_" << numEval << ".data";
	std::string filename = sstream.str();

	auto file = make_shared<std::ofstream>(filename);

	if(!file->good()) {
		std::cout << "Couldn't create file " << filename <<
			" ... please create the directory !!" << std::endl;
	}

	EpisodeMatlabOutput* output = new EpisodeMatlabOutput(modifier,
	    rewardFunction, actions, file);

	output->setNumEpisodes(numEpisode);
	agentSimulator->getAgent()->addSemiMDPListener(output);

	for(int i = 0; i < nEpisodes; i++) {
		agentSimulator->startNewEpisode();
		agentSimulator->doControllerEpisode(nSteps);
	}
	agentSimulator->getAgent()->removeSemiMDPListener(output);

	delete output;
}

void MatlabEpisodeOutputLogger::startNewEvaluation(
    std::string evaluationDirectory, Parameters *, int trial
) {
	std::stringstream sstream;
	sstream << "mkdir " << evaluationDirectory << outputDirectory << "/"
	    << "Trial" << trial;
	std::string scall = sstream.str();
	system(scall.c_str());
}

MatlabVAnalyzerLogger::MatlabVAnalyzerLogger(
    AbstractVFunction *l_vFunction, FeatureCalculator *featCalc,
    ErrorSender *l_vLearner, StateList *l_states, int l_dim1, int l_dim2,
    int l_part1, int l_part2,
    const std::set<StateModifierList*>& modifierLists
):
	TestSuiteEvaluatorLogger("Analyzer"),
	_modifierLists(modifierLists)
{
	vFunction = l_vFunction;
	vLearner = l_vLearner;

	visitCounter = new FeatureVFunction(featCalc);
	averageError = new FeatureVFunction(featCalc);
	averageVariance = new FeatureVFunction(featCalc);

	visitCounterLearner = new VisitStateCounter(visitCounter);

	errorLearner = new VAverageTDErrorLearner(averageError, 0.99);
	varianceLearner = new VAverageTDVarianceLearner(averageVariance, 0.99);

	vLearner->addErrorListener(errorLearner);
	vLearner->addErrorListener(varianceLearner);

	dim1 = l_dim1;
	dim2 = l_dim2;

	part1 = l_part1;
	part2 = l_part2;

	states = l_states;
}

MatlabVAnalyzerLogger::~MatlabVAnalyzerLogger() {
	delete visitCounter;
	delete averageError;
	delete averageVariance;
	delete visitCounterLearner;
	delete errorLearner;
	delete varianceLearner;

	vLearner->removeErrorListener(errorLearner);
	vLearner->removeErrorListener(varianceLearner);
}

void MatlabVAnalyzerLogger::evaluate(
    std::string evaluationDir, int trial, int numEpisode
) {
	int numEval = numEpisode / nEpisodesBeforeEvaluate;

	StateProperties *properties = states->getStateProperties();
	State modelState(properties);

	VFunctionAnalyzer *analyzer = new VFunctionAnalyzer(vFunction, properties,
	    _modifierLists);

	for(unsigned int j = 0; j < states->getNumStates(); j++) {
		states->getState(j, &modelState);
		std::string filename;

		filename = evaluationDir + outputDirectory + "/Trial" + std::to_string(trial) +
			"/vFunction_State" + std::to_string(j) + "_" + std::to_string(numEval) + ".data";

		analyzer->save2DValues(filename, &modelState, dim1, part1, dim2, part2);

		filename = evaluationDir + outputDirectory + "/Trial" + std::to_string(trial) +
			"/StateVisits_State" + std::to_string(j) + "_" + std::to_string(numEval) + ".data";

		analyzer->setVFunction(visitCounter);
		analyzer->save2DValues(filename, &modelState, dim1, part1, dim2, part2);

		filename = evaluationDir + outputDirectory + "/Trial" + std::to_string(trial) +
			"/AverageError_State" + std::to_string(j) + "_" + std::to_string(numEval) + ".data";

		analyzer->setVFunction(averageError);
		analyzer->save2DValues(filename, &modelState, dim1, part1, dim2, part2);

		filename = evaluationDir + outputDirectory + "/Trial" + std::to_string(trial) +
			"/AverageVariance_State" + std::to_string(j) + "_" + std::to_string(numEval) + ".data";

		analyzer->setVFunction(averageVariance);
		analyzer->save2DValues(filename, &modelState, dim1, part1, dim2, part2);
	}

	delete analyzer;
}

void MatlabVAnalyzerLogger::startNewEvaluation(
    std::string evaluationDirectory, Parameters *, int trial) {
	visitCounter->resetData();
	averageError->resetData();
	averageVariance->resetData();

	std::stringstream sstream;

	sstream << "mkdir " << evaluationDirectory << outputDirectory << "/"
	    << "Trial" << trial;
	std::string scall = sstream.str();
	system(scall.c_str());
}

void MatlabVAnalyzerLogger::addListenersToAgent(SemiMDPSender *agent) {
	agent->addSemiMDPListener(visitCounterLearner);
}

void MatlabVAnalyzerLogger::removeListenersToAgent(SemiMDPSender *agent) {
	agent->removeSemiMDPListener(visitCounterLearner);
}

MatlabQAnalyzerLogger::MatlabQAnalyzerLogger(
    FeatureQFunction *l_qFunction, FeatureCalculator *featCalc,
    ErrorSender *l_vLearner, StateList *l_states, int l_dim1, int l_dim2,
    int l_part1, int l_part2, const std::set<StateModifierList*>& modifierLists
):
	MatlabVAnalyzerLogger(
		new OptimalVFunctionFromQFunction(l_qFunction, featCalc), featCalc,
		l_vLearner, l_states, l_dim1, l_dim2, l_part1, l_part2, modifierLists)
{
	qFunction = l_qFunction;
	saVisits = new FeatureQFunction(qFunction->getActions(), featCalc);
	visitStateActionCounterLearner = new VisitStateActionCounter(saVisits);
}

MatlabQAnalyzerLogger::MatlabQAnalyzerLogger(
    FeatureVFunction *vFunction, FeatureQFunction *l_qFunction,
    FeatureCalculator *featCalc, ErrorSender *l_vLearner, StateList *l_states,
    int l_dim1, int l_dim2, int l_part1, int l_part2,
    const std::set<StateModifierList*>& modifierLists
):
	MatlabVAnalyzerLogger(vFunction, featCalc, l_vLearner, l_states, l_dim1,
		l_dim2, l_part1, l_part2, modifierLists)
{
	qFunction = l_qFunction;
	saVisits = new FeatureQFunction(qFunction->getActions(), featCalc);
	visitStateActionCounterLearner = new VisitStateActionCounter(saVisits);
}

MatlabQAnalyzerLogger::~MatlabQAnalyzerLogger() {
	delete vFunction;
	delete saVisits;
	delete visitStateActionCounterLearner;
}

void MatlabQAnalyzerLogger::evaluate(
    std::string evaluationDir, int trial, int numEpisode
) {
	MatlabVAnalyzerLogger::evaluate(evaluationDir, trial, numEpisode);
	int numEval = numEpisode / nEpisodesBeforeEvaluate;

	StateProperties *properties = states->getStateProperties();
	State modelState(properties);

	QFunctionAnalyzer *analyzer = new QFunctionAnalyzer(qFunction, properties,
		_modifierLists);

	for(unsigned int j = 0; j < states->getNumStates(); j++) {
		states->getState(j, &modelState);

		std::string filename;

		filename = evaluationDir + outputDirectory + "/Trial" + std::to_string(trial) +
			"/qFunction_State" + std::to_string(j) + "_" + std::to_string(numEval) + ".data";

		analyzer->save2DValues(filename, qFunction->getActions(), &modelState,
		    dim1, part1, dim2, part2);

		filename = evaluationDir + outputDirectory + "/Trial" + std::to_string(trial) +
			"/StateActionVisits_State" + std::to_string(j) + "_" + std::to_string(numEval) + ".data";

		analyzer->setQFunction(saVisits);
		analyzer->save2DValues(filename, qFunction->getActions(), &modelState,
		    dim1, part1, dim2, part2);

	}

	saVisits->saveDataToFile("Analyzer/debug.data");
	qFunction->saveDataToFile("Analyzer/debug1.data");
	vFunction->saveDataToFile("Analyzer/debug2.data");
	visitCounter->saveDataToFile("Analyzer/debug3.data");

	delete analyzer;
}

void MatlabQAnalyzerLogger::startNewEvaluation(
    std::string evaluationDirectory, Parameters *parameters, int trial) {
	saVisits->resetData();
	MatlabVAnalyzerLogger::startNewEvaluation(evaluationDirectory, parameters,
	    trial);
}

void MatlabQAnalyzerLogger::addListenersToAgent(SemiMDPSender *agent) {
	MatlabVAnalyzerLogger::addListenersToAgent(agent);
	agent->addSemiMDPListener(visitStateActionCounterLearner);
}

void MatlabQAnalyzerLogger::removeListenersToAgent(SemiMDPSender *agent) {
	MatlabVAnalyzerLogger::removeListenersToAgent(agent);
	agent->removeSemiMDPListener(visitStateActionCounterLearner);
}

TestSuite::TestSuite(
	AgentSimulator* agentSimulator, AgentController *controller,
	LearnDataObject *learnDataObject, char *l_testSuiteName) :
	testSuiteName(l_testSuiteName)
{
	this->agentSimulator = agentSimulator;
	this->learnDataObjects = new std::list<LearnDataObject *>();
	saveLearnData = new std::map<LearnDataObject *, bool>();

	paramCalculators = new std::list<AdaptiveParameterCalculator *>();

	if(learnDataObject) {
		addLearnDataObject(learnDataObject, true);
	}

	addParameters(controller);

	this->controller = controller;
	this->evaluationController = controller;
}

TestSuite::TestSuite(
	AgentSimulator* agentSimulator, AgentController *controller,
    AgentController *evaluationController, LearnDataObject *learnDataObject,
    char *l_testSuiteName) :
	testSuiteName(l_testSuiteName) {
	this->agentSimulator = agentSimulator;
	this->learnDataObjects = new std::list<LearnDataObject *>();
	saveLearnData = new std::map<LearnDataObject *, bool>();

	paramCalculators = new std::list<AdaptiveParameterCalculator *>();

	if(learnDataObject) {
		addLearnDataObject(learnDataObject, true);
	}

	addParameters(controller);
	addParameters(evaluationController);

	this->controller = controller;
	this->evaluationController = evaluationController;
}

TestSuite::~TestSuite() {
	delete learnDataObjects;
	delete saveLearnData;
	delete paramCalculators;
}

void TestSuite::deleteObjects() {
	if(evaluationController != controller) {
		delete evaluationController;
	}

	delete controller;

	std::list<LearnDataObject *>::iterator it = learnDataObjects->begin();
	for(; it != learnDataObjects->end(); it++) {
		delete *it;
	}
}

void TestSuite::addParamCalculator(
    AdaptiveParameterCalculator *paramCalculator) {
	addParameters(paramCalculator);
	paramCalculators->push_back(paramCalculator);
}

void TestSuite::resetParamCalculators() {
	std::list<AdaptiveParameterCalculator *>::iterator it =
	    paramCalculators->begin();

	for(; it != paramCalculators->end(); it++) {
		(*it)->resetCalculator();
	}
}

AgentController *TestSuite::getEvaluationController() {
	return evaluationController;
}

void TestSuite::setEvaluationController(
    AgentController *l_evaluationController) {
	this->evaluationController = l_evaluationController;
}

void TestSuite::saveLearnedData(std::ostream& stream) {
	std::list<LearnDataObject *>::iterator it = learnDataObjects->begin();

	for(; it != learnDataObjects->end(); it++) {
		if((*saveLearnData)[*it]) {
			(*it)->saveData(stream);
		}
	}
}

void TestSuite::loadLearnedData(std::istream& stream) {
	std::list<LearnDataObject *>::iterator it = learnDataObjects->begin();

	for(; it != learnDataObjects->end(); it++) {
		if((*saveLearnData)[*it]) {
			(*it)->loadData(stream);
		}
	}
}

void TestSuite::resetLearnedData() {
	std::list<LearnDataObject *>::iterator it = learnDataObjects->begin();

	for(; it != learnDataObjects->end(); it++) {
		(*it)->resetData();
	}

	resetParamCalculators();
}

std::string TestSuite::getTestSuiteName() {
	return testSuiteName;
}

void TestSuite::setTestSuiteName(std::string name) {
	testSuiteName = name;
}

void TestSuite::addLearnDataObject(
    LearnDataObject *learnDataObject, bool l_saveLearnData) {
	learnDataObjects->push_back(learnDataObject);
	(*saveLearnData)[learnDataObject] = l_saveLearnData;
}

AgentController *TestSuite::getController() {
	return controller;
}

void TestSuite::setController(AgentController *controller) {
	this->controller = controller;
}

ListenerTestSuite::ListenerTestSuite(
	AgentSimulator* agentSimulator, SemiMDPListener *learner, AgentController *controller,
    LearnDataObject *vFunction, char *testSuiteName) :
	TestSuite(agentSimulator, controller, vFunction, testSuiteName) {
	this->learnerObjects = new std::list<SemiMDPListener *>();
	addToAgent = new std::map<SemiMDPListener *, SemiMarkovDecisionProcess *>();

	if(learner != nullptr) {
		learnerObjects->push_back(learner);
		addParameters(learner);
	}
}

ListenerTestSuite::ListenerTestSuite(
	AgentSimulator* agentSimulator, SemiMDPListener *learner, AgentController *controller,
    AgentController *evaluationController, LearnDataObject *vFunction,
    char *testSuiteName) :
	TestSuite(agentSimulator, controller, evaluationController, vFunction, testSuiteName) {
	this->learnerObjects = new std::list<SemiMDPListener *>();
	addToAgent = new std::map<SemiMDPListener *, SemiMarkovDecisionProcess *>();

	if(learner != nullptr) {
		learnerObjects->push_back(learner);
		addParameters(learner);
	}
}

ListenerTestSuite::~ListenerTestSuite() {
	delete learnerObjects;
	delete addToAgent;
}

void ListenerTestSuite::deleteObjects() {
	std::list<SemiMDPListener *>::iterator it = learnerObjects->begin();
	for(; it != learnerObjects->end(); it++) {
		delete *it;
	}
}

void ListenerTestSuite::addLearnerObject(
    SemiMDPListener *listener, bool addParams, bool addBack,
    SemiMarkovDecisionProcess *addAgent) {
	if(addParams) {
		addParameters(listener);
	}

	if(addBack) {
		learnerObjects->push_back(listener);
		(*addToAgent)[listener] = addAgent;
	} else {
		learnerObjects->push_front(listener);
		(*addToAgent)[listener] = addAgent;
	}

}

void ListenerTestSuite::addLearnersToAgent() {
	std::list<SemiMDPListener *>::iterator it = learnerObjects->begin();

	for(; it != learnerObjects->end(); it++) {
		(*it)->enabled = true;
		if((*addToAgent)[(*it)] == nullptr) {
			agentSimulator->getAgent()->addSemiMDPListener(*it);
		} else {
			(*addToAgent)[(*it)]->addSemiMDPListener(*it);
		}
	}
}

void ListenerTestSuite::removeLearnersFromAgent() {
	std::list<SemiMDPListener *>::iterator it = learnerObjects->begin();

	for(; it != learnerObjects->end(); it++) {
		if((*addToAgent)[(*it)] == nullptr) {
			agentSimulator->getAgent()->removeSemiMDPListener(*it);
		} else {
			(*addToAgent)[(*it)]->removeSemiMDPListener(*it);
		}
		//(*it)->enabled = false;
	}
}

void ListenerTestSuite::learn(int nEpisodes, int nStepsPerEpisode) {
	addLearnersToAgent();
	agentSimulator->getAgent()->setController(controller);

	for(int i = 0; i < nEpisodes; i++) {
		agentSimulator->startNewEpisode();
		agentSimulator->doControllerEpisode(nStepsPerEpisode);
	}

	removeLearnersFromAgent();
}

PolicyEvaluationTestSuite::PolicyEvaluationTestSuite(
	AgentSimulator* agentSimulator, PolicyEvaluation *learner,
	AgentController *controller, LearnDataObject *vFunction, char *testSuiteName) :
	TestSuite(agentSimulator, controller, vFunction, testSuiteName) {
	evaluation = learner;
	addParameters(evaluation);
}

PolicyEvaluationTestSuite::~PolicyEvaluationTestSuite() {}

void PolicyEvaluationTestSuite::learn(int, int) {
	agentSimulator->getAgent()->setController(controller);
	evaluation->evaluatePolicy();
}

void PolicyEvaluationTestSuite::resetLearnedData() {
	TestSuite::resetLearnedData();
	evaluation->resetLearnData();
}

PolicyIterationTestSuite::PolicyIterationTestSuite(
	AgentSimulator* agentSimulator, PolicyIteration *learner, AgentController *controller,
    LearnDataObject *vFunction, char *testSuiteName) :
	TestSuite(agentSimulator, controller, vFunction, testSuiteName) {
	policyIteration = learner;
	addParameters(policyIteration);
}

PolicyIterationTestSuite::~PolicyIterationTestSuite() {}

void PolicyIterationTestSuite::learn(int, int) {
	agentSimulator->getAgent()->setController(controller);
	policyIteration->doPolicyIterationStep();
}

void PolicyIterationTestSuite::resetLearnedData() {
	TestSuite::resetLearnedData();
	policyIteration->initPolicyIteration();
}

PolicyGradientTestSuite::PolicyGradientTestSuite(
	AgentSimulator* agentSimulator, GradientLearner *learner, AgentController *controller,
    LearnDataObject *vFunction, char *testSuiteName, int maxGradientUpdates) :
	TestSuite(agentSimulator, controller, vFunction, testSuiteName) {
	this->learner = learner;

	addParameters(learner);
	addParameter("MaxGradientUpdates", maxGradientUpdates);
}

PolicyGradientTestSuite::PolicyGradientTestSuite(
	AgentSimulator* agentSimulator, GradientLearner *learner, AgentController *controller,
    AgentController *evaluationController, LearnDataObject *vFunction,
    char *testSuiteName, int maxGradientUpdates) :
	TestSuite(agentSimulator, controller, evaluationController, vFunction, testSuiteName) {
	this->learner = learner;

	addParameters(learner);
	addParameter("MaxGradientUpdates", maxGradientUpdates);
}

PolicyGradientTestSuite::~PolicyGradientTestSuite() {}

void PolicyGradientTestSuite::deleteObjects() {
	if(learner) {
		delete learner;
	}
}

void PolicyGradientTestSuite::learn(int, int) {
	agentSimulator->getAgent()->setController(controller);
	learner->doOptimization((int) getParameter("MaxGradientUpdates"));
}

void PolicyGradientTestSuite::resetLearnedData() {
	TestSuite::resetLearnedData();

	learner->resetOptimization();
}

TestSuiteCollection::TestSuiteCollection() {
	testSuiteMap = new std::map<std::string, TestSuite *>();
	objectsToDelete = new std::list<void *>();
}

TestSuiteCollection::~TestSuiteCollection() {
	std::map<std::string, TestSuite *>::iterator it = testSuiteMap->begin();
	for(; it != testSuiteMap->end(); it++) {
		delete (*it).second;
	}

	deleteObjects();
	delete objectsToDelete;
	delete testSuiteMap;
}

void TestSuiteCollection::addObjectToDelete(void *object) {
	objectsToDelete->push_back(object);
}

void TestSuiteCollection::deleteObjects() {
	std::list<void *>::iterator it = objectsToDelete->begin();
	/*for (; it != objectsToDelete->end(); it ++)
	 {
	 delete (*it);
	 }*/
}

void TestSuiteCollection::addTestSuite(TestSuite *testSuite) {
	(*testSuiteMap)[testSuite->getTestSuiteName()] = testSuite;
}

void TestSuiteCollection::removeTestSuite(TestSuite *testSuite) {
	(*testSuiteMap)[testSuite->getTestSuiteName()] = nullptr;
}

int TestSuiteCollection::getNumTestSuites() {
	return testSuiteMap->size();
}

TestSuite *TestSuiteCollection::getTestSuite(std::string testSuiteName) {
	return (*testSuiteMap)[testSuiteName];
}

TestSuite *TestSuiteCollection::getTestSuite(int index) {
	TestSuite *testSuite = nullptr;
	std::map<std::string, TestSuite *>::iterator it = testSuiteMap->begin();

	for(int i = 0; it != testSuiteMap->end(); it++, i++) {
		if(i == index) {
			testSuite = (*it).second;
			break;
		}
	}

	return testSuite;
}

void TestSuiteCollection::removeAllTestSuites() {
	std::map<std::string, TestSuite *>::iterator it = testSuiteMap->begin();

	for(int i = 0; it != testSuiteMap->end(); it++, i++) {
		TestSuite *testSuite = (*it).second;
		delete testSuite;
	}

	testSuiteMap->clear();
}

TestSuiteEvaluator::TestSuiteEvaluator(
	AgentSimulator* l_agentSimulator, std::string l_baseDirectory, TestSuite *l_testSuite,
    int l_nTrials) :
	baseDirectory(l_baseDirectory) {

	agentSimulator = l_agentSimulator;
	nTrials = l_nTrials;

	testSuite = l_testSuite;

	evaluators = new std::list<TestSuiteEvaluatorLogger *>();

	trialNumber = 0;

	parameterList = new std::list<Parameters *>();
	evaluations = new std::map<Parameters *, EvaluationValues *>();

	printf("Base %s\n", baseDirectory.c_str());
	std::string fileName = baseDirectory + std::string("/")
	    + testSuite->getTestSuiteName() + std::string("/EvaluationData.data");
//	loadEvaluationData(fileName);

	addParameter("TestSuiteEvaluator", 0.0);
}

TestSuiteEvaluator::~TestSuiteEvaluator() {
	std::map<Parameters *, EvaluationValues *>::iterator it =
	    evaluations->begin();

	for(; it != evaluations->end(); it++) {
		delete (*it).second;
	}

	std::list<Parameters *>::iterator it2 = parameterList->begin();
	for(; it2 != parameterList->end(); it2++) {
		delete *it2;
	}

	delete evaluators;
	delete parameterList;
	delete evaluations;
}

void TestSuiteEvaluator::evaluateParameters(Parameters *testSuiteParam) {
	Parameters *targetParams = getParametersObject(testSuiteParam);
	EvaluationValues *evalValues = nullptr;

	if(targetParams != nullptr) {
		evalValues = (*evaluations)[targetParams];
	} else {
		targetParams = new Parameters(*testSuiteParam);
		targetParams->addParameters(this);

		parameterList->push_back(targetParams);

		evalValues = new EvaluationValues();
		(*evaluations)[targetParams] = evalValues;
	}

	printf("Evaluating Parameter set:\n");
	targetParams->saveParameters(std::cout);
	printf("Found %lu Trials... Need to evaluate %lu Trials\n",
	    evalValues->size(), nTrials - evalValues->size());

	while(evalValues->size() < nTrials) {
		EvaluationValue value;
		std::stringstream sstream;
		value.trialNumber = getNewTrialNumber();

		doEvaluationTrial(testSuite, &value);
		evalValues->push_back(value);
		std::string fileName = baseDirectory + std::string("/")
		    + testSuite->getTestSuiteName()
		    + std::string("/EvaluationData.data");
//		saveEvaluationData(fileName);

		printf("Evaluation of Trial %d finished: Av: %f, Best %f\n",
		    value.trialNumber, value.averageValue, value.bestValue);
	}
}

std::string TestSuiteEvaluator::getEvaluationDirectory() {
	return baseDirectory + std::string("/") + testSuite->getTestSuiteName()
	    + std::string("/");
}

int TestSuiteEvaluator::getNewTrialNumber() {
	trialNumber++;
	return trialNumber - 1;
}

std::string TestSuiteEvaluator::getLearnDataFileName(int trialNumber) {
	char filename[500];
	sprintf(filename, "%s/%s/LearnData/Trial_%d.data", baseDirectory.c_str(),
	    testSuite->getTestSuiteName().c_str(), trialNumber);
	return std::string(filename);
}

void TestSuiteEvaluator::saveEvaluationDataMatlab(std::string filename) {
	std::ofstream parameterFile(filename);

	if(!parameterFile.good()) {
		std::cout << "Could not create file " << filename << std::endl;
	}

	std::list<Parameters*>::iterator it = parameterList->begin();

	for(int nParam = 1; it != parameterList->end(); it++, nParam++) {
		for(int j = 0; j < (*it)->getNumParameters(); j++) {
			fmt::fprintf(parameterFile,
			    "parameterSet{%d}.parameters{%d} = {'%s', %f};\n", nParam,
			    j + 1, (*it)->getParameterName(j).c_str(),
			    (*it)->getParameterFromIndex(j));
		}

		Parameters *parameters = *it;
		EvaluationValues *values = (*evaluations)[parameters];

		EvaluationValues::iterator itValues = values->begin();
		for(int j = 1; itValues != values->end(); itValues++, j++) {
			fmt::fprintf(parameterFile, "parameterSet{%d}.trials(%d) = %d;\n",
			    nParam, j, (*itValues).trialNumber + 1);

			fmt::fprintf(parameterFile, "trials{%d}.average = %f;\n",
			    (*itValues).trialNumber + 1, (*itValues).averageValue);
			fmt::fprintf(parameterFile, "trials{%d}.bestValue = %f;\n",
			    (*itValues).trialNumber + 1, (*itValues).bestValue);
			fmt::fprintf(parameterFile, "trials{%d}.time = %f;\n",
			    (*itValues).trialNumber + 1, (*itValues).evaluationTime);
			fmt::fprintf(parameterFile, "trials{%d}.parameterSet = %d;\n",
			    (*itValues).trialNumber + 1, nParam);
		}
	}
}

void TestSuiteEvaluator::doEvaluationTrial(
    Parameters *parameters, EvaluationValue *evaluationData) {
	testSuite->setParameters(parameters);
	testSuite->resetLearnedData();

	std::string learnDataFileName = getLearnDataFileName(
	    evaluationData->trialNumber);

	time_t startTime;
	time_t endTime;

	printf("Beginning to Evaluate Trial number %d (%d) ... LearnDataFile %s\n",
	    evaluationData->trialNumber, trialNumber, learnDataFileName.c_str());

	newEvaluationTrial(testSuite, evaluationData);

	std::list<TestSuiteEvaluatorLogger *>::iterator it = evaluators->begin();
	std::string evaluationDirectory = getEvaluationDirectory();

	for(; it != evaluators->end(); it++) {
		(*it)->startNewEvaluation(evaluationDirectory, testSuite,
		    evaluationData->trialNumber);
	}

	try {
		startTime = time(nullptr);
		int nEpisode = 0;

		while(!isFinished(nEpisode)) {
			nEpisode++;
			int actualSteps = agentSimulator->getTotalSteps();
			doEpisode(testSuite, nEpisode);

			if(agentSimulator->getEnvironmentModel()->isFailed()) {
				printf("Finished Episode %d  (%d steps, failed : ", nEpisode,
					agentSimulator->getTotalSteps() - actualSteps);
				agentSimulator->getCurrentState()->getState()->save(std::cout);
				printf(")\n");
			} else {
				printf("Finished Episode %d (%d steps, succeded :", nEpisode,
					agentSimulator->getTotalSteps() - actualSteps);
				agentSimulator->getCurrentState()->getState()->save(std::cout);
				printf(")\n");
			}

			std::list<TestSuiteEvaluatorLogger *>::iterator it =
			    evaluators->begin();

			for(; it != evaluators->end(); it++) {
				if(nEpisode % (*it)->nEpisodesBeforeEvaluate == 0) {
					(*it)->evaluate(evaluationDirectory,
					    evaluationData->trialNumber, nEpisode);
				}
			}
		}

		it = evaluators->begin();
		for(; it != evaluators->end(); it++) {
			(*it)->endEvaluation();
		}

	} catch(TracedError& E) {
		printf("EXCEPTION: ");
		printf(E.what());

		evaluationData->averageValue = -100000000;
		evaluationData->bestValue = -100000000;

		for(; it != evaluators->end(); it++) {
			(*it)->endEvaluation();
		}
	}
	endTime = time(nullptr);
	getEvaluationValue(evaluationData);
	evaluationData->evaluationTime = difftime(endTime, startTime);

	char *time = ctime(&endTime);
	char buffTime[50];
	sprintf(buffTime, "%s", time);

	buffTime[strlen(buffTime) - 1] = 0;

	evaluationData->evaluationDate = std::string(buffTime);
	std::ofstream dataFILE(learnDataFileName);

	if(!dataFILE.good()) {
		std::cout << "Could not create file " << learnDataFileName << std::endl;
	} else {
		std::cout << "Storing Learndata in file " << learnDataFileName << std::endl;
	}

	testSuite->saveLearnedData(dataFILE);
}

void TestSuiteEvaluator::addPolicyEvaluator(
    TestSuiteEvaluatorLogger *evaluator) {
	evaluators->push_back(evaluator);
}

Parameters *TestSuiteEvaluator::getParametersObject(Parameters *parameters) {
	Parameters tempParams(*parameters);
	tempParams.addParameters(this);

	std::list<Parameters *>::iterator it = parameterList->begin();

	for(; it != parameterList->end(); it++) {
		if(*(*it) == tempParams) {
			break;
		}
	}

	if(it != parameterList->end()) {
		return *it;
	} else {
		return nullptr;
	}
}

RealType TestSuiteEvaluator::getAverageValue(Parameters *testSuite) {
	Parameters *parameters = getParametersObject(testSuite);
	RealType average = 0.0;
	if(parameters != nullptr) {
		EvaluationValues *values = (*evaluations)[parameters];
		EvaluationValues::iterator itVal = values->begin();

		for(; itVal != values->end(); itVal++) {
			average += (*itVal).averageValue;
		}

		average = average / values->size();
	}

	return average;
}

RealType TestSuiteEvaluator::getBestValue(Parameters *testSuite) {
	Parameters *parameters = getParametersObject(testSuite);
	RealType best = 0.0;
	if(parameters != nullptr) {
		EvaluationValues *values = (*evaluations)[parameters];
		EvaluationValues::iterator itVal = values->begin();

		for(int i = 0; itVal != values->end(); itVal++, i++) {
			if(i == 0 || best < (*itVal).bestValue) {
				best = (*itVal).bestValue;
			}
		}
	}

	return best;
}

EvaluationValues *TestSuiteEvaluator::getEvaluationValues() {
	Parameters *parameters = getParametersObject(testSuite);

	if(parameters != nullptr) {
		return (*evaluations)[parameters];
	} else {
		return nullptr;
	}
}

void AverageRewardTestSuiteEvaluator::newEvaluationTrial(
    TestSuite *testSuite, EvaluationValue *evaluationData) {
	numEvals = 1;

	char filename[200];

	sprintf(filename, "%s/%s/Evaluation/Trial%d.data", baseDirectory.c_str(),
	    testSuite->getTestSuiteName().c_str(), evaluationData->trialNumber);

	evaluationFile = make_shared<std::ofstream>(filename);

	if(evaluationFile == nullptr) {
		printf(
		    "TestSuiteEvaluator : Could not create file %s ... please create directories\n",
		    filename);
	}

	RealType val = evaluator->evaluate();

	printf("Evaluating Policy after %d Episodes : %f\n", 0, val);

	averageValue = val;
	bestValue = val;

	fmt::fprintf(*evaluationFile, "%d %f\n", 0, val);
}

void AverageRewardTestSuiteEvaluator::doEpisode(
    TestSuite *testSuite, int nEpisode) {
	testSuite->learn(1, stepsLearnEpisode);

	if(nEpisode % episodesBeforeEvaluate == 0) {
		RealType val = evaluator->evaluate();

		averageValue += val;

		if(bestValue < val) {
			bestValue = val;
		}

		printf("Evaluating Policy after %d Episodes : %f, best: %f\n", nEpisode,
		    val, bestValue);

		numEvals++;
		fmt::fprintf(*evaluationFile, "%d %f\n", nEpisode, val);
		evaluationFile->flush();
	}
}

void AverageRewardTestSuiteEvaluator::getEvaluationValue(
    EvaluationValue *evaluationData) {
	evaluationData->bestValue = bestValue;
	evaluationData->averageValue = averageValue / numEvals;
}

bool AverageRewardTestSuiteEvaluator::isFinished(unsigned int nEpisode) {
	bool finished = (nEpisode >= totalLearnEpisodes);

	if(finished) {
		evaluationFile.reset();
	}

	return finished;
}

AverageRewardTestSuiteEvaluator::AverageRewardTestSuiteEvaluator(
    AgentSimulator* agentSimulator, std::string baseDirectory, TestSuite *testSuite,
    Evaluator *l_evaluator, int l_totalLearnEpisodes,
    int l_episodesBeforeEvaluate, int l_stepsLearnEpisode, int nTrials) :
	TestSuiteEvaluator(agentSimulator, baseDirectory, testSuite, nTrials) {
	this->episodesBeforeEvaluate = l_episodesBeforeEvaluate;
	this->totalLearnEpisodes = l_totalLearnEpisodes;
	this->stepsLearnEpisode = l_stepsLearnEpisode;

	numEvals = 0;
	averageValue = 0;
	bestValue = 0;

	this->evaluator = l_evaluator;
	evaluationFile = nullptr;

	setParameter("TestSuiteEvaluator", 1.0);
	addParameter("EpisodesBeforeEvaluate", episodesBeforeEvaluate);
	addParameter("TotalLearnEpisodes", totalLearnEpisodes);
	addParameter("StepsLearnEpisode", stepsLearnEpisode);
}

AverageRewardTestSuiteEvaluator::~AverageRewardTestSuiteEvaluator() {}

GraphLogger::GraphLogger(StateList *l_states, GraphDynamicProgramming *l_graph):
	TestSuiteEvaluatorLogger("Graph")
{
	graph = l_graph;
	states = l_states;
}

GraphLogger::~GraphLogger() {}

void GraphLogger::evaluate(
    std::string evaluationDirectory, int trial, int numEpisodes
) {
	State state(states->getStateProperties());

	for(unsigned int i = 0; i < states->getNumStates(); i++) {
		std::stringstream sstream;

		states->getState(i, &state);
		DataSubset subset;

		int nearestNeighbor = -1;
		RealType distance = -1.0;

		graph->getNearestNode(&state, nearestNeighbor, distance);

		graph->getGraph()->getConnectedNodes(nearestNeighbor, &subset);

		sstream << evaluationDirectory << outputDirectory << "/" << "Trial"
		    << trial << "/Graph" << numEpisodes << "_" << i;

		graph->saveCSV(sstream.str(), &subset);
	}
}

void GraphLogger::startNewEvaluation(
    std::string evaluationDirectory, Parameters *parameters, int trial) {
	std::stringstream sstream;

	sstream << "mkdir " << evaluationDirectory << outputDirectory << "/"
	    << "Trial" << trial;
	std::string scall = sstream.str();
	system(scall.c_str());
}

} //namespace rlearner
