// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>

#include "ril_debug.h"
#include "vetraces.h"
#include "featurefunction.h"
#include "state.h"
#include "statecollection.h"
#include "stateproperties.h"
#include "statemodifier.h"
#include "utility.h"
#include "vfunction.h"

namespace rlearner {

AbstractVETraces::AbstractVETraces(AbstractVFunction *vFunction) {
	this->vFunction = vFunction;

	addParameter("Lambda", 0.9);
	addParameter("DiscountFactor", 0.95);
	addParameter("ETraceThreshold", 0.001);
	addParameter("ReplacingETraces", 1.0);
}

void AbstractVETraces::setReplacingETraces(bool bReplace) {
	if(bReplace) {
		setParameter("ReplacingETraces", 1.0);
	} else {
		setParameter("ReplacingETraces", 0.0);
	}
}

bool AbstractVETraces::getReplacingETraces() {
	return getParameter("ReplacingETraces") > 0.5;
}

void AbstractVETraces::setLambda(RealType lambda) {
	setParameter("Lambda", lambda);
}

RealType AbstractVETraces::getLambda() {
	return getParameter("Lambda");
}

void AbstractVETraces::setThreshold(RealType treshold) {
	setParameter("ETraceThreshold", treshold);
}

RealType AbstractVETraces::getThreshold() {
	return getParameter("ETraceThreshold");
}

AbstractVFunction *AbstractVETraces::getVFunction() {
	return vFunction;
}

StateVETraces::StateVETraces(
    AbstractVFunction *vFunction, StateProperties *modelState,
    const std::set<StateModifierList*>& modifierLists
):
	AbstractVETraces(vFunction),
	eTraceStates(nullptr),
	eTraceLength(0),
	bufState(nullptr),
	eTraces()
{
	setParameter("ETraceThreshold", 0.05);
	addParameter("ETraceMaxListSize", 10);

	if(modifierLists.size()) {
		eTraceStates = new StateCollectionList(modelState, modifierLists);
		bufState = new StateCollectionImpl(modelState, modifierLists);
	} else {
		eTraceStates = new StateCollectionList(modelState);
		bufState = new StateCollectionImpl(modelState);
	}

	eTraces = new std::list<RealType>();
}

StateVETraces::~StateVETraces() {
	delete eTraceStates;
	delete bufState;
	delete eTraces;
}

void StateVETraces::resetETraces() {
	eTraces->clear();
	eTraceStates->clearStateLists();
}

void StateVETraces::updateETraces(int duration) {
	RealType mult = getParameter("Lambda")
	    * pow(getParameter("DiscountFactor"), duration);
	std::list<RealType>::iterator eIt = eTraces->begin();
	RealType treshold = getParameter("ETraceThreshold");

	for(; eIt != eTraces->end(); eIt++) {
		(*eIt) = (*eIt) * mult;
		if(*eIt < treshold) {
			eTraces->erase(eIt, eTraces->end());
			break;
		}
	}
}

void StateVETraces::addETrace(StateCollection *state, RealType factor) {
	eTraceStates->addStateCollection(state);
	eTraces->push_front(factor);
}

void StateVETraces::updateVFunction(RealType td) {
	// TODO: This really, really doesn't seem correct.

	std::list<RealType>::iterator evalue = eTraces->begin();

	for(int state = 0; evalue != eTraces->end(); evalue++) {
		eTraceStates->getStateCollection(
		    eTraceStates->getNumStateCollections() - state, bufState);
		vFunction->updateValue(bufState, (*evalue) * td);
	}
}

GradientVETraces::GradientVETraces(GradientVFunction *gradientVFunction) :
	AbstractVETraces(gradientVFunction) {
	eFeatures = new FeatureList(10, true, true);

	tmpList = new FeatureList();

	addParameter("ETraceMaxListSize", 1000);

	this->gradientVFunction = gradientVFunction;
}

GradientVETraces::~GradientVETraces() {
	delete eFeatures;
	delete tmpList;
}

void GradientVETraces::resetETraces() {
	eFeatures->clear();
}

void GradientVETraces::updateETraces(int duration) {
	RealType lambda = getParameter("Lambda");

	RealType mult = lambda * pow(getParameter("DiscountFactor"), duration);

	multETraces(mult);
}

void GradientVETraces::addETrace(StateCollection *State, RealType factor) {
	tmpList->clear();
	gradientVFunction->getGradient(State, tmpList);

	addGradientETrace(tmpList, factor);

}

void GradientVETraces::multETraces(RealType mult) {
	FeatureList::iterator it = eFeatures->begin();
	int i = 0;
	RealType treshold = getParameter("ETraceThreshold");

	if(DebugIsEnabled('e')) {
		DebugPrint('e', "Etraces Bevore Updating (factor: %f): ", mult);
		eFeatures->save(DebugGetFileHandle('e'));
		DebugPrint('e', "\n");
	}

	while(it != eFeatures->end()) {
		(*it)->factor *= mult;
		if(fabs((*it)->factor) < treshold) {
			eFeatures->remove(*it);
			it = eFeatures->begin();
			for(int j = 0; j < i; j++, it++)
				;

			//printf("Deleting Etrace \n");
		} else {
			i++;
			it++;
		}
	}

	if(DebugIsEnabled('e')) {
		DebugPrint('e', "Etraces After Updating: ");
		eFeatures->save(DebugGetFileHandle('e'));
		DebugPrint('e', "\n");
	}
}

void GradientVETraces::addGradientETrace(FeatureList *gradient, RealType factor) {
	FeatureList::iterator it = gradient->begin();
	DebugPrint('e', "Adding Etraces:\n");
	int maxListSize = my_round(getParameter("ETraceMaxListSize"));

	bool replacing = this->getReplacingETraces();
	for(; it != gradient->end(); it++) {
		DebugPrint('e', "%d : %f -> ", (*it)->featureIndex,
		    eFeatures->getFeatureFactor((*it)->featureIndex));

		RealType featureFactor = (*it)->factor * factor;

		bool signNew = featureFactor > 0;
		bool signOld = eFeatures->getFeatureFactor((*it)->featureIndex) > 0;

		if(replacing) {
			if(signNew == signOld) {
				if(fabs(featureFactor)
				    > fabs(eFeatures->getFeatureFactor((*it)->featureIndex))) {
					eFeatures->set((*it)->featureIndex, featureFactor);
				}
			} else {
				eFeatures->update((*it)->featureIndex, featureFactor);
			}
		} else {
			eFeatures->update((*it)->featureIndex, featureFactor);
		}
		DebugPrint('e', "%f\n",
		    eFeatures->getFeatureFactor((*it)->featureIndex));
	}

	while(eFeatures->size() > maxListSize) {
		eFeatures->remove(*eFeatures->rbegin());
	}
}

void GradientVETraces::updateVFunction(RealType td) {
	gradientVFunction->updateGradient(eFeatures, td);
}

FeatureList* GradientVETraces::getGradientETraces() {
	return eFeatures;
}

FeatureVETraces::FeatureVETraces(FeatureVFunction *featureVFunction) :
	GradientVETraces(featureVFunction) {
	this->featureVFunction = featureVFunction;

	this->featureProperties = featureVFunction->getStateProperties();

}

FeatureVETraces::FeatureVETraces(
    FeatureVFunction *featureVFunction, StateProperties *featureProperties) :
	GradientVETraces(featureVFunction) {
	this->featureVFunction = featureVFunction;

	this->featureProperties = featureProperties;
}

void FeatureVETraces::addETrace(StateCollection *stateCol, RealType factor) {
	bool replacing = this->getReplacingETraces();

	if(stateCol != nullptr) {
		int maxListSize = my_round(getParameter("ETraceMaxListSize"));

		DebugPrint('e', "Adding Etraces:\n");

		State *state = stateCol->getState(featureProperties);

		RealType featureFactor = 0.0;

		for(unsigned int i = 0; i < state->getNumActiveDiscreteStates(); i++) {
			int type = state->getStateProperties()->getType()
			    & (FEATURESTATE | DISCRETESTATE);

			switch(type) {
			case DISCRETESTATE:
				featureFactor = 1.0 * factor;
			break;
			case FEATURESTATE:
				featureFactor = state->getContinuousState(i) * factor;
			break;
			default:
				featureFactor = 1.0 * factor;
			break;
			}

			bool signNew = featureFactor > 0;
			bool signOld = eFeatures->getFeatureFactor(
			    state->getDiscreteState(i)) > 0;

			if(replacing) {
				if(signNew == signOld) {
					if(fabs(featureFactor)
					    < fabs(
					        eFeatures->getFeatureFactor(
					            state->getDiscreteState(i)))
					) {
						featureFactor = eFeatures->getFeatureFactor(
						    state->getDiscreteState(i));
					}
				} else {
					featureFactor += eFeatures->getFeatureFactor(
					    state->getDiscreteState(i));
				}
			} else {
				featureFactor += eFeatures->getFeatureFactor(
				    state->getDiscreteState(i));
			}

			DebugPrint('e', "%d: %f -> ", state->getDiscreteState(i),
			    eFeatures->getFeatureFactor(state->getDiscreteState(i)));
			eFeatures->set(state->getDiscreteState(i), featureFactor);
			DebugPrint('e', "%f\n",
			    eFeatures->getFeatureFactor(state->getDiscreteState(i)));

		}

		while(eFeatures->size() > maxListSize) {
			eFeatures->remove(*eFeatures->rbegin());
		}
	}
}

} //namespace rlearner
