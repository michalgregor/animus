#ifndef Rlearner_algebra_H_
#define Rlearner_algebra_H_

#include "system.h"
#include <Systematic/math/algebra.h>

#include <Eigen/Dense>
#include <Eigen/SVD>

namespace rlearner {
	typedef Eigen::DiagonalMatrix<RealType, Eigen::Dynamic> DiagonalMatrix;
	typedef Eigen::Matrix<RealType, Eigen::Dynamic, 1> ColumnVector;
	typedef ColumnVector Vector;
	using Eigen::JacobiSVD;
}

#endif //Rlearner_algebra_H_
