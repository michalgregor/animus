// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>

#include "ril_debug.h"
#include "qetraces.h"

#include "qfunction.h"
#include "vetraces.h"
#include "vfunction.h"
#include "featurefunction.h"
#include "action.h"
#include "ril_debug.h"
#include "utility.h"

namespace rlearner {

AbstractQETraces::AbstractQETraces(AbstractQFunction *qFunction) {
	this->qFunction = qFunction;

	addParameter("Lambda", 0.9);
	addParameter("DiscountFactor", 0.95);
	addParameter("ReplacingETraces", 1.0);
}

void AbstractQETraces::setLambda(RealType lambda) {
	setParameter("Lambda", lambda);
}

RealType AbstractQETraces::getLambda() {
	return getParameter("Lambda");
}

void AbstractQETraces::setReplacingETraces(bool bReplace) {
	if(bReplace) {
		setParameter("ReplacingETraces", 1.0);
	} else {
		setParameter("ReplacingETraces", 0.0);
	}
}

bool AbstractQETraces::getReplacingETraces() {
	return getParameter("ReplacingETraces") > 0.5;
}

QETraces::QETraces(QFunction *qfunction) :
	AbstractQETraces(qfunction) {
	this->qExFunction = qfunction;

	vETraces = new std::list<AbstractVETraces *>();
	ActionSet::iterator it = qExFunction->getActions()->begin();

	for(unsigned int i = 0; i < qExFunction->getNumActions(); i++, it++) {
		if((*it) != nullptr) {
			AbstractVETraces *vETrace =
			    qExFunction->getVFunction(*it)->getStandardETraces();
			addParameters(vETrace);
			vETraces->push_back(vETrace);
		} else {
			vETraces->push_back(nullptr);
		}
	}
}

QETraces::~QETraces() {
	std::list<AbstractVETraces *>::iterator it = vETraces->begin();
	for(; it != vETraces->end(); it++) {
		if(*it != nullptr) {
			delete (*it);
		}
	}

	delete vETraces;
}

void QETraces::resetETraces() {
	std::list<AbstractVETraces *>::iterator it = vETraces->begin();
	for(int i = 0; it != vETraces->end(); it++, i++) {
		assert((*it) != nullptr);
		(*it)->resetETraces();
	}
}

void QETraces::updateETraces(Action *action, ActionData *data) {
	std::list<AbstractVETraces *>::iterator it = vETraces->begin();
	int duration = 1;
	if(action->isType(AF_MultistepAction)) {
		if(data) {
			duration = dynamic_cast<MultiStepActionData *>(data)->duration;
		} else {
			duration = action->getDuration();
		}
	}

	for(int i = 0; it != vETraces->end(); it++, i++) {
		assert((*it) != nullptr);
		(*it)->updateETraces(duration);
	}
}

void QETraces::addETrace(
    StateCollection *state, Action *action, RealType factor, ActionData *) {
	int index = qExFunction->getActions()->getIndex(action);
	std::list<AbstractVETraces *>::iterator it = vETraces->begin();

	for(int i = 0; it != vETraces->end(); it++, i++) {
		assert((*it) != nullptr);

		if(index == i) {
			(*it)->addETrace(state, factor);
			break;
		}
	}
}

void QETraces::updateQFunction(RealType td) {
	std::list<AbstractVETraces *>::iterator it = vETraces->begin();
	AbstractVFunction *vFunction;

	for(int i = 0; it != vETraces->end(); it++, i++) {
		vFunction = qExFunction->getVFunction(i);
		if((*it) != nullptr && (*it)->getVFunction() != vFunction) {
			delete *it;
			*it = vFunction->getStandardETraces();
		}

		DebugPrint('e', "ETraces Nr: %d %f\n", i, td);

		(*it)->updateVFunction(td);
	}
}

void QETraces::setVETrace(AbstractVETraces *vETrace, int index, bool bDelete) {
	assert(qExFunction->getVFunction(index) == vETrace->getVFunction());

	std::list<AbstractVETraces *>::iterator it = vETraces->begin();

	for(int i = 0; i < index; i++, it++)
		;

	if(bDelete && *it != nullptr) {
		delete *it;
	}
	*it = vETrace;

	addParameters(vETrace);
}

AbstractVETraces *QETraces::getVETrace(int index) {
	std::list<AbstractVETraces *>::iterator it = vETraces->begin();

	for(int i = 0; i < index; i++, it++)
		;

	return *it;
}

void QETraces::setReplacingETraces(bool bReplace) {
	std::list<AbstractVETraces *>::iterator it = vETraces->begin();

	for(int i = 0; it != vETraces->end(); it++, i++) {
		if((*it) != nullptr) {
			(*it)->setReplacingETraces(bReplace);
		}
	}
}

ComposedQETraces::ComposedQETraces(ComposedQFunction *qfunction) :
	AbstractQETraces(qfunction) {
	this->qCompFunction = qfunction;

	qETraces = new std::list<AbstractQETraces *>();

	std::list<AbstractQFunction *>::iterator it =
	    qCompFunction->getQFunctions()->begin();

	for(int i = 0; i < qCompFunction->getNumQFunctions(); i++, it++) {
		if((*it) != nullptr) {
			AbstractQETraces *qETrace = (*it)->getStandardETraces();
			addParameters(qETrace);
			qETraces->push_back(qETrace);
		} else {
			qETraces->push_back(nullptr);
		}
	}
}

ComposedQETraces::~ComposedQETraces() {
	std::list<AbstractQETraces *>::iterator it = qETraces->begin();
	for(; it != qETraces->end(); it++) {
		if(*it != nullptr) {
			delete (*it);
		}
	}

	delete qETraces;
}

void ComposedQETraces::resetETraces() {
	std::list<AbstractQETraces *>::iterator it = qETraces->begin();
	for(int i = 0; it != qETraces->end(); it++, i++) {
		assert((*it) != nullptr);
		(*it)->resetETraces();
	}
}

void ComposedQETraces::addETrace(
    StateCollection *state, Action *action, RealType factor, ActionData *data) {
	std::list<AbstractQETraces *>::iterator it = qETraces->begin();
	std::list<AbstractQFunction *>::iterator itQFunc =
	    qCompFunction->getQFunctions()->begin();

	for(int i = 0; it != qETraces->end(); it++, i++) {
		assert((*it) != nullptr);

		if((*itQFunc)->getActions()->isMember(action)) {
			(*it)->addETrace(state, action, factor, data);
			break;
		}
	}
}

void ComposedQETraces::updateETraces(Action *action, ActionData *data) {
	std::list<AbstractQETraces *>::iterator it = qETraces->begin();

	for(int i = 0; it != qETraces->end(); it++, i++) {
		assert((*it) != nullptr);
		(*it)->updateETraces(action, data);
	}
}

void ComposedQETraces::updateQFunction(RealType td) {
	std::list<AbstractQETraces *>::iterator it = qETraces->begin();
	//std::list<AbstractQFunction *>::iterator itQFunc = qCompFunction->getQFunctions()->begin();

	//AbstractQFunction *qFunction;

	for(; it != qETraces->end(); it++) {
		(*it)->updateQFunction(td);
	}
}

void ComposedQETraces::setQETrace(
    AbstractQETraces *qETrace, int index, bool bDeleteOld) {
	std::list<AbstractQETraces *>::iterator it = qETraces->begin();

	for(int i = 0; i < index; i++, it++)
		;

	if(bDeleteOld && (*it) != nullptr) {
		delete *it;
	}
	*it = qETrace;
}

AbstractQETraces *ComposedQETraces::getQETrace(int index) {
	std::list<AbstractQETraces *>::iterator it = qETraces->begin();

	for(int i = 0; i < index; i++, it++)
		;

	return *it;
}

void ComposedQETraces::setReplacingETraces(bool bReplace) {
	std::list<AbstractQETraces *>::iterator it = qETraces->begin();

	for(int i = 0; it != qETraces->end(); it++, i++) {
		if((*it) != nullptr) {
			(*it)->setReplacingETraces(bReplace);
		}
	}
}
/*
 void ComposedQETraces::setLambda(RealType lambda)
 {
 this->lambda = lambda;
 std::list<AbstractQETraces *>::iterator it = qETraces->begin();


 for (int i = 0; it != qETraces->end(); it++, i++)
 {
 if ((*it) != nullptr)
 {
 (*it)->setLambda(lambda);
 }
 }
 }*/

GradientQETraces::GradientQETraces(GradientQFunction *qfunction) :
	AbstractQETraces(qfunction) {
	this->gradientQFunction = qfunction;

	gradient = new FeatureList(10);
	eTrace = new FeatureList(10, true, true);

	addParameter("ETraceThreshold", 0.001);
	addParameter("ETraceMaxListSize", 1000);

}

GradientQETraces::~GradientQETraces() {
	delete gradient;
	delete eTrace;
}

void GradientQETraces::resetETraces() {
	eTrace->clear();
}

void GradientQETraces::addETrace(
    StateCollection *State, Action *action, RealType factor, ActionData *data) {
	gradient->clear();
	gradientQFunction->getGradient(State, action, data, gradient);

	addGradientETrace(gradient, factor);
}

void GradientQETraces::addGradientETrace(
    FeatureList *l_gradient, RealType factor) {
	FeatureList::iterator it = l_gradient->begin();

	bool replacingETraces = this->getReplacingETraces();

	for(; it != l_gradient->end(); it++) {
		DebugPrint('e', "%d : %f -> ", (*it)->featureIndex,
		    eTrace->getFeatureFactor((*it)->featureIndex));

		RealType featureFactor = (*it)->factor * factor;

		bool signNew = featureFactor > 0;
		bool signOld = eTrace->getFeatureFactor((*it)->featureIndex) > 0;

		if(replacingETraces) {
			if(signNew == signOld) {
				if(fabs(featureFactor)
				    > fabs(eTrace->getFeatureFactor((*it)->featureIndex))) {
					eTrace->set((*it)->featureIndex, featureFactor);
				}
			} else {
				eTrace->update((*it)->featureIndex, featureFactor);
			}
		} else {
			eTrace->update((*it)->featureIndex, featureFactor);
		}

		DebugPrint('e', "%f\n", eTrace->getFeatureFactor((*it)->featureIndex));
	}

	int maxSize = my_round(getParameter("ETraceMaxListSize"));

	while(eTrace->size() > maxSize && maxSize > 0) {
		eTrace->remove(*eTrace->rbegin());
	}

}

void GradientQETraces::updateETraces(Action *action, ActionData *data) {
	FeatureList::iterator it = eTrace->begin();

	int duration = action->getDuration();

	if(DebugIsEnabled('e')) {
		DebugPrint('e', "Etraces Bevore Updating: ");
		eTrace->save(DebugGetFileHandle('e'));
		DebugPrint('e', "\n");
	}

	if(action->isType(AF_MultistepAction)) {
		if(data) {
			duration = dynamic_cast<MultiStepActionData *>(data)->duration;
		} else {
			duration = action->getDuration();
		}
	}

	int i = 0;

	RealType mult = getParameter("Lambda")
	    * pow(getParameter("DiscountFactor"), duration);
	RealType treshold = getParameter("ETraceThreshold");

	while(it != eTrace->end()) {
		(*it)->factor *= mult;
		if(fabs((*it)->factor) < treshold) {
			DebugPrint('e', "Deleting Etrace %d\n", (*it)->featureIndex);
			eTrace->remove(*it);

			it = eTrace->begin();
			for(int j = 0; j < i; j++, it++)
				;

		} else {
			i++;
			it++;
		}
	}

	if(DebugIsEnabled('e')) {
		DebugPrint('e', "Etraces After Updating: ");
		eTrace->save(DebugGetFileHandle('e'));
		DebugPrint('e', "\n");
	}
}

void GradientQETraces::updateQFunction(RealType td) {
	DebugPrint('t', "Updating GradientQ-Function with TD %f \n", td);
	gradientQFunction->updateGradient(eTrace, td);
}

} //namespace rlearner
