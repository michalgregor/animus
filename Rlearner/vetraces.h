// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cvetraces_H_
#define Rlearner_cvetraces_H_

#include <list>
#include <set>

#include "parameters.h"

namespace rlearner {

class AbstractVFunction;
class GradientVFunction;

class State;
class StateCollection;
class FeatureVFunction;
class FeatureList;
class StateProperties;
class StateCollectionList;
class StateCollectionImpl;
class StateModifier;
class StateModifierList;

/**
 * @class AbstractVETraces
 * Class representing etraces for a V-Function.
 *
 * V-ETraces store the "Trace" of each state, so all past states are stored in
 * that "Trace". Many Learning Algorithms use E-Traces for their Value-Updates,
 * which makes a very good improvement to most of the algorithms. V-ETraces
 * objects  stores the E-Traces only for the states, not for actions
 * (see AbstractQETraces), what exactly gets stored in the etraces depends on
 * the kind of the value function. The class AbstractVETraces is the interface
 * for the V-Etraces objects. It has 4 virtual functions for implementing the
 * E-Traces functionality.
 *
 * - addETrace(StateCollection *State, RealType factor = 1.0) adds the etrace of
 *   the specified state with the given factor.
 * - updateVFunction(RealType td): updates the VFunction with the ETraces. For
 *   each state in the ETraces the update value "td" gets multiplied by the
 *   state's factor and then the V-Value of the specified state is updated
 *   by this value.
 * - resetETraces(): resets the ETraces, i.e. all states are cleared from
 *   the ETraces.
 * - updateETraces(int duration): Multiplies all ETraces with
 *   (lambda * gamma)^duration. The duration is obviously the duration of the
 *   current step, so the time attenuation factor has to be exponentiated with
 *   the duration.
 *
 * Each ETraces object has the parameter "Lambda", which is a attenuation
 * factor. Every state from the past gets updated by updateVFunction with
 * the factor lambda^N*gamma^N, N is the time past since the state was active
 * the parameter gamma is the discount factor of the learning Problem
 * (Parameter: "DiscountFactor").
 *
 * In The RIL toolbox there are several implementations of V-ETraces.
 * StateVETraces stores the states directly in a state list and maintains
 * an own list for the factors, FeatureVETraces saves only the features of
 * a state in a Table and GradientVETraces saves always the current gradient
 * to the etrace object. To determine which E-Trace object shall be used for
 * a value-function the class AbstractVFunction provides the method
 * getStandardETraces, which returns a new VETraces object which is best
 * suited for that class of V-Functions.
 *
 * The class AbstractVETraces has following parameters:
 * - "Lambda", 0.9 : attenuation factor
 * - "DiscountFactor", 0.95 : gamma
 * - "ReplacingETraces", replacing etrace handling depends on the kind
 *   of the etraces
 * - "ETraceThreshold", 0.001 : smallest value of an etrace, the etrace will
 *   be deleted from the list if its lower than this value.
 *   Used for performance reasons.
**/
class AbstractVETraces: virtual public ParameterObject {
protected:
	//!pointer to the V-Function
	AbstractVFunction *vFunction;
	//! Use replacing etraces? Used for feature ETraces
public:
	//! Interface for clearing the Etraces.
	virtual void resetETraces() = 0;
	//! Interface for adding a Etrace
	virtual void addETrace(StateCollection *State, RealType factor = 1.0) = 0;

	/**
	 * Interface for updating the ETraces.
	 *
	 * All ETraces factors get multiplied by lambda * gamma. For a multistep
	 * action the update has to be lambda * gamma^N, so the duration can
	 * be given as parameter.
	 */
	virtual void updateETraces(int duration = 1) = 0;

	/**
	 * Update the V-Function
	 *
	 * For all States in the Etraces the "td" value is multiplied with the
	 * E-Trace factor (e.g. lambda^N*gamma^N$ for replacing E-Traces), N is
	 * the time past since the state was active is calculated, the Value of
	 * the state is updated.
	 */
	virtual void updateVFunction(RealType td) = 0;
 
	void setLambda(RealType lambda);
	RealType getLambda();
		
	void setThreshold(RealType treshold);
	RealType getThreshold();

	//! Sets the use of Replacing V-Etraces.
	void setReplacingETraces(bool bReplace);
	bool getReplacingETraces();

	AbstractVFunction *getVFunction();

public:
	AbstractVETraces& operator=(const AbstractVETraces&) = delete;
	AbstractVETraces(const AbstractVETraces&) = delete;

	//! Creates an ETrace for the given V-Function
	AbstractVETraces(AbstractVFunction *vFunction);
};

/**
 * State ETraces stores the state object itself.
 *
 * Stores states of the kind the V-Function uses. For each state there is
 * an Etrace factor stored in a RealType list, which are updated when an ETrace
 * is added (every factor gets multiplied by lambda^N * gamma^N), the active
 * state is initialised with the given factor (standard value is 1.0).
 *
 * State VETraces can be used for non-parametric Value Functions, when they
 * also don't depend on a feature state. But they are very slow and its not
 * recommended to use state etraces.
 **/
class StateVETraces:  public AbstractVETraces {
protected:
	StateCollectionList *eTraceStates;
	int eTraceLength;
	StateCollectionImpl *bufState;
	std::list<RealType> *eTraces;

public:
	virtual void resetETraces();
	virtual void addETrace(StateCollection *State, RealType factor = 1.0);
	virtual void updateETraces(int duration = 1);
	
	virtual void updateVFunction(RealType td);

public:
	StateVETraces& operator=(const StateVETraces&) = delete;
	StateVETraces(const StateVETraces&) = delete;

	StateVETraces(AbstractVFunction *vFunction, StateProperties *modelState,
		const std::set<StateModifierList*>& modifierLists = {});

	virtual ~StateVETraces();
};

/**
 * ETraces for gradient functions.
 *
 * This class maintains the etraces for gradient V-Functions. For the ETraces it
 * uses a feature list, every time an etrace is added with addETrace the e-trace
 * object calculates the gradient with respect to the weights from the current
 * state of the Q-Function and adds this gradient to the e-trace feature list.
 * For gradient E-Traces there are 2 different ways of adding a gradient,
 * whether you want to have replacing or non-replacing etraces:
 * - Non-Replacing ETraces: The gradient is just added to the current ETraces.
 * - Replacing ETraces: If the current gradient has the same sign as the current
 *   ETrace, the greater value of both remains the new E-Trace value. If
 *   the signs are different, the current gradient's value is taken.
 *
 *   For updating the gradient Q-Function the E-Trace object naturally uses
 *   the GradientUpdateFunction interface of the V-Function.
 *
 *   GradientQETraces has the following Parameters:
 *   - "Lambda", 0.9: attenuation factor
 *   - "DiscountFactor", 0.95: gamma
 *   - "ReplacingETraces", see description for adding a gradient to the etrace
 *     list.
 *   - "ETraceThreshold", 0.001: smallest value of an etrace, the etrace will be
 *     deleted from the list if its lower than this value. Used for performance
 *     reasons.
 *   - "ETraceMaxListSize", 100: Maximum size of the etrace-list, if the list
 *     exceeds this value, the smallest etraces will be deleted.
 **/
class GradientVETraces: public AbstractVETraces {
protected:
	//! List for the Etraces.
	FeatureList *eFeatures;
	FeatureList *tmpList;

	GradientVFunction *gradientVFunction;

public:
	/**
	 * Clear the etrace-feature list.
	 *
	 * All elements are deleted from the etraces list and added to the resource
	 * list.
	**/
	virtual void resetETraces();

	/**
	 * Adds the state to the etrace List.
	 *
	 * Calls addGradientETrace.
	**/
	virtual void addETrace(StateCollection *State, RealType factor = 1.0);

	/**
	 * Updates all etrace factors.
	 *
	 * All etrace factors get multiplied by lambda * gamma^N, where the duration
	 * parameter is N.
	**/
	virtual void updateETraces(int duration = 1);

	virtual void multETraces(RealType factor);
	
	/**
	 * All discrete states in the etrace list gets updated.
	 *
	 * The update of the V-Function for state s_i is td * e_i, where e_i is
	 * the etrace factor of s_i.
	*/
	virtual void updateVFunction(RealType td);

	/**
	 * Adds the current gradient to the etrace list.
	 *
	 * Every time an etrace is added with addETrace the e-trace object
	 * calculates the gradient with respect to the weights from the current
	 * state of the V-Function and adds this gradient to the e-trace feature
	 * list. For gradient E-Traces there are 2 different ways of adding
	 * a gradient, wether you want to have replacing or non-replacing etraces:
	 * - Non-Replacing ETraces : The gradient is just added to the current
	 *   ETraces.
	 * - Replacing ETraces : If the current gradient has the same sign as
	 *   the current ETrace, the greater value of both remains the new E-Trace
	 *   value. If the signs are different, the gradients are added.
	 **/
	virtual void addGradientETrace(FeatureList *gradient, RealType factor);

	FeatureList* getGradientETraces();

public:
	GradientVETraces& operator=(const GradientVETraces&) = delete;
	GradientVETraces(const GradientVETraces&) = delete;

	GradientVETraces(GradientVFunction *gradientVFunction);
	virtual ~GradientVETraces();
};

class FeatureVFunction;

/**
 * This class is used as ETraces for feature V-Functions.
 *
 * The class has the same functionality as the gradient v-etraces class, the
 * difference with features is that the gradient here is already calculated by
 * the state modifiers (since feature V-Functions are linear approximators),
 * so the modified feature state is added directly to the etrace list
 * (slightly better performance).
 *
 * The class has the following parameters: same as GradientVETraces.
*/
class FeatureVETraces : public GradientVETraces {
protected:
	FeatureVFunction *featureVFunction;
	StateProperties *featureProperties;

public:
	//! Adds the state to the etrace List.
	virtual void addETrace(StateCollection *State, RealType factor = 1.0);

public:
	FeatureVETraces& operator=(const FeatureVETraces&) = delete;
	FeatureVETraces(const FeatureVETraces&) = delete;

	FeatureVETraces(FeatureVFunction *gradientVFunction);
	FeatureVETraces(FeatureVFunction *gradientVFunction, StateProperties *featureProperties);
};

} //namespace rlearner

#endif //Rlearner_cvetraces_H_
