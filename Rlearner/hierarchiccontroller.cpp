// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>

#include "ril_debug.h"
#include "hierarchiccontroller.h"

#include "action.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "utility.h"

namespace rlearner {

HierarchicalStackSender::HierarchicalStackSender() {
	stackListeners = new std::list<HierarchicalStackListener *>();
}

HierarchicalStackSender::~HierarchicalStackSender() {
	delete stackListeners;
}

void HierarchicalStackSender::addHierarchicalStackListener(
    HierarchicalStackListener *listener) {
	stackListeners->push_back(listener);
}

void HierarchicalStackSender::removeHierarchicalStackListener(
    HierarchicalStackListener *listener) {
	stackListeners->remove(listener);
}

void HierarchicalStackSender::startNewEpisode() {
	for(std::list<HierarchicalStackListener *>::iterator it =
	    stackListeners->begin(); it != stackListeners->end(); it++) {
		(*it)->newEpisode();
	}
}

void HierarchicalStackSender::sendNextStep(
    StateCollection *oldState, HierarchicalStack *actionStack,
    StateCollection *newState) {
	for(std::list<HierarchicalStackListener *>::iterator it =
	    stackListeners->begin(); it != stackListeners->end(); it++) {
		(*it)->nextStep(oldState, actionStack, newState);
	}
}

HierarchicalStackEpisode::HierarchicalStackEpisode(ActionSet *behaviors) {
	actionStacks = new std::vector<ActionList *>();
	this->behaviors = behaviors;
}

HierarchicalStackEpisode::~HierarchicalStackEpisode() {
	newEpisode();

	std::vector<ActionList *>::iterator it;
	for(it = actionStacks->begin(); it != actionStacks->end(); it++) {
		delete *it;
	}

	delete actionStacks;
}

void HierarchicalStackEpisode::nextStep(HierarchicalStack *actionStack) {
	HierarchicalStack::iterator it;
	ActionList *stack = new ActionList(behaviors);
	for(it = actionStack->begin(); it != actionStack->end(); it++) {
		stack->addAction(*it);
	}
	actionStacks->push_back(stack);
}

void HierarchicalStackEpisode::newEpisode() {
	for(unsigned int i = 0; i < actionStacks->size(); i++) {
		delete (*actionStacks)[i];
	}
}

void HierarchicalStackEpisode::load(std::istream& stream) {
	unsigned int bufElems;
	int buf;
	xscanf(stream, "HierarchicalStacks: %d\n", bufElems);

	for(unsigned int i = 0; i < bufElems; i++) {
		ActionList *stack = new ActionList(behaviors);
		xscanf(stream, "%d: ", buf);
		stack->load(stream);
		xscanf(stream, "\n");

		actionStacks->push_back(stack);
	}
}

void HierarchicalStackEpisode::save(std::ostream& stream) {
	fmt::fprintf(stream, "HierarchicalStacks: %d\n", actionStacks->size());

	for(unsigned int i = 0; i < actionStacks->size(); i++) {
		fmt::fprintf(stream, "%d :", i);
		(*actionStacks)[i]->save(stream);
		fmt::fprintf(stream, "\n");
	}
}

void HierarchicalStackEpisode::getHierarchicalStack(
    unsigned int index, HierarchicalStack *actionStack, bool clearStack) {
	if(clearStack) {
		actionStack->clear();
	}

	for(unsigned int i = 0; i < (*actionStacks)[index]->getNumActions(); i++) {
		actionStack->push_back(((*actionStacks)[index])->getAction(i, nullptr));
	}

}

int HierarchicalStackEpisode::getNumSteps() {
	return actionStacks->size();
}

HierarchicalController::HierarchicalController(
    ActionSet *agentActions, ActionSet *allActions, ExtendedAction *rootAction) :
	AgentController(agentActions) {
	this->agentActions = agentActions;
	this->rootAction = rootAction;

	actionStack = new HierarchicalStack();

	addParameter("MaxHierarchicExecution", 0);

	this->hierarchichActionDataSet = new ActionDataSet(allActions);
}

HierarchicalController::~HierarchicalController() {
	delete actionStack;
	delete hierarchichActionDataSet;
}

int HierarchicalController::getMaxHierarchicalExecution() {
	return my_round(getParameter("MaxHierarchicExecution"));
}

void HierarchicalController::setMaxHierarchicalExecution(int maxExec) {
	setParameter("MaxHierarchicExecution", maxExec);
}

Action* HierarchicalController::getAgentAction(
    HierarchicalStack *stack, ActionDataSet *actionDataSet) {
	// returns primitiv action if it is member of the actionset of the controller
	PrimitiveAction *agentAction = nullptr;
	PrimitiveAction *primitiveAction =
	    dynamic_cast<PrimitiveAction *>(*stack->rbegin());
	if(agentActions->getIndex(primitiveAction) >= 0) {
		agentAction = primitiveAction;
	}

	assert(agentAction != nullptr);
	actionDataSet->setActionData(agentAction,
	    hierarchichActionDataSet->getActionData(agentAction));
	return agentAction;
}

Action *HierarchicalController::getNextAction(
    StateCollection *state, ActionDataSet *actionDataSet) {
	actionStack->clear();
	rootAction->getHierarchicalStack(actionStack);

	Action *stackElem = *actionStack->rbegin();
	Action *nextElem = nullptr;

	// rebuild the stack as long as a primitiv action occurs
	while(stackElem->isType(AF_ExtendedAction)) {
		ExtendedAction *exAction = dynamic_cast<ExtendedAction *>(stackElem);
		nextElem = exAction->getNextHierarchyLevel(state,
		    hierarchichActionDataSet);
		exAction->nextHierarchyLevel = nextElem;
		stackElem = nextElem;
		if(stackElem->isType(AF_MultistepAction)) {
			dynamic_cast<MultiStepAction *>(stackElem)->getMultiStepActionData()->duration =
			    0;
		}
		if(stackElem->isType(AF_ExtendedAction)) {
			dynamic_cast<ExtendedAction *>(stackElem)->nextHierarchyLevel =
			    nullptr;
		}
		actionStack->push_back(stackElem);
	}

	return getAgentAction(actionStack, actionDataSet);
}

void HierarchicalController::newEpisode() {
	actionStack->clear();
	rootAction->getHierarchicalStack(actionStack);

	HierarchicalStack::iterator it = actionStack->begin();
	it++;

	HierarchicalStackSender::startNewEpisode();

	for(; it != actionStack->end(); it++) {
		if((*it)->isType(AF_MultistepAction)) {
			dynamic_cast<MultiStepAction *>(*it)->getMultiStepActionData()->finished =
			    false;
		}
	}

	for(HierarchicalStack::iterator it = actionStack->begin();
	    it != actionStack->end(); it++) {
		if((*it)->isType(AF_MultistepAction)) {
			dynamic_cast<MultiStepAction *>(*it)->getMultiStepActionData()->duration =
			    0;
		}
		if((*it)->isType(AF_ExtendedAction)) {
			dynamic_cast<ExtendedAction *>(*it)->nextHierarchyLevel = nullptr;
		}
	}
	actionStack->clear();

}

void HierarchicalController::nextStep(
    StateCollection *oldState, Action *action, StateCollection *newState) {
	bool finishedBefore = false;
	MultiStepAction *newAction;
	int duration = 1;

	if(actionStack->size() == 0) {
		// Hierarchic stack wasnt build correctly, so the hierarchical controller is not the controller of the agent
		return;
	}
	// get duration of primitiv action

	if(action->isType(AF_MultistepAction)) {
		duration = dynamic_cast<MultiStepAction *>(action)->getDuration();
	}

	bool finishedEpisode = newState->isResetState();

	for(HierarchicalStack::iterator it = actionStack->begin();
	    it != actionStack->end(); it++) {
		if((*it)->isType(AF_MultistepAction)) {
			newAction = dynamic_cast<MultiStepAction *>(*it);
			// update all durations with the duration from primitiv action
			newAction->getMultiStepActionData()->duration += duration;

			if(finishedEpisode) // && (*it) != rootAction)
			{
				newAction->getMultiStepActionData()->finished = true;
			} else {
				// calculate the finished flag
				newAction->getMultiStepActionData()->finished = finishedBefore
				    || newAction->isFinished(oldState, newState);
			}

			if((*it) != rootAction && getParameter("MaxHierarchicExecution") > 0
			    && duration >= getParameter("MaxHierarchicExecution")) {
				newAction->getMultiStepActionData()->finished = true;
			}
			// if an element was finished, all other elements get finished too.
			finishedBefore = newAction->getMultiStepActionData()->finished;
		}
	}

	HierarchicalStackSender::sendNextStep(oldState, actionStack, newState);

	/// delete all finished actions from the stack

	bool isFinished = !(*actionStack->rbegin())->isType(AF_MultistepAction);
	if(!isFinished) {
		newAction = dynamic_cast<MultiStepAction *>(*actionStack->rbegin());
		isFinished = (newAction)->getMultiStepActionData()->finished;
	}

	Action *lastAction = nullptr;

	while(isFinished && actionStack->size() > 0) {
		lastAction = *actionStack->rbegin();

		/// set the next action duration

		if(lastAction->isType(AF_MultistepAction)) {
			dynamic_cast<MultiStepAction *>(lastAction)->getMultiStepActionData()->duration =
			    0;
		}

		actionStack->pop_back();

		if(actionStack->size() > 0) {
			lastAction = *actionStack->rbegin();

			/// set the next action field of the last action.
			if(lastAction->isType(AF_ExtendedAction)) {
				ExtendedAction *exAction =
				    dynamic_cast<ExtendedAction *>(lastAction);
				exAction->nextHierarchyLevel = nullptr;
			}

			isFinished = !lastAction->isType(AF_MultistepAction);
			if(!isFinished) {
				newAction = dynamic_cast<MultiStepAction *>(lastAction);
				isFinished = (newAction)->getMultiStepActionData()->finished;
			}
		}
	}
	actionStack->clear();
}

void HierarchicalController::intermediateStep(
    StateCollection *oldState, Action *action, StateCollection *newState) {
	nextStep(oldState, action, newState);
}

} //namespace rlearner
