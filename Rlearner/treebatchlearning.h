// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ctreebatchlearning_H_
#define Rlearner_ctreebatchlearning_H_

#include "parameters.h"
#include "supervisedlearner.h"
#include "qfunction.h"
#include "statemodifier.h"

namespace rlearner {

class DataSet;
class DataSet1D;
class DataSubset;
class DataPreprocessor;
class RegressionForest;

class RegressionTreeFunction;

class StateProperties;
class FeatureVFunction;

class ActionSet;
class Action;
class ActionData;

class EpisodeHistory;

class StateProperties;
class State;
class StateCollection;

class BatchQDataGenerator;
class KDTree;
class KNearestNeighbors;

class RegressionTreeVFunction;

class ExtraRegressionForestTrainer: virtual public ParameterObject {
public:
	virtual RegressionForest * getNewTree(
	    DataSet *input,
	    DataSet1D *output,
	    DataSet1D *weightData
	);

public:
	ExtraRegressionForestTrainer(
	    int numTrees,
	    int K,
	    int n_min,
	    RealType treshold
	);

	virtual ~ExtraRegressionForestTrainer();
};

class ExtraRegressionForestLearner:
    public ExtraRegressionForestTrainer, public SupervisedLearner,
    public SupervisedWeightedLearner {
protected:
	RegressionTreeFunction *treeFunction;

public:
	virtual void learnFA(DataSet *input, DataSet1D *output);

	virtual void learnWeightedFA(
	    DataSet *input,
	    DataSet1D *output,
	    DataSet1D *weightData
	);

	virtual void resetLearner();

public:
	ExtraRegressionForestLearner(
	    RegressionTreeFunction *treeFunction,
	    int numTrees,
	    int K,
	    int n_min,
	    RealType treshold
	);

	virtual ~ExtraRegressionForestLearner();

};

class ExtraRegressionForestFeatureLearner:
    public NewFeatureCalculator, public ExtraRegressionForestTrainer {
protected:
	StateProperties *originalState;

public:
	virtual FeatureCalculator * getFeatureCalculator(
	    FeatureVFunction *vFunction,
	    DataSet *inputData,
	    DataSet1D *outputData
	);

public:
	ExtraRegressionForestFeatureLearner(
	    StateProperties *originalState,
	    int numTrees,
	    int K,
	    int n_min,
	    RealType treshold
	);

	virtual ~ExtraRegressionForestFeatureLearner();
};

class ExtraLinearRegressionModelForestLearner: public SupervisedLearner {
protected:
	RegressionTreeFunction *treeFunction;

public:
	virtual void learnFA(DataSet *input, DataSet1D *output);

public:
	ExtraLinearRegressionModelForestLearner(
	    RegressionTreeFunction *treeFunction,
	    int numTrees,
	    int K,
	    int n_min,
	    RealType treshold,
	    int t1,
	    int t2,
	    int t3
	);

	virtual ~ExtraLinearRegressionModelForestLearner();
};

class RBFForestLearner: public SupervisedLearner {
protected:
	RegressionTreeFunction *treeFunction;

public:
	virtual void learnFA(DataSet *input, DataSet1D *output);

public:
	RBFForestLearner(
	    RegressionTreeFunction *treeFunction,
	    int numTrees,
	    int kNN,
	    int K,
	    int n_min,
	    RealType treshold,
	    RealType varMult,
	    RealType minVar
	);

	virtual ~RBFForestLearner();
};

class LocalLinearLearner: public SupervisedLearner {
protected:
	RegressionTreeFunction *treeFunction;
	DataSet *inputData;
	DataSet1D *outputData;
	DataPreprocessor *preprocessor;

public:
	virtual void learnFA(DataSet *input, DataSet1D *output);

public:
	LocalLinearLearner(
	    RegressionTreeFunction *treeFunction,
	    int kNN,
	    int degree
	);

	virtual ~LocalLinearLearner();
};

class LocalRBFLearner: public SupervisedLearner {
protected:
	RegressionTreeFunction *treeFunction;
	DataSet *inputData;
	DataSet1D *outputData;
	DataPreprocessor *preprocessor;

public:
	virtual void learnFA(DataSet *input, DataSet1D *output);

public:
	LocalRBFLearner(
	    RegressionTreeFunction *treeFunction,
	    int kNN,
	    RealType varMult
	);

	virtual ~LocalRBFLearner();
};

class UnknownDataQFunction: public AbstractQFunction {
protected:
	StateProperties *properties;
	std::map<Action *, KDTree *> *treeMap;
	std::map<Action *, KNearestNeighbors *> *nnMap;

//	std::map<Action *, ColumnVector *> *bufferMap;
	std::map<Action *, DataPreprocessor *> *preMap;

	EpisodeHistory *logger;
	BatchQDataGenerator *dataGenerator;
	ColumnVector *distVector;

	void clearMaps();

public:
	virtual RealType getValue(
	    StateCollection *state,
	    Action *action,
	    ActionData *data = nullptr
	);

	void recalculateTrees();
	virtual RealType getUnknownDataValue(ColumnVector *distances);
	virtual void onParametersChanged();
	virtual void resetData();

public:
	UnknownDataQFunction(
	    ActionSet *actions,
	    EpisodeHistory *logger,
	    StateProperties *properties,
	    RealType factor
	);

	virtual ~UnknownDataQFunction();
};

class UnknownDataQFunctionFromLocalRBFRegression: public AbstractQFunction {
protected:
	std::map<Action *, RegressionTreeVFunction *> *regressionMap;

public:
	bool recalculateFactors;

public:
	virtual RealType getValue(
	    StateCollection *state,
	    Action *action,
	    ActionData *data = nullptr
	);

public:
	UnknownDataQFunctionFromLocalRBFRegression(
	    ActionSet *actions,
	    std::map<Action *, RegressionTreeVFunction *> *regressionMap,
	    RealType factor
	);

	virtual ~UnknownDataQFunctionFromLocalRBFRegression();
};

} //namespace rlearner

#endif //Rlearner_ctreebatchlearning_H_
