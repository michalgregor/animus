// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cvfunctionlearner_H_
#define Rlearner_cvfunctionlearner_H_

#include "qfunction.h"
#include "vfunction.h"
#include "qetraces.h"
#include "residuals.h"
#include "ril_debug.h"

#include "agentlistener.h"
#include "errorlistener.h"
#include "parameters.h"

namespace rlearner {

class AgentController;
class AbstractVFunction;
class AbstractVETraces;
class ActionDataSet;
class GradientVFunction;
class GradientVETraces;
class ResidualFunction;
class ResidualGradientFunction;
class FeatureList;
class AbstractBetaCalculator;
class FeatureVFunction;

/**
 * @class AdaptiveParameterFromValueCalculator
 * Adaptive Parameter Calculator which calculates the parameter's value from
 * the current value of a V-Function.
 *
 * The target value in this class is the current value of the specified
 * V-Function, so its target value is bounded. For more details see the
 * super class.
 *
 * Parameters of AdaptiveParameterFromNStepsCalculator:
 * see AdaptiveParameterBoundedValuesCalculator
 */
class AdaptiveParameterFromValueCalculator:
    public AdaptiveParameterBoundedValuesCalculator,
    public SemiMDPListener {
protected:
	AbstractVFunction *vFunction;
	int nSteps;
	int nStepsPerUpdate;
	RealType value;

public:
	virtual void nextStep(
	    StateCollection *oldState, Action *action, StateCollection *newState);

	virtual void onParametersChanged() {
		AdaptiveParameterBoundedValuesCalculator::onParametersChanged();
	}

	virtual void resetCalculator();

public:
	AdaptiveParameterFromValueCalculator& operator=(
	    const AdaptiveParameterFromValueCalculator&) = delete;
	AdaptiveParameterFromValueCalculator(
	    const AdaptiveParameterFromValueCalculator&) = delete;

	AdaptiveParameterFromValueCalculator(
	    Parameters *targetObject, std::string targetParameter,
	    AbstractVFunction *vFunction, int stepsPerUpdate, int functionKind,
	    RealType param0,
	    RealType paramScale, RealType targetMin, RealType targetMax
	);

	virtual ~AdaptiveParameterFromValueCalculator();
};

/**
 * @class VFunctionLearner
 * TD Learner for Value Function learning.
 *
 * The Value function is learned by a normal TD-Update similar to the
 * TD-Learner for Q-Learning. The temporal difference is calculated each step
 * with the form td = r_t + gamma * V(s_{t+1}) - V(s_t). The class
 * VFunctionLearner uses an CVEtraces object to boost learning. The etraces
 * update the V-Function each step with the temporal difference value, which
 * gets multiplied by the learning rate (Parameter: "VLearningRate") before
 * updating. Each step, the etraces are multiplied by the usual attenuation
 * factor (lambda * gamma) and the current step is added to the etraces.
 * When a new episode is started the etraces gets reseted.
 *
 * Value Function learner are well used in combination with a Dynamic Model
 * for the policy (see VMStochasticPolicy). When you use a dynamic model for
 * your policy you learning performance will be considerably better than with
 * Q-Learning.
 *
 * VFunctionLearner has following Parameters:
 * - inherits all Parameters from the V-Function;
 * - inherits all Parameters from the ETraces;
 * - "VLearningRate", 0.2: learning rate of the algorithm;
 * - "DiscountFactor", 0.95: discount factor of the learning problem.
 */
class VFunctionLearner: public SemiMDPRewardListener, public ErrorSender {
protected:
	//! Learned VFunction.
	AbstractVFunction *vFunction;
	//! Etraces of the Value Function.
	AbstractVETraces *eTraces;

	//! Are extern Etraces used?
	bool bExternETraces;

	//! Adds the current state to the etrace object.
	virtual void addETraces(
	    StateCollection *oldState, StateCollection *newState, int duration);

public:
	/**
	 * Calculates the temporal difference.
	 *
	 * The temporal difference for the given step is td = r_t + gamma
	 * * V(s_{t+1}) - V(s_t) respectively td = r_t + gamma^N * V(s_{t+1})
	 * - V(s_t) for multistep actions.
	 */
	virtual RealType getTemporalDifference(
	    StateCollection *oldState, Action *action, RealType reward,
	    StateCollection *nextState);

	/**
	 * Updates the V-Function, calls the update V-Function method of the
	 * etrace object.
	 *
	 * First the etraces gets multiplied by the attenuation factor
	 * (lambda * gamma)^duration, then the etrace of the current step gets
	 * added, and than the V-Function is updated by the update function of
	 * the etrace object. The update factor is td * learningrate.
	 */
	virtual void updateVFunction(
	    StateCollection *oldState, StateCollection *newState, int duration,
	    RealType td);

	//! Calls updateVFunction with the calculated temporal difference.
	virtual void nextStep(StateCollection *oldState, Action *action,
	RealType reward, StateCollection *nextState);

	/**
	 * Updates the V-Function for a intermediate step (only for
	 * hierarchic MDP's).
	 *
	 * Since the intermediate steps aren't doubly member of the hierarchic
	 * episode they need special treatment for etraces. The state of the
	 * intermediate step is added to the ETraces object as usual, but the
	 * attenuation of all other etraces is cancelled and the V-Function is not
	 * updated with the whole ETraces object, only the current V-Value of the
	 * intermediate state is updated. This is done because the intermediate
	 * step isn't directly reachable for the past states and update all
	 * intermediate steps via etraces would falsify the V-Values since the
	 * same step gets updates several times.
	 */
	virtual void intermediateStep(StateCollection *oldState, Action *action,
	RealType reward, StateCollection *nextState);

	//! Resets the etraces.
	virtual void newEpisode();
	//! Returns the used V-Function.
	AbstractVFunction *getVFunction();

	RealType getLearningRate();
	void setLearningRate(RealType learningRate);

	//! Returns the used ETraces for the VFunction.
	AbstractVETraces *getVETraces();

public:
	VFunctionLearner& operator=(const VFunctionLearner&) = delete;
	VFunctionLearner(const VFunctionLearner&) = delete;

	//! Creates a V-Function Learner which uses the given etraces for
	//! the V-Function.
	VFunctionLearner(
	    RewardFunction *rewardFunction, AbstractVFunction *vFunction,
	    AbstractVETraces *eTraces);
	//! Creates a V-Function Learner which uses the standard etraces
	//! for the V-Function.
	VFunctionLearner(
	    RewardFunction *rewardFunction, AbstractVFunction *vFunction);

	virtual ~VFunctionLearner();
};

class VFunctionGradientLearner: public VFunctionLearner {
protected:
	ResidualFunction *residual;
	ResidualGradientFunction *residualGradientFunction;

	GradientVFunction *gradientVFunction;
	GradientVETraces *gradientETraces;

	FeatureList *oldGradient;
	FeatureList *newGradient;
	FeatureList *residualGradient;

protected:
	virtual void addETraces(
	    StateCollection *oldState, StateCollection *newState, int duration);

public:
	virtual RealType getTemporalDifference(
	    StateCollection *oldState, Action *action, RealType reward,
	    StateCollection *nextState);

public:
	VFunctionGradientLearner& operator=(const VFunctionGradientLearner&) = delete;
	VFunctionGradientLearner(const VFunctionGradientLearner&) = delete;

	VFunctionGradientLearner(
	    RewardFunction *rewardFunction, GradientVFunction *vFunction,
	    ResidualFunction *residual,
	    ResidualGradientFunction *residualGradientFunction);

	virtual ~VFunctionGradientLearner();
};

class VFunctionResidualLearner: public VFunctionGradientLearner {
protected:
	GradientVETraces *residualGradientTraces;
	GradientVETraces *directGradientTraces;

	GradientVETraces *residualETraces;

	AbstractBetaCalculator *betaCalculator;

	virtual void addETraces(
	    StateCollection *oldState, StateCollection *newState, int duration,
	    RealType td);

public:
	virtual void updateVFunction(
	    StateCollection *oldState, StateCollection *newState, int duration,
	    RealType td);

	virtual void newEpisode();

	GradientVETraces *getResidualETraces() {
		return residualETraces;
	}

public:
	VFunctionResidualLearner& operator=(const VFunctionResidualLearner&) = delete;
	VFunctionResidualLearner(const VFunctionResidualLearner&) = delete;

	VFunctionResidualLearner(
	    RewardFunction *rewardFunction, GradientVFunction *vfunction,
	    ResidualFunction *residual, ResidualGradientFunction *residualGradient,
	    AbstractBetaCalculator *betaCalc);

	virtual ~VFunctionResidualLearner();
};

class VAverageTDErrorLearner: public ErrorListener, public StateObject {
protected:
	RealType updateRate;
	FeatureVFunction *averageErrorFunction;

public:
	virtual void receiveError(
	    RealType error, StateCollection *state, Action *action, ActionData *data =
	        nullptr);

	virtual void onParametersChanged();

public:
	VAverageTDErrorLearner& operator=(const VAverageTDErrorLearner&) = delete;
	VAverageTDErrorLearner(const VAverageTDErrorLearner&) = delete;

	VAverageTDErrorLearner(FeatureVFunction *averageErrorFunction,
	RealType updateRate);
	virtual ~VAverageTDErrorLearner();
};

class VAverageTDVarianceLearner: public VAverageTDErrorLearner {
public:
	virtual void receiveError(
	    RealType error, StateCollection *state, Action *action, ActionData *data =
	        nullptr);

public:
	VAverageTDVarianceLearner& operator=(const VAverageTDVarianceLearner&) = delete;
	VAverageTDVarianceLearner(const VAverageTDVarianceLearner&) = delete;

	VAverageTDVarianceLearner(FeatureVFunction *averageErrorFunction,
	RealType updateRate);
	virtual ~VAverageTDVarianceLearner();
};

} //namespace rlearner

#endif //Rlearner_cvfunctionlearner_H_
