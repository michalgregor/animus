// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ccontinuoustime_H_
#define Rlearner_ccontinuoustime_H_

#include "algebra.h"
#include "policies.h"
#include "continuousactions.h"
#include "continuousactiongradientpolicy.h"

namespace rlearner {

class VFunctionInputDerivationCalculator;
class ContinuousTimeTransitionFunction;
class ContinuousTimeQFunctionFromTransitionFunction;
class RewardFunction;
class TransitionFunction;
class GradientVFunction;
class StateCollection;

class ContinuousTimeParameters {
public:
	static RealType getGammaFromSgamma(RealType sgamma, RealType dt);
	static RealType getLambdaFromKappa(RealType kappa, RealType sgamma, RealType dt);
};

class ContinuousTimeVMPolicy: public QStochasticPolicy {
protected:
	VFunctionInputDerivationCalculator *vfunction;
	ContinuousTimeTransitionFunction *model;

public:
	ContinuousTimeQFunctionFromTransitionFunction *getQFunctionFromTransitionFunction();

public:
	ContinuousTimeVMPolicy(ActionSet *actions, ActionDistribution *distribution,
	        VFunctionInputDerivationCalculator *vFunction,
	        ContinuousTimeTransitionFunction *model,
	        RewardFunction *rewardFunction);
	~ContinuousTimeVMPolicy();

};

class ContinuousTimeAndActionVMPolicy: public ContinuousActionController {
protected:
	VFunctionInputDerivationCalculator *dVFunction;
	TransitionFunction *model;

	ColumnVector *actionValues;
	ColumnVector *derivationX;
	Matrix *derivationU;

	virtual void getActionValues(ColumnVector *actionValues,
	        ColumnVector *noise) = 0;
public:
	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *contAction);

public:
	ContinuousTimeAndActionVMPolicy(ContinuousAction *action,
	        VFunctionInputDerivationCalculator *dVFunction,
	        TransitionFunction *model);
	virtual ~ContinuousTimeAndActionVMPolicy();
};

class ContinuousTimeAndActionSigmoidVMPolicy: public ContinuousTimeAndActionVMPolicy {
protected:
	ColumnVector *c;

protected:
	void getActionValues(ColumnVector *actionValues, ColumnVector *noise);

public:
	void setC(int index, RealType value);RealType getC(int index);

	ColumnVector *getC() {
		return c;
	}

	virtual void getNoise(StateCollection *state, ContinuousActionData *action,
	        ContinuousActionData *noise);

public:
	ContinuousTimeAndActionSigmoidVMPolicy(ContinuousAction *action,
	        VFunctionInputDerivationCalculator *vfunction,
	        TransitionFunction *model);
	~ContinuousTimeAndActionSigmoidVMPolicy();
};

class ContinuousTimeAndActionSigmoidVMGradientPolicy: public ContinuousActionGradientPolicy {
protected:
	GradientVFunction *vFunction;
	StateCollectionImpl *derivationState;

	FeatureList *gradient1;
	FeatureList *gradient2;

	VFunctionInputDerivationCalculator *dVFunction;
	TransitionFunction *model;
	ColumnVector *actionValues;
	ColumnVector *derivationX;
	Matrix *derivationU;

	ColumnVector *c;

protected:
	virtual void updateWeights(FeatureList *dParams);
	void getActionValues(ColumnVector *actionValues, ColumnVector *noise);
	virtual void getGradientActionValues(ColumnVector *, ColumnVector *) {}

public:
	virtual int getNumWeights();

	virtual void getWeights(RealType *parameters);
	virtual void setWeights(RealType *parameters);

	virtual void getGradient(StateCollection *inputState, int outputDimension,
	        FeatureList *gradientFeatures);

	virtual void resetData();

	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *contAction);

	void setC(int index, RealType value);RealType getC(int index);

	ColumnVector *getC() {
		return c;
	}

	virtual void getNoise(StateCollection *state, ContinuousActionData *action,
	        ContinuousActionData *noise);

public:
	ContinuousTimeAndActionSigmoidVMGradientPolicy(ContinuousAction *action,
	        GradientVFunction *gradVFunction,
	        VFunctionInputDerivationCalculator *vfunction,
	        TransitionFunction *model,
	        const std::set<StateModifierList*>& modifierLists
	);

	virtual ~ContinuousTimeAndActionSigmoidVMGradientPolicy();
};

class ContinuousTimeAndActionBangBangVMPolicy: public ContinuousTimeAndActionVMPolicy {
protected:
	virtual void getActionValues(ColumnVector *actionValues,
	        ColumnVector *noise);

public:
	virtual void getNoise(StateCollection *state, ContinuousActionData *action,
	        ContinuousActionData *noise);

public:
	ContinuousTimeAndActionBangBangVMPolicy(ContinuousAction *action,
	        VFunctionInputDerivationCalculator *vfunction,
	        TransitionFunction *model);
};

class ContinuousActionSmoother: public ContinuousActionController {
protected:
	ContinuousActionController *policy;
	RealType *actionValues;
	RealType alpha;

public:
	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *contAction);

	void setAlpha(RealType alpha);
	virtual RealType getAlpha();

public:
	ContinuousActionSmoother(ContinuousAction *action,
	        ContinuousActionController *policy, RealType alpha = 0.3);
	virtual ~ContinuousActionSmoother();
};

} //namespace rlearner

#endif //Rlearner_ccontinuoustime_H_
