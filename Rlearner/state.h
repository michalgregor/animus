// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cstate_H_
#define Rlearner_cstate_H_

#include <vector>
#include <iostream>

#include "algebra.h"
#include "baseobjects.h"

namespace rlearner {

class EnvironmentModel;
class State;

/**
 * @class StateCollection
 * Interface for a state collection.
 *
 * From a state collection you can retrieve the specific state you want.
 * A state collection has one general state (usually the model state) and
 * many other states coming from state modifiers. This interface only provides
 * functions to gather states from the collection. The only thing you need
 * is to call getState with the specific state properties object. Therefore
 * state properties object pointer serves as the "ID" of the state in
 * the collection, so it doesn't work to transfer a properties object with
 * just the same properties but which is another instance. If the getState
 * method is called without parameter, the "general" state is calculated.
 *
 * Implementations of StateCollections are StateCollectionImpl and State
 * itself. The State class can be seen as state collection only containing
 * the State object itself. This implementation was chosen in order to
 * provide more generality for many functions.
**/
class StateCollection {
protected:
	bool b_resetState;
	
public:
	//! Retrieves the state with the specified properties.
	virtual State *getState(StateProperties *properties) = 0;
	//! Retrieves the general state.
	virtual State *getState() = 0;
	//! Checks wether there exists a state with the specified state properties
	//! in the collection.
	virtual bool isMember(StateProperties *stateModifier) = 0;

	virtual bool isResetState() {return b_resetState;};
	virtual void setResetState(bool reset) {b_resetState = reset;};

public:
	StateCollection(): b_resetState(false) {};
	virtual ~StateCollection() {};
};

/**
 * @class State
 * Represents a single state.
 *
 * The State object saves the values of state-variables. There are discrete
 * and continuous state variables. The number of discrete and continuous state
 * variables are taken from the StateProperties object. These values
 * stand for the maximum number of continuous resp. discrete states, the arrays
 * are created with this size. The attributes numActiveContinuousStates resp.
 * numActiveDiscreteStates stand for the used state variables by the
 * actual state, these values can differ from the maximum values especially
 * with feature states. These values are used to gain performance when
 * updating the Q-Function, so you only have to look at the first, active state
 * variables.
 *
 * States can be of type DISCRETESTATE, FEATURESTATE or a normal state.
 * The type is specified in the state properties.
 *
 * State implements the StateCollection interface, the class can be seen as
 * state collection only containing the State object itself. This
 * implementation was chosen in order to provide more generality for many
 * functions which take only a state collection.
 *
 * @see StateProperties
*/
SYS_WNONVIRT_DTOR_OFF
class State: public StateObject, public StateCollection, public ColumnVector {
SYS_WNONVIRT_DTOR_ON
protected:
	//! Array for the discrete states.
	int *discreteState;

	//! Number of active continuous state-variables used by the actual state.
	unsigned int numActiveContinuousStates;
	//! Number of active discrete state-variables used by the actual state.
	unsigned int numActiveDiscreteStates;

public:
	/**
	 *  Sets all the values according to @param copy . The properties have to
	 *  be the same.
	 */
	void setState(State *copy);

	/**
	 * Resets the state.
	 *
	 * Sets all state-variables to 0, sets numActiveContinuousStates and
	 * numActiveDiscreteStates to the values given by the properties,
	 * so that all the state-variables are used.
	 **/
	void resetState();

	/**
	 * Implements the StateCollection interface.
	 *
	 * Always returns the state itself. So the returned state doesn't have to have
	 * the specified properties!
	 **/
	virtual State *getState(StateProperties *properties);

	/**
	 * Implements the StateCollection interface.
	 *
	 * Always returns the state itself, so it is the "general" state of its
	 * own state collection.
	 **/
	virtual State *getState();

	/**
	 * Checks whether there exists a state with the specified state properties
	 * in the collection.
	 *
	 * Actually since there is just one state in the "state collection" it
	 * just returns if the properties pointer of the state is the same
	 * as "stateModifier"
	 **/
	virtual bool isMember(StateProperties *stateModifier);

	/**
	 * Returns the number of continuous state-variables used for
	 * the actual state.
	 */
	unsigned int getNumActiveDiscreteStates();

	/**
	 * Returns the number of discrete state-variables used for the actual state.
	 */
	unsigned int getNumActiveContinuousStates();

	//!Sets the number of continuous state-variables used for the actual state.
	void setNumActiveDiscreteStates(int numActiveStates);
	//!Sets the number of discrete state-variables used for the actual state.
	void setNumActiveContinuousStates(int numActiveStates);

	//! Save as text.
	void save(std::ostream& stream);
	//! Load from a text.
	void load(std::istream& stream);

	//! Clones the actual state.
	virtual State* clone();

	//! Returns the dim-th continuous state.
	virtual RealType getContinuousState(unsigned int dim);

	/**
	 * Returns the dim-th normalized continuous state.
	 *
	 * The continuous state gets transformed in the intervall [0,1] according
	 * to its given min and max values.
	 **/
	virtual RealType getNormalizedContinuousState(unsigned int dim);

	//! Returns the dim-th  discrete state.
	virtual int getDiscreteState(unsigned int dim);
	virtual int getDiscreteStateNumber();

	//! Sets the dim-th continuous state.
	virtual void setContinuousState(unsigned int dim, RealType val);
	//! Sets the dim-th discrete state.
	virtual void setDiscreteState(unsigned int dim, int val);

	/**
	 * Compares the two states by comparing all state variables. As soon as one
	 * variable differs it returns false.
	 */
	virtual bool equals(State *state);

	virtual RealType getDistance(ColumnVector *vector);

	RealType getSingleStateDifference(int dim, RealType value);

public:
	State& operator=(const State&) = delete;
	State(const State&) = delete;

	//! Creates a new State with the specified properties.
	State(StateProperties *properties);
	//! Creates a new State with the properties of the model.
	State(EnvironmentModel *model);
	//! Copies the given state.
	State(State *copy);

	/**
	 * Loads a state from file.
	 *
	 * The file stream has to stand on the right position, and the properties
	 * have to be the same (the same values in the properties) as
	 * the properties from the saved State.
	 *
	 * @param properties Properties of the state.
	 * @param stream Stream from where to load the state.
	 **/
	State(StateProperties* properties, std::istream& stream);

	virtual ~State();
};

/**
 * @class StateList
 * Class for logging a list of states.
 *
 * The class maintains a list of RealType and a list of integer vectors. For each
 * state variable defined in its state properties there is an own vector.
 * So only states with the same properties as the state list can be added.
 * When a state is added with addState(...) the state gets decomposed in its
 * state variables, and each value of the state variable is added to its
 * vector. So the StateList class doesn't save the State object itself,
 * it only saves the state variables. This is done due to performance reasons
 * because by saving the state object a new State object would have to be
 * instantiated each time.
 *
 * To retrieve a state from the List you have to call the function
 * setState(int num, State *state). The values of the num-th state are then
 * filled in the specified state object.
 **/
class StateList: virtual public StateObject {
protected:
	//! Vectors for the continuous state variables.
	std::vector<std::vector<RealType> > _continuousStates;
	//! Vectors for the discrete state variables.
	std::vector<std::vector<int> > _discreteStates;
	
	std::vector<bool> _resetStates;

	//! Number of states saved.
	int numStates;

public:
	/**
	 * Add a state to the state list.
	 *
	 * The state must have the same properties as the state list object.
	 * The state gets then decomposed in its state-variables and the state
	 * variables are stored.
	 **/
	void addState(State* state);

	/** 
	 * Get a state from the state list.
	 *
	 * The state must have the same properties as the state list object.
	 * The state variables of the num-th state are retrieved from the vectors
	 * and set in the state object.
	 *
	 * @param num Number of the state to retrieve.
	 * @param state The state object in which the retrieved state variables
	 * are filled in.
	 **/
	void getState(unsigned int num, State* state);

	void removeLastState();
	void removeFirstState();

	//! Save the statelist to a text file.
	virtual void save(std::ostream& stream);

	/**
	 * Load a statelist from a text file.
	 *
	 * The saved statelist must have had the same properties as the state list
	 * (at least same number of discrete and continuous states).
	**/
	virtual void load(std::istream& stream);
	
	//! Get the number of states in the list.
	unsigned int getNumStates();
	void clear();

	//! Initialize the state list with continuous states coming from a
	//! d-dimensional grid.
	void initWithContinuousStates(State* initState, std::vector<int> dimensions, std::vector<RealType> partitions);

	//! Initialize the state list with all possible combination of the states
	//! of the given state variables.
	void initWithDiscreteStates(State* initState, std::vector<int> dimensions);

public:
	StateList& operator=(const StateList&) = delete;
	StateList(const StateList&) = delete;

	/**
	 * Creates a state list.
	 *
	 * The number of vectors for the discrete and continuous state variables
	 * are taken from the properties object.
	 **/
	StateList(StateProperties* properties);
	virtual ~StateList() = default;
};

} //namespace rlearner

#endif //Rlearner_cstate_H_
