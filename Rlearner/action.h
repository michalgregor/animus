// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_caction_H_
#define Rlearner_caction_H_

#include <vector>
#include <list>
#include <map>
#include <iostream>

#include "baseobjects.h"
#include "system.h"

namespace rlearner {

class StateCollection;
class StateProperties;

/**
 * @enum ActionFlag
 * Used to identify action types.
 */
enum ActionFlag {
	AF_ExtendedAction = 1, //!< AF_ExtendedAction
	AF_MultistepAction = 2, //!< AF_MultistepAction
	AF_PrimitiveAction = 4, //!< AF_PrimitiveAction
	AF_ContinuousAction = 16, //!< AF_ContinuousAction
	AF_ContinuousStaticAction = 32 //!< AF_ContinuousStaticAction
};

/**
 * @class ActionData
 * Interface for saving changeable data of an action.
 *
 * Since the "Id" of an action is the pointer itself, and this pointer is used
 * in every action set, we can't set the changeable data of an action that
 * easily, since it would change the action's data everywhere in the program.
 * If this isn't wanted (as usual), we have to use action data objects. New
 * action data objects can be retrieved from any action class (if there is no
 * changeable data for an action the action will return a nullptr pointer). This
 * action data object is now local, and not global any more and can therefore
 * be changed easily. If an action has changeable data, the action pointer and
 * a corresponding action data object determine the action. This is used in
 * many function-arguments. Normally, if the action-data object is missing
 * (= nullptr) a new action data object is retrieved from the action.
 *
 * At the moment its  used for saving the data of MultiStepActions (duration,
 * finished), and ContinousActions (saving the continuous action value)
 * all other classes return nullptr when asking them for a ActionData Object.
 *
 * @see Action
 * @see MultiStepActionData
 * @see ActionList
 */
class ActionData {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int /*version*/);
	void serialize(OArchive& ar, const unsigned int /*version*/);

protected:
	bool bIsChangeAble;

public:
	virtual void save(std::ostream& stream) = 0;
	virtual void load(std::istream& stream) = 0;

	/// Sets the changeable data according to the actiondata object.
	virtual void setData(ActionData *actionData) = 0;

	bool isChangeAble();
	void setIsChangeAble(bool changeAble);

public:
	ActionData();
	virtual ~ActionData() = default;
};

/**
 * @class MultiStepActionData
 * Class for saving the duration and the finished flag from a MultiStepAction.
 *
 * @see ActionData
 */
class MultiStepActionData: public ActionData {
public:
	int duration;
	bool finished;

	virtual void save(std::ostream& stream);
	virtual void load(std::istream& stream);

	//! Sets the changeable data according to the actiondata object.
	virtual void setData(ActionData *actionData);

public:
	MultiStepActionData();
	virtual ~MultiStepActionData() = default;
};

class ContinuousActionProperties;
class ActionDataSet;

/**
 * @class Action
 * Class Representing an action the agent or any other SMDP can choose from.
 *
 * The class represents an interface for all kind of actions. The interface
 * consists of an action type, methods for handling the action data object
 * of the action and the isAvailable Method.  The action itself is identified
 * with the pointer of the action abject together with an specific actionSet.
 * So if you want to know the index of an action, you first must have an action
 * set and then call its getIndex(Action *) method. The class is just an
 * interface for all other actions. It maintains type field to determine which
 * type the action is (enum ActionType).
 *
 * Each type belongs to a specific class. Don't add a type if the class isn't
 * subclass of the class belonging to the type! This will lead to type cast
 * errors. If the type is added the class has to be cast-able to the class
 * representing the type.
 *
 * All the direct subclasses of Action have Action as a virtual base class,
 * so you can combine the attributes of the different actions. For example you
 * can create an PrimitiveAction which can last for several steps (deviated
 * from PrimitiveAction and MultiStepAction). Be careful, due to the virtual
 * base class you ALWAYS have to do a dynamic_cast instead of a normal type
 * cast if you want to cast any Action class object.
 *
 * It also has the member isAvailable(StateCollection *), which is per default
 * always true. With this function you can exclude specific actions in some
 * states, when they are not available for the agent. Before choosing an action,
 * each Controller first determines the list of available actions from its
 * action set to choose a certain action. Since the Method isAvailable gets a
 * state collection you don't have to use the model state, you also have access
 * to other modified states (for example a discrete state). The index of action
 * can't be identified by the action itself, you need always an actionset
 * to get the index of the action.
 */
class Action {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int /*version*/);
	void serialize(OArchive& ar, const unsigned int /*version*/);

protected:
	/**
	 * The type field is an integer, but it serves as a bitmask. So if you want
	 * to add an specific type to the Action you have to add it with an or mask.
	 * If you want to add a new general type of Actions you have to create
	 * a new Typenumber. Since the bitmask only 2^n numbers are allowed.
	 */
	int type;
	ActionData *actionData;

public:
	int getType();
	bool isType(int type);

	/**
	 * Adds a specific type to the type field bitmap.
	 *
	 * So the parameter should be a power of 2, because all bits in the "Type"
	 * parameter get set with an OR mask to the internal type.
	 */
	void addType(int Type);

	/**
	 * Returns an new ActionData object of the specific sub-class.
	 *
	 * The ActionData Object is created with new and must be deleted by
	 * the programmer!
	 */
	virtual ActionData *getNewActionData();

	//! Sets the changeable data according to the actiondata object.
	virtual void loadActionData(ActionData *actionData);

	/**
	 * Returns an actiondata object initialised with the actions values.
	 *
	 * The ActionData Object is created with new and must be deleted by
	 * the programmer!
	 */
	virtual ActionData * getActionData();

	virtual bool isAvailable(StateCollection *) {
		return true;
	}

	//! Returns the duration of the action (defaults to 1).
	virtual int getDuration() {
		return 1;
	}

	//! Determines whether the 2 actions represent the same action.
	virtual bool equals(Action *action);

	//! Compares the action and the actionData objects.
	virtual bool isSameAction(Action *action, ActionData *data);

protected:
	Action(ActionData *actionData);

public:
	Action& operator=(const Action&) = delete;
	Action(const Action&) = delete;

	Action();
	virtual ~Action();
};

/**
 * @class HierarchicalStack
 * A list of Actions representing the actual hierarchical Stack.
 *
 * The actual hierarchical stack contains all ExtendedActions which are active
 * at the moment and the PrimitiveAction which is returned by the last extended
 * action.
 */
SYS_WNONVIRT_DTOR_OFF
class HierarchicalStack: public std::list<Action*> {
SYS_WNONVIRT_DTOR_ON
public:
	void clearAndDelete();

public:
	HierarchicalStack();
	virtual ~HierarchicalStack();
};

/**
 * An action which can long for several steps.
 *
 * An action with a duration different than one has to be treated with
 * SemiMarkov-Learning rules in all the learning algorithms, since otherwise
 * the result can be far from optimal. Many Learning-Algorithm in the this
 * package support Semi-Markov Learning updates. This action class maintains
 * its own action data object, a MultiStepActionData object. This data object
 * stores the duration it has needed by now and the finished flag. The duration
 * and the finished flag get normally set by an HierarchicalController, but it
 * is also possible to set the duration for example in the environment model,
 * if you have a primitive action which takes longer than other primitive
 * actions. In that case your model-specific action has to be derived from
 * PrimitiveAction and MultiStepAction.
 *
 * The class also contains the method isFinished(StateCollection *state).
 * This method is used (normally by a hierarchical controller) to determine
 * whether the action has finished or not. The controller sets the finished
 * flag according to isFinished, so other Listeners only have to look at this
 * flag. This method must be implemented by all (non-abstract) sub-classes.
 */
class MultiStepAction: public Action {
protected:
	MultiStepActionData *multiStepData;

	MultiStepAction(MultiStepActionData *multiStepData);

public:
	/**
	 * This method is normally used by a hierarchical controller to determine
	 * whether the action has finished or not. The finished method may depend
	 * only on the current state transition, so you get the old state and the
	 * new state as parameters. The controller sets the finished flag according
	 * to isFinished, so other Listeners only have to look at this flag. This
	 * method must be implemented by all (non-abstract) sub-classes.
	 */
	virtual bool isFinished(StateCollection *oldState,
	        StateCollection *newState) = 0;

	virtual ActionData *getNewActionData();

	virtual MultiStepActionData *getMultiStepActionData() {
		return multiStepData;
	}

	//! Returns the duration of the action (member: duration).
	virtual int getDuration() {
		return multiStepData->duration;
	}

public:
	MultiStepAction& operator=(const MultiStepAction&) = delete;
	MultiStepAction(const MultiStepAction&) = delete;

	MultiStepAction();
	virtual ~MultiStepAction() = default;
};

/**
 * Represents a primitive Action.
 *
 * The only kind of actions which can be added to the Agent and passed to the
 * EnvironmentModel as action to execute. For a specific learning problem you
 * have to derive your ModelActions from this class and add some specific
 * attributes to the action (for example force...). The type AF_PrimitiveAction
 * is added to the type field of the action.
 *
 * @see PrimitiveActionStateChange
 */
class PrimitiveAction: public Action {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int /*version*/);
	void serialize(OArchive& ar, const unsigned int /*version*/);

public:
	virtual int getDuration() {
		return 1;
	}

protected:
	PrimitiveAction(MultiStepActionData *actionData);

public:
	PrimitiveAction();
	virtual ~PrimitiveAction();
};

/**
 * An action characterized by a single numeric value.
 */
class NumericAction: public PrimitiveAction {
protected:
	RealType _value;

public:
	RealType getValue() const {return _value;}

public:
	NumericAction(RealType value): _value(value) {}
	virtual ~NumericAction() = default;
};

/**
 * This abstract class represents extended actions like behaviours
 * or hierarchical SMDPs.
 *
 * The ExtendedAction class can represent behaviours and other actions which
 * are composed of other primitive, or even extended actions. Since an extended
 * action usually consists of a composition of several other, "more primitive"
 * actions, the extended action has also an duration, so its derived from
 * MultiStepAction. Like an agent controller you can retrieve an other action
 * from the extended action to execute (with getNextHierarchyLevel(...)).
 * The action returned by this method can be a primitive action or as well an
 * extended action with lower hierarchy. Gathering the next action until a
 * primitive action occurs is done by the class HierarchicalController,
 * meanwhile the Hierarchical Stack is also created. The Hierarchical
 * Controller also sets the nextHierarchyLevel Pointer according to
 * getNextHierarchyLevel(...).
 *
 * When using extended actions you have the possibility that all intermediate
 * steps which occurred during the execution of the extended action get send
 * to the Listeners by the class SemiMarkovDecisionProcess. For sending the
 * intermediate steps to a agent listener the function "intermediateStep" is
 * used instead of "nextStep", the different function is needed because
 * intermediate steps has to be treated differently in some cases (ETraces).
 * To get the intermediate steps of a executed behaviour you take all states
 * occurred during the execution of the action. To create a S-A-S tuple you
 * take one state of that list of states for the first state in the tuple (so
 * its the "current" state), set the duration of the extended action correctly
 * and for the second state of the tuple you take the state in which the action
 * has finished. This is only useful for behaviours which finishing condition
 * only depends on the actual state. With this method your are able to provide
 * more training examples.
 */
class ExtendedAction: public MultiStepAction {
protected:
	ExtendedAction(MultiStepActionData *actionData);

public:
	//! Pointer to the action executed by the extended Action.
	Action *nextHierarchyLevel;

	//! Virtual function for determining the next action in the next
	//! hierarchical level.
	virtual Action* getNextHierarchyLevel(StateCollection *state,
	        ActionDataSet *actionDataSet = nullptr) = 0;

	//! Constructs a hierarchical ActionStack, with the extended action
	//! itself as root.
	void getHierarchicalStack(HierarchicalStack *actionStack);

	//! Flag for sending the intermediate Steps of this action.
	bool sendIntermediateSteps;

public:
	ExtendedAction& operator=(const ExtendedAction&) = delete;
	ExtendedAction(const ExtendedAction&) = delete;

	ExtendedAction();
	virtual ~ExtendedAction() {
	}
};

/**
 * @class ActionSet
 * Class maintaining all the actions available for a certain object (normally
 * a ActionObject like a controller).
 *
 * This class maintains a list of all actions which should be usable for
 * another object. It only saves the pointers of the action Objects, so you
 * can't save for example the duration of an action, if you change the duration
 * later (which is normally the case), for that case use ActionList. The
 * pointer of the action is also used as a kind of "Id". According to the
 * pointer the function getIndex(...) returns the index of the action, so the
 * other objects can determine which action was chosen.
 *
 * ActionSet also provides a function for getting all available actions
 * in the current State from the action set.
 *
 * @see ActionList
 */
SYS_WNONVIRT_DTOR_OFF
class ActionSet: public std::list<Action *> {
SYS_WNONVIRT_DTOR_ON
public:
	//! Returns the index of the action.
	int getIndex(Action *action);

	//! Returns the index of the index-th action.
	Action *get(unsigned int index);

	//! Add an action to the set.
	void add(Action *action);

	//! Add all actions from an action set to the action set.
	void add(ActionSet *actions);

	//! Get all available actions in the current State from the action set.
	void getAvailableActions(ActionSet *availableActions,
	        StateCollection *state);

	//! Returns whether the given action is member of the action set.
	bool isMember(Action *action);

public:
	ActionSet& operator=(const ActionSet&) = delete;
	ActionSet(const ActionSet&) = delete;

	ActionSet();
	virtual ~ActionSet();
};

/**
 * @class ActionDataSet
 * Class for maintaining the action data objects of all actions
 * of an action set.
 *
 * This class is needed mainly for controllers, since they are not allowed to
 * change the data of the action itself, they maintain a local action dataset,
 * and modify the data of the data set.
 */
class ActionDataSet {
protected:
	std::map<Action*, ActionData*>* actionDatas;

public:
	ActionData* getActionData(Action* action);
	void setActionData(Action* action, ActionData* data);

	void addActionData(Action *action);
	void removeActionData(Action *action);

public:
	ActionDataSet& operator=(const ActionDataSet&) = delete;
	ActionDataSet(const ActionDataSet&) = delete;

	ActionDataSet(ActionSet *actions);
	ActionDataSet();
	virtual ~ActionDataSet();
};

/**
 * @class ActionList
 * Class for logging a sequence of actions.
 *
 * This class saves the ActionData Objects and the index of the actions in
 * separate lists. If you add an action, the index is stored in the index list
 * (so the action has to be member of the actionlist's actionset. If the
 * specific action returns a valid ActionData Object, this is also stored in
 * the actionDatas map.
 *
 * If you want to get an action with a specified number in the sequence, the
 * action with the index specified by actionIndices[num] is returned, but
 * before if there is an action data, it is set.
 */
class ActionList: public ActionObject {
protected:
	//! Vector for the action indices.
	std::vector<int>* actionIndices;

	/**
	 * Map for the actionDatas, the mapIndex is the actionNumber in the
	 * sequence, because not all action have an action data
	 **/
	std::map<int, ActionData*>* actionDatas;

public:
	void addAction(Action* action);
	Action* getAction(unsigned int num, ActionDataSet* l_data);

	virtual void save(std::ostream& stream);
	virtual void load(std::istream& stream);

	//! Returns the number of actions in the action list.
	unsigned int getSize();
	unsigned int getNumActions();

	void clear();

public:
	ActionList& operator=(const ActionList&) = delete;
	ActionList(const ActionList&) = delete;

	ActionList(ActionSet* actions);
	virtual ~ActionList();
};

} //namespace rlearner

#endif //Rlearner_caction_H_
