#ifndef Animus_QInitializer_H_
#define Animus_QInitializer_H_

#include "qfunction.h"
#include "VInitializer.h"

namespace rlearner {

/**
 * @class QInitializer
 * A class that initializes Q-functions.
 */
class QInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//! Initializes the specified Q function.
	virtual void init(FeatureQFunction* qFunction) = 0;

	//! An alias for init.
	void operator()(FeatureQFunction* qFunction) {
		init(qFunction);
	}

public:
	virtual ~QInitializer() = default;
};

/**
 * @class QInitializer_Vinit
 * A QInitializer, which initializes each component v-function using
 * a given VInitializer.
 */
class QInitializer_Vinit: public QInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The VInitializer used to initialize each of the component v-functions.
	shared_ptr<VInitializer> _vinit;

public:
	//! Initializes the specified Q function.
	virtual void init(FeatureQFunction* qFunction);

	/**
	 * Returns the VInitializer used to initialize each of the component
	 * v-functions.
	 */
	const shared_ptr<VInitializer>& getVInit() const {
		return _vinit;
	}

	/**
	 * Sets the VInitializer used to initialize each of the component
	 * v-functions.
	 */
	void setVInit(const shared_ptr<VInitializer>& vinit) {
		_vinit = vinit;
	}

public:
	/**
	 * Constructor.
	 *
	 * \note VInitializer needs to be set before the initializer is used.
	 */
	QInitializer_Vinit(): _vinit() {}

	/**
	 * Constructor.
	 *
	 * @param vinit The VInitializer used to initialize each of the component
	 * v-functions.
	 */
	QInitializer_Vinit(const shared_ptr<VInitializer>& vinit):
		_vinit(vinit) {}

	virtual ~QInitializer_Vinit() = default;
};

} //namespace rlearner

#endif //Animus_QInitializer_H_
