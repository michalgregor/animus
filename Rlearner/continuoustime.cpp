// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>

#include "ril_debug.h"
#include "continuoustime.h"
#include "state.h"
#include "statecollection.h"
#include "stateproperties.h"
#include "transitionfunction.h"
#include "vfunction.h"
#include "continuousactiongradientpolicy.h"
#include "action.h"

namespace rlearner {

RealType ContinuousTimeParameters::getGammaFromSgamma(RealType sgamma, RealType dt) {
	return 1 - dt * sgamma;
}

RealType ContinuousTimeParameters::getLambdaFromKappa(
    RealType kappa, RealType sgamma, RealType dt) {
	return (kappa - dt) / (1 / sgamma - dt);
}

ContinuousTimeVMPolicy::ContinuousTimeVMPolicy(
    ActionSet *actions, ActionDistribution *distribution,
    VFunctionInputDerivationCalculator *vFunction,
    ContinuousTimeTransitionFunction *model, RewardFunction *rewardFunction) :
	    QStochasticPolicy(actions, distribution,
	        new ContinuousTimeQFunctionFromTransitionFunction(actions,
	            vFunction, model, rewardFunction)) {
	this->vfunction = vFunction;
	this->model = model;

	addParameters(vFunction);

//	assert(vfunction->getNumContinuousStates() == model->getNumContinuousStates());
}

ContinuousTimeVMPolicy::~ContinuousTimeVMPolicy() {
	delete this->qfunction;
}

ContinuousTimeQFunctionFromTransitionFunction *ContinuousTimeVMPolicy::getQFunctionFromTransitionFunction() {
	return dynamic_cast<ContinuousTimeQFunctionFromTransitionFunction *>(qfunction);
}

ContinuousTimeAndActionVMPolicy::ContinuousTimeAndActionVMPolicy(
    ContinuousAction *action, VFunctionInputDerivationCalculator *dvFunction,
    TransitionFunction *model) :
	ContinuousActionController(action, false) {
	this->dVFunction = dvFunction;
	this->model = model;

	assert(this->dVFunction->getNumInputs() == model->getNumContinuousStates());
	assert(model->isType(DM_DERIVATIONUMODEL));

	actionValues = new ColumnVector(
	    getContinuousActionProperties()->getNumActionValues());
	derivationU = new Matrix(model->getNumContinuousStates(),
	    getContinuousActionProperties()->getNumActionValues());
	derivationX = new ColumnVector(model->getNumContinuousStates());
	//noise = new ContinuousActionData(getContinuousActionProperties());

	randomControllerMode = RandomController_Intern;

	addParameters(dvFunction);
}

ContinuousTimeAndActionVMPolicy::~ContinuousTimeAndActionVMPolicy() {
	delete actionValues;
	delete derivationU;
	delete derivationX;
	//delete noise;
}

void ContinuousTimeAndActionVMPolicy::getNextContinuousAction(
    StateCollection *state, ContinuousActionData *action) {
	model->getDerivationU(state->getState(model->getStateProperties()),
	    derivationU);
	dVFunction->getInputDerivation(state, derivationX);

	Matrix temp;
	temp.noalias() = (*derivationU) * (*derivationX);
	(*actionValues) << temp.col(0).transpose();

	noise->initData(0.0);

	if(randomController && randomControllerMode == RandomController_Intern) {
		randomController->getNextContinuousAction(state, noise);
	}

	getActionValues(actionValues, noise);

	(*action) << (*actionValues);
}

ContinuousTimeAndActionSigmoidVMPolicy::ContinuousTimeAndActionSigmoidVMPolicy(
    ContinuousAction *action, VFunctionInputDerivationCalculator *vfunction,
    TransitionFunction *model) :
	ContinuousTimeAndActionVMPolicy(action, vfunction, model) {
	c = new ColumnVector(
	    action->getContinuousActionProperties()->getNumActionValues());
	c->setConstant(1.0);

	addParameter("SigmoidPolicyCFactor", 100.0);
}

ContinuousTimeAndActionSigmoidVMPolicy::~ContinuousTimeAndActionSigmoidVMPolicy() {
	delete c;
}

void ContinuousTimeAndActionSigmoidVMPolicy::getActionValues(
    ColumnVector *actionValues, ColumnVector *noise) {
	*actionValues = (actionValues->array() * c->array())
	    * getParameter("SigmoidPolicyCFactor");

	*actionValues = *actionValues + *noise;

	for(int i = 0; i < actionValues->rows(); i++) {
		RealType umax = getContinuousActionProperties()->getMaxActionValue(i);
		RealType umin = getContinuousActionProperties()->getMinActionValue(i);
		if(actionValues->operator[](i) < -400) {
			actionValues->operator[](i) = -400;
		}
		RealType s = 1 / (1 + exp(-(actionValues->operator[](i))));
		actionValues->operator[](i) = umin + s * (umax - umin);
	}
}

void ContinuousTimeAndActionSigmoidVMPolicy::getNoise(
    StateCollection *, ContinuousActionData *, ContinuousActionData *) {
	assert(false);
	/*
	 RealType generalC = getParameter("SigmoidPolicyCFactor");
	 if (randomControllerMode == RandomController_Intern)
	 {
	 ColumnVector tempVector(contAction->rows());
	 model->getDerivationU(state->getState(model->getStateProperties()), derivationU);
	 dVFunction->getInputDerivation(state, derivationX);

	 derivationX->multMatrix(derivationU, l_noise);

	 tempVector.setVector(action);

	 for (unsigned int i = 0; i < tempVector.rows(); i ++)
	 {
	 RealType umax = getContinuousActionProperties()->getMaxActionValue(i);
	 RealType umin = getContinuousActionProperties()->getMinActionValue(i);

	 RealType actionValue = tempVector.element(i);
	 actionValue = (actionValue - umin) / (umax - umin);

	 actionValue = - log(1 / actionValue - 1) / generalC / c->operator[](i);

	 tempVector.element(i) = actionValue;
	 }

	 *l_noise = *l_noise * -1.0;
	 *l_noise = *l_noise + tempVector;

	 }
	 else
	 {
	 ContinuousActionController::getNoise(state, action, l_noise);
	 }*/
}

void ContinuousTimeAndActionSigmoidVMPolicy::setC(int index, RealType value) {
	c->operator[](index) = value;
}

RealType ContinuousTimeAndActionSigmoidVMPolicy::getC(int index) {
	return c->operator[](index);
}

ContinuousTimeAndActionSigmoidVMGradientPolicy::ContinuousTimeAndActionSigmoidVMGradientPolicy(
    ContinuousAction *action, GradientVFunction *gradVFunction,
    VFunctionInputDerivationCalculator *dvFunction, TransitionFunction *model,
    const std::set<StateModifierList*>& modifierLists) :
	ContinuousActionGradientPolicy(action, model->getStateProperties()) {
	vFunction = gradVFunction;

	derivationState = new StateCollectionImpl(model->getStateProperties(),
	    modifierLists);

	gradient1 = new FeatureList();
	gradient2 = new FeatureList();

	c = new ColumnVector(
	    action->getContinuousActionProperties()->getNumActionValues());
	c->setConstant(1.0);

	addParameter("SigmoidPolicyCFactor", 1.0);

	this->dVFunction = dvFunction;
	this->model = model;

	assert(this->dVFunction->getNumInputs() == model->getNumContinuousStates());
	assert(model->isType(DM_DERIVATIONUMODEL));

	actionValues = new ColumnVector(
	    getContinuousActionProperties()->getNumActionValues());
	derivationU = new Matrix(model->getNumContinuousStates(),
	    getContinuousActionProperties()->getNumActionValues());
	derivationX = new ColumnVector(model->getNumContinuousStates());
	//noise = new ContinuousActionData(getContinuousActionProperties());

	randomControllerMode = RandomController_Intern;

	addParameters(dvFunction);
}

ContinuousTimeAndActionSigmoidVMGradientPolicy::~ContinuousTimeAndActionSigmoidVMGradientPolicy() {
	delete derivationState;
	delete c;
	delete actionValues;
	delete derivationU;
	delete derivationX;
}

void ContinuousTimeAndActionSigmoidVMGradientPolicy::getNextContinuousAction(
    StateCollection *state, ContinuousActionData *action) {
	model->getDerivationU(state->getState(model->getStateProperties()),
	    derivationU);
	dVFunction->getInputDerivation(state, derivationX);

	*actionValues = (*derivationU) * (*derivationX);

	noise->initData(0.0);

	if(randomController && randomControllerMode == RandomController_Intern) {
		randomController->getNextContinuousAction(state, noise);
	}

	getActionValues(actionValues, noise);

	(*action) << (*actionValues);
}

void ContinuousTimeAndActionSigmoidVMGradientPolicy::getActionValues(
    ColumnVector *actionValues, ColumnVector *noise) {

	*actionValues = actionValues->array() * c->array();

	(*actionValues) = (*actionValues) * getParameter("SigmoidPolicyCFactor");
	(*actionValues) = (*actionValues) + (*noise);

	for(int i = 0; i < actionValues->rows(); i++) {
		RealType umax = getContinuousActionProperties()->getMaxActionValue(i);
		RealType umin = getContinuousActionProperties()->getMinActionValue(i);
		if(actionValues->operator[](i) < -400) {
			actionValues->operator[](i) = -400;
		}
		RealType s = 1 / (1 + exp(-(actionValues->operator[](i))));
		actionValues->operator[](i) = umin + s * (umax - umin);
	}
}

void ContinuousTimeAndActionSigmoidVMGradientPolicy::getNoise(
    StateCollection *, ContinuousActionData *, ContinuousActionData *) {
	assert(false);
	/*

	 RealType generalC = getParameter("SigmoidPolicyCFactor");
	 if (randomControllerMode == RandomController_Intern)
	 {
	 ColumnVector tempVector(contAction->rows());
	 model->getDerivationU(state->getState(model->getStateProperties()), derivationU);
	 dVFunction->getInputDerivation(state, derivationX);

	 derivationX->multMatrix(derivationU, l_noise);
	 tempVector.setVector(action);

	 for (unsigned int i = 0; i < tempVector.rows(); i ++)
	 {
	 RealType umax = getContinuousActionProperties()->getMaxActionValue(i);
	 RealType umin = getContinuousActionProperties()->getMinActionValue(i);

	 RealType actionValue = tempVector.element(i);
	 actionValue = (actionValue - umin) / (umax - umin);

	 actionValue = - log(1 / actionValue - 1) / generalC / c->operator[](i);

	 tempVector.element(i) = actionValue;
	 }

	 l_noise->multScalar(-1.0);
	 l_noise->addVector(&tempVector);

	 }
	 else
	 {
	 ContinuousActionController::getNoise(state, action, l_noise);
	 }*/
}

void ContinuousTimeAndActionSigmoidVMGradientPolicy::setC(
    int index, RealType value) {
	c->operator[](index) = value;
}

RealType ContinuousTimeAndActionSigmoidVMGradientPolicy::getC(int index) {
	return c->operator[](index);
}

int ContinuousTimeAndActionSigmoidVMGradientPolicy::getNumWeights() {
	return vFunction->getNumWeights();
}

void ContinuousTimeAndActionSigmoidVMGradientPolicy::getWeights(
    RealType *parameters) {
	vFunction->getWeights(parameters);
}

void ContinuousTimeAndActionSigmoidVMGradientPolicy::setWeights(
    RealType *parameters) {
	vFunction->setWeights(parameters);
}

void ContinuousTimeAndActionSigmoidVMGradientPolicy::updateWeights(
    FeatureList *dParams) {
	vFunction->updateGradient(dParams);
}

void ContinuousTimeAndActionSigmoidVMGradientPolicy::getGradient(
    StateCollection *currentState, int outputDimension,
    FeatureList *gradientFeatures) {
	gradientFeatures->clear();

	model->getDerivationU(currentState->getState(model->getStateProperties()),
	    derivationU);
	dVFunction->getInputDerivation(currentState, derivationX);

	actionValues->noalias() = (*derivationU) * (*derivationX);

	RealType prodFactor = bounded_exp(-actionValues->operator[](outputDimension));
	RealType stepSize = 0.01;
	prodFactor = prodFactor / pow(1.0 + prodFactor, 2.0); // s'(dV/dx  * df/du)

	State *inputState = derivationState->getState(modelState);
	inputState->setState(currentState->getState(modelState));
	for(unsigned int x_i = 0; x_i < modelState->getNumContinuousStates();
	    x_i++) {
		gradient1->clear();
		gradient2->clear();
		RealType stepSize_i = (modelState->getMaxValue(x_i)
		    - modelState->getMinValue(x_i)) * stepSize;
		inputState->setContinuousState(x_i,
		    inputState->getContinuousState(x_i) + stepSize_i);
		derivationState->newModelState();

		vFunction->getGradient(derivationState, gradient1);

		inputState->setContinuousState(x_i,
		    inputState->getContinuousState(x_i) - 2 * stepSize_i);
		derivationState->newModelState();
		vFunction->getGradient(derivationState, gradient2);

		inputState->setContinuousState(x_i,
		    inputState->getContinuousState(x_i) + stepSize_i);

		gradient1->add(gradient2, -1.0);

		gradientFeatures->add(gradient1,
		    prodFactor * derivationU->operator()(x_i, outputDimension)
		        / (2 * stepSize_i));
	}

}

void ContinuousTimeAndActionSigmoidVMGradientPolicy::resetData() {
	vFunction->resetData();
}

ContinuousTimeAndActionBangBangVMPolicy::ContinuousTimeAndActionBangBangVMPolicy(
    ContinuousAction *action, VFunctionInputDerivationCalculator *vfunction,
    TransitionFunction *model) :
	ContinuousTimeAndActionVMPolicy(action, vfunction, model) {

}

void ContinuousTimeAndActionBangBangVMPolicy::getNoise(
    StateCollection *state, ContinuousActionData *action,
    ContinuousActionData *l_noise) {

	ContinuousActionController::getNoise(state, action, l_noise);
}

void ContinuousTimeAndActionBangBangVMPolicy::getActionValues(
    ColumnVector *actionValues, ColumnVector *noise) {
	*actionValues = *actionValues + (*noise);

	for(int i = 0; i < actionValues->rows(); i++) {
		RealType umax = getContinuousActionProperties()->getMaxActionValue(i);
		RealType umin = getContinuousActionProperties()->getMinActionValue(i);
		if(actionValues->operator[](i) > 0) {
			actionValues->operator[](i) = umax;
		} else {
			actionValues->operator[](i) = umin;
		}
	}
}

ContinuousActionSmoother::ContinuousActionSmoother(
    ContinuousAction *action, ContinuousActionController *policy, RealType alpha) :
	ContinuousActionController(action) {
	this->policy = policy;
	this->alpha = alpha;

	this->actionValues =
	    new RealType[contAction->getContinuousActionProperties()->getNumActionValues()];

	for(unsigned int i = 0; i < contAction->getNumDimensions(); i++) {
		actionValues[i] = 0.0;
	}
}

ContinuousActionSmoother::~ContinuousActionSmoother() {
	delete[] actionValues;
}

void ContinuousActionSmoother::getNextContinuousAction(
    StateCollection *state, ContinuousActionData *data) {
	policy->getNextContinuousAction(state, data);

	for(unsigned int i = 0; i < contAction->getNumDimensions(); i++) {
		data->operator[](i) = data->operator[](i) * (1 - getAlpha())
		    + getAlpha() * actionValues[i];
		actionValues[i] = data->operator[](i);
	}
}

void ContinuousActionSmoother::setAlpha(RealType alpha) {
	this->alpha = alpha;
}

RealType ContinuousActionSmoother::getAlpha() {
	return alpha;
}

} //namespace rlearner
