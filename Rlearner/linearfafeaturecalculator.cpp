// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>
#include <cstring>

#include "ril_debug.h"
#include "linearfafeaturecalculator.h"
#include "state.h"
#include "statecollection.h"
#include "utility.h"

namespace rlearner {

FeatureOperatorOr::FeatureOperatorOr() :
	FeatureCalculator() {
	addType(FEATURESTATEDERIVATIONX);

	this->featureFactors = new std::map<StateModifier *, RealType>();
}

FeatureOperatorOr::~FeatureOperatorOr() {
	delete featureFactors;
}

void FeatureOperatorOr::getModifiedState(
    StateCollection *stateCol, State *state) {
	assert(_bInit);

	auto it = getModifierList()->begin();
	auto stateIt = _states.begin();

	State *stateBuf;

	int i = 0;
	int numFeatures = 0;

	for(; it != getModifierList()->end(); it++, stateIt++) {
		if(stateCol->isMember(*it)) {
			stateBuf = stateCol->getState(*it);
		} else {
			stateBuf = *stateIt;
			(*it)->getModifiedState(stateCol, stateBuf);
		}
		RealType featureStateFactor = (*this->featureFactors)[*it];
		if(stateBuf->getStateProperties()->isType(FEATURESTATE)) {
			for(unsigned int j = 0; j < stateBuf->getNumDiscreteStates(); j++) {
				state->setDiscreteState(i,
				    stateBuf->getDiscreteState(j) + numFeatures);
				state->setContinuousState(i,
				    stateBuf->getContinuousState(j) * featureStateFactor);
				i++;
			}
		} else {
			if(stateBuf->getStateProperties()->isType(DISCRETESTATE)) {
				state->setDiscreteState(i,
				    stateBuf->getDiscreteState(0) + numFeatures);
				state->setContinuousState(i, featureStateFactor);
				i++;
			}
		}

		numFeatures += (*it)->getDiscreteStateSize();
	}
	normalizeFeatures(state);
}

void FeatureOperatorOr::addStateModifier(
    StateModifier *featCalc, RealType factor) {
	StateMultiModifier::addStateModifier(featCalc);

	if(!featCalc->isType(STATEDERIVATIONX)) {
		_type = getType() & ~ STATEDERIVATIONX;
	}
	(*this->featureFactors)[featCalc] = factor;

}

StateModifier *FeatureOperatorOr::getStateModifier(int feature) {
	auto it = getModifierList()->begin();
	int numFeatures = (*it)->getDiscreteStateSize();

	while(it != getModifierList()->end() && numFeatures < feature) {
		it++;

		numFeatures += (*it)->getDiscreteStateSize();
	}

	if(it != getModifierList()->end()) {
		return *it;
	} else {
		return nullptr;
	}
}

/*void FeatureOperatorOr::getFeatureDerivationX(int feature, StateCollection *state, ColumnVector *targetVector)
 {
 assert(false);
 StateModifier *stateMod = getStateModifier(feature);

 if (stateMod->isType(FEATURESTATEDERIVATIONX))
 {
 FeatureCalculator *featCalc = dynamic_cast<FeatureCalculator *>(stateMod);
 featCalc->getFeatureDerivationX(feature, state, targetVector)
 }
 }*/

void FeatureOperatorOr::initFeatureOperator() {
	assert(!_bInit);

	int numFeatures = 0;
	int numActiveFeatures = 0;
	for(auto it = _modifierList.begin(); it != _modifierList.end(); it++) {
		numActiveFeatures += (*it)->getNumDiscreteStates();

		numFeatures += (*it)->getDiscreteStateSize(0);
	}

	initFeatureCalculator(numFeatures, numActiveFeatures);
}

FeatureOperatorAnd::FeatureOperatorAnd() :
	FeatureCalculator() {
	addType(FEATURESTATEDERIVATIONX);
}

void FeatureOperatorAnd::getModifiedState(
    StateCollection *stateCol, State *featState) {
	int featureOffset = 1;

	auto it = getModifierList()->begin();
	auto stateIt = _states.begin();

	State *stateBuf;

	for(unsigned int i = 0; i < getNumDiscreteStates(); i++) {
		featState->setDiscreteState(i, 0);
		featState->setContinuousState(i, 1.0);
	}

	int repetitions = getNumDiscreteStates();
	for(int j = 0; it != getModifierList()->end(); it++, stateIt++, j++) {
		repetitions /= (*it)->getNumDiscreteStates();
		stateBuf = nullptr;
		if(stateCol->isMember(*it)) {
			stateBuf = stateCol->getState(*it);
		} else {
			stateBuf = *stateIt;
			(*it)->getModifiedState(stateCol, stateBuf);
		}

		if(stateBuf->getStateProperties()->isType(FEATURESTATE)) {
			for(unsigned int i = 0; i < getNumDiscreteStates(); i++) {
				unsigned int singleStateFeatureNum = (i / repetitions)
				    % stateBuf->getNumDiscreteStates();
				featState->setDiscreteState(i,
				    featState->getDiscreteState(i)
				        + featureOffset
				            * stateBuf->getDiscreteState(
				                singleStateFeatureNum));
				featState->setContinuousState(i,
				    featState->getContinuousState(i)
				        * stateBuf->getContinuousState(singleStateFeatureNum));
			}
		} else {
			for(unsigned int i = 0; i < getNumDiscreteStates(); i++) {
				featState->setDiscreteState(i,
				    featState->getDiscreteState(i)
				        + featureOffset * stateBuf->getDiscreteState(0));
			}
		}

		featureOffset = featureOffset * (*it)->getDiscreteStateSize();
	}
	normalizeFeatures(featState);
}

void FeatureOperatorAnd::addStateModifier(StateModifier *featCalc) {
	StateMultiModifier::addStateModifier(featCalc);

	if(!featCalc->isType(STATEDERIVATIONX)) {
		_type = getType() & ~ STATEDERIVATIONX;
	}
}

void FeatureOperatorAnd::initFeatureOperator() {
	assert(!_bInit);
	int numFeatures = 1;
	int numActiveFeatures = 1;
	for(auto it = _modifierList.begin(); it != _modifierList.end(); it++) {
		numActiveFeatures *= (*it)->getNumDiscreteStates();

		numFeatures *= (*it)->getDiscreteStateSize(0);
	}

	initFeatureCalculator(numFeatures, numActiveFeatures);
}

GridFeatureCalculator::GridFeatureCalculator(
    unsigned int numDim, unsigned int dimensions[], unsigned int part[],
    RealType off[], unsigned int numActiveFeatures) :
	FeatureCalculator(numActiveFeatures, numActiveFeatures) {
	this->numDim = numDim;
	this->dimensions = new unsigned int[numDim];
	this->partitions = new unsigned int[numDim];
	this->offsets = new RealType[numDim];

	dimensionSize = new unsigned int[numDim];

	numFeatures = 1;
	for(unsigned int i = 0; i < numDim; i++) {
		this->dimensions[i] = dimensions[i];
		this->partitions[i] = part[i];
		this->offsets[i] = off[i];
		dimensionSize[i] = numFeatures;
		numFeatures *= partitions[i];
	}

	for(unsigned int i = 0; i < this->getNumDiscreteStates(); i++) {
		this->setDiscreteStateSize(i, numFeatures);
	}

	for(unsigned int i = 0; i < this->getNumContinuousStates(); i++) {
		this->setMinValue(i, 0.0);
		this->setMaxValue(i, 1.0);
	}

	gridScale = new RealType[numDim];

	for(unsigned int i = 0; i < numDim; i++) {
		gridScale[i] = 1.0;
	}

	originalState = nullptr;
}

GridFeatureCalculator::~GridFeatureCalculator() {
	delete offsets;
	delete partitions;
	delete dimensions;
	delete dimensionSize;

	delete gridScale;
}

unsigned int GridFeatureCalculator::getNumDimensions() {
	return numDim;
}

void GridFeatureCalculator::setGridScale(int dimension, RealType scale) {
	gridScale[dimension] = scale;
}

void GridFeatureCalculator::getFeaturePosition(
    unsigned int feature, ColumnVector *position) {
	int partition = 0;
	unsigned int temp = feature;

	for(unsigned int i = 0; i < numDim; i++) {
		partition = temp % partitions[i];
		position->operator[](i) = offsets[i]
		    + 1.0 / partitions[i] * (0.5 + partition) * gridScale[i];

		temp = temp / partitions[i];
	}
}

unsigned int GridFeatureCalculator::getActiveFeature(State *state) {
	RealType part = 0;
	int singleStateFeature = 0;
	unsigned int feature = 0;

	for(unsigned int i = 0; i < numDim; i++) {
		assert(dimensions[i] < state->getNumContinuousStates());

		part =
		    (state->getNormalizedContinuousState(dimensions[i]) - offsets[i]);

		if(state->getStateProperties()->getPeriodicity(dimensions[i])
		    && gridScale[i] >= 1.0) {
			part = part - floor(part);
		}

		singleStateFeature = (int) floor(part * partitions[i] / gridScale[i]);

		if(singleStateFeature < 0) singleStateFeature = 0;
		if((unsigned int) singleStateFeature >= partitions[i]) singleStateFeature =
		    partitions[i] - 1;

		feature += singleStateFeature * dimensionSize[i];
	}
	return feature;
}

void GridFeatureCalculator::getSingleActiveFeature(
    State *state, unsigned int *activeFeature) {
	RealType part = 0;
	int tempSingleStateFeature = 0;

	for(unsigned int i = 0; i < numDim; i++) {
		assert(dimensions[i] < state->getNumContinuousStates());

		part =
		    (state->getNormalizedContinuousState(dimensions[i]) - offsets[i]);

		if(state->getStateProperties()->getPeriodicity(dimensions[i])
		    && gridScale[i] >= 1.0) {
			part = part - floor(part);
		}

		tempSingleStateFeature = (int) floor(
		    part * partitions[i] / gridScale[i]);

		if(tempSingleStateFeature < 0) tempSingleStateFeature = 0;
		if((unsigned int) tempSingleStateFeature >= partitions[i]) tempSingleStateFeature =
		    partitions[i] - 1;

		activeFeature[i] = tempSingleStateFeature;
	}
}

unsigned int GridFeatureCalculator::getFeatureIndex(int position[]) {
	unsigned int feature = 0;

	for(unsigned int i = 0; i < numDim; i++) {
		feature += position[i] * dimensionSize[i];
	}
	return feature;
}

TilingFeatureCalculator::TilingFeatureCalculator(
    unsigned int numDim, unsigned int dimensions[], unsigned int partitions[],
    RealType offsets[]) :
	GridFeatureCalculator(numDim, dimensions, partitions, offsets, 1) {
}

TilingFeatureCalculator::~TilingFeatureCalculator() {
}

void TilingFeatureCalculator::getModifiedState(
    StateCollection *state, State *featState) {
	featState->setDiscreteState(0,
	    getActiveFeature(state->getState(originalState)));
	featState->setContinuousState(0, 1.0);
}

LinearMultiFeatureCalculator::LinearMultiFeatureCalculator(
    unsigned int numDim, unsigned int dimensions[], unsigned int partitions[],
    RealType offsets[], unsigned int numActiveFeatures) :
	    GridFeatureCalculator(numDim, dimensions, partitions, offsets,
	        numActiveFeatures) {
	areaSize = new unsigned int[numDim];

	activePosition = new ColumnVector(numDim);
	featurePosition = new ColumnVector(numDim);
	actualPartition = new unsigned int[numDim];
	singleStateFeatures = new unsigned int[numDim];
}

LinearMultiFeatureCalculator::~LinearMultiFeatureCalculator() {
	delete areaSize;

	delete activePosition;
	delete featurePosition;
	delete[] actualPartition;
	delete[] singleStateFeatures;

}

void LinearMultiFeatureCalculator::initAreaSize() {
	memset(areaSize, 0, sizeof(unsigned int) * numDim);
}

void LinearMultiFeatureCalculator::calcNumActiveFeatures() {
	areaNumPart = 1;
	for(unsigned int i = 0; i < numDim; i++) {
		areaNumPart *= areaSize[i];
	}

	setNumContinuousStates(areaNumPart);
	setNumDiscreteStates(areaNumPart);

	numActiveFeatures = areaNumPart;

	_discreteStateSize.assign(_numDiscreteStates, numFeatures);
	_minValues.assign(_numContinuousStates, 0.0);
	_maxValues.assign(_numContinuousStates, 1.0);
}

void LinearMultiFeatureCalculator::getModifiedState(
    StateCollection *stateCol, State *featState
) {
	SYS_ASSERT(equals(featState->getStateProperties()));

	memset(actualPartition, 0, sizeof(unsigned int) * numDim);
	unsigned int j = 0;
	State *state = stateCol->getState(originalState);

	getSingleActiveFeature(state, singleStateFeatures);
	unsigned int i;
	int feature = 0;

	//offset to add to the actual feature
	int featureAdd = 0;

	getFeaturePosition(getActiveFeature(state), activePosition);

	for(i = 0; i < numDim; i++) {
		int singleFeatureOffset = 0;
		if(areaSize[i] % 2 == 0) {
			//RealType x1 = state->getNormalizedContinuousState(dimensions[i]);
			//RealType x2 = activePosition->operator[](i);
			if(state->getNormalizedContinuousState(dimensions[i])
			    < activePosition->operator[](i)) {
				singleFeatureOffset++;
			}
			singleFeatureOffset += (areaSize[i] - 1) / 2;
		} else {
			singleFeatureOffset += areaSize[i] / 2;
		}
		singleStateFeatures[i] -= singleFeatureOffset;
		activePosition->operator[](i) = activePosition->operator[](i)
		    - singleFeatureOffset * 1.0 / partitions[i] * gridScale[i];
	}

	unsigned int featureIndex = 0;

	for(i = 0; i < areaNumPart; i++) {
		feature = 0;

		for(j = 0; j < numDim; j++) {
			//int dist = (actualPartition[j] - areaSize[j]);
			featurePosition->operator[](j) = activePosition->operator[](j)
			    + (1.0 / partitions[j] * actualPartition[j]) * gridScale[j];

			featureAdd = (singleStateFeatures[j] + actualPartition[j]);
			if(state->getStateProperties()->getPeriodicity(j)
			    && gridScale[j] >= 1.0) {
				featurePosition->operator[](j) = featurePosition->operator[](j)
				    - floor(featurePosition->operator[](j));
				featureAdd = featureAdd
				    - (int) floor((RealType) featureAdd / (RealType) partitions[j])
				        * partitions[j];
			}

			feature = feature + featureAdd * dimensionSize[j];
		}
		if(feature >= 0 && (unsigned int) feature < getNumFeatures()) {
			featState->setDiscreteState(featureIndex, feature);
			featState->setContinuousState(featureIndex,
			    getFeatureFactor(state, featurePosition));
			featureIndex++;
		}

		j = 0;

		actualPartition[0]++;
		while(j < numDim && actualPartition[j] >= areaSize[j]) {
			actualPartition[j] = 0;
			j++;
			if(j < numDim) {
				actualPartition[j]++;
			}
		}
	}
	featState->setNumActiveContinuousStates(featureIndex);
	featState->setNumActiveDiscreteStates(featureIndex);

	for(; featureIndex < areaNumPart; featureIndex++) {
		featState->setDiscreteState(featureIndex, 0);
		featState->setContinuousState(featureIndex, 0.0);
	}

	this->normalizeFeatures(featState);
}

RBFFeatureCalculator::RBFFeatureCalculator(
    unsigned int numDim, unsigned int dimensions[], unsigned int partitions[],
    RealType offsets[], RealType sigma[]
):
	LinearMultiFeatureCalculator(numDim, dimensions, partitions, offsets, 0)
{
	this->sigma = new RealType[numDim];
	memcpy(this->sigma, sigma, sizeof(RealType) * numDim);
	sigmaMaxSize = 2.0;
	initAreaSize();
	addType(FEATURESTATEDERIVATIONX);
}

RBFFeatureCalculator::RBFFeatureCalculator(
    unsigned int numDim, unsigned int dimensions[], unsigned int partitions[],
    RealType offsets[], RealType sigma[], unsigned int areaSize[]
):
	LinearMultiFeatureCalculator(numDim, dimensions, partitions, offsets, 0)
{
	this->sigma = new RealType[numDim];
	memcpy(this->sigma, sigma, sizeof(RealType) * numDim);
	sigmaMaxSize = 2.0;
	memcpy(this->areaSize, areaSize, sizeof(unsigned int) * numDim);
	calcNumActiveFeatures();

	addType(FEATURESTATEDERIVATIONX);
}

RBFFeatureCalculator::~RBFFeatureCalculator() {
	delete[] sigma;
}

void RBFFeatureCalculator::initAreaSize() {
	for(unsigned int i = 0; i < numDim; i++) {
		areaSize[i] =
		    (2 * (unsigned int) floor(sigmaMaxSize * sigma[i] * partitions[i])
		        + 1);
		if(areaSize[i] <= 1) {
			areaSize[i] += 1;
		}
	}

	calcNumActiveFeatures();
}

RealType RBFFeatureCalculator::getFeatureFactor(
    State *state, ColumnVector *position) {
	RealType exponent = 0.0;

	for(unsigned int i = 0; i < numDim; i++) {
		RealType difference = fabs(
		    state->getNormalizedContinuousState(dimensions[i])
		        - position->operator[](i));

		if(state->getStateProperties()->getPeriodicity(dimensions[i])
		    && difference > 0.5) {
			difference = 1 - difference;
		}

		exponent += pow(difference / (sigma[i] * gridScale[i]), 2) / 2;
	}

	return bounded_exp(-exponent);
}

LinearInterpolationFeatureCalculator::LinearInterpolationFeatureCalculator(
    unsigned int numDim, unsigned int dimensions[], unsigned int partitions[],
    RealType offsets[]) :
	LinearMultiFeatureCalculator(numDim, dimensions, partitions, offsets, 0) {
	initAreaSize();
}

LinearInterpolationFeatureCalculator::~LinearInterpolationFeatureCalculator() {
}

RealType LinearInterpolationFeatureCalculator::getFeatureFactor(
    State *state, ColumnVector *featPos) {
	RealType factor = 1.0;

	for(unsigned int i = 0; i < numDim; i++) {

		RealType difference = fabs(
		    state->getNormalizedContinuousState(dimensions[i])
		        - featPos->operator[](i));

		if(state->getStateProperties()->getPeriodicity(dimensions[i])
		    && difference > 0.5) {
			difference = 1 - difference;
		}
		factor *= 1 - difference * partitions[i];
	}
	return factor;
}

void LinearInterpolationFeatureCalculator::initAreaSize() {
	for(unsigned int i = 0; i < numDim; i++) {
		areaSize[i] = 2;
	}
	calcNumActiveFeatures();
}

SingleStateFeatureCalculator::SingleStateFeatureCalculator(
    int dimension, int numPartitions, RealType *partitions, int numActiveFeatures) :
	FeatureCalculator(numPartitions, numActiveFeatures) {
	this->dimension = dimension;
	this->partitions = new RealType[numPartitions];
	memcpy(this->partitions, partitions, sizeof(RealType) * numPartitions);

	originalState = nullptr;
	this->numPartitions = numPartitions;
}

SingleStateFeatureCalculator::~SingleStateFeatureCalculator() {
	delete partitions;
}

void SingleStateFeatureCalculator::getModifiedState(
    StateCollection *stateCol, State *featState) {
	State *state = stateCol->getState(originalState);
	StateProperties *properties = state->getStateProperties();
	RealType contState = state->getContinuousState(dimension);
	RealType width = properties->getMaxValue(dimension)
	    - properties->getMinValue(dimension);

	if(contState < partitions[0] && properties->getPeriodicity(dimension)) {
		contState += width;
	}

	unsigned int activeFeature = 0;
	unsigned int featureNum = 0, realfeatureNum = 0;

	int featureIndex = 0;

	RealType part = partitions[activeFeature];

	while(activeFeature < numFeatures && part < contState) {
		activeFeature++;

		if(activeFeature < numFeatures) {
			part = partitions[activeFeature];
		}

		if(part < partitions[0]) {
			assert(properties->getPeriodicity(dimension));
			part += width;
		}
	}

	if(activeFeature == numFeatures && !properties->getPeriodicity(dimension)) {
		featureNum++;
	}

	DebugPrint('l', "Single State Features: [");
	for(; realfeatureNum < this->numActiveFeatures;
	    realfeatureNum++, featureNum++) {
		if(featureNum % 2 == 0) {
			featureIndex = activeFeature + featureNum / 2;
		} else {
			featureIndex = activeFeature - (featureNum / 2 + 1);
		}

		if(state->getStateProperties()->getPeriodicity(dimension)) {
			featureIndex = featureIndex % numFeatures;
		}

		if(featureIndex >= 0 && featureIndex < (signed int) numFeatures) {
			featState->setDiscreteState(realfeatureNum, featureIndex);

			RealType stateDiff = state->getSingleStateDifference(dimension,
			    partitions[featureIndex]);

			RealType diffNextPart = 1.0;

			if(!state->getStateProperties()->getPeriodicity(dimension)) {
				if(featureIndex == 0 && stateDiff <= 0) {
					stateDiff = 0;
				} else {
					if(featureIndex == (signed int) (numFeatures - 1)
					    && stateDiff > 0) {
						stateDiff = 0;
					} else {
						if(stateDiff <= 0) {
							diffNextPart = partitions[featureIndex]
							    - partitions[featureIndex - 1];
						} else {
							diffNextPart = partitions[featureIndex + 1]
							    - partitions[featureIndex];
						}
					}
				}

			} else {

				if(stateDiff <= 0) {
					diffNextPart = partitions[featureIndex]
					    - partitions[(numPartitions + featureIndex - 1)
					        % numPartitions];
				} else {
					diffNextPart =
					    partitions[(featureIndex + 1) % numPartitions]
					        - partitions[featureIndex];
				}

				if(diffNextPart < 0) {
					diffNextPart += width;
				}
				if(diffNextPart > 0) {
					diffNextPart -= width;
				}
			}

			featState->setContinuousState(realfeatureNum,
			    getFeatureFactor(featureIndex, stateDiff, diffNextPart));

			DebugPrint('l', "%f %f, ", partitions[featureIndex],
			    featState->getContinuousState(realfeatureNum));
		} else {
			featState->setContinuousState(realfeatureNum, 0.0);
			featState->setDiscreteState(realfeatureNum, 0);
		}
	}
	this->normalizeFeatures(featState);
	DebugPrint('l', "]\n");
}

SingleStateRBFFeatureCalculator::SingleStateRBFFeatureCalculator(
    int dimension, int numPartitions, RealType *partitions, int numActiveFeatures) :
	    SingleStateFeatureCalculator(dimension, numPartitions, partitions,
	        numActiveFeatures) {
	//addType(FEATURESTATEDERIVATIONX);
}

RealType SingleStateRBFFeatureCalculator::getFeatureFactor(
    int, RealType difference, RealType diffNextPart) {
	RealType distance = fabs(difference);
	return bounded_exp(-pow(distance / diffNextPart * 2, 2));
}

SingleStateLinearInterpolationFeatureCalculator::SingleStateLinearInterpolationFeatureCalculator(
    int dimension, int numPartitions, RealType *partitions) :
	SingleStateFeatureCalculator(dimension, numPartitions, partitions, 2) {
}

SingleStateLinearInterpolationFeatureCalculator::~SingleStateLinearInterpolationFeatureCalculator() {
}

RealType SingleStateLinearInterpolationFeatureCalculator::getFeatureFactor(
    int, RealType difference, RealType diffNextPart) {
	return 1 - fabs(difference) / diffNextPart;
}

FeatureStateNNInput::FeatureStateNNInput(FeatureCalculator *l_featureStateCalc) :
	StateModifier(l_featureStateCalc->getNumFeatures(), 0) {
	this->featureStateCalc = l_featureStateCalc;
	this->featureState = new State(l_featureStateCalc);
}

FeatureStateNNInput::~FeatureStateNNInput() {
	delete featureState;
}

void FeatureStateNNInput::getModifiedState(
    StateCollection *stateCol, State *state) {
	State *featureStateBuff;
	state->resetState();

	if(stateCol->isMember(featureStateCalc)) {
		featureStateBuff = stateCol->getState(featureStateCalc);
	} else {
		featureStateCalc->getModifiedState(stateCol, featureState);
		featureStateBuff = featureState;
	}

	for(unsigned int i = 0; i < featureStateCalc->getNumActiveFeatures(); i++) {
		state->setContinuousState(featureStateBuff->getDiscreteState(i),
		    featureStateBuff->getContinuousState(i));
	}
}

} //namespace rlearner
