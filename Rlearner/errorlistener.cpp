// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "ril_debug.h"
#include "errorlistener.h"
#include "action.h"
#include "state.h"
#include "vfunction.h"
#include "qfunction.h"

namespace rlearner {

ErrorSender::ErrorSender() {
	errorListeners = new std::list<ErrorListener *>();
}

ErrorSender::~ErrorSender() {
	delete errorListeners;
}

void ErrorSender::sendErrorToListeners(
    RealType error, StateCollection *state, Action *action, ActionData *data) {
	std::list<ErrorListener *>::iterator it = errorListeners->begin();

	for(; it != errorListeners->end(); it++) {
		(*it)->receiveError(error, state, action, data);
	}
}

void ErrorSender::addErrorListener(ErrorListener *listener) {
	errorListeners->push_back(listener);
}

void ErrorSender::removeErrorListener(ErrorListener *listener) {
	errorListeners->remove(listener);
}

StateErrorLearner::StateErrorLearner(AbstractVFunction *stateErrors) {
	this->stateErrors = stateErrors;
	addParameter("ErrorLearningRate", 0.5);
}

void StateErrorLearner::receiveError(
    RealType error, StateCollection *state, Action *, ActionData *) {
	RealType alpha = getParameter("ErrorLearningRate");
	RealType oldError = stateErrors->getValue(state);
	stateErrors->updateValue(state, alpha * (fabs(error) - oldError));
}

StateActionErrorLearner::StateActionErrorLearner(
    AbstractQFunction *stateActionErrors) {
	this->stateActionErrors = stateActionErrors;
	addParameter("ErrorLearningRate", 0.5);
}

void StateActionErrorLearner::receiveError(
    RealType error, StateCollection *state, Action *action, ActionData *data) {
	RealType alpha = getParameter("ErrorLearningRate");
	RealType oldError = stateActionErrors->getValue(state, action, data);
	stateActionErrors->updateValue(state, action,
	    alpha * (fabs(error) - oldError), data);
}

} //namespace rlearner
