#include "VInitializer.h"

namespace rlearner {

void VInitializer::serialize(IArchive& ar, const unsigned int) {}
void VInitializer::serialize(OArchive& ar, const unsigned int) {}

void VInitializer_Constant::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<VInitializer>(*this);
	ar & _constant;
}

void VInitializer_Constant::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<VInitializer>(*this);
	ar & _constant;
}

void VInitializer_Constant::init(FeatureVFunction* vFunction) {
	for(unsigned int i = 0; i < vFunction->getNumFeatures(); i++) {
		vFunction->setFeature(i, _constant);
	}
}

void VInitializer_Generator::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<VInitializer>(*this);
	ar & _generator;
}

void VInitializer_Generator::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<VInitializer>(*this);
	ar & _generator;
}

void VInitializer_Generator::init(FeatureVFunction* vFunction) {
	for(unsigned int i = 0; i < vFunction->getNumFeatures(); i++) {
		vFunction->setFeature(i, (*_generator)());
	}
}

void VInitializer_Uniform::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<VInitializer>(*this);
	ar & _generator;
}

void VInitializer_Uniform::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<VInitializer>(*this);
	ar & _generator;
}

void VInitializer_Uniform::init(FeatureVFunction* vFunction) {
	for(unsigned int i = 0; i < vFunction->getNumFeatures(); i++) {
		vFunction->setFeature(i, (*_generator)());
	}
}

} //namespace rlearner
