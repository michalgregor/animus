// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>
#include <cstring>

#include "ril_debug.h"
#include "policygradient.h"

#include "agent.h"
#include "reinforce.h"
#include "continuousactiongradientpolicy.h"
#include "parameters.h"
#include "featurefunction.h"
#include "action.h"
#include "policies.h"
#include "evaluator.h"

#include "system.h"

namespace rlearner {

PolicyGradientCalculator::PolicyGradientCalculator(
    AgentController *policy, PolicyEvaluator *evaluator) {
	this->policy = policy;
	this->evaluator = evaluator;
}

RealType PolicyGradientCalculator::getFunctionValue() {
	return -evaluator->evaluatePolicy();
}

GPOMDPGradientCalculator::GPOMDPGradientCalculator(
    RewardFunction *reward, StochasticPolicy *policy,
    PolicyEvaluator *evaluator, AgentSimulator* agentSimulator,
    ReinforcementBaseLineCalculator *baseLine, int TStepsPerEpsiode,
    int Episodes, RealType beta) :
	PolicyGradientCalculator(policy, evaluator), SemiMDPRewardListener(reward) {
	this->agentSimulator = agentSimulator;
	this->baseLine = baseLine;

	addParameters(baseLine);
	addParameter("GradientEstimationStepsPerEpisode", TStepsPerEpsiode);
	addParameter("GradientEstimationEpisodes", Episodes);
	addParameter("GPOMDPBeta", beta);

	localGradient = new FeatureList();
	localZTrace = new FeatureList();
	globalGradient = nullptr;
	stochPolicy = policy;
}

GPOMDPGradientCalculator::~GPOMDPGradientCalculator() {
	delete localGradient;
	delete localZTrace;
}

void GPOMDPGradientCalculator::nextStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *)
{
	if(globalGradient) {
		localZTrace->multFactor(getParameter("GPOMDPBeta"));
		localGradient->clear();

		stochPolicy->getActionProbabilityLnGradient(oldState, action,
		    action->getActionData(), localGradient);
		localZTrace->add(localGradient, 1.0);

		FeatureList::iterator it = localZTrace->begin();
		if(DebugIsEnabled('g')) {
			DebugPrint('g', "reward: %f, baseline %f, -> factor %f\n", reward,
			    baseLine->getReinforcementBaseLine((*it)->featureIndex));
			DebugPrint('g', "Z-trace: ");
			localZTrace->save(DebugGetFileHandle('g'));
		}

		for(; it != localZTrace->end(); it++) {
			globalGradient->update((*it)->featureIndex,
			    (reward
			        - baseLine->getReinforcementBaseLine((*it)->featureIndex))
			        * (*it)->factor);
		}
	}
}

void GPOMDPGradientCalculator::newEpisode() {
	localZTrace->clear();
}

void GPOMDPGradientCalculator::getGradient(FeatureList *gradient) {
	setGlobalGradient(gradient);

	int TSteps = my_round(getParameter("GradientEstimationStepsPerEpisode"));
	int nEpisodes = my_round(getParameter("GradientEstimationEpisodes"));

	agentSimulator->startNewEpisode();
	auto agent = agentSimulator->getAgent();

	bool bListen = agent->isListenerAdded(this);

	if(!bListen) {
		agent->addSemiMDPListener(this);
	}

	printf("Calculating PGradient with %d steps and %d Episodes\n", TSteps,
	    nEpisodes);

	unsigned int oldSteps = 0;
	unsigned int gradientSteps = 0;

	for(int i = 0; i < nEpisodes; i++) {
		agentSimulator->startNewEpisode();
		gradientSteps += agentSimulator->doControllerEpisode(TSteps);
		printf("Finished %d Episode\n", i);
	}

	if(!bListen) {
		agent->removeSemiMDPListener(this);
	}
	assert(gradientSteps > 0);

	gradient->multFactor(-1.0 / gradientSteps); // minus because of minimization

	if(DebugIsEnabled('g')) {
		DebugPrint('g', "Calculated GPOMDP Gradient (%d steps)\n", TSteps);
		gradient->save(DebugGetFileHandle('g'));
		DebugPrint('g', "\n");
	}

	setGlobalGradient(nullptr);
}

FeatureList* GPOMDPGradientCalculator::getGlobalGradient() {
	return globalGradient;
}

void GPOMDPGradientCalculator::setGlobalGradient(FeatureList *globalGradient) {
	this->globalGradient = globalGradient;
}

NumericPolicyGradientCalculator::NumericPolicyGradientCalculator(
    Agent *agent_, ContinuousActionGradientPolicy *policy,
    TransitionFunctionEnvironment *dynModel_, RewardFunction *l_rewardFunction,
    RealType stepSize, PolicyEvaluator *evaluator
):
	PolicyGradientCalculator(policy, evaluator),
	gradientFeatures(nullptr),
	weights(new RealType[policy->getNumWeights()]),
	rewardFunction(l_rewardFunction),
	agent(agent_),
	dynModel(dynModel_),
	gradientPolicy(policy)
{
	addParameter("PolicyGradientNumericStepSize", stepSize);
}

NumericPolicyGradientCalculator::~NumericPolicyGradientCalculator() {
	delete[] weights;
}

void NumericPolicyGradientCalculator::getGradient(
    FeatureList *gradientFeatures) {
	gradientPolicy->getWeights(weights);
	agent->setController(policy);

	RealType stepSize = getParameter("PolicyGradientNumericStepSize");
	RealType value = evaluator->evaluatePolicy();

	for(int i = 0; i < gradientPolicy->getNumWeights(); i++) {
		weights[i] -= stepSize;
		gradientPolicy->setWeights(weights);
		RealType vMinus = evaluator->evaluatePolicy();
		weights[i] += 2 * stepSize;
		gradientPolicy->setWeights(weights);
		RealType vPlus = evaluator->evaluatePolicy();

		weights[i] -= stepSize;

		if(vMinus > value || vPlus > value) {
			gradientFeatures->set(i, (vPlus - vMinus) / (2 * stepSize));
		} else {
			gradientFeatures->set(i, 0);
		}

		printf("%f %f %f\n", stepSize, vPlus, vMinus);
		printf("Calculated derivation for weight %d : %f\n", i,
		    gradientFeatures->getFeatureFactor(i));
	}

	gradientPolicy->setWeights(weights);
	gradientFeatures->multFactor(-1.0);
}

RandomPolicyGradientCalculator::RandomPolicyGradientCalculator(
    ContinuousActionGradientPolicy *policy, PolicyEvaluator *evaluator,
    int numEvaluations, RealType stepSize) :
	PolicyGradientCalculator(policy, evaluator)
{
	addParameter("RandomGradientNumEvaluations", numEvaluations);
	addParameter("RandomGradientStepSize", stepSize);

	gradientPolicy = policy;

	stepSizes = new RealType[gradientPolicy->getNumWeights()];
	minWeights = new RealType[gradientPolicy->getNumWeights()];
	nullWeights = new RealType[gradientPolicy->getNumWeights()];
	plusWeights = new RealType[gradientPolicy->getNumWeights()];

	numMinWeights = new int[gradientPolicy->getNumWeights()];
	numMaxWeights = new int[gradientPolicy->getNumWeights()];
	numNullWeights = new int[gradientPolicy->getNumWeights()];

	memset(stepSizes, 0, sizeof(RealType) * gradientPolicy->getNumWeights());
}

RandomPolicyGradientCalculator::~RandomPolicyGradientCalculator() {
	delete[] stepSizes;
	delete[] minWeights;
	delete[] nullWeights;
	delete[] plusWeights;
	delete[] numMinWeights;
	delete[] numMaxWeights;
	delete[] numNullWeights;
}

void RandomPolicyGradientCalculator::getGradient(FeatureList *gradient) {
	auto rand = make_rand();
	gradient->clear();

	memset(minWeights, 0, sizeof(RealType) * gradientPolicy->getNumWeights());
	memset(nullWeights, 0, sizeof(RealType) * gradientPolicy->getNumWeights());
	memset(plusWeights, 0, sizeof(RealType) * gradientPolicy->getNumWeights());

	memset(numMinWeights, 0, sizeof(int) * gradientPolicy->getNumWeights());
	memset(numMaxWeights, 0, sizeof(int) * gradientPolicy->getNumWeights());
	memset(numNullWeights, 0, sizeof(int) * gradientPolicy->getNumWeights());

	int numEvals = (int) getParameter("RandomGradientNumEvaluations");
	RealType stepSize = getParameter("RandomGradientStepSize");

	RealType *parameters = new RealType[gradientPolicy->getNumWeights()];
	RealType *oldParameters = new RealType[gradientPolicy->getNumWeights()];
	int *step = new int[gradientPolicy->getNumWeights()];

	gradientPolicy->getWeights(oldParameters);

	int *numWeights[3];
	RealType *weights[3];

	weights[0] = minWeights;
	weights[1] = nullWeights;
	weights[2] = plusWeights;

	numWeights[0] = numMinWeights;
	numWeights[1] = numNullWeights;
	numWeights[2] = numMaxWeights;

	printf("Calculating gradient with random samples\n");
	for(int i = 0; i < numEvals; i++) {

		for(int j = 0; j < gradientPolicy->getNumWeights(); j++) {
			if(i > 0) {
				step[j] = rand() % 3;
			} else {
				step[j] = 1;
			}

			if(stepSizes[j] == 0) {
				parameters[j] = oldParameters[j] + (step[j] - 1) * stepSize;
			} else {
				parameters[j] = oldParameters[j] + (step[j] - 1) * stepSizes[j];
			}
		}

		gradientPolicy->setWeights(parameters);
		RealType value = evaluator->evaluatePolicy();

		printf("Evaluation %d: %f\n", i, value);

		for(int j = 0; j < gradientPolicy->getNumWeights(); j++) {
			numWeights[step[j]][j]++;
			weights[step[j]][j] += value;
		}
	}

	for(int j = 0; j < gradientPolicy->getNumWeights(); j++) {
		RealType l_stepSize = stepSize;
		if(stepSizes[j] > 0) {
			l_stepSize = stepSizes[j];
		}

		if(numWeights[0][j] > 0
		    && weights[0][j] / numWeights[0][j]
		        > weights[1][j] / numWeights[1][j]) {
			if(numWeights[2][j] > 0
			    && weights[2][j] / numWeights[2][j]
			        > weights[0][j] / numWeights[0][j]) {
				gradient->set(j,
				    (weights[2][j] / numWeights[2][j]
				        - weights[1][j] / numWeights[1][j]) / l_stepSize);
			} else {
				gradient->set(j,
				    (weights[0][j] / numWeights[0][j]
				        - weights[1][j] / numWeights[1][j]) / (-l_stepSize));
			}
		} else {
			if(numWeights[2][j] > 0
			    && weights[2][j] / numWeights[2][j]
			        > weights[1][j] / numWeights[1][j]) {
				gradient->set(j,
				    (weights[2][j] / numWeights[2][j]
				        - weights[1][j] / numWeights[1][j]) / l_stepSize);
			} else {
				gradient->set(j, 0.0);
//				stepSizes[j] = l_stepSize;
//				printf("No Uphill direction found, decreasing step size of parameter %d (%f)\n", j, stepSizes[j]);
			}
		}

	}

	gradient->multFactor(-1.0);
	gradientPolicy->setWeights(oldParameters);

	delete[] step;
	delete[] parameters;
	delete[] oldParameters;
}

void RandomPolicyGradientCalculator::setStepSize(int index, RealType stepSize) {
	stepSizes[index] = stepSize;
}

RandomMaxPolicyGradientCalculator::RandomMaxPolicyGradientCalculator(
    ContinuousActionGradientPolicy *policy, PolicyEvaluator *evaluator,
    int numEvaluations, RealType stepSize) :
	PolicyGradientCalculator(policy, evaluator)
{
	addParameter("RandomGradientNumEvaluations", numEvaluations);
	addParameter("RandomGradientStepSize", stepSize);

	addParameter("RandomGradientStepSizeFactorNoImprove", 0.95);
	addParameter("RandomGradientStepSizeFactorImprove", 1.1);

	gradientPolicy = policy;

	stepSizes = new RealType[gradientPolicy->getNumWeights()];
	workStepSizes = new RealType[gradientPolicy->getNumWeights()];
	memset(stepSizes, 0, sizeof(RealType) * gradientPolicy->getNumWeights());
}

RandomMaxPolicyGradientCalculator::~RandomMaxPolicyGradientCalculator() {
	delete[] stepSizes;
	delete[] workStepSizes;
}

void RandomMaxPolicyGradientCalculator::resetGradientCalculator() {
	memcpy(workStepSizes, stepSizes,
	    sizeof(RealType) * gradientPolicy->getNumWeights());
}

void RandomMaxPolicyGradientCalculator::getGradient(FeatureList *gradient) {
	auto rand = make_rand();
	gradient->clear();

	int numEvals = (int) getParameter("RandomGradientNumEvaluations");
	RealType stepSize = getParameter("RandomGradientStepSize");

	RealType *parameters = new RealType[gradientPolicy->getNumWeights()];
	RealType *oldParameters = new RealType[gradientPolicy->getNumWeights()];
	RealType *bestParameters = new RealType[gradientPolicy->getNumWeights()];

	gradientPolicy->getWeights(oldParameters);

	printf("Calculating gradient with random samples, stepSize %f\n",
	    workStepSizes[0]);
	RealType bestValue = 0;

	bool newBest = false;

	for(int i = 0; i < numEvals; i++) {

		for(int j = 0; j < gradientPolicy->getNumWeights(); j++) {
			int step = 1;
			if(i > 0) {
				step = rand() % 3;
			}

			if(workStepSizes[j] == 0) {
				parameters[j] = oldParameters[j] + (step - 1) * stepSize;
			} else {
				parameters[j] = oldParameters[j]
				    + (step - 1) * workStepSizes[j];
			}
		}

		gradientPolicy->setWeights(parameters);
		RealType value = evaluator->evaluatePolicy();

		printf("Evaluation %d: %f\n", i, value);

		if(i == 0 || value > bestValue) {
			printf("Found new Maximum %f -> %f\n", value, bestValue);
			memcpy(bestParameters, parameters,
			    sizeof(RealType) * gradientPolicy->getNumWeights());
			bestValue = value;

			if(i > 0) {
				newBest = true;
			}
		}

	}

	printf("Setting Gradient...\n");
	for(int j = 0; j < gradientPolicy->getNumWeights(); j++) {
		gradient->set(j, -(bestParameters[j] - oldParameters[j]));
	}

	gradientPolicy->setWeights(oldParameters);

	RealType factor = getParameter("RandomGradientStepSizeFactorImprove");

	if(!newBest) {
		factor = getParameter("RandomGradientStepSizeFactorNoImprove");
	}

	for(int i = 0; i < gradientPolicy->getNumWeights(); i++) {
		if(workStepSizes[i] == 0) {
			workStepSizes[i] = stepSize * factor;
		} else {
			workStepSizes[i] = workStepSizes[i] * factor;
		}
	}

	printf("Finished calculating gradient, best value : %f\n", bestValue);

	delete[] parameters;
	delete[] oldParameters;
	delete[] bestParameters;
}

void RandomMaxPolicyGradientCalculator::setStepSize(int index,
RealType stepSize) {
	stepSizes[index] = stepSize;
}

GSearchPolicyGradientUpdater::GSearchPolicyGradientUpdater(
    GradientUpdateFunction *updateFunction,
    PolicyGradientCalculator *gradientCalculator, RealType s0,
    RealType epsilon) :
	GradientFunctionUpdater(updateFunction) {
	this->gradientCalculator = gradientCalculator;

	startParameters = new RealType[updateFunction->getNumWeights()];
	workParameters = new RealType[updateFunction->getNumWeights()];

	addParameters(gradientCalculator, "GSearch");
	addParameter("GSearchStartStepSize", s0);
	addParameter("GSearchEpsilon", epsilon);
	addParameter("GSearchUseLastStepSize", 0.0);

	addParameter("GSearchMinStepSize", s0 / 256);
	addParameter("GSearchMaxStepSize", s0 * 16);

	lastStepSize = s0;
}

GSearchPolicyGradientUpdater::~GSearchPolicyGradientUpdater() {
	delete[] startParameters;
	delete[] workParameters;
}

void GSearchPolicyGradientUpdater::setWorkingParamters(FeatureList *gradient,
RealType stepSize, RealType *startParameters, RealType *workParameters) {
	memcpy(workParameters, startParameters,
	    sizeof(RealType) * updateFunction->getNumWeights());

	FeatureList::iterator it = gradient->begin();
	for(; it != gradient->end(); it++) {
		workParameters[(*it)->featureIndex] += stepSize * (*it)->factor;
	}
}

void GSearchPolicyGradientUpdater::updateWeights(FeatureList *gradient) {
	RealType s = getParameter("GSearchStartStepSize");
//	RealType norm = sqrt(gradient->multFeatureList(gradient));

	if(getParameter("GSearchUseLastStepSize") > 0.5) {
		s = lastStepSize;
	}

	printf("Beginning GSEARCH with stepSize %f\n", s);

	RealType epsilon = getParameter("GSearchEpsilon");

	updateFunction->getWeights(startParameters);
	setWorkingParamters(gradient, s, startParameters, workParameters);

	updateFunction->setWeights(workParameters);
	FeatureList *newGradient = new FeatureList();
	gradientCalculator->getGradient(newGradient);

	RealType newGradientNorm = sqrt(newGradient->multFeatureList(newGradient));
	RealType prod = gradient->multFeatureList(newGradient); // * 1 / newGradientNorm;;
	RealType tempProd = prod;
	RealType sPlus = 0;
	RealType sMinus = 0;
	RealType pPlus = 0;
	RealType pMinus = 0;
	RealType sMin = getParameter("GSearchMinStepSize");
	RealType sMax = getParameter("GSearchMaxStepSize");

	printf("gradient * newgradient: %f\n", tempProd);

	if(prod < 0) {
		sPlus = s;

		while(tempProd < -epsilon && s > sMin) {
			sPlus = s;
			pPlus = tempProd;
			s = s / 2;

			printf("GSearch StepSize: %f ", s);

			setWorkingParamters(gradient, s, startParameters, workParameters);
			updateFunction->setWeights(workParameters);
			newGradient->clear();
			gradientCalculator->getGradient(newGradient);

			newGradientNorm = sqrt(newGradient->multFeatureList(newGradient));
			tempProd = gradient->multFeatureList(newGradient); // * 1 / newGradientNorm;

			printf("GSearch StepSize: %f, gradient * newGradient: %f\n", s,
			    tempProd);

		}
		sMinus = s;
		pMinus = tempProd;
		if(s < sMin) {
			s = sMin;
		}
	} else {
		sMinus = s;
		while(tempProd > epsilon && s < sMax) {
			sMinus = s;
			pMinus = tempProd;

			s = 2 * s;

			setWorkingParamters(gradient, s, startParameters, workParameters);
			updateFunction->setWeights(workParameters);
			newGradient->clear();

			gradientCalculator->getGradient(newGradient);

			newGradientNorm = sqrt(newGradient->multFeatureList(newGradient));
			tempProd = gradient->multFeatureList(newGradient); // * 1 / newGradientNorm;

			printf("GSearch StepSize: %f, gradient * newGradient: %f\n", s,
			    tempProd);
		}
		sPlus = s;
		pPlus = tempProd;

		if(s > sMax) {
			s = sMax;
		}
	}

	if(pMinus > 0 && pPlus < 0) {
		s = (pPlus * sMinus - pMinus * sPlus) / (pPlus - pMinus);
	} else {
		s = (sPlus + sMinus) / 2;
	}

	printf("GSearch: s: %f, s+ %f, s- %f, p+ %f, p- %f\n", s, sPlus, sMinus,
	    pPlus, pMinus);

	DebugPrint('g', "GSearch: s: %f, s+ %f, s- %f, p+ %f, p- %f\n", s, sPlus,
	    sMinus, pPlus, pMinus);

	setWorkingParamters(gradient, s, startParameters, workParameters);

	if(DebugIsEnabled('g')) {
		DebugPrint('g', "GSearch: Calculated StepSize %f\n", s);
		DebugPrint('g', "GSearch: New calculated Parameters\n");
		updateFunction->saveData(DebugGetFileHandle('g'));
	}

	lastStepSize = s;
	updateFunction->setWeights(workParameters);
	RealType normWeights = 0;

	for(int i = 0; i < updateFunction->getNumWeights(); i++) {
		normWeights += workParameters[i] * workParameters[i];
	}

	normWeights = sqrt(normWeights);
	printf("Weights Norm after Update %f\n", normWeights);
}

PolicyGradientWeightDecayListener::PolicyGradientWeightDecayListener(
    GradientUpdateFunction *updateFunction, RealType weightdecay) {
	addParameter("PolicyGradientWeightDecay", weightdecay);
	this->updateFunction = updateFunction;
	parameters = new RealType[updateFunction->getNumWeights()];
}

PolicyGradientWeightDecayListener::~PolicyGradientWeightDecayListener() {
	delete[] parameters;
}

void PolicyGradientWeightDecayListener::newEpisode() {
	updateFunction->getWeights(parameters);

	RealType factor = 1 - getParameter("PolicyGradientWeightDecay");
	for(int i = 0; i < updateFunction->getNumWeights(); i++) {
		parameters[i] = factor * parameters[i];
	}

	updateFunction->setWeights(parameters);
}

} //namespace rlearner

