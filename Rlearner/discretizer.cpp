// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cstring>
#include <algorithm>

#include "ril_debug.h"
#include "discretizer.h"

#include "statecollection.h"
#include "state.h"

namespace rlearner {

AbstractStateDiscretizer::AbstractStateDiscretizer(unsigned int numStates) :
	StateModifier(0, 1) {
	_type = DISCRETESTATE;
	setDiscreteStateSize(0, numStates);

	stateSubstitutions =
	    new std::map<int, std::pair<StateModifier *, State *>*>();
}

AbstractStateDiscretizer::~AbstractStateDiscretizer() {
	std::map<int, std::pair<StateModifier *, State *>*>::iterator it =
	    stateSubstitutions->begin();

	for(; it != stateSubstitutions->end(); it++) {
		delete (*it).second->second;
		delete (*it).second;
	}

	delete stateSubstitutions;
}

unsigned int AbstractStateDiscretizer::getDiscreteStateSize() {
	return StateProperties::getDiscreteStateSize(0);
}

void AbstractStateDiscretizer::getModifiedState(
    StateCollection *state, State *featState) {
//	assert(equals(featState->getStateProperties()));
	int discState = getDiscreteStateNumber(state);
	int stateOffset = 0;
	featState->resetState();

	std::map<int, std::pair<StateModifier *, State *>*>::iterator it =
	    stateSubstitutions->begin();

	while(it != stateSubstitutions->end() && (*it).first < discState) {
		stateOffset += (*it).second->first->getDiscreteStateSize() - 1;
		it++;
	}

	if(it != stateSubstitutions->end() && (*it).first == discState) {
		StateModifier *stateMod = (*it).second->first;
		State *stateBuf;

		if(state->isMember(stateMod)) {
			stateBuf = state->getState(stateMod);
		} else {
			stateBuf = (*it).second->second;
			stateMod->getModifiedState(state, stateBuf);
		}

		int type = stateMod->getType() & (FEATURESTATE | DISCRETESTATE);
		switch(type) {
		case FEATURESTATE: {
			featState->setNumActiveContinuousStates(
			    stateBuf->getNumActiveContinuousStates());
			featState->setNumActiveDiscreteStates(
			    stateBuf->getNumActiveDiscreteStates());
			unsigned int i;
			for(i = 0; i < featState->getNumActiveContinuousStates(); i++) {
				featState->setContinuousState(i,
				    stateBuf->getContinuousState(i));
			}

			for(i = 0; i < featState->getNumActiveDiscreteStates(); i++) {
				featState->setDiscreteState(i,
				    stateOffset + discState + stateBuf->getDiscreteState(i));
			}
			break;
		}
		case DISCRETESTATE: {
			featState->setNumActiveContinuousStates(0);
			featState->setNumActiveDiscreteStates(1);

			featState->setDiscreteState(0,
			    stateOffset + discState + stateBuf->getDiscreteState(0));
			break;
		}
		}
		//printf("Applied State Substitution: %d -> %d\n", discState, featState->getDiscreteState(0));
	} else {
		featState->setNumActiveContinuousStates(0);
		featState->setNumActiveDiscreteStates(1);

		featState->setDiscreteState(0, stateOffset + discState);
	}
}

void AbstractStateDiscretizer::addStateSubstitution(
    int discState, StateModifier *modifier) {
	std::pair<StateModifier *, State *> *stateMod = new std::pair<
	    StateModifier *, State *>(modifier, new State(modifier));

	(*stateSubstitutions)[discState] = stateMod;

	if(modifier->getNumContinuousStates() > this->getNumContinuousStates()) {
		_numContinuousStates = modifier->getNumContinuousStates();
	}

	if(modifier->getNumDiscreteStates() > this->getNumDiscreteStates()) {
		_numDiscreteStates = modifier->getNumDiscreteStates();
	}

	this->setDiscreteStateSize(0,
	    getDiscreteStateSize() + modifier->getDiscreteStateSize() - 1);

	if(modifier->isType(FEATURESTATE)) {
		_type = FEATURESTATE;
	}
}

void AbstractStateDiscretizer::removeStateSubstitution(int discState) {
	std::map<int, std::pair<StateModifier *, State *>*>::iterator it =
	    stateSubstitutions->find(discState);

	if(it != stateSubstitutions->end()) {
		delete (*it).second->second;
		delete (*it).second;

		stateSubstitutions->erase(it);
	}

}

ModelStateDiscretizer::ModelStateDiscretizer(
    StateProperties* prop, const std::vector<int>& discreteStates
):
	AbstractStateDiscretizer(calcDiscreteStateSize(prop, discreteStates)),
	originalState(prop), _discreteStates(discreteStates)
{}

ModelStateDiscretizer::ModelStateDiscretizer(
    StateProperties* prop, std::vector<int>&& discreteStates
):
	AbstractStateDiscretizer(calcDiscreteStateSize(prop, discreteStates)),
	originalState(prop), _discreteStates(std::move(discreteStates))
{}

ModelStateDiscretizer::~ModelStateDiscretizer() {}

unsigned int ModelStateDiscretizer::calcDiscreteStateSize(
    StateProperties *prop, const std::vector<int>& discreteStates
) {
	unsigned int dim = 1;

	if(!discreteStates.size()) {
		dim = prop->getDiscreteStateSize();
	} else {
		for(decltype(discreteStates.size()) i = 0; i < discreteStates.size(); i++) {
			dim *= prop->getDiscreteStateSize(discreteStates[i]);
		}
	}

	return dim;
}

unsigned int ModelStateDiscretizer::getDiscreteStateNumber(
    StateCollection *state
) {
	unsigned int dim = 1;
	unsigned int statenum = 0;

	if(!_discreteStates.size()) {
		for(unsigned int i = 0;
		    i < state->getState(originalState)->getNumDiscreteStates(); i++
		) {
			statenum += state->getState(originalState)->getDiscreteState(i) * dim;
			dim *= state->getState(originalState)->getStateProperties()->getDiscreteStateSize(i);
		}
	} else {
		for(unsigned int i = 0; i < _discreteStates.size(); i++) {
			statenum += state->getState(originalState)->getDiscreteState(
			    _discreteStates[i]) * dim;
			dim *=
			    state->getState(originalState)->getStateProperties()->getDiscreteStateSize(
			        _discreteStates[i]);
		}
	}
	return statenum;
}

SingleStateDiscretizer::SingleStateDiscretizer(
	int dimension, const std::vector<RealType>& partitions
): AbstractStateDiscretizer(partitions.size() + 1), _partitions(partitions),
	_dimension(dimension) {}

SingleStateDiscretizer::SingleStateDiscretizer(
	int dimension, std::vector<RealType>&& partitions
): AbstractStateDiscretizer(partitions.size() + 1),
	_partitions(std::move(partitions)), _dimension(dimension) {}

unsigned int SingleStateDiscretizer::getDiscreteStateNumber(
    StateCollection* state
) {
	RealType contState = state->getState()->getContinuousState(_dimension);
	auto iter = std::lower_bound(_partitions.begin(), _partitions.end(), contState);
	return std::distance(_partitions.begin(), iter);
}

void SingleStateDiscretizer::setOriginalState(StateProperties* originalState) {
	this->originalState = originalState;
}

DiscreteStateOperatorAnd::DiscreteStateOperatorAnd():
	AbstractStateDiscretizer(1) {}

DiscreteStateOperatorAnd::~DiscreteStateOperatorAnd() {}

unsigned int DiscreteStateOperatorAnd::getDiscreteStateNumber(
    StateCollection *state
) {
	int stateOffset = 1;
	int discState = 0;
	int ldiscState = 0;

	auto it = getModifierList()->begin();
	auto itStates = _states.begin();

	State *stateBuf;

	for(; it != getModifierList()->end(); itStates++, it++) {
		stateBuf = nullptr;
		if(state->isMember(*it)) {
			stateBuf = state->getState(*it);
			ldiscState = stateBuf->getDiscreteState(0);
		} else {
			stateBuf = (*itStates);
			(*it)->getModifiedState(state, stateBuf);
			ldiscState = stateBuf->getDiscreteState(0);
		}

		discState += ldiscState * stateOffset;
		stateOffset = stateOffset * (*it)->getDiscreteStateSize();
	}
	return discState;
}

void DiscreteStateOperatorAnd::addStateModifier(
    AbstractStateDiscretizer *featCalc) {
	StateMultiModifier::addStateModifier(featCalc);
	this->setDiscreteStateSize(0,
	    getDiscreteStateSize() * featCalc->getDiscreteStateSize());
}

} //namespace rlearner
