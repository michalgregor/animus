// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cinputdata_H_
#define Rlearner_cinputdata_H_

#include <list>
#include <vector>
#include <set>
#include <cstdio>

#include "algebra.h"
#include "system.h"

namespace rlearner {

class DataSet;

class DataPreprocessor {
public:
	virtual void preprocessInput(ColumnVector *input,
	        ColumnVector *preInput) = 0;
	void preprocessDataSet(DataSet *dataSet);
	
public:
	virtual ~DataPreprocessor() {}
};

class MeanStdPreprocessor: public DataPreprocessor {
protected:
	ColumnVector *mean;
	ColumnVector *std;

public:
	virtual void preprocessInput(ColumnVector *input, ColumnVector *preInput);

	void setMean(ColumnVector *mean);
	void setStd(ColumnVector *std);

public:
	MeanStdPreprocessor& operator=(const MeanStdPreprocessor&) = delete;
	MeanStdPreprocessor(const MeanStdPreprocessor&) = delete;

	MeanStdPreprocessor(ColumnVector *mean, ColumnVector *std);
	MeanStdPreprocessor(DataSet *dataSet);
	virtual ~MeanStdPreprocessor();
};

template<typename OutputValue> class Mapping {
protected:
	DataPreprocessor *preprocessor;
	int numDim;
	ColumnVector *buffVector;

protected:
	virtual OutputValue doGetOutputValue(ColumnVector *vector) = 0;

public:
	virtual OutputValue getOutputValue(ColumnVector *vector);
	virtual void save(std::ostream&) {}

	void setPreprocessor(DataPreprocessor *preprocessor);
	DataPreprocessor *getPreprocessor() {
		return preprocessor;
	}

	int getNumDimensions() {
		return numDim;
	}

	ColumnVector *getPreprocessedInput(ColumnVector *input);

public:
	Mapping& operator=(const Mapping&) = delete;
	Mapping(const Mapping&) = delete;

	Mapping(int numDim);
	virtual ~Mapping();
};

template<typename OutputValue>
Mapping<OutputValue>::Mapping(int l_numDim):
	preprocessor(nullptr), numDim(l_numDim), buffVector(new ColumnVector(numDim)) {}

template<typename OutputValue>
Mapping<OutputValue>::~Mapping() {
	delete buffVector;
}

template<typename OutputValue>
ColumnVector * Mapping<OutputValue>::getPreprocessedInput(
        ColumnVector *input) {
	if (preprocessor) {
		preprocessor->preprocessInput(input, buffVector);
	} else {
		*buffVector = *input;
	}
	return buffVector;
}

template<typename OutputValue>
OutputValue Mapping<OutputValue>::getOutputValue(ColumnVector *vector) {
	return doGetOutputValue(getPreprocessedInput(vector));
}

template<typename OutputValue>
void Mapping<OutputValue>::setPreprocessor(DataPreprocessor *l_preprocessor) {
	preprocessor = l_preprocessor;
}

SYS_WNONVIRT_DTOR_OFF
class DataSubset: public std::set<int> {
SYS_WNONVIRT_DTOR_ON
public:
	void addElements(std::list<int> *subsetList);

public:
	DataSubset() {}
	virtual ~DataSubset() {}
};

SYS_WNONVIRT_DTOR_OFF
class DataSet: public std::vector<ColumnVector *> {
SYS_WNONVIRT_DTOR_ON
protected:
	int numDimensions;

	ColumnVector *buffVector1;
	ColumnVector *buffVector2;

public:
	int getNumDimensions();
	virtual void addInput(ColumnVector *input);

	void saveCSV(std::ostream& stream);
	void loadCSV(std::istream& stream);

	virtual void getSubSet(DataSubset *subSet, DataSet *newSet);

	virtual void clear();

	RealType getVarianceNorm(DataSubset *dataSubset);
	void getVariance(DataSubset *dataSubset, ColumnVector *variance);

	void getMean(DataSubset *dataSubset, ColumnVector *mean);

public:
	DataSet& operator=(const DataSet&) = delete;
	DataSet(const DataSet&) = delete;

	DataSet(int numDimensions);
	DataSet(DataSet &dataset);
	virtual ~DataSet();
};

SYS_WNONVIRT_DTOR_OFF
class DataSet1D: public std::vector<RealType> {
SYS_WNONVIRT_DTOR_ON
public:
	void loadCSV(std::istream& stream);
	void saveCSV(std::ostream& stream);

	RealType getVariance(DataSubset *dataSubset, DataSet1D *weight = nullptr);
	RealType getMean(DataSubset *dataSubset, DataSet1D *weighting = nullptr);

public:
	DataSet1D(DataSet1D &dataset);
	DataSet1D();
};

} //namespace rlearner

#endif //Rlearner_cinputdata_H_
