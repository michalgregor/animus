// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>

#include "ril_debug.h"
#include "adaptivesoftmaxnetwork.h"

#include "featurefunction.h"
#include "gradientfunction.h"
#include "vfunction.h"
#include "errorlistener.h"
#include "linearfafeaturecalculator.h"
#include "inputdata.h"
#include "rbftrees.h"
#include "nearestneighbor.h"
#include "kdtrees.h"
#include "statecollection.h"
#include "state.h"

namespace rlearner {

RBFCenterNetwork::RBFCenterNetwork(
    std::vector<RBFBasisFunction *> *l_centers, int l_numDim) {
	centers = l_centers;
	initNetwork();

	numDim = l_numDim;
}

RBFCenterNetwork::RBFCenterNetwork(int l_numDim) {
	centers = new std::vector<RBFBasisFunction *>;

	numDim = l_numDim;
}

RBFCenterNetwork::~RBFCenterNetwork() {
	deleteCenters();
	delete centers;
}

void RBFCenterNetwork::saveData(std::ostream& stream) {
	for(int i = 0; i < getNumCenters(); i++) {
		RBFBasisFunction *basisFunction = getCenter(i);

		for(int j = 0; j < basisFunction->getCenter()->rows(); j++) {
			fmt::fprintf(stream, "%f ", basisFunction->getCenter()->operator[](j));
		}

		for(int j = 0; j < basisFunction->getSigma()->rows(); j++) {
			fmt::fprintf(stream, "%f ", basisFunction->getSigma()->operator[](j));
		}
		fmt::fprintf(stream, "\n");
	}
}

void RBFCenterNetwork::loadData(std::istream& stream) {
	deleteCenters();

	ColumnVector center(getNumDimensions());
	ColumnVector sigma(getNumDimensions());

	while(stream.good()) {
		int results = 0;
		RealType dBuf = 0;
		for(int j = 0; j < getNumDimensions(); j++) {
			results += xscanf(stream, "%lf ", dBuf);
			center.operator[](j) = dBuf;
		}

		for(int j = 0; j < getNumDimensions(); j++) {
			results += xscanf(stream, "%lf ", dBuf);
			sigma.operator[](j) = dBuf;
		}

		xscanf(stream, "\n");

		if(results < 2 * getNumDimensions()) {
			break;
		}

		RBFBasisFunction* basisFunction = new RBFBasisFunction(&center, &sigma);
		centers->push_back(basisFunction);
	}

	initNetwork();
}

RBFBasisFunction *RBFCenterNetwork::getCenter(int index) {
	return (*centers)[index];
}

int RBFCenterNetwork::getNumCenters() {
	return centers->size();
}

int RBFCenterNetwork::getNumDimensions() {
	return numDim;
}

void RBFCenterNetwork::deleteCenters() {
	std::vector<RBFBasisFunction *>::iterator it = centers->begin();

	for(; it != centers->end(); it++) {
		delete *it;
	}
	centers->clear();
}

RBFCenterNetworkSimpleSearch::RBFCenterNetworkSimpleSearch(int numDim) :
	RBFCenterNetwork(numDim) {
}

RBFCenterNetworkSimpleSearch::~RBFCenterNetworkSimpleSearch() {
}

void RBFCenterNetworkSimpleSearch::getNearestNeigbors(
    ColumnVector *state, unsigned int K, DataSubset *subset) {
	assert(K > 0);

	subset->clear();

	ColumnVector difference = *state;

	std::list<RealType> distSet;
	std::list<int> subsetList;

	for(int i = 0; i < getNumCenters(); i++) {
		difference.noalias() = *state - *(getCenter(i)->getCenter());
		RealType distance = difference.norm();

		if(distSet.size() < K || (*distSet.begin() > distance)) {
			std::list<RealType>::iterator itDist = distSet.begin();
			std::list<int>::iterator it = subsetList.begin();

			while(itDist != distSet.end() && (*itDist) > distance) {
				it++;
				itDist++;
			}
			if(distSet.size() > 0) {
				it--;
				itDist--;
			}
			distSet.insert(itDist, distance);
			subsetList.insert(it, i);
		}
		if(distSet.size() > K) {
			distSet.pop_back();
			subsetList.pop_back();
		}
	}

	std::list<int>::iterator it = subsetList.begin();

	for(; it != subsetList.end(); it++) {
		subset->insert(*it);
	}
}

void RBFCenterNetworkKDTree::initNetwork() {
	std::vector<RBFBasisFunction *>::iterator it = centers->begin();

	for(; it != centers->end(); it++) {
		dataset->addInput((*it)->getCenter());
	}

	if(tree) {
		delete tree;
		delete nearestNeighbors;
	}

	tree = new KDTree(dataset, 1);
	nearestNeighbors = new KNearestNeighbors(tree, dataset, K);
}

RBFCenterNetworkKDTree::RBFCenterNetworkKDTree(int numDim, unsigned int l_K) :
	RBFCenterNetwork(numDim) {
	K = l_K;
	tree = nullptr;
	nearestNeighbors = nullptr;

	dataset = new DataSet(getNumDimensions());
}

RBFCenterNetworkKDTree::~RBFCenterNetworkKDTree() {
	if(tree) {
		delete tree;
		delete nearestNeighbors;
	}
	delete dataset;
}

void RBFCenterNetworkKDTree::getNearestNeigbors(
    ColumnVector *state, unsigned int l_K, DataSubset *subset) {
	subset->clear();

	std::list<int> subsetList;

	nearestNeighbors->getNearestNeighbors(state, &subsetList, l_K);

	std::list<int>::iterator it = subsetList.begin();

	for(; it != subsetList.end(); it++) {
		subset->insert(*it);
	}

}

RBFCenterFeatureCalculator::RBFCenterFeatureCalculator(
    StateProperties *stateProperties, RBFCenterNetwork *l_network,
    unsigned int l_K) :
	FeatureCalculator(l_network->getNumCenters() + 1, l_K + 1) {
	network = l_network;

	normalizeFeat = true;
	useBias = true;

	setOriginalState(stateProperties);
}

RBFCenterFeatureCalculator::~RBFCenterFeatureCalculator() {

}

void RBFCenterFeatureCalculator::getModifiedState(
    StateCollection *stateCol, State *targetState) {
	State *state = stateCol->getState(originalState);

	DataSubset subset;

	targetState->resetState();

	// set Feature 0 for bias
	int bias = 0;
	if(useBias) {
		targetState->setContinuousState(0, 1.0);
		targetState->setDiscreteState(0, 0);

		bias = 1;
	}

	network->getNearestNeigbors(state, getNumActiveFeatures() - bias, &subset);

	targetState->setNumActiveContinuousStates(subset.size() + bias);
	targetState->setNumActiveDiscreteStates(subset.size() + bias);

	RealType sumFac = 0.0;

	DataSubset::iterator it = subset.begin();
	for(int i = 0; it != subset.end(); it++, i++) {
		RBFBasisFunction *basisFunction = network->getCenter(*it);

		targetState->setDiscreteState(i + bias, *it + bias);
		RealType factor = basisFunction->getActivationFactor(state);
		targetState->setContinuousState(i + bias, factor);
		sumFac += factor;
	}

	if(normalizeFeat) {
		for(unsigned int i = 0; i < subset.size(); i++) {
			targetState->setContinuousState(i + bias,
			    targetState->getContinuousState(i + bias) / sumFac);
		}
	}
}

} //namespace rlearner
