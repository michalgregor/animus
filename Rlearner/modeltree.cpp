// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "modeltree.h"

#include "datafactory.h"
#include "extratrees.h"

namespace rlearner {

LinearRegressionDataFactory::LinearRegressionDataFactory(
    DataSet *l_input, DataSet1D *l_output, int l_tresh1, int l_tresh2,
    int l_tresh3, RealType l_lambda) {
	input = l_input;
	output = l_output;

	tresh1 = l_tresh1;
	tresh2 = l_tresh2;
	tresh3 = l_tresh3;

	lambda = l_lambda;
}

LinearRegressionDataFactory::~LinearRegressionDataFactory() {}

Mapping<RealType> *LinearRegressionDataFactory::createTreeData(
    DataSubset *dataSubset, int) {
	int degree = 0;
	if(dataSubset->size() < (unsigned int) tresh1) {
		degree = 0;
	} else {
		if(dataSubset->size() < (unsigned int) tresh2) {
			degree = 1;
		} else {
			if(dataSubset->size() < (unsigned int) tresh3) {
				degree = 2;
			} else {
				degree = 3;
			}
		}
	}

	LinearRegression *regression = new LinearRegression(degree, input, output,
	    dataSubset);
	regression->lambda = lambda;

	return regression;
}

void LinearRegressionDataFactory::deleteData(Mapping<RealType> *linReg) {
	delete linReg;
}

RealType ModelTree::doGetOutputValue(ColumnVector *input) {
	Mapping<RealType> *linReg = tree->getOutputValue(input);
	return linReg->getOutputValue(input);
}

ModelTree::ModelTree(
    DataSet *inputData, SplittingConditionFactory *splittingFactory,
    TreeDataFactory<Mapping<RealType> *> *l_dataFactory) :
	Mapping<RealType>(inputData->getNumDimensions()) {
	tree = new Tree<Mapping<RealType> *>(inputData, splittingFactory,
	    l_dataFactory);
	deleteTree = true;
}

ModelTree::ModelTree(Tree<Mapping<RealType> *> *l_tree) :
	Mapping<RealType>(l_tree->getNumDimensions()) {
	tree = l_tree;
	deleteTree = false;
}

ModelTree::~ModelTree() {
	if(deleteTree && tree) {
		delete tree;
	}
}

Tree<Mapping<RealType> *> *ModelTree::getTree() {
	return tree;
}

ExtraModelTree::ExtraModelTree(
    DataSet *inputData, DataSet1D *outputData,
    TreeDataFactory<Mapping<RealType> *> *dataFactory, unsigned int K,
    unsigned int n_min, RealType outTresh) :
	    ModelTree(
	        new ExtraTree<Mapping<RealType> *>(inputData, outputData, dataFactory,
	            K, n_min, outTresh)) {
}

ExtraModelTree::~ExtraModelTree() {
	if(tree) {
		delete tree;
	}
}

ExtraLinearRegressionModelTree::ExtraLinearRegressionModelTree(
    DataSet *inputData, DataSet1D *outputData, unsigned int K,
    unsigned int n_min, RealType outTresh, int tresh1, int tresh2, int tresh3,
    RealType lambda) :
	    ExtraModelTree(inputData, outputData,
	        new LinearRegressionDataFactory(inputData, outputData, tresh1,
	            tresh2, tresh3, lambda), K, n_min, outTresh) {}

ExtraLinearRegressionModelTree::~ExtraLinearRegressionModelTree() {
	if(tree) {
		TreeDataFactory<Mapping<RealType> *> *dataFactory =
		    tree->getDataFactory();
		delete tree;
		delete dataFactory;
		tree = nullptr;
	}
}

} //namespace rlearner
