// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <algorithm>
#include <cstdio>
#include <iostream>

#include "algebra.h"
#include "kdtrees.h"
#include "datafactory.h"
#include "inputdata.h"

namespace rlearner {

KDTreeMedianSplittingFactory::KDTreeMedianSplittingFactory(
    DataSet *l_inputSet, int l_n_min) {
	n_min = l_n_min;
	inputData = l_inputSet;
}

KDTreeMedianSplittingFactory::~KDTreeMedianSplittingFactory() {}

SplittingCondition *KDTreeMedianSplittingFactory::createSplittingCondition(
    DataSubset *dataSubset) {
	DataSubset::iterator it = dataSubset->begin();

	ColumnVector minVector(inputData->getNumDimensions());
	ColumnVector maxVector(inputData->getNumDimensions());

	ColumnVector *vector = (*inputData)[*it];
	minVector = *vector;
	maxVector = *vector;

	it++;

	for(; it != dataSubset->end(); it++) {
		ColumnVector *vector = (*inputData)[*it];

		for(int i = 0; i < inputData->getNumDimensions(); i++) {
			if(vector->operator[](i) < minVector.operator[](i)) {
				minVector.operator[](i) = vector->operator[](i);
			}
			if(vector->operator[](i) > maxVector.operator[](i)) {
				maxVector.operator[](i) = vector->operator[](i);
			}
		}
	}

	maxVector = maxVector - minVector;

	//cout << "Dimension Width: " << maxVector.t();

	int index = 0;

	maxVector.maxCoeff(&index);
	index--;

	std::vector<RealType> median;

	it = dataSubset->begin();
	//printf("Values : ");
	RealType mean = 0.0;
	for(; it != dataSubset->end(); it++) {
		ColumnVector *vector = (*inputData)[*it];
//		median.push_back(vector->operator[](index));
//		printf("%f ", vector->operator[](index));
		mean += vector->operator[](index);
	}
	mean /= dataSubset->size();
	//std::sort(median.begin(), median.end());

	RealType treshold = mean; //median[median.size() / 2];

//	cout << "Dimension " << index << " Median " << treshold << endl;

	return new SplittingCondition1D(index, treshold);
}

bool KDTreeMedianSplittingFactory::isLeaf(DataSubset *dataSubset) {
	bool minSampels = dataSubset->size() < (unsigned int) n_min;

	RealType inputVar = inputData->getVarianceNorm(dataSubset);

//	printf("IsLeaf: %d %f\n", dataSubset->size(), inputVar);
	return minSampels || inputVar <= 0.000001;
}

KDTree::KDTree(DataSet *dataSet, int n_min) :
	Tree<DataSubset *>(dataSet->getNumDimensions()) {
	splittingFactory = new KDTreeMedianSplittingFactory(dataSet, n_min);
	Tree<DataSubset *>::createTree(dataSet, splittingFactory,
	    new SubsetFactory());
}

KDTree::~KDTree() {
	delete root;
	root = nullptr;
	delete dataFactory;
	delete splittingFactory;
}

void KDTree::addNewInput(int index) {
	Tree<DataSubset *>::addNewInput(index, splittingFactory);
}

} //namespace rlearner
