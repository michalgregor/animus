// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "datafactory.h"
#include "inputdata.h"

namespace rlearner {

RegressionFactory::RegressionFactory(
    DataSet1D *l_outputData, DataSet1D *weighting) {
	outputData = l_outputData;
	weightData = weighting;
}

RegressionFactory::~RegressionFactory() {
}

RealType RegressionFactory::createTreeData(DataSubset *dataSubset, int) {
	DataSubset::iterator it = dataSubset->begin();
	RealType mean = outputData->getMean(dataSubset, weightData);
	return mean;
}

DataSubset *SubsetFactory::createTreeData(DataSubset *dataSubset, int) {
	DataSubset *newSubSet = new DataSubset();

	DataSubset::iterator it = dataSubset->begin();

	for(; it != dataSubset->end(); it++) {
		newSubSet->insert(*it);
	}

	return newSubSet;
}

void SubsetFactory::deleteData(DataSubset *dataSet) {
	delete dataSet;
}

VectorQuantizationFactory::VectorQuantizationFactory(DataSet *l_inputData) {
	inputData = l_inputData;
}

VectorQuantizationFactory::~VectorQuantizationFactory() {
}

ColumnVector *VectorQuantizationFactory::createTreeData(
    DataSubset *dataSubset, int) {
	ColumnVector *mean = new ColumnVector(inputData->getNumDimensions());
	DataSubset::iterator it = dataSubset->begin();

	for(; it != dataSubset->end(); it++) {
		(*mean) = (*mean) + *(*inputData)[*it];
	}
	(*mean) = (*mean) / dataSubset->size();

	return mean;
}

void VectorQuantizationFactory::deleteData(ColumnVector *dataSet) {
	delete dataSet;
}

int LeafIndexFactory::createTreeData(DataSubset *, int numLeaves) {
	return numLeaves;
}

} //namespace rlearner
