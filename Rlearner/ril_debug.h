// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

/** @file ril_debug.h
 * This file contains the debugging routines for the RL Toolbox.
 * They where inspired by 'nachos'
 * Copyright (c) 1992-1993 The Regents of the University of California.
 *
 * The debugging routines allow the user to turn on selected
 * debugging messages and write then into different files.
 * The debug files can also be directly accessed. Enabling and
 * disabling of debug flags can easily be done by console or during
 * runtime.
 *
 * You are encouraged to add your own debugging flags. \n
 * The predefined debugging flags are: \n\n
 *  '+' -- turn on all debug messages \n
 *  'd' -- dynamic programming package \n
 *  'e' -- etraces \n
 *  'f' -- feature functions \n
 *  'p' -- policies \n
 *  'q' -- qfunctions \n
 *  't' -- tdleaners \n
 *  'v' -- torchfunction (gradient descent updates) \n
 **/

#ifndef Rlearner_ril_debug_H_
#define Rlearner_ril_debug_H_

#include <cstdio>
#include <limits>

#include "system.h"

#define MAX_EXP std::numeric_limits<RealType>::max_exponent10
#define MIN_EXP std::numeric_limits<RealType>::min_exponent10

namespace rlearner {

/**
 * The debug mode sets the file caching behavior of the debug system.
 *
 * In most cases speed of debugging is most important, in other cases
 * it is most important, that all debug data is saved in case of a program
 * crash. The debug mode allows the user to configure the debug system to
 * their needs.
 **/
enum DebugMode {
	//! Slowest mode; all debug files are closed immediately after writing.
	DebugModeCloseAlways,
	//! All debug files are flushed immediately after writing (default).
	DebugModeFlushAlways,
	//! Debug data is left cached in memory -- don't forget to close all debug
	//! files (DebugDisable) before finishing your program.
	DebugModeLeaveCached
};

//! Set the debug mode.
extern void DebugSetMode(DebugMode mode);

//! Get the actual debug mode.
extern DebugMode DebugGetMode();

/**
 * Initialize a debug file,  enable printing debug messages.
 *
 * all debug flags in 'flags' will be written into file 'filename'
 * if flgAppend = false and the file already exists it will be deleted.
 **/
extern void DebugInit(const std::string& filename, const std::string& flags, bool flgAppend = true);

/**
 * Disable (all) debug flags.
 *
 * All debug flags in 'flags' will be removed and their debug files closed
 * removes all flags if 'flags' = nullptr.
 **/
extern void DebugDisable(const std::string& flags = "");

//! Checks whether a debug flag is enabled.
extern bool DebugIsEnabled(char flag = 0);

/**
 * Get a handle to the debug file.
 *
 * If the flag is disabled, the return value is nullptr the file has to be
 * closed with DebugDisposeFileHandle(char flag) if no longer used.
 **/
extern std::ofstream* DebugGetFileHandlePtr(char flag);

extern std::ofstream& DebugGetFileHandle(char flag);

//! Disposes of a file handle acquired by DebugGetFileHandle(char flag).
extern void DebugDisposeFileHandle(char flag);

//! Print debug message into corresponding debug file if flag is enabled,
//! else do nothing.
template<typename... ArgTypes>
void DebugPrint(char flag, const std::string& format, ArgTypes... args) {
	std::ofstream* f = DebugGetFileHandlePtr(flag);

	if(f) {
		fmt::fprintf(*f, format.c_str(), args...);

		auto debugMode = DebugGetMode();

		if(debugMode == DebugModeFlushAlways) f->flush();
		else if(debugMode == DebugModeCloseAlways) {
			DebugDisposeFileHandle(flag);
		}
	}
}

} //namespace rlearner

#endif //Rlearner_ril_debug_H_
