// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "agentlistener.h"
#include "rewardfunction.h"
#include "ril_debug.h"

namespace rlearner {

/**
 * Boost serialization function.
 **/
void SemiMDPListener::serialize(
    IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<ParameterObject>(*this);
	ar & enabled;
}

void SemiMDPListener::serialize(
    OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<ParameterObject>(*this);
	ar & enabled;
}

SemiMDPRewardListener::SemiMDPRewardListener(RewardFunction *rewardFunction) {
	this->semiMDPRewardFunction = rewardFunction;
}

void SemiMDPRewardListener::nextStep(
    StateCollection *oldState, Action *action, StateCollection *newState) {
	nextStep(oldState, action,
	    semiMDPRewardFunction->getReward(oldState, action, newState), newState);
}

void SemiMDPRewardListener::intermediateStep(
    StateCollection *oldState, Action *action, StateCollection *nextState) {
	intermediateStep(oldState, action,
	    semiMDPRewardFunction->getReward(oldState, action, nextState),
	    nextState);
}

void SemiMDPRewardListener::setRewardFunction(RewardFunction *rewardFunction) {
	this->semiMDPRewardFunction = rewardFunction;
}

RewardFunction *SemiMDPRewardListener::getRewardFunction() {
	return semiMDPRewardFunction;
}

AdaptiveParameterFromNStepsCalculator::AdaptiveParameterFromNStepsCalculator(
    Parameters *targetObject, std::string targetParameter, int functionKind,
    int nStepsPerUpdate, RealType param0, RealType paramScale, RealType targetOffset,
    RealType targetScale) :
	    AdaptiveParameterUnBoundedValuesCalculator(targetObject,
	        targetParameter, functionKind, param0, paramScale, targetOffset,
	        targetScale) {
	targetValue = 0;
	this->nStepsPerUpdate = nStepsPerUpdate;
}

AdaptiveParameterFromNStepsCalculator::~AdaptiveParameterFromNStepsCalculator() {
}

void AdaptiveParameterFromNStepsCalculator::nextStep(
    StateCollection *, Action *, StateCollection *) {
	targetValue += 1;

	if(targetValue % nStepsPerUpdate == 0) {
		setParameterValue(targetValue);
	}
}

void AdaptiveParameterFromNStepsCalculator::resetCalculator() {
	targetValue = 0;
	setParameterValue(targetValue);
}

AdaptiveParameterFromNEpisodesCalculator::AdaptiveParameterFromNEpisodesCalculator(
    Parameters *targetObject, std::string targetParameter, int functionKind,
    RealType param0, RealType paramScale, RealType targetOffset, RealType targetScale) :
	    AdaptiveParameterUnBoundedValuesCalculator(targetObject,
	        targetParameter, functionKind, param0, paramScale, targetOffset,
	        targetScale) {
	targetValue = 0;
}

AdaptiveParameterFromNEpisodesCalculator::~AdaptiveParameterFromNEpisodesCalculator() {
}

void AdaptiveParameterFromNEpisodesCalculator::newEpisode() {
	targetValue += 1;
	setParameterValue(targetValue);

}

void AdaptiveParameterFromNEpisodesCalculator::resetCalculator() {
	targetValue = 0;
	setParameterValue(targetValue);
}

AdaptiveParameterFromAverageRewardCalculator::AdaptiveParameterFromAverageRewardCalculator(
    Parameters *targetObject, std::string targetParameter,
    RewardFunction *rewardFunction, int nStepsPerUpdate, int functionKind,
    RealType paramOffset, RealType paramScale, RealType targetMin, RealType targetMax,
    RealType alpha) :
	    AdaptiveParameterBoundedValuesCalculator(targetObject, targetParameter,
	        functionKind, paramOffset, paramScale, targetMin, targetMax),
	    SemiMDPRewardListener(rewardFunction) {
	this->alpha = alpha;
	addParameter("APRewardUpdateRate", alpha);
	this->nStepsPerUpdate = nStepsPerUpdate;
	nSteps = 0;
}

AdaptiveParameterFromAverageRewardCalculator::~AdaptiveParameterFromAverageRewardCalculator() {
}

void AdaptiveParameterFromAverageRewardCalculator::nextStep(
    StateCollection *, Action *, RealType reward, StateCollection *) {
	targetValue = targetValue * alpha + (1 - alpha) * reward;
	nSteps++;

	if(nSteps % nStepsPerUpdate == 0) {
		setParameterValue(targetValue);
	}
}

void AdaptiveParameterFromAverageRewardCalculator::onParametersChanged() {
	AdaptiveParameterBoundedValuesCalculator::onParametersChanged();
	alpha = getParameter("APRewardUpdateRate");
}

void AdaptiveParameterFromAverageRewardCalculator::resetCalculator() {
	targetValue = targetMin;
	nSteps = 0;
	setParameterValue(targetValue);

}

} //namespace rlearner
