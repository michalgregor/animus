// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>

#include "ril_debug.h"
#include "history.h"
#include "state.h"
#include "environmentmodel.h"
#include "action.h"
#include "statecollection.h"
#include "ril_debug.h"
#include "system.h"

namespace rlearner {

Step::Step(
    StateProperties *properties,
    const std::set<StateModifierList*>& modifierLists,
    ActionSet *actions
):
	StateObject(properties), ActionObject(actions)
{
	oldState = new StateCollectionImpl(properties, modifierLists);
	newState = new StateCollectionImpl(properties, modifierLists);
	action = nullptr;
	actionData = new ActionDataSet(actions);
}

Step::~Step() {
	delete oldState;
	delete newState;
	delete actionData;
}

StepHistory::StepHistory(StateProperties *properties, ActionSet *actions) :
	StateModifiersObject(properties), ActionObject(actions) {
}

BatchStepUpdate::BatchStepUpdate(
    SemiMDPListener *listener, StepHistory *logger, int numUpdatesPerStep,
    int numUpdatesPerEpisode, const std::set<StateModifierList*>& modifierLists
) {
	this->listener = listener;
	this->steps = logger;
	this->numUpdates = numUpdates;

	addParameter("BatchStepUpdatesPerEpisode", numUpdatesPerEpisode);
	addParameter("BatchStepUpdatesPerStep", numUpdatesPerStep);

	step = new Step(steps->getStateProperties(), modifierLists,
	    logger->getActions());
	dataSet = new ActionDataSet(logger->getActions());
}

BatchStepUpdate::~BatchStepUpdate() {
	delete step;
	delete dataSet;
}

void BatchStepUpdate::newEpisode() {
	simulateSteps(listener, (int) getParameter("BatchStepUpdatesPerEpisode"));
}

void BatchStepUpdate::nextStep(StateCollection *, Action *, StateCollection *) {
	simulateSteps(listener, (int) getParameter("BatchStepUpdatesPerStep"));
}

void BatchStepUpdate::simulateAllSteps(SemiMDPListener *listener) {
	simulateSteps(listener, steps->getNumSteps());
}

void BatchStepUpdate::simulateSteps(SemiMDPListener *listener, int num) {
	auto rand = make_rand();

	int rIndex = 0;
	int i = 0;

	ActionSet::iterator it = steps->getActions()->begin();
	for(; it != steps->getActions()->end(); it++) {
		dataSet->setActionData(*it, (*it)->getActionData());
	}

	for(i = 0; i < num && i < steps->getNumSteps(); i++) {
		rIndex = rand() % steps->getNumSteps();

		steps->getStep(rIndex, step);

		listener->newEpisode();

		step->action->loadActionData(
		    step->actionData->getActionData(step->action));
		listener->nextStep(step->oldState, step->action, step->newState);
	}

	it = steps->getActions()->begin();
	for(; it != steps->getActions()->end(); it++) {
		(*it)->loadActionData(dataSet->getActionData(*it));
	}
}

} //namespace rlearner
