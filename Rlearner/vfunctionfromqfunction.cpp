// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "ril_debug.h"
#include "vfunctionfromqfunction.h"

#include "policies.h"
#include "action.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "qfunction.h"
#include "vetraces.h"
#include "ril_debug.h"

namespace rlearner {

OptimalVFunctionFromQFunction::OptimalVFunctionFromQFunction(
    AbstractQFunction *qfunction, StateProperties *properties
):
	AbstractVFunction(properties)
{
	this->qFunction = qfunction;
	availableActions = new ActionSet();
}

OptimalVFunctionFromQFunction::~OptimalVFunctionFromQFunction() {
	delete availableActions;
}

RealType OptimalVFunctionFromQFunction::getValue(StateCollection *state) {
	qFunction->getActions()->getAvailableActions(availableActions, state);
	RealType value = qFunction->getMaxValue(state, availableActions);

	DebugPrint('v', "Optimal V-Function Value: %f\n", value);

	return value;
}

RealType OptimalVFunctionFromQFunction::getValue(State *state) {
	qFunction->getActions()->getAvailableActions(availableActions, state);
	return qFunction->getMaxValue(state, availableActions);
}

AbstractVETraces *OptimalVFunctionFromQFunction::getStandardETraces() {
	return nullptr;
}

VFunctionFromQFunction::VFunctionFromQFunction(
    AbstractQFunction *qfunction, StochasticPolicy *stochPolicy,
    StateProperties *properties) :
	OptimalVFunctionFromQFunction(qfunction, properties) {
	this->stochPolicy = stochPolicy;

	actionValues = new RealType[stochPolicy->getActions()->size()];
}

VFunctionFromQFunction::~VFunctionFromQFunction() {
	delete actionValues;
}

StochasticPolicy *VFunctionFromQFunction::getPolicy() {
	return this->stochPolicy;
}

void VFunctionFromQFunction::setPolicy(StochasticPolicy *policy) {
	this->stochPolicy = policy;
}

RealType VFunctionFromQFunction::getValue(State *state) {
	RealType qValue = 0;
	stochPolicy->getActions()->getAvailableActions(availableActions, state);

	if(availableActions->size() == 0) {
		return 0;
	}
	ActionSet::iterator it = availableActions->begin();
	stochPolicy->getActionProbabilities(state, availableActions, actionValues);

	for(int i = 0; it != availableActions->end(); it++, i++) {
		qValue += actionValues[i]
		    * ((AbstractQFunction *) qFunction)->getValue(state, *it);
	}
	return qValue;
}

RealType VFunctionFromQFunction::getValue(StateCollection *state) {
	RealType qValue = 0;
	stochPolicy->getActions()->getAvailableActions(availableActions, state);
	ActionSet::iterator it = availableActions->begin();
	stochPolicy->getActionProbabilities(state, availableActions, actionValues);

	for(int i = 0; it != availableActions->end(); it++, i++) {
		qValue += actionValues[i]
		    * ((AbstractQFunction *) qFunction)->getValue(state, *it);
	}
	return qValue;
}

} //namespace rlearner
