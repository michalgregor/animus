// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>
#include <cstring>

#include "system.h"

#include "continuousactions.h"
#include "policies.h"
#include "statecollection.h"
#include "vfunction.h"
#include "action.h"
#include "state.h"
#include "stateproperties.h"
#include "statemodifier.h"
#include "featurefunction.h"
#include "vetraces.h"

#include "ril_debug.h"

namespace rlearner {

ContinuousActionData::ContinuousActionData(
    ContinuousActionProperties *properties) :
	ColumnVector(properties->getNumActionValues()) {
	this->properties = properties;
}

ContinuousActionData::~ContinuousActionData() {
	//delete actionValues;
}

void ContinuousActionData::initData(RealType initVal) {
	for(int i = 0; i < rows(); i++) {
		operator[](i) = initVal;
	}
}

RealType ContinuousActionData::getDistance(ColumnVector *vector) {
	ColumnVector distance = *this;
	distance = distance - *vector;

	return distance.norm();
}

void ContinuousActionData::setData(ActionData *actionData) {
	if(isChangeAble()) {
		ContinuousActionData *contData =
		    dynamic_cast<ContinuousActionData *>(actionData);

		*this = *contData;
	}
}

void ContinuousActionData::setActionValue(int dim, RealType value) {
	if(isChangeAble()) {
		//assert(value <= properties->getMaxActionValue(dim) + 0.01 && value >= properties->getMinActionValue(dim) - 0.01);
		operator[](dim) = value;
	}
}

void ContinuousActionData::normalizeAction() {
	for(int dim = 0; dim < rows(); dim++) {
		if(operator[](dim) > properties->getMaxActionValue(dim)) {
			operator[](dim) = properties->getMaxActionValue(dim);
		} else {
			if(operator[](dim) < properties->getMinActionValue(dim)) {
				operator[](dim) = properties->getMinActionValue(dim);
			}
		}
	}
}

RealType ContinuousActionData::getActionValue(int dim) {
	return operator[](dim);
}

void ContinuousActionData::save(std::ostream& stream) {
	fmt::fprintf(stream, "[");
	for(unsigned int i = 0; i < properties->getNumActionValues(); i++) {
		fmt::fprintf(stream, "%lf ", operator[](i));
	}
	fmt::fprintf(stream, "]");
}

void ContinuousActionData::load(std::istream& stream) {
	xscanf(stream, "[");
	for(unsigned int i = 0; i < properties->getNumActionValues(); i++) {
		RealType buf;
		xscanf(stream, "%lf ", buf);
		operator[](i) = buf;
	}
	xscanf(stream, "]");
}

ContinuousActionProperties::ContinuousActionProperties(int numActionValues) {
	this->numActionValues = numActionValues;

	minValues = new RealType[numActionValues];
	maxValues = new RealType[numActionValues];

	for(int i = 0; i < numActionValues; i++) {
		minValues[i] = 0.0;
		maxValues[i] = 1.0;
	}
}

ContinuousActionProperties::~ContinuousActionProperties() {
	delete minValues;
	delete maxValues;
}

unsigned int ContinuousActionProperties::getNumActionValues() {
	return numActionValues;
}

RealType ContinuousActionProperties::getMinActionValue(int dim) {
	return minValues[dim];
}

RealType ContinuousActionProperties::getMaxActionValue(int dim) {
	return maxValues[dim];
}

void ContinuousActionProperties::setMinActionValue(int dim, RealType value) {
	minValues[dim] = value;
}

void ContinuousActionProperties::setMaxActionValue(int dim, RealType value) {
	maxValues[dim] = value;
}

ContinuousAction::ContinuousAction(
    ContinuousActionProperties *properties, ContinuousActionData *actionData) :
	PrimitiveAction(actionData) {
	continuousActionData = actionData;
	this->properties = properties;

	addType(AF_ContinuousAction);
}

ContinuousAction::ContinuousAction(ContinuousActionProperties *properties) :
	PrimitiveAction(new ContinuousActionData(properties)) {
	continuousActionData = dynamic_cast<ContinuousActionData *>(actionData);
	this->properties = properties;

	addType(AF_ContinuousAction);
}

ContinuousAction::~ContinuousAction() {
}

RealType ContinuousAction::getActionValue(int dim) {
	return continuousActionData->getActionValue(dim);
}

unsigned int ContinuousAction::getNumDimensions() {
	return (unsigned int) continuousActionData->rows();
}

bool ContinuousAction::equals(Action *action) {
	if(action->isType(AF_ContinuousStaticAction)) {
		StaticContinuousAction *staticAction =
		    dynamic_cast<StaticContinuousAction *>(action);
		return this == (staticAction->getContinuousAction());
	} else {
		return this == (action);
	}
}

bool ContinuousAction::isSameAction(Action *action, ActionData *data) {
	if(action->isType(AF_ContinuousAction)) {
		ContinuousAction *lcontAction = dynamic_cast<ContinuousAction *>(action);
		if(lcontAction->getContinuousActionProperties()
		    == getContinuousActionProperties()) {
			ContinuousActionData *lcontData;
			if(data) {
				lcontData = dynamic_cast<ContinuousActionData *>(data);
			} else {
				lcontData = lcontAction->getContinuousActionData();
			}
			ColumnVector distance = *continuousActionData - *lcontData;
			return distance.norm() < 0.0001;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

void ContinuousAction::loadActionData(ActionData *data) {
	PrimitiveAction::loadActionData(data);
	continuousActionData->normalizeAction();
}

ContinuousActionProperties *ContinuousAction::getContinuousActionProperties() {
	return properties;
}

ActionData *ContinuousAction::getNewActionData() {
	return dynamic_cast<ActionData *>(new ContinuousActionData(properties));
}

ContinuousActionController::ContinuousActionController(
    ContinuousAction *contAction, int l_randomControllerMode) :
	AgentController(new ActionSet()) {
	this->contAction = contAction;
	actions->add(contAction);
	randomController = nullptr;
	this->randomControllerMode = l_randomControllerMode;

	noise =
	    dynamic_cast<ContinuousActionData *>(contAction->getNewActionData());
}

ContinuousActionController::~ContinuousActionController() {
	delete actions;
	delete noise;
}

void ContinuousActionController::setRandomController(
    ContinuousActionRandomPolicy *randomController) {
	this->randomController = randomController;
	addParameters(randomController);

}

ContinuousActionRandomPolicy *ContinuousActionController::getRandomController() {
	return randomController;
}

void ContinuousActionController::setRandomControllerMode(
    int l_randomControllerMode) {
	this->randomControllerMode = l_randomControllerMode;
}

int ContinuousActionController::getRandomControllerMode() {
	return randomControllerMode;
}

Action *ContinuousActionController::getNextAction(
    StateCollection *state, ActionDataSet *dataSet) {
	assert(dataSet != nullptr);

	ContinuousActionData *actionData =
	    dynamic_cast<ContinuousActionData *>(dataSet->getActionData(contAction));

	getNextContinuousAction(state, actionData);

	if(randomController && randomControllerMode == RandomController_Extern) {
		randomController->getNextContinuousAction(state, noise);
		(*actionData) << (*actionData) + (*noise);
	}

	return contAction;
}

void ContinuousActionController::getNoise(
    StateCollection *state, ContinuousActionData *action,
    ContinuousActionData *l_noise) {
	int tempRandomMode = randomControllerMode;

	randomControllerMode = RandomController_NoRandom;

	getNextContinuousAction(state, l_noise);

	randomControllerMode = tempRandomMode;

	(*l_noise) << (*action) - (*l_noise);
}

StaticContinuousAction::StaticContinuousAction(
    ContinuousAction *contAction, RealType *actionValues, RealType l_maxDistance) :
	ContinuousAction(contAction->getContinuousActionProperties()) {
	this->contAction = contAction;

	if(actionValues != nullptr) {
		for(unsigned int i = 0; i < getNumDimensions(); i++) {
			continuousActionData->operator[](i) = actionValues[i];
		}
	}

	addType(AF_ContinuousStaticAction);

	actionData->setIsChangeAble(false);

	this->maximumDistance = l_maxDistance;
}

StaticContinuousAction::~StaticContinuousAction() {
}

RealType StaticContinuousAction::getMaximumDistance() {
	return maximumDistance;
}

void StaticContinuousAction::setContinuousAction(
    ContinuousActionData *contAction) {
	*contAction = *continuousActionData;
}

void StaticContinuousAction::addToContinuousAction(
    ContinuousActionData *contAction, RealType factor) {
	for(unsigned int i = 0; i < properties->getNumActionValues(); i++) {
		contAction->setActionValue(i,
		    contAction->getActionValue(i) + factor * getActionValue(i));
	}
}

ContinuousAction *StaticContinuousAction::getContinuousAction() {
	//contAction->loadActionData(getActionData());
	return contAction;
}

bool StaticContinuousAction::equals(Action *action) {
	if(action->isType(AF_ContinuousAction)
	    && !action->isType(AF_ContinuousStaticAction)) {
		return getContinuousAction() == action;
	} else {
		return this == (action);
	}
}

bool StaticContinuousAction::isSameAction(Action *action, ActionData *data) {
	if(action->isType(AF_ContinuousAction)
	    && !action->isType(AF_ContinuousStaticAction)) {
		ContinuousActionData *lcontData;
		if(data) {
			lcontData = dynamic_cast<ContinuousActionData *>(data);
		} else {
			lcontData =
			    dynamic_cast<ContinuousActionData *>(action->getActionData());
		}
		if(contAction->getContinuousActionProperties()
		    == getContinuousActionProperties()) {
			return continuousActionData->getDistance(lcontData) < 0.0001;
		} else {
			return false;
		}
	} else {
		return this == (action);
	}
}

ContinuousActionLinearFA::ContinuousActionLinearFA(
    ActionSet *contActions, ContinuousActionProperties *properties) {
	this->actionProperties = properties;
	this->contActions = contActions;

}

ContinuousActionLinearFA::~ContinuousActionLinearFA() {
}

void ContinuousActionLinearFA::getContinuousAction(
    ContinuousActionData *contAction, RealType *actionFactors) {
	contAction->initData(0.0);
	ActionSet::iterator it = contActions->begin();
	for(unsigned int i = 0; i < contActions->size(); it++, i++) {
		LinearFAContinuousAction *lFAcontAction =
		    dynamic_cast<LinearFAContinuousAction *>(*it);

		lFAcontAction->addToContinuousAction(contAction, actionFactors[i]);
	}

}

void ContinuousActionLinearFA::getActionFactors(
    ContinuousActionData *action, RealType *actionFactors) {
	RealType sum = 0.0;
	RealType val = 0.0;
	unsigned int i = 0;
	ActionSet::iterator it;
	for(i = 0, it = contActions->begin(); it != contActions->end(); it++, i++) {
		val = dynamic_cast<LinearFAContinuousAction *>(*it)->getActionFactor(
		    action);
		sum += val;
		actionFactors[i] = val;
	}
	assert(sum > 0);
	for(i = 0; i < contActions->size(); i++) {
		actionFactors[i] = actionFactors[i] / sum;
	}
}

void ContinuousActionLinearFA::getContinuousAction(
    unsigned int index, ContinuousActionData *action) {
	assert(index < contActions->size());
	unsigned int i = 0;

	ActionSet::iterator it;
	for(i = 0, it = contActions->begin(); it != contActions->end(), i < index;
	    it++, i++)
		;

	if(it != contActions->end()) {
		dynamic_cast<LinearFAContinuousAction *>((*it))->setContinuousAction(
		    action);
	}
}

int ContinuousActionLinearFA::getNumContinuousActionFA() {
	return contActions->size();
}

LinearFAContinuousAction::LinearFAContinuousAction(
    ContinuousAction *contAction, RealType *actionValues) :
	StaticContinuousAction(contAction, actionValues, 0) {
}

ContinuousRBFAction::ContinuousRBFAction(
    ContinuousAction *contAction, RealType *rbfCenter, RealType *rbfSigma) :
	LinearFAContinuousAction(contAction, rbfCenter) {
	this->rbfSigma = new RealType[properties->getNumActionValues()];

	if(rbfSigma != nullptr) {
		memcpy(this->rbfSigma, rbfSigma,
		    sizeof(RealType) * properties->getNumActionValues());
	} else {
		memset(this->rbfSigma, 0,
		    sizeof(RealType) * properties->getNumActionValues());
	}
	maximumDistance = 0;
}

ContinuousRBFAction::~ContinuousRBFAction() {
	delete rbfSigma;
}

RealType ContinuousRBFAction::getActionFactor(ContinuousActionData *dynAction) {
	RealType malDist = 0.0;
	RealType faktor = 0.0;

	for(unsigned int i = 0; i < properties->getNumActionValues(); i++) {
		malDist += pow(
		    (dynAction->getActionValue(i) - getActionValue(i)) / rbfSigma[i],
		    2);
	}
	malDist = malDist / 2;

	faktor = exp(-malDist);

	return faktor;
}

/*
 ContinuousActionVFunction::ContinuousActionVFunction(StateProperties *properties,  ContinuousActionProperties *actionProp) : AbstractVFunction(properties)
 {
 this->actionProp = actionProp;
 addType(CONTINUOUSVFUNCTION);
 }

 void ContinuousActionVFunction::updateValue(StateCollection *state, ContinuousAction *action, RealType td)
 {
 updateValue(state->getState(getStateProperties()), action, td);
 }

 void ContinuousActionVFunction::setValue(StateCollection *state, ContinuousAction *action, RealType qValue)
 {
 setValue(state->getState(getStateProperties()), action, qValue);
 }

 RealType ContinuousActionVFunction::getValue(StateCollection *state, ContinuousAction *action)
 {
 return getValue(state->getState(getStateProperties()), action);
 }
 */

ContinuousActionQFunction::ContinuousActionQFunction(
    ContinuousAction *contAction) :
	GradientQFunction(new ActionSet()) {
	this->contAction = contAction;
	actions->add(contAction);

	addType(AF_ContinuousActionQFUNCTION);
}

ContinuousActionQFunction::~ContinuousActionQFunction() {
	delete actions;
}

void ContinuousActionQFunction::updateCAValue(
    StateCollection *, ContinuousActionData *, RealType) {
}

void ContinuousActionQFunction::setCAValue(
    StateCollection *, ContinuousActionData *, RealType) {
}

void ContinuousActionQFunction::getCAGradient(
    StateCollection *, ContinuousActionData *, FeatureList *) {

}

void ContinuousActionQFunction::getWeights(RealType *) {
}

void ContinuousActionQFunction::setWeights(RealType *) {
}

Action *ContinuousActionQFunction::getMax(
    StateCollection *state, ActionSet *, ActionDataSet *actionDatas) {
	getBestContinuousAction(state,
	    dynamic_cast<ContinuousActionData *>(actionDatas->getActionData(
	        contAction)));
	return contAction;
}

void ContinuousActionQFunction::updateValue(
    StateCollection *state, Action *action, RealType td, ActionData *data) {
	if(data != nullptr) {
		updateCAValue(state, dynamic_cast<ContinuousActionData*>(data), td);
	} else {
		updateCAValue(state,
		    dynamic_cast<ContinuousAction*>(action)->getContinuousActionData(),
		    td);
	}
}

void ContinuousActionQFunction::setValue(
    StateCollection *state, Action *action, RealType qValue, ActionData *data) {
	if(data != nullptr) {
		setCAValue(state, dynamic_cast<ContinuousActionData*>(data), qValue);
	} else {
		setCAValue(state,
		    dynamic_cast<ContinuousAction*>(action)->getContinuousActionData(),
		    qValue);
	}
}

RealType ContinuousActionQFunction::getValue(
    StateCollection *state, Action *action, ActionData *data) {
	RealType value = 0.0;
	if(data != nullptr) {

		value = getCAValue(state, dynamic_cast<ContinuousActionData *>(data));
	} else {
		value =
		    getCAValue(state,
		        dynamic_cast<ContinuousAction *>(action)->getContinuousActionData());
	}

	if(!mayDiverge
	    && (value > DIVERGENTVFUNCTIONVALUE || value < - DIVERGENTVFUNCTIONVALUE)) {
		throw DivergentQFunctionException("Continuous Action Q-Function",
		    this, state->getState(), value);
	}
	return value;
}

void ContinuousActionQFunction::getGradient(
    StateCollection *state, Action *action, ActionData *data,
    FeatureList *gradient) {
	if(data) {
		getCAGradient(state, dynamic_cast<ContinuousActionData *>(data),
		    gradient);
	} else {
		getCAGradient(state,
		    dynamic_cast<ContinuousActionData *>(action->getActionData()),
		    gradient);
	}
}

CALinearFAQFunction::CALinearFAQFunction(
    QFunction *qFunction, ContinuousAction *contAction) :
	    ContinuousActionQFunction(contAction),
	    ContinuousActionLinearFA(qFunction->getActions(),
	        contAction->getContinuousActionProperties()) {
	this->qFunction = qFunction;

	actionFactors = new RealType[qFunction->getNumActions()];
	CAactionValues = new RealType[qFunction->getNumActions()];
	tempGradient = new FeatureList();
}

CALinearFAQFunction::~CALinearFAQFunction() {
	delete[] actionFactors;
	delete[] CAactionValues;
	delete tempGradient;
}

void CALinearFAQFunction::getBestContinuousAction(
    StateCollection *state, ContinuousActionData *actionData) {
	StaticContinuousAction *action =
	    dynamic_cast<StaticContinuousAction *>(qFunction->getMax(state,
	        qFunction->getActions()));

	actionData->setData(action->getActionData());

	/*RealType sum = 0.0;
	 RealType minVal = actionFactors[0];
	 for (unsigned int i = 0; i < qFunction->getActions()->size(); i++)
	 {
	 sum += actionFactors[i];
	 if (minVal > actionFactors[i])
	 {
	 minVal = actionFactors[i];
	 }
	 }

	 for (unsigned int i = 0; i < qFunction->getActions()->size(); i++)
	 {
	 if (sum - minVal * qFunction->getActions()->size() == 0)
	 {
	 actionFactors[i] = 1 / qFunction->getActions()->size();
	 }
	 else
	 {
	 actionFactors[i] = (actionFactors[i] - minVal) / (sum - minVal * qFunction->getActions()->size());
	 }
	 }*/

//	getContinuousAction(actionData, actionFactors);
}

void CALinearFAQFunction::updateCAValue(
    StateCollection *state, ContinuousActionData *data, RealType td) {
	getActionFactors(data, actionFactors);
	ActionSet::iterator it = qFunction->getActions()->begin();
	for(unsigned int i = 0; i < qFunction->getNumActions(); it++, i++) {
		if(actionFactors[i] > 0.0001) {
			qFunction->getVFunction(*it)->updateValue(state,
			    td * actionFactors[i]);
		}
	}
}

void CALinearFAQFunction::setCAValue(
    StateCollection *state, ContinuousActionData *data, RealType qValue) {
	getActionFactors(data, actionFactors);
	ActionSet::iterator it = qFunction->getActions()->begin();
	for(unsigned int i = 0; i < qFunction->getNumActions(); it++, i++) {
		DebugPrint('q',
		    "Set LinearFAQFunction: Action %d, Value %f, ActionFactor %f\n", i,
		    qValue, actionFactors[i]);
		if(actionFactors[i] > 0.0001) {
			qFunction->getVFunction(*it)->setValue(state,
			    qValue * actionFactors[i]);
		}
	}
}

RealType CALinearFAQFunction::getCAValue(
    StateCollection *state, ContinuousActionData *data) {
	getActionFactors(data, actionFactors);
	this->getQFunctionForCA()->getActionValues(state,
	    getQFunctionForCA()->getActions(), CAactionValues);
	RealType value = 0.0;

	for(unsigned int i = 0; i < qFunction->getNumActions(); i++) {
		value += CAactionValues[i] * actionFactors[i];
	}
	return value;
}

QFunction *CALinearFAQFunction::getQFunctionForCA() {
	return qFunction;
}

void CALinearFAQFunction::updateWeights(FeatureList *features) {
	qFunction->updateGradient(features, 1.0);
}

void CALinearFAQFunction::getCAGradient(
    StateCollection *state, ContinuousActionData *action,
    FeatureList *gradient) {
	getActionFactors(action, actionFactors);

	ActionSet::iterator it;
	int i = 0;

	for(it = this->contActions->begin(); it != contActions->end(); it++, i++) {
		tempGradient->clear();
		qFunction->getGradient(state, *it, nullptr, tempGradient);
		tempGradient->multFactor(actionFactors[i]);

		FeatureList::iterator itFeat = tempGradient->begin();

		for(; itFeat != tempGradient->end(); itFeat++) {
			if(fabs((*itFeat)->factor) > 0.00001) {
				gradient->update((*itFeat)->featureIndex, (*itFeat)->factor);
			}
		}
	}
}

int CALinearFAQFunction::getNumWeights() {
	return qFunction->getNumWeights();
}

AbstractQETraces* CALinearFAQFunction::getStandardETraces() {
	return new CALinearFAQETraces(this);
}

void CALinearFAQFunction::getWeights(RealType *weights) {
	qFunction->getWeights(weights);
}

void CALinearFAQFunction::setWeights(RealType *weights) {
	qFunction->setWeights(weights);
}

CALinearFAQETraces::CALinearFAQETraces(CALinearFAQFunction *qfunction) :
	QETraces(qfunction->getQFunctionForCA()) {
	contQFunc = qfunction;

	actionFactors = new RealType[qfunction->getNumContinuousActionFA()];
}

CALinearFAQETraces::~CALinearFAQETraces() {
	delete actionFactors;
}

void CALinearFAQETraces::addETrace(
    StateCollection *State, Action *action, RealType factor, ActionData *data) {
	if(action->isType(AF_ContinuousAction)) {
		ContinuousActionData *contAction = nullptr;
		if(data == nullptr) {
			contAction =
			    dynamic_cast<ContinuousActionData *>(action->getActionData());
		} else {
			contAction = dynamic_cast<ContinuousActionData *>(data);
		}
		contQFunc->getActionFactors(contAction, actionFactors);

		std::list<AbstractVETraces *>::iterator it = vETraces->begin();

		DebugPrint('e', "Adding CALinearFA Etraces: %f factor\n", factor);
		for(unsigned int i = 0; i < qFunction->getNumActions(); i++, it++) {
			(*it)->addETrace(State, factor * actionFactors[i]);
			DebugPrint('e', "%f ", actionFactors[i]);
		}
		DebugPrint('e', "\n");
	}
}

ContinuousActionPolicy::ContinuousActionPolicy(
    ContinuousAction *contAction, ActionDistribution *distribution,
    AbstractQFunction *continuousActionQFunc,
    ActionSet *continuousStaticActions) :
	ContinuousActionController(contAction) {
	this->distribution = distribution;
	this->continuousActionQFunc = continuousActionQFunc;
	this->continuousStaticActions = continuousStaticActions;
	actionValues = new RealType[continuousStaticActions->size()];

	addParameters(distribution);
}

ContinuousActionPolicy::~ContinuousActionPolicy() {
	delete[] actionValues;
}

void ContinuousActionPolicy::getNextContinuousAction(
    StateCollection *state, ContinuousActionData *action) {
	ActionSet availableActions;
	continuousStaticActions->getAvailableActions(&availableActions, state);

	continuousActionQFunc->getActionValues(state, &availableActions,
	    actionValues, nullptr);

	DebugPrint('p', "ContinuousActionPolicy ActionValues: ");

	for(unsigned int i = 0; i < availableActions.size(); i++) {
		DebugPrint('p', "%f ", actionValues[i]);
	}
	DebugPrint('p', "\n");

	distribution->getDistribution(state, &availableActions, actionValues);

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "ContinuousActionPolicy ActionFactors: ");

		for(unsigned int i = 0; i < availableActions.size(); i++) {
			DebugPrint('p', "%f ", actionValues[i]);
		}
		DebugPrint('p', "\n");
	}

	ActionSet::iterator it = availableActions.begin();

	int actionIndex = Distributions::getSampledIndex(actionValues,
	    availableActions.size());

	ColumnVector *sampledActionData = nullptr;

	for(int i = 0; i < actionIndex; i++, it++);

	StaticContinuousAction *sampledAction =
	    dynamic_cast<StaticContinuousAction *>(*it);
	sampledActionData = sampledAction->getContinuousActionData();

	it = availableActions.begin();
	action->initData(0.0);
	RealType sum = 0.0;

	for(int i = 0; it != availableActions.end(); it++, i++) {
		StaticContinuousAction *contAction =
		    dynamic_cast<LinearFAContinuousAction *>(*it);

		if(i == actionIndex
		    || contAction->getContinuousActionData()->getDistance(
		        sampledActionData) < sampledAction->getMaximumDistance()) {
			contAction->addToContinuousAction(action, actionValues[i]);
			sum += actionValues[i];
		}
	}

	(*action) *= (1.0 / sum);

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "ContinuousActionPolicy Calculated Action: ");
		action->save(DebugGetFileHandle('p'));
		DebugPrint('p', "\n");
	}
}

ContinuousActionRandomPolicy::ContinuousActionRandomPolicy(
    ContinuousAction *action, RealType sigma, RealType alpha) :
	ContinuousActionController(action) {
	addParameter("RandomPolicySigma", sigma);
	addParameter("RandomPolicySmoothFactor", alpha);

	lastNoise = new ColumnVector(action->getNumDimensions());
	lastNoise->setZero();

	currentNoise = new ColumnVector(action->getNumDimensions());
	currentNoise->setZero();

	this->sigma = sigma;
	this->alpha = alpha;
}

ContinuousActionRandomPolicy::~ContinuousActionRandomPolicy() {
	delete lastNoise;
	delete currentNoise;
}

void ContinuousActionRandomPolicy::onParametersChanged() {
	ContinuousActionController::onParametersChanged();

	sigma = getParameter("RandomPolicySigma");
	alpha = getParameter("RandomPolicySmoothFactor");
}

void ContinuousActionRandomPolicy::newEpisode() {
	lastNoise->setZero();

	for(unsigned int i = 0; i < getContinuousAction()->getNumDimensions();
	    i++) {
		RealType randValue = 0.0;
		if(sigma > 0.00001) {
			randValue = Distributions::getNormalDistributionSample(0.0, sigma);
		}
		DebugPrint('p', "Random  Controller : %f\n", randValue);
		currentNoise->operator[](i) = currentNoise->operator[](i) * alpha
		    + randValue;
	}
}

ColumnVector *ContinuousActionRandomPolicy::getCurrentNoise() {
	return currentNoise;
}

ColumnVector *ContinuousActionRandomPolicy::getLastNoise() {
	return lastNoise;
}

void ContinuousActionRandomPolicy::nextStep(
    StateCollection *, Action *, StateCollection *) {
//	RealType sigma = getParameter("RandomPolicySigma");
//	RealType alpha = getParameter("RandomPolicySmoothFactor");

	for(unsigned int i = 0; i < getContinuousAction()->getNumDimensions();
	    i++) {
		RealType randValue = 0.0;
		if(sigma > 0.00001) {
			randValue = Distributions::getNormalDistributionSample(0.0, sigma);
		}
		DebugPrint('p', "Random  Controller : %f, %f\n",
		    lastNoise->operator[](i), randValue);

		lastNoise->operator[](i) = currentNoise->operator[](i);
		currentNoise->operator[](i) = currentNoise->operator[](i) * alpha
		    + randValue;
	}
}

void ContinuousActionRandomPolicy::getNextContinuousAction(
    StateCollection *, ContinuousActionData *action) {
	for(int i = 0; i < action->rows(); i++) {
		action->setActionValue(i, currentNoise->operator[](i));
	}
}

ContinuousActionAddController::ContinuousActionAddController(
    ContinuousAction *action) :
	ContinuousActionController(action) {
	this->controllers = new std::list<ContinuousActionController *>();
	this->controllerWeights =
	    new std::map<ContinuousActionController*, RealType>();

	actionValues = new ColumnVector(action->getNumDimensions());
}

ContinuousActionAddController::~ContinuousActionAddController() {
	delete controllers;
	delete controllerWeights;

	delete actionValues;
}

void ContinuousActionAddController::getNextContinuousAction(
    StateCollection *state, ContinuousActionData *action) {
	std::list<ContinuousActionController *>::iterator it = controllers->begin();

	actionValues->setZero();
	RealType weightsSum = 0.0;
	for(; it != controllers->end(); it++) {
		action->initData(0.0);
		(*it)->getNextContinuousAction(state, action);
		RealType weight = getControllerWeight(*it);
		weightsSum += weight;
		(*action) *= weight;
		(*actionValues) << (*action) + (*actionValues);
	}
	(*action) << (*actionValues) / weightsSum;
}

void ContinuousActionAddController::addContinuousActionController(
    ContinuousActionController *controller, RealType weight) {
	controllers->push_back(controller);
	(*controllerWeights)[controller] = weight;
}

void ContinuousActionAddController::setControllerWeight(
    ContinuousActionController *controller, RealType weight) {
	(*controllerWeights)[controller] = weight;
}

RealType ContinuousActionAddController::getControllerWeight(
    ContinuousActionController *controller) {
	return (*controllerWeights)[controller];
}

} //namespace rlearner
