#include "ActionEncoder.h"

namespace rlearner {

/**
 * Boost serialization function.
 **/
void ActionEncoder::serialize(
    IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {
}
void ActionEncoder::serialize(
    OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {
}

/**
 * Empty body of the pure virtual destructor.
 */
ActionEncoder::~ActionEncoder() {
}

} //namespace rlearner
