#ifndef Rlearner_StateInitializer_H_
#define Rlearner_StateInitializer_H_

#include "system.h"
#include "state.h"
#include <vector>
#include <Systematic/generator/BoundaryGenerator.h>

namespace rlearner {

/**
 * @class StateInitializer
 * @author Michal Gregor
 *
 * The base of state initializers -- objects that can be used to produce
 * initial states (in some tasks we may want several ways of initializing the
 * state, such as choosing different starting points, randomization, etc.).
 */
class StateInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	/**
	 * This method initializes the state provided as the argument.
	 */
	virtual void initialize(State* state) = 0;

public:
	virtual ~StateInitializer() = default;
};

/**
 * A state initializer that initializes the state to a predefined
 * constant point.
 */
class StateInitializer_Constant: public StateInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	std::vector<int> _discreteDimensions;
	std::vector<RealType> _continuousDimensions;

public:
	void setDiscreteValue(unsigned int dim, int value);
	void setContinuousValue(unsigned int dim, RealType value);

	void setDiscreteValues(const std::vector<int>& values);
	void setContinuousValues(const std::vector<RealType>& values);

public:
	/**
	 * This method initializes the state provided as the argument.
	 */
	virtual void initialize(State* state);

public:
	StateInitializer_Constant();
	StateInitializer_Constant(
		const std::vector<int>& discreteValues,
		const std::vector<RealType> continuousValues
	);

	virtual ~StateInitializer_Constant() = default;
};

/**
 * A random StateInitializer, with a uniform distribution for each dimension of
 * the state.
 */
class StateInitializer_Uniform: public StateInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	std::vector<BoundaryGenerator<int> > _discreteDimensions;
	std::vector<BoundaryGenerator<RealType> > _continuousDimensions;

public:
	void setDiscreteBound(unsigned int dim, int lbound, int ubound);
	void setContinuousBound(unsigned int dim, RealType lbound, RealType ubound);

	void setDiscreteBounds(const std::vector<int>& lbounds, const std::vector<int>& ubounds);
	void setContinuousBounds(const std::vector<RealType>& lbounds, const std::vector<RealType>& ubounds);

public:
	/**
	 * This method initializes the state provided as the argument.
	 */
	virtual void initialize(State* state);

public:
	StateInitializer_Uniform();

	StateInitializer_Uniform(
		const std::vector<int>& lboundsDiscrete,
		const std::vector<int>& uboundsDiscrete,
		const std::vector<RealType>& lboundsContinuous,
		const std::vector<RealType>& uboundsContinuous
	);

	virtual ~StateInitializer_Uniform() = default;
};

} //namespace rlearner

#endif //Rlearner_StateInitializer_H_
