// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>

#include "ril_debug.h"
#include "state.h"
#include "environmentmodel.h"
#include "stateproperties.h"
#include "utility.h"

#include "system.h"
#include <Systematic/math/Compare.h>

namespace rlearner {

State::State(StateProperties *properties_) :
	StateObject(properties_),
	ColumnVector(properties_->getNumContinuousStates()),
	discreteState(new int[getNumDiscreteStates()]),
	numActiveContinuousStates(),
	numActiveDiscreteStates()
{
	resetState();
}

State::State(State *copy):
	StateObject(copy->getStateProperties()),
	ColumnVector(copy->getStateProperties()->getNumContinuousStates()),
	discreteState(new int[getNumDiscreteStates()]),
	numActiveContinuousStates(),
	numActiveDiscreteStates()
{
	resetState();
	setState(copy);
}

State::State(EnvironmentModel *model):
	StateObject(model->getStateProperties()),
	ColumnVector(model->getStateProperties()->getNumContinuousStates()),
	discreteState(new int[getNumDiscreteStates()]),
	numActiveContinuousStates(),
	numActiveDiscreteStates()
{
	resetState();
}

State::State(StateProperties* properties_, std::istream& stream):
	StateObject(properties_),
	ColumnVector(properties_->getNumContinuousStates()),
	discreteState(),
	numActiveContinuousStates(),
	numActiveDiscreteStates()
{
	properties = properties_;
	load(stream);

	numActiveContinuousStates = properties->getNumContinuousStates();
	numActiveDiscreteStates = properties->getNumDiscreteStates();
}

void State::resetState() {
	unsigned int i = 0;

	for(i = 0; i < getNumContinuousStates(); i++) {
		operator[](i) = 0;
	}

	for(i = 0; i < getNumDiscreteStates(); i++) {
		discreteState[i] = 0;
	}

	numActiveContinuousStates = properties->getNumContinuousStates();
	numActiveDiscreteStates = properties->getNumDiscreteStates();

	b_resetState = false;
}

void State::setState(State *copy) {
	unsigned int i = 0;
	assert(equalsModelProperties(copy));
	for(i = 0; i < getNumContinuousStates(); i++) {
		operator[](i) = copy->getContinuousState(i);
	}

	for(i = 0; i < getNumDiscreteStates(); i++) {
		discreteState[i] = copy->getDiscreteState(i);
	}

	numActiveContinuousStates = copy->getNumActiveContinuousStates();
	numActiveDiscreteStates = copy->getNumActiveDiscreteStates();

	b_resetState = copy->isResetState();
}

State::~State() {
	delete[] discreteState;
}

RealType State::getContinuousState(unsigned int dim) {
	assert(dim < getNumContinuousStates());
	return operator[](dim);
}

RealType State::getNormalizedContinuousState(unsigned int dim) {
	assert(dim < getNumContinuousStates());
	return (operator[](dim) - this->getStateProperties()->getMinValue(dim))
	    / (this->getStateProperties()->getMaxValue(dim)
	        - this->getStateProperties()->getMinValue(dim));
}

int State::getDiscreteState(unsigned int dim) {
	assert(dim < getNumDiscreteStates());
	return discreteState[dim];
}

int State::getDiscreteStateNumber() {
	int index = 0;
	int stateSize = 1;
	for(unsigned int i = 0; i < getNumDiscreteStates(); i++) {
		index += discreteState[i] * stateSize;
		stateSize *= properties->getDiscreteStateSize(i);
	}
	return index;
}

State* State::clone() {
	return new State(this);
}

void State::setContinuousState(unsigned int dim, RealType value_) {
	assert(dim < getNumContinuousStates());
	operator[](dim) = value_;

	if(properties->getPeriodicity(dim)) {
		if((operator[](dim) < properties->getMinValue(dim))
		    || (operator[](dim) > properties->getMaxValue(dim))) {
			RealType Period = properties->getMaxValue(dim) - properties->getMinValue(dim);
			assert(Period > 0);
			operator[](dim) =
			    operator[](dim)
			        - Period
			            * std::floor(
			                (operator[](dim)
			                    - properties->getMinValue(dim)) / Period);
		}
	} else {
		RealType minVal = properties->getMinValue(dim);
		if(this->operator[](dim) < minVal) {
			this->operator[](dim) = minVal;
		} else {
			RealType maxVal = properties->getMaxValue(dim);
			if(this->operator[](dim) > maxVal) {
				this->operator[](dim) = maxVal;
			}
		}
	}
}

void State::setDiscreteState(unsigned int dim, int value_) {
	assert(dim < getNumDiscreteStates());
	if((value_ < 0
	    || (unsigned int) value_ >= properties->getDiscreteStateSize(dim))
	    && properties->getDiscreteStateSize(dim) > 0) {
		fprintf(stderr,
		    "Error: Setting discrete state variable %d to %d, State Size is %d\n",
		    dim, value_, properties->getDiscreteStateSize(dim));
		assert(false);
	}

	discreteState[dim] = value_;
}

State *State::getState(StateProperties *) {
	return this;
}

State *State::getState() {
	return this;
}

bool State::isMember(StateProperties *stateModifier) {
	return getStateProperties() == stateModifier;
}

unsigned int State::getNumActiveDiscreteStates() {
	return numActiveDiscreteStates;
}

unsigned int State::getNumActiveContinuousStates() {
	return numActiveContinuousStates;
}

void State::setNumActiveDiscreteStates(int numActiveStates) {
	numActiveDiscreteStates = numActiveStates;
}

void State::setNumActiveContinuousStates(int numActiveStates) {
	numActiveContinuousStates = numActiveStates;
}

bool State::equals(State *state) {
	unsigned int i;
	if(!this->equalsModelProperties(state)) {
		return false;
	}
	for(i = 0; i < getNumContinuousStates(); i++) {
		if(!CompareSimply(getContinuousState(i),
		    state->getContinuousState(i))) {
			return false;
		}
	}
	for(i = 0; i < getNumDiscreteStates(); i++) {
		if(!CompareSimply(getDiscreteState(i), state->getDiscreteState(i))) {
			return false;
		}
	}
	return true;
}

void State::save(std::ostream& stream) {
	unsigned int i = 0;
	fmt::fprintf(stream, "[");
	for(i = 0; i < getNumContinuousStates(); i++) {
		fmt::fprintf(stream, "%lf ", getContinuousState(i));
	}
	for(i = 0; i < getNumDiscreteStates(); i++) {
		fmt::fprintf(stream, "%d ", (int) getDiscreteState(i));
	}
	fmt::fprintf(stream, "]");
}

void State::load(std::istream& stream) {
	unsigned int i;
	int bBuf;
	xscanf(stream, "[");
	for(i = 0; i < getNumContinuousStates(); i++) {
		RealType buf;
		xscanf(stream, "%lf ", buf);
		operator[](i) = buf;
	}
	for(i = 0; i < getNumDiscreteStates(); i++) {
		xscanf(stream, "%d ", bBuf);
		discreteState[i] = (bBuf != 0);
	}
	xscanf(stream, "]");
}

RealType State::getDistance(ColumnVector *vector) {
	ColumnVector distance = (*this) - (*vector);
	assert(rows() == vector->rows());

	return distance.norm();
}

RealType State::getSingleStateDifference(int i, RealType value_) {
	RealType distance = 0.0;
	if(properties->getPeriodicity(i)) {
		RealType period = properties->getMaxValue(i) - properties->getMinValue(i);
		assert(!CompareSimply(period, 0));
		distance = (operator[](i) - value_);
		if(distance < -period / 2) {
			distance += period;
		} else {
			if(distance > period / 2) {
				distance -= period;
			}
		}
	} else {
		distance = operator[](i) - value_;
	}
	return distance;
}

StateList::StateList(StateProperties* properties_) :
	    StateObject(properties_),
	    _continuousStates(),
	    _discreteStates(),
	    _resetStates(),
	    numStates(0) {
	unsigned int i;
	_continuousStates.assign(properties_->getNumContinuousStates(),
	    std::vector<RealType>());
	_discreteStates.assign(properties_->getNumDiscreteStates(),
	    std::vector<int>());
}

void StateList::addState(State *state) {
	unsigned int i;
	assert(state->equalsModelProperties(this));

	for(i = 0; i < getNumContinuousStates(); i++) {
		_continuousStates[i].push_back(state->getContinuousState(i));
	}
	for(i = 0; i < getNumDiscreteStates(); i++) {
		_discreteStates[i].push_back(state->getDiscreteState(i));
	}
	_resetStates.push_back(state->isResetState());

	numStates++;
}

void StateList::removeFirstState() {
	unsigned int i;
	for(i = 0; i < getNumContinuousStates(); i++) {
		_continuousStates[i].erase(_continuousStates[i].begin());
	}
	for(i = 0; i < getNumDiscreteStates(); i++) {
		_discreteStates[i].erase(_discreteStates[i].begin());
	}
	_resetStates.erase(_resetStates.begin());

	numStates--;
}

void StateList::removeLastState() {
	unsigned int i;
	for(i = 0; i < getNumContinuousStates(); i++) {
		_continuousStates[i].pop_back();
	}
	for(i = 0; i < getNumDiscreteStates(); i++) {
		_discreteStates[i].pop_back();
	}
	_resetStates.pop_back();

	numStates--;
}

unsigned int StateList::getNumStates() {
	return numStates;
}

void StateList::clear() {
	unsigned int i;

	for(i = 0; i < getNumContinuousStates(); i++) {
		_continuousStates[i].clear();
	}

	for(i = 0; i < getNumDiscreteStates(); i++) {
		_discreteStates[i].clear();
	}

	_resetStates.clear();
	numStates = 0;
}

void StateList::getState(unsigned int num, State *state) {
	unsigned int i;

	for(i = 0; i < getNumContinuousStates(); i++) {
		state->setContinuousState(i, _continuousStates[i][num]);
	}

	for(i = 0; i < getNumDiscreteStates(); i++) {
		state->setDiscreteState(i, _discreteStates[i][num]);
	}

	state->setResetState(_resetStates[num]);
}

void StateList::save(std::ostream& stream) {
	fmt::fprintf(stream, "States: %d\n", getNumStates());
	fmt::fprintf(stream, "\n");
	unsigned int i, j;

	for(i = 0; i < properties->getNumContinuousStates(); i++) {
		fmt::fprintf(stream, "ContinuousState %d:\n", i);
		for(j = 0; j < getNumStates(); j++) {
			fmt::fprintf(stream, "%f ", _continuousStates[i][j]);
		}
		fmt::fprintf(stream, "\n");
	}

	for(i = 0; i < properties->getNumDiscreteStates(); i++) {
		fmt::fprintf(stream, "DiscreteState %d:\n", i);
		for(j = 0; j < getNumStates(); j++) {
			fmt::fprintf(stream, "%d ", (int) _discreteStates[i][j]);
		}
		fmt::fprintf(stream, "\n");
	}

	fmt::fprintf(stream, "Reset States:\n");

	for(j = 0; j < getNumStates(); j++) {
		fmt::fprintf(stream, "%d ", (int) _resetStates[j]);
	}

	fmt::fprintf(stream, "\n");
}

void StateList::load(std::istream& stream) {
	int buf1, buf2, res;
	unsigned int i, j, buf;

	RealType dBuf;
	int bBuf;

	res = xscanf(stream, "States: %u\n", buf);
	SYS_THROW_ASSERT(res == 1, TracedError_InvalidFormat);
	xscanf(stream, "\n");

	for(i = 0; i < properties->getNumContinuousStates(); i++) {
		res = xscanf(stream, "ContinuousState %d:\n", buf1);
		SYS_THROW_ASSERT(res == 1, TracedError_InvalidFormat);

		for(j = 0; j < buf; j++) {
			SYS_THROW_ASSERT(xscanf(stream, "%lf ", dBuf) == 1, TracedError_InvalidFormat);
			_continuousStates[i].push_back(dBuf);
		}

		xscanf(stream, "\n");
	}

	for(i = 0; i < properties->getNumDiscreteStates(); i++) {
		res = xscanf(stream, "DiscreteState %d:\n", buf2);
		SYS_THROW_ASSERT(res == 1, TracedError_InvalidFormat);

		for(j = 0; j < buf; j++) {
			SYS_THROW_ASSERT(xscanf(stream, "%d ", bBuf) == 1, TracedError_InvalidFormat);
			_discreteStates[i].push_back(bBuf);
		}

		xscanf(stream, "\n");
	}

	res = xscanf(stream, "Reset States:\n");
	SYS_THROW_ASSERT(res == 0, TracedError_InvalidFormat);

	for(j = 0; j < buf; j++) {
		SYS_THROW_ASSERT(xscanf(stream, "%d ", bBuf) == 1, TracedError_InvalidFormat);
		_resetStates.push_back((bool) bBuf);
	}

	xscanf(stream, "\n");
	numStates = buf;
}

void StateList::initWithDiscreteStates(
    State *initState, std::vector<int> dimensions) {
	int numDiscStates = properties->getNumDiscreteStates();

	std::vector<int>::size_type numTakenStates = dimensions.size();
	assert(numTakenStates > 0);

	std::vector<int>::size_type i, j;
	int dim;

	// Calculate Size of State List
	std::vector<int> stateSize(numTakenStates);
	std::vector<int> curState(numTakenStates);

	std::vector<int>::size_type numStates_ = 1;
	for(i = 0; i < numTakenStates; i++) {
		dim = dimensions[i];
		assert(dim >= 0 && dim < numDiscStates);

		stateSize[i] = properties->getDiscreteStateSize(dim);
		if(stateSize[i] > 0) {
			numStates_ *= stateSize[i];
		}
		curState[i] = 0;
	}

	// Generate all States
	State dummyState(initState);

	bool increment;
	std::vector<int>::size_type curIndex;

	for(i = 0; i < numStates_; i++) {
		for(j = 0; j < numTakenStates; j++) {
			dim = dimensions[j];
			dummyState.setDiscreteState(dim, curState[j]);
		}

		addState(&dummyState);

		// Increment state
		curIndex = 0;
		increment = true;
		while(increment) {
			curState[curIndex]++;
			if(curState[curIndex] < stateSize[curIndex]) {
				increment = false;
			} else {
				curState[curIndex] = 0;
				curIndex++;
			}
			if(curIndex >= numTakenStates) increment = false;
		}
	}

}

void StateList::initWithContinuousStates(
    State *initState, std::vector<int> dimensions,
    std::vector<RealType> partitions) {

	int numContStates = properties->getNumContinuousStates();
	std::vector<int>::size_type numTakenStates = dimensions.size();
	assert(numTakenStates > 0);

	std::vector<int>::size_type i, j;
	int dim;

	// Calculate Size of State List
	std::vector<int> stateSize(numTakenStates);
	std::vector<int> curState(numTakenStates);
	std::vector<int>::size_type numStates_ = 1;

	for(i = 0; i < numTakenStates; i++) {
		dim = dimensions[i];
		assert(dim >= 0 && dim < numContStates);

		numStates_ *= numeric_cast<std::vector<int>::size_type>(
		    partitions[i] + 1);

		curState[i] = 0;
	}

	// Generate all States
	State dummyState(initState);

	bool increment;
	std::vector<int>::size_type curIndex;

	for(i = 0; i < numStates_; i++) {
		for(j = 0; j < numTakenStates; j++) {
			dim = dimensions[j];

			RealType width = properties->getMaxValue(dim)
			    - properties->getMinValue(dim);
			RealType x = properties->getMinValue(dim)
			    + curState[j] * width / partitions[j];

			dummyState.setContinuousState(dim, x);
		}

		addState(&dummyState);

		// Increment state
		curIndex = 0;
		increment = true;
		while(increment) {
			curState[curIndex]++;
			if(curState[curIndex] < stateSize[curIndex]) {
				increment = false;
			} else {
				curState[curIndex] = 0;
				curIndex++;
			}
			if(curIndex >= numTakenStates) increment = false;
		}
	}
}

} //namespace rlearner
