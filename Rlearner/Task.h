#ifndef Example_RlearnerStateChart_Task_H_
#define Example_RlearnerStateChart_Task_H_

#include "system.h"

#include "transitionfunction.h"
#include "rewardfunction.h"
#include "stateproperties.h"
#include "action.h"
#include "agent.h"
#include "discretizer.h"
#include "ActionEncoder.h"

namespace rlearner {

class Task {
public:
	/**
	 * Returns the reward function associated with the task. If there is
	 * a main reward function, this should be at index 0.
	 *
	 * \exception TracedError_OutOfRange Throws when index >= numRewardFunctions().
	 */
	virtual RewardFunction* getRewardFunction(unsigned int index) = 0;

	/**
	 * Returns the number of reward functions associated with the task.
	 */
	virtual unsigned int numRewardFunctions() const = 0;

	/**
	 * Returns the transition function.
	 */
	virtual TransitionFunction* getTransitionFunction() = 0;

	/**
	 * Creates a new action set with actions appropriate for the task.
	 */
	virtual ActionSet* newActionSet() = 0;

	/**
	 * Creates a new discretizer for the state space of the task.
	 */
	virtual AbstractStateDiscretizer* newDiscretizer() = 0;

	/**
	 * Returns the EnvironmentModel for the task. This is
	 * owned by the task.
	 */
	virtual EnvironmentModel* getEnvironment() = 0;

	/**
	 * Creates a new ActionEncoder able to encode actions of the type used
	 * by the Task.
	 */
	virtual ActionEncoder* newActionEncoder() = 0;

public:
	Task() = default;
	virtual ~Task() = default;
};

} //namespace rlearner

#endif //Example_RlearnerStateChart_Task_H_
