// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <memory>
#include <cstring>
#include <stdexcept>
#include <map>
#include <iostream>
#include <sstream>
#include <string>

#include "ril_debug.h"

namespace rlearner {

static DebugMode debugMode = DebugModeFlushAlways;

using DebugFileItem = std::pair<std::string, std::shared_ptr<std::ofstream> >;
using DebugFileMap = std::map<char, DebugFileItem>;

static DebugFileMap* debugFileMap = new DebugFileMap();

void DebugSetMode(DebugMode mode) {
	debugMode = mode;
}

DebugMode DebugGetMode() {
	return debugMode;
}

void DebugInit(const std::string& fileName, const std::string& flagList, bool flgAppend) {
	auto f = std::make_shared<std::ofstream>(fileName, flgAppend ? std::ofstream::app : std::ofstream::out);

	if(!f->good()) {
		throw std::runtime_error(
		    "RL Toolbox DEBUG subsystem: could not open debug file!");
	} else {
		if(debugMode == DebugModeCloseAlways) {
			f.reset();
		}

		for(unsigned int i = 0; i < flagList.size(); i++) {
			auto it = debugFileMap->find(flagList[i]);

			if(it != debugFileMap->end()) {
				debugFileMap->erase(it);
			}

			(*debugFileMap)[flagList[i]] = DebugFileItem(fileName, f);
		}
	}
}

void DebugDisable(const std::string& flagList) {
	if(!flagList.size()) {
		debugFileMap->clear();
	} else {
		for(unsigned int i = 0; i < flagList.size(); i++) {
			auto it = debugFileMap->find(flagList[i]);
			if(it != debugFileMap->end()) {
				debugFileMap->erase(it);
			}
		}
	}
}

bool DebugIsEnabled(char flag) {
	if(debugFileMap->size() > 0) {
		if(flag == 0) {
			return true;
		} else {
			if(debugFileMap->find(flag) == debugFileMap->end())
				return debugFileMap->find('+') != debugFileMap->end();
			else return true;
		}
	} else return false;
}

std::ofstream& DebugGetFileHandle(char flag) {
	auto f = DebugGetFileHandlePtr(flag);
	if(!f) throw TracedError_NotAvailable("No debug file with flag " + std::to_string(flag) + "available.");
	else return *f;
}

std::ofstream* DebugGetFileHandlePtr(char flag) {
	if(!debugFileMap->size())
		return nullptr;

	auto it = debugFileMap->find(flag);
	if(it == debugFileMap->end()) {
		if(flag == '+') {
			it = debugFileMap->begin();
		} else {
			return DebugGetFileHandlePtr('+');
		}
	}

	auto f = (*it).second.second;

	if(!f) {
		f = std::make_shared<std::ofstream>((*it).second.first, std::ofstream::app);
		(*it).second.second = f;
	}

	if(!f->good()) {
		throw std::runtime_error("RL Toolbox DEBUG subsystem: could not open debug file for writing!");
	} else {
		return f.get();
	}
}

void DebugDisposeFileHandle(char flag) {
	if(debugFileMap->size() > 0 && debugMode != DebugModeLeaveCached) {
		auto it = debugFileMap->find(flag);

		if(it == debugFileMap->end()) {
			it = debugFileMap->find('+');
			if(it == debugFileMap->end()) return;
		}

		auto& f = (*it).second.second;

		if(f) {
			if(debugMode == DebugModeFlushAlways) f->flush();
			else if(debugMode == DebugModeCloseAlways) {
				f.reset();
			}
		}
	}
}

} //namespace rlearner
