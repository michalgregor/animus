// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>
#include <cstring>

#include "ril_debug.h"
#include "statemodifier.h"
#include "statecollection.h"
#include "state.h"

namespace rlearner {

StateModifier::StateModifier(
    unsigned int numContinuousStates, unsigned int numDiscreteStates) :
	StateProperties(numContinuousStates, numDiscreteStates
) {
	stateCollections = new std::list<StateCollectionImpl *>;

	this->changeState = false;
}

StateModifier::StateModifier():
	StateProperties()
{
	stateCollections = new std::list<StateCollectionImpl *>;
	this->changeState = false;
}

StateModifier::~StateModifier() {
	delete stateCollections;
}

void StateModifier::registerStateCollection(
    StateCollectionImpl *stateCollection) {
	stateCollections->push_back(stateCollection);
}

void StateModifier::removeStateCollection(
    StateCollectionImpl *stateCollection) {
	stateCollections->remove(stateCollection);
}

void StateModifier::stateChanged() {
	std::list<StateCollectionImpl *>::iterator it = stateCollections->begin();

	for(; it != stateCollections->end(); it++) {
		(*it)->setIsStateCalculated(this, false);
	}
}

NeuralNetworkStateModifier::NeuralNetworkStateModifier(
    StateProperties *originalState) :
	StateModifier(getNumInitContinuousStates(originalState, nullptr, 0), 0) {
	this->originalState = originalState;

	for(unsigned int i = 0; i < getNumContinuousStates(); i++) {
		setMinValue(i, -10000.0);
		setMaxValue(i, 10000.0);
	}

	numDim = 0;
	dimensions = nullptr;

	input_mean = new ColumnVector(numDim);
	input_std = new ColumnVector(numDim);
	input_mean->setZero();
	input_std->setConstant(1);

	// preserve normalization on the intervall [-1 1]

	for(unsigned int i = 0; i < numDim; i++) {
		input_mean->operator[](i) = (originalState->getMaxValue(dimensions[i])
		    + originalState->getMinValue(dimensions[i])) / 2.0;

		input_std->operator[](i) = (originalState->getMaxValue(dimensions[i])
		    - originalState->getMinValue(dimensions[i])) / 2.0;
	}

	buffVector = new ColumnVector(numDim);
}

NeuralNetworkStateModifier::NeuralNetworkStateModifier(
    StateProperties *originalState, unsigned int *l_dimensions,
    unsigned int l_numDim
):
	StateModifier(
		getNumInitContinuousStates(originalState, l_dimensions, l_numDim),
		0)
{
	this->originalState = originalState;

	this->numDim = l_numDim;
	dimensions = new unsigned int[numDim];
	memcpy(dimensions, l_dimensions, sizeof(int) * numDim);

	normValues = false;

	for(unsigned int i = 0; i < getNumContinuousStates(); i++) {
		setMinValue(i, -100000.0);
		setMaxValue(i, 100000.0);
	}

	input_mean = new ColumnVector(numDim);
	input_std = new ColumnVector(numDim);

	input_mean->setZero();
	input_std->setConstant(1);

	// preserve normalization on the interval [-1 1]

	for(unsigned int i = 0; i < numDim; i++) {
		input_mean->operator[](i) = (originalState->getMaxValue(dimensions[i])
		    + originalState->getMinValue(dimensions[i])) / 2.0;

		input_std->operator[](i) = (originalState->getMaxValue(dimensions[i])
		    - originalState->getMinValue(dimensions[i])) / 2.0;
	}

	buffVector = new ColumnVector(numDim);
}

NeuralNetworkStateModifier::~NeuralNetworkStateModifier() {
	delete dimensions;
	delete input_mean;
	delete input_std;
	delete buffVector;
}

int NeuralNetworkStateModifier::getNumInitContinuousStates(
    StateProperties *properties, unsigned int *dimensions,
    unsigned int numDim) {

	int numConStates = properties->getNumContinuousStates();

	if(numDim == 0) {
		for(unsigned int i = 0; i < properties->getNumContinuousStates(); i++) {
			if(properties->getPeriodicity(i)) {
				numConStates++;
			}
		}
	} else {
		numConStates = numDim;
		for(unsigned int i = 0; i < numDim; i++) {
			if(properties->getPeriodicity(dimensions[i])) {
				numConStates++;
			}
		}
	}

	return numConStates;
}

void NeuralNetworkStateModifier::getModifiedState(
    StateCollection *originalStateCol, State *modifiedState) {
	int contStateIndex = 0;

	// set Discrete States
	State *state = originalStateCol->getState(originalState);

	buffVector->setZero();

	if(dimensions == nullptr) {
		for(unsigned int i = 0; i < originalState->getNumContinuousStates();
		    i++) {
			buffVector->operator[](i) = state->getContinuousState(i);
		}

		if(normValues) {
			preprocessInput(buffVector, buffVector);
		}

		for(unsigned int i = 0; i < originalState->getNumContinuousStates();
		    i++) {
			RealType stateVal = buffVector->operator[](i);

			if(originalState->getPeriodicity(i)) {
				modifiedState->setContinuousState(contStateIndex++,
				    sin(stateVal * Constant::pi));
				modifiedState->setContinuousState(contStateIndex++,
				    cos(stateVal * Constant::pi));
			} else {
				modifiedState->setContinuousState(contStateIndex++, stateVal);
			}
		}
	} else {
		for(unsigned int i = 0; i < numDim; i++) {
			buffVector->operator[](i) = state->getContinuousState(
			    dimensions[i]);
		}

		if(normValues) {
			preprocessInput(buffVector, buffVector);
		}

		for(unsigned int i = 0; i < numDim; i++) {
			RealType stateVal = buffVector->operator[](i);

			if(originalState->getPeriodicity(dimensions[i])) {
				modifiedState->setContinuousState(contStateIndex,
				    sin(stateVal * Constant::pi));
				contStateIndex++;
				modifiedState->setContinuousState(contStateIndex,
				    cos(stateVal * Constant::pi));
				contStateIndex++;
			} else {
				modifiedState->setContinuousState(contStateIndex, stateVal);
				contStateIndex++;
			}
		}
	}
}

void NeuralNetworkStateModifier::preprocessInput(
    ColumnVector *input, ColumnVector *norm_input) {
	for(int i = 0; i < input->rows(); i++) {
		norm_input->operator[](i) = (input->operator[](i)
		    - input_mean->operator[](i)) / input_std->operator[](i);
	}
}

void NeuralNetworkStateModifier::setPreprocessing(
    ColumnVector *l_input_mean, ColumnVector *l_input_std) {
	*input_mean = *l_input_mean;
	*input_std = *l_input_std;
	normValues = true;
}

FeatureCalculator::FeatureCalculator(
    unsigned int numFeatures, unsigned int numActiveFeatures) :
	StateModifier() {
	initFeatureCalculator(numFeatures, numActiveFeatures);
	originalState = nullptr;
}

FeatureCalculator::FeatureCalculator() :
	StateModifier() {
	numFeatures = 0;
	numActiveFeatures = 0;
	originalState = nullptr;
}

void FeatureCalculator::initFeatureCalculator(
    unsigned int numFeatures, unsigned int numActiveFeatures) {
	StateProperties::initProperties(numActiveFeatures, numActiveFeatures,
	    FEATURESTATE);

	for(unsigned int i = 0; i < this->getNumDiscreteStates(); i++) {
		this->setDiscreteStateSize(i, numFeatures);
	}

	this->numFeatures = numFeatures;
	this->numActiveFeatures = numActiveFeatures;
}

unsigned int FeatureCalculator::getDiscreteStateSize(unsigned int) {
	return numFeatures;
}

unsigned int FeatureCalculator::getDiscreteStateSize() {
	return numFeatures;
}

RealType FeatureCalculator::getMin(unsigned int) {
	return 0.0;
}

RealType FeatureCalculator::getMax(unsigned int) {
	return 1.0;
}

unsigned int FeatureCalculator::getNumFeatures() {
	return numFeatures;
}

unsigned int FeatureCalculator::getNumActiveFeatures() {
	return numActiveFeatures;
}

void FeatureCalculator::normalizeFeatures(State *featState) {
	RealType sum = 0.0;
	unsigned int i = 0;
	for(i = 0; i < featState->getNumActiveDiscreteStates(); i++) {
		sum += featState->getContinuousState(i);
	}

	if(sum > 0) {
		for(i = 0; i < featState->getNumActiveDiscreteStates(); i++) {
			featState->setContinuousState(i,
			    featState->getContinuousState(i) / sum);
		}
	}
}

StateMultiModifier::StateMultiModifier(): _states(), _modifierList() {}

StateMultiModifier::~StateMultiModifier() {
	for(auto& state: _states) {
		delete state;
	}
}

void StateMultiModifier::addStateModifier(StateModifier* modifier) {
	_modifierList.addStateModifier(modifier);
	_states.push_back(new State(modifier));
}

StateVariablesChooser::StateVariablesChooser(
    unsigned int numContStates, unsigned int *l_contStatesInd,
    unsigned int numDiscStates, unsigned int *l_discStatesInd,
    StateProperties *originalState) :
	StateModifier(numContStates, numDiscStates) {
	contStatesInd = nullptr;
	discStatesInd = nullptr;

	if(numContStates > 0) {
		contStatesInd = new unsigned int[numContStates];
		memcpy(contStatesInd, l_contStatesInd,
		    sizeof(unsigned int) * numContStates);
	}

	if(numDiscStates > 0) {
		discStatesInd = new unsigned int[numDiscStates];

		memcpy(discStatesInd, l_discStatesInd,
		    sizeof(unsigned int) * numDiscStates);
	}

	this->originalState = originalState;

	for(int i = 0; i < getNumContinuousStates(); i++) {
		setMinValue(i, originalState->getMinValue(contStatesInd[i]));
		setMaxValue(i, originalState->getMaxValue(contStatesInd[i]));
	}

	for(int i = 0; i < getNumDiscreteStates(); i++) {
		setDiscreteStateSize(i,
		    originalState->getDiscreteStateSize(discStatesInd[i]));
	}

}

StateVariablesChooser::~StateVariablesChooser() {
	if(contStatesInd) {
		delete contStatesInd;
	}

	if(discStatesInd) {
		delete discStatesInd;
	}
}

void StateVariablesChooser::getModifiedState(
    StateCollection *originalStateCol, State *modifiedState) {
	State *origState = originalStateCol->getState(originalState);

	for(unsigned int i = 0; i < getNumContinuousStates(); i++) {
		modifiedState->setContinuousState(i,
		    origState->getContinuousState(contStatesInd[i]));
	}

	for(unsigned int i = 0; i < getNumDiscreteStates(); i++) {
		modifiedState->setDiscreteState(i,
		    origState->getDiscreteState(discStatesInd[i]));
	}
}

} //namespace rlearner
