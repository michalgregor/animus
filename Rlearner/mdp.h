#ifndef RLEARNER_MDP_H_
#define RLEARNER_MDP_H_

#include "action.h"
#include "baseobjects.h"
#include "agentcontroller.h"
#include "hierarchiccontroller.h"
#include "StateModifierList.h"

namespace rlearner {

class SemiMDPListener;
class Episode;
class EnvironmentModel;
class StateCollection;
class StateCollectionImpl;

/**
 * @class SemiMDPSender
 * Class for sending the State-Action-State Tuple to the Listeners.
 *
 * Maintains a List of SemiMDPListeners. The class provides methods for
 * sending the Listeners that a new Episode has started, a new Step
 * (State-Action-State Tuple) or an intermediate Step has occurred.
 *
 * @see SemiMDPListener
**/
class SemiMDPSender {
private:
	typedef std::list<SemiMDPListener*> list_type;

public:
	typedef list_type::value_type value_type;
	typedef list_type::reference reference;
	typedef list_type::const_reference const_reference;
	typedef list_type::pointer pointer;
	typedef list_type::const_pointer const_pointer;
	typedef list_type::iterator iterator;
	typedef list_type::const_iterator const_iterator;
	typedef list_type::reverse_iterator reverse_iterator;
	typedef list_type::const_reverse_iterator const_reverse_iterator;
	typedef list_type::difference_type difference_type;
	typedef list_type::size_type size_type;

private:
	typedef std::map<SemiMDPListener*, iterator> index_type;

private:
	//! The listeners in the order of addition.
	list_type _listeners;
	//! An index for quick lookup by SemiMDPListener*.
	index_type _index;

public:
	iterator begin() {
		return _listeners.begin();
	}

	const_iterator begin() const {
		return _listeners.begin();
	}

	iterator end() {
		return _listeners.end();
	}

	const_iterator end() const {
		return _listeners.end();
	}

	const_iterator cbegin() const {
		return _listeners.begin();
	}

	const_iterator cend() const {
		return _listeners.end();
	}

	reverse_iterator rbegin() {
		return _listeners.rbegin();
	}

	const_reverse_iterator rbegin() const {
		return _listeners.rbegin();
	}

	const_reverse_iterator crbegin() const {
		return _listeners.rbegin();
	}

	reverse_iterator rend() {
		return _listeners.rend();
	}

	const_reverse_iterator rend() const {
		return _listeners.rend();
	}

	const_reverse_iterator crend() const {
		return _listeners.rend();
	}

public:
	const_iterator find(SemiMDPListener* listener) const;
	iterator find(SemiMDPListener* listener);

	iterator insert(iterator position, const value_type& val);

	iterator erase(iterator position);
	iterator erase(iterator first, iterator last);

	void swap(SemiMDPSender& obj) {
		_listeners.swap(obj._listeners);
		_index.swap(obj._index);
	}

	void clear() {
		_listeners.clear();
		_index.clear();
	}

public:
	//! Add a Listener to the Listener-List.
	void addSemiMDPListener(SemiMDPListener* listener);
	//! Remove a Listener from the Listerner-List.
	void removeSemiMDPListener(SemiMDPListener* listener);

	bool isListenerAdded(SemiMDPListener* listener);

	//! Tells all Listeners that a new Episode has occurred.
	virtual void startNewEpisode();
	//! Sends the State-Action-State Tuple to all Listeners.
	virtual void sendNextStep(StateCollection* lastState, Action* action,  StateCollection* currentState);
	//! Sends the State-Action-State Tuple to all Listeners, indicating that
	//! it was an intermediate step.
	virtual void sendIntermediateStep(StateCollection* lastState, Action* action, StateCollection* currentState);

public:
	SemiMDPSender& operator=(const SemiMDPSender&) = delete;
	SemiMDPSender(const SemiMDPSender&) = delete;

	SemiMDPSender();
	virtual ~SemiMDPSender();
};

/**
 * @class SemiMarkovDecisionProcess
 * Class for providing the general Functions for the learning Environment.
 *
 * SemiMarkovDecisionProcess is the super class of all "acting" agents. It
 * maintains a list of available Actions for the SMDP. It also logs the number
 * of Episodes and the Number of Steps done in the current Episode. It provides
 * the functionality for sending a Semi-Markov Step, but its only able to send
 * PrimitiveActions. For extended Actions you have to use the hierarchicalMDP.
 *
 * The class is a subclass of DeterministicController in order to make
 * any Controller assigned to the SMDP a deterministic Controller.
 * The DeterministicController Object is always the first Object to be
 * informed about the new Step, and its not in the ListenerList (Recursions).
 * This feature is needed for learning the exact Policy of the agent
 * (see SarsaLearner), so the agent can be used as estimation policy.
 *
 * @see Agent
 * @see HierarchicalalSemiMarkovDecisionProcess
**/
class SemiMarkovDecisionProcess:
	public DeterministicController, public SemiMDPSender
{
protected:
	Action *lastAction;

	unsigned int _currentSteps;
	bool _isFirstStep;

public:
	//! Sends the next Step to all Listeners. I.e that if the Action is
	//! a finished MultiStepAction.
	virtual void sendNextStep(StateCollection *lastState, Action *action, StateCollection *currentState);

	//! Returns the last Action sent to all Listeners.
	Action* getLastAction();

	//! Sends to all Listeners that a new Episode occurred and updates
	//! currentEpisodeNummer.
	virtual void startNewEpisode();

	//! Adds an Action to the ActionSet of the SMDP.
	virtual void addAction(Action *action);
	virtual void addActions(ActionSet *action);

public:
	SemiMarkovDecisionProcess& operator=(const SemiMarkovDecisionProcess&) = delete;
	SemiMarkovDecisionProcess(const SemiMarkovDecisionProcess&) = delete;

	SemiMarkovDecisionProcess();
	virtual ~SemiMarkovDecisionProcess();
};

/**
 * @class HierarchicalSemiMarkovDecisionProcess
 * Subclass of SemiMarkovDecisionProcess, used for hierarchical Learning.
 *
 * This abstract class provides full Hierarchical learning functionality.
 * It implements the HierarchicalStackListener interface,
 * so the HierarchicalController can inform the SMDP about a hierarchical Step.
 * Then the SMDP sends the State-Action-State Tuple with the action done by
 * the specific hierarchical SMDP.
 *
 * In order to provide hierarchical Functionality the class also represents
 * an ExtendedAction, so it can be used as Action for another hierarchical SMDP.
 * It can't be used as action for the agent, you have to use
 * HierarchicalController to create the HierarchicalStructure.
 * Use the hierarchical controller as controller for the agent, the agent
 * itself does'nt know anything about the hierarchical structure of the
 * learning Problem.
 *
 * The class is abstract because the isFinished Method from MultiStepAction
 * remains to be implemented.
 *
 * @see HierarchicalController
**/
class HierarchicalSemiMarkovDecisionProcess:
	public SemiMarkovDecisionProcess, public HierarchicalStackListener,
	public ExtendedAction, public StateModifiersObject
{
protected:
	//! Returns the action done by the SMDP
	virtual Action *getExecutedAction(HierarchicalStack *actionStack);

	/**
	 * Pointer to the currentEpisode, the currenEpisode must be updated before
	 * the sendNextStep method is called. So currentEpisode has to be the first
	 * Element of the agent's Listener-List. Needed for determining
	 * the intermediate Steps.
	**/
	Episode *currentEpisode;

	StateCollectionImpl *pastState;
	StateCollectionImpl *currentState;

	StateModifierList _modifierList;

public:
	virtual void setLoggedEpisode(Episode *loggedEpisode);
	virtual void newEpisode();

	virtual void nextStep(StateCollection *oldState, HierarchicalStack *actionStack, StateCollection *newState);

	/**
	 * Sends the nextStep to the listeners.
	 *
	 * If the action is an extended action, all intermediated steps and the
	 * RealType step itself get recovered from the Episode object, and send to
	 * the listeners (intermediate Steps gets send with the "intermediateStep"
	 * method). If the action is not an extended action, the nextSend Method
	 * from the super class gets called.
	**/
	virtual void sendNextStep(Action *action);

	virtual bool isFinished(StateCollection *, StateCollection *) {return false;};

	virtual Action *getNextHierarchyLevel(StateCollection *stateCollection, ActionDataSet *actionDataSet = nullptr);

	//! Add a state Modifier to the StateCollections.
	virtual void addModifierList(StateModifierList* modifierList);
	//! Remove a state Modifier from the StateCollections.
	virtual void removeModifierList(StateModifierList* modifierList);

	virtual void addStateModifier(StateModifier* modifier);
	virtual void removeStateModifier(StateModifier* modifier);

public:
	HierarchicalSemiMarkovDecisionProcess& operator=(const HierarchicalSemiMarkovDecisionProcess&) = delete;
	HierarchicalSemiMarkovDecisionProcess(const HierarchicalSemiMarkovDecisionProcess&) = delete;

	/**
	 * Creates a new hierarchical SMDP. The episode is needed for
	 * reconstruction of the intermediate and hierarchical steps.
	 * @param currentEpisode Pointer to the current Episode. It is recommended
	 * to use the currentEpisode Object of the Agent.
	**/
	HierarchicalSemiMarkovDecisionProcess(Episode *currentEpisode);
	HierarchicalSemiMarkovDecisionProcess(StateProperties *modelProperties, const std::set<StateModifierList*>& modifierLists = {});

	virtual ~HierarchicalSemiMarkovDecisionProcess();
};

} //namespace rlearner

#endif /* RLEARNER_MDP_H_ */
