// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cstdlib>
#include <cassert>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <cmath>
#include <vector>

#include "ril_debug.h"

#include "policies.h"
#include "tdlearner.h"
#include "qfunction.h"
#include "agentcontroller.h"
#include "action.h"
#include "statecollection.h"
#include "state.h"
#include "qetraces.h"
#include "residuals.h"
#include "featurefunction.h"

namespace rlearner {

TDLearner::TDLearner(
    RewardFunction *rewardFunction, AbstractQFunction *qfunction,
    AbstractQETraces *etraces, AgentController *estimationPolicy) :
	SemiMDPRewardListener(rewardFunction
) {
	this->qfunction = qfunction;
	this->etraces = etraces;
	this->estimationPolicy = estimationPolicy;

	addParameter("QLearningRate", 0.2);
	addParameter("DiscountFactor", 0.95);
	addParameters(qfunction);
	addParameters(etraces);

	addParameter("ResetETracesOnWrongEstimate", 1.0);

	if(estimationPolicy) {
		addParameters(estimationPolicy);
	}

	this->externETraces = true;
	this->actionDataSet = new ActionDataSet(qfunction->getActions());
	lastEstimatedAction = nullptr;
}

TDLearner::TDLearner(
    RewardFunction *rewardFunction, AbstractQFunction *qFunction,
    AgentController *estimationPolicy
):
	SemiMDPRewardListener(rewardFunction)
{
	this->qfunction = qFunction;
	this->etraces = qFunction->getStandardETraces();
	this->estimationPolicy = estimationPolicy;

	addParameter("QLearningRate", 0.2);
	addParameter("DiscountFactor", 0.95);
	addParameters(qfunction);
	addParameters(etraces);

	addParameter("ResetETracesOnWrongEstimate", 1.0);

	if(estimationPolicy) {
		addParameters(estimationPolicy);
	}

	this->externETraces = false;
	this->actionDataSet = new ActionDataSet(qfunction->getActions());
	lastEstimatedAction = nullptr;
}

TDLearner::~TDLearner() {
	if(!this->externETraces) {
		delete etraces;
	}

	delete actionDataSet;
}

void TDLearner::newEpisode() {
	lastEstimatedAction = nullptr;
}

RealType TDLearner::getTemporalDifference(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *newState) {
	RealType newQ = 0.0, oldQ = 0.0;
	RealType temporalDiff = 0.0;

	int duration = 1;

	if(action->isType(AF_MultistepAction)) {
		duration = dynamic_cast<MultiStepAction *>(action)->getDuration();
	}

//	assert(lastEstimatedAction->getIndex() >= 0);
	oldQ = qfunction->getValue(oldState, action); // Save old prediction: Q(st,at)

	if(!newState->isResetState()) {
		if(lastEstimatedAction == nullptr) {
			lastEstimatedAction = qfunction->getMax(newState,
			    qfunction->getActions(), actionDataSet);
		}

		newQ = qfunction->getValue(newState, lastEstimatedAction,
		    actionDataSet->getActionData(lastEstimatedAction));
	} else {
		DebugPrint('t', "TD Learner: Last State of Episode, Action %d\n",
		    qfunction->getActions()->getIndex(action));
	}

	temporalDiff = getResidual(oldQ, reward, duration, newQ);

	DebugPrint('t', "OldQValue: %f\n", oldQ);
	DebugPrint('t', "NewQValue: %f\n", newQ);
	DebugPrint('t', "Reward: %f\n", reward);
	DebugPrint('t', "TemporalDiff: %f\n", temporalDiff);

	sendErrorToListeners(temporalDiff, oldState, action, nullptr);

	return temporalDiff;
}

void TDLearner::learnStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *newState) {
	DebugPrint('t', "TD Learner Start\n");
	bool resetEtraces = getParameter("ResetETracesOnWrongEstimate") > 0.5;

	if(resetEtraces
	    && (lastEstimatedAction == nullptr
	        || !action->isSameAction(lastEstimatedAction,
	            actionDataSet->getActionData(lastEstimatedAction))))
	{
		etraces->resetETraces();
	}

	etraces->updateETraces(action);

	if(!newState->isResetState()) {
		lastEstimatedAction = estimationPolicy->getNextAction(newState,
		    actionDataSet);

		// if there is no estimated action, take the greedy policy
		if(lastEstimatedAction == nullptr) {
			lastEstimatedAction = qfunction->getMax(newState,
			    qfunction->getActions(), actionDataSet);
		}
	}

	addETraces(oldState, newState, action);

//	assert(qfunction->getActions()->getIndex(lastEstimatedAction) >= 0);

	etraces->updateQFunction(
	    getParameter("QLearningRate")
	        * getTemporalDifference(oldState, action, reward, newState));

	DebugPrint('t', "TD Learner end\n");
}

RealType TDLearner::getResidual(
    RealType oldQ, RealType reward, int duration, RealType newQ
) {
	return (reward + pow(getParameter("DiscountFactor"), duration) * newQ - oldQ);
}

void TDLearner::addETraces(
    StateCollection *oldState, StateCollection *, Action *oldAction
) {
	etraces->addETrace(oldState, oldAction, 1.0);
}

void TDLearner::nextStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState
) {
	learnStep(oldState, action, reward, nextState);
}

void TDLearner::intermediateStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState
) {
	addETraces(oldState, nextState, action);

	qfunction->updateValue(oldState, action,
	    getParameter("QLearningRate")
	        * getTemporalDifference(oldState, action, reward, nextState));
}

void TDLearner::saveValues(const std::string& filename) {
	std::ofstream stream(filename);
	saveValues(stream);
}

void TDLearner::loadValues(const std::string& filename) {
	std::ifstream stream(filename);
	loadValues(stream);
}

void TDLearner::saveValues(std::ostream& stream) {
	assert(qfunction != nullptr);
	qfunction->saveData(stream);
}

void TDLearner::loadValues(std::istream& stream) {
	assert(qfunction != nullptr);
	qfunction->loadData(stream);
}

void TDLearner::setAlpha(RealType alpha) {
	setParameter("QLearningRate", alpha);
}

void TDLearner::setLambda(RealType lambda) {
	assert(etraces != nullptr);
	etraces->setLambda(lambda);
}

AgentController* TDLearner::getEstimationPolicy() {
	return estimationPolicy;
}

void TDLearner::setEstimationPolicy(AgentController * estimationPolicy) {
	this->estimationPolicy = estimationPolicy;
}

AbstractQFunction* TDLearner::getQFunction() {
	return qfunction;
}

AbstractQETraces* TDLearner::getETraces() {
	return etraces;
}

QLearner::QLearner(RewardFunction *rewardFunction, AbstractQFunction *qfunc) :
	    TDLearner(rewardFunction, qfunc,
	        new QGreedyPolicy(qfunc->getActions(), qfunc)
) {
}

QLearner::~QLearner() {
	delete estimationPolicy;
}

SarsaLearner::SarsaLearner(
    RewardFunction *rewardFunction, AbstractQFunction *qfunction,
    DeterministicController *agent) :
	TDLearner(rewardFunction, qfunction, agent
) {
	setParameter("ResetETracesOnWrongEstimate", 1.0);
}

SarsaLearner::~SarsaLearner() {}

TDGradientLearner::TDGradientLearner(
    RewardFunction *rewardFunction, GradientQFunction *qfunction,
    AgentController *agentController, ResidualFunction *residual,
    ResidualGradientFunction *residualGradient) :
	    TDLearner(rewardFunction, qfunction, new GradientQETraces(qfunction),
	        agentController
) {
	assert(qfunction->isType(GRADIENTQFUNCTION));
	this->gradientQFunction = qfunction;
	this->residual = residual;
	this->residualGradient = residualGradient;

	if(residual) {
		addParameters(residual);
	}

	if(residualGradient) {
		addParameters(residualGradient);
	}

	this->gradientQETraces = dynamic_cast<GradientQETraces *>(etraces);
	gradientQETraces->setReplacingETraces(true);

	oldGradient = new FeatureList();
	newGradient = new FeatureList();
	residualGradientFeatures = new FeatureList();
}

TDGradientLearner::~TDGradientLearner() {
	delete oldGradient;
	delete newGradient;
	delete residualGradientFeatures;
}

RealType TDGradientLearner::getResidual(
    RealType oldQ, RealType reward, int duration, RealType newQ
) {
	return residual->getResidual(oldQ, reward, duration, newQ);
}

void TDGradientLearner::addETraces(
    StateCollection *oldState, StateCollection *newState, Action *oldAction) {
	RealType duration = oldAction->getDuration();

	oldGradient->clear();
	newGradient->clear();
	residualGradientFeatures->clear();

	gradientQFunction->getGradient(oldState, oldAction,
	    oldAction->getActionData(), oldGradient);

	if(!newState->isResetState()) {
		if(lastEstimatedAction == nullptr) {
			lastEstimatedAction = qfunction->getMax(newState,
			    qfunction->getActions(), actionDataSet);
		}

		gradientQFunction->getGradient(newState, lastEstimatedAction,
		    actionDataSet->getActionData(lastEstimatedAction), newGradient);
	}

	residualGradient->getResidualGradient(oldGradient, newGradient, duration,
	    residualGradientFeatures);

	if(DebugIsEnabled('t')) {
		DebugPrint('t', "Residual Gradient: ");
		residualGradientFeatures->save(DebugGetFileHandle('t'));
		DebugPrint('t', "\n");
	}

	gradientQETraces->addGradientETrace(residualGradientFeatures, -1.0);
}

TDResidualLearner::TDResidualLearner(
    RewardFunction *rewardFunction, GradientQFunction *qfunction,
    AgentController *agent, ResidualFunction *residual,
    ResidualGradientFunction *residualGradient,
    AbstractBetaCalculator *betaCalc) :
	    TDGradientLearner(rewardFunction, qfunction, agent, residual,
	        residualGradient
) {
	this->betaCalculator = betaCalc;

	residualETraces = new GradientQETraces(qfunction);
	residualETraces->setReplacingETraces(true);

	directGradientTraces = new GradientQETraces(qfunction);
	directGradientTraces->setReplacingETraces(true);

	residualGradientTraces = new GradientQETraces(qfunction);
	residualGradientTraces->setReplacingETraces(true);

	addParameters(residualETraces);
	addParameters(directGradientTraces, "Gradient");
	addParameters(residualGradientTraces, "Gradient");
	addParameters(betaCalculator);
	addParameter("ScaleResidualGradient", 0.0);
}

TDResidualLearner::~TDResidualLearner() {
	delete residualETraces;
}

void TDResidualLearner::learnStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *newState
) {
	bool resetEtraces = getParameter("ResetETracesOnWrongEstimate") > 0.5;

	if(resetEtraces
	    && (lastEstimatedAction == nullptr
	        || !action->isSameAction(lastEstimatedAction,
	            actionDataSet->getActionData(lastEstimatedAction)))) {
		etraces->resetETraces();
		residualETraces->resetETraces();
	}

	etraces->updateETraces(action);
	residualETraces->updateETraces(action);

	directGradientTraces->updateETraces(action);
	residualGradientTraces->updateETraces(action);

	if(!newState->isResetState()) {
		lastEstimatedAction = estimationPolicy->getNextAction(newState,
		    actionDataSet);

		// if there is no estimated action, take the greedy policy
		if(lastEstimatedAction == nullptr) {
			lastEstimatedAction = qfunction->getMax(newState,
			    qfunction->getActions(), actionDataSet);
		}

		assert(qfunction->getActions()->getIndex(lastEstimatedAction) >= 0);
	}

	RealType td = getParameter("QLearningRate")
	    * getTemporalDifference(oldState, action, reward, newState);
	addETraces(oldState, newState, action, td);

	RealType beta = betaCalculator->getBeta(
	    directGradientTraces->getGradientETraces(),
	    residualGradientTraces->getGradientETraces());

	gradientQETraces->updateQFunction(td * (1 - beta));
	residualETraces->updateQFunction(td * beta);
}

void TDResidualLearner::addETraces(
    StateCollection *oldState, StateCollection *newState, Action *action,
    RealType td
) {
	RealType duration = action->getDuration();

	oldGradient->clear();
	newGradient->clear();
	residualGradientFeatures->clear();

	gradientQFunction->getGradient(oldState, action, action->getActionData(),
	    oldGradient);

	if(!newState->isResetState()) {
		if(lastEstimatedAction == nullptr) {
			lastEstimatedAction = qfunction->getMax(newState,
			    qfunction->getActions(), actionDataSet);
		}

		gradientQFunction->getGradient(newState, lastEstimatedAction,
		    actionDataSet->getActionData(lastEstimatedAction), newGradient);
	}

	// Add Direct Gradient
	gradientQETraces->addGradientETrace(oldGradient, 1.0);

	residualGradient->getResidualGradient(oldGradient, newGradient, duration,
	    residualGradientFeatures);

	if(getParameter("ScaleResidualGradient") > 0.5) {
		residualGradientFeatures->multFactor(
		    oldGradient->getLength() / residualGradientFeatures->getLength());
	}

	// Add Residual Gradient
	residualETraces->addGradientETrace(residualGradientFeatures, -1.0);

	directGradientTraces->addGradientETrace(oldGradient, td);
	residualGradientTraces->addGradientETrace(residualGradientFeatures, -td);

	if(DebugIsEnabled('t')) {
		DebugPrint('t', "Residual Gradient: ");
		residualGradientFeatures->save(DebugGetFileHandle('t'));
		DebugPrint('t', "\n");
	}
}

void TDResidualLearner::newEpisode() {
	TDGradientLearner::newEpisode();
	residualETraces->resetETraces();
	residualGradientTraces->resetETraces();
	directGradientTraces->resetETraces();
}

QAverageTDErrorLearner::QAverageTDErrorLearner(
    FeatureQFunction *l_averageErrorFunction, RealType l_updateRate) :
	StateObject(l_averageErrorFunction->getFeatureCalculator()) {
	averageErrorFunction = l_averageErrorFunction;
	updateRate = l_updateRate;

	addParameter("TDErrorUpdateRate", updateRate);
}

QAverageTDErrorLearner::~QAverageTDErrorLearner() {}

void QAverageTDErrorLearner::onParametersChanged() {
	updateRate = ParameterObject::getParameter("TDErrorUpdateRate");
}

void QAverageTDErrorLearner::receiveError(
    RealType error, StateCollection *state, Action *action, ActionData *) {
	State *featureState = state->getState(
	    averageErrorFunction->getFeatureCalculator());

	for(unsigned int i = 0; i < featureState->getNumDiscreteStates(); i++) {
		int index = featureState->getDiscreteState(i);
		RealType featureFac = featureState->getContinuousState(i);
		RealType featureVal = averageErrorFunction->getValue(index, action);

		featureVal = featureVal
		    * (updateRate + (1 - featureFac) * (1 - updateRate))
		    + error * (1 - updateRate) * featureFac;
		averageErrorFunction->setValue(index, action, featureVal);
	}
}

QAverageTDVarianceLearner::QAverageTDVarianceLearner(
    FeatureQFunction *averageErrorFunction, RealType updateRate) :
	QAverageTDErrorLearner(averageErrorFunction, updateRate) {

}

QAverageTDVarianceLearner::~QAverageTDVarianceLearner() {}

void QAverageTDVarianceLearner::receiveError(
    RealType error, StateCollection *state, Action *action, ActionData *data) {
	QAverageTDErrorLearner::receiveError(pow(error, 2.0), state, action, data);
}

} //namespace rlearner
