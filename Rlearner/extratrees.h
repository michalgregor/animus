// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cextratrees_H_
#define Rlearner_cextratrees_H_

#include "algebra.h"
#include "trees.h"

namespace rlearner {

class DataSet;
class DataSet1D;

class ExtraTreesSplittingConditionFactory: public SplittingConditionFactory {
protected:
	unsigned int K;
	unsigned int n_min;

	RealType outThreshold;

	DataSet *inputData;
	DataSet1D *outputData;
	DataSet1D *weightingData;

protected:
	RealType getScore(SplittingCondition *condition, DataSubset *dataSubset);

public:
	virtual SplittingCondition *createSplittingCondition(
	        DataSubset *dataSubset);
	virtual bool isLeaf(DataSubset *dataSubset);

public:
	ExtraTreesSplittingConditionFactory(DataSet *inputData,
	        DataSet1D *outputData, unsigned int K, unsigned int n_min,
	        RealType outTresh = 0.0, DataSet1D *weightingData = nullptr);
	virtual ~ExtraTreesSplittingConditionFactory();
};

template<typename TreeData> class ExtraTree: public Tree<TreeData> {
public:
	ExtraTree(
		DataSet *inputData,
		DataSet1D *outputData,
		TreeDataFactory<TreeData> *dataFactory,
		unsigned int K,
		unsigned int n_min,
		RealType outTresh,
		DataSet1D *weightingData = nullptr
	): Tree<TreeData>(inputData->getNumDimensions()) {
		SplittingConditionFactory *splittingFactory =
		        new ExtraTreesSplittingConditionFactory(inputData, outputData,
		                K, n_min, outTresh, weightingData);
		Tree<TreeData>::createTree(inputData, splittingFactory, dataFactory);
		delete splittingFactory;
	}

	virtual ~ExtraTree() {}
};

class ExtraRegressionTree: public ExtraTree<RealType> {
public:
	ExtraRegressionTree(
		DataSet *inputData,
		DataSet1D *outputData,
		unsigned int K,
		unsigned int n_min,
		RealType treshold,
		DataSet1D *weightingData = nullptr
	);

	virtual ~ExtraRegressionTree();
};

} //namespace rlearner

#endif //Rlearner_cextratrees_H_
