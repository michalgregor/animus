// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_clinearfafeaturecalculator_H_
#define Rlearner_clinearfafeaturecalculator_H__

#include <map>

#include "statemodifier.h"

namespace rlearner {

class State;
class StateCollection;

/** 
 * @class FeatureOperatorOr
 * Combines 2 or more feature states with an "Or" operation.
 *
 * The "or" operator gives you the possibility to use different feature states
 * simultaneously that are independent. You can add several feature calculators,
 * all calculated features from the different modifiers can now be active
 * simultaneously in the same feature state. It is understood that you have
 * to distinguish between the different features from the different feature
 * states, so, to get a unique feature index again, the sum off all feature
 * state sizes from the previously added feature states is added to the feature
 * index of a particular feature state. So our feature state size is the sum
 * off all feature state sizes, and the number of active features is naturally
 * the sum of all number of active features.
 *
 * The feature operator or is used to combine 2 or more (more or less)
 * independent states, for example tilings or RBF networks with different
 * offsets/resolutions.
 *
 * You always have to init the feature operator with the function
 * initFeatureOperator after ALL feature states have been added and before
 * you add the feature operator to the agent's modifier list.
 */
class FeatureOperatorOr: public StateMultiModifier, public FeatureCalculator {
protected:
	std::map<StateModifier *, RealType> *featureFactors;

public:
	/**
	 * Calculates the feature state.
	 *
	 * The "or" operator gives you the possibility to use different feature
	 * states simultaneously. You can add several feature calculators, all
	 * calculated features from the different modifiers can now be active
	 *  simultaneously in the same feature state. It is understood that you
	 *  have to distinguish between the different features from the different
	 *  feature states, so, to get a unique feature index again, the sum off
	 *  all feature state sizes from the previously added feature states is
	 *  added to the feature index of a particular feature state. So our
	 *  feature state size is the sum off all feature state sizes, and the
	 *  number of active features is naturally the sum of all number of
	 *  active features.
	 */
	virtual void getModifiedState(StateCollection *state,
	        State *modifiedState);

	/** 
	 * Adds a feature state to the modifier.
	 *
	 * The state modifier HAS to be a feature calculator or a discretizer.
	 * After the operator has been initialised, no feature calculators can
	 * be added any more.
	 */
	virtual void addStateModifier(StateModifier *featCalc,
	        RealType factor = 1.0);

	/**
	 * Inits the feature operator.
	 *
	 * You always have to init the feature operator after ALL feature states
	 * have been added and before you use the feature operator (e.g. add the
	 * feature operator to the agent's modifier list).
	 */
	virtual void initFeatureOperator();

	//! Returns the feature calculator for a given feature index.
	StateModifier *getStateModifier(int feature);

public:
	FeatureOperatorOr& operator=(const FeatureOperatorOr&) = delete;
	FeatureOperatorOr(const FeatureOperatorOr&) = delete;

	//! Creates empty operator.
	FeatureOperatorOr();
	virtual ~FeatureOperatorOr();
};

/**
 * @class FeatureOperatorAnd
 * Combines 2 or more feature states with an "And" operation.
 *
 *  The "and" operator gives you the possibility to use different feature
 *  states simultaneously that are dependent. This class works primary like
 *  the discrete operator and class, it combines several dependent feature
 *  state to one. The feature state size of the operator is the product of
 *  all feature state sizes, the number of active states is the product of
 *  all numbers of active states. Each active feature in the operator is
 *  assigned to different active states from the feature states (there is
 *  one active feature in the operator for each possibility of choosing one
 *  active feature from each feature calculator). A new feature index is
 *  calculated according to the feature indices of the assigned active
 *  features, the feature factor is the product of all feature factors.
 *
 *  The feature operator and is used to combine 2 or more dependent states,
 *  for example single state RBF networks (SingleStateRBFFeatureCalculator).
 *
 *  You always have to init the feature operator with the function
 *  initFeatureOperator after ALL feature states have been added and
 *  before you add the feature operator to the agent's modifier list.
 */
class FeatureOperatorAnd: public StateMultiModifier, public FeatureCalculator {
public:
	/**
	 * Calculates the feature state.
	 *
	 * The "and" operator gives you the possibility to use different feature
	 * states simultaneously that are dependent. This class works primary like
	 * the discrete operator and class, it combines several dependent feature
	 * state to one. The feature state size of the operator is the product of
	 * all feature state sizes, the number of active states is the product of
	 * all numbers of active states. Each active feature in the operator is
	 * assigned to different active states from the feature states (there is
	 * one active feature in the operator for each possibility of choosing one
	 * active feature from each feature calculator). A new feature index is
	 * calculated according to the feature indices of the assigned active
	 * features, the feature factor is the product of all feature factors.
	 */
	virtual void getModifiedState(StateCollection *state,
	        State *modifiedState);

	/**
	 * Adds a feature state to the modifier.
	 *
	 * The state modifier HAS to be a feature calculator or a discretizer.
	 * After the operator has been initialized, no feature calculators can
	 * be added any more.
	 */
	virtual void addStateModifier(StateModifier *featCalc);

	/**
	 * Inits the feature operator.
	 *
	 * You always have to init the feature operator after ALL feature states
	 * have been added and before you use the feature operator (e.g. add the
	 * feature operator to the agent's modifier list).
	 */
	virtual void initFeatureOperator();

public:
	FeatureOperatorAnd();
	virtual ~FeatureOperatorAnd() = default;
};

/**
 * @class GridFeatureCalculator
 * Abstract Superclass for all feature calculator that partition the state
 * space with a grid.
 *
 * This class lays a grid over a specified sub-space of the model space. How
 * this grid is used (e.g. Tiling or RBF network) is specified by the
 * sub-classes. You can specify which dimensions you want to use
 * (dimensions array), how much partitions you need for each dimension and
 * also an offset for each dimension which is added to the grid centres.
 * The size of the feature space is the product of all numbers of partitions.
 *
 * The class provides methods for retrieving the position of a feature
 * (getFeaturePosition), retrieving the active feature (feature nearest to
 * the current state) (getActiveFeature), and retrieving the current partitions
 * of all dimensions (getSingleActiveFeature).
 *
 * This class always works with the normalized continuous state variable
 * values (scaled to the interval [0, 1], so consider that when choosing
 * your offsets.
 */
class GridFeatureCalculator: public FeatureCalculator {
protected:
	unsigned int *partitions;
	unsigned int *dimensions;
	RealType *offsets;

	unsigned int numDim;

	unsigned int *dimensionSize;
	RealType *gridScale;

public:
	void setGridScale(int dimension, RealType scale);

	//! Returns the number of dimensions you use.
	int unsigned getNumDimensions();

	//! Returns the feature index of the given position array (active
	//! partitions of the single state variables).
	int unsigned getFeatureIndex(int position[]);

	//! Stores the position of the specified feature in the position vector.
	virtual void getFeaturePosition(unsigned int feature,
	        ColumnVector *position);

	//! Returns the number of the nearest feature to the current state.
	virtual unsigned int getActiveFeature(State *state);

	//! Retrievs the current partitions of all dimensions.
	virtual void getSingleActiveFeature(State *state,
	        unsigned int *activeFeature);

	//! Interface method for subclasses.
	virtual void getModifiedState(StateCollection *state,
	        State *featState) = 0;

public:
	GridFeatureCalculator& operator=(const GridFeatureCalculator&) = delete;
	GridFeatureCalculator(const GridFeatureCalculator&) = delete;

	/**
	 * This class lays a grid over a specified sub-space of the model space.
	 * How this grid is used (e.g. Tiling or RBF network) is specified by
	 * the sub-classes. You can specify which dimensions you want to use, how
	 * much partitions you need for each dimension and also an offset for each
	 * dimension which is added to the grid centres. The first parameter is
	 * the number of dimensions you want to use so its the size of your arrays.
	 * The size of the feature space is the product of all numbers of
	 * partitions.
	 */
	GridFeatureCalculator(unsigned int numDim, unsigned int dimensions[],
	        unsigned int partitions[], RealType offsets[],
	        unsigned int numActiveFeatures);
	virtual ~GridFeatureCalculator();
};

/**
 * @class TilingFeatureCalculator
 * Tilings represent a grid layered over the state space.
 *
 * The grid is specified the same way as it is in the superclass. A tiling
 * always has just one active state (factor = 1.0), which is the feature
 * containing the current model state. So a single tiling is exactly seen
 * a discrete state. Use FeatureOperatorOr to combine more tiling objects.
 */
class TilingFeatureCalculator: public GridFeatureCalculator {
public:
	//! Returns the active feature.
	virtual void getModifiedState(StateCollection *state, State *featState);

public:
	TilingFeatureCalculator(unsigned int numDim, unsigned int dimensions[],
	        unsigned int partitions[], RealType offsets[]);
	virtual ~TilingFeatureCalculator();
};

/**
 * @class LinearMultiFeatureCalculator
 * Super class for all feature calculators that use a grid and where more
 * than one feature can be active.
 *
 * If we use LinearMultiFeatureCalculator more than one feature can be active
 * in the grid. The class maintains an additional array which maintains the
 * number of active features in each direction, so a value of 1 for dimension
 * 0 means that there are 3 active features for dimension 0 (the current
 * feature itself and it's left and right neighbours) . This array has to
 * be set by the subclasses with the function initAreaSize (which has also
 * to be called by the subclasses in the constructor). For each feature that
 * is in the "active" area, the feature factor is calculated by the function
 * getFeatureFactor, which also has to be implemented by the subclasses.
 */
class LinearMultiFeatureCalculator: public GridFeatureCalculator {
protected:
	unsigned int *areaSize;
	unsigned int areaNumPart;

	ColumnVector *activePosition;
	ColumnVector *featurePosition;
	unsigned int *actualPartition;
	unsigned int *singleStateFeatures;

	/**
	 * Interface function for returning the feature activation factor.
	 *
	 * As input the method gets the current model state and the position
	 * of the feature as vector.
	 */
	virtual RealType getFeatureFactor(State *state, ColumnVector *featPos) = 0;

	/**
	 * Interface function for setting the areaSize array.
	 *
	 * Function has to set the number of active features in each direction.
	 * To a value of 1 for dimension 0 means that there are 3 active features
	 * for dimension 0 (the current feature itself and it's left and right
	 * neighbours). Has to be called by the subclass's constructor and has
	 * to call the calcNumActiveFeatures method.
	 */
	virtual void initAreaSize();

	/**
	 * Calculates the number of active feature and stores it in areaNumPart.
	 *
	 * Has to be called by initAreaSize. Uses the areaSize array to calculate
	 * the number of active features. The number of active features is
	 * 2 * areaSize[i] + 1 for each dimension i.
	 */
	virtual void calcNumActiveFeatures();

public:
	/**
	 * Calculates the feature state with multiple active features.
	 *
	 * The class maintains an additional array which maintains the number
	 * of active features in each direction, so a value of 1 for dimension
	 * 0 means that there are 3 active features for dimension 0 (the current
	 * feature itself and it's left and right neighbours) . This array has
	 * to be set by the subclasses with the function initAreaSize (which has
	 * also to be called by the subclasses in the constructor). For each
	 * feature that is in the "active" area, the feature factor is calculated
	 * by the function getFeatureFactor, which also has to be implemented by
	 * the subclasses. For periodic states the active area is mirrored into
	 * the state intervall again if it leaves the interval, for non-periodic
	 * states the active area is cut off. At the end all feature factors get
	 * normalized (sum = 1.0)*/
	virtual void getModifiedState(StateCollection *state, State *featState);

public:
	LinearMultiFeatureCalculator& operator=(const LinearMultiFeatureCalculator&) = delete;
	LinearMultiFeatureCalculator(const LinearMultiFeatureCalculator&) = delete;

	//! For details about creating the grid see the super class.
	LinearMultiFeatureCalculator(unsigned int numDim,
	        unsigned int dimensions[], unsigned int partitions[],
	        RealType offsets[], unsigned int numFeatures);
	virtual ~LinearMultiFeatureCalculator();
};

/**
 * @class RBFFeatureCalculator
 * Represents a normalized RBF network.
 *
 * This class lays a grid of RBF-Functions over a specified sub-space of
 * the model space.  You can specify which dimensions you want to use
 * (dimensions array), how much partitions you need for each dimension,
 * an offset for each dimension which is added to the RBF centres and the
 * sigmas for each dimension. When talking about grids, the RBF centre is
 * always in the centre of the partition. The size of the feature space is
 * the product of all numbers of partitions.
 *
 * This class always works with the normalized continuous state variable values
 * (scaled to the interval [0, 1], so consider that when choosing your offsets
 * and sigmas.
 *
 * The number of active states for each dimension is chosen according to the
 * sigma value. All features are active that are within a range of 2 * sigma.
 * So if you want only the neighbours to be active, use the rule of thumb
 * sigma[i] = 1 / (2 * numPartitions[i] ), so you get 3 active features per
 * dimension.
 */
class RBFFeatureCalculator: public LinearMultiFeatureCalculator {
protected:
	RealType* sigma;
	RealType sigmaMaxSize;

protected:
	//! Calculates the feature factor with the RBF function
	virtual RealType getFeatureFactor(State *state, ColumnVector *featPos);

	/**
	 * Set the areaSize array.
	 *
	 * The number of active states for each dimension is chosen according to
	 * the sigma value. All features are active that are within a range of
	 * 2 * sigma. So if you want only the neighbours to be active, use
	 * the rule of thumb sigma[i] = 1 / (2 * numPartitions[i] ),
	 * so you get 3 active features per dimension.
	 */
	virtual void initAreaSize();

public:
	RBFFeatureCalculator& operator=(const RBFFeatureCalculator&) = delete;
	RBFFeatureCalculator(const RBFFeatureCalculator&) = delete;

	RBFFeatureCalculator(unsigned int numDim, unsigned int dimensions[],
	        unsigned int partitions[], RealType offsets[], RealType sigma[]);

	RBFFeatureCalculator(unsigned int numDim, unsigned int dimensions[],
	        unsigned int partitions[], RealType offsets[], RealType sigma[],
	        unsigned int areaSize[]);

	virtual ~RBFFeatureCalculator();
};

class LinearInterpolationFeatureCalculator: public LinearMultiFeatureCalculator {
private:
	virtual RealType getFeatureFactor(State *state, ColumnVector *featPos);
	virtual void initAreaSize();

public:
	LinearInterpolationFeatureCalculator(unsigned int numDim,
	        unsigned int dimensions[], unsigned int partitions[],
	        RealType offsets[]);
	virtual ~LinearInterpolationFeatureCalculator();
};

/**
 * @class SingleStateFeatureCalculator
 * Superclass for calculating features from a single continuous state variable.
 *
 * This class allows you, similar to SingleStateDiscretizer to calculate your
 * features from a single continuous state variable. Therefore a 1-dimensional
 * grid is laid over the specified continuous state variable. How the grid is
 * used is specified by the subclasses (e.g. RBF-Centres). For the current
 * partition of the grid and the neighbours (as much as numActiveFeatures
 * set in the constructor) the feature factor is calculated by the interface
 * function getFeatureFactor. For the current feature and the neighbours
 * calculation periodic state variables get also considered.
 *
 * At the end the features get normalized (factor sum = 1).
 *
 * Use FeatureOperatorOr to combine the feature states coming from different
 * continuous state variables.
 */
class SingleStateFeatureCalculator: public FeatureCalculator {
protected:
	int dimension;
	int numPartitions;
	RealType *partitions;

protected:
	/**
	 * Interface function for calculating the feature activation factor.
	 *
	 * Gets the number of the partition (= featureindex), the value of the
	 * continuous state variable and the distance to the feature centre
	 * to the current state variable value as input. In this distance periodic
	 * states are already considered.
	 */
	virtual RealType getFeatureFactor(int partition, RealType contState,
	        RealType difference) = 0;

public:
	/**
	 * Calculates the feature state from one continuous state variable.
	 *
	 * For the current partition of the grid and the neighbours (as much as
	 * numActiveFeatures set in the constructor) the feature factor is
	 * calculated by the interface function getFeatureFactor. At the end
	 * the features get normalized (factor sum = 1).
	 */
	virtual void getModifiedState(StateCollection *state, State *featState);

public:
	SingleStateFeatureCalculator& operator=(
	        const SingleStateFeatureCalculator&) = delete;
	SingleStateFeatureCalculator(const SingleStateFeatureCalculator&) = delete;

	/**
	 * Creates the feature calculator.
	 *
	 * You have to set the dimension of the modelstate you want to calculate
	 * your features from, the size of your grid, the grid itself as a RealType
	 * array and how much features should be active. If you set
	 * numActiveFeatures for example to 3, the current feature (feature
	 * nearest to the current state) and its left and right neighbour
	 * are active.
	 */
	SingleStateFeatureCalculator(int dimension, int numPartitions,
	        RealType *partitions, int numActiveFeatures);
	virtual ~SingleStateFeatureCalculator();
};

/**
 * @class SingleStateRBFFeatureCalculator
 * Represents a 1-D RBF-network laid over a specified continuous state variable.
 *
 * You can specify the centres of the RBF-Functions (partitions array) and the
 * sigma of each RBF-Function. The RBF-centres have to be in an ascending
 * order. If you don't specify any sigma, the sigma gets automatically
 * calculated so that the distance between two neighbored features is equal
 * 2 * sigma. The rest of the parameters are the same as for the superclass.
 */
class SingleStateRBFFeatureCalculator: public SingleStateFeatureCalculator {
protected:
	//! Calculates the feature factor according the RBF formular
	virtual RealType getFeatureFactor(int partition, RealType difference,
	        RealType nextPart);

public:
	SingleStateRBFFeatureCalculator(int dimension, int numPartitions,
	        RealType *partitions, int numActiveFeatures);
	virtual ~SingleStateRBFFeatureCalculator() = default;
};

/**
 * @class SingleStateLinearInterpolationFeatureCalculator
 * Class for linear interpolation features of a single continuous
 * state variable.
 *
 * This class represent a linear interpolator for a single continuous state
 * variable. You can specify the centres of the features and which dimension
 * of the modelstate you want to use. There are always 2 features active,
 * the nearest feature to the left and to the right. The feature factors are
 * calculated per linear interpolation, so if dist is the distance of 2
 * neighboured features and dist_left is the distance to the left feature
 * from the current state x, the feature factor of the left feature is
 * 1 - dist_left / dist and the factor of the right feature is naturally
 * dist_left/dist.
 */
class SingleStateLinearInterpolationFeatureCalculator: public SingleStateFeatureCalculator {
protected:
	virtual RealType getFeatureFactor(int partition, RealType difference,
	        RealType nextPart);

public:
	SingleStateLinearInterpolationFeatureCalculator(int dimension,
	        int numPartitions, RealType *partitions);
	virtual ~SingleStateLinearInterpolationFeatureCalculator();
};

class FeatureStateNNInput: public StateModifier {
protected:
	FeatureCalculator *featureStateCalc;
	State *featureState;

public:
	virtual void getModifiedState(StateCollection *state, State *featState);

public:
	FeatureStateNNInput& operator=(const FeatureStateNNInput&) = delete;
	FeatureStateNNInput(const FeatureStateNNInput&) = delete;

	FeatureStateNNInput(FeatureCalculator *featureStateCalc);
	virtual ~FeatureStateNNInput();
};

} //namespace rlearner

#endif //Rlearner_clinearfafeaturecalculator_H_
