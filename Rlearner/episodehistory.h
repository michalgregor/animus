// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cepisodehistory_H_
#define Rlearner_cepisodehistory_H_

#include <vector>
#include <iostream>

#include "agentlistener.h"
#include "history.h"
#include "agentcontroller.h"
#include "baseobjects.h"
#include "environmentmodel.h"
#include "StateModifierList.h"

namespace rlearner {

class Episode;

/**
 * @class EpisodeHistory
 * Interface for all classes which are able to store episodes.
 *
 * This class is an interface for all histories containing episode objects.
 * It provides abstract functions for fetching a specific episode (getEpisode)
 * and retrieving the number of episodes (getNumEpisodes). These 2 functions
 * must be implemented by all classes implementing this interface
 * (e.g. AgentLogger).
 *
 * The class also provides to functions for simulating a certain episode
 * (simulateEpisode) and simulating all episodes (simulateAllEpisodes) to
 * a specific listener. Since AgentLogger implements this interface it can
 * be used to simulate the training trial to a single listener. If you use more
 * than one listener it is recommended to use StoredEpisodeModel.
 *
 * @see AgentLogger
 */
class EpisodeHistory: public StepHistory, virtual public StateModifiersObject {
protected:
	std::map<int, Episode*> *stepToEpisodeMap;
	std::map<Episode*, int> *episodeOffsetMap;
	StateModifierList _modifierList;

public:
	//! Returns the number of episodes.
	virtual int getNumEpisodes() = 0;
	//! Returns a specific episode.
	virtual Episode* getEpisode(int index) = 0;

	virtual int getNumSteps();
	virtual void getStep(int index, Step *step);

	virtual void createStepToEpisodeMap();

public:
	void addStateModifier(StateModifier* modifier);
	void removeStateModifier(StateModifier* modifier);

public:
	EpisodeHistory& operator=(const EpisodeHistory&) = delete;
	EpisodeHistory(const EpisodeHistory&) = delete;

	EpisodeHistory(StateProperties *properties, ActionSet *actions);
	virtual ~EpisodeHistory();
};

class EpisodeHistorySubset: public EpisodeHistory {
protected:
	EpisodeHistory *episodes;
	std::vector<int> *indices;
public:
	//! Returns the number of episodes.
	virtual int getNumEpisodes();
	//! Returns a specific episode.
	virtual Episode* getEpisode(int index);

	virtual void resetData() {}
	virtual void loadData(std::istream&) {}
	virtual void saveData(std::ostream&) {}

public:
	EpisodeHistorySubset& operator=(const EpisodeHistorySubset&) = delete;
	EpisodeHistorySubset(const EpisodeHistorySubset&) = delete;

	EpisodeHistorySubset(EpisodeHistory *episodes, std::vector<int> *indices);
	virtual ~EpisodeHistorySubset();
};

/**
 * @class StoredEpisodeModel
 * Serves as environment model for an agent, simulating the episodes from
 * an agent logger.
 *
 * The simulated environment model reproduces logged episodes for the agent,
 * it can be used as it were a normal environment model, so the listeners of
 * the agent can learn from the logged data as if it is new created data.
 * The environment takes an episode history and simulates the episodes in
 * sequence. Therefore it handles pointer to the currentEpisode and the number
 * of steps already simulated in that episode.
 *
 * The class also serves as agent controller, giving back the actions from
 * the steps. The environment episode must be used as agent controller because
 * obviously only the actions from the episode history can be "executed".
 *
 * When fetching a state collection via getState(StateCollection *),
 * the environment model registers all states in the state collection,
 * which are in the episode history and member of the state collection.
 * These states are marked as already calculated, so only the states
 * which are not stored in the history have to be recalculated.
 *
 * @see EpisodeHistory
 */
class StoredEpisodeModel: public EnvironmentModel, public AgentController {
protected:
	//! Pointer to the history.
	EpisodeHistory *history;
	//! Pointer to the current episode.
	Episode *currentEpisode;

	int numEpisode;
	int numStep;

protected:
	/**
	 * Sets the state to the next step.
	 *
	 * Increases the numSteps field and sets the reset flag if the episode
	 * has ended.
	 */
	virtual void doNextState(PrimitiveAction *action);

	//! Sets the currentEpisode to the next Episode in history.
	virtual void doResetModel();

public:
	int getNumEpisode() const {
		return numEpisode;
	}

	int getNumStep() const {
		return numStep;
	}

	void setNumEpisode(int numEpisode_) {
		numEpisode = numEpisode_;
	}

	void setNumStep(int numStep_) {
		numStep = numStep_;
	}

public:
	virtual EpisodeHistory* getEpisodeHistory();
	virtual void setEpisodeHistory(EpisodeHistory *hist);

	//! Gets the state with the given states properties from the history.
	virtual void getState(State *state);

	/**
	 * Registers all states in the state collection, which are in the episode
	 * history and member of the state collection. These states are marked
	 * as already calculated, so only the states which are not stored in
	 * the history have to be recalculated.
	 */
	virtual void getState(StateCollectionImpl *stateCollection);

	//! Implementation of the agent controller interface, gets the action
	//! executed in the current step.
	virtual Action* getNextAction(StateCollection* state, ActionDataSet* data = nullptr);

public:
	StoredEpisodeModel& operator=(const StoredEpisodeModel&) = delete;
	StoredEpisodeModel(const StoredEpisodeModel&) = delete;

	//! Creates a simulated environment model, simulating the given history.
	StoredEpisodeModel(EpisodeHistory *history);
	virtual ~StoredEpisodeModel();
};

/**
 * @class CBatchEpisodeUpdate
 * Class for doing batch updates during the learning trial.
 *
 * After each episode all the episodes from the past (which are in the given
 * episode history) are be showed to the listener object again. This can
 * improve learning specially for TD-Learning algorithms.
 *
 * Each time a newEpisode event occurs the episodes in the history are shown
 * to the given listener, so the batch update object must be added to the
 * agents listener. This is used in connection with an agent logger which
 * logs the current learning trial.
 */
class CBatchEpisodeUpdate: public SemiMDPListener {
protected:
	//! Listener to show the episodes.
	SemiMDPListener *listener;
	//! Episode history containing all episodes.
	EpisodeHistory *logger;

	int numEpisodes;
	std::list<int> *episodeIndex;

	ActionDataSet *dataSet;
	Step *step;

public:
	/**
	 * Simulates all episodes from the history to the listener.
	 *
	 * Just calls the simulate all episodes method of the interface
	 * EpisodeHistory.
	 */
	virtual void newEpisode();

	//! Simulates the requested Episode to the listener.
	virtual void simulateEpisode(int episode, SemiMDPListener *listener);
	//! Simulates all episodes to the listener.
	virtual void simulateAllEpisodes(SemiMDPListener *listener);

	//! Simulates numEpisodes randomly choosen Episodes to the listener.
	void simulateNRandomEpisodes(int numEpisodes, SemiMDPListener *listener);

public:
	CBatchEpisodeUpdate& operator=(const CBatchEpisodeUpdate&) = delete;
	CBatchEpisodeUpdate(const CBatchEpisodeUpdate&) = delete;

	//! Creates a batch update object for the specified listener with
	//! the specified episode history.
	CBatchEpisodeUpdate(SemiMDPListener *listener, EpisodeHistory *logger,
	        int numEpisodes, const std::set<StateModifierList*>& modifierLists);
	virtual ~CBatchEpisodeUpdate();
};

} //namespace rlearner

#endif //Rlearner_cepisodehistory_H_
