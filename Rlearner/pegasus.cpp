// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "pegasus.h"
#include "transitionfunction.h"
#include "rewardfunction.h"
#include "evaluator.h"
#include "continuousactions.h"
#include "continuousactiongradientpolicy.h"
#include "ril_debug.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "featurefunction.h"
#include "agent.h"

namespace rlearner {

TransitionFunctionInputDerivationCalculator::TransitionFunctionInputDerivationCalculator(
    ContinuousTimeAndActionTransitionFunction *dynModel)
{
	this->dynModel = dynModel;
}

TransitionFunctionInputDerivationCalculator::~TransitionFunctionInputDerivationCalculator() {}

TransitionFunctionNumericalInputDerivationCalculator::TransitionFunctionNumericalInputDerivationCalculator(
    ContinuousTimeAndActionTransitionFunction *dynModel,
    RealType stepSize = 0.001) :
	TransitionFunctionInputDerivationCalculator(dynModel) {
	addParameter("InputDerivationCalculatorStepSize", stepSize);

	nextState1 = new State(dynModel->getStateProperties());
	nextState2 = new State(dynModel->getStateProperties());

	buffState = new State(dynModel->getStateProperties());

	buffData =
	    dynamic_cast<ContinuousActionData *>(dynModel->getContinuousAction()->getNewActionData());
}

TransitionFunctionNumericalInputDerivationCalculator::~TransitionFunctionNumericalInputDerivationCalculator() {
	delete buffState;
	delete nextState1;
	delete nextState2;
	delete buffData;
}

void TransitionFunctionNumericalInputDerivationCalculator::getInputDerivation(
    State *currentState, ContinuousActionData *data, Matrix *dModelInput)
{
	int column = 0;
	StateProperties *stateProp = dynModel->getStateProperties();
	RealType stepSize = getParameter("InputDerivationCalculatorStepSize");
	for(unsigned int i = 0; i < currentState->getNumContinuousStates();
	    i++, column++) {
		RealType stepSize_i = stepSize
		    * (stateProp->getMaxValue(i) - stateProp->getMinValue(i));
		buffState->setState(currentState);

		buffState->setContinuousState(i,
		    currentState->getContinuousState(i) - stepSize_i);
		dynModel->transitionFunction(buffState, dynModel->getContinuousAction(),
		    nextState1, data);

		buffState->setContinuousState(i,
		    currentState->getContinuousState(i) + stepSize_i);
		dynModel->transitionFunction(buffState, dynModel->getContinuousAction(),
		    nextState2, data);

		for(unsigned int row = 0; row < stateProp->getNumContinuousStates();
		    row++) {
			dModelInput->operator()(row, column) =
			    (nextState2->getSingleStateDifference(row,
			        nextState1->getContinuousState(row))) / (2 * stepSize_i);
		}
	}

	ContinuousActionProperties *actionProp =
	    dynModel->getContinuousAction()->getContinuousActionProperties();

	for(int i = 0; i < data->rows(); i++, column++) {
		RealType stepSize_i = stepSize
		    * (actionProp->getMaxActionValue(i)
		        - actionProp->getMinActionValue(i));

		buffData->setData(data);
		buffData->operator[](i) = data->operator[](i) - stepSize_i;
		dynModel->transitionFunction(buffState, dynModel->getContinuousAction(),
		    nextState1, buffData);

		buffData->operator[](i) = data->operator[](i) + stepSize_i;
		dynModel->transitionFunction(buffState, dynModel->getContinuousAction(),
		    nextState2, buffData);

		for(unsigned int row = 0; row < stateProp->getNumContinuousStates();
		    row++) {
			dModelInput->operator()(row, column) =
			    (nextState2->getSingleStateDifference(row,
			        nextState1->getContinuousState(row))) / (2 * stepSize_i);
		}
	}
}

PEGASUSPolicyGradientCalculator::PEGASUSPolicyGradientCalculator(
	AgentSimulator* agentSimulator, RewardFunction *reward,
    ContinuousActionGradientPolicy *policy,
    TransitionFunctionEnvironment *dynModel, int numStartStates, int horizon,
    RealType gamma
):
	PolicyGradientCalculator(policy, nullptr)
{
	addParameter("DiscountFactor", gamma);
	addParameter("PEGASUSHorizon", horizon);
	addParameter("PEGASUSNumStartStates", numStartStates);
	addParameter("PEGASUSUseNewStartStates", 0.0);

	this->dynModel = dynModel;

	startStates = new StateList(dynModel->getStateProperties());

	this->policy = policy;

	setRandomStartStates();

	sameStateEvaluator = new ValueSameStateCalculator(agentSimulator, reward, dynModel,
	    startStates, (int) getParameter("PEGASUSHorizon"),
	    getParameter("DiscountFactor"));
	evaluator = sameStateEvaluator;
}

PEGASUSPolicyGradientCalculator::~PEGASUSPolicyGradientCalculator() {
	delete startStates;
	delete evaluator;
}

void PEGASUSPolicyGradientCalculator::getGradient(FeatureList *gradient) {
	bool bUseNewStates = getParameter("PEGASUSUseNewStartStates") > 0.5;
	if(bUseNewStates || startStates->getNumStates() == 0) {
		setRandomStartStates();
	}
	getPEGASUSGradient(gradient, startStates);

	gradient->multFactor(-1.0);
}

StateList* PEGASUSPolicyGradientCalculator::getStartStates() {
	return startStates;
}

void PEGASUSPolicyGradientCalculator::setStartStates(StateList *startStates) {
	this->startStates->clear();
	State *state = new State(dynModel->getStateProperties());

	for(unsigned int i = 0; i < startStates->getNumStates(); i++) {
		startStates->getState(i, state);

		this->startStates->addState(state);
	}

	delete state;
}

void PEGASUSPolicyGradientCalculator::setRandomStartStates() {
	this->startStates->clear();
	StateProperties *properties = dynModel->getStateProperties();
	State *state = new State(properties);

	for(int i = 0; i < my_round(getParameter("PEGASUSNumStartStates")); i++) {
		dynModel->resetModel();
		dynModel->getState(state);

		this->startStates->addState(state);
	}

	delete state;
}

PEGASUSAnalyticalPolicyGradientCalculator::PEGASUSAnalyticalPolicyGradientCalculator(
    AgentSimulator* agentSimulator, ContinuousActionGradientPolicy *policy,
    CAGradientPolicyInputDerivationCalculator *policydInput,
    TransitionFunctionEnvironment *dynModel,
    TransitionFunctionInputDerivationCalculator *dynModeldInput,
    StateReward *rewardFunction, int numStartStates, int horizon, RealType gamma
):
    PEGASUSPolicyGradientCalculator(agentSimulator, rewardFunction, policy, dynModel,
	        numStartStates, horizon, gamma)
{
	addParameters(policydInput, "DPolicy");
	addParameters(dynModeldInput, "DModel");

	int numActionValues =
	    policy->getContinuousActionProperties()->getNumActionValues();

	this->rewardFunction = rewardFunction;
	this->agentSimulator = agentSimulator;
	this->dynModeldInput = dynModeldInput;
	this->policydInput = policydInput;

	this->dReward = new ColumnVector(dynModel->getNumContinuousStates());
	stateGradient1 = new std::list<FeatureList *>();

	for(unsigned int i = 0; i < dynModel->getNumContinuousStates(); i++) {
		stateGradient1->push_back(new FeatureList());
	}

	stateGradient2 = new std::list<FeatureList *>();

	for(unsigned int i = 0; i < dynModel->getNumContinuousStates(); i++) {
		stateGradient2->push_back(new FeatureList());
	}

	dModelGradient = new std::list<FeatureList *>();

	for(int i = 0; i < numActionValues; i++) {
		dModelGradient->push_back(new FeatureList());
	}

	episodeGradient = new FeatureList();

	dPolicy = new Matrix(numActionValues, dynModel->getNumContinuousStates());
	dModelInput = new Matrix(dynModel->getNumContinuousStates(),
	    numActionValues + dynModel->getNumContinuousStates());

	steps = 0;
}

PEGASUSAnalyticalPolicyGradientCalculator::~PEGASUSAnalyticalPolicyGradientCalculator() {
	delete dReward;

	std::list<FeatureList *>::iterator it = stateGradient1->begin();

	for(; it != stateGradient1->end(); it++) {
		delete *it;
	}

	delete stateGradient1;

	it = stateGradient2->begin();

	for(; it != stateGradient2->end(); it++) {
		delete *it;
	}

	it = dModelGradient->begin();

	for(; it != dModelGradient->end(); it++) {
		delete *it;
	}

	delete dModelGradient;

	delete episodeGradient;
	delete dPolicy;
	delete dModelInput;
}

void PEGASUSAnalyticalPolicyGradientCalculator::getPEGASUSGradient(
    FeatureList *gradientFeatures, StateList *startStates) {
	printf("Pegasus Gradient Evaluation\n");
	agentSimulator->getAgent()->addSemiMDPListener(this);
	int horizon = my_round(getParameter("PEGASUSHorizon"));
	State *startState = new State(dynModel->getStateProperties());
	for(unsigned int i = 0; i < startStates->getNumStates(); i++) {
		printf("Evaluate Episode %d\n", i);
		agentSimulator->startNewEpisode();
		startStates->getState(i, startState);
		dynModel->setState(startState);

		agentSimulator->doControllerEpisode(horizon);
		gradientFeatures->add(episodeGradient, 1.0);
	}

	gradientFeatures->multFactor(1.0 / startStates->getNumStates());
	RealType norm = sqrt(gradientFeatures->multFeatureList(gradientFeatures));

	if(DebugIsEnabled()) {
		DebugPrint('p', "Calculated Pegasus Gradient Norm: %f\n", norm);
		DebugPrint('p', "Calculated Gradient:\n");

		gradientFeatures->save(DebugGetFileHandle('p'));
	}
	printf("Finished Gradient Calculation, Gradient Norm: %f\n", norm);

	delete startState;
	agentSimulator->getAgent()->removeSemiMDPListener(this);
}

void PEGASUSAnalyticalPolicyGradientCalculator::multMatrixFeatureList(
    Matrix *matrix, FeatureList *features, int index,
    std::list<FeatureList *> *newFeatures) {
	FeatureList::iterator itFeat = features->begin();

	for(; itFeat != features->end(); itFeat++) {
		std::list<FeatureList *>::iterator itList = newFeatures->begin();
		for(int row = 0; itList != newFeatures->end(); itList++, row++) {
			(*itList)->update((*itFeat)->featureIndex,
			    (*itFeat)->factor * matrix->operator()(row, index));
		}
	}
}

void PEGASUSAnalyticalPolicyGradientCalculator::nextStep(
    StateCollection *oldStateCol, Action *action,
    StateCollection *newStateCol) {
	State *oldState = oldStateCol->getState(dynModel->getStateProperties());
	State *nextState = newStateCol->getState(dynModel->getStateProperties());

	ContinuousActionData *data =
	    dynamic_cast<ContinuousActionData *>(action->getActionData());

	// Clear 2nd StateGradient list
	std::list<FeatureList *>::iterator it = stateGradient2->begin();

	for(; it != stateGradient2->end(); it++) {
		(*it)->clear();
	}

	//Clear Model Gradient
	it = dModelGradient->begin();

	for(; it != dModelGradient->end(); it++) {
		(*it)->clear();
	}

	// Derivation of the Reward Function
	rewardFunction->getInputDerivation(nextState, dReward);

	// Derivation of the Model
	dynModeldInput->getInputDerivation(oldState, data, dModelInput);

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "Pegasus Gradient Calculation:\n ");
		DebugPrint('p', "State Gradient:\n ");
		for(it = stateGradient1->begin(); it != stateGradient1->end(); it++) {
			(*it)->save(DebugGetFileHandle('p'));
			DebugPrint('p', "\n");
		}

		DebugPrint('p', "dReward: ");
		//dReward->saveASCII(DebugGetFileHandle('p'));

		DebugPrint('p', "\n");
		DebugPrint('p', "dModel: ");
		//dModelInput->saveASCII(DebugGetFileHandle('p'));
	}

	it = stateGradient1->begin();
	for(unsigned int i = 0; i < dynModel->getNumContinuousStates(); i++, it++) {
		multMatrixFeatureList(dModelInput, *it, i, stateGradient2);
	}

	// Derivation of the policy
	policydInput->getInputDerivation(oldStateCol, dPolicy);

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "dPolicy: ");
		//dPolicy->saveASCII(DebugGetFileHandle('p'));
		DebugPrint('p', "\n");
	}

	it = dModelGradient->begin();

	for(int i = 0; it != dModelGradient->end(); it++, i++) {
		policy->getGradient(oldStateCol, i, *it);
	}

	it = stateGradient1->begin();

	for(int i = 0; it != stateGradient1->end(); i++, it++) {
		multMatrixFeatureList(dPolicy, *it, i, dModelGradient);
	}

	it = dModelGradient->begin();

	for(int i = 0; it != dModelGradient->end(); it++, i++) {
		multMatrixFeatureList(dModelInput, *it,
		    i + dynModel->getNumContinuousStates(), stateGradient2);
	}

	if(DebugIsEnabled('p')) {
		DebugPrint('p', "Model Gradients:\n ");
		for(it = dModelGradient->begin(); it != dModelGradient->end(); it++) {
			(*it)->save(DebugGetFileHandle('p'));
			DebugPrint('p', "\n");

		}
		DebugPrint('p', "New State Gradient:\n ");

		for(it = stateGradient2->begin(); it != stateGradient2->end(); it++) {
			(*it)->save(DebugGetFileHandle('p'));
			DebugPrint('p', "\n");

		}

	}

	RealType discountFactor = pow(getParameter("DiscountFactor"), steps);
	*dReward *= discountFactor;

	it = stateGradient2->begin();
	for(int i = 0; it != stateGradient2->end(); i++, it++) {
		episodeGradient->add(*it, dReward->operator[](i));
	}

	std::list<FeatureList *> *tempGradient = stateGradient1;
	stateGradient1 = stateGradient2;
	stateGradient2 = tempGradient;

	steps++;
}

void PEGASUSAnalyticalPolicyGradientCalculator::newEpisode() {
	std::list<FeatureList *>::iterator it = stateGradient1->begin();

	for(; it != stateGradient1->end(); it++) {
		(*it)->clear();
	}
	episodeGradient->clear();
	steps = 0;
}

PEGASUSNumericPolicyGradientCalculator::PEGASUSNumericPolicyGradientCalculator(
    AgentSimulator* agentSimulator, ContinuousActionGradientPolicy *policy,
    TransitionFunctionEnvironment *dynModel, RewardFunction *rewardFunction,
    RealType stepSize, int startStates, int horizon, RealType gamma
):
	PEGASUSPolicyGradientCalculator(agentSimulator, rewardFunction, policy, dynModel,
		startStates, horizon, gamma),
	gradientFeatures(nullptr),
	weights(new RealType[policy->getNumWeights()]),
	rewardFunction(rewardFunction),
	agentSimulator(agentSimulator)
{
	addParameter("PEGASUSNumericStepSize", stepSize);
	addParameter("DiscountFactor", gamma);
}

PEGASUSNumericPolicyGradientCalculator::~PEGASUSNumericPolicyGradientCalculator() {
	delete[] weights;
}

void PEGASUSNumericPolicyGradientCalculator::getPEGASUSGradient(
    FeatureList *gradientFeatures, StateList *startStates) {
	sameStateEvaluator->setStartStates(startStates);

	policy->getWeights(weights);
	agentSimulator->getAgent()->setController(policy);

	RealType stepSize = getParameter("PEGASUSNumericStepSize");
	RealType value = evaluator->evaluatePolicy();

	for(int i = 0; i < policy->getNumWeights(); i++) {
		weights[i] -= stepSize;
		policy->setWeights(weights);
		RealType vMinus = evaluator->evaluatePolicy();
		weights[i] += 2 * stepSize;
		policy->setWeights(weights);
		RealType vPlus = evaluator->evaluatePolicy();

		weights[i] -= stepSize;

		if(vMinus > value || vPlus > value) {
			gradientFeatures->set(i, (vPlus - vMinus) / (2 * stepSize));
		} else {
			gradientFeatures->set(i, 0);
		}

		printf("%f %f %f %d %d\n", stepSize, vPlus, vMinus,
		    startStates->getNumStates(),
		    sameStateEvaluator->getStartStates()->getNumStates());
		printf("Calculated derivation for weight %d : %f\n", i,
		    gradientFeatures->getFeatureFactor(i));
	}

	policy->setWeights(weights);
}

} //namespace rlearner
