// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>
#include <iostream>

#include "extratrees.h"
#include "datafactory.h"
#include "inputdata.h"
#include "system.h"

namespace rlearner {

ExtraTreesSplittingConditionFactory::ExtraTreesSplittingConditionFactory(
    DataSet *l_inputData, DataSet1D *l_outputData, unsigned int l_K,
    unsigned int l_n_min, RealType l_outTresh, DataSet1D *l_weightingData) {
	inputData = l_inputData;
	outputData = l_outputData;
	weightingData = l_weightingData;

	K = l_K;
	n_min = l_n_min;
	outThreshold = l_outTresh;
}

ExtraTreesSplittingConditionFactory::~ExtraTreesSplittingConditionFactory() {}

RealType ExtraTreesSplittingConditionFactory::getScore(
    SplittingCondition *condition, DataSubset *dataSubset) {
	DataSubset leftDataSubset;
	DataSubset rightDataSubset;

	DataSubset::iterator it = dataSubset->begin();

	for(; it != dataSubset->end(); it++) {
		ColumnVector *vector = (*inputData)[*it];
		if(condition->isLeftNode(vector)) {
			leftDataSubset.insert(*it);
		} else {
			rightDataSubset.insert(*it);
		}
	}
	if(leftDataSubset.size() == 0 || rightDataSubset.size() == 0) {
		return 1.0;
	}

	return (-outputData->getVariance(&leftDataSubset, weightingData)
	    * leftDataSubset.size()
	    - outputData->getVariance(&rightDataSubset, weightingData)
	        * rightDataSubset.size()) / dataSubset->size();

}

SplittingCondition *ExtraTreesSplittingConditionFactory::createSplittingCondition(
    DataSubset *dataSubset) {
	auto rand = make_rand();

	ColumnVector minVector(inputData->getNumDimensions());
	ColumnVector maxVector(inputData->getNumDimensions());

	DataSubset::iterator it = dataSubset->begin();

	ColumnVector *vector = (*inputData)[*it];
	minVector = *vector;
	maxVector = *vector;

	for(; it != dataSubset->end(); it++) {
		ColumnVector *vector = (*inputData)[*it];

		for(int i = 0; i < inputData->getNumDimensions(); i++) {
			if(vector->operator[](i) < minVector.operator[](i)) {
				minVector.operator[](i) = vector->operator[](i);
			}
			if(vector->operator[](i) > maxVector.operator[](i)) {
				maxVector.operator[](i) = vector->operator[](i);
			}
		}
	}
	RealType bestScore = 0.0;
	SplittingCondition *bestSplit = nullptr;
	unsigned int i = 0;

	int numValid = 0;
	while(i < K || numValid == 0) {
		int dim = rand() % inputData->getNumDimensions();

		RealType treshold = (((RealType) rand()) / RAND_MAX)
		    * (maxVector.operator[](dim) - minVector.operator[](dim))
		    + minVector.operator[](dim);

		SplittingCondition *newSplit = new SplittingCondition1D(dim, treshold);

		RealType score = getScore(newSplit, dataSubset);

		if((score > bestScore || numValid == 0) && score <= 0.5) {
			bestScore = score;

			if(bestSplit != nullptr) {
				delete bestSplit;
			}
			bestSplit = newSplit;
		} else {
			delete newSplit;
		}
		if(score <= 0.5) {
			numValid++;
		}
		i++;
		if(i > 100) {
			//printf("%d: %d %f\n", i, dataSubset->size(), score);
		}
		if(i > 200) {
			it = dataSubset->begin();
			printf("Could not find a split for : \n");
			for(; it != dataSubset->end(); it++) {
				std::cout << (*inputData)[*it]->transpose();
			}

			exit(0);
		}
	}
	return bestSplit;
}

bool ExtraTreesSplittingConditionFactory::isLeaf(DataSubset *dataSubset) {
	bool minSampels = dataSubset->size() < n_min;

	bool outputVar = outputData->getVariance(dataSubset, weightingData)
	    < outThreshold;

	RealType inputVar = inputData->getVarianceNorm(dataSubset);

	bool leaf = minSampels || outputVar || inputVar <= 0.0001;

	/*if (leaf)
	 {
	 printf("Leaf: Input Variance: %f (%d, %d)\n", inputVar, dataSubset->size(), n_min);

	 }
	 else
	 {
	 printf("Input Variance: %f (%d)\n", inputVar, dataSubset->size());
	 }*/

	return leaf;
}

ExtraRegressionTree::ExtraRegressionTree(
    DataSet *inputData, DataSet1D *outputData, unsigned int K,
    unsigned int n_min, RealType treshold, DataSet1D *weightData) :
	    ExtraTree<RealType>(inputData, outputData,
	        new RegressionFactory(outputData, weightData), K, n_min, treshold,
	        weightData) {}

ExtraRegressionTree::~ExtraRegressionTree() {
	delete root;
	root = nullptr;
	delete dataFactory;
}

} //namespace rlearner
