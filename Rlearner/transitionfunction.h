// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ctransitionfunction_H_
#define Rlearner_ctransitionfunction_H_

#include "parameters.h"
#include "baseobjects.h"
#include "rewardfunction.h"
#include "environmentmodel.h"
#include "qfunction.h"

#define DM_CONTINUOUSMODEL 1
#define DM_DERIVATIONUMODEL 2
#define DM_AF_ExtendedActionMODEL 4

#define DM_RESET_TYPE_ALL_RANDOM 2
#define DM_RESET_TYPE_RANDOM 1
#define DM_RESET_TYPE_ZERO 0

namespace rlearner {

class StateCollectionImpl;
class StateCollectionList;
class ActionDataSet;
class ContinuousActionProperties;
class ContinuousAction;
class StateList;
class Region;
class AbstractFeatureStochasticModel;
class VFunctionInputDerivationCalculator;
class AbstractVFunction;

class TransitionFunction:
    public StateObject, public ActionObject, virtual public ParameterObject {
protected:
	int type;
	int resetType;

public:
	int getType();
	void addType(int Type);
	bool isType(int type);

	virtual void transitionFunction(
	    State *oldstate,
	    Action *action,
	    State *newState,
	    ActionData *data = nullptr) = 0;

	virtual void getDerivationU(State *oldstate, Matrix *derivation);

	virtual bool isResetState(State *) {
		return false;
	}

	virtual bool isFailedState(State *) {
		return false;
	}

	virtual void getResetState(State *resetState);

	virtual void setResetType(int resetType);

public:
	TransitionFunction& operator=(const TransitionFunction&) = delete;
	TransitionFunction(const TransitionFunction&) = delete;

	TransitionFunction(StateProperties *properties, ActionSet *actions);
	virtual ~TransitionFunction() = default;
};

class ExtendedActionTransitionFunction:
    public TransitionFunction, public RewardFunction {
protected:
	TransitionFunction *dynModel;
	StateCollectionImpl *intermediateState;
	StateCollectionImpl *nextState;
	ActionDataSet *actionDataSet;
	RewardFunction *rewardFunction;
	RealType lastReward;

public:
	virtual void transitionFunction(
	    State *oldstate,
	    Action *action,
	    State *newState,
	    ActionData *data = nullptr);
	virtual RealType transitionFunctionAndReward(
	    State *oldState,
	    Action *action,
	    State *newState,
	    ActionData *data,
	    RewardFunction *reward,
	    RealType gamma);

	virtual void getDerivationU(State *oldstate, Matrix *derivation);

	virtual bool isResetState(State *state);
	virtual bool isFailedState(State *state);

	virtual void getResetState(State *resetState);

	virtual void setResetType(int resetType);

	virtual RealType getReward(
	    StateCollection *oldState,
	    Action *action,
	    StateCollection *newState);

public:
	ExtendedActionTransitionFunction& operator=(
	    const ExtendedActionTransitionFunction&) = delete;
	ExtendedActionTransitionFunction(const ExtendedActionTransitionFunction&) = delete;

	ExtendedActionTransitionFunction(
	    ActionSet *actions,
	    TransitionFunction *model,
	    const std::set<StateModifierList*>& modifierLists,
	    RewardFunction *rewardFunction = nullptr);
	virtual ~ExtendedActionTransitionFunction();
};

class ComposedTransitionFunction: public TransitionFunction {
protected:
	std::list<TransitionFunction *>* _transitionFunction;

public:
	void addTransitionFunction(TransitionFunction *model);
	virtual void transitionFunction(
	    State *oldstate,
	    Action *action,
	    State *newState,
	    ActionData *data = nullptr);

public:
	ComposedTransitionFunction& operator=(const ComposedTransitionFunction&) = delete;
	ComposedTransitionFunction(const ComposedTransitionFunction&) = delete;

	ComposedTransitionFunction(StateProperties *properties);
	virtual ~ComposedTransitionFunction();
};

class ContinuousTimeTransitionFunction: public TransitionFunction {
protected:
	RealType dt;
	int simulationSteps;

	ColumnVector *derivation;

	virtual void doSimulationStep(
	    State *oldState,
	    RealType timeStep,
	    Action *action,
	    ActionData *data);

public:
	virtual void transitionFunction(
	    State *oldstate,
	    Action *action,
	    State *newState,
	    ActionData *data = nullptr);

	RealType getTimeIntervall();
	void setTimeIntervall(RealType dt);

	void setSimulationSteps(int steps);
	int getSimulationSteps();

	virtual void getDerivationX(
	    State *oldstate,
	    Action *action,
	    ColumnVector *derivation,
	    ActionData *data = nullptr) = 0;

public:
	ContinuousTimeTransitionFunction& operator=(
	    const ContinuousTimeTransitionFunction&
	) = delete;

	ContinuousTimeTransitionFunction(
		const ContinuousTimeTransitionFunction&
	) = delete;

	ContinuousTimeTransitionFunction(
	    StateProperties *properties,
	    ActionSet *actions,
	    RealType dt
	);

	virtual ~ContinuousTimeTransitionFunction();
};

class ContinuousAction;
class ContinuousActionData;

class ContinuousTimeAndActionTransitionFunction:
    public ContinuousTimeTransitionFunction {
protected:
	ContinuousActionProperties *actionProp;
	ContinuousAction *contAction;

public:
	virtual void getDerivationX(
	    State *oldState,
	    Action *action,
	    ColumnVector *derivationX,
	    ActionData *data = nullptr);
	virtual void getCADerivationX(
	    State *oldState,
	    ContinuousActionData *action,
	    ColumnVector *derivationX) = 0;

	ContinuousAction *getContinuousAction();

public:
	ContinuousTimeAndActionTransitionFunction& operator=(
	    const ContinuousTimeAndActionTransitionFunction&) = delete;
	ContinuousTimeAndActionTransitionFunction(
	    const ContinuousTimeAndActionTransitionFunction&) = delete;

	ContinuousTimeAndActionTransitionFunction(
	    StateProperties *properties,
	    ContinuousAction *action,
	    RealType dt);
	virtual ~ContinuousTimeAndActionTransitionFunction();
};

class LinearActionContinuousTimeTransitionFunction:
    public ContinuousTimeAndActionTransitionFunction {
protected:
	ColumnVector *A;
	Matrix *B;

public:
	virtual void getCADerivationX(
	    State *oldState,
	    ContinuousActionData *action,
	    ColumnVector *derivationX
	);

	virtual void getDerivationU(State *oldstate, Matrix *derivation);
	virtual Matrix *getB(State *state) = 0;
	virtual ColumnVector *getA(State *state) = 0;

public:
	LinearActionContinuousTimeTransitionFunction& operator=(
	    const LinearActionContinuousTimeTransitionFunction&) = delete;
	LinearActionContinuousTimeTransitionFunction(
	    const LinearActionContinuousTimeTransitionFunction&) = delete;

	LinearActionContinuousTimeTransitionFunction(
	    StateProperties *properties,
	    ContinuousAction *action,
	    RealType dt
	);

	virtual ~LinearActionContinuousTimeTransitionFunction();
};

class DynamicLinearContinuousTimeModel:
    public LinearActionContinuousTimeTransitionFunction {
protected:
	Matrix *B;
	Matrix *AMatrix;

public:
	virtual Matrix *getB(State *state);
	virtual ColumnVector *getA(State *state);

public:
	DynamicLinearContinuousTimeModel& operator=(
	    const DynamicLinearContinuousTimeModel&) = delete;
	DynamicLinearContinuousTimeModel(const DynamicLinearContinuousTimeModel&) = delete;

	DynamicLinearContinuousTimeModel(
	    StateProperties *properties,
	    ContinuousAction *action,
	    RealType dt,
	    Matrix *A,
	    Matrix *B
	);

	virtual ~DynamicLinearContinuousTimeModel();
};

class TransitionFunctionEnvironment: public EnvironmentModel {
protected:
	TransitionFunction* _transitionFunction;
	State *modelState;
	State *nextState;

	StateList *startStates;
	int nEpisode;
	bool createdStartStates;

	Region *failedRegion;
	Region *sampleRegion;
	Region *targetRegion;

public:
	virtual void doNextState(PrimitiveAction *action);
	virtual void doResetModel();

	virtual void getState(State *state);

	virtual void setState(State *state);

	virtual void setStartStates(StateList *startStates);
	virtual void setStartStates(const std::string& filename);

	TransitionFunction *getTransitionFunction() {
		return _transitionFunction;
	}

	void setSampleRegion(Region *sampleRegion);
	void setFailedRegion(Region *failedRegion);
	void setTargetRegion(Region *sampleRegion);

public:
	TransitionFunctionEnvironment& operator=(
	    const TransitionFunctionEnvironment&) = delete;
	TransitionFunctionEnvironment(const TransitionFunctionEnvironment&) = delete;

	TransitionFunctionEnvironment(TransitionFunction *model);
	virtual ~TransitionFunctionEnvironment();
};

class TransitionFunctionFromStochasticModel: public TransitionFunction {
protected:
	AbstractFeatureStochasticModel *stochasticModel;

	std::list<int> *startStates;
	std::list<RealType> *startProbabilities;
	std::map<int, RealType> *endStates;

public:
	virtual void transitionFunction(
	    State *oldstate,
	    Action *action,
	    State *newState,
	    ActionData *data = nullptr);

	void addStartState(int state, RealType probability);
	void addEndState(int state, RealType probability);

	virtual bool isResetState(State *state);
	virtual void getResetState(State *state);

public:
	TransitionFunctionFromStochasticModel& operator=(
	    const TransitionFunctionFromStochasticModel&) = delete;
	TransitionFunctionFromStochasticModel(
	    const TransitionFunctionFromStochasticModel&) = delete;

	TransitionFunctionFromStochasticModel(
	    StateProperties *properties,
	    AbstractFeatureStochasticModel *model);
	~TransitionFunctionFromStochasticModel();
};

class QFunctionFromTransitionFunction:
    public AbstractQFunction, public StateModifiersObject {
protected:
	//! The given V-Function.
	AbstractVFunction *vfunction;
	//! The model.
	TransitionFunction *model;
	//! Feature Reward Function for the learning problem.
	RewardFunction *rewardfunction;

	//! State buffer.
	StateCollectionImpl *intermediateState;
	StateCollectionImpl *nextState;

	StateCollectionList *stateCollectionList;
	ActionDataSet *actionDataSet;

public:
	//! Does nothing.
	virtual void setValue(StateCollection *, Action *, RealType, ActionData * =
	    nullptr) {
	}

	//! Does nothing.
	virtual void updateValue(StateCollection *, Action *, RealType, ActionData * =
	    nullptr) {
	}

	/**
	 * A getValue function for state collections.
	 *
	 * Calls the getValue function for the specific state (retrieved from
	 * the collection by the discretizer).
	 */
	virtual RealType getValue(
	    StateCollection *state,
	    Action *action,
	    ActionData *data = nullptr);

	RealType getValueDepthSearch(
	    StateCollectionList *state,
	    Action *action,
	    ActionData *data,
	    int depth);

	virtual AbstractQETraces *getStandardETraces() {
		return nullptr;
	}

	virtual void addModifierList(StateModifierList* modifierList);

public:
	QFunctionFromTransitionFunction& operator=(
	    const QFunctionFromTransitionFunction&) = delete;
	QFunctionFromTransitionFunction(const QFunctionFromTransitionFunction&) = delete;

	//! Creates a new QFunction from VFunction object for the given V-Function
	//! and the given model, the discretizer is taken from the V-Function.
	QFunctionFromTransitionFunction(
	    ActionSet *actions,
	    AbstractVFunction *vfunction,
	    TransitionFunction *model,
	    RewardFunction *rewardfunction,
	    const std::set<StateModifierList*>& modifierLists);

	virtual ~QFunctionFromTransitionFunction();
};

class ContinuousTimeQFunctionFromTransitionFunction:
    public AbstractQFunction, public StateModifiersObject {
protected:
	//! The given V-Function.
	VFunctionInputDerivationCalculator *vfunction;
	//! The model.
	ContinuousTimeTransitionFunction *model;
	//! Feature Reward Function for the learning problem.
	RewardFunction *rewardfunction;

	StateCollectionImpl *nextState;

	State *derivationXModel;
	State *derivationXVFunction;

	virtual RealType getValueVDerivation(
	    StateCollection *state,
	    Action *action,
	    ActionData *data,
	    ColumnVector *derivationXVFunction);

public:
	virtual void getActionValues(StateCollection *state, ActionSet *actions,
	RealType *actionValues, ActionDataSet *actionDataSet);

	//! Does nothing.
	virtual void setValue(StateCollection *, Action *, RealType, ActionData * =
	    nullptr) {
	}

	//! Does nothing.
	virtual void updateValue(StateCollection *, Action *, RealType, ActionData * =
	    nullptr) {
	}

	/**
	 * A getValue function for state collections.
	 *
	 * Calls the getValue function for the specific state (retrieved from the
	 * collection by the discretizer).
	 */
	virtual RealType getValue(
	    StateCollection *state,
	    Action *action,
	    ActionData *data = nullptr);

	virtual AbstractQETraces *getStandardETraces() {
		return nullptr;
	}

	virtual void addModifierList(StateModifierList* modifierList);

public:
	ContinuousTimeQFunctionFromTransitionFunction& operator=(
	    const ContinuousTimeQFunctionFromTransitionFunction&) = delete;
	ContinuousTimeQFunctionFromTransitionFunction(
	    const ContinuousTimeQFunctionFromTransitionFunction&) = delete;

	//! Creates a new QFunction from VFunction object for the given V-Function
	//! and the given model, the discretizer is taken from the V-Function.
	ContinuousTimeQFunctionFromTransitionFunction(
	    ActionSet *actions,
	    VFunctionInputDerivationCalculator *vfunction,
	    ContinuousTimeTransitionFunction *model,
	    RewardFunction *rewardfunction,
	    const std::set<StateModifierList*>& modifierLists);

	ContinuousTimeQFunctionFromTransitionFunction(
	    ActionSet *actions,
	    VFunctionInputDerivationCalculator *vfunction,
	    ContinuousTimeTransitionFunction *model,
	    RewardFunction *rewardfunction);

	virtual ~ContinuousTimeQFunctionFromTransitionFunction();
};

} //namespace rlearner

#endif //Rlearner_ctransitionfunction_H_
