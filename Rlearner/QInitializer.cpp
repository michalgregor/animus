#include "QInitializer.h"

namespace rlearner {

void QInitializer::serialize(IArchive& ar, const unsigned int) {}
void QInitializer::serialize(OArchive& ar, const unsigned int) {}

void QInitializer_Vinit::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<QInitializer>(*this);
	ar & _vinit;
}

void QInitializer_Vinit::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<QInitializer>(*this);
	ar & _vinit;
}

void QInitializer_Vinit::init(FeatureQFunction* qFunction) {
	for(auto& vFunc: qFunction->getVFunctionList()) {
		_vinit->init(vFunc);
	}
}

} //namespace rlearner
