// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "ril_debug.h"
#include "vfunctionlearner.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "statemodifier.h"
#include "action.h"
#include "vetraces.h"
#include "vfunction.h"

namespace rlearner {

AdaptiveParameterFromValueCalculator::AdaptiveParameterFromValueCalculator(
    Parameters *targetObject, std::string targetParameter,
    AbstractVFunction *l_vFunction, int nStepsPerUpdate, int functionKind,
    RealType param0, RealType paramScale, RealType targetMin, RealType targetMax
):
	AdaptiveParameterBoundedValuesCalculator(targetObject, targetParameter,
		functionKind, param0, paramScale, targetMin, targetMax
) {
	this->vFunction = l_vFunction;
	nSteps = 0;
	value = 0;
	this->nStepsPerUpdate = nStepsPerUpdate;
}

AdaptiveParameterFromValueCalculator::~AdaptiveParameterFromValueCalculator() {}

void AdaptiveParameterFromValueCalculator::nextStep(
    StateCollection *, Action *, StateCollection *newState) {
	value += vFunction->getValue(newState);

	nSteps++;

	if(nSteps % nStepsPerUpdate == 0) {
		setParameterValue(value / nSteps);
		nSteps = 0;
		value = 0;
	}
}

void AdaptiveParameterFromValueCalculator::resetCalculator() {
	setParameterValue(targetMin);
	value = 0;
	nSteps = 0;
}

VFunctionLearner::VFunctionLearner(
    RewardFunction *rewardFunction, AbstractVFunction *vFunction,
    AbstractVETraces *eTraces) :
	SemiMDPRewardListener(rewardFunction) {
	this->vFunction = vFunction;
	this->eTraces = eTraces;

	bExternETraces = true;

	addParameter("VLearningRate", 0.2);
	addParameter("DiscountFactor", 0.95);
	addParameters(vFunction);
	addParameters(eTraces);
}

VFunctionLearner::VFunctionLearner(
    RewardFunction *rewardFunction, AbstractVFunction *vFunction) :
	SemiMDPRewardListener(rewardFunction) {
	this->vFunction = vFunction;
	this->eTraces = vFunction->getStandardETraces();

	bExternETraces = false;

	addParameter("VLearningRate", 0.2);
	addParameter("DiscountFactor", 0.95);

	addParameters(vFunction);
	addParameters(eTraces);
}

VFunctionLearner::~VFunctionLearner() {
	if(!bExternETraces) {
		delete eTraces;
	}
}

RealType VFunctionLearner::getLearningRate() {
	return getParameter("VLearningRate");
}

void VFunctionLearner::setLearningRate(RealType learningRate) {
	setParameter("VLearningRate", learningRate);
}

void VFunctionLearner::nextStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState) {
	RealType td = getTemporalDifference(oldState, action, reward, nextState);
	DebugPrint('t', "TD %f\n", td);
	updateVFunction(oldState, nextState, action->getDuration(), td);
}

void VFunctionLearner::intermediateStep(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState) {
	addETraces(oldState, nextState, action->getDuration());
	vFunction->updateValue(oldState,
	    getTemporalDifference(oldState, action, reward, nextState)
	        * getLearningRate());
}

void VFunctionLearner::updateVFunction(
    StateCollection *oldState, StateCollection *newState, int duration,
    RealType td) {
	eTraces->updateETraces(duration);
	addETraces(oldState, newState, duration);
	eTraces->updateVFunction(td * getLearningRate());
}

void VFunctionLearner::addETraces(
    StateCollection *oldState, StateCollection *, int) {
	eTraces->addETrace(oldState);
}

AbstractVETraces *VFunctionLearner::getVETraces() {
	return eTraces;
}

RealType VFunctionLearner::getTemporalDifference(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState) {
	RealType oldQValue = vFunction->getValue(oldState);
	RealType newQValue = 0.0;

	RealType temporalDifference = 0.0;

	if(!nextState->getState()->isResetState()) {
		newQValue = vFunction->getValue(nextState);
		temporalDifference = reward
		    + pow(getParameter("DiscountFactor"), action->getDuration())
		        * newQValue - oldQValue;

		DebugPrint('t', "VFunctionLearner: oldQ %f,newQ %f, reward %f, ",
		    oldQValue, newQValue, reward);
	} else {
		temporalDifference = reward - oldQValue;
		DebugPrint('t', "VFunctionLearner (last State): oldQ %f, reward %f, ",
		    oldQValue, reward);
	}

	sendErrorToListeners(temporalDifference, oldState, action, nullptr);

	return temporalDifference;
}

AbstractVFunction *VFunctionLearner::getVFunction() {
	return vFunction;
}

void VFunctionLearner::newEpisode() {
	eTraces->resetETraces();
}

VFunctionGradientLearner::VFunctionGradientLearner(
    RewardFunction *rewardFunction, GradientVFunction *vFunction,
    ResidualFunction *residual,
    ResidualGradientFunction *residualGradientFunction) :
	VFunctionLearner(rewardFunction, vFunction) {
	this->residual = residual;
	this->residualGradientFunction = residualGradientFunction;

	addParameters(residual);
	addParameters(residualGradientFunction);

	this->gradientVFunction = vFunction;
	this->oldGradient = new FeatureList();
	this->newGradient = new FeatureList();
	this->residualGradient = new FeatureList();
	this->gradientETraces = dynamic_cast<GradientVETraces *>(eTraces);
}

VFunctionGradientLearner::~VFunctionGradientLearner() {
	delete oldGradient;
	delete newGradient;
	delete residualGradient;
}

void VFunctionGradientLearner::addETraces(
    StateCollection *oldState, StateCollection *newState, int duration) {
	oldGradient->clear();
	newGradient->clear();
	residualGradient->clear();

	gradientVFunction->getGradient(oldState, oldGradient);

	if(!newState->isResetState()) {
		gradientVFunction->getGradient(newState, newGradient);
	}

	residualGradientFunction->getResidualGradient(oldGradient, newGradient,
	    duration, residualGradient);

	gradientETraces->addGradientETrace(residualGradient, -1.0);
}

RealType VFunctionGradientLearner::getTemporalDifference(
    StateCollection *oldState, Action *action, RealType reward,
    StateCollection *nextState) {

	RealType temporalDifference = 0.0;

	if(!nextState->isResetState()) {
		temporalDifference = residual->getResidual(
		    vFunction->getValue(oldState), reward, action->getDuration(),
		    vFunction->getValue(nextState));
	} else {
		temporalDifference = residual->getResidual(
		    vFunction->getValue(oldState), reward, action->getDuration(), 0.0);
	}

	sendErrorToListeners(temporalDifference, oldState, action, nullptr);

	return temporalDifference;
}

VFunctionResidualLearner::VFunctionResidualLearner(
    RewardFunction *rewardFunction, GradientVFunction *vFunction,
    ResidualFunction *residual, ResidualGradientFunction *residualGradient,
    AbstractBetaCalculator *betaCalc) :
	    VFunctionGradientLearner(rewardFunction, vFunction, residual,
	        residualGradient) {
	this->betaCalculator = betaCalc;
	this->residualETraces = new GradientVETraces(gradientVFunction);

	this->directGradientTraces = new GradientVETraces(gradientVFunction);
	this->residualGradientTraces = new GradientVETraces(gradientVFunction);

	addParameters(betaCalc);
	addParameters(residualETraces);

	addParameters(directGradientTraces, "Gradient");
	addParameters(residualGradientTraces, "Gradient");

	setParameter("GradientReplacingETraces", 0.0);
}

VFunctionResidualLearner::~VFunctionResidualLearner() {
	delete residualETraces;
	delete residualGradientTraces;
	delete directGradientTraces;
}

void VFunctionResidualLearner::newEpisode() {
	VFunctionGradientLearner::newEpisode();
	residualETraces->resetETraces();
	residualGradientTraces->resetETraces();
	directGradientTraces->resetETraces();
}

void VFunctionResidualLearner::addETraces(
    StateCollection *oldState, StateCollection *newState, int duration,
    RealType td) {
	oldGradient->clear();
	newGradient->clear();
	residualGradient->clear();

	gradientVFunction->getGradient(oldState, oldGradient);

	if(!newState->isResetState()) {
		gradientVFunction->getGradient(newState, newGradient);
	}

	residualGradientFunction->getResidualGradient(oldGradient, newGradient,
	    duration, residualGradient);

	if(DebugIsEnabled('v')) {
		DebugPrint('v', "Residual Gradient: ");
		residualGradient->save(DebugGetFileHandle('v'));
		DebugPrint('v', "\n");
	}

	directGradientTraces->addGradientETrace(oldGradient, td);
	residualGradientTraces->addGradientETrace(residualGradient, -td);

	gradientETraces->addGradientETrace(oldGradient, 1.0);
	residualETraces->addGradientETrace(residualGradient, -1.0);
}

void VFunctionResidualLearner::updateVFunction(
    StateCollection *oldState, StateCollection *newState, int duration,
    RealType td) {
	gradientETraces->updateETraces(duration);
	residualETraces->updateETraces(duration);

	residualGradientTraces->updateETraces(1);
	directGradientTraces->updateETraces(1);

	RealType beta = betaCalculator->getBeta(
	    directGradientTraces->getGradientETraces(),
	    residualGradientTraces->getGradientETraces());

	addETraces(oldState, newState, duration, td);

	RealType learningRate = getLearningRate();
	gradientETraces->updateVFunction(td * learningRate * (1 - beta));
	residualETraces->updateVFunction(td * learningRate * beta);
}

VAverageTDErrorLearner::VAverageTDErrorLearner(
    FeatureVFunction *l_averageErrorFunction, RealType l_updateRate
):
	StateObject(l_averageErrorFunction->getStateProperties())
{
	averageErrorFunction = l_averageErrorFunction;
	updateRate = l_updateRate;
	addParameter("TDErrorUpdateRate", updateRate);
}

VAverageTDErrorLearner::~VAverageTDErrorLearner() {}

void VAverageTDErrorLearner::onParametersChanged() {
	updateRate = ParameterObject::getParameter("TDErrorUpdateRate");
}

void VAverageTDErrorLearner::receiveError(
    RealType error, StateCollection *state, Action *, ActionData *
) {
	State *featureState = state->getState(
	    averageErrorFunction->getStateProperties());

	for(unsigned int i = 0; i < featureState->getNumDiscreteStates(); i++) {
		int index = featureState->getDiscreteState(i);
		RealType featureFac = featureState->getContinuousState(i);
		RealType featureVal = averageErrorFunction->getFeature(index);

		featureVal = featureVal
		    * (updateRate + (1 - featureFac) * (1 - updateRate))
		    + error * (1 - updateRate) * featureFac;
		averageErrorFunction->setFeature(index, featureVal);
	}
}

VAverageTDVarianceLearner::VAverageTDVarianceLearner(
    FeatureVFunction *averageErrorFunction, RealType updateRate) :
	VAverageTDErrorLearner(averageErrorFunction, updateRate) {}

VAverageTDVarianceLearner::~VAverageTDVarianceLearner() {}

void VAverageTDVarianceLearner::receiveError(
    RealType error, StateCollection *state, Action *action, ActionData *data) {
	VAverageTDErrorLearner::receiveError(pow(error, 2.0), state, action, data);
}

} //namespace rlearner
