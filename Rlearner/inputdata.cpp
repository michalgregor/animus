// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>

#include "inputdata.h"

namespace rlearner {

void DataPreprocessor::preprocessDataSet(DataSet *dataSet) {
	for(unsigned int i = 0; i < dataSet->size(); i++) {
		preprocessInput((*dataSet)[i], (*dataSet)[i]);
	}
}

MeanStdPreprocessor::MeanStdPreprocessor(
    ColumnVector *l_mean, ColumnVector *l_std) {
	mean = new ColumnVector(*l_mean);
	std = new ColumnVector(*l_std);
}

MeanStdPreprocessor::MeanStdPreprocessor(DataSet *dataSet) {
	mean = new ColumnVector(dataSet->getNumDimensions());
	std = new ColumnVector(dataSet->getNumDimensions());

	dataSet->getMean(nullptr, mean);
	dataSet->getVariance(nullptr, std);

	for(int i = 0; i < std->rows(); i++) {
		std->operator[](i) = sqrt(std->operator[](i));
	}
}

MeanStdPreprocessor::~MeanStdPreprocessor() {
	delete mean;
	delete std;
}

void MeanStdPreprocessor::preprocessInput(
    ColumnVector *input, ColumnVector *preInput)
{
	for(int i = 0; i < std->rows(); i++) {
		preInput->operator[](i) = (input->operator[](i) - mean->operator[](i))
		    / std->operator[](i);
	}
}

void MeanStdPreprocessor::setMean(ColumnVector *l_mean) {
	*mean = *l_mean;
}

void MeanStdPreprocessor::setStd(ColumnVector *l_std) {
	*std = *l_std;
}

DataSet::DataSet(int l_numDimensions) {
	numDimensions = l_numDimensions;

	buffVector1 = new ColumnVector(numDimensions);
	buffVector2 = new ColumnVector(numDimensions);
}

DataSet::DataSet(DataSet &dataset) :
	std::vector<ColumnVector *>() {
	numDimensions = dataset.getNumDimensions();

	buffVector1 = new ColumnVector(numDimensions);
	buffVector2 = new ColumnVector(numDimensions);

	DataSet::iterator it = dataset.begin();
	for(; it != dataset.end(); it++) {
		addInput(*it);
	}
}

DataSet::~DataSet() {
	DataSet::iterator it = begin();

	for(; it != end(); it++) {
		delete *it;
	}

	delete buffVector1;
	delete buffVector2;
}

int DataSet::getNumDimensions() {
	return numDimensions;
}

void DataSet::clear() {
	DataSet::iterator it = begin();

	for(; it != end(); it++) {
		delete *it;
	}
	std::vector<ColumnVector *>::clear();
}

void DataSet::addInput(ColumnVector *input) {
	ColumnVector *vector = new ColumnVector(*input);

	assert(vector->rows() >= numDimensions);

	push_back(vector);
}

void DataSet::saveCSV(std::ostream& stream) {
	ColumnVector *vector;
	DataSet::iterator it = begin();
	for(; it != end(); it++) {
		vector = *it;

		for(int i = 0; i < numDimensions; i++) {
			fmt::fprintf(stream, "%f ", vector->operator[](i));
		}
		fmt::fprintf(stream, "\n");
	}
}

void DataSet::loadCSV(std::istream& stream) {
	ColumnVector vector(getNumDimensions());
	while(!stream.eof()) {
		int results = 0;
		for(int i = 0; i < numDimensions; i++) {
			RealType buf = 0;

			results += xscanf(stream, "%lf ", buf);
			vector.operator[](i) = buf;
		}

		xscanf(stream, "\n");

		if(results == numDimensions) {
			addInput(&vector);
		} else {
			throw TracedError_InvalidFormat("Loading Input Data: Wrong number of Dimensions!");
		}
	}
}

void DataSet::getSubSet(DataSubset *subSet, DataSet *newSet) {
	DataSubset::iterator it = subSet->begin();

	for(; it != subSet->begin(); it++) {
		newSet->addInput((*this)[(*it)]);
	}
}

RealType DataSet::getVarianceNorm(DataSubset *dataSubset) {
	getVariance(dataSubset, buffVector1);
	return buffVector1->norm();
}

void DataSet::getVariance(DataSubset *dataSubset, ColumnVector *variance) {
	ColumnVector *mean = buffVector1;
	ColumnVector *squaredMean = buffVector2;

	mean->setZero();
	squaredMean->setZero();

	if(dataSubset != nullptr) {
		DataSubset::iterator it = dataSubset->begin();

		for(; it != dataSubset->end(); it++) {
			ColumnVector *data = (*this)[*it];
			*mean += *data;

			*squaredMean += ColumnVector(data->array().pow(2));
		}
		(*mean) = (*mean) / dataSubset->size();
		(*squaredMean) = (*squaredMean) / dataSubset->size();
	} else {
		for(unsigned int i = 0; i < size(); i++) {
			ColumnVector *data = (*this)[i];
			(*mean) += *data;

			*squaredMean += ColumnVector(data->array().pow(2));
		}
		(*mean) = *mean / size();
		(*squaredMean) = *squaredMean / size();
	}

	*mean = mean->array().pow(2);
	variance->noalias() = *squaredMean - *mean;
}

void DataSet::getMean(DataSubset *dataSubset, ColumnVector *mean) {
	mean->setZero();

	if(dataSubset) {
		DataSubset::iterator it = dataSubset->begin();

		for(; it != dataSubset->end(); it++) {
			ColumnVector *data = (*this)[*it];
			*mean += *data;

		}
		(*mean) = *mean / dataSubset->size();
	} else {
		for(unsigned int i = 0; i < size(); i++) {
			ColumnVector *data = (*this)[i];
			*mean += *data;
		}
		(*mean) = *mean / size();

	}
}

DataSet1D::DataSet1D(DataSet1D &dataset):
	std::vector<RealType>(dataset) {}

DataSet1D::DataSet1D():
	std::vector<RealType>() {}

void DataSet1D::loadCSV(std::istream& stream) {
	while(!stream.eof()) {
		RealType buf = 0;

		if(xscanf(stream, "%lf ", buf) == 1) {
			push_back(buf);
		} else {
			throw TracedError_InvalidFormat("Loading Input Data: Wrong number  of Dimensions!");
		}
	}
}

void DataSet1D::saveCSV(std::ostream& stream) {
	DataSet1D::iterator it = begin();
	for(; it != end(); it++) {
		fmt::fprintf(stream, "%f ", *it);
		fmt::fprintf(stream, "\n");
	}
}

RealType DataSet1D::getVariance(DataSubset *dataSubset, DataSet1D *weighting) {
	RealType mean = 0;
	RealType squaredMean = 0;

	RealType weightSum = 0;
	if(dataSubset == nullptr) {
		for(unsigned int i = 0; i < size(); i++) {
			RealType weight = 1.0;

			if(weighting != nullptr) {
				weight = (*weighting)[i];
			}

			mean += weight * (*this)[i];
			squaredMean += weight * pow((*this)[i], 2.0);

			weightSum += weight;
		}
	} else {
		DataSubset::iterator it = dataSubset->begin();

		for(; it != dataSubset->end(); it++) {
			RealType weight = 1.0;

			if(weighting != nullptr) {
				weight = (*weighting)[*it];
			}

			mean += weight * (*this)[*it];
			squaredMean += weight * pow((*this)[*it], 2.0);

			weightSum += weight;
		}
	}
	if(weightSum > 0) {
		mean /= weightSum;
		squaredMean /= weightSum;
	}

	return squaredMean - pow(mean, 2.0);
}

RealType DataSet1D::getMean(DataSubset *dataSubset, DataSet1D *weighting) {
	RealType mean = 0;
	RealType weightSum = 0;

	if(dataSubset != nullptr) {
		DataSubset::iterator it = dataSubset->begin();

		for(; it != dataSubset->end(); it++) {
			RealType weight = 1.0;

			if(weighting != nullptr) {
				weight = (*weighting)[*it];
			}
			mean += weight * (*this)[*it];
			weightSum += weight;
		}
	} else {
		for(unsigned int i = 0; i < size(); i++) {
			RealType weight = 1.0;

			if(weighting != nullptr) {
				weight = (*weighting)[i];
			}
			mean += weight * (*this)[i];
			weightSum += weight;
		}
	}

	if(weightSum > 0) {
		mean /= weightSum;
	}

	return mean;
}

void DataSubset::addElements(std::list<int> *subsetList) {
	std::list<int>::iterator it = subsetList->begin();

	for(; it != subsetList->end(); it++) {
		insert(*it);
	}
}

} //namespace rlearner
