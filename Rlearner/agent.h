// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cagent_H_
#define Rlearner_cagent_H_

#include <map>
#include <list>
#include <vector>

#include "mdp.h"
#include "StateModifierList.h"

namespace rlearner {

/**
 * @class Agent
 * The class represents the main acting Object of the Learning System,
 * the agent.
 *
 * The agent is the object which acts within its environment an sends every
 * step to its SemiMDPListener, its the only "acting" object so it's the most
 * important part of the toolbox. The Agent follows the Policy set by
 * setController(AgentController *). It saves the currentState, then tells
 * the model which action to execute and then saves the new state. Having done
 * that the agent is able to send the State-Action-State tuple to all its
 * Listeners.
 *
 * The agent's actionset can only maintain PrimitiveActions. The agent has
 * an agent controller which can choose from the actions in the agent's
 * actionset. It is not allowed that an controller returns an action which
 * isn't in the agent's action set. ExtendedActions can only be added to
 * the HierarchicalSemiMarkovDecisionProcess class.
 *
 * Another important functionality of the agent are the StateModifiers which
 * can be added to the agent. The stateModifier is then added to the
 * stateCollections (currentState, lastState) of the agent. If you add
 * a StateModifier to the agent, the modified state is calculated  by the
 * state modifier after the modelstate has changed and added to the state
 * collection. The modified State, which can be discrete, a feature or any
 * other State calculated from the original model state is now available to all
 * Listeners, and it only gets calculated once. So the Listeners have access
 * to several different kind of states.
 *
 * For the execution of actions you have several possibilities, the agent
 * can execute:
 * - a given action (doAction);
 * - a single action from the controller (doControllerStep);
 * - or one or more Episodes following the Policy from the Controller
 *   (doControllerEpisode). You can specify how much steps each episode should
 *   have at maximum.
 *
 * The agent also logs the current episode. You need the current Episode
 * for the hierarchical SMDPs, they need an instance of the Episode
 * to reconstruct the intermediate steps. This feature can be turned off
 * by setLoggedEpisode(bool) for performance reasons if it isn't needed.
 *
 * @see SemiMDPListener
 * @see HierarchicalSemiMarkovDecisionProcess
 * @see SemiMarkovDecisionProcess
**/
class Agent: public SemiMarkovDecisionProcess, public StateModifiersObject {
protected:
	bool bLogEpisode;

	StateModifierList _modifierList; // this must go before _currentEpisode
	std::unique_ptr<Episode> _currentEpisode;

public:
	//! Add a state Modifier to the StateCollections.
	virtual void addStateModifier(StateModifier* modifier);
	//! Remove a state Modifier from the StateCollections.
	virtual void removeStateModifier(StateModifier* modifier);

	StateModifierList* getModifierList() {return &_modifierList;}

	//! Outputs the action that is supposed to be executed in the current
	//! state according to the agent.
	Action* computeAction(StateCollection* currentState) {
		return getNextAction(currentState);
	}

    //! Add a primitive action to the agent's action list. The agent can only
    //! choose from these actions.
	virtual void addAction(PrimitiveAction *action);

	//! Sets whether the currentEpisode should be logged or not.
	virtual void setLogEpisode(bool bLogEpisode);

	//! Returns the currentEpisode Object (only valid if bLogEpisode = true).
	virtual Episode *getCurrentEpisode();

public:
	Agent& operator=(const Agent&) = delete;
	Agent(const Agent&) = delete;

	Agent(StateProperties* properties);
	virtual ~Agent();
};

} //namespace rlearner

#endif //Rlearner_cagent_H_
