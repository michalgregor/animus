#ifndef Rlearner_system_H_
#define Rlearner_system_H_

#include <Systematic/configure.h>
#include <Systematic/system.h>
#include <Systematic/math/numeric_cast.h>
#include <Systematic/math/RealType.h>
#include <Systematic/format/xformat.h>
#include <Systematic/format/ostream.h>
#include <Systematic/utils/rand.h>

namespace rlearner {
	//this is required so that shared_ptr and intrusive_ptr
	//are resolved unambiguously
	using systematic::shared_ptr;
	using systematic::intrusive_ptr;

	using namespace systematic;

	typedef unsigned int SizeType;
} //namespace rlearner

#endif //Rlearner_system_H_
