// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cagentlistener_H_
#define Rlearner_cagentlistener_H_

#include "parameters.h"
#include "system.h"

namespace rlearner {

class Action;
class State;
class StateCollection;
class RewardFunction;

/**
 * @class SemiMDPListener
 * Interface for all SemiMDP Listeners.
 *
 * This class is the base class of all Learning and Logging objects. If the
 * listeners get added to a SemiMarkovDecisionProcess the listener gets
 * informed about all Steps from the SMDP and whether to start a new Episode.
 *
 * There are 3 different kind of events which can be sent to the Listener:
 * - nextStep(StateCollection *oldState, Action *action,
 *   StateCollection *nextState). The Listener gets S-A-S Tuple of the current
 *   step. Usually this is the only data a learning algorithm needs (including
 *   the reward which can be calculated from the S-A-S tuple).
 * - intermediateStep(StateCollection *oldState, Action *action,
 *   StateCollection *nextState). Only needed for Hierarchical Reinforcement
 *   Learning. The Listener gets the same data as in nextStep, but instead of a
 *   RealType step, the S-A-S tuple comes from an intermediate step from an
 *   extended Action (see ExtendedAction for details about intermediate steps).
 *   The difference is made because intermediate steps has to be treated
 *   differently in some cases (e.g. ETraces).
 * - newEpisode(). Indicates that a new Episode has started.
 *
 * One or more of this event-functions should be implemented by all subclasses.
 * The class is also subclass of ParameterObject, so the parameters of the
 * listeners can be set through that interface.
 *
 * @see SemiMDPSender
 * @see SemiMarkovDecisionProcess
 * @see CParamterObject
 */
class SemiMDPListener: virtual public ParameterObject {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	bool enabled;

	//! Sends the Listener the S-A-S tuple from a new step.
	virtual void nextStep(StateCollection *, Action *, StateCollection *) {
	}

	//! Sends the Listener the S-A-S tuple from a intermediate step.
	virtual void intermediateStep(StateCollection *, Action *,
	        StateCollection *) {
	}

	//! Tells the Listener that a new Episode has started.
	virtual void newEpisode() {
	}

public:
	SemiMDPListener(): enabled(true) {}
	virtual ~SemiMDPListener() {}
};

/**
 * @class SemiMDPRewardListener
 * Represents SMDP Listener which also need a reward.
 *
 * The SemiMDPRewardListener maintains a reward function. With this reward
 * function, each time a nextStep or an intermediateStep event occurs
 * the listener can calculate the reward and then he calls the specific
 * abstract event function with the S-A-R(eward)-S tuple.
 */
class SemiMDPRewardListener: public SemiMDPListener {
protected:
	//! Reward function for reward calculation.
	RewardFunction *semiMDPRewardFunction;

public:
	//! Calculates the reward and then calls nextStep(...) with the reward
	//! as additional argument.
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *nextState);

	//! Virtual function, to be implemented by subclass.
	virtual void nextStep(StateCollection *, Action *, RealType,
	        StateCollection *) {
	}

	//! Calculates the reward and then calls intermediateStep(...) with
	//! the reward as additional argument.
	virtual void intermediateStep(StateCollection *oldState, Action *action,
	        StateCollection *nextState);

	//! Virtual function, to be implemented by subclass.
	virtual void intermediateStep(StateCollection *, Action *, RealType,
	        StateCollection *) {
	}

	void setRewardFunction(RewardFunction *semiMDPRewardFunction);
	RewardFunction *getRewardFunction();

public:
	SemiMDPRewardListener& operator=(const SemiMDPRewardListener&) = delete;
	SemiMDPRewardListener(const SemiMDPRewardListener&) = delete;

	/**
	 * @param semiMDPRewardFunction reward function for reward calculation
	 */
	SemiMDPRewardListener(RewardFunction *semiMDPRewardFunction);
	virtual ~SemiMDPRewardListener() {}
};

/**
 * @class AdaptiveParameterFromNStepsCalculator
 *
 * Adaptive Parameter Calculator which calculates the parameter's value from
 * the number of learning steps.
 *
 * The target value in this class is the number of learning steps, so its target
 * value is unbounded. The target value gets reset to 0 if a new learning trial
 * has started.
 *
 * This adaptive parameter has to be added to the agent's listener list in
 * order to count the number of steps. For more details see the super class.
 *
 * Parameters of AdaptiveParameterFromNStepsCalculator:
 * see AdaptiveParameterUnBoundedValuesCalculator
 */
class AdaptiveParameterFromNStepsCalculator: public AdaptiveParameterUnBoundedValuesCalculator,
        public SemiMDPListener {
protected:
	int targetValue;
	int nStepsPerUpdate;

public:
	virtual void nextStep(StateCollection *, Action *, StateCollection *);
	virtual void onParametersChanged() {
		AdaptiveParameterUnBoundedValuesCalculator::onParametersChanged();
	}

	virtual void resetCalculator();

public:
	AdaptiveParameterFromNStepsCalculator(Parameters *targetObject,
			std::string targetParameter, int nStepsPerUpdate, int functionKind,
			RealType param0, RealType paramScale, RealType targetOffset,
			RealType targetScale);

	virtual ~AdaptiveParameterFromNStepsCalculator();
};

/**
 * @class AdaptiveParameterFromNEpisodesCalculator
 *
 * Adaptive Parameter Calculator which calculates the parameter's value from
 * the number of learning episodes.
 *
 * The target value in this class is the number of learning episodes, so its
 * target value is unbounded. The target value gets reset to 0 if a new
 * learning trial has started. This adaptive parameter has to be added to the
 * agent's listener list in order to count the number of episodes. For more
 * details see the super class.
 *
 * Parameters of AdaptiveParameterFromNStepsCalculator:
 * see AdaptiveParameterUnBoundedValuesCalculator
 */
class AdaptiveParameterFromNEpisodesCalculator:
	public AdaptiveParameterUnBoundedValuesCalculator,
	public SemiMDPListener
{
protected:
	int targetValue;

public:
	virtual void newEpisode();
	virtual void onParametersChanged() {
		AdaptiveParameterUnBoundedValuesCalculator::onParametersChanged();
	}

	virtual void resetCalculator();

public:
	AdaptiveParameterFromNEpisodesCalculator(Parameters *targetObject,
			std::string targetParameter, int functionKind, RealType param0,
			RealType paramScale, RealType targetOffset, RealType targetScale);
	virtual ~AdaptiveParameterFromNEpisodesCalculator();
};

/**
 * @class AdaptiveParameterFromAverageRewardCalculator
 * Adaptive Parameter Calculator which calculates the parameter's value from
 * the current average reward.
 *
 * The target value in this class is the current average reward. The target
 * value gets reset the minimum expected reward if a new learning trial has
 * started. This adaptive parameter has to be added to the agent's listener
 * list in order to calculate the average reward. The average reward is
 * calculated dynamically with the form
 * averagereward_t+1 = averagereward_t * alpha + reward_t+1 * (1 - alpha).
 * Alpha can be set with the parameter "APRewardUpdateRate" and defines the
 * update rate of the average reward. Alpha should be chosen close to 0.99
 * to get good results. The average reward is not reset when a new episode
 * begins. For more details see the super class.
 *
 * Parameters of AdaptiveParameterFromNStepsCalculator:
 * - "APRewardUpdateRate": Update rate for the average reward.
 *
 * @see AdaptiveParameterBoundedValuesCalculator
 */
class AdaptiveParameterFromAverageRewardCalculator:
		public AdaptiveParameterBoundedValuesCalculator,
        public SemiMDPRewardListener
{
protected:
	RealType alpha;
	RealType targetValue;
	int nSteps;
	int nStepsPerUpdate;

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *newState);
	virtual void onParametersChanged();

	virtual void resetCalculator();

public:
	AdaptiveParameterFromAverageRewardCalculator(Parameters *targetObject,
	        std::string targetParameter, RewardFunction *reward,
	        int nStepsPerUpdate, int functionKind, RealType paramMin,
	        RealType paramMax, RealType targetMin, RealType targetMax, RealType alpha);
	virtual ~AdaptiveParameterFromAverageRewardCalculator();
};

} //namespace rlearner

SYS_EXPORT_CLASS(rlearner::SemiMDPListener)

#endif //Rlearner_cagentlistener_H_
