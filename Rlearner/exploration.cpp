// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "exploration.h"
#include "stateproperties.h"
#include "ril_debug.h"
#include "vfunction.h"
#include "agentcontroller.h"
#include "utility.h"
#include "action.h"
#include "state.h"
#include "statecollection.h"

namespace rlearner {

VisitStateCounter::VisitStateCounter(FeatureVFunction *visits, RealType decay) {
	this->visits = visits;
	addParameter("VisitDecayFactor", decay);
	addParameter("VisitDecayUpdateSteps", 1);

	weights = new RealType[visits->getNumWeights()];
	steps = 0;
}

VisitStateCounter::~VisitStateCounter() {
	delete weights;
}

void VisitStateCounter::nextStep(
    StateCollection *state, Action *, StateCollection *) {
	RealType decay = getParameter("VisitDecayFactor");
	int updateSteps = my_round(getParameter("VisitDecayUpdateSteps"));
	steps++;
	if((updateSteps > 0) && ((steps % updateSteps) == 0) && (decay < 1.0)) {
		doDecay(decay);
	}
	visits->updateValue(state, 1.0);
}

void VisitStateCounter::newEpisode() {
}

void VisitStateCounter::doDecay(RealType decay) {
	visits->getWeights(weights);

	for(int i = 0; i < visits->getNumWeights(); i++) {
		weights[i] *= decay;
	}

	visits->setWeights(weights);
}

VisitStateActionCounter::VisitStateActionCounter(
    FeatureQFunction *visits, RealType decay) {
	this->visits = visits;
	addParameter("VisitDecayFactor", decay);
	addParameter("VisitDecayUpdateSteps", 1);

	weights = new RealType[visits->getNumWeights()];
	steps = 0;
}

VisitStateActionCounter::~VisitStateActionCounter() {
	delete weights;
}

void VisitStateActionCounter::doDecay(RealType decay) {
	visits->getWeights(weights);

	for(int i = 0; i < visits->getNumWeights(); i++) {
		weights[i] *= decay;
	}

	visits->setWeights(weights);
}

void VisitStateActionCounter::nextStep(
    StateCollection *state, Action *action, StateCollection *) {
	RealType decay = getParameter("VisitDecayFactor");
	int updateSteps = my_round(getParameter("VisitDecayUpdateSteps"));
	steps++;
	if((updateSteps > 0) && ((steps % updateSteps) == 0) && (decay < 1.0)) {
		doDecay(decay);
	}
	visits->updateValue(state, action, 1.0);

}

void VisitStateActionCounter::newEpisode() {
}

VisitStateActionEstimator::VisitStateActionEstimator(
    FeatureVFunction *visits, FeatureQFunction *actionVisits, RealType decay) :
	VisitStateCounter(visits, decay) {
	this->actionVisits = actionVisits;
	addParameter("VisitsEstimatorLearningRate", 0.5);
}

VisitStateActionEstimator::~VisitStateActionEstimator() {

}

void VisitStateActionEstimator::doDecay(RealType decay) {
	actionVisits->getWeights(weights);

	for(int i = 0; i < visits->getNumWeights(); i++) {
		weights[i] *= decay;
	}

	visits->setWeights(weights);
}

void VisitStateActionEstimator::nextStep(
    StateCollection *state, Action *action, StateCollection *nextState) {
	RealType oldValue = actionVisits->getValue(state, action);
	RealType newVisits = visits->getValue(nextState);

	VisitStateCounter::nextStep(state, action, nextState);

	actionVisits->updateValue(state, action,
	    getParameter("VisitsEstimatorLearningRate") * (newVisits - oldValue));
}

void VisitStateActionEstimator::newEpisode() {
}

ExplorationQFunction::ExplorationQFunction(
    AbstractVFunction *stateVisitCounter, AbstractQFunction *actionVisitCounter) :
	AbstractQFunction(actionVisitCounter->getActions()) {
	this->actionVisitCounter = actionVisitCounter;
	this->stateVisitCounter = stateVisitCounter;
}

ExplorationQFunction::~ExplorationQFunction() {
}

void ExplorationQFunction::updateValue(
    StateCollection *, Action *, RealType, ActionData *) {
}

void ExplorationQFunction::setValue(
    StateCollection *, Action *, RealType, ActionData *) {
}

RealType ExplorationQFunction::getValue(
    StateCollection *state, Action *action, ActionData *) {
	RealType vValue = stateVisitCounter->getValue(state);
	RealType qValue = actionVisitCounter->getValue(state, action);

	if(fabs(qValue) < 0.000001) {
		qValue = 0.00001;
	}
	return vValue / qValue;
}

AbstractQETraces *ExplorationQFunction::getStandardETraces() {
	return nullptr;
}

QStochasticExplorationPolicy::QStochasticExplorationPolicy(
    ActionSet *actions, ActionDistribution *distribution,
    AbstractQFunction *qFunctoin, AbstractQFunction *explorationFunction,
    RealType alpha) :
	QStochasticPolicy(actions, distribution, qFunctoin) {
	this->explorationFunction = explorationFunction;
	addParameter("ExplorationFactor", alpha);
	addParameter("AttentionFactor", 0.5);

	explorationValues = new RealType[actions->size()];

}

QStochasticExplorationPolicy::~QStochasticExplorationPolicy() {
	delete explorationValues;
}

void QStochasticExplorationPolicy::getActionValues(
    StateCollection *state, ActionSet *availableActions, RealType *actionValues,
    ActionDataSet *) {
	for(unsigned int i = 0; i < availableActions->size(); actionValues[i++] =
	    0.0)
		;
	qfunction->getActionValues(state, availableActions, actionValues);

	for(unsigned int i = 0; i < availableActions->size();
	    explorationValues[i++] = 0.0)
		;
	explorationFunction->getActionValues(state, availableActions,
	    explorationValues);

	RealType alpha = getParameter("ExplorationFactor");
	RealType attentionFactor = getParameter("AttentionFactor");

	for(unsigned int i = 0; i < availableActions->size(); i++) {
		actionValues[i] = 2
		    * (attentionFactor * actionValues[i]
		        + (1 - attentionFactor) * alpha * explorationValues[i]);
	}

}

SelectiveExplorationCalculator::SelectiveExplorationCalculator(
    QStochasticExplorationPolicy *explorationFunction) {
	attention = 0.5;
	this->explorationPolicy = explorationFunction;
	addParameter("SelectiveAttentionSquashingFactor", 1.0);
}

SelectiveExplorationCalculator::~SelectiveExplorationCalculator() {}

void SelectiveExplorationCalculator::nextStep(
    StateCollection *state, Action *action, StateCollection *) {
	RealType alpha = getParameter("SelectiveAttentionSquashingFactor");

	RealType kappa = attention
	    * explorationPolicy->getQFunction()->getValue(state, action, nullptr)
	    / explorationPolicy->getQFunction()->getMaxValue(state,
	        explorationPolicy->getActions());
	kappa -= (1 - attention)
	    * explorationPolicy->getExplorationQFunction()->getValue(state, action);

	attention = 0.8 / (1 + exp(-alpha * kappa)) + 0.1;

	explorationPolicy->setParameter("SelectiveAttention", attention);
}

void SelectiveExplorationCalculator::newEpisode() {
	attention = 0.5;
	explorationPolicy->setParameter("SelectiveAttention", attention);
}

} //namespace rlearner
