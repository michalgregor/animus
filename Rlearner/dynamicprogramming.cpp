// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "ril_debug.h"
#include "dynamicprogramming.h"

#include "rewardfunction.h"
#include "theoreticalmodel.h"
#include "vfunction.h"
#include "qfunction.h"
#include "featurefunction.h"
#include "agentlistener.h"
#include "policies.h"
#include "vfunctionfromqfunction.h"
#include "ril_debug.h"
#include "state.h"
#include "statecollection.h"
#include "stateproperties.h"
#include "action.h"
#include "statemodifier.h"
#include "utility.h"
#include "system.h"

namespace rlearner {

RealType DynamicProgramming::getActionValue(
    AbstractFeatureStochasticModel *model, FeatureRewardFunction *rewardFunc,
    AbstractVFunction *vFunction, State *discState, Action *action,
    RealType gamma) {
	RealType V = 0;

	int feature = discState->getDiscreteState(0);

	assert(vFunction != nullptr && discState->getStateProperties()->isType(DISCRETESTATE));

	TransitionList *transList;

	TransitionList::iterator itTrans;

	transList = model->getForwardTransitions(
	    model->getActions()->getIndex(action), discState->getDiscreteState(0));

	for(itTrans = transList->begin(); itTrans != transList->end(); itTrans++) {
		discState->setDiscreteState(0, (*itTrans)->getEndState());
		if(action->isType(AF_MultistepAction)) {
			MultiStepAction *mAction = dynamic_cast<MultiStepAction *>(action);
			int oldDur = mAction->getDuration();
			SemiMDPTransition *trans = ((SemiMDPTransition *) (*itTrans));
			std::map<int, RealType>::iterator itDurations =
			    trans->getDurations()->begin();

			for(; itDurations != trans->getDurations()->end(); itDurations++) {
				mAction->getMultiStepActionData()->duration =
				    (*itDurations).first;
				V += (*itDurations).second * (*itTrans)->getPropability()
				    * (rewardFunc->getReward(feature, mAction,
				        (*itTrans)->getEndState())
				        + pow(gamma, (*itDurations).first)
				            * vFunction->getValue(discState));
			}
			mAction->getMultiStepActionData()->duration = oldDur;
		} else {
			V += (*itTrans)->getPropability()
			    * (rewardFunc->getReward(feature, action,
			        (*itTrans)->getEndState())
			        + gamma * vFunction->getValue(discState));
		}
		DebugPrint('d', "Transition: [%d->%d, %f] ",
		    (*itTrans)->getStartState(), (*itTrans)->getEndState(),
		    (*itTrans)->getPropability());

		DebugPrint('d', "Reward: %f, V: %f\n",
		    rewardFunc->getReward(feature, action, (*itTrans)->getEndState()),
		    V);
	}
	discState->setDiscreteState(0, feature);

	DebugPrint('d', "ActionValue (State: %d, Action: %d): %f\n", feature,
	    model->getActions()->getIndex(action), V);

	return V;
}

RealType DynamicProgramming::getBellmanValue(
    AbstractFeatureStochasticModel *model, FeatureRewardFunction *rewardFunc,
    AbstractVFunction *vFunction, State *feature, RealType gamma) {
	RealType maxV = 0, V = 0;

	assert(vFunction != nullptr);

	Action *action = nullptr;

	for(unsigned int naction = 0; naction < model->getNumActions(); naction++) {
		V = 0;

		action = model->getActions()->get(naction);

		V = getActionValue(model, rewardFunc, vFunction, feature, action,
		    gamma);

		if(action == 0 || maxV < V) {
			maxV = V;
		}
	}
	return maxV;
}

RealType DynamicProgramming::getBellmanError(
    AbstractFeatureStochasticModel *model, FeatureRewardFunction *rewardFunc,
    AbstractVFunction *vFunction, State *feature, RealType gamma) {
	return getBellmanValue(model, rewardFunc, vFunction, feature, gamma)
	    - vFunction->getValue(feature);
}

ValueIteration::ValueIteration(
    FeatureQFunction *qFunction, AbstractFeatureStochasticModel *model,
    FeatureRewardFunction *rewardModel) {
	addParameters(qFunction);
	addParameter("DiscountFactor", 0.95);

	this->qFunction = qFunction;

	this->actions = qFunction->getActions();

	this->vFunction = new OptimalVFunctionFromQFunction(qFunction,
	    qFunction->getFeatureCalculator());

	learnVFunction = false;

	init(model, rewardModel);
}

ValueIteration::ValueIteration(
    FeatureQFunction *qFunction, AbstractFeatureStochasticModel *model,
    FeatureRewardFunction *rewardModel, StochasticPolicy *stochPolicy) {
	addParameters(qFunction);
	addParameter("DiscountFactor", 0.95);

	this->qFunction = qFunction;

	this->actions = qFunction->getActions();

	this->vFunction = new VFunctionFromQFunction(qFunction, stochPolicy,
	    qFunction->getFeatureCalculator());

	learnVFunction = false;

	init(model, rewardModel);
}

ValueIteration::ValueIteration(
    FeatureVFunction *vFunction, AbstractFeatureStochasticModel *model,
    FeatureRewardFunction *rewardModel) {
	addParameters(vFunction);
	addParameter("DiscountFactor", 0.95);

	this->vFunction = vFunction;
	this->qFunctionFromVFunction = new QFunctionFromStochasticModel(vFunction,
	    model, rewardModel);
	this->vFunctionFromQFunction = new OptimalVFunctionFromQFunction(
	    qFunctionFromVFunction, vFunction->getStateProperties());

	this->actions = model->getActions();

	learnVFunction = true;

	init(model, rewardModel);
}

ValueIteration::ValueIteration(
    FeatureVFunction *vFunction, AbstractFeatureStochasticModel *model,
    FeatureRewardFunction *rewardModel, StochasticPolicy *stochPolicy) {
	addParameters(vFunction);
	addParameter("DiscountFactor", 0.95);

	this->vFunction = vFunction;
	this->qFunctionFromVFunction = new QFunctionFromStochasticModel(vFunction,
	    model, rewardModel);
	this->vFunctionFromQFunction = new VFunctionFromQFunction(
	    qFunctionFromVFunction, stochPolicy, vFunction->getStateProperties());

	this->actions = model->getActions();

	init(model, rewardModel);

	learnVFunction = true;
}

void ValueIteration::init(
    AbstractFeatureStochasticModel *model, FeatureRewardFunction *rewardModel) {
	addParameter("ValueIterationMaxListSize", model->getNumFeatures() / 4);

	this->model = model;
	this->rewardModel = rewardModel;

	this->discState = new State(new StateProperties(0, 1, DISCRETESTATE));
	this->discState->getStateProperties()->setDiscreteStateSize(0,
	    model->getNumFeatures());

	this->priorityList = new FeatureList(model->getNumFeatures() / 20, true);
}

ValueIteration::~ValueIteration() {
	delete priorityList;

	if(learnVFunction) {
		delete qFunctionFromVFunction;
		delete this->vFunctionFromQFunction;
	} else {
		delete vFunction;
	}

	delete discState->getStateProperties();
	delete discState;

}

void ValueIteration::updateFirstFeature() {
	auto rand = make_rand();

	int feature = 0;
	if(priorityList->size() > 0) {
		feature = (*priorityList->begin())->featureIndex;
	} else {
		feature = ((rand() * RAND_MAX)) % model->getNumFeatures();
	}
	updateFeature(feature);
}

RealType ValueIteration::getPriority(Transition *trans, RealType bellE) {
	if(trans->isType(SEMIMDPTRANSITION)) {
		SemiMDPTransition *semiTrans = (SemiMDPTransition *) trans;
		return semiTrans->getSemiMDPFaktor(getParameter("DiscountFactor"))
		    * trans->getPropability() * bellE;
	} else {
		return trans->getPropability() * bellE;
	}
}

void ValueIteration::updateFeature(int feature) {
	DebugPrint('d', "Updating State: %d\n", feature);

	discState->setDiscreteState(0, feature);
	RealType oldV = vFunction->getValue(discState);
	RealType bellV = 0;
	RealType bellE = 0;
	RealType actionValue;
	ActionSet::iterator it = actions->begin();

	if(!learnVFunction) {
		for(int i = 0; it != actions->end(); it++, i++) {
			actionValue = DynamicProgramming::getActionValue(model, rewardModel,
			    vFunction, discState, *it, getParameter("DiscountFactor"));
			((FeatureVFunction *) qFunction->getVFunction(*it))->setFeature(
			    feature, actionValue);
		}
	} else {
		((FeatureVFunction *) vFunction)->setFeature(feature,
		    vFunctionFromQFunction->getValue(discState));
	}

	bellV = vFunction->getValue(discState);

	bellE = bellV - oldV;
	DebugPrint('d', "OldV: %f, NewV %f, bellE: %f\n", oldV, bellV, bellE);

	priorityList->remove(feature);

	TransitionList *backTrans = nullptr;
	TransitionList::iterator transIt;

	if(fabs(bellE) >= 0.00001) {
		for(unsigned int action = 0; action < model->getNumActions();
		    action++) {
			backTrans = model->getBackwardTransitions(action, feature);
			for(transIt = backTrans->begin(); transIt != backTrans->end();
			    transIt++) {
				DebugPrint('d',
				    "Adding State %d with Priority: %lf (prob: %lf)\n",
				    (*transIt)->getStartState(),
				    (*transIt)->getPropability() * bellE,
				    (*transIt)->getPropability());
				addPriority((*transIt)->getStartState(),
				    getPriority((*transIt), fabs(bellE)));
			}
		}
	}
}

void ValueIteration::addPriorities(FeatureList *featList) {
	FeatureList::iterator it = featList->begin();
	for(; it != featList->end(); featList++) {
		addPriority((*it)->featureIndex, (*it)->factor);
	}
}

void ValueIteration::addPriority(int feature, RealType priority) {
	priorityList->set(feature,
	    priority + priorityList->getFeatureFactor(feature));

	while(priorityList->size() > getParameter("ValueIterationMaxListSize")) {
		priorityList->remove(*priorityList->rbegin());
	}
}

AbstractFeatureStochasticModel *ValueIteration::getTheoreticalModel() {
	return model;
}

AbstractVFunction *ValueIteration::getVFunction() {
	return vFunction;
}

int ValueIteration::getMaxListSize() {
	return my_round(getParameter("ValueIterationMaxListSize"));
}

void ValueIteration::setMaxListSize(int maxListSize) {
	setParameter("ValueIterationMaxListSize", maxListSize);
}

void ValueIteration::doUpdateSteps(int kSteps) {
	for(int k = 0; k < kSteps; k++) {
		updateFirstFeature();
	}
}

void ValueIteration::doUpdateStepsUntilEmptyList(int kSteps) {
	for(int k = 0; k < kSteps && priorityList->size() > 0; k++) {
		updateFirstFeature();
	}
}

FeatureQFunction *ValueIteration::getQFunction() {
	return qFunction;
}

StochasticPolicy *ValueIteration::getStochasticPolicy() {
	return stochPolicy;
}

void ValueIteration::doUpdateBackwardStates(int feature) {
	TransitionList *backTrans = nullptr;
	TransitionList::iterator transIt;

	for(unsigned int action = 0; action < model->getNumActions(); action++) {
		backTrans = model->getBackwardTransitions(action, feature);
		for(transIt = backTrans->begin(); transIt != backTrans->end();
		    transIt++) {
			updateFeature((*transIt)->getStartState());
		}
	}
}

} //namespace rlearner
