// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cmontecarlo_H_
#define Rlearner_cmontecarlo_H_

#include "batchlearning.h"
#include "evaluator.h"
#include "parameters.h"

#define MC_MSE 0
#define MC_MAE 1

namespace rlearner {

class Agent;
class Episode;
class RewardFunction;

class StateCollectionImpl;
class StateCollectionImpl;

class SemiMDPSender;
class RewardHistory;
class RewardEpisode;
class EpisodeHistory;

class MonteCarloError: public Evaluator, public ParameterObject {
protected:
	AgentSimulator* agentSimulator;
	Episode *episode;
	RewardFunction *rewardFunction;

	StateCollectionImpl *oldState;
	StateCollectionImpl *newState;

	int nEpisodes;
	int nStepsPerEpisode;

	SemiMDPSender *semiMDPSender;
	RewardHistory *rewardLogger;
	EpisodeHistory *episodeHistory;

protected:
	virtual RealType getValue(StateCollection *state, Action *action) = 0;

public:
	bool useRewardEpisode;
	int errorFunction;

public:
	void setEpisodeHistory(EpisodeHistory *episodeHistory,
	        RewardHistory *rewardLogger);

	RealType getMonteCarloError(Episode *episode, RewardEpisode *rewardEpisode);
	RealType getMeanMonteCarloError(EpisodeHistory *episodeHistory,
	        RewardHistory *rewardLogger);

	virtual RealType evaluate();
	void setSemiMDPSender(SemiMDPSender *sender);

public:
	MonteCarloError(
		AgentSimulator* agentSimulator, RewardFunction *reward,
		StateProperties *modelState, ActionSet *actions,
		const std::set<StateModifierList*>& modifierLists, int numEpisodes,
		int numSteps, RealType discountFactor
	);

	virtual ~MonteCarloError();
};

class MonteCarloVError: public MonteCarloError {
protected:
	AbstractVFunction *vFunction;

protected:
	virtual RealType getValue(StateCollection *state, Action *action);

public:
	MonteCarloVError(
		AbstractVFunction *vFunction, AgentSimulator* agentSimulator,
		RewardFunction *reward, StateProperties *modelState,
		ActionSet *actions, const std::set<StateModifierList*>& modifierLists,
		int numEpisodes, int numSteps, RealType discountFactor
	);

	virtual ~MonteCarloVError();
};

class MonteCarloQError: public MonteCarloError {
protected:
	AbstractQFunction *qFunction;

protected:
	virtual RealType getValue(StateCollection *state, Action *action);

public:
	MonteCarloQError(AbstractQFunction *vFunction, AgentSimulator* agentSimulator,
	        RewardFunction *reward, StateProperties *modelState,
	        ActionSet *actions, const std::set<StateModifierList*>& modifierLists,
	        int numEpisodes, int numSteps, RealType discountFactor);
	virtual ~MonteCarloQError();
};

class MonteCarloSupervisedLearner: public PolicyEvaluation {
protected:
	EpisodeHistory *episodeHistory;
	RewardHistory *rewardLogger;
	BatchDataGenerator *dataGenerator;

public:
	virtual void evaluatePolicy(int trials);

public:
	MonteCarloSupervisedLearner(
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		BatchDataGenerator *dataGenerator
	);

	virtual ~MonteCarloSupervisedLearner();
};

class MonteCarloVLearner: public MonteCarloSupervisedLearner {
public:
	MonteCarloVLearner(
		AbstractVFunction *vFunction,
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		SupervisedLearner *learner
	);

	virtual ~MonteCarloVLearner();
};

class MonteCarloCAQLearner: public MonteCarloSupervisedLearner {
public:
	MonteCarloCAQLearner(
		StateProperties *properties,
		ContinuousActionQFunction *qFunction,
		EpisodeHistory *episodeHistory,
		RewardHistory *rewardLogger,
		SupervisedLearner *learner
	);

	virtual ~MonteCarloCAQLearner();
};

class MonteCarloQLearner: public MonteCarloSupervisedLearner {
public:
	MonteCarloQLearner(QFunction *qFunction, EpisodeHistory *episodeHistory,
	        RewardHistory *rewardLogger, SupervisedQFunctionLearner *learner);
	virtual ~MonteCarloQLearner();
};

} //namespace rlearner

#endif //Rlearner_cmontecarlo_H_
