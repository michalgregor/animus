// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "action.h"
#include "state.h"
#include "ril_debug.h"

#include <cassert>
#include <cmath>

namespace rlearner {

void ActionData::serialize(IArchive& ar, const unsigned int) {
	ar & bIsChangeAble;
}

void ActionData::serialize(OArchive& ar, const unsigned int) {
	ar & bIsChangeAble;
}

ActionData::ActionData() {
	bIsChangeAble = true;
}

bool ActionData::isChangeAble() {
	return bIsChangeAble;
}

void ActionData::setIsChangeAble(bool changeAble) {
	this->bIsChangeAble = changeAble;
}

void Action::serialize(IArchive& ar, const unsigned int) {
	ar & type;
	ar & actionData;
}

void Action::serialize(OArchive& ar, const unsigned int) {
	ar & type;
	ar & actionData;
}

Action::Action() {
	this->type = 0;
	actionData = nullptr;
}

Action::Action(ActionData *actionData) {
	this->type = 0;
	this->actionData = actionData;
}

Action::~Action() {
	if(actionData) {
		delete actionData;
	}
}

int Action::getType() {
	return type;
}

void Action::addType(int Type) {
	type = type | Type;
}

bool Action::isType(int type) {
	return (this->type & type) > 0;
}

void Action::loadActionData(ActionData *actionData) {
	if(this->actionData) {
		this->actionData->setData(actionData);
	}
}

bool Action::equals(Action *action) {
	return this == action;
}

bool Action::isSameAction(Action *action, ActionData *) {
	return this == action;
}

ActionData* Action::getActionData() {
	return actionData;
}

ActionData *Action::getNewActionData() {
	return nullptr;
}

MultiStepActionData::MultiStepActionData() {
	finished = true;
	duration = 1;
}

void MultiStepActionData::load(std::istream& stream) {
	int buff;
	xscanf(stream, "[%d %d] ", duration, buff);
	finished = buff != 0;
}

void MultiStepActionData::save(std::ostream& stream) {
	fmt::fprintf(stream, "[%d %d] ", duration, finished);
}

void MultiStepActionData::setData(ActionData *actionData) {
	if(isChangeAble()) {
		MultiStepActionData *data =
		    dynamic_cast<MultiStepActionData *>(actionData);

		duration = data->duration;
		finished = data->finished;
	}
}

MultiStepAction::MultiStepAction() :
	Action(new MultiStepActionData()) {
	type = type | AF_MultistepAction;

	this->multiStepData = dynamic_cast<MultiStepActionData *>(actionData);
}

MultiStepAction::MultiStepAction(MultiStepActionData *actionData) :
	Action(actionData) {
	type = type | AF_MultistepAction;

	this->multiStepData = actionData;
}

ActionData *MultiStepAction::getNewActionData() {
	MultiStepActionData *data = new MultiStepActionData();
	return dynamic_cast<ActionData *>(data);
}

void PrimitiveAction::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Action>(*this);
}

void PrimitiveAction::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Action>(*this);
}

PrimitiveAction::PrimitiveAction(MultiStepActionData *actionData) :
	Action(actionData) {
	this->type = this->type | AF_PrimitiveAction;
}

PrimitiveAction::PrimitiveAction() {
	this->type = this->type | AF_PrimitiveAction;
}

PrimitiveAction::~PrimitiveAction() {

}

ExtendedAction::ExtendedAction() {
	type = type | AF_ExtendedAction;
	sendIntermediateSteps = true;
	nextHierarchyLevel = nullptr;
}

ExtendedAction::ExtendedAction(MultiStepActionData *actionData) :
	MultiStepAction(actionData) {
	type = type | AF_ExtendedAction;
	sendIntermediateSteps = true;
	nextHierarchyLevel = nullptr;
}

void ExtendedAction::getHierarchicalStack(HierarchicalStack *actionStack) {
	actionStack->push_back(this);

	if(nextHierarchyLevel != nullptr) {
		if(nextHierarchyLevel->isType(AF_ExtendedAction)) {
			ExtendedAction *exAction =
			    dynamic_cast<ExtendedAction *>(nextHierarchyLevel);
			exAction->getHierarchicalStack(actionStack);
		} else {
			actionStack->push_back(nextHierarchyLevel);
		}
	}
}

HierarchicalStack::HierarchicalStack() {

}

HierarchicalStack::~HierarchicalStack() {
}

void HierarchicalStack::clearAndDelete() {
	for(HierarchicalStack::iterator it = begin(); it != end(); it++) {
		delete (*it);
	}
	clear();
}

ActionSet::ActionSet() {
}

ActionSet::~ActionSet() {
}

int ActionSet::getIndex(Action *action) {
	int index = -1, i = 0;
	ActionSet::iterator it;

	assert(action != nullptr);

	for(it = begin(); it != end(); it++, i++) {
		if((*it)->equals(action)) {
			index = i;
			break;
		}
	}
	return index;
}

bool ActionSet::isMember(Action *action) {
	ActionSet::iterator it;

	assert(action != nullptr);

	for(it = begin(); it != end(); it++) {
		if((*it)->equals(action)) {
			return true;
		}
	}
	return false;
}

void ActionSet::add(ActionSet *actions) {
	if(actions != nullptr) {
		ActionSet::iterator it = actions->begin();

		for(; it != actions->end(); it++) {
			push_back(*it);
		}
	}
}

Action *ActionSet::get(unsigned int index) {
	assert(index < size());

	ActionSet::iterator it = begin();
	for(unsigned int i = 0; i < index; it++, i++)
		;
	return (*it);
}

void ActionSet::add(Action *action) {
	push_back(action);
}

void ActionSet::getAvailableActions(
    ActionSet *availableActions, StateCollection *stateCol) {
	availableActions->clear();
	for(ActionSet::iterator it = begin(); it != end(); it++) {
		if((*it)->isAvailable(stateCol)) {
			availableActions->add(*it);
		}
	}
}

ActionList::ActionList(ActionSet *actions) :
	ActionObject(actions) {
	actionIndices = new std::vector<int>();
	actionDatas = new std::map<int, ActionData *>();
}

ActionList::~ActionList() {
	clear();

	delete actionIndices;
	delete actionDatas;
}

void ActionList::addAction(Action *action) {
	unsigned int numAction = actionIndices->size();
	actionIndices->push_back(actions->getIndex(action));
	ActionData *data = action->getNewActionData();
	if(data != nullptr) {
		data->setData(action->getActionData());
		(*actionDatas)[numAction] = data;
	}
}

Action *ActionList::getAction(unsigned int num, ActionDataSet *l_data) {
	Action *action = actions->get((*actionIndices)[num]);

	ActionData *data = (*actionDatas)[num];
	if(data != nullptr && l_data != nullptr) {
		l_data->setActionData(action, data);
	}
	return action;
}

void ActionList::load(std::istream& stream) {
	int numActions = 0, bufAction = 0;
	xscanf(stream, "ActionList: %d Actions\n", numActions);
	for(int i = 0; i < numActions; i++) {
		xscanf(stream, "%d ", bufAction);
		actionIndices->push_back(bufAction);
		ActionData *data = actions->get(bufAction)->getNewActionData();
		if(data != nullptr) {
			data->load(stream);
			(*actionDatas)[i] = data;
		}
	}
	xscanf(stream, "\n");
}

void ActionList::save(std::ostream& stream) {
	fmt::fprintf(stream, "ActionList: %d Actions\n", getSize());
	for(unsigned int i = 0; i < getSize(); i++) {
		fmt::fprintf(stream, "%d ", (*actionIndices)[i]);
		if((*actionDatas)[i] != nullptr) {
			(*actionDatas)[i]->save(stream);
		}
	}
	fmt::fprintf(stream, "\n");
}

unsigned int ActionList::getSize() {
	return actionIndices->size();
}

unsigned int ActionList::getNumActions() {
	return actions->size();
}

void ActionList::clear() {
	actionIndices->clear();
	std::map<int, ActionData *>::iterator it = actionDatas->begin();

	for(; it != actionDatas->end(); it++) {
		delete ((*it).second);
	}
	actionDatas->clear();
}

ActionDataSet::ActionDataSet(ActionSet *actions) //: ActionObject(actions)
    {
	actionDatas = new std::map<Action *, ActionData *>();

	ActionSet::iterator it;
	for(it = actions->begin(); it != actions->end(); it++) {
		(*actionDatas)[(*it)] = (*it)->getNewActionData();
		if((*actionDatas)[(*it)] != nullptr) {
			(*actionDatas)[(*it)]->setData((*it)->getActionData());
		}
	}
}

ActionDataSet::ActionDataSet() //: ActionObject(actions)
{
	actionDatas = new std::map<Action *, ActionData *>();

}

ActionDataSet::~ActionDataSet() {
	std::map<Action *, ActionData *>::iterator it;

	for(it = actionDatas->begin(); it != actionDatas->end(); it++) {
		if((*it).second != nullptr) {
			delete (*it).second;
		}
	}
	delete actionDatas;
}

ActionData *ActionDataSet::getActionData(Action *action) {
	return (*actionDatas)[action];
}

void ActionDataSet::setActionData(Action *action, ActionData *actionData) {
	ActionData *data = (*actionDatas)[action];
	if(data) {
		data->setData(actionData);
	}
}

void ActionDataSet::addActionData(Action *action) {
	if((*actionDatas)[action] == nullptr) {
		(*actionDatas)[action] = action->getNewActionData();
	}
}

void ActionDataSet::removeActionData(Action *action) {
	if((*actionDatas)[action] != nullptr) {
		delete (*actionDatas)[action];
	}
	(*actionDatas)[action] = nullptr;
}

} //namespace rlearner
