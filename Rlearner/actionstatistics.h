// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cactionstatistics_H_
#define Rlearner_cactionstatistics_H_

#include "ril_debug.h"

namespace rlearner {

class Action;
class AgentController;

/**
 * @class ActionStatistics
 * Actionstatistics - for comparison of policies and agent controllers.
 *
 * Actionstatistics are used together with a @see CMultiController to
 * incorporate prior knowledge and / or compare different policies.
 */
class ActionStatistics {
public:
	//! Probability for this action.,
	RealType probability;
	//! How many actions promise the same reward.
	int equal;
	//! How many actions promise the more reward. if > 0 this action is
	//! not greedy.
	int superior;
	//! The policy / agent controller which proposed this action.
	AgentController *owner;
	//! The action itself.
	Action *action;

	//! Clones ActionStatistics,
	void copy(ActionStatistics *stat);
	//! Initializes ActionStatistics to zero.
	void reset();

public:
	ActionStatistics& operator=(const ActionStatistics&) = delete;
	ActionStatistics(const ActionStatistics&) = delete;

	ActionStatistics();
	virtual ~ActionStatistics() {}
};

/**
 * @class ActionStatisticsComparator
 *
 * Base class for all ActionStatisticComparators, used by
 * @see CMultiControllerGreedyPolicy.
 */
class ActionStatisticsComparator {
public:
	//! Compare 2 different ActionStatistics return values: -1: first < second;
	//! 0 first = second; 1 first > second.
	virtual int compare(ActionStatistics *first,
			ActionStatistics *second) = 0;

public:
	virtual ~ActionStatisticsComparator() {}
};

/**
 * @class GreedyASComparator
 *
 * Comparator for MulticontrollerGreedyPolicy prefers best action.
 */
class GreedyASComparator: public ActionStatisticsComparator {
public:
	virtual int compare(ActionStatistics *first, ActionStatistics *second);

public:
	virtual ~GreedyASComparator() {}
};

/**
 * Comparator for MulticontrollerGreedyPolicy prefers epsilon-greedy action
 * over best action.
 */
class PEGreedyASComparator: public ActionStatisticsComparator {
public:
	virtual int compare(ActionStatistics *first, ActionStatistics *second);

public:
	virtual ~PEGreedyASComparator() {}
};

/**
 * @class POASComparator
 *
 * Comparator for MulticontrollerGreedyPolicy prefers actions of
 * a specific owner.
 */
class POASComparator: public ActionStatisticsComparator {
private:
	AgentController* owner;

public:
	void setOwner(AgentController* owner_) {
		owner = owner_;
	}

	AgentController *getOwner() {
		return this->owner;
	}

	virtual int compare(ActionStatistics *first, ActionStatistics *second);

public:
	POASComparator& operator=(const POASComparator&) = delete;
	POASComparator(const POASComparator&) = delete;

	POASComparator(AgentController* owner_): owner(owner_) {}
	virtual ~POASComparator() {}
};

} //namespace rlearner

#endif //Rlearner_cactionstatistics_H_
