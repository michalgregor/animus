// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cmodeltree_H_
#define Rlearner_cmodeltree_H__

#include "trees.h"
#include "localregression.h"

namespace rlearner {

class LinearRegressionDataFactory: public TreeDataFactory<Mapping<RealType> *> {
protected:
	int tresh1;
	int tresh2;
	int tresh3;

	DataSet *input;
	DataSet1D *output;

	RealType lambda;

public:
	virtual Mapping<RealType> *createTreeData(DataSubset *dataSubset,
	        int numLeaves);
	virtual void deleteData(Mapping<RealType> *linReg);

public:
	LinearRegressionDataFactory(
		DataSet *input, DataSet1D *output, int tresh1,
	    int tresh2, int tresh3, RealType lambda
	);

	virtual ~LinearRegressionDataFactory();
};

class ModelTree: public Mapping<RealType> {
protected:
	Tree<Mapping<RealType> *> *tree;
	bool deleteTree;

protected:
	virtual RealType doGetOutputValue(ColumnVector *input);

public:
	Tree<Mapping<RealType> *> *getTree();

public:
	ModelTree(
		DataSet *inputData, SplittingConditionFactory *splittingFactory,
	    TreeDataFactory<Mapping<RealType> *> *l_dataFactory
	);

	ModelTree(Tree<Mapping<RealType> *> *tree);
	virtual ~ModelTree();
};

class ExtraModelTree: public ModelTree {
public:
	ExtraModelTree(
		DataSet *inputData, DataSet1D *outputData,
		TreeDataFactory<Mapping<RealType> *> *dataFactory,
		unsigned int K, unsigned int n_min, RealType outTresh
	);

	virtual ~ExtraModelTree();
};

class ExtraLinearRegressionModelTree: public ExtraModelTree {
public:
	ExtraLinearRegressionModelTree(DataSet *inputData, DataSet1D *outputData,
	        unsigned int K, unsigned int n_min, RealType outTresh, int tresh1,
	        int tresh2, int tresh3, RealType lambda);

	virtual ~ExtraLinearRegressionModelTree();
};

} //namespace rlearner

#endif //Rlearner_cmodeltree_H_
