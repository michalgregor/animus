// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>

#include <sys/time.h>

#include "ril_debug.h"
#include "episodehistory.h"
#include "statecollection.h"
#include "action.h"
#include "episode.h"
#include "system.h"

namespace rlearner {

EpisodeHistory::EpisodeHistory(StateProperties *prop, ActionSet *actions) :
	StateModifiersObject(prop), StepHistory(properties, actions) {
	stepToEpisodeMap = nullptr;
	episodeOffsetMap = nullptr;

	StateModifiersObject::addModifierList(&_modifierList);
}

EpisodeHistory::~EpisodeHistory() {
	StateModifiersObject::removeModifierList(&_modifierList);

	if(stepToEpisodeMap) {
		delete stepToEpisodeMap;
		delete episodeOffsetMap;
	}
}

void EpisodeHistory::addStateModifier(StateModifier* modifier) {
	_modifierList.addStateModifier(modifier);
}

void EpisodeHistory::removeStateModifier(StateModifier* modifier) {
	_modifierList.removeStateModifier(modifier);
}

void EpisodeHistory::createStepToEpisodeMap() {
	if(stepToEpisodeMap == nullptr) {
		stepToEpisodeMap = new std::map<int, Episode *>;
		episodeOffsetMap = new std::map<Episode *, int>;
	}

	stepToEpisodeMap->clear();

	int step = 0;
	for(int i = 0; i < getNumEpisodes(); i++) {
		Episode *episode = getEpisode(i);

		(*episodeOffsetMap)[episode] = step;

		for(int j = 0; j < episode->getNumSteps(); j++) {
			(*stepToEpisodeMap)[step] = episode;
			step++;
		}
	}
}

int EpisodeHistory::getNumSteps() {
	int sum = 0;
	int i = 0;

	for(i = 0; i < getNumEpisodes(); i++) {
		sum += getEpisode(i)->getNumSteps();
	}

	return sum;
}

void EpisodeHistory::getStep(int index, Step *step) {
	assert(index < getNumSteps());

	int lnum = index;
	int i = 0;
	Episode *episode;

	if(stepToEpisodeMap) {
		episode = (*stepToEpisodeMap)[index];
		lnum = lnum - (*episodeOffsetMap)[episode];
	} else {
		while(lnum >= (episode = getEpisode(i))->getNumSteps()) {
			lnum -= episode->getNumSteps();
			i++;
		}
	}

	episode->getStep(lnum, step);
}

EpisodeHistorySubset::EpisodeHistorySubset(
    EpisodeHistory *l_episodes, std::vector<int> *l_indices
):
	StateModifiersObject(
		l_episodes->getStateProperties(), l_episodes->getModifierLists()
	),
	EpisodeHistory(
		l_episodes->getStateProperties(), l_episodes->getActions()
	),
	episodes()
{
	episodes = l_episodes;
	indices = l_indices;
}

EpisodeHistorySubset::~EpisodeHistorySubset() {}

int EpisodeHistorySubset::getNumEpisodes() {
	return indices->size();
}

Episode* EpisodeHistorySubset::getEpisode(int index) {
	return episodes->getEpisode((*indices)[index]);
}

StoredEpisodeModel::StoredEpisodeModel(EpisodeHistory *history) :
	    EnvironmentModel(history->getStateProperties()),
	    AgentController(history->getActions()) {
	this->history = history;

	this->numEpisode = -1;
	this->numStep = 0;

	doResetModel();
}

StoredEpisodeModel::~StoredEpisodeModel() {
}

void StoredEpisodeModel::doNextState(PrimitiveAction *) {
	numStep++;

	if(numStep >= currentEpisode->getNumSteps()) {
		reset = true;
	}
}

void StoredEpisodeModel::doResetModel() {
	if(reset == true) {
		numEpisode++;
		if(numEpisode >= history->getNumEpisodes()) {
			numEpisode = 0;
		}
	}
	currentEpisode = history->getEpisode(numEpisode);
	numStep = 0;
	if(currentEpisode->getNumSteps() == 0) {
		doResetModel();
	}
}

EpisodeHistory* StoredEpisodeModel::getEpisodeHistory() {
	return history;
}

void StoredEpisodeModel::setEpisodeHistory(EpisodeHistory *hist) {
	history = hist;
}

void StoredEpisodeModel::getState(State *state) {
	currentEpisode->getState(numStep, state);
}

void StoredEpisodeModel::getState(StateCollectionImpl *stateCollection) {
	stateCollection->newModelState();
	currentEpisode->getStateCollection(numStep, stateCollection);
}

Action* StoredEpisodeModel::getNextAction(StateCollection*, ActionDataSet*) {
	if(numStep >= currentEpisode->getNumSteps()) {
		return nullptr;
	} else {
		return currentEpisode->getAction(numStep);
	}
}

CBatchEpisodeUpdate::CBatchEpisodeUpdate(
    SemiMDPListener *listener, EpisodeHistory *logger, int numEpisodes,
    const std::set<StateModifierList*>& modifierLists
):
	listener(listener),
	logger(logger),
	numEpisodes(numEpisodes),
	episodeIndex(0),
	dataSet(new ActionDataSet(logger->getActions())),
	step(new Step(logger->getStateProperties(), modifierLists,
	    logger->getActions()))
{}

CBatchEpisodeUpdate::~CBatchEpisodeUpdate() {
	delete dataSet;
	delete step;
}

void CBatchEpisodeUpdate::newEpisode() {
	simulateNRandomEpisodes(numEpisodes, listener);
}

void CBatchEpisodeUpdate::simulateEpisode(
    int index, SemiMDPListener *listener) {
	assert(index < logger->getNumEpisodes());

	Episode *episode = logger->getEpisode(index);

	listener->newEpisode();

	int i = 0;

	ActionSet::iterator it = logger->getActions()->begin();
	for(; it != logger->getActions()->end(); it++) {
		dataSet->setActionData(*it, (*it)->getActionData());
	}

	for(i = 0; i < episode->getNumSteps(); i++) {
		episode->getStep(i, step);
		step->action->loadActionData(
		    step->actionData->getActionData(step->action));

		listener->nextStep(step->oldState, step->action, step->newState);
	}
	it = logger->getActions()->begin();
	for(; it != logger->getActions()->end(); it++) {
		(*it)->loadActionData(dataSet->getActionData(*it));
	}

	listener->newEpisode();
}

void CBatchEpisodeUpdate::simulateAllEpisodes(SemiMDPListener *listener) {
	for(int i = 0; i < logger->getNumEpisodes(); i++) {
		simulateEpisode(i, listener);
	}
}

void CBatchEpisodeUpdate::simulateNRandomEpisodes(
    int numEpisodes, SemiMDPListener *listener) {
	auto rand = make_rand();
	std::list<int> *episodeIndex = new std::list<int>();

	for(int i = 0; i < logger->getNumEpisodes(); i++) {
		episodeIndex->push_back(i);
	}

	for(int i = 0; i < numEpisodes && episodeIndex->size() > 0; i++) {
		int nRand = rand() % episodeIndex->size();
		std::list<int>::iterator it = episodeIndex->begin();
		for(int j = 0; j < nRand && it != episodeIndex->end(); j++, it++)
			;
		nRand = *it;
		episodeIndex->erase(it);
		simulateEpisode(nRand, listener);
	}

	delete episodeIndex;
}

} //namespace rlearner
