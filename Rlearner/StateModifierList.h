#ifndef RLEARNER_STATEMODIFIERLIST_H_
#define RLEARNER_STATEMODIFIERLIST_H_

#include <set>
#include "system.h"

namespace rlearner {

class StateModifier;
class ModifierListReporter;

/**
 * A class that represents a list of state modifiers and has the ability to
 * notify other objects of changes.
 */
class StateModifierList {
private:
	typedef std::set<StateModifier*> base_list;

public:
	typedef base_list::value_type value_type;
	typedef base_list::reference reference;
	typedef base_list::const_reference const_reference;
	typedef base_list::pointer pointer;
	typedef base_list::const_pointer const_pointer;
	typedef base_list::iterator iterator;
	typedef base_list::const_iterator const_iterator;
	typedef base_list::reverse_iterator reverse_iterator;
	typedef base_list::const_reverse_iterator const_reverse_iterator;
	typedef base_list::difference_type difference_type;
	typedef base_list::size_type size_type;

public:
    iterator begin() noexcept {return _modifiers.begin();}
    const_iterator begin() const noexcept {return _modifiers.begin();}
    iterator end() noexcept {return _modifiers.end();}
    const_iterator end() const noexcept {return _modifiers.end();}

    reverse_iterator rbegin() noexcept {return _modifiers.rbegin();}
    const_reverse_iterator rbegin() const noexcept {return _modifiers.rbegin();}
    reverse_iterator rend() noexcept {return _modifiers.rend();}
    const_reverse_iterator rend() const noexcept {return _modifiers.rend();}

    const_iterator cbegin() const noexcept {return _modifiers.cbegin();}
    const_iterator cend() const noexcept {return _modifiers.cend();}
    const_reverse_iterator crbegin() const noexcept {return _modifiers.crbegin();}
    const_reverse_iterator crend() const noexcept {return _modifiers.crend();}

private:
	//! The underlying list of state modifiers.
	base_list _modifiers;
	//! The list of reporters used to report changes to the modifier list.
	std::set<ModifierListReporter*> _reporters;

public:
	void addStateModifier(StateModifier* modifier);
	void removeStateModifier(StateModifier* modifier);

	void addReporter(ModifierListReporter* reporter);
	void removeReporter(ModifierListReporter* reporter);

	void clear();
};

/**
 * A class used to report changes to the StateModifierList.
 */
class ModifierListReporter {
public:
	virtual void modifiersAdded(StateModifierList::iterator first,
		StateModifierList::iterator last) = 0;

	virtual void modifiersRemoved(StateModifierList::iterator first,
		StateModifierList::iterator last) = 0;

public:
	virtual ~ModifierListReporter() = 0;
};

} // namespace rlearner

#endif /* RLEARNER_STATEMODIFIERLIST_H_ */
