// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_clearndataobject_H_
#define Rlearner_clearndataobject_H_

#include <cstdio>
#include <string>

#include "parameters.h"
#include "ril_debug.h"

namespace rlearner {

/**
 * @class LearnDataObject
 * Interface for all objects that learn, like V-Functions or Q-Functions.
 *
 * This is just a small interface, providing general functions for all objects
 * which maintains changeable, learned data. The interface consists of
 * following functions, which has to be implemented by every subclass:
 * - saveData(std::ostream& stream)
 * - loadData(std::istream& stream)
 * - resetData()
 **/
class LearnDataObject: virtual public ParameterObject {
protected:
	std::string _resetFile;
	
public:
	virtual void saveData(std::ostream& stream) = 0;
	virtual void loadData(std::istream& stream) = 0;
	virtual void resetData() = 0;
	void resetLearnData();
	
	void setResetFile(const std::string& restFile);
	void saveDataToFile(const std::string& filename);
	void loadDataFromFile(const std::string& filename);

	virtual void copy(LearnDataObject *) {};

public:
	LearnDataObject& operator=(const LearnDataObject&) = delete;
	LearnDataObject(const LearnDataObject&) = delete;

	LearnDataObject();
	virtual ~LearnDataObject() {};
};

} //namespace rlearner

#endif //Rlearner_clearndataobject_H_
