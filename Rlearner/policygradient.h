// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cpolicygradient_H_
#define Rlearner_cpolicygradient_H_

#include "agentlistener.h"
#include "supervisedlearner.h"
#include "AgentSimulator.h"

namespace rlearner {

class AgentController;
class PolicyEvaluator;
class FeatureList;
class ReinforcementBaseLineCalculator;

class RewardFunction;
class Agent;
class TransitionFunctionEnvironment;
class ContinuousActionGradientPolicy;
class GradientUpdateFunction;
class StochasticPolicy;

class PolicyGradientCalculator: public GradientCalculator {
protected:
	AgentController* policy;
	PolicyEvaluator* evaluator;

public:
	virtual void getGradient(FeatureList *gradient) = 0;
	virtual RealType getFunctionValue();

public:
	PolicyGradientCalculator(AgentController *policy, PolicyEvaluator *evaluator);
	virtual ~PolicyGradientCalculator() {}
};

class GPOMDPGradientCalculator:
	public PolicyGradientCalculator,
	public SemiMDPRewardListener
{
protected:
	FeatureList *localGradient;
	FeatureList *localZTrace;
	FeatureList *globalGradient;

	AgentSimulator* agentSimulator;
	ReinforcementBaseLineCalculator *baseLine;
	StochasticPolicy *stochPolicy;

public:
	virtual void nextStep(StateCollection *oldState, Action *action,
	        RealType reward, StateCollection *newState);
	virtual void newEpisode();

	virtual void getGradient(FeatureList *gradient);

	virtual FeatureList* getGlobalGradient();
	virtual void setGlobalGradient(FeatureList *globalGradient);

public:
	GPOMDPGradientCalculator(
		RewardFunction *reward, StochasticPolicy *policy,
		PolicyEvaluator *evaluator, AgentSimulator* agentSimulator,
		ReinforcementBaseLineCalculator *baseLine,
		int TSteps, int nEpisodes, RealType beta
	);

	virtual ~GPOMDPGradientCalculator();
};

class ContinuousActionGradientPolicy;

class NumericPolicyGradientCalculator: public PolicyGradientCalculator {
protected:
	FeatureList *gradientFeatures;
	RealType *weights;

	RewardFunction *rewardFunction;
	Agent *agent;
	TransitionFunctionEnvironment *dynModel;
	ContinuousActionGradientPolicy *gradientPolicy;

public:
	virtual void getGradient(FeatureList *gradientFeatures);

public:
	NumericPolicyGradientCalculator(
		Agent *agent,
		ContinuousActionGradientPolicy *policy,
		TransitionFunctionEnvironment *dynModel,
		RewardFunction *reward,
		RealType stepSize,
		PolicyEvaluator *evaluator
	);

	virtual ~NumericPolicyGradientCalculator();
};

class RandomPolicyGradientCalculator: public PolicyGradientCalculator {
protected:
	RealType *stepSizes;
	RealType *minWeights;
	RealType *nullWeights;
	RealType *plusWeights;

	int *numMinWeights;
	int *numMaxWeights;
	int *numNullWeights;

	ContinuousActionGradientPolicy *gradientPolicy;

public:
	virtual void getGradient(FeatureList *gradient);
	virtual void setStepSize(int index, RealType stepSize);

	virtual void resetGradientCalculator() {}

public:
	RandomPolicyGradientCalculator(ContinuousActionGradientPolicy *policy,
	        PolicyEvaluator *evaluator, int numEvaluations, RealType stepSize);
	virtual ~RandomPolicyGradientCalculator();
};

class RandomMaxPolicyGradientCalculator: public PolicyGradientCalculator {
protected:
	RealType *stepSizes;
	RealType *workStepSizes;

	ContinuousActionGradientPolicy *gradientPolicy;

public:
	virtual void getGradient(FeatureList *gradient);
	virtual void setStepSize(int index, RealType stepSize);
	virtual void resetGradientCalculator();

public:
	RandomMaxPolicyGradientCalculator(
		ContinuousActionGradientPolicy *policy,
		PolicyEvaluator *evaluator,
		int numEvaluations,
		RealType stepSize
	);

	virtual ~RandomMaxPolicyGradientCalculator();
};

class GSearchPolicyGradientUpdater: public GradientFunctionUpdater {
protected:
	PolicyGradientCalculator *gradientCalculator;

	RealType *startParameters;
	RealType *workParameters;
	RealType lastStepSize;

protected:
	void setWorkingParamters(
		FeatureList *gradient,
		RealType stepSize,
		RealType *startParameters,
		RealType *workParameters
	);

public:
	virtual void updateWeights(FeatureList *gradient);

	GSearchPolicyGradientUpdater(
		GradientUpdateFunction *updateFunction,
		PolicyGradientCalculator *gradientCalculator,
		RealType s0,
		RealType epsilon
	);

	virtual ~GSearchPolicyGradientUpdater();
};

class PolicyGradientWeightDecayListener: public SemiMDPListener {
protected:
	GradientUpdateFunction *updateFunction;
	RealType *parameters;

public:
	virtual void newEpisode();

public:
	PolicyGradientWeightDecayListener(
		GradientUpdateFunction *updateFunction,
		RealType weightdecay
	);

	virtual ~PolicyGradientWeightDecayListener();
};

} //namespace rlearner

#endif //Rlearner_cpolicygradient_H_
