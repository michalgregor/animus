// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "treevfunction.h"
#include "state.h"
#include "statecollection.h"

namespace rlearner {

RegressionTreeFunction::RegressionTreeFunction(
    Mapping<RealType> *l_tree, int l_numDim) {
	tree = l_tree;
	numDim = l_numDim;
}

void RegressionTreeFunction::setTree(Mapping<RealType> *l_tree) {
	tree = l_tree;
//	assert(tree->getNumDimensions() == vector->rows());
}

Mapping<RealType> *RegressionTreeFunction::getTree() {
	return tree;
}

int RegressionTreeFunction::getNumDimensions() {
	return numDim;
}

RegressionTreeVFunction::RegressionTreeVFunction(
    StateProperties *properties, Mapping<RealType> *l_tree
):
	AbstractVFunction(properties),
	RegressionTreeFunction(l_tree, properties->getNumContinuousStates())
{
//	assert(tree->getNumDimensions() == properties->getNumContinuousStates());
}

RealType RegressionTreeVFunction::getValue(State *state) {
	if(tree == nullptr) {
		return 0;
	}
	RealType value = tree->getOutputValue(state);

	return value;
}

void RegressionTreeVFunction::resetData() {
	if(tree != nullptr) {
		delete tree;
	}
	tree = nullptr;
}

void RegressionTreeVFunction::saveData(std::ostream& stream) {
	if(tree) {
		tree->save(stream);
	}
}

void RegressionTreeVFunction::getInputData(
    StateCollection *stateCol, Action *, ColumnVector *buffVector
) {
	State *state = stateCol->getState(properties);
	*buffVector = *state;
}

RegressionTreeQFunction::RegressionTreeQFunction(
    ContinuousAction *action, StateProperties *properties,
    Mapping<RealType> *l_tree
):
	ContinuousActionQFunction(action),
	StateObject(properties),
	RegressionTreeFunction(l_tree,
		properties->getNumContinuousStates() + action->getNumDimensions())
{
	buffVector = new ColumnVector(
	    properties->getNumContinuousStates() + action->getNumDimensions());

//	assert(tree->getNumDimensions() == buffVector->rows());
}

RegressionTreeQFunction::~RegressionTreeQFunction() {
	delete buffVector;
}

RealType RegressionTreeQFunction::getCAValue(
    StateCollection *stateCol, ContinuousActionData *data) {
	if(tree == nullptr) {
		return 0;
	}

	State *state = stateCol->getState(properties);
	for(unsigned int i = 0; i < properties->getNumContinuousStates(); i++) {
		buffVector->operator[](i) = state->getContinuousState(i);
	}
	int dim = properties->getNumContinuousStates();
	for(int i = 0; i < data->rows(); i++) {
		buffVector->operator[](i + dim) = data->operator[](i);
	}

	return tree->getOutputValue(buffVector);
}

void RegressionTreeQFunction::getInputData(
    StateCollection *stateCol, Action *action, ColumnVector *buffVector) {
	ContinuousActionData *data =
	    dynamic_cast<ContinuousAction *>(action)->getContinuousActionData();

	State *state = stateCol->getState(properties);
	for(unsigned int i = 0; i < properties->getNumContinuousStates(); i++) {
		buffVector->operator[](i) = state->getContinuousState(i);
	}
	int dim = properties->getNumContinuousStates();
	for(int i = 0; i < data->rows(); i++) {
		buffVector->operator[](i + dim) = data->operator[](i);
	}
}

void RegressionTreeQFunction::resetData() {
	if(tree != nullptr) {
		delete tree;
	}

	tree = nullptr;
}

FeatureVRegressionTreeFunction::FeatureVRegressionTreeFunction(
    RegressionForest *regForest, FeatureCalculator *featCalc) :
	FeatureVFunction(featCalc) {
	setForest(regForest, featCalc);
}

FeatureVRegressionTreeFunction::FeatureVRegressionTreeFunction(int numFeatures) :
	FeatureVFunction(numFeatures) {}

void FeatureVRegressionTreeFunction::setForest(
    RegressionForest *regForest, FeatureCalculator *featCalc) {

	if(numFeatures < featCalc->getNumFeatures()) {
		printf(
		    "Setting Forest with too many features: V-Function: %d Forest: %d\n",
		    numFeatures, featCalc->getNumFeatures());
		assert(numFeatures >= featCalc->getNumFeatures());
	}
	properties = featCalc;

	int feature = 0;
	resetData();

	for(int i = 0; i < regForest->getNumTrees(); i++) {
		Tree<RealType> *regTree = regForest->getTree(i);
		for(int j = 0; j < regTree->getNumLeaves(); j++) {
			setFeature(feature, regTree->getLeaf(j)->getTreeData());
			feature++;
		}
	}
}

RealType FeatureVRegressionTreeFunction::getValue(State *state) {
	if(properties == nullptr) {
		return 0.0;
	} else {
		return FeatureVFunction::getValue(state);
	}
}

void FeatureVRegressionTreeFunction::copy(LearnDataObject *vFunction) {
	FeatureVRegressionTreeFunction *regVFunction =
	    dynamic_cast<FeatureVRegressionTreeFunction *>(vFunction);

	FeatureVFunction::copy(vFunction);

	regVFunction->setFeatureCalculator(
	    dynamic_cast<FeatureCalculator *>(properties));
}

} //namespace rlearner
