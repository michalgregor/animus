#include "Task_Charging.h"
#include "ActionEncoder_ManuAction.h"

namespace rlearner {

const unsigned int CAPACITY = 5;
const unsigned int CHARGE_STEP = 5;

namespace rlearner_charging {

/**
 * Creates a new StateProperties object (allocated using new) and returns
 * a pointer to it.
 */
StateProperties* ManuState::newProperties() {
	StateProperties* prop = new StateProperties(0, 4);

	prop->setDiscreteStateSize(0, 5 + 1);
	prop->setDiscreteStateSize(1, 3);
	prop->setDiscreteStateSize(3, 100 + 1);

	return prop;
}

void ManuState::initState(State* state) {
	state->setDiscreteState(0, 0);
	state->setDiscreteState(1, static_cast<int>(Location::WAREHOUSE));
	state->setDiscreteState(2, 0);
	state->setDiscreteState(3, 100);
}

class ChargingReward: public RewardFunction {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version) {
		ar & boost::serialization::base_object<RewardFunction>(*this);
	}

	void serialize(OArchive& ar, const unsigned int version) {
		ar & boost::serialization::base_object<RewardFunction>(*this);
	}

public:
	//! Virtual function for calculating the reward.
	virtual RealType getReward(
	    StateCollection *oldState, Action *action, StateCollection *newState) {
		ManuState state(newState->getState());
		return std::max((state.getCharge() - 50) / 50, 0);
	}

public:
	virtual ~ChargingReward() = default;
};

void ManuAction::serialize(IArchive& ar, const unsigned int /*version*/) {
	ar & boost::serialization::base_object<PrimitiveAction>(*this);
	ar & _actionType;
}

void ManuAction::serialize(OArchive& ar, const unsigned int /*version*/) {
	ar & boost::serialization::base_object<PrimitiveAction>(*this);
	ar & _actionType;
}

} //namespace rlearner_charging

/**
 * Returns the reward function associated with the task. If there is
 * a main reward function, this should be at index 0.
 *
 * \exception TracedError_OutOfRange Throws when index >= numRewardFunctions().
 */
RewardFunction* Task_Charging::getRewardFunction(unsigned int index) {
	if(index == 0) return this;
	else if(index == 1) return _charge.get();
	else throw TracedError_OutOfRange("Reward function index out of range.");
}

/**
 * Returns the number of reward functions associated with the task.
 */
unsigned int Task_Charging::numRewardFunctions() const {
	return 2;
}

TransitionFunction* Task_Charging::getTransitionFunction() {
	return this;
}

void Task_Charging::getResetState(State *resetState) {
	ManuState::initState(resetState);
}

void Task_Charging::transitionFunction(
    State *oldstate, Action *action_, State *newState,
    ActionData* UNUSED(data)) {
	newState->setState(oldstate);

	ManuState oldManuState(oldstate);
	ManuState newManuState(newState);

	ManuAction* action = dynamic_cast<ManuAction*>(action_);

//	std::cerr << "begin" << std::endl;
//	newState->setDiscreteState(1, L_WAREHOUSE);
//	std::cerr << "end" << std::endl;

	if(oldManuState.getCharge()) {

		newManuState.setCharge(oldManuState.getCharge() - CHARGE_STEP);

		switch(action->getActionType()) {
		case ActionType::MOVE_TO_CHARGER:
			newManuState.setLocation(Location::CHARGER);
		break;
		case ActionType::CHARGE:
			if(oldManuState.getLocation() == Location::CHARGER) newManuState.setCharge(
			    std::min(oldManuState.getCharge() + 10, 100));
		break;
		case ActionType::MOVE_TO_WAREHOUSE:
			newManuState.setLocation(Location::WAREHOUSE);
		break;
		case ActionType::MOVE_TO_STORE:
			newManuState.setLocation(Location::STORE);
		break;
		case ActionType::GET_MATERIAL: {
			if(oldManuState.getLocation() == Location::STORE) {
				unsigned int material = oldManuState.getNumMaterial();
				if(material < CAPACITY) newManuState.setNumMaterial(
				    material + 1);
			}
		}
		break;
		case ActionType::CREATE_PRODUCT: {
			if(oldManuState.getLocation() == Location::WAREHOUSE) {
				unsigned int material = oldManuState.getNumMaterial();
				if(material >= 1) {
					newManuState.setNumProducts(
					    oldManuState.getNumProducts() + 1);
					newManuState.setNumMaterial(material - 1);
				}
			}
		}
		break;
		default:
		break;
		}

	}
}

RealType Task_Charging::getReward(
    StateCollection *oldState, Action*, StateCollection *newState) {
	ManuState oldManuState(oldState->getState());
	ManuState newManuState(newState->getState());

	RealType reward = std::max(
	    newManuState.getNumProducts() - oldManuState.getNumProducts(), 0u);

//	if(reward <= 0) reward -= 0.75;

//	reward += 20;

	return reward;
}

bool Task_Charging::isResetState(State* state) {
	ManuState manuState(state);
	return manuState.getCharge() < CHARGE_STEP;
}

/**
 * Creates a new discretizer for the state space of the task.
 */
AbstractStateDiscretizer* Task_Charging::newDiscretizer() {
	return new ModelStateDiscretizer(properties, {0, 1, 3});
}

/**
 * Returns the TransitionFunctionEnvironment for the task. This is
 * owned by the task.
 */
EnvironmentModel* Task_Charging::getEnvironment() {
	return _environment.get();
}

/**
 * Creates a new ActionEncoder able to encode actions of the type used
 * by the Task.
 */
ActionEncoder* Task_Charging::newActionEncoder() {
	return new ActionEncoder_ManuAction<Task_Charging::ManuAction>();
}

Task_Charging::Task_Charging() :
	    TransitionFunction(ManuState::newProperties(), new ActionSet()),
	    _environment(),
	    _charge(new rlearner_charging::ChargingReward) {
	_environment = make_shared<TransitionFunctionEnvironment>(this);
}

/**
 * Virtual destructor.
 */
Task_Charging::~Task_Charging() {
	delete properties;
}

} //namespace rlearner
