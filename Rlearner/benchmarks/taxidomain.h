// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ctaxidomain_H_
#define Rlearner_ctaxidomain_H_

#include <vector>

#include "GridWorldModel.h"
#include "../agent.h"
#include "../action.h"
#include "../discretizer.h"
#include "../rewardfunction.h"

#define PICKUPACTION 32
#define PUTDOWNACTION 64

namespace rlearner {

class TaxiDomain: public GridWorldModel {
protected:
	std::vector<std::pair<int, int> *> *targetXYValues;

protected:
	virtual void initTargetVector();

public:
	virtual void load(const char* filename);
	virtual void load(std::istream& stream);

	int getTargetPositionX(int numTarget);

	int getTargetPositionY(int numTarget);

	int getNumTargets() {
		return targetXYValues->size();
	}

	RealType getReward(StateCollection *, Action *, StateCollection *);

	virtual void transitionFunction(
	    State *oldstate,
	    Action *action,
	    State *newState,
	    ActionData *data = nullptr);

	virtual bool isResetState(State *state);
	virtual void getResetState(State *resetState);

public:
	TaxiDomain(const char* filename);
	virtual ~TaxiDomain();
};

class PickupAction: public PrimitiveAction {
public:
	PickupAction() {
		addType(PICKUPACTION);
	}
};

class PutdownAction: public PrimitiveAction {
public:
	PutdownAction() {
		addType(PUTDOWNACTION);
	}
};

class TaxiHierarchicalBehaviour:
    public HierarchicalSemiMarkovDecisionProcess, public RewardFunction {
protected:
	int target;
	TaxiDomain *model;

public:
	virtual bool isFinished(StateCollection *state, StateCollection *newState);
	virtual RealType getReward(
	    StateCollection *oldState,
	    Action *action,
	    StateCollection *newState);

public:
	TaxiHierarchicalBehaviour(
	    Episode *currentEpisode,
	    int target,
	    TaxiDomain *taximodel);
	virtual ~TaxiHierarchicalBehaviour();
};

class TaxiIsTargetDiscreteState: public AbstractStateDiscretizer {
protected:
	TaxiDomain *model;

public:
	virtual unsigned int getDiscreteStateNumber(StateCollection *stateCol);

public:
	TaxiIsTargetDiscreteState(TaxiDomain *model);
};

} //namespace rlearner

#endif //Rlearner_ctaxidomain_H_
