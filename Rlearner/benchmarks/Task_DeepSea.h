#ifndef Example_RlearnerStateChart_Task_DeepSea_H_
#define Example_RlearnerStateChart_Task_DeepSea_H_

#include "../system.h"
#include "../Task.h"
#include "../state.h"
#include "GridWorldModel.h"

namespace rlearner {

class Task_DeepSea_TimeReward: public RewardFunction {
public:
	virtual RealType getReward(StateCollection* oldState, Action* action, StateCollection* newState);
};

class Task_DeepSea: public Task, public RewardFunction, public TransitionFunction {
private:
	shared_ptr<GridWorldModel> _model;
	shared_ptr<EnvironmentModel> _environment;
	shared_ptr<Task_DeepSea_TimeReward> _timeReward;

	unsigned int _statePos = 0;

public:
	static const std::string& getMap();
	static RealType getSymbolReward(char symbol);

public:
	virtual ActionSet* newActionSet();

public:
	virtual RealType getReward(StateCollection* oldState, Action* action, StateCollection* newState);

	virtual RewardFunction* getRewardFunction(unsigned int index = 0);
	virtual unsigned int numRewardFunctions() const;

	virtual TransitionFunction* getTransitionFunction();

	virtual bool isResetState(State *);
	virtual void transitionFunction(State* oldstate, Action* action, State* newState, ActionData* data = NULL);

	virtual void getResetState(State* resetState);
	virtual AbstractStateDiscretizer* newDiscretizer();

	virtual EnvironmentModel* getEnvironment();
	virtual ActionEncoder* newActionEncoder();

protected:
	Task_DeepSea(const shared_ptr<GridWorldModel>& model);

public:
	Task_DeepSea& operator=(const Task_DeepSea&) = delete;
	Task_DeepSea(const Task_DeepSea&) = delete;

	Task_DeepSea(unsigned int maxBounces = -3);
	virtual ~Task_DeepSea();
};

} //namespace rlearner

#endif //Example_RlearnerStateChart_Task_DeepSea_H_
