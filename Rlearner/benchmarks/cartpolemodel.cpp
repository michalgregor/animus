// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY std::expRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "../system.h"
#include "cartpolemodel.h"
#include "../ril_debug.h"
#include "../state.h"
#include "../stateproperties.h"
#include "../action.h"
#include "../continuousactions.h"

namespace rlearner {

CartPoleModel::CartPoleModel(
    RealType dt, RealType uMax, RealType lengthTrack, RealType lengthPole,
    RealType massCart, RealType massPole, RealType mu_c, RealType mu_p, RealType g,
    bool endLeaveTrack, bool endOverRotate) :
	    LinearActionContinuousTimeTransitionFunction(new StateProperties(5, 0),
	        new ContinuousAction(new ContinuousActionProperties(1)), dt) {
	this->uMax = uMax;
	this->lengthTrack = lengthTrack;
	this->lengthPole = lengthPole;
	this->massCart = massCart;
	this->massPole = massPole;
	this->mu_c = mu_c;
	this->mu_p = mu_p;
	this->g = g;

	properties->setMaxValue(0, lengthTrack / 2 + 0.1);
	properties->setMinValue(0, -lengthTrack / 2 - 0.1);

	properties->setMaxValue(1, lengthTrack);
	properties->setMinValue(1, -lengthTrack);

	properties->setMaxValue(2, Constant::pi);
	properties->setMinValue(2, -Constant::pi);

	properties->setPeriodicity(2, true);

	properties->setMaxValue(3, 15);
	properties->setMinValue(3, -15);

	properties->setMaxValue(4, Constant::pi * 11);
	properties->setMinValue(4, -Constant::pi * 11);

	actionProp->setMaxActionValue(0, uMax);
	actionProp->setMinActionValue(0, -uMax);

	this->endLeaveTrack = endLeaveTrack;
	this->endOverRotate = endOverRotate;

	failedReward = -100.0;
}

CartPoleModel::~CartPoleModel() {
	delete properties;
	delete actionProp;
	delete contAction;
}

Matrix *CartPoleModel::getB(State *state) {
	RealType Phi, x, dx, dPhi;

	x = state->getContinuousState(0);
	dx = state->getContinuousState(1);
	Phi = state->getContinuousState(2);
	dPhi = state->getContinuousState(3);

	RealType denum = -4 * lengthPole / 3 * (massCart + massPole)
	    + lengthPole * massPole * pow(std::cos(Phi), 2);
	RealType uFactordx = -4 * lengthPole / denum / 3;
	RealType uFactordPhi = -std::cos(Phi) / denum;

	B->setZero();
	B->operator()(1, 0) = uFactordx;
	B->operator()(3, 0) = uFactordPhi;

	return B;
}

ColumnVector *CartPoleModel::getA(State *state) {
	RealType Phi, x, dx, ddx, dPhi, ddPhi;

	x = state->getContinuousState(0);
	dx = state->getContinuousState(1);
	Phi = state->getContinuousState(2);
	dPhi = state->getContinuousState(3);

	RealType sign_dx = 1.0;
	if(dx < 0) {
		sign_dx = -1.0;
	}
	// Calculate Inverse Matrix

	RealType denum = -4 * lengthPole / 3 * (massCart + massPole)
	    + lengthPole * massPole * pow(std::cos(Phi), 2);
	RealType b1 = g * sin(Phi) - mu_p * dPhi / (lengthPole * massPole);
	RealType b2 = lengthPole * massPole * dPhi * dPhi * sin(Phi) + mu_c * sign_dx;

	ddx = -lengthPole * massPole * std::cos(Phi) * b1 + 4 / 3 * lengthPole * b2;
	ddx = ddx / denum;

	ddPhi = (-massCart - massPole) * b1 + std::cos(Phi) * b2;
	ddPhi = ddPhi / denum;

	A->operator[](0) = dx;
	A->operator[](1) = ddx;
	A->operator[](2) = dPhi;
	A->operator[](3) = ddPhi;
	A->operator[](4) = dPhi;

	return A;
}

bool CartPoleModel::isFailedState(State *state) {
	bool failed = (endLeaveTrack
	    && std::abs(state->getContinuousState(0)) > lengthTrack / 2);

	failed = failed
	    | (endOverRotate
	        && std::abs(state->getContinuousState(4)) > 10 * Constant::pi);

	return failed;
}

void CartPoleModel::doSimulationStep(
    State *state, RealType timestep, Action *action, ActionData *data) {
	getDerivationX(state, action, derivation, data);

	RealType ddx = derivation->operator[](1);
	RealType ddPhi = derivation->operator[](3);

	for(unsigned int i = 0; i < state->getNumContinuousStates(); i++) {
		state->setContinuousState(i,
		    state->getContinuousState(i)
		        + timestep * derivation->operator[](i));
	}

	state->setContinuousState(0,
	    state->getContinuousState(0) + pow(timestep, 2) * ddx / 2);
	state->setContinuousState(2,
	    state->getContinuousState(2) + pow(timestep, 2) * ddPhi / 2);
	state->setContinuousState(4,
	    state->getContinuousState(4) + pow(timestep, 2) * ddPhi / 2);

	if(!endLeaveTrack
	    && std::abs(state->getContinuousState(0)) >= lengthTrack / 2) {
		state->setContinuousState(1, 0.0);
	}
}

void CartPoleModel::getResetState(State *state) {
	TransitionFunction::getResetState(state);
	state->setContinuousState(0, state->getContinuousState(0) * 0.8);
	state->setContinuousState(1, 0.0);
	state->setContinuousState(3, 0.0);
	state->setContinuousState(4, state->getContinuousState(2));
}

CartPoleRewardFunction::CartPoleRewardFunction(CartPoleModel *model) :
	StateReward(model->getStateProperties()) {
	this->cartpoleModel = model;
	useHeighPeak = true;
	punishOverRotate = true;
}

RealType CartPoleRewardFunction::getStateReward(State *state) {
	RealType Phi = state->getContinuousState(2);
	RealType x = state->getContinuousState(0);

	RealType reward = std::cos(Phi) - 1
	    - 100 * bounded_exp((std::abs(x) - cartpoleModel->lengthTrack / 2) * 25);

	if(useHeighPeak) {
		RealType dreward = std::abs(-std::pow(Phi, 2.0) * 25);
		reward += dreward;
	}
	if(punishOverRotate) {
		RealType phi_ = state->getContinuousState(4);
		RealType dreward = 20 * std::exp(std::abs(phi_) - 10 * Constant::pi);
		reward -= dreward;
	}

	return reward;
}

void CartPoleRewardFunction::getInputDerivation(
    State *modelState, ColumnVector *targetState) {
	RealType Phi = modelState->getState(properties)->getContinuousState(2);
	RealType x = modelState->getContinuousState(0);

	if(x < 0) {
		targetState->operator[](0) = 25 * 100
		    * std::exp((std::abs(x) - cartpoleModel->lengthTrack / 2) * 25);
	} else {
		targetState->operator[](0) = -25 * 100
		    * std::exp((std::abs(x) - cartpoleModel->lengthTrack / 2) * 25);
	}

	targetState->operator[](2) = 0.0;

	if(useHeighPeak) {

		targetState->operator[](2) = -50 * Phi * std::exp(-pow(Phi, 2.0) * 25);
	}

	if(punishOverRotate) {
		RealType phi_ = modelState->getContinuousState(4);
		if(phi_ < 0) {
			targetState->operator[](4) = 20
			    * std::exp(std::abs(phi_) - 10 * Constant::pi);
		} else {
			targetState->operator[](4) = -20
			    * std::exp(std::abs(phi_) - 10 * Constant::pi);
		}
	}

	targetState->operator[](1) = 0;
	targetState->operator[](2) = targetState->operator[](2) - sin(Phi);
	targetState->operator[](3) = 0;
}

CartPoleHeightRewardFunction::CartPoleHeightRewardFunction(
    CartPoleModel *model) :
	StateReward(model->getStateProperties()) {
	this->cartpoleModel = model;
}

RealType CartPoleHeightRewardFunction::getStateReward(State *state) {
	RealType Phi = state->getContinuousState(2);
	//RealType x = state->getContinuousState(0);

	RealType reward = std::cos(Phi) - 1;
	return reward;
}

void CartPoleHeightRewardFunction::getInputDerivation(
    State *modelState, ColumnVector *targetState) {
	RealType Phi = modelState->getState(properties)->getContinuousState(2);
//	RealType x = modelState->getContinuousState(0);

	targetState->operator[](1) = 0;
	targetState->operator[](2) = -sin(Phi);
	targetState->operator[](3) = 0;
}

} //namespace rlearner
