// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "acrobotmodel.h"
#include <cmath>

#include "../system.h"
#include "../stateproperties.h"
#include "../state.h"
#include "../action.h"
#include "../continuousactions.h"

namespace rlearner {

AcroBotModel::AcroBotModel(
    RealType dt, RealType uMax, RealType length1, RealType length2, RealType mass1,
    RealType mass2, RealType mu1, RealType mu2, RealType g) :
	    _uMax(uMax),
	    _g(g),
	    _mass1(mass1),
	    _mass2(mass2),
	    _length1(length1),
	    _length2(length2),
	    _mu1(mu1),
	    _mu2(mu2),
	    LinearActionContinuousTimeTransitionFunction(new StateProperties(4, 0),
	        new ContinuousAction(new ContinuousActionProperties(1)), dt) {
	properties->setMaxValue(0, Constant::pi);
	properties->setMinValue(0, -Constant::pi);

	properties->setPeriodicity(0, true);

	properties->setMaxValue(1, Constant::pi * 2);
	properties->setMinValue(1, -Constant::pi * 2);

	properties->setMaxValue(2, Constant::pi);
	properties->setMinValue(2, -Constant::pi);

	properties->setPeriodicity(2, true);

	properties->setMaxValue(3, Constant::pi * 3);
	properties->setMinValue(3, -Constant::pi * 3);

	contAction->getContinuousActionProperties()->setMaxActionValue(0, _uMax);
	contAction->getContinuousActionProperties()->setMinActionValue(0, -_uMax);

}

AcroBotModel::~AcroBotModel() {
	delete properties;
	delete actionProp;
	delete contAction;
}

Matrix *AcroBotModel::getB(State *state) {
	RealType Phi1 = state->getContinuousState(0);
//	RealType dPhi1 = state->getContinuousState(1);
	RealType Phi2 = state->getContinuousState(2) + Phi1;
//	RealType dPhi2 = state->getContinuousState(3);

	RealType denum = 16 * pow(_length2 / _length1, 2)
	    * (_mass1 * _mass2 / 9 + pow(_mass2, 2) / 3)
	    - 4 * pow(_length1 * _length2 * _mass2 * cos(Phi1 - Phi2), 2);

	RealType dPhi1_U = 4 / 3 * pow(_length2, 2) * _mass2 * (-1)
	    - 2 * _length1 * _length2 * _mass2 * cos(Phi1 - Phi2);
	RealType dPhi2_U = 2 * _length1 * _length2 * _mass2 * cos(Phi1 - Phi2)
	    + 4 * (_mass1 / 3 + _mass2) / pow(_length1, 2);

	B->setZero();
	B->operator()(1, 0) = dPhi1_U / denum;
	B->operator()(3, 0) = dPhi2_U / denum;

	return B;
}

ColumnVector *AcroBotModel::getA(State *state) {
	RealType Phi1 = state->getContinuousState(0);
	RealType dPhi1 = state->getContinuousState(1);
	RealType Phi2 = state->getContinuousState(2) + Phi1;
	RealType dPhi2 = state->getContinuousState(3);

	RealType denum = 16 * pow(_length2 / _length1, 2)
	    * (_mass1 * _mass2 / 9 + pow(_mass2, 2) / 3)
	    - 4 * pow(_length1 * _length2 * _mass2 * cos(Phi1 - Phi2), 2);

	RealType b1, b2;

	b1 = 2 * _mass2 * _length2 * _length1 * pow(dPhi2, 2) * sin(Phi2 - Phi1)
	    + (_mass1 + 2 * _mass2) * sin(Phi1) * _length1 * _g - _mu1 * dPhi1;
	b2 = 2 * _mass2 * _length2 * _length1 * pow(dPhi1, 2) * sin(Phi1 - Phi2)
	    + _mass2 * sin(Phi2) * _length2 * _g - _mu2 * dPhi2;

	RealType ddPhi1 = 4 / 3 * pow(_length2, 2) * _mass2 * b1
	    - 2 * _length1 * _length2 * _mass2 * cos(Phi1 - Phi2) * b2;
	RealType ddPhi2 = -2 * _length1 * _length2 * _mass2 * cos(Phi1 - Phi2) * b1
	    + 4 * (_mass1 / 3 + _mass2) / pow(_length1, 2) * b2;

	ddPhi1 = ddPhi1 / denum;
	ddPhi2 = ddPhi2 / denum;

	A->operator[](0) = dPhi1;
	A->operator[](1) = ddPhi1;
	A->operator[](2) = dPhi2;
	A->operator[](3) = ddPhi2;

	return A;
}

bool AcroBotModel::isFailedState(State *) {
	return false;
}

void AcroBotModel::doSimulationStep(
    State *state, RealType timestep, Action *action, ActionData *data) {
	getDerivationX(state, action, derivation, data);

	RealType ddPhi1 = derivation->operator[](1);
	RealType ddPhi2 = derivation->operator[](3);

	for(unsigned int i = 0; i < state->getNumContinuousStates(); i++) {
		state->setContinuousState(i,
		    state->getContinuousState(i)
		        + timestep * derivation->operator[](i));
	}

	state->setContinuousState(0,
	    state->getContinuousState(0) + pow(timestep, 2) * ddPhi1 / 2);
	state->setContinuousState(2,
	    state->getContinuousState(2) + pow(timestep, 2) * ddPhi2 / 2);
}

void AcroBotModel::getResetState(State *state) {
	TransitionFunction::getResetState(state);
	state->setContinuousState(1, 0.0);
	state->setContinuousState(3, 0.0);
//	state->setContinuousState(2, state->getContinuousState(2) - state->getContinuousState(0));
}

AcroBotRewardFunction::AcroBotRewardFunction(
    AcroBotModel *model, RealType segmentFactor) :
	StateReward(model->getStateProperties()), _model(model) {
	useHeighPeak = true;
	this->segmentFactor = segmentFactor;
	power = 1.0;

}

RealType AcroBotRewardFunction::getStateReward(State *state) {

	RealType Phi1 = state->getContinuousState(0);
	RealType Phi2 = state->getContinuousState(2);

	RealType height = 2
	    * (segmentFactor * _model->_length1 * (cos(Phi1) + 1)
	        + (1 - segmentFactor) * _model->_length2 * (cos(Phi2 + Phi1) + 1))
	    / (_model->_length1 + _model->_length2) / 2.0;

	RealType reward = 2 * (pow(height, power) - 1.0);
	if(useHeighPeak) {
		RealType dreward = 0.5 * exp((-pow(Phi1, 2.0) - pow(Phi2, 2.0)) * 25);
		reward += dreward;
	}

	return reward;
}

void AcroBotRewardFunction::getInputDerivation(
    State *modelState, ColumnVector *targetState) {
	RealType Phi1 = modelState->getState(properties)->getContinuousState(0);
	RealType Phi2 = modelState->getState(properties)->getContinuousState(2);

	targetState->operator[](0) = -segmentFactor * _model->_length1 * sin(Phi1);
	targetState->operator[](1) = 0;
	targetState->operator[](2) = -(1 - segmentFactor) * _model->_length2
	    * sin(Phi2 + Phi1);
	targetState->operator[](3) = 0;

	if(useHeighPeak) {
		targetState->operator[](0) = targetState->operator[](0)
		    - 25 * (Phi1) * exp((-pow(Phi1, 2.0) - pow(Phi2, 2.0)) * 25);
		targetState->operator[](2) = targetState->operator[](2)
		    - 25 * (Phi1) * exp((-pow(Phi1, 2.0) - pow(Phi2, 2.0)) * 25);
	}
}

AcroBotHeightRewardFunction::AcroBotHeightRewardFunction(AcroBotModel *model) :
	StateReward(model->getStateProperties()), _model(model), _useHeighPeak(true) {
}

RealType AcroBotHeightRewardFunction::getStateReward(State *state) {
	RealType Phi1 = state->getContinuousState(0);
	RealType Phi2 = state->getContinuousState(2);

	RealType reward = _model->_length1 * (cos(Phi1) - 1)
	    + _model->_length2 * (cos(Phi2 + Phi1) - 1);

	return reward;
}

void AcroBotHeightRewardFunction::getInputDerivation(
    State *modelState, ColumnVector *targetState) {
	RealType Phi1 = modelState->getState(properties)->getContinuousState(0);
	RealType Phi2 = modelState->getState(properties)->getContinuousState(2);

	targetState->operator[](0) = -_model->_length1 * sin(Phi1);
	targetState->operator[](1) = 0;
	targetState->operator[](2) = -_model->_length2 * sin(Phi2 + Phi1);
	targetState->operator[](3) = 0;
}

AcroBotExpRewardFunction::AcroBotExpRewardFunction(
    AcroBotModel *l_model, RealType l_expFactor) :
	    StateReward(l_model->getStateProperties()),
	    _model(l_model),
	    expFactor(l_expFactor) {
}

RealType AcroBotExpRewardFunction::getStateReward(State *state) {
	RealType Phi1 = state->getContinuousState(0);
	RealType Phi2 = state->getContinuousState(2);

	RealType reward = _model->_length1 * (cos(Phi1) - 1)
	    + _model->_length2 * (cos(Phi2 + Phi1) - 1);

	reward = -1.0 + exp(expFactor * reward);

	return reward;
}

void AcroBotExpRewardFunction::getInputDerivation(State *, ColumnVector *) {

}

AcroBotVelocityRewardFunction::AcroBotVelocityRewardFunction(
    AcroBotModel *model) :
	StateReward(model->getStateProperties()), _model(model) {
	invertVelocity = false;
}

RealType AcroBotVelocityRewardFunction::getStateReward(State *state) {
	RealType reward = fabs(state->getContinuousState(1));

	if(!invertVelocity) {
		reward = -1.0 + reward;
	} else {
		reward = -reward;
	}
	return reward;
}

void AcroBotVelocityRewardFunction::getInputDerivation(
    State *, ColumnVector *) {}

} //namespace rlearner
