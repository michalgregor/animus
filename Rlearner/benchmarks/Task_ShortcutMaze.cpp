#include "Task_ShortcutMaze.h"

namespace rlearner {

Task_ShortcutMaze::Task_ShortcutMaze() {
	std::stringstream map;

	map <<
	R"(Gridworld
	StartValues: S
	TargetValues: G
	ProhibitedValues: X

	00000000G
	000000000
	000000000
	0XXXXXXXX
	000000000
	000S00000)";

	auto model = systematic::make_shared<GridWorldModel>(map, -1);
	model->setRewardBounce(0);
	model->setRewardStandard(0);
	model->setRewardSuccess(1.0);

	setGridWorldModel(model);
}

} // namespace rlearner
