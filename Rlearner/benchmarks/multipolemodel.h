// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cmultipolemodel_H_
#define Rlearner_cmultipolemodel_H_

#include <cmath>

#include "../environmentmodel.h"
#include "../rewardfunction.h"
#include "../agentcontroller.h"
#include "../discretizer.h"
#include "../action.h"

namespace rlearner {

class MultiPoleDiscreteState: public AbstractStateDiscretizer {
public:
	virtual unsigned int getDiscreteStateNumber(StateCollection *state);

public:
	MultiPoleDiscreteState();
	virtual ~MultiPoleDiscreteState() {}
};

class MultiPoleFailedState: public AbstractStateDiscretizer {
public:
	virtual unsigned int getDiscreteStateNumber(StateCollection *state);

public:
	MultiPoleFailedState();
	virtual ~MultiPoleFailedState() {}
};

class MultiPoleModel: public EnvironmentModel, public RewardFunction {
public:
	static constexpr RealType one_degree = Constant::deg2rad;
	static constexpr RealType six_degrees = 6*Constant::deg2rad;
	static constexpr RealType twelve_degrees = 12*Constant::deg2rad;
	static constexpr RealType fifty_degrees = 50*Constant::deg2rad;

protected:
	//! Internal state variables.
	RealType x, x_dot, theta, theta_dot;
	//! Calculate the next state based on the action.
	virtual void doNextState(PrimitiveAction *action);

public:
	//! Returns the reward for the transition, implements the RewardFunction
	//! interface.
	virtual RealType getReward(
	    StateCollection *oldState,
	    Action *action,
	    StateCollection *newState);

	//! Fetches the internal state and stores it in the state object.
	virtual void getState(State *state);
	//! Resets the model.
	virtual void doResetModel();

public:
	MultiPoleModel();
	virtual ~MultiPoleModel();
};

class MultiPoleContinuousReward: public StateReward {
public:
	RealType getStateReward(State *modelState);

public:
	MultiPoleContinuousReward(StateProperties *modelState);
	virtual ~MultiPoleContinuousReward() {}
};

class MultiPoleAction: public PrimitiveAction {
protected:
	RealType force;

public:
	RealType getForce();

public:
	MultiPoleAction(RealType force);
};

class MultiPoleController: public AgentController {
public:
	virtual Action* getNextAction(StateCollection *state, ActionDataSet *data =
	    nullptr);

public:
	MultiPoleController(ActionSet *actions);
	virtual ~MultiPoleController();
};

class MultiPoleDiscreteController: public AgentController, StateObject {
public:
	virtual Action* getNextAction(StateCollection *state, ActionDataSet *data =
	    nullptr);

public:
	MultiPoleDiscreteController(
	    ActionSet *actions,
	    StateProperties *discState);
	virtual ~MultiPoleDiscreteController();
};

} //namespace rlearner

#endif //Rlearner_cmultipolemodel_H_
