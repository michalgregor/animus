// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "../ril_debug.h"
#include "taxidomain.h"
#include "../state.h"
#include "../statecollection.h"
#include "../stateproperties.h"
#include "../statemodifier.h"
#include "../action.h"
#include "../system.h"

namespace rlearner {

TaxiDomain::TaxiDomain(const char* filename) :
		GridWorldModel(filename, 100) {
	targetXYValues = nullptr;
	delete properties;
	properties = new StateProperties(0, 5);
	initTargetVector();
}

TaxiDomain::~TaxiDomain() {
	if (targetXYValues != nullptr) {
		std::vector<std::pair<int, int> *>::iterator it =
		        targetXYValues->begin();
		for (; it != targetXYValues->end(); it++) {
			delete *it;
		}
		delete targetXYValues;
	}
}

void TaxiDomain::initTargetVector() {
	if (targetXYValues == nullptr) {
		targetXYValues = new std::vector<std::pair<int, int> *>();
	} else {
		std::vector<std::pair<int, int> *>::iterator it =
		        targetXYValues->begin();
		for (; it != targetXYValues->end(); it++) {
			delete *it;
		}
		targetXYValues->clear();
	}

	for (unsigned int x = 0; x < this->getSizeX(); x++) {
		for (unsigned int y = 0; y < this->getSizeY(); y++) {
			if (_targetValues.find(getGridValue(x, y)) != _targetValues.end()) {
				targetXYValues->push_back(new std::pair<int, int>(x, y));
			}
		}
	}
	properties->setDiscreteStateSize(0, getSizeX());
	properties->setDiscreteStateSize(1, getSizeY());
	properties->setDiscreteStateSize(2, 0);
	properties->setDiscreteStateSize(3, targetXYValues->size() + 1);
	properties->setDiscreteStateSize(4, targetXYValues->size());
}

void TaxiDomain::load(const char* filename) {
	GridWorldModel::load(filename);
	initTargetVector();
}

void TaxiDomain::load(std::istream& stream) {
	GridWorldModel::load(stream);
	initTargetVector();
}

int TaxiDomain::getTargetPositionX(int numTarget) {
	return (*targetXYValues)[numTarget]->first;
}

int TaxiDomain::getTargetPositionY(int numTarget) {
	return (*targetXYValues)[numTarget]->second;
}

void TaxiDomain::transitionFunction(State *oldState, Action *action,
        State *newState, ActionData *data) {
	if (action->isType(RLEARNER_GRIDWORLDACTION)) {
		GridWorldModel::transitionFunction(oldState, action, newState, data);
	}
	int pos_x = oldState->getDiscreteState(0);
	int pos_y = oldState->getDiscreteState(1);

	int pasLocation = oldState->getDiscreteState(3);
	int pasDestination = oldState->getDiscreteState(4);

	if (action->isType(PICKUPACTION)) {
		if (pasLocation < getNumTargets()) {
			if (getTargetPositionX(pasLocation) == pos_x
			        && getTargetPositionY(pasLocation) == pos_y) {
				pasLocation = getNumTargets();
			}
		}
	}
	if (action->isType(PUTDOWNACTION)) {
		if (pasLocation == getNumTargets()) {
			if (getTargetPositionX(pasDestination) == pos_x
			        && getTargetPositionY(pasDestination) == pos_y) {
				pasLocation = pasDestination;
			}
		}
	}

	newState->setDiscreteState(3, pasLocation);
	newState->setDiscreteState(4, pasDestination);
}

bool TaxiDomain::isResetState(State *state) {
	return (GridWorldModel::isFailedState(state)
	        || state->getDiscreteState(3) == state->getDiscreteState(4));
}

void TaxiDomain::getResetState(State *resetState) {
	auto rand = make_rand();

	GridWorldModel::getResetState(resetState);
	int location = rand() % targetXYValues->size();
	resetState->setDiscreteState(3, location);

	int target = rand() % targetXYValues->size();

	if (target == location) {
		target = (target + 1) % this->getNumTargets();
	}
	resetState->setDiscreteState(4, target);
}

RealType TaxiDomain::getReward(StateCollection *oldStateCol, Action *action,
        StateCollection *newStateCol) {
	State *oldState = oldStateCol->getState();
	State *newState = newStateCol->getState();

	RealType reward = this->getRewardStandard();

	if (action->isType(RLEARNER_GRIDWORLDACTION)) {
		if (oldState->getDiscreteState(0) == newState->getDiscreteState(0)
		        && oldState->getDiscreteState(1)
		                == newState->getDiscreteState(1)) {
			reward += this->getRewardBounce();
		}
	}

	if (action->isType(PICKUPACTION)) {
		// Wrong Pickup action
		if (!(oldState->getDiscreteState(3) < getNumTargets()
		        && newState->getDiscreteState(3) == getNumTargets())) {
			reward += this->getRewardBounce();
			;
		}
	}
	if (action->isType(PUTDOWNACTION)) {
		if (oldState->getDiscreteState(3) == getNumTargets()
		        && oldState->getDiscreteState(0)
		                == getTargetPositionX(oldState->getDiscreteState(4))
		        && oldState->getDiscreteState(1)
		                == getTargetPositionY(oldState->getDiscreteState(4))) {
			reward += getRewardSuccess();
		} else {
			reward += this->getRewardBounce();
			;
		}
	}
	return reward;
}

TaxiHierarchicalBehaviour::TaxiHierarchicalBehaviour(Episode *currentEpisode,
        int target, TaxiDomain *taximodel) :
		HierarchicalSemiMarkovDecisionProcess(currentEpisode) {
	this->model = taximodel;
	this->target = target;
}

TaxiHierarchicalBehaviour::~TaxiHierarchicalBehaviour() {

}

bool TaxiHierarchicalBehaviour::isFinished(StateCollection *,
        StateCollection *newStateCol) {
	State *state = newStateCol->getState();
	if (state->getDiscreteState(0) == model->getTargetPositionX(target)
	        && state->getDiscreteState(1)
	                == model->getTargetPositionY(target)) {
		return true;
	} else {
		return false;
	}
}

RealType TaxiHierarchicalBehaviour::getReward(StateCollection *oldStateCol,
        Action *action, StateCollection *newStateCol) {
	State *oldState = oldStateCol->getState();
	State *newState = newStateCol->getState();

	RealType reward = model->getRewardStandard();

	if (action->isType(RLEARNER_GRIDWORLDACTION)) {
		if (oldState->getDiscreteState(0) == newState->getDiscreteState(0)
		        && oldState->getDiscreteState(1)
		                == newState->getDiscreteState(1)) {
			reward += model->getRewardBounce();
		}
		if (newState->getDiscreteState(0) == model->getTargetPositionX(target)
		        && newState->getDiscreteState(1)
		                == model->getTargetPositionY(target)) {
			reward += model->getRewardSuccess() / 2;
		}
	}
	return reward;
}

TaxiIsTargetDiscreteState::TaxiIsTargetDiscreteState(TaxiDomain *model) :
		AbstractStateDiscretizer(model->getNumTargets() + 1) {
	this->model = model;
}

unsigned int TaxiIsTargetDiscreteState::getDiscreteStateNumber(
        StateCollection *stateCol) {
	int target = -1;
	State *state = stateCol->getState();
	for (int i = 0; i < model->getNumTargets(); i++) {
		if (state->getDiscreteState(0) == model->getTargetPositionX(i)
		        && state->getDiscreteState(1) == model->getTargetPositionY(i)) {
			target = i;
			break;
		}
	}
	return target + 1;
}

} //namespace rlearner
