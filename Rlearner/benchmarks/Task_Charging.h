#ifndef Example_RlearnerStateChart_Task_Charging_H_
#define Example_RlearnerStateChart_Task_Charging_H_

#include "../system.h"
#include "../Task.h"
#include "../state.h"

namespace rlearner {

#ifndef SWIG

namespace rlearner_charging {

enum class ActionType {
	MOVE_TO_WAREHOUSE,
	MOVE_TO_STORE,
	MOVE_TO_CHARGER,
	CHARGE,
	GET_MATERIAL,
	CREATE_PRODUCT
};

enum class Location {
	WAREHOUSE,
	STORE,
	CHARGER
};

class ManuAction: public PrimitiveAction {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int /*version*/);
	void serialize(OArchive& ar, const unsigned int /*version*/);

protected:
	ActionType _actionType;

protected:
	//! For use in serialization.
	ManuAction(): _actionType() {}

public:
	ManuAction(ActionType actionType): _actionType(actionType) {}

	ActionType getActionType() const {
		return _actionType;
	}

	void setActionType(ActionType actionType) {
		_actionType = actionType;
	}
};

/**
 * @class ManuState
 *
 * A helper class that makes it easier to set up and handle StateProperties.
 */
class ManuState {
private:
	//! Pointer to the underlying State.
	State* _state;

public:
	unsigned int getNumMaterial() {
		return _state->getDiscreteState(0);
	}

	void setNumMaterial(unsigned int num) {
		_state->setDiscreteState(0, num);
	}

	Location getLocation() {
		return Location(_state->getDiscreteState(1));
	}

	void setLocation(Location loc) {
		_state->setDiscreteState(1, static_cast<int>(loc));
	}

	unsigned int getNumProducts() const {
		return _state->getDiscreteState(2);
	}

	void setNumProducts(unsigned int num) {
		_state->setDiscreteState(2, num);
	}

	int getCharge() {
		return _state->getDiscreteState(3);
	}

	void setCharge(int charge) {
		_state->setDiscreteState(3, charge);
	}

public:
	const State* getState() const {
		return _state;
	}

	State* getState() {
		return _state;
	}

	static StateProperties* newProperties();
	static void initState(State* state);

	/**
	 * Constructor.
	 * @param properties Must point to a valid StateProperties object
	 * associated with the state.
	 */
	ManuState(State* state): _state(state) {}
};

} //rlearner_charging

#endif //#ifndef SWIG

class Task_Charging: public Task, public RewardFunction, public TransitionFunction {
public:
	using ActionType = rlearner_charging::ActionType;
	using Location = rlearner_charging::Location;
	using ManuAction = rlearner_charging::ManuAction;
	using ManuState = rlearner_charging::ManuState;

private:
	shared_ptr<EnvironmentModel> _environment;
	shared_ptr<RewardFunction> _charge;

public:
	virtual RewardFunction* getRewardFunction(unsigned int index = 0);
	virtual unsigned int numRewardFunctions() const;

	virtual TransitionFunction* getTransitionFunction();

	virtual ActionSet* newActionSet() {
		ActionSet* actionSet = new ActionSet;

		actionSet->add(new ManuAction(ActionType::MOVE_TO_STORE));
		actionSet->add(new ManuAction(ActionType::MOVE_TO_WAREHOUSE));
		actionSet->add(new ManuAction(ActionType::GET_MATERIAL));
		actionSet->add(new ManuAction(ActionType::CREATE_PRODUCT));
		actionSet->add(new ManuAction(ActionType::MOVE_TO_CHARGER));
		actionSet->add(new ManuAction(ActionType::CHARGE));

		return actionSet;
	}

	virtual bool isResetState(State* state);

	virtual void transitionFunction(State *oldstate, Action *action, State *newState, ActionData *data = NULL);
	virtual RealType getReward(StateCollection *oldState, Action *action, StateCollection *newState);

	virtual void getResetState(State *resetState);
	virtual AbstractStateDiscretizer* newDiscretizer();

	virtual EnvironmentModel* getEnvironment();
	virtual ActionEncoder* newActionEncoder();

public:
	Task_Charging& operator=(const Task_Charging&) = delete;
	Task_Charging(const Task_Charging&) = delete;

	Task_Charging();
	virtual ~Task_Charging();
};

} //namespace rlearner

SYS_EXPORT_CLASS(rlearner::rlearner_charging::ManuAction)

#endif //Example_RlearnerStateChart_Task_Charging_H_
