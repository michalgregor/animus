#ifndef Rlearner_Task_HungryThirsty_H_
#define Rlearner_Task_HungryThirsty_H_

#include "Task_GridWorld.h"
#include <Systematic/generator/BoundaryGenerator.h>

namespace rlearner {

class HugryThirstyAction: public GridWorldAction {
public:
	enum ActionType {
		none,
		eat,
		drink
	};

protected:
	ActionType _actionType;

public:
	ActionType getActionType() const {
		return _actionType;
	}

	void setActionType(ActionType actionType) {
		_actionType = actionType;
	}

public:
	HugryThirstyAction(int x_move, int y_move, ActionType actionType = none):
		GridWorldAction(x_move, y_move), _actionType(actionType) {}

	virtual ~HugryThirstyAction() = default;
};

class HugryThirstyGridWorldModel: public GridWorldModel {
protected:
	std::set<char> _foodValues;
	std::set<char> _drinkValues;
	unsigned int _statePos;
	BoundaryGenerator<float> _generator{0, 1};

	bool isFoodValue(char value) const {
		return _foodValues.find(value) != _foodValues.end();
	}

	bool isDrinkValue(char value) const {
		return _drinkValues.find(value) != _foodValues.end();
	}

public:
	virtual void transitionFunction(State *oldstate, Action *action,
			State *newState, ActionData *data = nullptr);

	virtual RealType getReward(StateCollection *oldState, Action *action,
	        StateCollection *newState);

	void addFoodValue(char value);
	void removeFoodValue(char value);
	void addDrinkValue(char value);
	void removeDrinkValue(char value);

public:
	HugryThirstyGridWorldModel(unsigned int size_x, unsigned int size_y, unsigned int max_bounces = -1);
	HugryThirstyGridWorldModel(std::istream& file, unsigned int max_bounces = -1);
	HugryThirstyGridWorldModel(const char* filename, unsigned int max_bounces = -1);

	virtual ~HugryThirstyGridWorldModel() = default;
};

class Task_HugryThirsty: public Task_GridWorld {
public:
	//! Provides actions for moving up, down, left and right; and actions
	//! for eating and drinking.
	virtual ActionSet* newActionSet();

public:
	Task_HugryThirsty();
	virtual ~Task_HugryThirsty() = default;
};

} // namespace rlearner

#endif //Rlearner_Task_HungryThirsty_H_
