// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cpendulummodel_H_
#define Rlearner_cpendulummodel_H_

#include "../system.h"
#include "../transitionfunction.h"
#include "../rewardfunction.h"
#include "../agentlistener.h"

namespace rlearner {

class PendulumModel: public LinearActionContinuousTimeTransitionFunction {
protected:
	virtual void doSimulationStep(
	    State *state,
	    RealType timestep,
	    Action *action,
	    ActionData *data);

public:
	RealType uMax;
	RealType dPhiMax;
	RealType g;
	RealType mass;
	RealType length;
	RealType mu; // friction

public:
	virtual Matrix *getB(State *state);
	virtual ColumnVector *getA(State *state);

	virtual bool isFailedState(State *state);
	virtual void getResetState(State *resetState);
	virtual void setParameter(std::string paramName, RealType value);

public:
	PendulumModel(
	    RealType dt,
	    RealType uMax = 5,
	    RealType dPhiMax = 10,
	    RealType length = 1,
	    RealType mass = 1,
	    RealType mu = 1.0,
	    RealType g = 9.81);

	virtual ~PendulumModel();
};

class PendulumRewardFunction: public StateReward {
public:
	RealType rewardFactor;

public:
	virtual RealType getStateReward(State *state);
	virtual void getInputDerivation(
	    State *modelState,
	    ColumnVector *targetState);

public:
	PendulumRewardFunction(PendulumModel *model);
};

class PendulumUpTimeCalculator: public SemiMDPListener {
protected:
	RealType phi_up;RealType dt;
	int up_steps;

public:
	virtual void nextStep(
	    StateCollection *oldState,
	    Action *action,
	    StateCollection *nextState);
	virtual void newEpisode();

	RealType getUpTime();
	int getUpSteps();

public:
	PendulumUpTimeCalculator(RealType phi_up, RealType dt);
};

} //namespace rlearner

#endif //Rlearner_cpendulummodel_H__
