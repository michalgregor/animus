#include "Task_HungryThirsty.h"
#include "../state.h"
#include <Systematic/generator/SpaceSeqGenerator.h>
#include <Systematic/type_id/pair.h>
#include <boost/serialization/utility.hpp>

namespace rlearner {

void HugryThirstyGridWorldModel::addFoodValue(char value) {
	_foodValues.insert(value);
}

void HugryThirstyGridWorldModel::removeFoodValue(char value) {
	_foodValues.erase(value);
}

void HugryThirstyGridWorldModel::addDrinkValue(char value) {
	_drinkValues.insert(value);
}

void HugryThirstyGridWorldModel::removeDrinkValue(char value) {
	_drinkValues.erase(value);
}

HugryThirstyGridWorldModel::HugryThirstyGridWorldModel(
	unsigned int size_x, unsigned int size_y, unsigned int max_bounces
): GridWorldModel(size_x, size_y, max_bounces),
	_statePos(properties->getNumDiscreteStates())
{
	properties->setNumDiscreteStates(_statePos + 2);
	properties->setDiscreteStateSize(_statePos, 2);
	properties->setDiscreteStateSize(_statePos + 1, 2);
}

HugryThirstyGridWorldModel::HugryThirstyGridWorldModel(
	std::istream& file, unsigned int max_bounces
): GridWorldModel(file, max_bounces),
	_statePos(properties->getNumDiscreteStates())
{
	properties->setNumDiscreteStates(_statePos + 2);
	properties->setDiscreteStateSize(_statePos, 2);
	properties->setDiscreteStateSize(_statePos + 1, 2);
}

HugryThirstyGridWorldModel::HugryThirstyGridWorldModel(
	const char* filename, unsigned int max_bounces
): GridWorldModel(filename, max_bounces),
	_statePos(properties->getNumDiscreteStates())
{
	properties->setNumDiscreteStates(_statePos + 2);
	properties->setDiscreteStateSize(_statePos, 2);
	properties->setDiscreteStateSize(_statePos + 1, 2);
}

void HugryThirstyGridWorldModel::transitionFunction(
	State *oldstate, Action *action, State *newState, ActionData *data
) {
	GridWorldModel::transitionFunction(oldstate, action, newState, data);
	HugryThirstyAction* haction = dynamic_cast<HugryThirstyAction*>(action);

	if(haction->getActionType() == HugryThirstyAction::drink) {
		int x = newState->getState()->getDiscreteState(0);
		int y = newState->getState()->getDiscreteState(1);
		if(isDrinkValue(getGridValue(x, y))) newState->getState()->setDiscreteState(_statePos, 1);
	} else {
		if(_generator() <= 0.1) newState->getState()->setDiscreteState(_statePos, 0);
	}

	// agent should always be hungry by default
	newState->getState()->setDiscreteState(_statePos + 1, 0);

	if(haction->getActionType() == HugryThirstyAction::eat &&
		newState->getState()->getDiscreteState(_statePos)
	) {
		newState->getState()->setDiscreteState(_statePos + 1, 1);
	}
}

RealType HugryThirstyGridWorldModel::getReward(
	StateCollection *oldState, Action *action, StateCollection *newState
) {
	RealType reward = GridWorldModel::getReward(oldState, action, newState);
	return reward + newState->getState()->getDiscreteState(_statePos + 1);
}

ActionSet* Task_HugryThirsty::newActionSet() {
	ActionSet* actionSet = new ActionSet;

	actionSet->add(new HugryThirstyAction(0, 1));
	actionSet->add(new HugryThirstyAction(1, 0));
	actionSet->add(new HugryThirstyAction(0, -1));
	actionSet->add(new HugryThirstyAction(-1, 0));

	actionSet->add(new HugryThirstyAction(0, 0, HugryThirstyAction::drink));
	actionSet->add(new HugryThirstyAction(0, 0, HugryThirstyAction::eat));

	return actionSet;
}

Task_HugryThirsty::Task_HugryThirsty() {
	std::stringstream map;

	map <<
	R"(Gridworld
	StartValues: S
	ProhibitedValues: X

	0000000
	000X000
	000X000
	XXXXXX0
	000XS00
	0000000
	000X000)";

	auto model = systematic::make_shared<HugryThirstyGridWorldModel>(map, -1);
	model->setRewardBounce(0);
	model->setRewardStandard(0);
	model->setRewardSuccess(0);
	model->addDrinkValue('D');
	model->addFoodValue('F');

	SpaceSeqGenerator<std::pair<unsigned int, unsigned int> > locationGenerator(
		{
			{0, 0},
			{0, model->getSizeY()-1},
			{model->getSizeX()-1, 0},
			{model->getSizeX()-1, model->getSizeY()-1}
		});

	auto locations = locationGenerator(2, false);

	model->setGridValue(locations[0].first, locations[0].second, 'D');
	model->setGridValue(locations[1].first, locations[1].second, 'D');

	setGridWorldModel(model);
}

} // namespace rlearner
