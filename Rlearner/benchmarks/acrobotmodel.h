// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cacrobotmodel_H_
#define Rlearner_cacrobotmodel_H_

#include "../transitionfunction.h"
#include "../rewardfunction.h"
#include "../ril_debug.h"

namespace rlearner {

class AcroBotModel: public LinearActionContinuousTimeTransitionFunction {
protected:
	virtual void doSimulationStep(State *state, RealType timestep,
	        Action *action, ActionData *data);

public:
	RealType _uMax;
	RealType _g;
	RealType _mass1;
	RealType _mass2;
	RealType _length1;
	RealType _length2;
	RealType _mu1;
	RealType _mu2;

public:
	virtual Matrix *getB(State *state);
	virtual ColumnVector *getA(State *state);
	virtual bool isFailedState(State *state);
	virtual void getResetState(State *state);

public:
	AcroBotModel(RealType dt, RealType uMax = 2, RealType length1 = 0.5,
	        RealType length2 = 0.5, RealType mass1 = 1.0, RealType mass2 = 1.0,
	        RealType mu1 = 0.05, RealType mu2 = 0.05, RealType g = 9.8);
	virtual ~AcroBotModel();
};

class AcroBotRewardFunction: public StateReward {
protected:
	AcroBotModel *_model;

public:
	RealType segmentFactor;
	bool useHeighPeak;
	RealType power;

public:
	virtual RealType getStateReward(State *state);
	virtual void getInputDerivation(State *modelState,
	        ColumnVector *targetState);

public:
	AcroBotRewardFunction& operator=(const AcroBotRewardFunction&) = delete;
	AcroBotRewardFunction(const AcroBotRewardFunction&) = delete;

	AcroBotRewardFunction(AcroBotModel *model, RealType segmentFactor = 0.5);
	virtual ~AcroBotRewardFunction() {}
};

class AcroBotHeightRewardFunction: public StateReward {
protected:
	AcroBotModel *_model;
	bool _useHeighPeak;

public:
	virtual RealType getStateReward(State *state);
	virtual void getInputDerivation(State *modelState,
	        ColumnVector *targetState);

public:
	AcroBotHeightRewardFunction& operator=(const AcroBotHeightRewardFunction&) = delete;
	AcroBotHeightRewardFunction(const AcroBotHeightRewardFunction&) = delete;

	AcroBotHeightRewardFunction(AcroBotModel *model);
	virtual ~AcroBotHeightRewardFunction() {}
};

class AcroBotVelocityRewardFunction: public StateReward {
protected:
	AcroBotModel *_model;

public:
	bool invertVelocity;

public:
	virtual RealType getStateReward(State *state);
	virtual void getInputDerivation(State *modelState,
	        ColumnVector *targetState);

public:
	AcroBotVelocityRewardFunction& operator=(const AcroBotVelocityRewardFunction&) = delete;
	AcroBotVelocityRewardFunction(const AcroBotVelocityRewardFunction&) = delete;

	AcroBotVelocityRewardFunction(AcroBotModel *model);
	virtual ~AcroBotVelocityRewardFunction() {}
};

class AcroBotExpRewardFunction: public StateReward {
protected:
	AcroBotModel *_model;

public:
	RealType expFactor;

public:
	virtual RealType getStateReward(State *state);
	virtual void getInputDerivation(State *modelState,
	        ColumnVector *targetState);

public:
	AcroBotExpRewardFunction& operator=(const AcroBotExpRewardFunction&) = delete;
	AcroBotExpRewardFunction(const AcroBotExpRewardFunction&) = delete;

	AcroBotExpRewardFunction(AcroBotModel *model, RealType expFactor = 10.0);
	virtual ~AcroBotExpRewardFunction() {}
};

} //namespace rlearner

#endif //Rlearner_cacrobotmodel_H_
