#ifndef Animus_Task_ShortcutMaze_H_
#define Animus_Task_ShortcutMaze_H_

#include "Task_GridWorld.h"

namespace rlearner {

/**
 * Implements a maze through which the agent is supposed to navigate to the
 * goal position. After several epochs, a shortcut in the maze is opened.
 * The ability of the agent to adapt in a non-staionary environment can
 * be tested this way.
 */
class Task_ShortcutMaze: public Task_GridWorld {
public:
	Task_ShortcutMaze();
	virtual ~Task_ShortcutMaze() = default;
};

} // namespace rlearner

#endif //Animus_Task_ShortcutMaze_H_
