#ifndef Example_RlearnerGridWorld_GridWorldModel_H_
#define Example_RlearnerGridWorld_GridWorldModel_H_

#include <cmath>
#include <vector>
#include <map>
#include <set>

#include "../transitionfunction.h"
#include "../rewardfunction.h"
#include "../agentcontroller.h"
#include "../discretizer.h"
#include "../action.h"

#include <iostream>
#include "GridWorld.h"

/*
 *TODO: SOMEHOW GET RID OF THIS AWWWWWWWWWWWFFUUUUUUULLLL MACROOOOOOOOOOOOOOOOOOOOOOOOOOOO!!!!!!!!!!!!!!!!!!!
 */

#define RLEARNER_GRIDWORLDACTION 16

namespace rlearner {

//******************************************************************************
//******************************GridWorldModel**********************************
//******************************************************************************

/**
 * @class GridWorldModel
 *
 * Implements a GridWorld-based TransitionFunction and RewardFunction.
 */

class GridWorldModel: public GridWorld,
        public TransitionFunction,
        public RewardFunction {
protected:
	unsigned int max_bounces;

	std::vector<std::pair<int, int> > _startPoints;
	std::map<char, RealType> *rewards;

	RealType reward_standard;
	RealType reward_success;
	RealType reward_bounce;

	bool is_parsed;
	virtual void parseGrid();

public:
	void setMaxBounces(unsigned int value);
	unsigned int getMaxBounces();

	void setRewardStandard(RealType value);
	void setRewardSuccess(RealType value);
	void setRewardBounce(RealType value);

	void setRewardForSymbol(char symbol, RealType reward);
	RealType getRewardForSymbol(char symbol);

	RealType getRewardStandard();
	RealType getRewardSuccess();
	RealType getRewardBounce();

	virtual void load(const char* filename);
	virtual void load(std::istream& stream);

	virtual void initGrid();
	virtual void setGridValue(unsigned int pos_x, unsigned int pos_y,
	        char value);
	virtual void addStartValue(char value);
	virtual void removeStartValue(char value);

	virtual void transitionFunction(State *oldstate, Action *action,
	        State *newState, ActionData *data = nullptr);

	virtual bool isResetState(State *state);
	virtual bool isFailedState(State *state);

	virtual void getResetState(State *resetState);

	virtual RealType getReward(StateCollection *oldState, Action *action,
	        StateCollection *newState);

public:
	GridWorldModel(unsigned int size_x, unsigned int size_y,
	        unsigned int max_bounces);
	GridWorldModel(std::istream& file, unsigned int max_bounces);
	GridWorldModel(const char* filename, unsigned int max_bounces);

	virtual ~GridWorldModel();
};

//******************************************************************************
//*************************GridWorld State Modifiers****************************
//******************************************************************************

class GridWorldState_Local4: public StateModifier {
protected:
	GridWorld* grid_world;
public:
	GridWorldState_Local4(GridWorld *grid_world);
	virtual ~GridWorldState_Local4();

	virtual void getModifiedState(StateCollection *originalState,
	        State *modifiedState);
};

class GridWorldState_Local4X: public StateModifier {
protected:
	GridWorld* grid_world;
public:
	GridWorldState_Local4X(GridWorld *grid_world);
	virtual ~GridWorldState_Local4X();

	virtual void getModifiedState(StateCollection *originalState,
	        State *modifiedState);
};

class GridWorldState_Local8: public StateModifier {
protected:
	GridWorld* grid_world;
public:
	GridWorldState_Local8(GridWorld *grid_world);
	virtual ~GridWorldState_Local8();

	virtual void getModifiedState(StateCollection *originalState,
	        State *modifiedState);
};

class GridWorldDiscreteState_Global: public AbstractStateDiscretizer {
protected:
	unsigned int size_x, size_y;

public:
	GridWorldDiscreteState_Global(unsigned int size_x, unsigned int size_y);
	virtual ~GridWorldDiscreteState_Global() {
	}

	virtual unsigned int getDiscreteStateNumber(StateCollection *state);
};

class GridWorldDiscreteState_Local: public AbstractStateDiscretizer {
protected:
	StateModifier* orig_state;
	std::map<char, short>* valuemap;

public:
	GridWorldDiscreteState_Local(StateModifier* orig_state,
	        unsigned int neigbourhood, std::set<char> *possible_values);
	virtual ~GridWorldDiscreteState_Local();

	virtual unsigned int getDiscreteStateNumber(StateCollection *state);
};

class GridWorldDiscreteState_SmallLocal: public AbstractStateDiscretizer {
protected:
	StateModifier* orig_state;
	GridWorld *gridworld;

public:
	GridWorldDiscreteState_SmallLocal(StateModifier* orig_state,
	        unsigned int neigbourhood, GridWorld *gridworld);
	virtual ~GridWorldDiscreteState_SmallLocal();

	virtual unsigned int getDiscreteStateNumber(StateCollection *state);
};

class GridWorldAction: public PrimitiveAction {
protected:
	int x_move, y_move;

public:
	int getXMove();
	int getYMove();

public:
	GridWorldAction(int x_move, int y_move);
	virtual ~GridWorldAction() = default;
};

class RaceTrackDiscreteState: public AbstractStateDiscretizer {
protected:
	StateModifier* orig_state;
	GridWorld *gridworld;

public:
	RaceTrackDiscreteState(StateModifier* orig_state,
	        unsigned int neigbourhood, GridWorld *gridworld);
	virtual ~RaceTrackDiscreteState();

	virtual unsigned int getDiscreteStateNumber(StateCollection *state);
};

class RaceTrack {
public:
	static void generateRaceTrack(GridWorld *gridworld, unsigned int width = 40,
	        unsigned int length = 200, unsigned int h_max = 5,
	        unsigned int dy_min = 1, unsigned int dy_max = 8);
};

} //namespace rlearner

#endif //Example_RlearnerGridWorld_GridWorldModel_H_
