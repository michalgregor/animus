#ifndef Example_RlearnerGridWorld_GridWorld_H_
#define Example_RlearnerGridWorld_GridWorld_H_

#include <iostream>
#include <vector>
#include <set>
#include <string>

#include <boost/multi_array.hpp>
#include "../system.h"

namespace rlearner {

class GridWorld {
protected:
	std::set<char> _startValues;
	std::set<char> _targetValues;
	std::set<char> _prohibitedValues;
	boost::multi_array<char, 2>* _grid;

private:
	static void loadList(const std::string& str, std::set<char>& set);
	static void writeList(std::ostream& stream, const std::set<char>& set);

public:
	virtual void load(const char* filename);
	virtual void load(std::istream& stream);

	virtual void save(const char* filename) const;
	virtual void save(std::ostream& stream) const;

	virtual void setGridValue(size_t posX, size_t posY, char value);
	char getGridValue(size_t posX, size_t posY) const;

	virtual void addStartValue(char value);
	virtual void removeStartValue(char value);
	//! Returns the list of start values.
	std::set<char>& getStartValues() {return _startValues;}
	//! Returns the list of start values.
	const std::set<char>& getStartValues() const {return _startValues;}

	virtual void addTargetValue(char value);
	virtual void removeTargetValue(char value);
	//! Returns the list of target values.
	std::set<char>& getTargetValues() {return _targetValues;}
	//! Returns the list of target values.
	const std::set<char>& getTargetValues() const {return _targetValues;}

	virtual void addProhibitedValue(char value);
	virtual void removeProhibitedValue(char value);
	//! Returns the list of prohibited values.
	std::set<char>& getProhibitedValues() {return _prohibitedValues;}
	//! Returns the list of prohibited values.
	const std::set<char>& getProhibitedValues() const {return _prohibitedValues;}

	/**
	 * Resizes the grid, preserving contents. If resized to a larger size, padded
	 * with default-initialized tile codes.
	 */
	void setSize(size_t sizeX, size_t sizeY) {
		_grid->resize(boost::extents[sizeX][sizeY]);
	}

	size_t getSizeX() const {return _grid->shape()[0];}
	size_t getSizeY() const {return _grid->shape()[1];}

	//! Returns the total number of tiles in the grid world (equals getSizeX()*getSizeY()).
	size_t size() const {return _grid->num_elements();}

	std::set<char> getUsedValues() const;

	//! Erases all elements. The grid becomes of size 0x0.
	void clear() {_grid->resize(boost::extents[0][0]);}

	//! Swaps contents of both GridWorlds.
	void swap(GridWorld& gridWorld) {
		_startValues.swap(gridWorld._startValues);
		_targetValues.swap(gridWorld._targetValues);
		_prohibitedValues.swap(gridWorld._prohibitedValues);

		boost::multi_array<char, 2>* tmpGridPtr = _grid;
		_grid = gridWorld._grid;
		gridWorld._grid = tmpGridPtr;
	}

	/**
	 * Constructs a GridWorld from the specified file.
	 */

	GridWorld(const char* filename):
		_startValues(), _targetValues(), _prohibitedValues(), _grid(new boost::multi_array<char, 2>)
	{
		load(filename);
	}

	/**
	 * Constructs a GridWorld from a stream (a file, ...).
	 */

	GridWorld(std::istream& stream):
		_startValues(), _targetValues(), _prohibitedValues(), _grid(new boost::multi_array<char, 2>)
	{
		load(stream);
	}

	/**
	 * Cosntructs a Gridworld of size sizeX x sizeY with default-initialized tile
	 * codes.
	 */

	GridWorld(size_t sizeX, size_t sizeY) :
		_startValues(), _targetValues(), _prohibitedValues(),
		_grid(new boost::multi_array<char, 2>(boost::extents[sizeX][sizeY])) {
	}

	virtual ~GridWorld() {
		delete _grid;
	}
};

} //namespace rlearner

#endif //Example_RlearnerGridWorld_GridWorld_H_
