#include <fstream>
#include <cstdio>
#include <sstream>

#include "GridWorld.h"
#include <Systematic/utils/DataFile.h>

namespace rlearner {

//! Loads characters from a comma-separated list and inserts them into the
//! specified set.
void GridWorld::loadList(const std::string& str, std::set<char>& set) {
	std::stringstream stream(str);
	char c;

	while(stream.good()) {
		stream >> c;
		if(!std::isspace(c) && c != ',') {
			set.insert(c);
		}
	}
}

//! Writes characters from the specified set into the stream in the form of
//! a comma-separated list.
void GridWorld::writeList(std::ostream& stream, const std::set<char>& set) {
	std::set<char>::iterator iter = set.begin();
	if(iter == set.end()) return;
	else {
		stream << *iter;
		++iter;
	}

	for(; iter != set.end(); iter++) {
		stream << ", " << *iter;
	}
}

/**
 * Load the GridWorld from the specified file.
 */

void GridWorld::load(const char* filename) {
	std::ifstream stream(filename);
	if(!stream.good()) throw TracedError_FileError(std::string("Cannot open file '") + filename + "'.");
	load(stream);
}

/**
 * Load the GridWorld from the specified stream.
 *
 * \note If size is specified in the GridWorld file, a grid world of that size
 * is created and its tiles initialised with those provided in the file. If more
 * tile codes are provided than there are tiles, the rest of the tile codes is
 * ignored. If less tile codes are provided, the rest of the tiles are
 * default-initialized using char().
 *
 * \exception TracedError_InvalidFormat An exception is thrown if data in the
 * stream is not of the proper GridWorld format.
 */

void GridWorld::load(std::istream& stream) {
	std::set<char> startValues;
	std::set<char> targetValues;
	std::set<char> prohibitedValues;

	typedef DataFile::OptionContainer::iterator optiter;
	unsigned int sizeX = 0, sizeY = 0;
	bool sizeSet = false;

	DataFile file;
	stream >> file;

	if(file.header() != "Gridworld") throw TracedError_InvalidFormat(
	    "Header \"Gridworld\" expected. Got " + file.header() + " instead.");

	for(optiter iter = file.options().begin(); iter != file.options().end();
	    iter++) {
		if(iter->first == "Size") {
			if(sscanf(iter->second.c_str(), "%ux%u", &sizeY, &sizeX) != 2) throw TracedError_InvalidFormat(
			    "Size is of an invalid format: should be written as 10x10.");
			sizeSet = true;
		} else if(iter->first == "StartValues") {
			loadList(iter->second, startValues);
		} else if(iter->first == "ProhibitedValues") {
			loadList(iter->second, prohibitedValues);
		} else if(iter->first == "TargetValues") {
			loadList(iter->second, targetValues);
		} else throw TracedError_InvalidFormat("Unknown option.");
	}

	if(file.data().size()) {
		// if size not set, set number of columns to the length of the first
		// data line and set number of rows to num_of_data / num_of_columns.
		if(!sizeSet) {
			sizeY = file.data().front().size();
			sizeX = 0;

			for(size_t i = 0; i < file.data().size(); i++) {
				sizeX += file.data()[i].size();
			}

			sizeX = (sizeX - 1) / sizeY + 1;
		}

		_grid->resize(boost::extents[sizeX][sizeY]);

		size_t gridPos = 0;

		for(size_t i = 0; i < file.data().size(); i++) {
			for(size_t j = 0; j < file.data()[i].size(); j++, gridPos++) {
				//once the grid has been filled, leave the loops
				if(gridPos >= _grid->num_elements()) goto gridFilled;
				size_t gridX = gridPos / _grid->shape()[1], gridY = gridPos
				    % _grid->shape()[1];
				(*_grid)[gridX][gridY] = file.data()[i][j];
			}
		}

		gridFilled: ;

	} else {
		_grid->resize(boost::extents[sizeX][sizeY]);
	}

	_startValues.swap(startValues);
	_targetValues.swap(targetValues);
	_prohibitedValues.swap(prohibitedValues);
}

void GridWorld::save(const char* filename) const {
	std::ofstream stream(filename);
	save(stream);
}

void GridWorld::save(std::ostream& stream) const {
	stream << "Gridworld" << std::endl << std::endl;

	stream << "Size: " << getSizeX() << "x" << getSizeY() << std::endl;
	stream << "StartValues: ";
	writeList(stream, _startValues);
	stream << std::endl;
	stream << "ProhibitedValues: ";
	writeList(stream, _prohibitedValues);
	stream << std::endl;
	stream << "TargetValues: ";
	writeList(stream, _targetValues);
	stream << std::endl;

	stream << std::endl;

	size_t sizeX = _grid->shape()[0];
	size_t sizeY = _grid->shape()[1];

	for(size_t i = 0; i < sizeX; i++) {
		for(size_t j = 0; j < sizeY; j++) {
			stream << (*_grid)[i][j];
		}
		stream << std::endl;
	}
}

/**
 * Sets the value at position grid[posX][posY] to value.
 */

void GridWorld::setGridValue(size_t posX, size_t posY, char value) {
	(*_grid)[posX][posY] = value;
}

/**
 * Returns the value of tile at position grid[posX][posY].
 */

char GridWorld::getGridValue(size_t posX, size_t posY) const {
	return (*_grid)[posX][posY];
}

/**
 * Adds the specified tile code to the list of start values.
 */

void GridWorld::addStartValue(char value) {
	_startValues.insert(value);
}

/**
 * Removes the specified tile code to the list of start values.
 */

void GridWorld::removeStartValue(char value) {
	_startValues.erase(value);
}

/**
 * Adds the specified tile code to the list of target values.
 */

void GridWorld::addTargetValue(char value) {
	_targetValues.insert(value);
}

/**
 * Removes the specified tile code to the list of target values.
 */

void GridWorld::removeTargetValue(char value) {
	_targetValues.erase(value);
}

/**
 * Adds the specified tile code to the list of prohibited values.
 */

void GridWorld::addProhibitedValue(char value) {
	_prohibitedValues.insert(value);
}

/**
 * Removes the specified tile code to the list of prohibited values.
 */

void GridWorld::removeProhibitedValue(char value) {
	_prohibitedValues.erase(value);
}

/**
 * Returns a set of all tile codes used in the GridWorld.
 *
 * \todo It might be better to cache this list, or keep track of this while
 * setting tile codes.
 */

std::set<char> GridWorld::getUsedValues() const {
	std::set<char> values;

	size_t sizeX = _grid->shape()[0];
	size_t sizeY = _grid->shape()[1];

	for(size_t i = 0; i < sizeX; i++) {
		for(size_t j = 0; j < sizeY; j++) {
			values.insert((*_grid)[i][j]);
		}
	}

	return values;
}

} //namespace rlearner
