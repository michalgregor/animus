#include "Task_DeepSea.h"
#include <sstream>

namespace rlearner {

RealType Task_DeepSea_TimeReward::getReward(
    StateCollection*, Action*, StateCollection*) {
	return -1;
}

const std::string& Task_DeepSea::getMap() {
	static std::string map = "Gridworld\n"
		"Size: 10x11\n"
		"StartValues: S\n"
		"ProhibitedValues: X\n"
		"S.........\n"
		"A.........\n"
		"XB........\n"
		"XXC.......\n"
		"XXXDEF....\n"
		"XXXXXX....\n"
		"XXXXXX....\n"
		"XXXXXXGH..\n"
		"XXXXXXXX..\n"
		"XXXXXXXXI.\n"
		"XXXXXXXXXJ\n";

	return map;
}

ActionSet* Task_DeepSea::newActionSet() {
	ActionSet* actionSet = new ActionSet;

	actionSet->add(new GridWorldAction(-1, 0)); //left 0
	actionSet->add(new GridWorldAction(1, 0)); //right 1
	actionSet->add(new GridWorldAction(0, -1)); //up 2
	actionSet->add(new GridWorldAction(0, 1)); //down 3

	return actionSet;
}

RealType Task_DeepSea::getSymbolReward(char symbol) {
	static std::vector<RealType> rewards = { 1, 2, 3, 5, 8, 16, 24, 50, 74, 124 };

	if(symbol < 'A' || symbol > 'J') return 0;
	return rewards[symbol - 'A'];
}

void Task_DeepSea::getResetState(State* resetState) {
	_model->getResetState(resetState);
	resetState->setDiscreteState(_statePos, 0);
}

/**
 * Returns the reward function associated with the task. If there is
 * a main reward function, this should be at index 0.
 *
 * \exception TracedError_OutOfRange Throws when index >= numRewardFunctions().
 */
RewardFunction* Task_DeepSea::getRewardFunction(unsigned int index) {
	if(index == 0) return this;
	else if(index == 1) return _timeReward.get();
	else throw TracedError_OutOfRange("Reward function index out of range.");
}

/**
 * Returns the number of reward functions associated with the task.
 */
unsigned int Task_DeepSea::numRewardFunctions() const {
	return 2;
}

TransitionFunction* Task_DeepSea::getTransitionFunction() {
	return this;
}

bool Task_DeepSea::isResetState(State* state) {
	return (state->getDiscreteState(_statePos));
}

void Task_DeepSea::transitionFunction(
    State* oldState, Action* action_, State* newState, ActionData* data) {
	int treasureState = oldState->getDiscreteState(_statePos);

	_model->transitionFunction(oldState, action_, newState, data);
	oldState->setDiscreteState(_statePos, treasureState);
	newState->setDiscreteState(_statePos, treasureState);

	int x = newState->getState()->getDiscreteState(0), y =
	    newState->getState()->getDiscreteState(1);
	char symbol = _model->getGridValue(x, y);

	if(symbol >= 'A' && symbol <= 'J') {
		treasureState |= (1 << (symbol - 'A'));
		newState->setDiscreteState(_statePos, treasureState);
	}
}

RealType Task_DeepSea::getReward(
    StateCollection* oldState, Action* action, StateCollection* newState) {
	int x = newState->getState()->getDiscreteState(0), y =
	    newState->getState()->getDiscreteState(1);

	char symbol = _model->getGridValue(x, y);

	if(symbol >= 'A' && symbol <= 'J') {
		int treasureState = oldState->getState()->getDiscreteState(_statePos);
		if(!((1 << (symbol - 'A')) & treasureState)) return getSymbolReward(
		    symbol);
	}

	return 0;
}

/**
 * Creates a new discretizer for the state space of the task.
 */
AbstractStateDiscretizer* Task_DeepSea::newDiscretizer() {
	return new GridWorldDiscreteState_Global(_model->getSizeX(),
	    _model->getSizeY());
}

/**
 * Returns the TransitionFunctionEnvironment for the task. This is
 * owned by the task.
 */
EnvironmentModel* Task_DeepSea::getEnvironment() {
	return _environment.get();
}

/**
 * Creates a new ActionEncoder able to encode actions of the type used
 * by the Task.
 */
ActionEncoder* Task_DeepSea::newActionEncoder() {
	return nullptr;
}

Task_DeepSea::Task_DeepSea(const shared_ptr<GridWorldModel>& model) :
	    TransitionFunction(model->getStateProperties(), new ActionSet()),
	    _model(model),
	    _environment(),
	    _timeReward(new Task_DeepSea_TimeReward) {
	_model->setRewardBounce(0);
	_model->setRewardStandard(0);

	_statePos = properties->getNumDiscreteStates();
	properties->setNumDiscreteStates(_statePos + 1);

	_environment = make_shared<TransitionFunctionEnvironment>(this);
}

Task_DeepSea::Task_DeepSea(unsigned int maxBounces) :
	    Task_DeepSea(
	        [&maxBounces]() {std::stringstream file(getMap()); return systematic::make_shared<GridWorldModel>(file, maxBounces);}()) {
}

/**
 * Virtual destructor.
 */
Task_DeepSea::~Task_DeepSea() {
}

} //namespace rlearner
