#include "Task_SingleProduct.h"
#include "ActionEncoder_ManuAction.h"

namespace rlearner {
namespace rlearner_singleproduct {

/**
 * Creates a new StateProperties object (allocated using new) and returns
 * a pointer to it.
 */
StateProperties* ManuState::newProperties() {
	StateProperties* prop = new StateProperties(0, 3);

	prop->setDiscreteStateSize(0, 5 + 1);
	prop->setDiscreteStateSize(1, 2);

	return prop;
}

void ManuState::initState(State* state) {
	state->setDiscreteState(0, 0);
	state->setDiscreteState(1, static_cast<int>(Location::WAREHOUSE));
	state->setDiscreteState(2, 0);
}

void ManuAction::serialize(IArchive& ar, const unsigned int /*version*/) {
	ar & boost::serialization::base_object<PrimitiveAction>(*this);
	ar & _actionType;
}

void ManuAction::serialize(OArchive& ar, const unsigned int /*version*/) {
	ar & boost::serialization::base_object<PrimitiveAction>(*this);
	ar & _actionType;
}

} //namespace rlearner_singleproduct

void Task_SingleProduct::getResetState(State *resetState) {
	ManuState::initState(resetState);
}

/**
 * Returns the reward function associated with the task. If there is
 * a main reward function, this should be at index 0.
 *
 * \exception TracedError_OutOfRange Throws when index >= numRewardFunctions().
 */
RewardFunction* Task_SingleProduct::getRewardFunction(unsigned int index) {
	if(index > 0) throw TracedError_OutOfRange("Only one reward function is associated with this task.");
	return this;
}

/**
 * Returns the number of reward functions associated with the task.
 */
unsigned int Task_SingleProduct::numRewardFunctions() const {
	return 1;
}

TransitionFunction* Task_SingleProduct::getTransitionFunction() {
	return this;
}

void Task_SingleProduct::transitionFunction(State *oldstate, Action *action_,
        State *newState, ActionData* UNUSED(data)) {
	newState->setState(oldstate);

	ManuState oldManuState(oldstate);
	ManuState newManuState(newState);

	ManuAction* action = dynamic_cast<ManuAction*>(action_);

	switch (action->getActionType()) {
	case ActionType::MOVE_TO_WAREHOUSE:
		newManuState.setLocation(Location::WAREHOUSE);
	break;
	case ActionType::MOVE_TO_STORE:
		newManuState.setLocation(Location::STORE);
	break;
	case ActionType::GET_MATERIAL: {
		if (Location(oldstate->getDiscreteState(1)) == Location::STORE) {
			unsigned int material = oldManuState.getNumMaterial();
			if (material < CAPACITY) newManuState.setNumMaterial(material + 1);
		}
	}
	break;
	case ActionType::CREATE_PRODUCT: {
		if (Location(oldstate->getDiscreteState(1)) == Location::WAREHOUSE) {
			unsigned int material = oldManuState.getNumMaterial();
			if (material >= 1) {
				newManuState.setNumProducts(oldManuState.getNumProducts() + 1);
				newManuState.setNumMaterial(material - 1);
			}
		}
	}
	break;
	default:
	break;
	}
}

RealType Task_SingleProduct::getReward(StateCollection *oldState, Action*,
        StateCollection *newState) {
	ManuState oldManuState(oldState->getState());
	ManuState newManuState(newState->getState());

	RealType reward = std::max(
	        newManuState.getNumProducts() - oldManuState.getNumProducts(), 0u);

//	if(reward <= 0) reward -= 0.75;

	return reward;
}

/**
 * Creates a new discretizer for the state space of the task.
 */
AbstractStateDiscretizer* Task_SingleProduct::newDiscretizer() {
	return new ModelStateDiscretizer(properties, {0, 1});
}

/**
 * Returns the TransitionFunctionEnvironment for the task. This is
 * owned by the task.
 */
EnvironmentModel* Task_SingleProduct::getEnvironment() {
	return _environment.get();
}

/**
 * Creates a new ActionEncoder able to encode actions of the type used
 * by the Task.
 */
ActionEncoder* Task_SingleProduct::newActionEncoder() {
	return new ActionEncoder_ManuAction<Task_SingleProduct::ManuAction>();
}

Task_SingleProduct::Task_SingleProduct():
	TransitionFunction(ManuState::newProperties(), new ActionSet()), _environment()
{
	_environment = make_shared<TransitionFunctionEnvironment>(this);
}

/**
 * Virtual destructor.
 */
Task_SingleProduct::~Task_SingleProduct() {
	delete properties;
}

} //namespace rlearner
