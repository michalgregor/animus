#ifndef Example_RlearnerStateChart_ActionEncoder_ManuAction_H_
#define Example_RlearnerStateChart_ActionEncoder_ManuAction_H_

#include "../system.h"
#include "../ActionEncoder.h"

namespace rlearner {

/**
* @class ActionEncoder_ManuAction
**/
template<class ManuAction>
class ActionEncoder_ManuAction: public ActionEncoder {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function
	**/
	void serialize(IArchive& ar, const unsigned int /*version*/) {
		ar & boost::serialization::base_object<ActionEncoder>(*this);
	}

	void serialize(OArchive& ar, const unsigned int /*version*/) {
		ar & boost::serialization::base_object<ActionEncoder>(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	virtual size_t size() const;
	virtual std::vector<RealType> operator()(const Action* action);

public:
	virtual ~ActionEncoder_ManuAction() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
 * Returns the size of the vector used to represent the action.
 */
template<class ManuAction>
size_t ActionEncoder_ManuAction<ManuAction>::size() const {
	return 1;
}

/**
 * Encodes the provided Action object using a vector of RealType numbers.
 */
template<class ManuAction>
std::vector<RealType> ActionEncoder_ManuAction<ManuAction>::operator()(const Action* action) {
	return std::vector<RealType>(1, static_cast<RealType>(dynamic_cast<const ManuAction*>(action)->getActionType()));
}

}//namespace rlearner

SYS_EXPORT_TEMPLATE(rlearner::ActionEncoder_ManuAction, 1)

#endif //Example_RlearnerStateChart_ActionEncoder_ManuAction_H_
