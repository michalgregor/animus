#ifndef Rlearner_Task_GridWorld_H_
#define Rlearner_Task_GridWorld_H_

#include "../system.h"
#include "../Task.h"
#include "GridWorldModel.h"

namespace rlearner {

/**
 * @class Task_GridWorld
 * @author Michal Gregor
 *
 * Implements a GridWorldModel-based task.
 *
 * Other classes may derive from this and initialize the GridWorldModel
 * in different ways and/or with different maps.
 *
 * The class may also be used as-is.
 */
class Task_GridWorld: public Task {
private:
	shared_ptr<GridWorldModel> _environment;
	shared_ptr<TransitionFunctionEnvironment> _model;

public:
	//! Provides 4 actions for moving: up, down, left and right.
	virtual ActionSet* newActionSet();

public:
	virtual RewardFunction* getRewardFunction(unsigned int index = 0);
	virtual unsigned int numRewardFunctions() const;

	virtual TransitionFunction* getTransitionFunction();
	virtual AbstractStateDiscretizer* newDiscretizer();

	virtual EnvironmentModel* getEnvironment();
	virtual ActionEncoder* newActionEncoder();

	GridWorldModel& getGridWorldModel() {return *_environment;}
	const GridWorldModel& getGridWorldModel() const {return *_environment;}
	void setGridWorldModel(const shared_ptr<GridWorldModel>& model);

public:
	Task_GridWorld& operator=(const Task_GridWorld&) = delete;
	Task_GridWorld(const Task_GridWorld&) = delete;

	Task_GridWorld();
	Task_GridWorld(const shared_ptr<GridWorldModel>& model);
	virtual ~Task_GridWorld() = default;
};

} // namespace rlearner

#endif //Rlearner_Task_GridWorld_H_
