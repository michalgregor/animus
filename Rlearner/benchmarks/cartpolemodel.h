// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ccartpolemodel_H_
#define Rlearner_ccartpolemodel_H_

#include "../transitionfunction.h"
#include "../rewardfunction.h"

namespace rlearner {

class CartPoleModel: public LinearActionContinuousTimeTransitionFunction {
protected:
	virtual void doSimulationStep(
	    State *state,
	    RealType timestep,
	    Action *action,
	    ActionData *data);

public:
	RealType uMax;
	RealType lengthTrack;
	RealType g;
	RealType massCart;
	RealType massPole;
	RealType lengthPole;
	RealType mu_c; // friction cart
	RealType mu_p; // friction pole

	bool endLeaveTrack;
	bool endOverRotate;

	RealType failedReward;

public:
	virtual Matrix *getB(State *state);
	virtual ColumnVector *getA(State *state);

	virtual bool isFailedState(State *state);
	virtual void getResetState(State *state);

public:
	CartPoleModel(
	    RealType dt,
	    RealType uMax = 10,
	    RealType lengthTrack = 4.8,
	    RealType lengthPole = 0.5,
	    RealType massCart = 1.0,
	    RealType massPole = 0.5,
	    RealType mu_c = 1.0,
	    RealType mu_p = 0.1,
	    RealType g = 9.8,
	    bool endLeaveTrack = true,
	    bool endOverRotate = true);

	virtual ~CartPoleModel();
};

class CartPoleRewardFunction: public StateReward {
protected:
	CartPoleModel *cartpoleModel;

public:
	bool useHeighPeak;
	bool punishOverRotate;

public:
	virtual RealType getStateReward(State *state);
	virtual void getInputDerivation(
	    State *modelState,
	    ColumnVector *targetState);

public:
	CartPoleRewardFunction(CartPoleModel *model);
	virtual ~CartPoleRewardFunction() {}
};

class CartPoleHeightRewardFunction: public StateReward {
protected:
	CartPoleModel *cartpoleModel;

public:
	virtual RealType getStateReward(State *state);
	virtual void getInputDerivation(
	    State *modelState,
	    ColumnVector *targetState);

public:
	CartPoleHeightRewardFunction(CartPoleModel *model);
};

} //namespace rlearner

#endif //Rlearner_ccartpolemodel_H_
