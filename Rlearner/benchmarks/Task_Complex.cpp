#include "Task_Complex.h"
#include "ActionEncoder_ManuAction.h"
#include <random>

namespace rlearner {
using namespace rlearner_complex;

namespace rlearner_complex {

const unsigned int CAPACITY = 5;
const unsigned int num_locations = 5;
const unsigned int capacity_products = 5;
const unsigned int capacity_processors = 5;
const unsigned int capacity_water = 5;
const unsigned int capacity_planks = 5;

void ManuAction::serialize(IArchive& ar, const unsigned int /*version*/) {
	ar & boost::serialization::base_object<PrimitiveAction>(*this);
	ar & _actionType;
}

void ManuAction::serialize(OArchive& ar, const unsigned int /*version*/) {
	ar & boost::serialization::base_object<PrimitiveAction>(*this);
	ar & _actionType;
}

/**
 * Creates a new StateProperties object (allocated using new) and returns
 * a pointer to it.
 */
StateProperties* ManuState::newProperties() {
	StateProperties* prop = new StateProperties(0, 6);

	prop->setDiscreteStateSize(0, CAPACITY + 1);
	prop->setDiscreteStateSize(1, num_locations + 1);
//		prop->setDiscreteStateSize(2, capacity_products + 1);
	prop->setDiscreteStateSize(3, capacity_processors + 1);
	prop->setDiscreteStateSize(4, capacity_water + 1);
	prop->setDiscreteStateSize(5, capacity_planks + 1);

	return prop;
}

} //rlearner_complex

void Task_Complex::setStateInitializer(
    const shared_ptr<StateInitializer>& initializer) {
	_stateInitializer = initializer;
}

const shared_ptr<StateInitializer>& Task_Complex::getStateInitializer() const {
	return _stateInitializer;
}

/**
 * Returns the reward function associated with the task. If there is
 * a main reward function, this should be at index 0.
 *
 * \exception TracedError_OutOfRange Throws when index >= numRewardFunctions().
 */
RewardFunction* Task_Complex::getRewardFunction(unsigned int index) {
	if(index > 0) throw TracedError_OutOfRange(
	    "Only one reward function is associated with this task.");
	return this;
}

/**
 * Returns the number of reward functions associated with the task.
 */
unsigned int Task_Complex::numRewardFunctions() const {
	return 1;
}

TransitionFunction* Task_Complex::getTransitionFunction() {
	return this;
}

void Task_Complex::getResetState(State* resetState) {
	_stateInitializer->initialize(resetState);
}

void Task_Complex::transitionFunction(
    State *oldstate, Action *action_, State *newState,
    ActionData* UNUSED(data)) {
	newState->setState(oldstate);

	ManuState oldManuState(oldstate);
	ManuState newManuState(newState);

	ManuAction* action = dynamic_cast<ManuAction*>(action_);

	switch(action->getActionType()) {
	case ActionType::MOVE_TO_WAREHOUSE:
		newManuState.setLocation(Location::WAREHOUSE);
	break;
	case ActionType::MOVE_TO_STORE:
		newManuState.setLocation(Location::STORE);
	break;
	case ActionType::GET_MATERIAL: {
		if(Location(oldstate->getDiscreteState(1)) == Location::STORE) {
			unsigned int material = oldManuState.getNumMaterial();
			if(material < CAPACITY) newManuState.setNumMaterial(material + 1);
		}
	}
	break;
	case ActionType::CREATE_PRODUCT: {
		if(Location(oldstate->getDiscreteState(1)) == Location::WAREHOUSE) {
			unsigned int material = oldManuState.getNumMaterial();
			unsigned int planks = newManuState.getNumPlanks();
//			unsigned int water = newManuState.getNumWater();

			if(material >= 1 && planks >= 1) {
				newManuState.setNumProducts(oldManuState.getNumProducts() + 1);
				newManuState.setNumMaterial(material - 1);
				newManuState.setNumPlanks(planks - 1);
//				newManuState.setNumWater(water - 1);
			}
		}
	}
	break;

		//new actions

	case ActionType::TAKE_PLANKS: {
		if(oldManuState.getLocation() == Location::STORE) {
			unsigned int planks = newManuState.getNumPlanks();
			if(planks < capacity_planks) {
				newManuState.setNumPlanks(planks + 1);
			}
		}
	}
	break;
	case ActionType::DROP_PLANKS: {
		unsigned int planks = newManuState.getNumPlanks();
		if(planks > 0) {
			newManuState.setNumPlanks(planks - 1);
		}
	}
	break;
	case ActionType::GET_WATER: {
		if(oldManuState.getLocation() == Location::GREENHOUSE) {
			unsigned int water = newManuState.getNumWater();
			if(water < capacity_water) {
				newManuState.setNumWater(water + 1);
			}
		}
	}
	break;
	case ActionType::WATER_PLANTS: {
		if(oldManuState.getLocation() == Location::GREENHOUSE) {
			unsigned int water = newManuState.getNumWater();
			if(water > 0) {
				newManuState.setNumWater(water - 1);
			}
		}
	}
	break;
	case ActionType::TAKE_PROCESSOR: {
		if(oldManuState.getLocation() == Location::PCCENTER) {
			unsigned int processors = newManuState.getNumProcessors();
			if(processors < capacity_processors) {
				newManuState.setNumProcessors(processors + 1);
			}
		}
	}
	break;
	case ActionType::REPLACE_PROCESSOR: {
		if(oldManuState.getLocation() == Location::PCCENTER) {
			unsigned int processors = newManuState.getNumProcessors();
			if(processors > 0) {
				newManuState.setNumProcessors(processors - 1);
			}
		}
	}
	break;
	case ActionType::MOVE_TO_GARAGE:
		newManuState.setLocation(Location::GARAGE);
	break;
	case ActionType::MOVE_TO_GREENHOUSE:
		newManuState.setLocation(Location::GREENHOUSE);
	break;
	case ActionType::MOVE_TO_PCCENTER:
		newManuState.setLocation(Location::PCCENTER);
	break;
	case ActionType::MOVE_TO_SERVERROOM:
		newManuState.setLocation(Location::SERVERROOM);
	break;
	default:
	break;
	}
}

RealType Task_Complex::getReward(
    StateCollection *oldState, Action*, StateCollection *newState) {
	ManuState oldManuState(oldState->getState());
	ManuState newManuState(newState->getState());

	RealType reward = std::max(
	    newManuState.getNumProducts() - oldManuState.getNumProducts(), 0u);

//	if(reward <= 0) reward -= 0.75;

	return reward;
}

/**
 * Creates a new discretizer for the state space of the task.
 */
AbstractStateDiscretizer* Task_Complex::newDiscretizer() {
	return new ModelStateDiscretizer(properties, {0, 1, 3, 4, 5});
}

/**
 * Returns the TransitionFunctionEnvironment for the task. This is
 * owned by the task.
 */
EnvironmentModel* Task_Complex::getEnvironment() {
	return _environment.get();
}

/**
 * Creates a new ActionEncoder able to encode actions of the type used
 * by the Task.
 */
ActionEncoder* Task_Complex::newActionEncoder() {
	return new ActionEncoder_ManuAction<Task_Complex::ManuAction>();
}

Task_Complex::Task_Complex() :
	    TransitionFunction(ManuState::newProperties(), new ActionSet()),
	    _environment(),
	    _stateInitializer(
	        new StateInitializer_Constant(
	            std::vector<int>(properties->getNumDiscreteStates(), 0),
	            std::vector<RealType>(properties->getNumContinuousStates(), 0))) {
	_environment = make_shared<TransitionFunctionEnvironment>(this);
}

/**
 * Virtual destructor.
 */
Task_Complex::~Task_Complex() {
	delete properties;
}

} //namespace rlearner
