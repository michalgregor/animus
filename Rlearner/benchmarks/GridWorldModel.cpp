// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <string>
#include <stdexcept>
#include <cstring>

#include "../ril_debug.h"
#include "GridWorldModel.h"

#include "../state.h"
#include "../stateproperties.h"
#include "../statecollection.h"
#include "../system.h"

namespace rlearner {

GridWorldModel::GridWorldModel(const char* filename, unsigned int max_bounces_):
	    GridWorld(filename),
	    TransitionFunction(new StateProperties(0, 3), new ActionSet()),
	    max_bounces(max_bounces_),
	    _startPoints(),
	    reward_standard(-1.0),
	    reward_bounce(-10),
	    reward_success(100.0),
	    is_parsed(false),
	    rewards(new std::map<char, RealType>()) {
	properties->setDiscreteStateSize(0, getSizeX());
	properties->setDiscreteStateSize(1, getSizeY());

	if(max_bounces == (unsigned int) -1) properties->setDiscreteStateSize(2,
	    (unsigned int) -1);
	else properties->setDiscreteStateSize(2, max_bounces + 2);
}

GridWorldModel::GridWorldModel(std::istream& file, unsigned int max_bounces_):
	    GridWorld(file),
	    TransitionFunction(new StateProperties(0, 3), new ActionSet()),
	    max_bounces(max_bounces_),
	    _startPoints(),
	    reward_standard(-1.0),
	    reward_bounce(-10),
	    reward_success(100.0),
	    is_parsed(false),
	    rewards(new std::map<char, RealType>()) {
	properties->setDiscreteStateSize(0, getSizeX());
	properties->setDiscreteStateSize(1, getSizeY());

	if(max_bounces == (unsigned int) -1) properties->setDiscreteStateSize(2,
	    (unsigned int) -1);
	else properties->setDiscreteStateSize(2, max_bounces + 2);
}

GridWorldModel::GridWorldModel(
    unsigned int size_x, unsigned int size_y, unsigned int max_bounces_):
	    GridWorld(size_x, size_y),
	    TransitionFunction(new StateProperties(0, 3), new ActionSet()),
	    max_bounces(max_bounces_),
	    _startPoints(),
	    reward_standard(-1.0),
	    reward_bounce(-10),
	    reward_success(100.0),
	    is_parsed(false),
	    rewards(new std::map<char, RealType>()) {
	properties->setDiscreteStateSize(0, getSizeX());
	properties->setDiscreteStateSize(1, getSizeY());

	if(max_bounces == (unsigned int) -1) properties->setDiscreteStateSize(2,
	    (unsigned int) -1);
	else properties->setDiscreteStateSize(2, max_bounces + 2);
}

GridWorldModel::~GridWorldModel() {
	delete properties;
	delete actions;
	delete rewards;
}

void GridWorldModel::setMaxBounces(unsigned int value) {
	this->max_bounces = value;
	if(max_bounces == (unsigned int) -1) properties->setDiscreteStateSize(2,
	    (unsigned int) -1);
	else properties->setDiscreteStateSize(2, max_bounces + 2);
}

unsigned int GridWorldModel::getMaxBounces() {
	return max_bounces;
}

void GridWorldModel::setRewardStandard(RealType value) {
	this->reward_standard = value;

}

RealType GridWorldModel::getRewardStandard() {
	return this->reward_standard;
}

void GridWorldModel::setRewardSuccess(RealType value) {
	this->reward_success = value;

}

RealType GridWorldModel::getRewardSuccess() {
	return this->reward_success;
}

void GridWorldModel::setRewardBounce(RealType value) {
	reward_bounce = value;
}

RealType GridWorldModel::getRewardBounce() {
	return reward_bounce;
}

void GridWorldModel::setRewardForSymbol(char symbol, RealType reward) {
	(*rewards)[symbol] = reward;
}

RealType GridWorldModel::getRewardForSymbol(char symbol) {
	std::map<char, RealType>::iterator it = rewards->find(symbol);
	RealType rew = 0.0;

	if(it != rewards->end()) {
		rew = (*it).second;
	} else {
		if(_targetValues.find(symbol) != _targetValues.end()) {
			rew = reward_success;
		} else {
			rew = reward_standard;
		}
	}
	return rew;
}

void GridWorldModel::parseGrid() {
	if(size() && !is_parsed) {
		_startPoints.clear();
		for(size_t j = 0; j < getSizeY(); j++) {
			for(size_t i = 0; i < getSizeX(); i++) {
				if(_startValues.find(getGridValue(i, j)) != _startValues.end()) _startPoints.push_back(
				    std::pair<int, int>(j, i));
			}
		}
	}
	is_parsed = true;
}

/**
 * Opens the specified file and load the contained GridWorld.
 */

void GridWorldModel::load(const char* filename) {
	GridWorld::load(filename);
	is_parsed = false;
}

/**
 * Loads the GridWorld contained in the specified stream.
 */

void GridWorldModel::load(std::istream& stream) {
	GridWorld::load(stream);
	is_parsed = false;
}

void GridWorldModel::initGrid() {
	is_parsed = false;
}

void GridWorldModel::setGridValue(
    unsigned int pos_x, unsigned int pos_y, char value) {
	GridWorld::setGridValue(pos_x, pos_y, value);
	is_parsed = false;
}

void GridWorldModel::addStartValue(char value) {
	GridWorld::addStartValue(value);
	is_parsed = false;
}

void GridWorldModel::removeStartValue(char value) {
	GridWorld::removeStartValue(value);
	is_parsed = false;
}

void GridWorldModel::transitionFunction(
    State *oldstate, Action *action, State *newState, ActionData *) {
	int pos_x = oldstate->getDiscreteState(0);
	int pos_y = oldstate->getDiscreteState(1);
	int actual_bounces = oldstate->getDiscreteState(2);

	GridWorldAction* gridAction = dynamic_cast<GridWorldAction*>(action);

	int tmp_pos_x = pos_x + gridAction->getXMove();
	int tmp_pos_y = pos_y + gridAction->getYMove();

	if(tmp_pos_x < 0 || (size_t) tmp_pos_x >= getSizeX() || tmp_pos_y < 0
	    || (size_t) tmp_pos_y >= getSizeY()) {
		actual_bounces++;
	} else if(_prohibitedValues.find(getGridValue(tmp_pos_x, tmp_pos_y))
	    != _prohibitedValues.end()) {
		actual_bounces++;
	} else {
		pos_x = tmp_pos_x;
		pos_y = tmp_pos_y;
	}

	newState->setDiscreteState(0, pos_x);
	newState->setDiscreteState(1, pos_y);
	newState->setDiscreteState(2, actual_bounces);
}

bool GridWorldModel::isResetState(State *state) {
	return ((!(this->max_bounces == (unsigned int) -1)
	    && (unsigned int) state->getDiscreteState(2) > this->max_bounces))
	    || (_targetValues.find(
	        getGridValue(state->getDiscreteState(0),
	            state->getDiscreteState(1))) != _targetValues.end());
}

bool GridWorldModel::isFailedState(State *state) {
	return (!(this->max_bounces == (unsigned int) -1)
	    && (unsigned int) state->getDiscreteState(2) > this->max_bounces);
}

void GridWorldModel::getResetState(State *resetState) {
	auto rand = make_rand();

	if(!is_parsed) parseGrid();
	if(_startPoints.size() > 0) {
		int i = rand() % _startPoints.size();
		resetState->setDiscreteState(0, _startPoints[i].second);
		resetState->setDiscreteState(1, _startPoints[i].first);
	} else {
		resetState->setDiscreteState(0, 0);
		resetState->setDiscreteState(1, 0);
	}
	resetState->setDiscreteState(2, 0);
}

RealType GridWorldModel::getReward(
    StateCollection *oldState, Action *, StateCollection *newState) {
	RealType rew = 0.0;
	if(newState->getState()->getDiscreteState(2)
	    > oldState->getState()->getDiscreteState(2)) {
		rew = reward_bounce;
	} else {
		int x = newState->getState()->getDiscreteState(0);
		int y = newState->getState()->getDiscreteState(1);

		rew = getRewardForSymbol(getGridValue(x, y));
	}
	return rew;
}

GridWorldState_Local4::GridWorldState_Local4(GridWorld* grid_world_) :
	StateModifier(0, 4), grid_world(grid_world_) {
	for(int i = 0; i < 4; i++) {
		setDiscreteStateSize(i, grid_world_->getUsedValues().size());
	}
}

GridWorldState_Local4::~GridWorldState_Local4() {
}

void GridWorldState_Local4::getModifiedState(
    StateCollection *originalState, State *state) {
	int pos_x = originalState->getState()->getDiscreteState(0);
	int pos_y = originalState->getState()->getDiscreteState(1);

	if(pos_y > 0) state->setDiscreteState(0,
	    grid_world->getGridValue(pos_x, pos_y - 1));
	else state->setDiscreteState(0, grid_world->getGridValue(pos_x, pos_y));

	if(pos_x < (int) grid_world->getSizeX() - 1) state->setDiscreteState(1,
	    grid_world->getGridValue(pos_x + 1, pos_y));
	else state->setDiscreteState(1, grid_world->getGridValue(pos_x, pos_y));

	if(pos_y < (int) grid_world->getSizeY() - 1) state->setDiscreteState(2,
	    grid_world->getGridValue(pos_x, pos_y + 1));
	else state->setDiscreteState(2, grid_world->getGridValue(pos_x, pos_y));

	if(pos_x > 0) state->setDiscreteState(3,
	    grid_world->getGridValue(pos_x - 1, pos_y));
	else state->setDiscreteState(3, grid_world->getGridValue(pos_x, pos_y));
}

GridWorldState_Local4X::GridWorldState_Local4X(GridWorld* grid_world_) :
	StateModifier(0, 4), grid_world(grid_world_) {
	for(int i = 0; i < 4; i++) {
		setDiscreteStateSize(i, grid_world->getUsedValues().size());
	}
}

GridWorldState_Local4X::~GridWorldState_Local4X() {
}

void GridWorldState_Local4X::getModifiedState(
    StateCollection *originalState, State *state) {
	int pos_x = originalState->getState()->getDiscreteState(0);
	int pos_y = originalState->getState()->getDiscreteState(1);

	if((pos_y > 0) && (pos_x > 0)) state->setDiscreteState(0,
	    grid_world->getGridValue(pos_x - 1, pos_y - 1));
	else state->setDiscreteState(0, grid_world->getGridValue(pos_x, pos_y));

	if((pos_y > 0) && (pos_x < (int) grid_world->getSizeX() - 1)) state->setDiscreteState(
	    1, grid_world->getGridValue(pos_x + 1, pos_y - 1));
	else state->setDiscreteState(1, grid_world->getGridValue(pos_x, pos_y));

	if((pos_y < (int) grid_world->getSizeY() - 1)
	    && (pos_x < (int) grid_world->getSizeX() - 1)) state->setDiscreteState(
	    2, grid_world->getGridValue(pos_x + 1, pos_y + 1));
	else state->setDiscreteState(2, grid_world->getGridValue(pos_x, pos_y));

	if((pos_y < (int) grid_world->getSizeY() - 1) && (pos_x > 0)) state->setDiscreteState(
	    3, grid_world->getGridValue(pos_x - 1, pos_y + 1));
	else state->setDiscreteState(3, grid_world->getGridValue(pos_x, pos_y));
}

GridWorldState_Local8::GridWorldState_Local8(GridWorld* grid_world_) :
	StateModifier(0, 8), grid_world(grid_world_) {
	for(int i = 0; i < 8; i++) {
		setDiscreteStateSize(i, grid_world->getUsedValues().size());
	}
}

GridWorldState_Local8::~GridWorldState_Local8() {
}

void GridWorldState_Local8::getModifiedState(
    StateCollection *originalState, State *state) {
	int pos_x = originalState->getState()->getDiscreteState(0);
	int pos_y = originalState->getState()->getDiscreteState(1);

	if(pos_y > 0) state->setDiscreteState(0,
	    grid_world->getGridValue(pos_x, pos_y - 1));
	else state->setDiscreteState(0, grid_world->getGridValue(pos_x, pos_y));

	if((pos_y > 0) && (pos_x < (int) grid_world->getSizeX() - 1)) state->setDiscreteState(
	    1, grid_world->getGridValue(pos_x + 1, pos_y - 1));
	else state->setDiscreteState(1, grid_world->getGridValue(pos_x, pos_y));

	if(pos_x < (int) grid_world->getSizeX() - 1) state->setDiscreteState(2,
	    grid_world->getGridValue(pos_x + 1, pos_y));
	else state->setDiscreteState(2, grid_world->getGridValue(pos_x, pos_y));

	if((pos_y < (int) grid_world->getSizeY() - 1)
	    && (pos_x < (int) grid_world->getSizeX() - 1)) state->setDiscreteState(
	    3, grid_world->getGridValue(pos_x + 1, pos_y + 1));
	else state->setDiscreteState(3, grid_world->getGridValue(pos_x, pos_y));

	if(pos_y < (int) grid_world->getSizeY() - 1) state->setDiscreteState(4,
	    grid_world->getGridValue(pos_x, pos_y + 1));
	else state->setDiscreteState(4, grid_world->getGridValue(pos_x, pos_y));

	if((pos_y < (int) grid_world->getSizeY() - 1) && (pos_x > 0)) state->setDiscreteState(
	    5, grid_world->getGridValue(pos_x - 1, pos_y + 1));
	else state->setDiscreteState(5, grid_world->getGridValue(pos_x, pos_y));

	if(pos_x > 0) state->setDiscreteState(6,
	    grid_world->getGridValue(pos_x - 1, pos_y));
	else state->setDiscreteState(6, grid_world->getGridValue(pos_x, pos_y));

	if((pos_y > 0) && (pos_x > 0)) state->setDiscreteState(7,
	    grid_world->getGridValue(pos_x - 1, pos_y - 1));
	else state->setDiscreteState(7, grid_world->getGridValue(pos_x, pos_y));
}

GridWorldDiscreteState_Global::GridWorldDiscreteState_Global(
    unsigned int size_x_, unsigned int size_y_) :
	    AbstractStateDiscretizer(size_x_ * size_y_ + 1),
	    size_x(size_x_),
	    size_y(size_y_) {
}

unsigned int GridWorldDiscreteState_Global::getDiscreteStateNumber(
    StateCollection *state) {
	unsigned int discstate;
	int x = state->getState()->getDiscreteState(0);
	int y = state->getState()->getDiscreteState(1);

	if(x < 0 || (unsigned int) x >= size_x || y < 0
	    || (unsigned int) y >= size_y) {
		discstate = 0;
	} else {
		discstate = y * size_x + x + 1;
	}
	return discstate;
}

GridWorldDiscreteState_Local::GridWorldDiscreteState_Local(
    StateModifier* orig_state_, unsigned int neighbourhood,
    std::set<char> *possible_values) :
	    AbstractStateDiscretizer(
	        (int) pow((RealType) possible_values->size(),
	            (RealType) neighbourhood)),
	    orig_state(orig_state_),
	    valuemap(new std::map<char, short>()) {
	std::set<char>::iterator it = possible_values->begin();
	for(short i = 0; it != possible_values->end(); i++, it++) {
		(*valuemap)[(*it)] = i;
	}
}

GridWorldDiscreteState_Local::~GridWorldDiscreteState_Local() {
	valuemap->clear();
	delete valuemap;
}

unsigned int GridWorldDiscreteState_Local::getDiscreteStateNumber(
    StateCollection *state) {
	State *source_state = state->getState(orig_state);
	unsigned int discstate = 0;
	for(unsigned int i = 0; i < source_state->getNumDiscreteStates() - 1; i++) {
		discstate = discstate * valuemap->size()
		    + (unsigned int) ((*valuemap)[(char) source_state->getDiscreteState(
		        i)]);
	}
	return discstate;
}

GridWorldDiscreteState_SmallLocal::GridWorldDiscreteState_SmallLocal(
    StateModifier* orig_state_, unsigned int neighbourhood,
    GridWorld *gridworld_) :
	    AbstractStateDiscretizer(
	        (unsigned int) pow((RealType) 3.0, (RealType) neighbourhood)),
	    orig_state(orig_state_),
	    gridworld(gridworld_) {
}

GridWorldDiscreteState_SmallLocal::~GridWorldDiscreteState_SmallLocal() {}

unsigned int GridWorldDiscreteState_SmallLocal::getDiscreteStateNumber(
    StateCollection *state) {
	State *source_state = state->getState(orig_state);
	unsigned int discstate = 0;
	unsigned int temp;
	for(unsigned int i = 0; i < source_state->getNumDiscreteStates() - 1; i++) {
		if(gridworld->getTargetValues().find(
		    (char) source_state->getDiscreteState(i))
		    != gridworld->getTargetValues().end()) temp = 2;
		else if(gridworld->getProhibitedValues().find(
		    (char) source_state->getDiscreteState(i))
		    != gridworld->getProhibitedValues().end()) temp = 1;
		else temp = 0;
		discstate = discstate * 3 + temp;
	}
	return discstate;
}

GridWorldAction::GridWorldAction(int x_move_, int y_move_) :
	x_move(x_move_), y_move(y_move_) {
	addType(RLEARNER_GRIDWORLDACTION);
}

int GridWorldAction::getXMove() {
	return this->x_move;
}

int GridWorldAction::getYMove() {
	return this->y_move;
}

void RaceTrack::generateRaceTrack(
    GridWorld *gridworld, unsigned int width, unsigned int length,
    unsigned int h_max, unsigned int dx_min, unsigned int dx_max) {
	auto rand = make_rand();

	unsigned int i, j, x, dx, tmp1, tmp2, y1, y2, h;
	gridworld->setSize(length, width);
	gridworld->addProhibitedValue(1);
	gridworld->addProhibitedValue(2);
	gridworld->addStartValue(3);
	gridworld->addTargetValue(4);

	for(j = 1; j < width - 1; j++) {
		gridworld->setGridValue(0, j, 2);
		gridworld->setGridValue(1, j, 3);
		gridworld->setGridValue(length - 2, j, 4);
		gridworld->setGridValue(length - 1, j, 2);
		for(i = 2; i < length - 2; i++) {
			gridworld->setGridValue(i, j, 0);
		}

	}
	for(i = 0; i < length; i++) {
		gridworld->setGridValue(i, 0, 2);
		gridworld->setGridValue(i, width - 1, 2);
	}

	dx = (int) ceil((RealType) rand() / (RealType) RAND_MAX * (RealType) dx_max);
	x = 2 + dx;
	while(x < length - 2) {
		tmp1 = (int) ceil(
		    (RealType) rand() / (RealType) RAND_MAX * (RealType) (width - 1));
		tmp2 = (int) ceil(
		    (RealType) rand() / (RealType) RAND_MAX * (RealType) (width - 1));
		y1 = std::min(tmp1, tmp2);
		y2 = std::max(tmp1, tmp2);
		if((y1 < 2) && (y2 > width - 3)) {
			if(rand() < (RAND_MAX / 2)) {
				y1 = 2;
				y2 = width - 2;
			} else {
				y1 = 1;
				y2 = width - 3;
			}
		}
		h = std::min(dx,
		    (unsigned int) ceil(
		        (RealType) rand() / (RealType) RAND_MAX * (RealType) h_max)) - 1;
		for(i = y1; i <= y2; i++) {
			gridworld->setGridValue(x, i, 1);
		}
		for(i = x - h + 1; i <= x; i++) {
			gridworld->setGridValue(i, y1, 1);
			gridworld->setGridValue(i, y2, 1);
		}
		dx = dx_min
		    + (int) ceil((RealType) rand() / (RealType) RAND_MAX * (RealType) dx_max);
		x += dx;
	}
}

RaceTrackDiscreteState::RaceTrackDiscreteState(
    StateModifier* orig_state_, unsigned int neighbourhood,
    GridWorld *gridworld_) :
	    AbstractStateDiscretizer(
	        (unsigned int) pow((RealType) 2.0, (RealType) neighbourhood)),
	    orig_state(orig_state_),
	    gridworld(gridworld_) {
}

RaceTrackDiscreteState::~RaceTrackDiscreteState() {
}

unsigned int RaceTrackDiscreteState::getDiscreteStateNumber(
    StateCollection *state) {
	State *source_state = state->getState(orig_state);
	unsigned int discstate = 0;
	unsigned int temp;
	for(unsigned int i = 0; i < source_state->getNumDiscreteStates() - 1; i++) {
		if(gridworld->getProhibitedValues().find(
		    (char) source_state->getDiscreteState(i))
		    != gridworld->getProhibitedValues().end()) temp = 1;
		else temp = 0;
		discstate = discstate * 2 + temp;
	}
	return discstate;
}

} //namespace rlearner
