#ifndef Example_RlearnerStateChart_Task_Complex_H_
#define Example_RlearnerStateChart_Task_Complex_H_

#include "../system.h"
#include "../Task.h"
#include "../state.h"

#include "../StateInitializer.h"

namespace rlearner {
namespace rlearner_complex {

enum class ActionType {
	MOVE_TO_WAREHOUSE,
	MOVE_TO_STORE,
	GET_MATERIAL,
	CREATE_PRODUCT,

	TAKE_PLANKS,
	DROP_PLANKS,
	GET_WATER,
	WATER_PLANTS,
	TAKE_PROCESSOR,
	REPLACE_PROCESSOR,
	MOVE_TO_GARAGE,
	MOVE_TO_GREENHOUSE,
	MOVE_TO_PCCENTER,
	MOVE_TO_SERVERROOM
};

enum class Location {
	STORE,
	WAREHOUSE,

	GARAGE,
	GREENHOUSE,
	PCCENTER,
	SERVERROOM
};

class ManuAction: public PrimitiveAction {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int /*version*/);
	void serialize(OArchive& ar, const unsigned int /*version*/);

protected:
	ActionType _actionType;

protected:
	//! For use in serialization.
	ManuAction(): _actionType() {}

public:
	ManuAction(ActionType actionType): _actionType(actionType) {}

	ActionType getActionType() const {
		return _actionType;
	}

	void setActionType(ActionType actionType) {
		_actionType = actionType;
	}
};

/**
 * @class ManuState
 *
 * A helper class that makes it easier to set up and handle StateProperties.
 */
class ManuState {
private:
	//! Pointer to the underlying State.
	State* _state;

public:
	unsigned int getNumMaterial() {
		return _state->getDiscreteState(0);
	}

	void setNumMaterial(unsigned int num) {
		_state->setDiscreteState(0, num);
	}

	Location getLocation() {
		return Location(_state->getDiscreteState(1));
	}

	void setLocation(Location loc) {
		_state->setDiscreteState(1, static_cast<int>(loc));
	}

	unsigned int getNumProducts() const {
		return _state->getDiscreteState(2);
	}

	void setNumProducts(unsigned int num) {
		_state->setDiscreteState(2, num);
	}

	unsigned int getNumProcessors() {
		return _state->getDiscreteState(3);
	}

	void setNumProcessors(unsigned int num) {
		_state->setDiscreteState(3, num);
	}

	unsigned int getNumWater() {
		return _state->getDiscreteState(4);
	}

	void setNumWater(unsigned int num) {
		_state->setDiscreteState(4, num);
	}

	unsigned int getNumPlanks() {
		return _state->getDiscreteState(5);
	}

	void setNumPlanks(unsigned int num) {
		_state->setDiscreteState(5, num);
	}

public:
	const State* getState() const {
		return _state;
	}

	State* getState() {
		return _state;
	}

	static StateProperties* newProperties();

	/**
	 * Constructor.
	 * @param properties Must point to a valid StateProperties object
	 * associated with the state.
	 */
	ManuState(State* state): _state(state) {}
};

} //rlearner_complex

class Task_Complex: public Task, public RewardFunction, public TransitionFunction {
public:
	using ActionType = rlearner_complex::ActionType;
	using Location = rlearner_complex::Location;
	using ManuAction = rlearner_complex::ManuAction;
	using ManuState = rlearner_complex::ManuState;

private:
	shared_ptr<EnvironmentModel> _environment;
	shared_ptr<StateInitializer> _stateInitializer;

public:
	virtual ActionSet* newActionSet() {
		ActionSet* actionSet = new ActionSet;

		actionSet->add(new ManuAction(ActionType::MOVE_TO_STORE)); 		//0
		actionSet->add(new ManuAction(ActionType::MOVE_TO_WAREHOUSE));	//1
		actionSet->add(new ManuAction(ActionType::GET_MATERIAL));			//2
		actionSet->add(new ManuAction(ActionType::CREATE_PRODUCT));		//3

		actionSet->add(new ManuAction(ActionType::TAKE_PLANKS));			//4
		actionSet->add(new ManuAction(ActionType::DROP_PLANKS));			//5
		actionSet->add(new ManuAction(ActionType::GET_WATER));			//6
		actionSet->add(new ManuAction(ActionType::WATER_PLANTS));			//7
		actionSet->add(new ManuAction(ActionType::TAKE_PROCESSOR));		//8
		actionSet->add(new ManuAction(ActionType::REPLACE_PROCESSOR));	//9
		actionSet->add(new ManuAction(ActionType::MOVE_TO_GARAGE));		//10
		actionSet->add(new ManuAction(ActionType::MOVE_TO_GREENHOUSE));	//11
		actionSet->add(new ManuAction(ActionType::MOVE_TO_PCCENTER));		//12
		actionSet->add(new ManuAction(ActionType::MOVE_TO_SERVERROOM));	//13

		return actionSet;
	}

public:
	virtual RewardFunction* getRewardFunction(unsigned int index = 0);
	virtual unsigned int numRewardFunctions() const;

	virtual TransitionFunction* getTransitionFunction();

	virtual void transitionFunction(State *oldstate, Action *action, State *newState, ActionData *data = NULL);
	virtual RealType getReward(StateCollection *oldState, Action *action, StateCollection *newState);

	virtual void getResetState(State *resetState);
	virtual AbstractStateDiscretizer* newDiscretizer();

	virtual EnvironmentModel* getEnvironment();
	virtual ActionEncoder* newActionEncoder();

	void setStateInitializer(const shared_ptr<StateInitializer>& initializer);
	const shared_ptr<StateInitializer>& getStateInitializer() const;

public:
	Task_Complex& operator=(const Task_Complex&) = delete;
	Task_Complex(const Task_Complex&) = delete;

	Task_Complex();
	virtual ~Task_Complex();
};

} //namespace rlearner

SYS_EXPORT_CLASS(rlearner::rlearner_complex::ManuAction)

#endif //Example_RlearnerStateChart_Task_Complex_H_
