// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>

#include "pendulummodel.h"
#include "../stateproperties.h"
#include "../state.h"
#include "../statecollection.h"
#include "../action.h"
#include "../continuousactions.h"

namespace rlearner {

PendulumModel::PendulumModel(
    RealType dt, RealType uMax, RealType dPhiMax, RealType length, RealType mass,
    RealType mu, RealType g):
	    LinearActionContinuousTimeTransitionFunction(new StateProperties(2, 0),
	        new ContinuousAction(new ContinuousActionProperties(1)), dt)
{
	this->uMax = uMax;
	this->dPhiMax = dPhiMax;
	this->length = length;
	this->mass = mass;
	this->mu = mu;
	this->g = g;

	/*addParameter("UMax", uMax);
	 addParameter("DPhiMax",dPhiMax);
	 addParameter("Length", length);
	 addParameter("Mass", mass);
	 addParameter("Friction", mu);
	 addParameter("Gravity", g);*/

	actionProp->setMaxActionValue(0, uMax);
	actionProp->setMinActionValue(0, -uMax);

	properties->setMaxValue(0, Constant::pi);
	properties->setMinValue(0, -Constant::pi);

	properties->setPeriodicity(0, true);

	properties->setMaxValue(1, dPhiMax);
	properties->setMinValue(1, -dPhiMax);

}

PendulumModel::~PendulumModel() {
	delete properties;
	delete actionProp;
	delete contAction;
}

Matrix *PendulumModel::getB(State *) {
	/*	RealType mass = getParameter("Mass");
	 RealType length = getParameter("Length");*/
	B->operator()(0, 0) = 0.0;
	B->operator()(1, 0) = 1 / (mass * pow(length, 2));

	return B;
}

ColumnVector *PendulumModel::getA(State *state) {
	/*RealType mass = getParameter("Mass");
	 RealType length = getParameter("Length");
	 RealType mu = getParameter("Friction");
	 RealType g = getParameter("Gravity");
	 */
	RealType dphi = state->getContinuousState(1);
	A->operator[](0) = dphi;
	RealType ddphi = 1 / (mass * pow(length, 2))
	    * (-mu * state->getContinuousState(1)
	        + mass * g * length * sin(state->getContinuousState(0)));
	A->operator[](1) = ddphi;

	return A;
}

void PendulumModel::setParameter(std::string paramName, RealType value) {
	if(paramName == "UMax") {
		actionProp->setMaxActionValue(0, value);
		actionProp->setMinActionValue(0, -value);
	}
	if(paramName == "DPhiMax") {
		properties->setMaxValue(1, value);
		properties->setMinValue(1, -value);
	}
	ParameterObject::setParameter(paramName, value);
}

bool PendulumModel::isFailedState(State *) {
	return false;
}

void PendulumModel::getResetState(State *resetState) {
	TransitionFunction::getResetState(resetState);

	if(resetType != DM_RESET_TYPE_ALL_RANDOM) {
		resetState->setContinuousState(1, 0);
	}
}

void PendulumModel::doSimulationStep(
    State *state, RealType timestep, Action *action, ActionData *data) {
	getDerivationX(state, action, derivation, data);

	RealType ddPhi = derivation->operator[](1);

	state->setContinuousState(0,
	    state->getContinuousState(0) + timestep * derivation->operator[](0)
	        + pow(timestep, 2) / 2 * ddPhi);
	state->setContinuousState(1,
	    state->getContinuousState(1) + timestep * derivation->operator[](1));
}

PendulumRewardFunction::PendulumRewardFunction(PendulumModel *model) :
	StateReward(model->getStateProperties()) {
	rewardFactor = 1.0;
}

RealType PendulumRewardFunction::getStateReward(State *state) {
	RealType Phi = state->getContinuousState(0);
	return rewardFactor * (cos(Phi) - 1);
}

void PendulumRewardFunction::getInputDerivation(
    State *modelState, ColumnVector *targetState) {
	RealType Phi = modelState->getState(properties)->getContinuousState(0);
	targetState->operator[](1) = 0;
	targetState->operator[](0) = -sin(Phi);
}

PendulumUpTimeCalculator::PendulumUpTimeCalculator(RealType phi_up, RealType dt) {
	this->phi_up = phi_up;
	this->dt = dt;

	this->up_steps = 0;
}

void PendulumUpTimeCalculator::nextStep(
    StateCollection *oldState, Action *, StateCollection *) {
	if(fabs(oldState->getState()->getContinuousState(0)) < phi_up) {
		up_steps++;
	}
}

void PendulumUpTimeCalculator::newEpisode() {
	up_steps = 0;
}

RealType PendulumUpTimeCalculator::getUpTime() {
	return up_steps * dt;
}

int PendulumUpTimeCalculator::getUpSteps() {
	return up_steps;
}

} //namespace rlearner
