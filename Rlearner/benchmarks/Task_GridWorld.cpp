#include "Task_GridWorld.h"

namespace rlearner {

ActionSet* Task_GridWorld::newActionSet() {
	ActionSet* actionSet = new ActionSet;

	actionSet->add(new GridWorldAction(0, 1));
	actionSet->add(new GridWorldAction(1, 0));
	actionSet->add(new GridWorldAction(0, -1));
	actionSet->add(new GridWorldAction(-1, 0));

	return actionSet;
}

RewardFunction* Task_GridWorld::getRewardFunction(unsigned int index) {
	return _environment.get();
}

unsigned int Task_GridWorld::numRewardFunctions() const {
	return 1;
}

TransitionFunction* Task_GridWorld::getTransitionFunction() {
	return _environment.get();
}

AbstractStateDiscretizer* Task_GridWorld::newDiscretizer() {
	return new GridWorldDiscreteState_Global(_environment->getSizeX(),
	    _environment->getSizeY());
}

EnvironmentModel* Task_GridWorld::getEnvironment() {
	return _model.get();
}

ActionEncoder* Task_GridWorld::newActionEncoder() {
	return nullptr;
}

void Task_GridWorld::setGridWorldModel(
    const shared_ptr<GridWorldModel>& model) {
	_environment = model;
	_model = make_shared<TransitionFunctionEnvironment>(_environment.get());
}

Task_GridWorld::Task_GridWorld() :
	_environment(), _model() {
}

Task_GridWorld::Task_GridWorld(const shared_ptr<GridWorldModel>& model) :
	_environment(), _model() {
	setGridWorldModel(model);
}

} // namespace rlearner
