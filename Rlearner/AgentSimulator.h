#ifndef RLEARNER_AGENTSIMULATOR_H_
#define RLEARNER_AGENTSIMULATOR_H_

#include "agent.h"
#include "environmentmodel.h"
#include "statecollection.h"

namespace rlearner {

/**
 * @class AgentSimulator
 *
 * A class used to run an agent on an environment model.
 **/
class AgentSimulator {
protected:
	//! The environment model -- this takes care of performing the actions
	//! and observing the resulting transitions.
	shared_ptr<EnvironmentModel> _model;
	//! The agent that selects the action in each step.
	shared_ptr<Agent> _agent;

	unique_ptr<StateCollectionImpl> _lastState;
	unique_ptr<StateCollectionImpl> _currentState;

	StateModifierList _modifierList;

	//! Counts the total number of simulation steps run.
	unsigned int _totalSteps = 0;
	//! Counts the total number of episodes run.
	unsigned int _totalEpisodes = 0;

protected:
	//! Initializes the state once a new episode has started.
	void initializeState();

public:
	// Returns the total number of simulation steps run since the last call to
	// resetStepCounters.
	unsigned int getTotalSteps() const {return _totalSteps;}

	//! Returns the total number of episodes run since the last call to
	//! resetStepCounters.
	unsigned int getTotalEpisodes() const {return _totalEpisodes;}

	//! Resets the total number of steps and the totals episodes counter.
	void resetStepCounters() {
		_totalSteps = 0;
		_totalEpisodes = 0;
	}

	//! Adds the StateModifier to this object's StateModifierList (the list
	//! itself is already added to the underlying StateCollections).
	void addStateModifier(StateModifier* modifier);
	//! Removes the StateModifier from this object's StateModifierList.
	void removeStateModifier(StateModifier* modifier);

	//! Adds the StateModifierList to the StateCollections.
	void addModifierList(StateModifierList* modifierList);
	//! Removes the StateModifierList from the StateCollections.
	void removeModifierList(StateModifierList* modifierList);

	//! Returns simulators's list of state modifiers.
	StateModifierList* getModifierList() {return &_modifierList;}

	//! Tells all Listeners that a new Episode has occurred and resets the model.
	void startNewEpisode();

	/**
	 * Returns true if the currently running episode has ended and a new episode
	 * has not started yet; more precisely, this queries the reset state of the
	 * environment model (return model->isReset()).
	 **/
	bool isEpisodeEnded() const;

	/**
	 * Tells the model which action to execute and then saves the new State.
	 * Send the oldState, the action and the newState as S-A-S Tuple to all
	 * Listeners (see SemiMarkovDecisionProcess::sendNextStep(...)).
	 *
	 * There is a special treatment for actions of the type
	 * AF_PrimitiveActionSTATECHANGE, @see PrimitiveActionStateChanged
	 **/
	void doAction(Action *action);

	StateCollection *getCurrentState();

	/**
	 * Executes maxEpisodes, if an episode reaches maxsteps, a new episode is
	 * started automatically.
	 **/
	int doControllerEpisode(int maxSteps = 5000);

	/**
	 * Gets action from the controller and executes it.
	 *
	 * @return Returns true if the step was successfully applied and false if
	 * if the current episode has ended and the step was rejected.
	 *
	 * Be aware that you will get an assertion if the agent controller isn't
	 * set properly!
	**/
	bool doControllerStep();

	const shared_ptr<Agent>& getAgent() {return _agent;}
	const shared_ptr<EnvironmentModel>& getEnvironmentModel() {return _model;}

	/**
	 * @param maxEpisodes The maximum number of episodes to run.
	 * @param maxSteps The maximum number of steps to run.
	 * @return Returns a pair of the form (number of simulation steps run,
	 * number of episodes run).
	 */
	std::pair<unsigned int, unsigned int> run(unsigned int maxEpisodes, unsigned int maxSteps);

public:
	AgentSimulator(const shared_ptr<EnvironmentModel>& model,
		const shared_ptr<Agent>& agent);
	~AgentSimulator();
};

} // namespace rlearner

#endif /* RLEARNER_AGENTSIMULATOR_H_ */
