// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include "ril_debug.h"
#include "actorcritic.h"
#include "policies.h"
#include <cmath>

#include "qfunction.h"
#include "vfunction.h"
#include "vfunctionlearner.h"
#include "vetraces.h"
#include "qetraces.h"
#include "parameters.h"
#include "continuousactiongradientpolicy.h"
#include "ril_debug.h"
#include "action.h"
#include "system.h"

namespace rlearner {

Actor::Actor() {
	addParameter("ActorLearningRate", 0.2);
}

RealType Actor::getLearningRate() {
	return getParameter("ActorLearningRate");
}

void Actor::setLearningRate(RealType learningRate) {
	setParameter("ActorLearningRate", learningRate);
}

ActorFromQFunction::ActorFromQFunction(AbstractQFunction *qFunction) :
	Actor() {
	this->qFunction = qFunction;
	eTraces = qFunction->getStandardETraces();

	addParameters(qFunction);
	addParameters(eTraces, "Actor");

}

ActorFromQFunction::~ActorFromQFunction() {
	delete eTraces;
}

void ActorFromQFunction::receiveError(
    RealType critic, StateCollection *state, Action *action, ActionData *) {
	DebugPrint('t', "Actor updating Etraces \n");
	eTraces->updateETraces(action);
	eTraces->addETrace(state, action);
	DebugPrint('t', "Actor updating QFunction with critic %f \n", critic);
	eTraces->updateQFunction(critic * getLearningRate());
}

void ActorFromQFunction::newEpisode() {
	eTraces->resetETraces();
}

AbstractQFunction *ActorFromQFunction::getQFunction() {
	return qFunction;
}

AbstractQETraces *ActorFromQFunction::getETraces() {
	return eTraces;
}

ActorFromQFunctionAndPolicy::ActorFromQFunctionAndPolicy(
    AbstractQFunction *qFunction, StochasticPolicy *policy) :
	ActorFromQFunction(qFunction) {
	this->policy = policy;

	actionValues = new RealType[policy->getActions()->size()];

	addParameters(policy);

	addParameter("PolicyMinimumLearningRate", 0.5);
}

ActorFromQFunctionAndPolicy::~ActorFromQFunctionAndPolicy() {
	delete actionValues;
}

void ActorFromQFunctionAndPolicy::receiveError(
    RealType critic, StateCollection *state, Action *Action, ActionData *) {
	policy->getActionProbabilities(state, qFunction->getActions(),
	    actionValues);
	RealType prob = actionValues[qFunction->getActions()->getIndex(Action)];

	eTraces->updateETraces(Action);
	eTraces->addETrace(state, Action,
	    getParameter("PolicyMinimumLearningRate") + 1.0 - prob);
	eTraces->updateQFunction(critic * getLearningRate());
}

ActorFromActionValue::ActorFromActionValue(
    AbstractVFunction *vFunction, Action *action1, Action *action2) :
	AgentController(new ActionSet()) {
	actions->add(action1);
	actions->add(action2);

	addParameters(vFunction);

	this->vFunction = vFunction;
	this->eTraces = vFunction->getStandardETraces();

	addParameters(eTraces, "Actor");

	setParameter("ActorLearningRate", 1000.0);
}

ActorFromActionValue::~ActorFromActionValue() {
	delete actions;
	delete eTraces;
}

void ActorFromActionValue::receiveError(
    RealType critic, StateCollection *oldState, Action *action, ActionData *) {
	int actionIndex = actions->getIndex(action);
	eTraces->updateETraces(action->getDuration());
	eTraces->addETrace(oldState, (actionIndex == 0) - 0.5);
	eTraces->updateVFunction(critic * getLearningRate());
}

Action *ActorFromActionValue::getNextAction(
    StateCollection *state, ActionDataSet *) {
	auto rand = make_rand();
	RealType value = vFunction->getValue(state);

	if(value > 50) {
		value = 50;
	}
	if(value < -50) {
		value = -50;
	}

	RealType propability = 1.0 / (1.0 + exp(-value));
	int actionIndex = _rand() > propability;

	return actions->get(actionIndex);
}

void ActorFromActionValue::newEpisode() {
	eTraces->resetETraces();
}

ActorFromContinuousActionGradientPolicy::ActorFromContinuousActionGradientPolicy(
    ContinuousActionGradientPolicy *l_gradientPolicy) {
	this->gradientPolicy = l_gradientPolicy;
	gradientETraces = new GradientVETraces(nullptr);
	gradientFeatureList = new FeatureList();

	addParameters(gradientPolicy);
	addParameters(gradientETraces, "Actor");

	policyDifference = new ContinuousActionData(
	    l_gradientPolicy->getContinuousActionProperties());
}

ActorFromContinuousActionGradientPolicy::~ActorFromContinuousActionGradientPolicy() {
	delete gradientETraces;
	delete gradientFeatureList;
	delete policyDifference;
}

void ActorFromContinuousActionGradientPolicy::receiveError(
    RealType critic, StateCollection *oldState, Action *Action,
    ActionData *data) {
	gradientETraces->updateETraces(Action->getDuration());

	ContinuousActionData *contData = nullptr;
	if(data) {
		contData = dynamic_cast<ContinuousActionData *>(data);
	} else {
		contData =
		    dynamic_cast<ContinuousActionData *>(Action->getActionData());

	}

	assert(gradientPolicy->getRandomController());
	ColumnVector *noise = gradientPolicy->getRandomController()->getLastNoise();

	if(DebugIsEnabled('a')) {
		DebugPrint('a', "ActorCritic Noise: ");
		policyDifference->save(DebugGetFileHandle('a'));
	}

	for(int i = 0; i < gradientPolicy->getNumOutputs(); i++) {
		gradientFeatureList->clear();
		gradientPolicy->getGradient(oldState, i, gradientFeatureList);

		gradientETraces->addGradientETrace(gradientFeatureList,
		    noise->operator[](i));
	}

	gradientPolicy->updateGradient(gradientETraces->getGradientETraces(),
	    critic * getParameter("ActorLearningRate"));
}

void ActorFromContinuousActionGradientPolicy::newEpisode() {
	gradientETraces->resetETraces();
}

ActorForMultipleAgents::ActorForMultipleAgents(ActionSet *actions) :
	AgentController(actions) {
	actors = new std::list<Actor *>();
	actionSets = new std::list<AgentController *>();
	numActions = 1;
}

ActorForMultipleAgents::~ActorForMultipleAgents() {
	delete actors;
	delete actionSets;
}

void ActorForMultipleAgents::addActor(Actor *actor, AgentController *policy) {
	actors->push_back(actor);
	actionSets->push_back(policy);

	numActions = numActions * policy->getActions()->size();

	assert(numActions <= actions->size());

	addParameters(actor);
	addParameters(policy);
}

void ActorForMultipleAgents::receiveError(
    RealType critic, StateCollection *state, Action *action, ActionData *data) {
	int actionIndex = actions->getIndex(action);

	std::list<Actor *>::iterator it = actors->begin();
	std::list<AgentController *>::iterator it2 = actionSets->begin();

	for(; it != actors->end(); it++, it2++) {
		int l_index = actionIndex % (*it2)->getActions()->size();
		Action *l_action = (*it2)->getActions()->get(l_index);
		(*it)->receiveError(critic, state, l_action, data);
		actionIndex = actionIndex / (*it2)->getActions()->size();
	}
}

Action* ActorForMultipleAgents::getNextAction(
    StateCollection *state, ActionDataSet *) {
	int actionIndex = 0;
	int actionDim = 1;

	std::list<Actor *>::iterator it = actors->begin();
	std::list<AgentController *>::iterator it2 = actionSets->begin();

	for(; it != actors->end(); it++, it2++) {
		Action *l_action = (*it2)->getNextAction(state, nullptr);
		int l_index = (*it2)->getActions()->getIndex(l_action);

		//printf("Actor %d choosed Action %d\n", i, l_index);

		actionIndex += l_index * actionDim;

		actionDim = actionDim * (*it2)->getActions()->size();
	}
	//printf("Choosed Action %d\n", actionIndex);

	return actions->get(actionIndex);
}

} //namespace rlearner
