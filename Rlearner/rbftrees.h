// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_crbftrees_H_
#define Rlearner_crbftrees_H_

#include <list>
#include <vector>
#include <cstdio>

#include "algebra.h"
#include "inputdata.h"
#include "trees.h"
#include "forest.h"
#include "extratrees.h"
#include "nearestneighbor.h"

namespace rlearner {

class RBFBasisFunction {
protected:
	ColumnVector *center;
	ColumnVector *sigma;

public:
	RealType getActivationFactor(ColumnVector *x);

	ColumnVector *getCenter();
	ColumnVector *getSigma();

	void setSigma(ColumnVector *sigma);
	void setCenter(ColumnVector *center);

public:
	RBFBasisFunction(ColumnVector *center, ColumnVector *sigma);
};

class RBFBasisFunctionLinearWeight: public RBFBasisFunction {
protected:
	RealType weight;

public:
	RealType getOutputWeight();
	void setWeight(RealType weight);

public:
	RBFBasisFunctionLinearWeight(
	    ColumnVector *center,
	    ColumnVector *sigma,
	    RealType weight
	);
};

class RBFDataFactory: public TreeDataFactory<RBFBasisFunction *> {
protected:
	ColumnVector *minVar;
	ColumnVector *varMultiplier;
	DataSet *inputData;

public:
	virtual RBFBasisFunction *createTreeData(
	    DataSubset *dataSubset,
	    int numLeaves);
	virtual void deleteData(RBFBasisFunction *basisFunction);

public:
	RBFDataFactory(
	    DataSet *inputData,
	    ColumnVector *varMultiplier,
	    ColumnVector *minVar);

	RBFDataFactory(DataSet *inputData);
	virtual ~RBFDataFactory();
};

class RBFLinearWeightDataFactory: public TreeDataFactory<
    RBFBasisFunctionLinearWeight *>{
protected:
	ColumnVector *minVar;
	ColumnVector *varMultiplier;

	DataSet *inputData;
	DataSet1D *outputData;

public:
	virtual RBFBasisFunctionLinearWeight *createTreeData(
	    DataSubset *dataSubset,
	    int numLeaves);
	virtual void deleteData(RBFBasisFunctionLinearWeight *basisFunction);

public:
	RBFLinearWeightDataFactory(
	    DataSet *inputData,
	    DataSet1D *outputData,
	    ColumnVector *varMultiplier,
	    ColumnVector *minVar);

	virtual ~RBFLinearWeightDataFactory();
};

class RBFExtraRegressionTree: public ExtraTree<RBFBasisFunctionLinearWeight *> {
public:
	RBFExtraRegressionTree(
	    DataSet *inputData,
	    DataSet1D *outputData,
	    unsigned int K,
	    unsigned int n_min,
	    RealType treshold,
	    ColumnVector *varMultiplier,
	    ColumnVector *minVar);

	virtual ~RBFExtraRegressionTree();
};

class KNearestRBFCenters: public KNearestNeighborsTreeData<int,
    RBFBasisFunctionLinearWeight *> {
protected:
	ColumnVector *buffVector;

protected:
	virtual void addDataElements(
	    ColumnVector *point,
	    Leaf<RBFBasisFunctionLinearWeight *> *leaf,
	    KDRectangle *rectangle);

public:
	KNearestRBFCenters(Tree<RBFBasisFunctionLinearWeight *> *tree, int K);
	virtual ~KNearestRBFCenters();
};

class RBFRegressionTreeOutputMapping: public Mapping<RealType> {
protected:
	Tree<RBFBasisFunctionLinearWeight *> *tree;
	KNearestRBFCenters *nearestLeaves;

protected:
	RealType doGetOutputValue(ColumnVector *output);

public:
	RBFRegressionTreeOutputMapping(
	    Tree<RBFBasisFunctionLinearWeight *> *tree,
	    int K
	);

	virtual ~RBFRegressionTreeOutputMapping();
};

class RBFExtraRegressionForest:
    public Forest<RBFBasisFunctionLinearWeight *>, public Mapping<RealType> {
protected:
	RBFRegressionTreeOutputMapping **mapping;
	TreeDataFactory<RBFBasisFunctionLinearWeight *> *dataFactory;

protected:
	void initRBFMapping(int kNN);
	virtual RealType doGetOutputValue(ColumnVector *input);

public:
	RBFExtraRegressionForest(
	    int numTrees,
	    int kNN,
	    DataSet *inputData,
	    DataSet1D *outputData,
	    unsigned int K,
	    unsigned int n_min,
	    RealType treshold,
	    ColumnVector *varMultiplier,
	    ColumnVector *minVar
	);

	virtual ~RBFExtraRegressionForest();
};

class RBFLinearWeightForest:
    public Forest<RBFBasisFunctionLinearWeight *>, public Mapping<RealType> {
protected:
	RealType getOutputValue(ColumnVector *inputData);
	virtual void save(std::ostream& stream);

protected:
	RBFLinearWeightForest(int numTrees, int numDim);
	virtual ~RBFLinearWeightForest();
};

class ExtraTreeRBFLinearWeightForest: public RBFLinearWeightForest {
protected:
	RBFLinearWeightDataFactory *dataFactory;

public:
	ExtraTreeRBFLinearWeightForest(
	    int numTrees,
	    DataSet *inputData,
	    DataSet1D *outputData,
	    unsigned int K,
	    unsigned int n_min,
	    RealType treshold,
	    ColumnVector *varMultiplier,
	    ColumnVector *minVar
	);

	virtual ~ExtraTreeRBFLinearWeightForest();
};

} //namespace rlearner

#endif //Rlearner_crbftrees_H_
