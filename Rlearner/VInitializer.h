#ifndef Animus_initializers_H_
#define Animus_initializers_H_

#include "system.h"
#include "vfunction.h"

#include <Systematic/generator/Generator.h>
#include <Systematic/generator/BoundaryGenerator.h>

namespace rlearner {

/**
 * @class VInitializer
 * A class that initializes feature v-functions.
 */
class VInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//! Initializes the specified v-function.
	virtual void init(FeatureVFunction* vFunction) = 0;

	//! An alias for init.
	void operator()(FeatureVFunction* vFunction) {
		init(vFunction);
	}

public:
	virtual ~VInitializer() = default;
};

/**
 * @class VInitializer_Constant
 * A class that initializes all features of a feature v-function to a constant.
 */
class VInitializer_Constant: public VInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The constant to which the features will be initialized.
	RealType _constant;

public:
	//! Initializes the specified v-function.
	virtual void init(FeatureVFunction* vFunction);

	//! Returns the value of the initialization constant.
	RealType getConstant() const {
		return _constant;
	}

	//! Sets the value of the initialization constant.
	void setConstant(RealType constant) {
		_constant = constant;
	}

public:
	/**
	 * Constructor.
	 * @param constant The constant value to which features of the
	 * v-function are to be initialized.
	 */
	VInitializer_Constant(RealType constant = 0): _constant(constant) {}
	virtual ~VInitializer_Constant() = default;
};

/**
 * @class VInitializer_Constant
 * A class that initializes features of a feature v-function using a (random)
 * generator.
 */
class VInitializer_Generator: public VInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The generator used to initialize features.
	shared_ptr<Generator<RealType> > _generator;

public:
	//! Initializes the specified v-function.
	virtual void init(FeatureVFunction* vFunction);

	//! Returns the generator.
	const shared_ptr<Generator<RealType> >& getGenerator() const {
		return _generator;
	}

	//! Sets the generator.
	void setGenerator(const shared_ptr<Generator<RealType> >& generator) {
		_generator = generator;
	}

public:
	/**
	 * Default constructor.
	 *
	 * \note You need to set the generator before using the initializer.
	 */
	VInitializer_Generator():
		_generator() {}

	/**
	 * Constructor.
	 * @param constant The constant value to which features of the
	 * v-function are to be initialized.
	 */
	VInitializer_Generator(const shared_ptr<Generator<RealType> >& generator):
		_generator(generator) {}

	virtual ~VInitializer_Generator() = default;
};

/**
 * @class VInitializer_Uniform
 *
 * A class that initializes features of a feature v-function using a random
 * generator with uniform distribution of probability from range [min, max).
 */
class VInitializer_Uniform: public VInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The generator used to initialize features.
	shared_ptr<BoundaryGenerator<RealType> > _generator;

public:
	//! Initializes the specified v-function.
	virtual void init(FeatureVFunction* vFunction);

	//! Returns the lower bound of the range.
	RealType getMin() const {
		return _generator->getLowerBound();
	}

	//! Returns the upper bound of the range.
	RealType getMax() const {
		return _generator->getUpperBound();
	}

	//! Sets the range to [min, max).
	void setRange(RealType min, RealType max) {
		_generator->createSpace(min, max);
	}

public:
	/**
	 * Constructor.
	 *
	 * Set the intializer to generate features from range [min, max).
	 */
	VInitializer_Uniform(RealType min = 0, RealType max = 1):
		_generator(new BoundaryGenerator<RealType>(min, max)) {}

	virtual ~VInitializer_Uniform() = default;
};

} //namespace rlearner

#endif //Animus_initializers_H_
