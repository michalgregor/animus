// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_crewardfunction_H_
#define Rlearner_crewardfunction_H_

#include "system.h"

#include "algebra.h"
#include "baseobjects.h"

namespace rlearner {

class FeatureList;
class AbstractVFunction;
class FeatureVFunction;

/**
 * @class RewardFunction
 * Class for calculating the reward for the learning objects
 *
 * The reward is calculated for each learning object separately. The reward
 * function objects have to return a RealType-valued reward given an S-A-S tuple,
 * this is done by the function getReward. So for an own learning problem this
 * class has to be derived to implement the getReward function. If you need
 * access to the action index, you have to provide an actionset for your reward
 * function.
 *
 * Each listener which needs a reward is subclass of SemiMDPRewardListener,
 * which maintains a reward function object and calculates the reward for
 * the listener.
 *
 * @see SemiMDPRewardListener
 **/
class RewardFunction {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//! Virtual function for calculating the reward.
	virtual RealType getReward(StateCollection *oldState, Action *action,
	        StateCollection *newState) = 0;

public:
	virtual ~RewardFunction() = default;
};

/**
 * @class FeatureRewardFunction
 * Class for calculating the reward given a feature not a state.
 *
 * Used to calculate the reward for features (integer values). Also implements
 * the RewardFunction interface, so it can be used as normal reward function.
 * When the getReward function is called with state collection objects as
 * parameters, the state from the specified discretizer is chosen. This state
 * has to be a feature or discrete state, and it gets decomposed into
 * its features.
 **/
class FeatureRewardFunction: public RewardFunction, public StateObject {
protected:
	StateProperties *discretizer;

public:
	//! Calls getReward(State *oldState, Action *action, State *newState).
	virtual RealType getReward(StateCollection *oldState, Action *action,
	        StateCollection *newState);
	//! The function to be implemented by the subclasses, has to return
	//! a reward based on discrete integer states.
	virtual RealType getReward(int oldState, Action *action, int newState) = 0;

	/**
	 * Calls getReward(int oldState, Action *action, int newState) with
	 * the features from the state as argument.
	 *
	 * The state has to be a feature state, this state gets decomposed into its
	 * features, the reward for the features is calculated and summed up
	 * weighted with the feature factors.
	 **/
	virtual RealType getReward(State *oldState, Action *action,
	        State *newState);

	//! Deprecated.
	virtual RealType getReward(FeatureList *oldState, Action *action,
	        FeatureList *newState);

public:
	FeatureRewardFunction& operator=(const FeatureRewardFunction&) = delete;
	FeatureRewardFunction(const FeatureRewardFunction&) = delete;

	//! Creates the reward function with the specified discretizer
	//! for the states.
	FeatureRewardFunction(StateProperties *discretizer);
	virtual ~FeatureRewardFunction();
};

/**
 * @class StateReward
 * Reward Function that only depends on the current state.
 *
 * This class is the interface for all reward functions that only depend on
 * the current (model) state. All subclasses have to implement the function
 * getStateReward, which is called by the getReward function of the class with
 * the newState object as argument.
 *
 * The class also offers the function getInputDerivation where the derivation
 * of the reward function with respect to the model state can be calculated.
 * This is needed by some algorithm (analytical pegasus), you will have to
 * implement this function if you want to use these algorithms.
 **/
class StateReward: public RewardFunction, public StateObject {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	StateProperties *properties;

public:
	virtual RealType getReward(StateCollection *oldState, Action *action,
	        StateCollection *newState);

	virtual RealType getStateReward(State *modelState) = 0;
	virtual void getInputDerivation(State *, ColumnVector *) {}

public:
	StateReward& operator=(const StateReward&) = delete;
	StateReward(const StateReward&) = delete;

	StateReward(StateProperties *properties);
	virtual ~StateReward() {}
};

class ConstantReward: public RewardFunction {
private:
	RealType _reward;

public:
	virtual RealType getReward(StateCollection *, Action *,
	        StateCollection *) {
		return _reward;
	}

public:
	void setReward(RealType reward) {
		_reward = reward;
	}

	RealType getReward() const {
		return _reward;
	}

public:
	ConstantReward(RealType reward = 0): _reward(reward) {}
	virtual ~ConstantReward() {}
};

class ZeroReward: public RewardFunction {
public:
	virtual RealType getReward(StateCollection *, Action *,
	        StateCollection *) {
		return 0;
	}

public:
	virtual ~ZeroReward() {}
};

class RewardFunctionFromValueFunction: public RewardFunction {
protected:
	AbstractVFunction *vFunction;
	bool useNewState;

public:
	virtual RealType getReward(StateCollection *oldState, Action *action,
	        StateCollection *newState);

public:
	RewardFunctionFromValueFunction& operator=(const RewardFunctionFromValueFunction&) = delete;
	RewardFunctionFromValueFunction(const RewardFunctionFromValueFunction&) = delete;

	RewardFunctionFromValueFunction(AbstractVFunction *vFunction,
		        bool useNewState = true);

	virtual ~RewardFunctionFromValueFunction() {}
};

class FeatureRewardFunctionFromValueFunction: public FeatureRewardFunction {
protected:
	FeatureVFunction *vFunction;
	bool useNewState;

public:
	virtual RealType getReward(int oldState, Action *action, int newState);

public:
	FeatureRewardFunctionFromValueFunction& operator=(const FeatureRewardFunctionFromValueFunction&) = delete;
	FeatureRewardFunctionFromValueFunction(const FeatureRewardFunctionFromValueFunction&) = delete;

	FeatureRewardFunctionFromValueFunction(StateModifier *discretizer,
		        FeatureVFunction *vFunction, bool useNewState = true);
	virtual ~FeatureRewardFunctionFromValueFunction();
};

} //namespace rlearner

SYS_EXPORT_CLASS(rlearner::RewardFunction)

#endif //Rlearner_crewardfunction_H_
