// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cprioritizedsweeping_H_
#define Rlearner_cprioritizedsweeping_H_

#include <map>

#include "dynamicprogramming.h"
#include "baseobjects.h"
#include "agentlistener.h"

namespace rlearner {

class FeatureQFunction;
class StateModifier;
class AbstractFeatureStochasticModel;
class FeatureRewardFunction;
class FeatureVFunction;
class FeatureCalculator;

/**
 * @class PrioritizedSweeping
 * Class for model based Prioritized Sweeping.
 *
 * Prioritized sweeping is used if the model is learned during the training
 * trial and is very similar to value iteration. Since it would be too complex
 * to do value iteration each time the model changes, only the first k states
 * from the priority list are updated each step, starting with the current
 * features. The Prioritized Sweeping class is subclass of ValueIteration,
 * the only extension is that, each time a nextStep event occurs, the current
 * features are updated (and so the backward states of the current features
 * are added to the priority list) and than the states from the list are
 * updated k times. Since it is subclass of ValueIteration it provides the
 * full functionality for state updates. The prioritized sweeping class always
 * learns the value function of the greedy policy, so its the optimal value
 * function.
 *
 * The class only provides Q-Function learning because the Q-Function is needed
 * for the policies. Additionally it takes a model
 * (AbstractFeatureStochasticModel) and a feature reward function as parameters.
 * The model (and the reward function, if it is a reward model itself) has to
 * be added to the agents listener list before the prioritized sweeping
 * algorithm is added.
 **/
class PrioritizedSweeping:
	public SemiMDPListener,
	public ValueIteration,
	public StateObject
{
public:
	/**
	 * Updates the features of the old State.
	 *
	 * Retrieves the state from the statecollection (with the given modifier
	 * pointer from the constructor), and updates each feature. So the backwards
	 * states priorities get updated as well. After that, the first kSteps
	 * states in the list are updated.
	 **/
	virtual void nextStep(
		StateCollection *oldState,
		Action *action,
		StateCollection *newState
	);

	FeatureCalculator *getFeatureCalculator();

public:
	/**
	 * The class only provides Q-Function learning because the Q-Function is
	 * needed for the policies. Additionally it takes a model
	 * (AbstractFeatureStochasticModel) and a feature reward function as
	 * parameters. The model (and the reward function, if it is a reward model
	 * itself) has to be added to the agents listener list before the
	 * prioritized sweeping algorithm is added.
	 **/
	PrioritizedSweeping(
		FeatureQFunction *qFunction,
		StateModifier *discretizer,
		AbstractFeatureStochasticModel *model,
		FeatureRewardFunction *rewardFunction,
		int kSteps
	);

	PrioritizedSweeping(
		FeatureVFunction *vFunction,
		StateModifier *discretizer,
		AbstractFeatureStochasticModel *model,
		FeatureRewardFunction *rewardFunction,
		int kSteps
	);

	virtual ~PrioritizedSweeping();
};

} //namespace rlearner

#endif //Rlearner_cprioritizedsweeping_H_
