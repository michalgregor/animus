// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_canalyzer_H_
#define Rlearner_canalyzer_H_

/**
 * @enum AnalyzerType
 */
enum AnalyzerType {
	AT_MSE = 1, //!< AT_MSE
	AT_MAE = 2, //!< AT_MAE
	AT_MAXERROR = 3 //!< AT_MAXERROR
};

#include <list>
#include <iostream>

#include "batchlearning.h"
#include "testsuit.h"
#include "rewardfunction.h"
#include "agentcontroller.h"
#include "baseobjects.h"
#include "system.h"

namespace rlearner {

class AbstractQFunction;
class AbstractVFunction;

/**
 * @class VFunctionAnalyzer
 * Analyzer for V-Functions.
 *
 * With V-Function Analyzers you can create different tables for showing
 * the shape of your continuous V-Function. These tables can only be saved
 * to a file.
 *
 * The analyzer supports:
 * - 1 dimensional Tables;
 * - 2 dimensional Tables;
 * - calculate the Value for each State in a given list.
 *
 * A 1-dimensional Table can be created with the function save1DValues.
 * You can choose the dimension for which the table shall be created and
 * the number of partitions for this dimension. Additionally you can specify
 * an init-state, this init-state is used for the state values for all other
 * dimensions. This function can only be used for continuous state variables.
 *
 * A 2-dimensional Table can be created with the function save2DValues.
 * You can choose both dimension, and the number of partitions for each
 * dimension. The init-state is used in the same way as for 1-D Tables.
 * This function can only be used for continuous state variables.
 *
 * The table for specific states is created saveStateValues, here you just have
 * to specify the states in the state list object. This function can also be
 * used for discrete states.
 */
class VFunctionAnalyzer {
protected:
	AbstractVFunction *vFunction;
	StateProperties *modelStateProperties;
	StateCollectionImpl *stateCollection;

public:
	/**
	 * Create a 1 dimensional Table of the Shape of the V-Function.
	 *
	 * A 1-dimensional Table can be created with the function save1DValues.
	 * You can choose the dimension for which the table shall be created and
	 * the number of partitions for this dimension. Additionally you can
	 * specify an init-state, this init-state is used for the state values
	 * for all other dimensions. This function can only be used for continuous
	 * state variables.
	 **/
	void save1DValues(std::ostream& stream, State *initstate, int dim1, int part1);

	/**
	 * Same as save1DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i v_i \n
	 * s_i ... Value of the continuous state variable
	 * v_i ... V-Value of the state
	 */
	void save1DMatlab(std::ostream& stream, State *initstate, int dim1, int part1);

	/**
	 * Create a 1 dimensional Table of the Shape of the V-Function.
	 *
	 * A 1-dimensional Table can be created with the function
	 * saveDiscrete1DValues. You can choose the dimension for which the table
	 * shall be created. Additionally you can specify an init-state, this
	 * init-state is used for the state values for all other dimensions.
	 * This function can only be used for discrete state variables.
	 **/
	void saveDiscrete1DValues(std::ostream& stream, State *initstate, int dim1);

	/** Same as saveDiscrete1DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i v_i \n
	 * s_i ... Discrete State Index
	 * v_i ... V-Value of the state
	 */
	void saveDiscrete1DMatlab(std::ostream& stream, State *initstate, int dim1);

	/**
	 * Create a 2 dimensional Table of the Shape of the V-Function.
	 *
	 * A 2-dimensional Table can be created with the function save2DValues.
	 * You can choose both dimension, and the number of partitions for each
	 * dimension. The init-state is used in the same way as for 1-D Tables.
	 * This function can only be used for continuous state variables.
	 **/
	void save2DValues(std::ostream& stream, State *initstate, int dim1, int part1,
	        int dim2, int part2);

	/**
	 * Same as save2DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i s_j v_i \n
	 * s_i, s_j ... Values of the first and second continuous state variables.
	 * v_i ... V-Value of the state.
	 **/
	void save2DMatlab(std::ostream& stream, State *initstate, int dim1, int part1,
	        int dim2, int part2);

	/**
	 * Create a 2 dimensional Table of the Shape of the V-Function.
	 *
	 * A 2-dimensional Table can be created with the function
	 * saveDiscrete2DValues. You can choose the dimensions for which the table
	 * shall be created. The dimension row_dim will be used as the row index,
	 * col_dim is the column index in the 2D output. The init-state is used
	 * in the same way as for 1-D Tables. This function can only be used for
	 * discrete state variables.
	 **/
	void saveDiscrete2DValues(std::ostream& stream, State *initstate, int row_dim,
	        int col_dim);

	/**
	 * Same as saveDiscrete2DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i s_j v_i \n
	 * s_i, s_j ... First and second discrete state index.
	 * v_i ... V-Value of the state.
	 **/
	void saveDiscrete2DMatlab(std::ostream& stream, State *initstate, int row_dim,
	        int col_dim);

	//! Creates a table of the values of the specified states.
	void saveStateValues(std::ostream& stream, StateList *states);

	/**
	 * Create a 1 dimensional Table of the Shape of the V-Function.
	 *
	 * A 1-dimensional Table can be created with the function save1DValues.
	 * You can choose the dimension for which the table shall be created and
	 * the number of partitions for this dimension. Additionally you can
	 * specify an init-state, this init-state is used for the state values
	 * for all other dimensions. This function can only be used for continuous
	 * state variables.
	 **/
	void save1DValues(const std::string& filename, State *initstate, int dim1, int part1);

	/**
	 * Create a 1 dimensional Table of the Shape of the V-Function.
	 *
	 * A 1-dimensional Table can be created with the function
	 * saveDiscrete1DValues. You can choose the dimension for which the table
	 * shall be created. Additionally you can specify an init-state, this
	 * init-state is used for the state values for all other dimensions.
	 * This function can only be used for discrete state variables.
	 **/
	void saveDiscrete1DValues(const std::string& filename, State *initstate, int dim1);

	/**
	 * Create a 2 dimensional Table of the Shape of the V-Function.
	 *
	 * A 2-dimensional Table can be created with the function save2DValues.
	 * You can choose both dimension, and the number of partitions for each
	 * dimension. The init-state is used in the same way as for 1-D Tables.
	 * This function can only be used for continuous state variables.
	 **/
	void save2DValues(const std::string& filename, State *initstate, int dim1, int part1,
	        int dim2, int part2);

	/**
	 * Create a 2 dimensional Table of the Shape of the V-Function.
	 *
	 * A 2-dimensional Table can be created with the function
	 * saveDiscrete2DValues. You can choose the dimensions for which the table
	 * shall be created. The dimension row_dim will be used as the row index,
	 * col_dim is the column index in the 2D output. The init-state is used
	 * in the same way as for 1-D Tables. This function can only be used for
	 * discrete state variables.
	 **/
	void saveDiscrete2DValues(const std::string& filename, State *initstate, int row_dim,
	        int col_dim);

	//! Creates a table of the values of the specified states.
	void saveStateValues(const std::string& filename, StateList *states);

	/**
	 * Same as save1DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i v_i \n
	 * s_i ... Value of the continuous state variable.
	 * v_i ... V-Value of the state.
	 */
	void save1DMatlab(const std::string& filename, State *initstate, int dim1, int part1);

	/**
	 * Same as saveDiscrete1DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i v_i \n
	 * s_i ... Discrete State Index.
	 * v_i ... V-Value of the state.
	 */
	void saveDiscrete1DMatlab(const std::string& filename, State *initstate, int dim1);

	/**
	 * Same as save2DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i s_j v_i \n
	 * s_i, s_j ... Values of the first and second continuous state variables.
	 * v_i ... V-Value of the state.
	 */
	void save2DMatlab(const std::string& filename, State *initstate, int dim1, int part1,
	        int dim2, int part2);

	/**
	 * Same as saveDiscrete2DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i s_j v_i \n
	 * s_i, s_j ... First and second discrete state index.
	 * v_i ... V-Value of the state.
	 */
	void saveDiscrete2DMatlab(const std::string& filename, State *initstate, int row_dim,
	        int col_dim);

	void setVFunction(AbstractVFunction *vFunction);

public:
	/**
	 * Create a new V-Function Analyzer.
	 *
	 * The analyzer needs the vFunction, the modelstate and all the modifiers
	 * the v-function needs. It is recommended to take the agent's state
	 * modifiers (agent->getStateModifiers();).
	 */
	VFunctionAnalyzer(
		AbstractVFunction *vFunction,
		StateProperties *modelState,
		const std::set<StateModifierList*>& modifierLists
	);

	virtual ~VFunctionAnalyzer();
};

/**
 * @class QFunctionAnalyzer
 * Analyzer for Q-Functions.
 *
 * With Q-Function Analyzers you can create different tables for showing
 * the shape of your continuous Q-Functions for different actions. These tables
 * can only be saved to a file.
 *
 * The analyzer supports:
 * - 1 dimensional Tables;
 * - 2 dimensional Tables;
 * - calculate the Value for each State in a given list.
 *
 * The tables are created for each action in the specified action set.
 *
 * A 1-dimensional Table can be created with the function save1DValues. You can
 * choose the dimension for which the table shall be created and the number
 * of partitions for this dimension. Additionally you can specify an init-state,
 * this init-state is used for the state values for all other dimensions. This
 * function can only be used for continuous state variables.
 *
 * A 2-dimensional Table can be created with the function save2DValues.
 * You can choose both dimension, and the number of partitions for each
 * dimension. The init-state is used in the same way as for 1-D Tables. This
 * function can only be used for continuous state variables.
 *
 * The table for specific states is created saveStateValues, here you just have
 * to specify the states in the state list object. This function can also be
 * used for discrete states.
 */
class QFunctionAnalyzer {
protected:
	AbstractQFunction *qFunction;
	StateProperties *modelStateProperties;
	StateCollectionImpl *stateCollection;

public:
	void setQFunction(AbstractQFunction *l_qFunction);

	/**
	 * Create a 1 dimensional Table of the Shape of the Q-Function.
	 *
	 * A 1-dimensional Table can be created with the function save1DValues.
	 * You can choose the dimension for which the table shall be created
	 * and the number of partitions for this dimension. Additionally you can
	 * specify an init-state, this init-state is used for the state values for
	 * all other dimensions. The tables are created for each action in
	 * the specified action set.
	 *
	 * This function can only be used for continuous state variables.
	 **/
	void save1DValues(std::ostream& stream, ActionSet *action, State *initstate,
	        int dim1, int part1);

	void save1DValues(const std::string& filename, ActionSet *action, State *initstate,
	        int dim1, int part1);

	//! Saves the maximum actions for a grid over one continuous state variable.
	void save1DMaxAction(std::ostream& stream, ActionSet *action, State *initstate,
	        int dim1, int part1, char *actionSymbols = nullptr);

	void save1DMaxAction(const std::string& filename, ActionSet *action, State *initstate,
	        int dim1, int part1, char *actionSymbols = nullptr);

	/**
	 * Same as saveDiscrete2DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i q_1 q_2 ... q_n \n
	 * s_i ... Value of continuous State variable.
	 * q_i ... n Q-Values (n... size of the action set).
	 **/
	void save1DMatlab(std::ostream& stream, ActionSet *action, State *initstate,
	        int dim1, int part1);

	void save1DMatlab(const std::string& filename, ActionSet *action, State *initstate,
	        int dim1, int part1);

	/**
	 * Create a 1 dimensional Table of the Shape of the Q-Function.
	 *
	 * A 1-dimensional Table can be created with the function save1DValues.
	 * You can choose the dimension for which the table shall be created.
	 * Additionally you can specify an init-state, this init-state is used
	 * for the state values for all other dimensions. The tables are created
	 * for each action in the specified action set.
	 *
	 * This function can only be used for discrete state variables.
	 **/
	void saveDiscrete1DValues(std::ostream& stream, ActionSet *action,
	        State *initstate, int dim1);

	void saveDiscrete1DValues(const std::string& filename, ActionSet *action,
	        State *initstate, int dim1);

	//! Saves the maximum action for all states of one discrete state variable.
	void saveDiscrete1DMaxAction(std::ostream& stream, ActionSet *action,
	        State *initstate, int dim1, char *actionSymbols = nullptr);

	void saveDiscrete1DMaxAction(const std::string& filename, ActionSet *action,
	        State *initstate, int dim1, char *actionSymbols = nullptr);

	/**
	 * Same as saveDiscrete2DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * d_i q_1 q_2 ... q_n \n
	 * d_i ... Discrete State Number.
	 * q_i ... n Q-Values (n... size of the action set).
	 */
	void saveDiscrete1DMatlab(std::ostream& stream, ActionSet *action,
	        State *initstate, int dim1);

	void saveDiscrete1DMatlab(const std::string& filename, ActionSet *action,
	        State *initstate, int dim1);

	/**
	 * Create a 2 dimensional Table of the Shape of the Q-Function.
	 *
	 * A 2-dimensional Table can be created with the function save2DValues.
	 * You can choose both dimension, and the number of partitions for each
	 * dimension. The init-state is used in the same way as for 1-D Tables.
	 * This function can only be used for continuous state variables.
	 * The tables are created for each action in the specified action set.
	 **/
	void save2DValues(std::ostream& stream, ActionSet *action, State *initstate,
	        int dim1, int part1, int dim2, int part2);

	void save2DValues(const std::string& filename, ActionSet *action, State *initstate,
	        int dim1, int part1, int dim2, int part2);

	//! Save maximum action for a grid over 2 continuous state variables.
	void save2DMaxAction(std::ostream& stream, ActionSet *action, State *initstate,
	        int dim1, int part1, int dim2, int part2, char *actionSymbols =
	                nullptr);

	void save2DMaxAction(const std::string& filename, ActionSet *action, State *initstate,
	        int dim1, int part1, int dim2, int part2, char *actionSymbols =
	                nullptr);

	/**
	 * Same as saveDiscrete2DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * s_i s_j q_1 q_2 ... q_n \n
	 * s_i s_j ... First and second Continuous State Variable.
	 * q_i ... n Q-Values (n... size of the action set).
	 **/
	void save2DMatlab(std::ostream& stream, ActionSet *action, State *initstate,
	        int dim1, int part1, int dim2, int part2);

	void save2DMatlab(const std::string& filename, ActionSet *action, State *initstate,
	        int dim1, int part1, int dim2, int part2);

	/**
	 * Create a 2 dimensional Table of the Shape of the Q-Function.
	 *
	 * A 2-dimensional Table can be created with the function
	 * saveDiscrete2DValues. You can choose the dimensions for which the table
	 * shall be created. The dimension row_dim will be used as the row index,
	 * col_dim is the column index in the 2D output. The init-state is used
	 * in the same way as for 1-D Tables. This function can only be used for
	 * discrete state variables. The tables are created for each action in
	 * the specified action set.
	 **/
	void saveDiscrete2DValues(std::ostream& stream, ActionSet *action,
	        State *initstate, int row_dim, int col_dim);

	void saveDiscrete2DValues(const std::string& filename, ActionSet *action,
	        State *initstate, int row_dim, int col_dim);

	//! Saves maximum action for all states of 2 discrete state variables.
	void saveDiscrete2DMaxAction(std::ostream& stream, ActionSet *action,
	        State *initstate, int row_dim, int col_dim, char *actionSymbols =
	                nullptr);

	void saveDiscrete2DMaxAction(const std::string& filename, ActionSet *action,
	        State *initstate, int row_dim, int col_dim, char *actionSymbols =
	                nullptr);

	/**
	 * Same as saveDiscrete2DValues, saves the output values in a readable
	 * matlab format.
	 *
	 * Format of a row:
	 * d_i d_j q_1 q_2 ... q_n \n
	 * d_i d_j ... First and second Discrete State Variable.
	 * q_i ... n Q-Values (n... size of the action set).
	 */
	void saveDiscrete2DMatlab(std::ostream& stream, ActionSet *action,
	        State *initstate, int row_dim, int col_dim);

	void saveDiscrete2DMatlab(const std::string& filename, ActionSet *action,
	        State *initstate, int row_dim, int col_dim);

	/**
	 * Creates a table of the values of the specified states, the tables are
	 * created for each action in the specified action set.
	 */
	void saveStateValues(std::ostream& stream, ActionSet *action, StateList *states);

	void saveStateValues(const std::string& filename, ActionSet *action,
	        StateList *states);

public:
	/**
	 * Create a new Q-Function Analyzer.
	 *
	 * The analyzer needs the qFunction, the modelstate and all the modifiers
	 * the v-function needs. It is recommended to take the agent's state
	 * modifiers (agent->getStateModifiers();).
	 */
	QFunctionAnalyzer(AbstractQFunction *qFunction,
	        StateProperties *modelState,
	        const std::set<StateModifierList*>& modifierLists);
	virtual ~QFunctionAnalyzer();
};

/**
 * @class CFunctionComparator
 * Super class of the V-Function Comparators and Q-Function comperators.
 *
 * Function comperators calculate the difference between 2 V-Functions or
 * Q-Functions. You have2 possibilities to do this:
 * - calculate the difference for N random states (with compareFunctionsRandom);
 * - calculate the difference for a given state list (with compareFunctionsStates).
 *
 * For each of these 2 functions you can define the errorfunction which should
 * be used. There are 3 different kinds of errorfunctions:
 * - MSE: calculate the mean squared error between the 2 functions;
 * - MAE: calculate the mean average error between the 2 functions;
 * - MAXERROR: calculate the maximum error between the 2 functions.
 */
class CFunctionComparator: public RewardFunction {
protected:
	StateProperties *modelStateProperties;
	StateCollectionImpl *stateCollection;

	//! Get the value of the numFunc function for the given state.
	virtual RealType getValue(int numFunc, StateCollection *state) = 0;
	//! Get the difference of the 2 functions for the given state, use the
	//! given errorfunction.
	virtual RealType getDifference(StateCollection *state, int errorFunction);

	/// Returns a random state.
	void getRandomState(State *state);

public:
	/**
	 * Compares the 2 functions for nSamples random states.
	 *
	 * For more details about the errorfunction see the class description.
	 */
	RealType compareFunctionsRandom(int nSamples, int errorFunction = 1);

	/**
	 * Compares the 2 functions for every state in the state list.
	 *
	 * For more details about the errorfunction see the class description.
	 */
	RealType compareFunctionsStates(StateList *states, int errorFunction = 1);

	RealType getReward(StateCollection *oldState, Action *action,
	        StateCollection *newState);

public:
	/**
	 * The comperator needs the properties of the model state and all modifiers
	 * the 2 functions use.
	 *
	 * It is recommended to take the agent's state modifiers
	 * (agent->getStateModifiers();).
	 */
	CFunctionComparator(StateProperties *modelState,
	        const std::set<StateModifierList*>& modifierLists);

	virtual ~CFunctionComparator();
};

/**
 * @class VFunctionComparator
 * Comparator for 2 V-Functions.
 *
 * See the description of the super class for more details.
 */
class VFunctionComparator: public CFunctionComparator {
protected:
	AbstractVFunction *vFunction1;
	AbstractVFunction *vFunction2;

	virtual RealType getValue(int numFunc, StateCollection *state);

public:
	VFunctionComparator(
		StateProperties *modelState,
		const std::set<StateModifierList*>& modifierLists,
		AbstractVFunction *vFunction1,
		AbstractVFunction *vFunction2
	);

	virtual ~VFunctionComparator() {}
};

/**
 * Comparator for 2 Q-Functions.
 *
 * The 2 Q-Functions are compared for the given action in the constructor.
 * See the description of the super class for more details.
 */
class QFunctionComparator: public CFunctionComparator {
protected:
	AbstractQFunction *qFunction1;
	AbstractQFunction *qFunction2;
	Action *action;

	virtual RealType getValue(int numFunc, StateCollection *state);

public:
	QFunctionComparator(
		StateProperties *modelState,
		const std::set<StateModifierList*>& modifierLists,
		AbstractQFunction *qFunction1,
		AbstractQFunction *qFunction2,
		Action *action
	);

	virtual ~QFunctionComparator() {}
};

/**
 * Controller analyzer.
 *
 * The controller analyzer supports creating a table of the chosen action
 * for every given state in the state list. This can be done by the function
 * saveActions. The function additionally needs all the modifiers used by
 * the controllers, which are usually the modifiers of the used
 * V or Q Functions.
 */
class ControllerAnalyzer: public ActionObject {
protected:
	StateList *states;
	AgentController *controller;

public:
	StateList *getStateList();
	void setStateList(StateList *states);

	AgentController *getController();
	void setController(AgentController *Controller);

	void saveActions(std::ostream& stream, const std::set<StateModifierList*>& modifierLists);

public:
	ControllerAnalyzer(
		StateList *states,
		AgentController *controller,
		ActionSet *actions
	);

	virtual ~ControllerAnalyzer();
};

class FittedQIterationAnalyzer:
	public FittedQIteration, public TestSuiteEvaluatorLogger
{
protected:
	PolicySameStateEvaluator *evaluator;

	int numEvaluations;
	shared_ptr<std::ostream> analyzerFile;

	State *buffState2;
	bool useQValues;

	RealType lastQValue;
	RealType lastEstimatedQValue;

	virtual RealType getValue(StateCollection *stateCollection, Action *action);

public:
	virtual void addResidualInput(Step *step, Action *action, RealType V,
	        RealType newV, RealType nearestNeighborDistance,
	        Action *nextHistoryAction = nullptr, RealType nextReward = 0.0);

	virtual void evaluate(std::string evaluationDirectory, int trial,
	        int numEpisodes);

	virtual void startNewEvaluation(std::string evaluationDirectory,
	        Parameters *parameters, int trial);

public:
	FittedQIterationAnalyzer(QFunction *qFunction,
	        AgentController *estimationPolicy, EpisodeHistory *episodeHistory,
	        RewardHistory *rewardLogger, SupervisedQFunctionLearner *learner,
	        StateProperties *residualProperties,
	        PolicySameStateEvaluator *evaluator);

	virtual ~FittedQIterationAnalyzer();
};

} //namespace rlearner

#endif //Rlearner_canalyzer_H_
