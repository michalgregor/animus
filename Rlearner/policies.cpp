// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>

#include "ril_debug.h"
#include "policies.h"
#include "utility.h"
#include "theoreticalmodel.h"
#include "qfunction.h"
#include "vfunction.h"
#include "actionstatistics.h"
#include "stateproperties.h"
#include "state.h"
#include "statecollection.h"
#include "transitionfunction.h"
#include "featurefunction.h"

#include <Systematic/math/Compare.h>
#include "system.h"

namespace rlearner {

QGreedyPolicy::QGreedyPolicy(ActionSet *actions,
        AbstractQFunction *qFunction
):
	AgentController(actions)
{
	this->qFunction = qFunction;
	availableActions = new ActionSet();
}

QGreedyPolicy::~QGreedyPolicy() {
	delete availableActions;
}

Action *QGreedyPolicy::getNextAction(StateCollection *state,
        ActionDataSet *data) {
	getActions()->getAvailableActions(availableActions, state);
	return qFunction->getMax(state, availableActions, data);
}

void ActionDistribution::getGradientFactors(
	StateCollection *, Action *, ActionSet *, RealType *, ColumnVector *
) {}

SoftMaxDistribution::SoftMaxDistribution(RealType beta) {
	addParameter("SoftMaxBeta", beta);
}

void SoftMaxDistribution::getDistribution(
	StateCollection*, ActionSet* availableActions, RealType* values
) {
	RealType sum = 0.0;
	RealType beta = getParameter("SoftMaxBeta");
	unsigned int i;
	unsigned int numValues = availableActions->size();

	auto result = std::minmax_element(values, values + numValues);
	auto minValue = *result.first, maxValue = *result.second;

	if (beta * (maxValue - minValue) > MAX_EXP) {
		beta = MAX_EXP / (maxValue - minValue);
	}

	for (i = 0; i < numValues; i++) {
		RealType temp = exp(beta * (values[i] - minValue));

		if (temp != temp) {
			printf("Too large a value for the exp function (%f) : %f %f %f\n", temp,
			        beta * (values[i] - minValue), values[i], minValue);
		}

		values[i] = temp;

		sum += values[i];
	}

	assert(sum > 0);
	for (i = 0; i < numValues; i++) {
		values[i] = values[i] / sum;

		if (!(values[i] >= 0 && values[i] <= 1.000001)) {
			printf("That is not a Probability: %1.10f\n", values[i]);
		}
		assert(values[i] >= 0 && values[i] <= 1.000001);
	}
}

void SoftMaxDistribution::getGradientFactors(StateCollection *,
        Action *usedAction, ActionSet *availableActions, RealType *actionValues,
        ColumnVector *factors) {
	int numValues = availableActions->size();
	RealType normTerm = 0.0;
	RealType beta = getParameter("SoftMaxBeta");
	int actIndex = availableActions->getIndex(usedAction);

	RealType minValue = actionValues[0];
	RealType maxValue = actionValues[0];

	DebugPrint('p', "SoftMax Gradient Factors:\n");

	for (int i = 0; i < numValues; i++) {
		if (minValue > actionValues[i]) {
			minValue = actionValues[i];
		}
		if (maxValue < actionValues[i]) {
			maxValue = actionValues[i];
		}
		DebugPrint('p', "%f ", actionValues[i]);
	}
	DebugPrint('p', "\n");

	if (beta * (maxValue - minValue) > 200) {
		beta = 200 / (maxValue - minValue);
	}

	for (int i = 0; i < numValues; i++) {
		normTerm += exp(beta * (actionValues[i] - minValue));
	}

	RealType buf = exp(beta * (actionValues[actIndex] - minValue));

	DebugPrint('p', "Beta:%f\n", normTerm);

	for (int i = 0; i < numValues; i++) {
		factors->operator[](i) = -beta * buf
		        * exp(beta * (actionValues[i] - minValue))
		        / pow(normTerm, (RealType) 2.0);
	}
	factors->operator[](actIndex) = factors->operator[](actIndex)
	        + buf * beta / normTerm;

	DebugPrint('p', "SoftMax Gradient Factors:\n");
	for (int i = 0; i < numValues; i++) {
		DebugPrint('p', "%f ", factors->operator[](i));
	}
	DebugPrint('p', "\n");
}

AbsoluteSoftMaxDistribution::AbsoluteSoftMaxDistribution(
RealType absoluteValue) {
	addParameter("SoftMaxAbsoluteValue", absoluteValue);
}

void AbsoluteSoftMaxDistribution::getDistribution(StateCollection *,
        ActionSet *availableActions, RealType *values) {
	RealType sum = 0.0;
	RealType absoluteValue = getParameter("SoftMaxAbsoluteValue");
	unsigned int i;
	unsigned int numValues = availableActions->size();
	RealType beta = 0.0;

	RealType minValue = values[0];
	RealType maxValue = values[0];

	for (i = 1; i < numValues; i++) {
		if (minValue > values[i]) {
			minValue = values[i];
		}
		if (maxValue < values[i]) {
			maxValue = values[i];
		}
	}

	if ((fabs(maxValue) <= 0.0000001 && fabs(minValue) <= 0.0000001)) {
		beta = 100;
	} else {
		if (fabs(maxValue) < fabs(minValue)) {
			beta = absoluteValue / (fabs(minValue));
		} else {
			beta = absoluteValue / (fabs(maxValue));
		}
	}

	if (beta * fabs((maxValue - minValue)) > 400) {
		beta = 400 / fabs(maxValue - minValue);
	}

	for (i = 0; i < numValues; i++) {
		values[i] = exp(beta * (values[i] - minValue));
		sum += values[i];
	}
	assert(sum > 0);
	for (i = 0; i < numValues; i++) {
		values[i] = values[i] / sum;
		assert(values[i] >= 0 && values[i] <= 1);
	}
}

void GreedyDistribution::getDistribution(StateCollection *,
        ActionSet *availableActions, RealType *actionValues) {
	unsigned int numValues = availableActions->size();
	SYS_ASSERT(numValues);

	if (_multiGreedy) {
		RealType max = actionValues[0];
		std::vector<unsigned int> maxIndex(1, 0);

		actionValues[0] = 0.0;

		for (unsigned int i = 1; i < numValues; i++) {
			if (actionValues[i] > max) {
				max = actionValues[i];
				maxIndex.assign(1, i);
			} else if (Compare(actionValues[i], max)) {
				maxIndex.push_back(i);
			}
			actionValues[i] = 0.0;
		}

		numValues = static_cast<unsigned int>(maxIndex.size());
		RealType probability = 1 / static_cast<RealType>(numValues);
		for (unsigned int i = 0; i < numValues; i++) {
			actionValues[maxIndex[i]] = probability;
		}

	} else {
		RealType max = actionValues[0];
		int maxIndex = 0;

		actionValues[0] = 0.0;

		for (unsigned int i = 1; i < numValues; i++) {
			if (actionValues[i] > max) {
				max = actionValues[i];
				maxIndex = i;
			}
			actionValues[i] = 0.0;
		}
		actionValues[maxIndex] = 1.0;
	}
}

EpsilonGreedyDistribution::EpsilonGreedyDistribution(RealType epsilon,
        bool multiGreedy) :
		_multiGreedy(multiGreedy) {
	addParameter("EpsilonGreedy", epsilon);
}

void EpsilonGreedyDistribution::getDistribution(StateCollection *,
        ActionSet *availableActions, RealType *actionValues) {
	unsigned int numValues = availableActions->size();
	SYS_ASSERT(numValues);

	RealType epsilon = getParameter("EpsilonGreedy");
	RealType prop = epsilon / numValues;

	if (_multiGreedy) {
		RealType max = actionValues[0];
		std::vector<unsigned int> maxIndex(1, 0);
		actionValues[0] = prop;

		for (unsigned int i = 1; i < numValues; i++) {
			if (actionValues[i] > max) {
				max = actionValues[i];
				maxIndex.assign(1, i);
			} else if (Compare(actionValues[i], max)) {
				maxIndex.push_back(i);
			}
			actionValues[i] = prop;
		}

		numValues = static_cast<unsigned int>(maxIndex.size());
		prop += (1 - epsilon) / static_cast<RealType>(numValues);

		for (unsigned int i = 0; i < numValues; i++) {
			actionValues[maxIndex[i]] = prop;
		}
	} else {
		RealType max = actionValues[0];
		int maxIndex = 0;

		for (unsigned int i = 0; i < numValues; i++) {
			if (actionValues[i] > max) {
				max = actionValues[i];
				maxIndex = i;
			}
			actionValues[i] = prop;
		}
		actionValues[maxIndex] += 1 - epsilon;
	}
}

StochasticPolicy::StochasticPolicy(ActionSet *actions,
        ActionDistribution *distribution) :
		AgentStatisticController(actions) {
	actionValues = new RealType[actions->size()];
	this->distribution = distribution;

	addParameters(distribution);

	gradientFactors = new ColumnVector(actions->size());
	actionGradientFeatures = new FeatureList();
	availableActions = new ActionSet();
}

StochasticPolicy::~StochasticPolicy() {
	delete[] actionValues;
	delete gradientFactors;
	delete actionGradientFeatures;
	delete availableActions;
}

void StochasticPolicy::getActionProbabilities(StateCollection *state,
        ActionSet *availableActions, RealType *actionValues,
        ActionDataSet *actionDataSet) {
	getActionValues(state, availableActions, actionValues, actionDataSet);
	distribution->getDistribution(state, availableActions, actionValues);
}

Action *StochasticPolicy::getNextAction(StateCollection *state,
        ActionDataSet *dataSet, ActionStatistics *stat) {
	auto rand = make_rand();

	getActions()->getAvailableActions(availableActions, state);
	assert(availableActions->size() > 0);

	getActionProbabilities(state, availableActions, actionValues, dataSet);

	RealType sum = actionValues[0];
	ActionSet::iterator it = availableActions->begin();
	RealType z = (RealType) rand() / (RAND_MAX);
	unsigned int i = 0;

	while (sum <= z && i < availableActions->size() - 1) {
		i++;
		it++;
		sum += actionValues[i];
	}

	if (stat != nullptr) {
		stat->owner = this;
		getActionStatistics(state, (*it), stat);
	}

	DebugPrint('p', "ActionPropabilities: ");
	for (unsigned int j = 0; j < availableActions->size(); j++) {
		DebugPrint('p', "%f ", actionValues[j]);
	}
	DebugPrint('p', "\nChoosed Action: %d\n", actions->getIndex(*it));

	Action *action = *it;
	return action;
}

void StochasticPolicy::getActionProbabilityGradient(StateCollection *state,
        Action *action, ActionData *, FeatureList *gradientState) {
	gradientState->clear();

	if (isDifferentiable()) {
		getActionValues(state, actions, this->actionValues);
		distribution->getGradientFactors(state, action, actions, actionValues,
		        gradientFactors);

		ActionSet::iterator it = actions->begin();

		for (int j = 0; it != actions->end(); it++, j++) {
			actionGradientFeatures->clear();
			getActionGradient(state, *it, nullptr, actionGradientFeatures);
			FeatureList::iterator itFeat = actionGradientFeatures->begin();

			for (; itFeat != actionGradientFeatures->end(); itFeat++) {
				gradientState->update((*itFeat)->featureIndex,
				        (*itFeat)->factor * gradientFactors->operator[](j));
			}
		}
	}

	if (DebugIsEnabled('p')) {
		DebugPrint('p', "Policy Gradient Factors:\n");
		gradientState->save(DebugGetFileHandle('p'));
		DebugPrint('p', "\n");
	}
}

void StochasticPolicy::getActionProbabilityLnGradient(StateCollection *state,
        Action *action, ActionData *data, FeatureList *gradientState) {
	RealType prop = 0.0;

	getActionProbabilities(state, actions, actionValues);

	prop = actionValues[actions->getIndex(action)];

	getActionProbabilityGradient(state, action, data, gradientState);

	gradientState->multFactor(1 / prop);

	if (DebugIsEnabled('p')) {
		DebugPrint('p', "Policy Gradient Ln Factors:\n");
		gradientState->save(DebugGetFileHandle('p'));
		DebugPrint('p', "\n");
	}
}

void StochasticPolicy::getActionGradient(StateCollection *, Action *,
        ActionData *, FeatureList *) {
}

QStochasticPolicy::QStochasticPolicy(ActionSet *actions,
        ActionDistribution *distribution, AbstractQFunction *qfunction) :
		StochasticPolicy(actions, distribution)
{
	this->qfunction = qfunction;
	addParameters(qfunction);
}

QStochasticPolicy::~QStochasticPolicy() {
}

bool QStochasticPolicy::isDifferentiable() {
	return (distribution->isDifferentiable()
	        && qfunction->isType(GRADIENTQFUNCTION));
}

void QStochasticPolicy::getActionGradient(StateCollection *state,
        Action *action, ActionData *data, FeatureList *gradientState) {
	gradientState->clear();

	if (isDifferentiable()) {
		GradientQFunction *gradQFunc =
		        dynamic_cast<GradientQFunction *>(qfunction);
		gradQFunc->getGradient(state, action, data, gradientState);
	}

}

void QStochasticPolicy::getActionStatistics(StateCollection *state,
        Action *action, ActionStatistics *stat) {
	this->qfunction->getStatistics(state, action, qfunction->getActions(),
	        stat);
}

void QStochasticPolicy::getActionValues(StateCollection *state,
        ActionSet *availableActions, RealType *actionValues, ActionDataSet *) {
	for (unsigned int i = 0; i < availableActions->size(); actionValues[i++] =
	        0.0) {
	}
	qfunction->getActionValues(state, availableActions, actionValues);
}
/*
 void QStochasticPolicy::updateGradient(FeatureList *gradient, RealType factor)
 {
 if (qfunction->isType(GRADIENTQFUNCTION))
 {
 GradientQFunction *gradQFunc = dynamic_cast<GradientQFunction *>(qfunction);
 gradQFunc->updateGradient(gradient, factor);
 }
 }

 int QStochasticPolicy::getNumWeights()
 {
 if (qfunction->isType(GRADIENTQFUNCTION))
 {
 return dynamic_cast<GradientQFunction *>(qfunction)->getNumWeights();
 }
 else
 {
 return 0;
 }
 }*/

VMStochasticPolicy::VMStochasticPolicy(ActionSet *actions,
        ActionDistribution* distribution, AbstractVFunction* vFunction,
        TransitionFunction* model, RewardFunction* reward,
        const std::set<StateModifierList*>& modifierLists
):
	QStochasticPolicy(
		actions, distribution,
		new QFunctionFromTransitionFunction(actions, vFunction, model,
			reward, modifierLists)
	),
	_nextState(new StateCollectionImpl(model->getStateProperties())),
	_intermediateState(new StateCollectionImpl(model->getStateProperties())),
	_vFunction(vFunction),
	_model(model),
	_reward(reward)
{
	addParameters(vFunction);
//	addParameters(model);
	addParameter("DiscountFactor", 0.95);

	for(auto& list: modifierLists) {
		_nextState->addModifierList(list);
		_intermediateState->addModifierList(list);
	}
}

VMStochasticPolicy::~VMStochasticPolicy() {
	delete _nextState;
	delete _intermediateState;
}

/*
 void VMStochasticPolicy::getActionValues(StateCollection *state, ActionSet *availableActions, RealType *actionValues,  ActionDataSet *actionDataSet)
 {
 ActionSet::iterator it = availableActions->begin();
 for (int i = 0; it != availableActions->end(); it ++, i++)
 {
 PrimitiveAction *primAction = ((PrimitiveAction *)(*it));
 int duration = 1;
 if (primAction->isStateToChange())
 {
 StateCollectionImpl *buf = nullptr;
 nextState->getState(model->getStateProperties())->setState(state->getState(model->getStateProperties()));

 MultiStepActionData *data = nullptr;
 if (actionDataSet)
 {
 data = dynamic_cast<MultiStepActionData *>(actionDataSet->getActionData(*it));
 }
 duration = 0;
 do
 {
 //exchange Model State

 buf = intermediateState;
 intermediateState = nextState;
 nextState = buf;

 model->transitionFunction(intermediateState->getState(model->getStateProperties()), (*it), nextState->getState(model->getStateProperties()));
 nextState->newModelState();
 duration += primAction->getSingleExecutionDuration();
 }
 // Execute the action until the state changed
 while (duration < primAction->maxStateToChangeDuration && !primAction->stateChanged(state, nextState));

 if (data)
 {
 data->duration = duration;
 }
 }
 else
 {
 model->transitionFunction(state->getState(model->getStateProperties()), *it, nextState->getState(model->getStateProperties()));
 nextState->newModelState();
 }

 if (actionDataSet && (*it)->isType(AF_MultistepAction))
 {
 ActionData *actionData = actionDataSet->getActionData(*it);
 MultiStepActionData *multiStepActionData  = dynamic_cast<MultiStepActionData *>(actionData);
 duration = multiStepActionData->duration;
 }
 else
 {
 duration = (*it)->getDuration();
 }

 RealType rewardValue = reward->getReward(state, *it, nextState);
 RealType value = vFunction->getValue(nextState);
 actionValues[i] = rewardValue + pow(getParameter("DiscountFactor"), duration) * value;

 if (DebugIsEnabled('p'))
 {
 DebugPrint('p', "VM Stochastic Policy: Action %d, State: ",i);
 nextState->getState()->saveASCII(DebugGetFileHandle('p'));
 DebugPrint('p', ", functionValue: %f, reward %f\n", value, rewardValue);
 }

 }
 }*/

void VMStochasticPolicy::getActionGradient(StateCollection *state,
        Action *action, ActionData *data, FeatureList *gradientState)
{
	gradientState->clear();

	if (isDifferentiable()) {
		_model->transitionFunction(state->getState(_model->getStateProperties()),
		        action, _nextState->getState(_model->getStateProperties()), data);
		_nextState->newModelState();

		GradientVFunction *gradVFunc =
		        dynamic_cast<GradientVFunction *>(_vFunction);

		gradVFunc->getGradient(_nextState, gradientState);

		int duration = 1;

		if (data && action->isType(AF_MultistepAction)) {
			duration = dynamic_cast<MultiStepActionData *>(data)->duration;
		} else {
			duration = action->getDuration();
		}

		gradientState->multFactor(
		        pow(getParameter("DiscountFactor"), duration));
	}
}

bool VMStochasticPolicy::isDifferentiable() {
	return (distribution->isDifferentiable()
	        && _vFunction->isType(GRADIENTVFUNCTION));
}

} //namespace rlearner
