// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_ccontinuousactiongradientpolicy_H_
#define Rlearner_ccontinuousactiongradientpolicy_H_

#include <list>
#include <map>
#include <vector>

#include "parameters.h"
#include "baseobjects.h"
#include "continuousactions.h"
#include "gradientfunction.h"
#include "StateModifierList.h"

namespace rlearner {

class StateProperties;
class StateCollection;
class StateCollectionImpl;
class FeatureList;
class FeatureCalculator;
class FeatureVFunction;
class VFunctionInputDerivationCalculator;

class StateProperties;

class CAGradientPolicyInputDerivationCalculator: virtual public ParameterObject {
public:
	virtual void getInputDerivation(StateCollection *inputState,
	        Matrix *targetVector) = 0;

public:
	virtual ~CAGradientPolicyInputDerivationCalculator() = default;
};

class ContinuousActionGradientPolicy: public ContinuousActionController,
        public GradientFunction,
        public StateObject {
protected:
	StateProperties *modelState;

protected:
	virtual void updateWeights(FeatureList *dParams) = 0;

public:
	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *action) = 0;

	virtual int getNumWeights() = 0;

	virtual void getWeights(RealType *parameters) = 0;
	virtual void setWeights(RealType *parameters) = 0;

	virtual void getGradient(StateCollection *inputState, int outputDimension,
	        FeatureList *gradientFeatures) = 0;
	virtual void getGradientPre(ColumnVector *input, ColumnVector *outputErrors,
	        FeatureList *gradientFeatures);

	virtual void getFunctionValuePre(ColumnVector *input, ColumnVector *output);

	virtual void resetData() = 0;

public:
	ContinuousActionGradientPolicy& operator=(const ContinuousActionGradientPolicy&) = delete;
	ContinuousActionGradientPolicy(const ContinuousActionGradientPolicy&) = delete;

	ContinuousActionGradientPolicy(ContinuousAction *contAction,
	        StateProperties *modelState);
	virtual ~ContinuousActionGradientPolicy();
};

class ContinuousActionPolicyFromGradientFunction: public ContinuousActionGradientPolicy,
        public CAGradientPolicyInputDerivationCalculator {
protected:
	GradientFunction *gradientFunction;
	ColumnVector *outputError;

protected:
	virtual void updateWeights(FeatureList *dParams);

public:
	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *action);
	virtual void getGradient(StateCollection *inputState, int outputDimension,
	        FeatureList *gradientFeatures);
	virtual void getInputDerivation(StateCollection *inputState,
	        Matrix *targetVector);

	virtual int getNumWeights();

	virtual void getWeights(RealType *parameters);
	virtual void setWeights(RealType *parameters);

	virtual void resetData();

public:
	ContinuousActionPolicyFromGradientFunction& operator=(const ContinuousActionPolicyFromGradientFunction&) = delete;
	ContinuousActionPolicyFromGradientFunction(const ContinuousActionPolicyFromGradientFunction&) = delete;

	ContinuousActionPolicyFromGradientFunction(ContinuousAction *contAction,
	        GradientFunction *gradientFunction, StateProperties *modelState);
	virtual ~ContinuousActionPolicyFromGradientFunction();
};

class ContinuousActionFeaturePolicy: public ContinuousActionGradientPolicy,
        public CAGradientPolicyInputDerivationCalculator {
protected:
	std::list<FeatureCalculator *> *featureCalculators;
	std::list<FeatureVFunction *> *featureFunctions;

protected:
	virtual void updateWeights(FeatureList *dParams);

	int numWeights;
	FeatureList *localGradient;
	ColumnVector *inputDerivation;

	std::map<FeatureVFunction *, VFunctionInputDerivationCalculator *> *inputDerivationFunctions;
	StateModifierList _modifierList;

public:
	virtual int getNumWeights();

	virtual void getWeights(RealType *parameters);
	virtual void setWeights(RealType *parameters);

	virtual void resetData();

	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *action);
	virtual void getGradient(StateCollection *inputState, int outputDimension,
	        FeatureList *gradientFeatures);
	virtual void getInputDerivation(StateCollection *inputState,
	        Matrix *targetVector);

public:
	ContinuousActionFeaturePolicy& operator=(const ContinuousActionFeaturePolicy&) = delete;
	ContinuousActionFeaturePolicy(const ContinuousActionFeaturePolicy&) = delete;

	ContinuousActionFeaturePolicy(ContinuousAction *contAction,
	        StateProperties *modelState,
	        std::list<FeatureCalculator *> *featureCalcualtors);
	virtual ~ContinuousActionFeaturePolicy();
};

class ContinuousActionSigmoidPolicy: public ContinuousActionGradientPolicy,
        public CAGradientPolicyInputDerivationCalculator {
protected:
	ContinuousActionGradientPolicy *policy;
	CAGradientPolicyInputDerivationCalculator *inputDerivation;

	ContinuousActionData *contData;

protected:
	virtual void updateWeights(FeatureList *dParams);

public:
	virtual int getNumWeights();

	virtual void getWeights(RealType *parameters);
	virtual void setWeights(RealType *parameters);

	virtual void resetData();

	virtual void getNextContinuousAction(StateCollection *state,
	        ContinuousActionData *action);
	virtual void getGradient(StateCollection *inputState, int outputDimension,
	        FeatureList *gradientFeatures);
	virtual void getInputDerivation(StateCollection *inputState,
	        Matrix *targetVector);

	virtual void getNoise(StateCollection *state,
	        ContinuousActionData *action, ContinuousActionData *l_noise);

public:
	ContinuousActionSigmoidPolicy& operator=(const ContinuousActionSigmoidPolicy&) = delete;
	ContinuousActionSigmoidPolicy(const ContinuousActionSigmoidPolicy&) = delete;

	ContinuousActionSigmoidPolicy(ContinuousActionGradientPolicy *policy,
		        CAGradientPolicyInputDerivationCalculator *inputDerivation);
	virtual ~ContinuousActionSigmoidPolicy();
};

class CAGradientPolicyNumericInputDerivationCalculator: public CAGradientPolicyInputDerivationCalculator {
protected:
	ContinuousActionGradientPolicy *policy;

	ContinuousActionData *contDataPlus;
	ContinuousActionData *contDataMinus;

	StateCollectionImpl *stateBuffer;

public:
	virtual void getInputDerivation(StateCollection *inputState,
	        Matrix *targetVector);

public:
	CAGradientPolicyNumericInputDerivationCalculator& operator=(const CAGradientPolicyNumericInputDerivationCalculator&) = delete;
	CAGradientPolicyNumericInputDerivationCalculator(const CAGradientPolicyNumericInputDerivationCalculator&) = delete;

	CAGradientPolicyNumericInputDerivationCalculator(
	        ContinuousActionGradientPolicy *policy, RealType stepSize,
	        const std::set<StateModifierList*>& modifierLists);
	virtual ~CAGradientPolicyNumericInputDerivationCalculator();
};

} //namespace rlearner

#endif //Rlearner_ccontinuousactiongradientpolicy_H_
