// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cadaptivesoftmaxnetwork_H_
#define Rlearner_cadaptivesoftmaxnetwork_H_

#include <iostream>
#include <vector>
#include <list>
#include <map>

#include "learndataobject.h"
#include "statemodifier.h"

namespace rlearner {

class RBFBasisFunction;

class DataSubset;
class DataSet;

class KDTree;
class KNearestNeighbors;

class StateCollection;
class State;
class StateProperties;

class RBFCenterNetwork: public LearnDataObject {
protected:
	int numDim;
	std::vector<RBFBasisFunction*> *centers;

protected:
	virtual void initNetwork() {}

public:
	virtual void getNearestNeigbors(ColumnVector *state, unsigned int K,
	        DataSubset *subset) = 0;
	virtual void saveData(std::ostream& stream);
	virtual void loadData(std::istream& stream);
	virtual void resetData() {}
	RBFBasisFunction *getCenter(int index);
	int getNumCenters();
	virtual void deleteCenters();
	int getNumDimensions();

protected:
	RBFCenterNetwork(std::vector<RBFBasisFunction *> *centers, int numDim);

public:
	RBFCenterNetwork& operator=(const RBFCenterNetwork&) = delete;
	RBFCenterNetwork(const RBFCenterNetwork&) = delete;

	RBFCenterNetwork(int numDim);
	virtual ~RBFCenterNetwork();
};

class RBFCenterNetworkSimpleSearch: public RBFCenterNetwork {
protected:
	virtual void initNetwork() {}

public:
	virtual void getNearestNeigbors(ColumnVector *state, unsigned int K,
	        DataSubset *subset);

public:
	RBFCenterNetworkSimpleSearch(int numDim);
	virtual ~RBFCenterNetworkSimpleSearch();
};

class RBFCenterNetworkKDTree: public RBFCenterNetwork {
protected:
	unsigned int K;

	DataSet *dataset;
	KDTree *tree;
	KNearestNeighbors *nearestNeighbors;

protected:
	virtual void initNetwork();

public:
	virtual void getNearestNeigbors(ColumnVector *state, unsigned int K,
	        DataSubset *subset);

public:
	RBFCenterNetworkKDTree& operator=(const RBFCenterNetworkKDTree&) = delete;
	RBFCenterNetworkKDTree(const RBFCenterNetworkKDTree&) = delete;

	RBFCenterNetworkKDTree(int numDim, unsigned int K);
	virtual ~RBFCenterNetworkKDTree();
};

class RBFCenterFeatureCalculator: public FeatureCalculator {
protected:
	RBFCenterNetwork *network;

public:
	bool useBias;
	bool normalizeFeat;

public:
	virtual void getModifiedState(StateCollection *state, State *targetState);

public:
	RBFCenterFeatureCalculator& operator=(const RBFCenterFeatureCalculator&) = delete;
	RBFCenterFeatureCalculator(const RBFCenterFeatureCalculator&) = delete;

	RBFCenterFeatureCalculator(StateProperties *stateProperties,
	        RBFCenterNetwork *network, unsigned int K);
	virtual ~RBFCenterFeatureCalculator();
};

} //namespace rlearner

#endif //Rlearner_cadaptivesoftmaxnetwork_H_
