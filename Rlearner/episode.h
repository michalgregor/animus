// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cepisode_H_
#define Rlearner_cepisode_H_

#include <vector>
#include <vector>
#include <map>
#include <iostream>

#include "history.h"
#include "agentlistener.h"

namespace rlearner {

class State;
class StateProperties;
class StateCollection;
class StateCollectionList;
class StateModifier;
class StateList;
class Action;
class ActionList;
class ActionDataSet;
class StateModifierList;

/**
 * @class Episode
 * Class for logging a single Episode.
 *
 * The episodes objects (Episode) can store only one episode. The episode
 * stores numSteps actions and numSteps + 1 states, therefore it uses a
 * ActionList and a StateCollectionList object. The episodes are also
 * listeners, so you can add them to the agent's listeners list and they will
 * log the current episode. When a newEpisode event occurs the episode object
 * is cleared. The agent logger uses a list Episode objects to log the whole
 * training trial. There are always numSteps + 1 states and numSteps actions
 * in the lists (except numSteps = 0, then both lists are empty).
 *
 * The episode stores only those states which's state modifier objects
 * (i.e. the properties) are in the state modifier list of the logger. For
 * retrieving state collections or action objects, the methods from the
 * StateCollectionList and ActionList objects are used. The episode
 * implements the interface StepHistory, so you can retrieve random steps
 * for learning from any episode. This is an alternative way of updating
 * your Q or V Functions.
 *
 * @see EpisodeHistory
 * @see AgentLogger
 */
class Episode: public SemiMDPListener, public StepHistory {
protected:
	//! Clear Episode when a newEpisode event occurs, default = true.
	bool autoNewEpisode;

	//! State collection list for the numSteps + 1 states.
	StateCollectionList* stateCollectionList;
	//! Action list for the numSteps actions.
	ActionList* actionList;

public:
	/**
	 * Stores the newState and the action, if it was the first step, the
	 * oldState object is also saved (before newState).
	 */
	virtual void nextStep(StateCollection *oldState, Action *action,
	        StateCollection *newState);

	//! If autoNewEpisode is true, the episode gets cleared.
	virtual void newEpisode();

	StateList *getStateList(StateProperties *properties);

	//! Get a state collection from the Episode, just calls the equivalent
	//! function of the StateCollectionList object.
	void getStateCollection(int index, StateCollectionImpl *stateCollection);
	//! Get a state from the Episode, just calls the equivalent function
	//! of the StateCollectionList object.
	void getState(int index, State *state);

	//! Add state modifier list to the episodes state collection list.
	virtual void addModifierList(StateModifierList* modifierList);
	//! Remove state modifier list to the episodes state collection list.
	virtual void removeModifierList(StateModifierList* modifierList);

	//! Get an action from the Episode, just calls the equivalent function
	//! of the ActionList object.
	Action *getAction(unsigned int num, ActionDataSet *dataSet = nullptr);

	/**
	 * Save as text.
	 *
	 * Uses the save functions from the state collection list and the action
	 * list object.
	 */
	virtual void saveData(std::ostream& stream);

	/**
	 * Load from a text.
	 *
	 * Uses the load functions from the state collection list and the action
	 * list object.
	 */
	virtual void loadData(std::istream& stream);

	//! Returns the number of steps logged by the episode.
	virtual int getNumSteps();

	//! Returns the num-th step.
	virtual void getStep(int num, Step *step);

	virtual int getNumStateCollections();

	virtual void resetData();

public:
	Episode& operator=(const Episode&) = delete;
	Episode(const Episode&) = delete;

	//! Creates an Episode object, the base state of the collection list
	//! has the properties "properties".
	Episode(StateProperties *properties, ActionSet *actions,
	        bool autoNewEpisode = true);
	/**
	 * Creates an Episode object, the base state of the collection list has
	 * the properties "properties".
	 *
	 * Initializes the collection list with the modifiers.
	 */
	Episode(StateProperties *properties, ActionSet *actions,
	        const std::set<StateModifierList*>& modifierLists, bool autoNewEpisode = true);
	virtual ~Episode();
};

} //namespace rlearner

#endif //Rlearner_cepisode_H_
