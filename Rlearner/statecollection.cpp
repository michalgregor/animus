// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>

#include "ril_debug.h"
#include "statecollection.h"
#include "stateproperties.h"
#include "statemodifier.h"

namespace rlearner {

StateCollectionImpl::StateCollectionImpl(StateProperties* modelProperties):
	StateModifiersObject(modelProperties),
	_modelState(new State(modelProperties)), _modifiedStates() {}

StateCollectionImpl::StateCollectionImpl(StateCollectionImpl *stateCol):
	StateModifiersObject(stateCol->getState()->getStateProperties()),
	_modelState(new State(stateCol->getState())), _modifiedStates()
{
	for(auto& list: stateCol->getModifierLists()) {
		addModifierList(list);
	}
}

StateCollectionImpl::StateCollectionImpl(
    StateProperties* properties,
    const std::set<StateModifierList*>& modifierLists
):
	StateModifiersObject(properties),
	_modelState(new State(properties)), _modifiedStates()
{
	for(auto& list: modifierLists) {
		addModifierList(list);
	}
}

StateCollectionImpl::~StateCollectionImpl() {
	// remove the reporter from any modifierLists
	for(auto& modifierList: _modifierLists) {
		modifierList->removeReporter(&_reporter);
	}

	for(auto& item: _modifiedStates) {
		if(item.first->changeState) {
			item.first->removeStateCollection(this);
		}
	}
}

void StateCollectionImpl::setStateCollection(StateCollection* stateCollection) {
	_modelState->setState(
	    stateCollection->getState(_modelState->getStateProperties())
	);

	newModelState();

	for(auto& item: _modifiedStates) {
		if(stateCollection->isMember(item.first)) {
			State *targetState = item.second.state.get();
			targetState->setState(stateCollection->getState(item.first));
			item.second.calculated = true;
		}
	}

	setResetState(stateCollection->isResetState());
}

void StateCollectionImpl::setResetState(bool reset) {
	_modelState->setResetState(reset);

	for(auto& item: _modifiedStates) {
		State* targetState = item.second.state.get();
		targetState->setResetState(reset);
	}

	StateCollection::setResetState(reset);
}

void StateCollectionImpl::newModelState() {
	for(auto& item: _modifiedStates) {
		item.second.calculated = false;
	}
}

void StateCollectionImpl::calculateModifiedStates() {
	std::map<StateModifier*, State*>::iterator it;
	std::map<StateModifier*, bool>::iterator itCalc;

	for(auto& item: _modifiedStates) {
		if(!item.second.calculated) {
			item.first->getModifiedState(this, item.second.state.get());
			item.second.calculated = true;
		}
	}
}

void StateCollectionImpl::setState(State* state) {
	if(state->getStateProperties() == _modelState->getStateProperties()) {
		_modelState->setState(state);
		newModelState();
	} else {
		auto it = _modifiedStates.find(static_cast<StateModifier*>(state->getStateProperties()));
		if(it != _modifiedStates.end()) {
			it->second.state->setState(state);
			it->second.calculated = true;
		}
	}
}

State* StateCollectionImpl::getState(StateProperties* properties) {
	if(properties == nullptr
	    || properties == _modelState->getStateProperties()) {
		return _modelState.get();
	}

	State *targetState = nullptr;
	auto it = _modifiedStates.find(static_cast<StateModifier*>(properties));

	if(it != _modifiedStates.end()) {
		targetState = it->second.state.get();
		if(!it->second.calculated) {
			it->first->getModifiedState(this, targetState);
			it->second.calculated = true;
		}
	}

	assert(targetState != nullptr);
	return targetState;
}

State* StateCollectionImpl::getState() {
	return _modelState.get();
}

void StateCollectionImpl::addStateModifier(StateModifier* modifier) {
	auto iter = _modifiedStates.find(modifier);

	if(iter == _modifiedStates.end()) {
		iter = _modifiedStates.insert(std::make_pair(modifier,
			StateEntry{std::unique_ptr<State>(new State(modifier))})).first;

		if(modifier->changeState) {
			modifier->registerStateCollection(this);
		}
	}

	iter->second.refcount++;
}

void StateCollectionImpl::removeStateModifier(StateModifier* modifier) {
	auto iter = _modifiedStates.find(modifier);
	if(iter == _modifiedStates.end()) return;

	iter->second.refcount--;
	if(!iter->second.refcount) _modifiedStates.erase(iter);
}

void StateCollectionReporter::modifiersAdded(
	StateModifierList::iterator first,
	StateModifierList::iterator last
) {
	for(auto iter = first; iter != last; iter++) {
		_stateCollection->addStateModifier(*iter);
	}
}

void StateCollectionReporter::modifiersRemoved(
	StateModifierList::iterator first,
	StateModifierList::iterator last
) {
	for(auto iter = first; iter != last; iter++) {
		_stateCollection->removeStateModifier(*iter);
	}
}

void StateCollectionImpl::addModifierList(StateModifierList* modifierList) {
	StateModifiersObject::addModifierList(modifierList);

	for(auto& modifier: *modifierList) {
		addStateModifier(modifier);
	}

	modifierList->addReporter(&_reporter);
}

void StateCollectionImpl::removeModifierList(StateModifierList* modifierList) {
	StateModifiersObject::removeModifierList(modifierList);

	for(auto& modifier: *modifierList) {
		removeStateModifier(modifier);
	}

	modifierList->removeReporter(&_reporter);
}

bool StateCollectionImpl::isMember(StateProperties* stateProperties) {
	return _modelState->getStateProperties() == stateProperties
	    || _modifiedStates.find(static_cast<StateModifier*>(stateProperties))
	        != _modifiedStates.end();
}

bool StateCollectionImpl::isStateCalculated(StateModifier* modifier) {
	auto it = _modifiedStates.find(modifier);
	if(it == _modifiedStates.end()) return false;
	else return it->second.calculated;
}

void StateCollectionImpl::setIsStateCalculated(StateModifier *modifier, bool isCalculated) {
	auto it = _modifiedStates.find(modifier);
	if(it == _modifiedStates.end()) throw TracedError_OutOfRange("Cannot set isCalculated for a modifier, which has not been added.");
	it->second.calculated = isCalculated;
}

StateCollectionList::StateCollectionList(StateProperties* model) :
	StateModifiersObject(model), _stateLists()
{
	_stateLists.insert(
		std::make_pair(model,
		StateEntry(std::unique_ptr<StateList>(new StateList(model))))
	);
}

StateCollectionList::StateCollectionList(
    StateProperties *model, const std::set<StateModifierList*>& modifierLists
):
	StateModifiersObject(model), _stateLists()
{
	_stateLists.insert(
		std::make_pair(model,
		StateEntry(std::unique_ptr<StateList>(new StateList(model))))
	);

	for(auto& list: modifierLists) {
		addModifierList(list);
	}
}

StateCollectionList::~StateCollectionList() {}

void StateCollectionList::clearStateLists() {
	for(auto it = _stateLists.begin(); it != _stateLists.end(); it++) {
		it->second.stateList->clear();
	}
}

void StateCollectionList::addStateCollection(StateCollection* stateCollection) {
	for(auto it = _stateLists.begin(); it != _stateLists.end(); it++) {
		it->second.stateList->addState(
			stateCollection->getState(it->second.stateList->getStateProperties()));
	}
}

void StateCollectionList::removeLastStateCollection() {
	for(auto it = _stateLists.begin(); it != _stateLists.end(); it++) {
		it->second.stateList->removeLastState();
	}
}

void StateCollectionList::getStateCollection(
    int index, StateCollectionImpl *stateCollection
) {
	auto it = _stateLists.begin();

	if(stateCollection->isMember(it->second.stateList->getStateProperties())) {
		it->second.stateList->getState(index,
		    stateCollection->getState(it->second.stateList->getStateProperties()));
	}

	stateCollection->newModelState();

	for(; it != _stateLists.end(); it++) {
		if(stateCollection->isMember(it->second.stateList->getStateProperties())
			and it->second.stateList->getStateProperties() != stateCollection->getStateProperties()
		) {
			it->second.stateList->getState(index, stateCollection->getState(it->first));
			stateCollection->setIsStateCalculated(static_cast<StateModifier*>(it->first), true);
		}
	}

	stateCollection->setResetState(stateCollection->getState()->isResetState());
}

void StateCollectionList::getState(int index, State *state) {
	state->resetState();

	for(auto it = _stateLists.begin(); it != _stateLists.end(); it++) {
		if(it->second.stateList->getStateProperties() == state->getStateProperties()) {
			it->second.stateList->getState(index, state);
		}
	}
}

StateList* StateCollectionList::getStateList(StateProperties *properties) {
	auto it = _stateLists.find(properties);
	if(it != _stateLists.end()) return it->second.stateList.get();
	else return nullptr;
}

void StateCollectionList::addStateModifier(StateModifier* modifier) {
	auto iter = _stateLists.find(static_cast<StateProperties*>(modifier));
	if(iter == _stateLists.end()) {
		_stateLists.insert(std::make_pair(
			static_cast<StateProperties*>(modifier),
			StateEntry(std::unique_ptr<StateList>(new StateList(modifier)))
		));
	}

	iter->second.refcount++;
}

void StateCollectionList::removeStateModifier(StateModifier* modifier) {
	auto iter = _stateLists.find(static_cast<StateProperties*>(modifier));
	if(iter == _stateLists.end()) return;

	iter->second.refcount--;
	if(!iter->second.refcount) _stateLists.erase(iter);
}

void StateCollectionListReporter::modifiersAdded(
	StateModifierList::iterator first,
	StateModifierList::iterator last
) {
	for(auto iter = first; iter != last; iter++) {
		_stateCollectionList->addStateModifier(*iter);
	}
}

void StateCollectionListReporter::modifiersRemoved(
	StateModifierList::iterator first,
	StateModifierList::iterator last
) {
	for(auto iter = first; iter != last; iter++) {
		_stateCollectionList->removeStateModifier(*iter);
	}
}

void StateCollectionList::addModifierList(StateModifierList* modifierList) {
	StateModifiersObject::addModifierList(modifierList);

	for(auto& modifier: *modifierList) {
		addStateModifier(modifier);
	}

	modifierList->addReporter(&_reporter);
}

void StateCollectionList::removeModifierList(StateModifierList* modifierList) {
	StateModifiersObject::removeModifierList(modifierList);

	for(auto& modifier: *modifierList) {
		removeStateModifier(modifier);
	}

	modifierList->removeReporter(&_reporter);
}

void StateCollectionList::load(std::istream& stream) {
	for(auto& item: _stateLists) {
		item.second.stateList->load(stream);
	}
}

void StateCollectionList::save(std::ostream& stream) {
	for(auto& item: _stateLists) {
		item.second.stateList->save(stream);
	}
}

int StateCollectionList::getNumStateCollections() {
	return _stateLists.begin()->second.stateList->getNumStates();
}

} //namespace rlearner
