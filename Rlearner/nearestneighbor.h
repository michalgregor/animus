// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cnearestneighbor_H_
#define Rlearner_cnearestneighbor_H_

#include <iostream>
#include <limits>

#include "algebra.h"
#include "trees.h"
#include "inputdata.h"

namespace rlearner {

class KDRectangle {
protected:
	ColumnVector *minValues;
	ColumnVector *maxValues;

public:
	ColumnVector *getMinVector();
	ColumnVector *getMaxVector();

	void setMaxValue(int dim, RealType value);
	void setMinValue(int dim, RealType value);

	RealType getMinValue(int dim);
	RealType getMaxValue(int dim);

	RealType getDistanceToPoint(ColumnVector *point);
	bool intersects(KDRectangle *rectangle);

public:
	KDRectangle(int numDim);
	KDRectangle(KDRectangle &rectangle);
	virtual ~KDRectangle();
};

template<typename DataElement, typename TreeData>
class KNearestNeighborsTreeData {
protected:
	RealType *distList;
	DataElement *elementList;

	int K;
	Tree<TreeData> *tree;

protected:
	void getNearestNeighborsElements(
		ColumnVector *point,
		TreeElement<TreeData> *element,
		KDRectangle *rectangle
	);

	virtual void addAndSortDataElements(DataElement element, RealType distance);

	virtual void addDataElements(
		ColumnVector *point,
		Leaf<TreeData> *leaf,
		KDRectangle *rectangle
	) = 0;

public:
	void getNearestNeighbors(ColumnVector *point,
	        std::list<DataElement> *elementList, int K = -1,
	        ColumnVector *distances = nullptr);

	void getNearestNeighborDistance(ColumnVector *input,
	        DataElement &nearestNeighbor, RealType &distance);

public:
	KNearestNeighborsTreeData(Tree<TreeData> *tree, int K);
	virtual ~KNearestNeighborsTreeData();
};

template<typename DataElement, typename TreeData>
void KNearestNeighborsTreeData<DataElement, TreeData>::getNearestNeighborsElements(
	ColumnVector *point,
	TreeElement<TreeData> *element,
	KDRectangle *rectangle
) {
	if (element->isLeaf()) {
		//printf("Adding Leaf with %f Distance\n", rectangle->getDistanceToPoint( point));
		Leaf<TreeData> *leaf = dynamic_cast<Leaf<TreeData> *>(element);
		addDataElements(point, leaf, rectangle);
	} else {
		Node<TreeData> *node = dynamic_cast<Node<TreeData> *>(element);
		SplittingCondition1D *split =
		        dynamic_cast<SplittingCondition1D *>(node->getSplittingCondition());

		RealType minValue = rectangle->getMinValue(split->getDimension());
		RealType maxValue = rectangle->getMaxValue(split->getDimension());

		RealType leftDist = 0;
		RealType rightDist = 0;
		// set value for left branch
		rectangle->setMaxValue(split->getDimension(), split->getThreshold());
		leftDist = rectangle->getDistanceToPoint(point);

		rectangle->setMaxValue(split->getDimension(), maxValue);
		rectangle->setMinValue(split->getDimension(), split->getThreshold());

		rightDist = rectangle->getDistanceToPoint(point);

		//printf("LeftDist: %f, RightDist: %f...", leftDist, rightDist);	
		/*
		 for (int i = 0; i < K; i ++)
		 {
		 if (distList[i] < numeric_limits<RealType>::max())
		 {
		 printf("%f ", distList[i]);
		 }
		 }
		 printf("\n");*/

		if (rightDist < leftDist) {
			// point is nearer to the right branch
			if (distList[0] == std::numeric_limits<RealType>::max()
			        || rightDist < distList[0]) {
				// search NN in right branch
				//printf("Going Right...\n");
				getNearestNeighborsElements(point, node->getRightElement(),
				        rectangle);

				if (distList[0] == std::numeric_limits<RealType>::max()
				        || leftDist < distList[0]) {
					//	printf("Going Left (2)...\n");
					// also search NN in left branch
					rectangle->setMaxValue(split->getDimension(),
					        split->getThreshold());
					rectangle->setMinValue(split->getDimension(), minValue);

					getNearestNeighborsElements(point, node->getLeftElement(),
					        rectangle);

				}
			}
		} else {
			// point is nearer to the right branch
			if (distList[0] == std::numeric_limits<RealType>::max()
			        || leftDist < distList[0]) {
				//printf("Going Left...\n");
				rectangle->setMaxValue(split->getDimension(),
				        split->getThreshold());
				rectangle->setMinValue(split->getDimension(), minValue);

				// search NN in left branch
				getNearestNeighborsElements(point, node->getLeftElement(),
				        rectangle);

				if (distList[0] == std::numeric_limits<RealType>::max()
				        || rightDist < distList[0]) {
					//printf("Going Right (2)...\n");
					// also search NN in right branch
					rectangle->setMaxValue(split->getDimension(), maxValue);
					rectangle->setMinValue(split->getDimension(),
					        split->getThreshold());

					getNearestNeighborsElements(point, node->getRightElement(),
					        rectangle);
				}
			}
		}
		rectangle->setMaxValue(split->getDimension(), maxValue);
		rectangle->setMinValue(split->getDimension(), minValue);
	}
}

template<typename DataElement, typename TreeData>
void KNearestNeighborsTreeData<DataElement, TreeData>::addAndSortDataElements(
	DataElement data, RealType distance
) {
	int i = 0;
	while (i < K && distList[i] > distance) {
		i++;
	}
	i--;

	if (i >= 0) {
		for (int j = 0; j < i; j++) {
			distList[j] = distList[j + 1];
			elementList[j] = elementList[j + 1];
		}
		distList[i] = distance;
		elementList[i] = data;
	}
}

template<typename DataElement, typename TreeData>
KNearestNeighborsTreeData<DataElement, TreeData>::KNearestNeighborsTreeData(
	Tree<TreeData> *l_tree, int l_K
) {
	tree = l_tree;
	K = l_K;
	distList = new RealType[K];
	elementList = new DataElement[K];
}

template<typename DataElement, typename TreeData>
KNearestNeighborsTreeData<DataElement, TreeData>::~KNearestNeighborsTreeData() {
	delete[] distList;
	delete[] elementList;
}

template<typename DataElement, typename TreeData>
void KNearestNeighborsTreeData<DataElement, TreeData>::getNearestNeighbors(
	ColumnVector *point,
	std::list<DataElement> *l_elementList, int l_K,
	ColumnVector *distances
) {
	int tempK = K;
	if (l_K > 0) {
		K = l_K;
	}

	for (int i = 0; i < K; i++) {
		distList[i] = std::numeric_limits<RealType>::max();
	}

	ColumnVector *input = tree->getPreprocessedInput(point);

	KDRectangle rectangle(tree->getNumDimensions());

	//printf("Starting NN search: %f for point: ", rectangle.getDistanceToPoint(point));
	//cout << point->t() << endl;	
	//cout << input->t() << endl;		

	getNearestNeighborsElements(input, tree->getRoot(), &rectangle);

	l_elementList->clear();

	for (int i = 0; i < K; i++) {
		if (distances) {
			distances->operator[](K - 1 - i) = distList[i];
		}
		if (distList[i] < std::numeric_limits<RealType>::max()) {
			l_elementList->push_front(elementList[i]);
		}
	}

	K = tempK;
}

template<typename DataElement, typename TreeData>
void KNearestNeighborsTreeData<DataElement, TreeData>::getNearestNeighborDistance(
	ColumnVector *point,
	DataElement &nearestNeighbor,
	RealType &distance
) {
	int tempK = K;
	K = 1;

	for (int i = 0; i < K; i++) {
		distList[i] = std::numeric_limits<RealType>::max();
	}

	ColumnVector *input = tree->getPreprocessedInput(point);

	KDRectangle rectangle(tree->getNumDimensions());

	//printf("Starting NN search: %f for point: ", rectangle.getDistanceToPoint(point));
	//cout << point->t() << endl;	
	//cout << input->t() << endl;		

	getNearestNeighborsElements(input, tree->getRoot(), &rectangle);

	K = tempK;

	nearestNeighbor = elementList[0];
	distance = distList[0];
}

class KNearestNeighbors: public KNearestNeighborsTreeData<int, DataSubset *> {
protected:
	DataSet *inputSet;
	ColumnVector *buffVector;

protected:
	virtual void addDataElements(ColumnVector *point, Leaf<DataSubset *> *leaf,
	        KDRectangle *rectangle);

public:
	DataSet *getInputSet() {
		return inputSet;
	}

public:
	KNearestNeighbors(Tree<DataSubset *> *tree, DataSet *inputSet, int K);
	virtual ~KNearestNeighbors();
};

template<typename TreeData> class KNearestLeaves:
	public KNearestNeighborsTreeData<int, TreeData>
{
protected:
	virtual void addDataElements(
		ColumnVector *point, Leaf<TreeData> *leaf, KDRectangle *rectangle
	);

public:
	KNearestLeaves(Tree<TreeData> *tree, int K);
	virtual ~KNearestLeaves();
};

template<typename TreeData> void KNearestLeaves<TreeData>::
addDataElements(
	ColumnVector *point, Leaf<TreeData> *leaf, KDRectangle *rectangle
) {
	addAndSortDataElements(leaf->getLeafNumber(),
	rectangle->getDistanceToPoint(point));
}

template<typename TreeData>
KNearestLeaves<TreeData>::KNearestLeaves(Tree<TreeData> *tree, int K):
	KNearestNeighborsTreeData<int, TreeData>(tree, K) {}

template<typename TreeData>
KNearestLeaves<TreeData>::~KNearestLeaves() {}

class RangeSearch {
protected:
	DataSubset *elementList;
	Tree<DataSubset *> *tree;
	DataSet *inputSet;

protected:
	void getSamplesInRangeElements(
		KDRectangle *range,
	    TreeElement<DataSubset *> *element,
	    KDRectangle *rectangle
	);

	virtual void addAndSortDataElements(int element);

	virtual void addDataElements(
		KDRectangle *range,
		Leaf<DataSubset *> *leaf,
		KDRectangle *leafRectangle
	);

public:
	void getSamplesInRange(KDRectangle *range, DataSubset *elementList);

public:
	RangeSearch(Tree<DataSubset *> *tree, DataSet *l_inputSet);
	virtual ~RangeSearch();
};

} //namespace rlearner

#endif //Rlearner_cnearestneighbor_H_
