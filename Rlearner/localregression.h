// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net)
//
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_clocalregression_H_
#define Rlearner_clocalregression_H_

#include <iostream>
#include <limits>

#include "algebra.h"
#include "inputdata.h"

namespace rlearner {

class DataSet;
class DataSet1D;
class CDataSubSet;

class KDTree;
class KNearestNeighbors;

class LocalRegression: public Mapping<RealType> {
protected:
	KDTree *kdTree;
	KNearestNeighbors *nearestNeighbors;

	DataSet *input;
	DataSet1D *output;

	std::list<int> *subsetList;
	DataSubset *subset;
	int K;

protected:
	virtual RealType doGetOutputValue(ColumnVector *vector);

public:
	virtual RealType doRegression(ColumnVector *vector, DataSubset *subset) = 0;
	DataSubset *getLastNearestNeighbors();
	int getNumNearestNeighbors();

public:
	LocalRegression(DataSet *input, DataSet1D *output, int K);
	virtual ~LocalRegression();
};

class LocalRBFRegression: public LocalRegression {
protected:
	ColumnVector *rbfFactors;
	ColumnVector *sigma;

public:
	virtual RealType doRegression(ColumnVector *vector, DataSubset *subset);
	ColumnVector *getRBFFactors(ColumnVector *vector, DataSubset *subset);
	ColumnVector *getLastRBFFactors();

public:
	LocalRBFRegression(
		DataSet *input,
		DataSet1D *output,
		int K,
		ColumnVector *sigma
	);

	virtual ~LocalRBFRegression();
};

class LinearRegression: public Mapping<RealType> {
protected:
	Matrix *X;
	ColumnVector *xVector;
	ColumnVector *yVector;

	ColumnVector *w;
	Matrix *X_pinv;

	int degree;
	int numDimensions;
	int xDim;

protected:
	void init(int l_degree, int numDataPoints, int numDimensions);
	virtual RealType doGetOutputValue(ColumnVector *input);

public:
	RealType lambda;

public:
	virtual void getXVector(ColumnVector *input, ColumnVector *xVector);
	virtual void calculateRegressionMatrix(DataSet *dataSet,
	        DataSet1D *outputValues, DataSubset *subset);

public:
	LinearRegression(int degree, int numDataPoints, int numDimensions);
	LinearRegression(int degree, DataSet *dataSet, DataSet1D *outputValues,
	        DataSubset *subset);
	virtual ~LinearRegression();
};

class LocalLinearRegression: public LocalRegression {
protected:
	LinearRegression *regression;

public:
	virtual RealType doRegression(ColumnVector *vector, DataSubset *subset);

public:
	LocalLinearRegression(
		DataSet *input,
		DataSet1D *output,
		int K,
		int degree,
		RealType lambda
	);

	~LocalLinearRegression();
};

} //namespace rlearner

#endif //Rlearner_clocalregression_H_
