// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cassert>
#include <cmath>
#include <cstdlib>

#include "featurefunction.h"
#include "ril_debug.h"

namespace rlearner {

Feature::Feature(unsigned int Index, RealType updateValue) :
	featureIndex(Index), factor(updateValue) {
}

Feature::Feature() :
	featureIndex(0), factor(0.0) {
}

Feature::~Feature() {
	featureIndex = 0;
}

FeatureList::FeatureList(int initMemSize, bool isSorted_, bool sortAbs_) :
	    freeResources(new std::list<Feature *>()),
	    allResources(new std::list<Feature *>()),
	    featureMap(new std::map<int, Feature *>()),
	    isSorted(isSorted_),
	    sortAbs(sortAbs_) {
	for(int i = 0; i < initMemSize; i++) {
		Feature *feat = new Feature(0, 0.0);
		freeResources->push_back(feat);
		allResources->push_back(feat);
	}
}

FeatureList::~FeatureList() {
	delete freeResources;
	delete featureMap;

	std::list<Feature *>::iterator it = allResources->begin();

	for(int i = 0; it != allResources->end(); it++, i++) {
		delete *it;
	}
}

Feature *FeatureList::getFreeFeatureResource() {
	Feature *freeFeat = nullptr;
	if(freeResources->size() > 0) {
		freeFeat = *freeResources->begin();
		freeResources->pop_front();
	} else {
		freeFeat = new Feature(0, 0);
		allResources->push_back(freeFeat);
	}
	return freeFeat;
}

void FeatureList::sortFeature(Feature *feature) {
	std::list<Feature *>::remove(feature);
	FeatureList::iterator it = begin();
	if(sortAbs) {
		while(it != end() && fabs((*it)->factor) > fabs(feature->factor)) {
			it++;
		}
	} else {
		while(it != end() && (*it)->factor > feature->factor) {
			it++;
		}
	}

	std::list<Feature *>::insert(it, feature);
}

void FeatureList::add(Feature *feature) {
	Feature *feat = nullptr;
	if((*featureMap)[feature->featureIndex] == nullptr) {
		feat = getFreeFeatureResource();
		feat->featureIndex = feature->featureIndex;
		feat->factor = feature->factor;
		push_back(feat);
		(*featureMap)[feature->featureIndex] = feat;

	} else {
		feat = (*featureMap)[feature->featureIndex];
		feat->factor += feature->factor;
	}
	if(isSorted) {
		sortFeature(feat);
	}
}

void FeatureList::add(FeatureList *featureList, RealType factor) {
	FeatureList::iterator it = featureList->begin();

	for(; it != featureList->end(); it++) {
		update((*it)->featureIndex, (*it)->factor * factor);
	}
}

RealType FeatureList::multFeatureList(FeatureList *featureList) {
	FeatureList::iterator it = featureList->begin();

	RealType sum = 0.0;
	for(; it != featureList->end(); it++) {
		sum += getFeatureFactor((*it)->featureIndex) * (*it)->factor;
	}
	return sum;
}

void FeatureList::set(int feature, RealType factor) {
	Feature *feat = nullptr;
	if((*featureMap)[feature] == nullptr) {
		feat = getFreeFeatureResource();
		feat->featureIndex = feature;
		feat->factor = factor;
		push_back(feat);
		(*featureMap)[feature] = feat;

	} else {
		feat = (*featureMap)[feature];
		feat->factor = factor;
	}
	if(isSorted) {
		sortFeature(feat);
	}
}

void FeatureList::update(int feature, RealType factor) {
	Feature *feat = nullptr;
	if((*featureMap)[feature] == nullptr) {
		feat = getFreeFeatureResource();
		feat->featureIndex = feature;
		feat->factor = factor;
		push_back(feat);
		(*featureMap)[feature] = feat;

	} else {
		feat = (*featureMap)[feature];
		feat->factor += factor;
	}
	if(isSorted) {
		sortFeature(feat);
	}
}

RealType FeatureList::getFeatureFactor(int featureIndex) {
	if((*featureMap)[featureIndex] != nullptr) {
		return (*featureMap)[featureIndex]->factor;
	} else {
		return 0.0;
	}
}

Feature* FeatureList::getFeature(int featureIndex) {
	return (*featureMap)[featureIndex];
}

void FeatureList::normalize() {
	FeatureList::iterator it = begin();

	RealType sum = 0.0;
	for(; it != end(); it++) {
		sum += (*it)->factor;
	}

	multFactor(1.0 / sum);
}

RealType FeatureList::getLength() {
	FeatureList::iterator it = begin();

	RealType sum = 0.0;
	for(; it != end(); it++) {
		sum += pow((*it)->factor, (RealType) 2.0);
	}

	return sqrt(sum);
}

void FeatureList::remove(Feature *feature) {
	remove(feature->featureIndex);
}

void FeatureList::remove(int feature) {
	Feature *feat = (*featureMap)[feature];
	if(feat != nullptr) {
		std::list<Feature *>::remove(feat);
		freeResources->push_back(feat);
		featureMap->erase(feature);
	}
}

void FeatureList::clear() {
	FeatureList::iterator it = begin();
	for(; it != end(); it++) {
		freeResources->push_back(*it);
	}
	std::list<Feature *>::clear();
	featureMap->clear();
}

void FeatureList::clearAndDelete() {
	freeResources->clear();
	featureMap->clear();
	std::list<Feature *>::clear();

	std::list<Feature *>::iterator it = allResources->begin();
	for(int i = 0; it != allResources->end(); it++, i++) {
		delete *it;
	}

	allResources->clear();
}

void FeatureList::multFactor(RealType factor) {
	FeatureList::iterator it = begin();

	for(int i = 0; it != end(); it++, i++) {
		(*it)->factor *= factor;
	}
}

void FeatureList::addIndexOffset(int offset) {
	FeatureList::iterator it = begin();
	this->featureMap->clear();
	for(; it != end(); it++) {
		(*it)->featureIndex += offset;
		(*featureMap)[(*it)->featureIndex] = *it;
	}
}

FeatureList::iterator FeatureList::getFeaturePos(unsigned int feature) {
	FeatureList::iterator it = begin();
	for(int i = 0; it != end(); it++, i++) {
		if((*it)->featureIndex >= feature) {
			break;
		}
	}
	return it;
}

void FeatureList::load(std::istream& stream) {
	clear();
	FeatureList::iterator it = begin();

	xscanf(stream, "[");
	for(; it != end(); it++) {
		int index;
		RealType buffer;
		xscanf(stream, "(%d,%lf)", index, buffer);
		set(index, buffer);
	}
	xscanf(stream, "]");
}

void FeatureList::save(std::ostream& stream) {
	FeatureList::iterator it = begin();

	fmt::fprintf(stream, "[");
	for(; it != end(); it++) {
		fmt::fprintf(stream, "(%d,%1.3f)", (*it)->featureIndex, (*it)->factor);
	}
	fmt::fprintf(stream, "]");
}

FeatureList::iterator FeatureList::begin() {
	return std::list<Feature *>::begin();
}

FeatureList::iterator FeatureList::end() {
	return std::list<Feature *>::end();
}

FeatureList::reverse_iterator FeatureList::rbegin() {
	return std::list<Feature *>::rbegin();
}

FeatureList::reverse_iterator FeatureList::rend() {
	return std::list<Feature *>::rend();
}

FeatureFunction::FeatureFunction(unsigned int numFeatures) {
	externFeatures = false;

	this->numFeatures = numFeatures;
	features = new RealType[numFeatures];

	for(unsigned int i = 0; i < numFeatures; i++) {
		features[i] = 0.0;
	}
}

FeatureFunction::FeatureFunction(unsigned int numFeatures, RealType *features) {
	externFeatures = true;
	this->numFeatures = numFeatures;
	this->features = features;
}

FeatureFunction::~FeatureFunction() {
	if(!externFeatures) delete features;
}

void FeatureFunction::randomInit(RealType min, RealType max) {
	auto rand = make_rand();

	for(unsigned int i = 0; i < numFeatures; i++) {
		features[i] = ((RealType) rand()) / ((RealType) RAND_MAX) * (max - min)
		    + min;
	}
}

void FeatureFunction::init(RealType value) {
	for(unsigned int i = 0; i < numFeatures; i++) {
		features[i] = value;
	}
}

void FeatureFunction::setFeature(Feature *update, RealType value) {
	assert(update->featureIndex < numFeatures);

	features[update->featureIndex] = value * update->factor;
}

void FeatureFunction::setFeature(unsigned int featureIndex, RealType value) {
	features[featureIndex] = value;
}

void FeatureFunction::setFeatureList(FeatureList *updateList, RealType value) {
	FeatureList::iterator it = updateList->begin();
	for(; it != updateList->end(); it++) {
		setFeature(*it, value);
	}
}

void FeatureFunction::updateFeature(Feature *update, RealType difference) {
	assert(update->featureIndex < numFeatures);

	RealType oldV = features[update->featureIndex];
	features[update->featureIndex] += difference * update->factor;

	DebugPrint('f', "Feature (%d %f) Update: %2.4f -> %2.4f\t\n",
	    update->featureIndex, update->factor, oldV,
	    features[update->featureIndex]);
}

void FeatureFunction::updateFeature(int feature, RealType difference) {
	features[feature] += difference;
}

void FeatureFunction::updateFeatureList(FeatureList *updateList,
RealType difference) {
	FeatureList::iterator it = updateList->begin();

	for(; it != updateList->end(); it++) {
		updateFeature(*it, difference);
	}
}

RealType FeatureFunction::getFeature(unsigned int featureIndex) {
	assert(featureIndex < numFeatures);

	return features[featureIndex];
}

RealType FeatureFunction::getFeatureList(FeatureList *featureList) {
	RealType sum = 0.0;
	FeatureList::iterator it = featureList->begin();

	for(; it != featureList->end(); it++) {
		sum += (*it)->factor * getFeature((*it)->featureIndex);
	}

	return sum;
}

void FeatureFunction::saveFeatures(std::ostream& stream) {
	fmt::fprintf(stream, "FeatureFunction (Features: %d)\n", numFeatures);
	for(unsigned int i = 0; i < numFeatures; i++) {
		fmt::fprintf(stream, " %8.4lf", features[i]);
	}
	fmt::fprintf(stream, "\n");
}

void FeatureFunction::loadFeatures(std::istream& stream) {
	unsigned int tmpnumFeatures;
	std::string tmpName;

	xscanf(stream, "%s (Features: %d)\n", tmpName, tmpnumFeatures);

	if(features != nullptr) {
		SYS_THROW_ASSERT(tmpnumFeatures == numFeatures, TracedError_InvalidFormat);
	} else {
		numFeatures = tmpnumFeatures;
		features = new RealType[numFeatures];
	}

	for(unsigned int j = 0; j < numFeatures; j++) {
		SYS_THROW_ASSERT(xscanf(stream, " %lf", features[j]) == 1, TracedError_InvalidFormat);
	}

	xscanf(stream, "\n");
}

void FeatureFunction::printFeatures() {
	saveFeatures(std::cout);
}

unsigned int FeatureFunction::getNumFeatures() {
	return numFeatures;
}

void FeatureFunction::postProcessWeights(RealType mean, RealType std) {
	// de-normalizaton if the output has been normalized, works only if feature[0] is the bias!!
	for(unsigned int i = 0; i < numFeatures; i++) {
		features[i] *= std;
	}
	features[0] += mean;
}

} //namespace rlearner
