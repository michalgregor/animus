// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#ifndef Rlearner_cutility_H_
#define Rlearner_cutility_H_

#include <map>
#include <cassert>
#include <cstring>

#include <Systematic/macros.h>

#include "algebra.h"

namespace rlearner {

int my_round(RealType value);
RealType bounded_exp(RealType value);
void getPseudoInverse(Matrix *J, Matrix *pinv, RealType lambda);

/**
 * A multidimensional Array.
 *
 * Stores an one dimensional array of Type T1, and provides the simulation
 * of a multi-dimensional array. The number of dimensions and the size of each
 * dimension is given to the constructor. There are accessing functions for
 * one dimensional an multidimensional indices.
 **/
template<typename T1> class MyArray {
protected:
	T1 *data;
	int *dim;
	int numDim;
	int size;

protected:
	MyArray() = default;

	void initialize(int numDim_, int dim_[]) {
		numDim = numDim_;
		dim = new int[numDim];

		memcpy(dim, dim_, numDim * sizeof(int));

		size = 1;

		for(int i = 0; i < numDim; i++) {
			size = size * dim[i];
		}

		data = new T1[size];
	}

public:
	T1 get(int indices[]) {
		int index = 0, sz = 1;

		for(int i = 0; i < numDim; i++) {
			assert(indices[i] < dim[i] && indices[i] >= 0);

			index += indices[i] * sz;
			sz = sz * dim[i];
		}
		return data[index];
	}

	void set(int indices[], T1 d) {
		int index = 0, sz = 1;

		for(int i = 0; i < numDim; i++) {
			assert(indices[i] < dim[i] && indices[i] >= 0);

			index += indices[i] * sz;
			sz = sz * dim[i];
		}
		data[index] = d;

	}

	void init(T1 initVal) {
		for(int i = 0; i < size; i++) {
			data[i] = initVal;
		}
	}

	int getSize() {
		return size;
	}

	void set1D(int index1d, T1 d) {
		data[index1d] = d;
	}

	T1 get1D(int index1d) {
		return data[index1d];
	}

public:
	MyArray(int numDim_, int dim_[]) {
		MyArray<T1>::initialize(numDim_, dim_);
	}

	~MyArray() {
		delete dim;
		delete data;
	}
};

//! Collection of Distributions
class Distributions {
public:

	/**
	 * Returns the gibs distribution (soft max).
	 *
	 * Parameter beta is the head, used by the formula. In the values array
	 * there are given the single values. The function then writes the
	 * probability of the indices into the array.
	 */
	static void getGibbsDistribution(
	    RealType beta, RealType *values, unsigned int numValues);

	static void getS1L0Distribution(RealType *values, unsigned int numValues);

	static RealType getNormalDistributionSample(RealType mu, RealType sigma);

	static int getSampledIndex(RealType *distribution, int numValues);
};

//! 2 dimensional array.
template<typename T1>
class MyArray2D: public MyArray<T1> {
public:
	T1 get(int xIndex, int yIndex) {
		int indices[2];
		indices[0] = xIndex;
		indices[1] = yIndex;

		return MyArray<T1>::get(indices);
	}

	void set(int xIndex, int yIndex, T1 d) {
		int indices[2];
		indices[0] = xIndex;
		indices[1] = yIndex;

		MyArray<T1>::set(indices, d);
	}

public:
	MyArray2D(int xDim, int yDim) {
		int *l_dim = new int[2];
		l_dim[0] = xDim;
		l_dim[1] = yDim;

		MyArray<T1>::initialize(2, l_dim);

		delete l_dim;
	}

	~MyArray2D() = default;
};

//! 3 dimensional array.
template<typename T1> class MyArray3D: public MyArray<T1> {
public:
	T1 get(int xIndex, int yIndex, int zIndex) {
		int indices[3];
		indices[0] = xIndex;
		indices[1] = yIndex;
		indices[2] = zIndex;

		return MyArray<T1>::get(indices);
	}

	void set(int xIndex, int yIndex, int zIndex, T1 d) {
		int indices[3];
		indices[0] = xIndex;
		indices[1] = yIndex;
		indices[2] = zIndex;

		MyArray<T1>::set(indices, d);
	}

public:
	MyArray3D(int xDim, int yDim, int zDim): MyArray<T1>() {
		int *ldim = new int[3];
		ldim[0] = xDim;
		ldim[1] = yDim;
		ldim[2] = zDim;

		MyArray<T1>::initialize(3, ldim);
	}

	~MyArray3D() {}
};

//! Maps feature indices to feature factors.
SYS_WNONVIRT_DTOR_OFF
class FeatureMap: public std::map<int, RealType> {
SYS_WNONVIRT_DTOR_ON
protected:
	RealType stdValue;

public:
	RealType getValue(unsigned int featureIndex);

public:
	FeatureMap(RealType stdValue = 0.0);
};

} //namespace rlearner

#endif //Rlearner_cutility_H_
