// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <cmath>
#include <cstring>

#include "algebra.h"
#include "ril_debug.h"
#include "gradientfunction.h"
#include "featurefunction.h"
#include "utility.h"

namespace rlearner {

IndividualEtaCalculator::IndividualEtaCalculator(
    int numWeights, RealType *l_etas) {
	this->numWeights = numWeights;
	this->etas = new RealType[numWeights];

	if(l_etas != nullptr) {
		memcpy(this->etas, l_etas, sizeof(RealType) * numWeights);
	} else {
		for(int i = 0; i < numWeights; i++) {
			etas[i] = 1.0;
		}
	}
}

IndividualEtaCalculator::~IndividualEtaCalculator() {
	delete etas;
}

void IndividualEtaCalculator::getWeightUpdates(FeatureList *updates) {
	FeatureList::iterator it = updates->begin();
	for(; it != updates->end(); it++) {
		(*it)->factor *= etas[(*it)->featureIndex];
	}
}

void IndividualEtaCalculator::setEta(int index, RealType value) {
	assert(index >= 0 && index < numWeights);
	etas[index] = value;
}

VarioEta::VarioEta(
    unsigned int numParams, RealType eta, RealType beta, RealType epsilon) {
	/*this->beta = beta;
	 this->epsilon = epsilon;
	 this->eta = eta;*/
	addParameter("VarioEtaLearningRate", eta);
	addParameter("VarioEtaBeta", beta);
	addParameter("VarioEtaEpsilon", epsilon);

	this->numParams = numParams;

	eta_i = new RealType[numParams];
	v_i = new RealType[numParams];

	for(unsigned int i = 0; i < numParams; i++) {
		v_i[i] = 1;
		eta_i[i] = eta / (sqrt(v_i[i]) + epsilon);
	}
}

VarioEta::~VarioEta() {
	delete eta_i;
	delete v_i;
}

void VarioEta::getWeightUpdates(FeatureList *Updates) {
	RealType epsilon = getParameter("VarioEtaEpsilon");
	RealType beta = getParameter("VarioEtaBeta");
	RealType eta = getParameter("VarioEtaLearningRate");

	for(unsigned int i = 0; i < numParams; i++) {
		eta_i[i] = eta / (sqrt(v_i[i]) + epsilon);
		v_i[i] *= (1 - beta);
	}

	/*for (unsigned int i = 0; i < numParams; i ++)
	 {
	 v_i[i] = v_i[i] * (1 - beta);
	 }*/

	FeatureList::iterator it = Updates->begin();
	for(; it != Updates->end(); it++) {
		v_i[(*it)->featureIndex] += beta * pow((*it)->factor / eta, 2);
		DebugPrint('v',
		    "Vario Eta: Updating Feature %d with Eta %f (v_i: %f)\n",
		    (*it)->featureIndex, eta_i[(*it)->featureIndex],
		    v_i[(*it)->featureIndex]);
		(*it)->factor = (*it)->factor * eta_i[(*it)->featureIndex];
	}
}

GradientUpdateFunction::GradientUpdateFunction() {
	localGradientFeatureBuffer = new FeatureList();

	etaCalc = nullptr;
}

GradientUpdateFunction::~GradientUpdateFunction() {
	delete localGradientFeatureBuffer;
}

void GradientUpdateFunction::updateGradient(
    FeatureList *gradientFeatures, RealType factor) {
	if(gradientFeatures != this->localGradientFeatureBuffer) {
		DebugPrint('g', "1...");

		this->localGradientFeatureBuffer->clear();

		FeatureList::iterator it1 = gradientFeatures->begin();

		for(; it1 != gradientFeatures->end(); it1++) {
			this->localGradientFeatureBuffer->update((*it1)->featureIndex,
			    factor * (*it1)->factor);

			DebugPrint('g', "%d : %f %f \n", (*it1)->featureIndex,
			    (*it1)->factor, factor);
		}

		if(DebugIsEnabled('g')) {
			DebugPrint('g', "localGradientFeatures: ");
			localGradientFeatureBuffer->save(DebugGetFileHandle('g'));
		}

	} else {
		localGradientFeatureBuffer->multFactor(factor);
		DebugPrint('g', "2...");
	}

	if(etaCalc) {
		etaCalc->getWeightUpdates(this->localGradientFeatureBuffer);
		DebugPrint('g', "3...");
	}

	if(DebugIsEnabled('g')) {
		DebugPrint('g', "gradientFeatures: ");
		gradientFeatures->save(DebugGetFileHandle('g'));
		DebugPrint('g', "localGradientFeatures: ");
		localGradientFeatureBuffer->save(DebugGetFileHandle('g'));
	}

	updateWeights(this->localGradientFeatureBuffer);
}

AdaptiveEtaCalculator* GradientUpdateFunction::getEtaCalculator() {
	return getEtaCalculator();
}

void GradientUpdateFunction::setEtaCalculator(AdaptiveEtaCalculator *etaCalc) {
	addParameters(etaCalc);
	this->etaCalc = etaCalc;
}

void GradientUpdateFunction::copy(LearnDataObject *l_gradientUpdateFunction) {
	GradientUpdateFunction *gradientUpdateFunction =
	    dynamic_cast<GradientUpdateFunction *>(l_gradientUpdateFunction);

	assert(gradientUpdateFunction->getNumWeights() == getNumWeights());

	RealType *weights = new RealType[getNumWeights()];

	getWeights(weights);

	gradientUpdateFunction->setWeights(weights);

	delete[] weights;
}

void GradientUpdateFunction::saveData(std::ostream& stream) {
	RealType *parameters = new RealType[getNumWeights()];
	getWeights(parameters);

	fmt::fprintf(stream, "Gradient Function\n");
	fmt::fprintf(stream, "Parameters: %d\n", getNumWeights());

	for(int i = 0; i < getNumWeights(); i++) {
		fmt::fprintf(stream, "%f ", parameters[i]);
	}
	fmt::fprintf(stream, "\n");
	delete[] parameters;
}

void GradientUpdateFunction::loadData(std::istream& stream) {
	std::vector<RealType> parameters(getNumWeights(), 0);
	int bufNumParam;

	xscanf(stream, "Gradient Function\n");
	xscanf(stream, "Parameters: %d", bufNumParam);

	SYS_ASSERT(bufNumParam == getNumWeights());

	for(int i = 0; i < getNumWeights(); i++) {
		xscanf(stream, "%lf ", parameters[i]);
	}

	xscanf(stream, "\n");
	setWeights(parameters.data());
}

GradientFunction::GradientFunction(int l_num_inputs, int l_num_outputs) {
	num_inputs = l_num_inputs;
	num_outputs = l_num_outputs;

	input_mean = new ColumnVector(num_inputs);
	input_std = new ColumnVector(num_inputs);

	output_mean = new ColumnVector(num_outputs);
	output_std = new ColumnVector(num_outputs);

	input_mean->setZero();
	output_mean->setZero();

	output_std->setConstant(1);
	input_std->setConstant(1);
}

GradientFunction::~GradientFunction() {
	delete input_mean;
	delete output_mean;
	delete input_std;
	delete output_std;
}

void GradientFunction::preprocessInput(
    ColumnVector *input, ColumnVector *norm_input) {
	norm_input->noalias() = (*input - *input_mean);

	for(int i = 0; i < num_inputs; i++) {
		norm_input->operator[](i) = norm_input->operator[](i)
		    / input_std->operator[](i);
	}

}

void GradientFunction::postprocessOutput(Matrix *norm_output, Matrix *output) {
	for(int i = 0; i < num_outputs; i++) {
		for(int j = 0; j < output->cols(); j++) {
			output->operator()(i, j) = (norm_output->operator()(i, j)
			    * output_std->operator[](i)) + output_mean->operator[](i);
			;
		}
	}

}

void GradientFunction::postprocessOutput(
    ColumnVector *norm_output, ColumnVector *output) {
	for(int i = 0; i < num_outputs; i++) {
		output->operator[](i) = (norm_output->operator[](i)
		    * output_std->operator[](i)) + output_mean->operator[](i);
		;
	}

}

void GradientFunction::getGradient(
    ColumnVector *input, ColumnVector *outputErrors,
    FeatureList *gradientFeatures) {
	ColumnVector norm_input(num_inputs);
	preprocessInput(input, &norm_input);
	getGradientPre(&norm_input, outputErrors, gradientFeatures);

}

void GradientFunction::getFunctionValue(
    ColumnVector *input, ColumnVector *output) {
	ColumnVector norm_input(num_inputs);
	preprocessInput(input, &norm_input);
	getFunctionValuePre(&norm_input, output);
	postprocessOutput(output, output);
}

void GradientFunction::getInputDerivation(
    ColumnVector *input, Matrix *targetVector) {
	ColumnVector norm_input(num_inputs);
	preprocessInput(input, &norm_input);
	getInputDerivation(&norm_input, targetVector);
	postprocessOutput(targetVector, targetVector);
}

void GradientFunction::getInputDerivation(
    ColumnVector *input, ColumnVector *targetVector) {
	ColumnVector norm_input(num_inputs);
	preprocessInput(input, &norm_input);
	getInputDerivation(&norm_input, targetVector);
	postprocessOutput(targetVector, targetVector);
}

void GradientFunction::setInputMean(ColumnVector *l_input_mean) {
	*input_mean = *l_input_mean;
}

void GradientFunction::setOutputMean(ColumnVector *l_output_mean) {
	*output_mean = *l_output_mean;
}

void GradientFunction::setInputStd(ColumnVector *l_input_std) {
	*input_std = *l_input_std;
}

void GradientFunction::setOutputStd(ColumnVector *l_output_std) {
	*output_std = *l_output_std;
}

int GradientFunction::getNumInputs() {
	return num_inputs;
}

int GradientFunction::getNumOutputs() {
	return num_outputs;
}

} //namespace rlearner
