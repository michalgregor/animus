// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Repacked by Michal Gregor.

#include <sstream>
#include <cassert>

#include "ril_debug.h"
#include "qfunction.h"

#include "action.h"
#include "actionstatistics.h"
#include "environmentmodel.h"
#include "vfunction.h"
#include "episode.h"
#include "rewardfunction.h"
#include "qetraces.h"
#include "ril_debug.h"
#include "statecollection.h"
#include "state.h"
#include "stateproperties.h"
#include "dynamicprogramming.h"
#include "utility.h"
#include "theoreticalmodel.h"
#include "statemodifier.h"

namespace rlearner {

AbstractQFunction::AbstractQFunction(ActionSet *actions) :
	ActionObject(actions, false) {
	type = 0;
	mayDiverge = false;
}

AbstractQFunction::~AbstractQFunction() {}

void AbstractQFunction::setValue(
    StateCollection *, Action *, RealType, ActionData *) {
}

void AbstractQFunction::getActionValues(
    StateCollection *stateCol, ActionSet *actions, RealType *actionValues,
    ActionDataSet *data) {
	ActionSet::iterator it = actions->begin();
	for(unsigned int i = 0; it != actions->end(); it++, i++) {
		if(data) {
			actionValues[i] = this->getValue(stateCol, *it,
			    data->getActionData(*it));
		} else {
			actionValues[i] = this->getValue(stateCol, *it);
		}
	}
}

RealType AbstractQFunction::getMaxValue(
    StateCollection *state, ActionSet *availableActions) {
	assert(availableActions->size() > 0);

	RealType max, value;
	RealType *actionValues = new RealType[availableActions->size()];

	getActionValues(state, availableActions, actionValues);

	max = actionValues[0];

	for(unsigned int i = 1; i < availableActions->size(); i++) {
		value = actionValues[i];
		if(max < value) {
			max = value;
		}
	}

	delete[] actionValues;

	return max;
}

Action* AbstractQFunction::getMax(
    StateCollection* stateCol, ActionSet *availableActions, ActionDataSet *) {
	assert(availableActions->size() > 0);

	RealType max, value;
	RealType *actionValues = new RealType[availableActions->size()];

	ActionSet::iterator it = availableActions->begin();
	ActionSet *max_list = new ActionSet();

	getActionValues(stateCol, availableActions, actionValues);

	max = actionValues[0];
	max_list->push_back(*it++);

	for(unsigned int i = 1; it != availableActions->end(); it++, i++) {
		value = actionValues[i];
		if(max < value) {
			max_list->clear();
			max = value;
			max_list->push_back(*it);
		} else if(max == value) {
			max_list->push_back(*it);
		}
	}

	int index = 0;
	Action *action = max_list->get(index);

	DebugPrint('q', "ActionValues: ");
	for(unsigned int j = 0; j < availableActions->size(); j++) {
		DebugPrint('q', "%f ", actionValues[j]);
	}
	DebugPrint('q', "\nMax: %d\n", actions->getIndex(action));

	delete max_list;
	delete[] actionValues;
	return action;
}

void AbstractQFunction::getStatistics(
    StateCollection* state, Action* action, ActionSet *availableActions,
    ActionStatistics *statistics) {
	assert(availableActions->size() > 0);
	assert(statistics != nullptr);

	RealType *actionValues = new RealType[availableActions->size()];
	// get Q-Values
	getActionValues(state, availableActions, actionValues);
	// Hier wird die WK-Verteilung erstellt, aus der die Statistik berechnet wird
	//transform: smallest Value = 0, Value sum = 1;
	Distributions::getS1L0Distribution(actionValues, availableActions->size());

	statistics->action = action;
	statistics->equal = 0;
	statistics->superior = 0;
	statistics->probability = actionValues[availableActions->getIndex(action)];

	for(unsigned int i = 0; i < availableActions->size(); i++) {
		if(statistics->probability == actionValues[i]) statistics->equal++;
		if(statistics->probability < actionValues[i]) statistics->superior++;
	}
	delete[] actionValues;
}

int AbstractQFunction::getType() {
	return type;
}

void AbstractQFunction::addType(int Type) {
	type = type | Type;
}

bool AbstractQFunction::isType(int type) {
	return (this->type & type) > 0;
}

void AbstractQFunction::saveData(std::ostream& file) {
	fmt::fprintf(file, "Q-Function:\n");
	fmt::fprintf(file, "Actions: %d\n\n", actions->size());
}

void AbstractQFunction::loadData(std::istream& file) {
	unsigned int buf = 0;

	SYS_THROW_ASSERT(xscanf(file, "Q-Function:\n") == 0, TracedError_InvalidFormat);

	SYS_THROW_ASSERT(
		xscanf(file, "Actions: %d\n\n", buf) == 1 && buf == actions->size(),
		TracedError_InvalidFormat
	);

	SYS_THROW_ASSERT(xscanf(file, "\n") == 0, TracedError_InvalidFormat);
}

QFunctionSum::QFunctionSum(ActionSet *actions) :
	AbstractQFunction(actions) {
	qFunctions = new std::map<AbstractQFunction *, RealType>;
}

QFunctionSum::~QFunctionSum() {
	delete qFunctions;
}

RealType QFunctionSum::getValue(
    StateCollection *state, Action *action, ActionData *data) {
	std::map<AbstractQFunction *, RealType>::iterator it = qFunctions->begin();

	RealType sum = 0.0;
	for(; it != qFunctions->end(); it++) {
		AbstractQFunction *qFunc = (*it).first;
		if(qFunc->getActions()->isMember(action)) {
			sum += (*it).second * qFunc->getValue(state, action, data);
		}
	}
	return sum;
}

RealType QFunctionSum::getQFunctionFactor(AbstractQFunction *qFunction) {
	return (*qFunctions)[qFunction];
}

void QFunctionSum::setQFunctionFactor(AbstractQFunction *qFunction,
RealType factor) {
	(*qFunctions)[qFunction] = factor;
}

void QFunctionSum::addQFunction(AbstractQFunction *qFunction, RealType factor) {
	addParameters(qFunction);
	(*qFunctions)[qFunction] = factor;
}

void QFunctionSum::removeQFunction(AbstractQFunction *qFunction) {
	std::map<AbstractQFunction *, RealType>::iterator it = qFunctions->find(
	    qFunction);

	qFunctions->erase(it);
}

void QFunctionSum::normFactors(RealType factor) {
	std::map<AbstractQFunction *, RealType>::iterator it = qFunctions->begin();

	RealType sum = 0.0;
	for(; it != qFunctions->end(); it++) {
		sum += (*it).second;
	}
	for(; it != qFunctions->end(); it++) {
		(*it).second *= factor / sum;
	}
}

DivergentQFunctionException::DivergentQFunctionException(
    std::string qFunctionName, AbstractQFunction *qFunction, State *state,
    RealType value
):
	TracedError(qFunctionName + " diverges (value = " + std::to_string(value) + ", |value| > 1000000).")
{
	this->qFunction = qFunction;
	this->qFunctionName = qFunctionName;
	this->state = state;
	this->value = value;
}

GradientQFunction::GradientQFunction(ActionSet *actions) :
	AbstractQFunction(actions) {
	addType(GRADIENTQFUNCTION);

	this->localGradientQFunctionFeatures = new FeatureList();
}

GradientQFunction::~GradientQFunction() {
	delete localGradientQFunctionFeatures;
}

void GradientQFunction::updateValue(StateCollection *state, Action *action,
RealType td, ActionData *data) {
	localGradientFeatureBuffer->clear();
	getGradient(state, action, data, localGradientFeatureBuffer);

	updateGradient(localGradientFeatureBuffer, td);
}

AbstractQETraces *GradientQFunction::getStandardETraces() {
	return new GradientQETraces(this);
}

QFunction::QFunction(ActionSet *act) :
	GradientQFunction(act) {
	this->vFunctions = new std::map<Action *, AbstractVFunction *>();
}

QFunction::~QFunction() {
	delete vFunctions;
}

void QFunction::updateValue(
    State *state, Action *action, RealType td, ActionData *) {
	assert((*vFunctions)[action]);

	(*vFunctions)[action]->updateValue(state, td);
}

void QFunction::setValue(
    State *state, Action *action, RealType value, ActionData *) {
	assert((*vFunctions)[action]);

	(*vFunctions)[action]->setValue(state, value);
}

RealType QFunction::getValue(State *state, Action *action, ActionData *data) {
	RealType value = 0.0;

	if((*vFunctions)[action] == nullptr) {
		printf("No V-Function found for action : ");
		if(data) {
			data->save(std::cout);
		} else {
			action->getActionData()->save(std::cout);
		}
		printf("\n");
		assert(false);
	}
	value = (*vFunctions)[action]->getValue(state);

	return value;
}

void QFunction::updateValue(StateCollection *state, Action *action,
RealType td, ActionData *) {
	assert((*vFunctions)[action]);

	(*vFunctions)[action]->updateValue(state, td);
}

void QFunction::setValue(StateCollection *state, Action *action,
RealType value, ActionData *) {
	assert((*vFunctions)[action]);

	(*vFunctions)[action]->setValue(state, value);
}

RealType QFunction::getValue(
    StateCollection *state, Action *action, ActionData *data) {
	RealType value = 0.0;

	if((*vFunctions)[action] == nullptr) {
		printf("No V-Function found for action %d : ",
		    actions->getIndex(action));
		if(data) {
			data->save(std::cout);
		} else {
			action->getActionData()->save(std::cout);
		}
		printf("\n");
		assert(false);
	}

	value = (*vFunctions)[action]->getValue(state);

	return value;
}

AbstractVFunction *QFunction::getVFunction(Action *action) {
	return (*vFunctions)[action];
}

AbstractVFunction *QFunction::getVFunction(int index) {
	return (*vFunctions)[actions->get(index)];
}

void QFunction::setVFunction(
    Action *action, AbstractVFunction *vfunction, bool bDelete) {
	if(bDelete && (*vFunctions)[action] != nullptr) {
		delete (*vFunctions)[action];
	}
	(*vFunctions)[action] = vfunction;

	if(!vfunction->isType(GRADIENTVFUNCTION)) {
		type = type & (~ GRADIENTQFUNCTION);
	}

	addParameters(vfunction);
}

void QFunction::setVFunction(
    int index, AbstractVFunction *vfunction, bool bDelete) {
	setVFunction(actions->get(index), vfunction, bDelete);
}

int QFunction::getNumVFunctions() {
	return vFunctions->size();
}

void QFunction::saveData(std::ostream& file) {
	AbstractQFunction::saveData(file);

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		(*vFunctions)[(*it)]->saveData(file);
	}
}

void QFunction::loadData(std::istream& file) {
	AbstractQFunction::loadData(file);
	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		(*vFunctions)[(*it)]->loadData(file);
	}

	SYS_THROW_ASSERT(xscanf(file, "\n") == 0, TracedError_InvalidFormat);
}

void QFunction::printValues() {
	AbstractQFunction::printValues();

	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		(*vFunctions)[(*it)]->printValues();
	}
}

AbstractQETraces *QFunction::getStandardETraces() {
	return new QETraces(this);
}

void QFunction::resetData() {
	ActionSet::iterator it = actions->begin();

	for(; it != actions->end(); it++) {
		(*vFunctions)[(*it)]->resetData();
	}
}

void QFunction::copy(LearnDataObject *qFunc) {
	ActionSet::iterator it = actions->begin();

	QFunction *qFunction = dynamic_cast<QFunction *>(qFunc);

	for(; it != actions->end(); it++) {
		(*vFunctions)[(*it)]->copy(qFunction->getVFunction(*it));
	}

}

void QFunction::getGradient(
    StateCollection *stateCol, Action *action, ActionData *,
    FeatureList *gradient) {
	if(gradient->size() > 0) {
		printf(
		    "Warning : QFunction... getting Gradient, gradient list not empty!!\n");
	}

	if((*vFunctions)[action]->isType(GRADIENTVFUNCTION)) {
		GradientVFunction *gradVFunc =
		    dynamic_cast<GradientVFunction *>((*vFunctions)[action]);
		gradVFunc->getGradient(stateCol, gradient);

		gradient->addIndexOffset(getWeightsOffset(action));
	}
}

void QFunction::updateWeights(FeatureList *features) {
	unsigned int featureBegin = 0;
	unsigned int featureEnd = 0;

	if(DebugIsEnabled('q')) {
		DebugPrint('q', "Updating Features: ");
		features->save(DebugGetFileHandle('q'));
		DebugPrint('q', "\n");
	}

	if(isType(GRADIENTQFUNCTION)) {
		std::map<Action *, AbstractVFunction *>::iterator it =
		    vFunctions->begin();
		FeatureList::iterator itFeat;

		for(int i = 0; it != vFunctions->end(); it++, i++) {
			GradientVFunction *gradVFunction =
			    dynamic_cast<GradientVFunction *>((*it).second);
			featureEnd += gradVFunction->getNumWeights();

			localGradientQFunctionFeatures->clear();

			for(itFeat = features->begin(); itFeat != features->end();
			    itFeat++) {
				if((*itFeat)->featureIndex >= featureBegin
				    && (*itFeat)->featureIndex < featureEnd) {
					localGradientQFunctionFeatures->update(
					    (*itFeat)->featureIndex - featureBegin,
					    (*itFeat)->factor);
				}
			}

			if(DebugIsEnabled('q')) {
				DebugPrint('q', "Updating Features for Action %d: ", i);
				localGradientQFunctionFeatures->save(
				    DebugGetFileHandle('q'));
				DebugPrint('q', "\n");
			}
			gradVFunction->updateGradient(localGradientQFunctionFeatures, 1.0);

			featureBegin += gradVFunction->getNumWeights();
		}
	}
}

int QFunction::getNumWeights() {
	int nparams = 0;
	std::map<Action *, AbstractVFunction *>::iterator it = vFunctions->begin();
	for(; it != vFunctions->end(); it++) {
		GradientVFunction *gradVFunction =
		    dynamic_cast<GradientVFunction *>((*it).second);
		nparams += gradVFunction->getNumWeights();
	}
	return nparams;
}

int QFunction::getWeightsOffset(Action *action) {
	int nparams = 0;
	std::map<Action *, AbstractVFunction *>::iterator it = vFunctions->begin();
	for(; it != vFunctions->end(); it++) {
		if((*it).first == action) {
			break;
		}
		GradientVFunction *gradVFunction =
		    dynamic_cast<GradientVFunction *>((*it).second);
		nparams += gradVFunction->getNumWeights();
	}
	return nparams;
}

void QFunction::getWeights(RealType *weights) {
	RealType *vFuncWeights = weights;
	std::map<Action *, AbstractVFunction *>::iterator it = vFunctions->begin();
	for(; it != vFunctions->end(); it++) {
		GradientVFunction *gradVFunction =
		    dynamic_cast<GradientVFunction *>((*it).second);
		gradVFunction->getWeights(vFuncWeights);
		vFuncWeights += gradVFunction->getNumWeights();
	}
}

void QFunction::setWeights(RealType *weights) {
	RealType *vFuncWeights = weights;
	std::map<Action *, AbstractVFunction *>::iterator it = vFunctions->begin();
	for(; it != vFunctions->end(); it++) {
		GradientVFunction *gradVFunction =
		    dynamic_cast<GradientVFunction *>((*it).second);
		gradVFunction->setWeights(vFuncWeights);
		vFuncWeights += gradVFunction->getNumWeights();
	}
}

QFunctionFromStochasticModel::QFunctionFromStochasticModel(
    FeatureVFunction *vfunction, AbstractFeatureStochasticModel *model,
    FeatureRewardFunction *rewardfunction) :
	    AbstractQFunction(model->getActions()),
	    StateObject(vfunction->getStateProperties()) {
	this->vfunction = vfunction;
	this->model = model;
	this->discretizer = vfunction->getStateProperties();
	this->rewardfunction = rewardfunction;

	discState = new State(new StateProperties(0, 1, DISCRETESTATE));
	discState->getStateProperties()->setDiscreteStateSize(0,
	    vfunction->getNumFeatures());

	addParameter("DiscountFactor", 0.95);
}

QFunctionFromStochasticModel::~QFunctionFromStochasticModel() {
	delete discState->getStateProperties();
	delete discState;
}

RealType QFunctionFromStochasticModel::getValue(
    StateCollection *state, Action *action, ActionData *) {
	RealType value = getValue(state->getState(properties), action);

	return value;
}

RealType QFunctionFromStochasticModel::getValue(
    int state, Action *action, ActionData *) {
	discState->setDiscreteState(0, state);

	RealType value = DynamicProgramming::getActionValue(model,
	    this->rewardfunction, this->vfunction, discState, action,
	    getParameter("DiscountFactor"));

	return value;
}

RealType QFunctionFromStochasticModel::getValue(
    State *featState, Action *action, ActionData *) {
	RealType stateValue = 0.0;

	int type = featState->getStateProperties()->getType()
	    & (DISCRETESTATE | FEATURESTATE);
	switch(type) {
	case DISCRETESTATE:
		stateValue = DynamicProgramming::getActionValue(model,
		    this->rewardfunction, this->vfunction, featState, action,
		    getParameter("DiscountFactor"));
	break;
	case FEATURESTATE:
		for(unsigned int i = 0; i < featState->getNumContinuousStates(); i++) {
			stateValue += getValue(featState->getDiscreteState(i), action)
			    * featState->getContinuousState(i);
		}
		break;
	break;
	default:
		stateValue = getValue(featState->getDiscreteStateNumber(), action);
	break;
	}

	return stateValue;
}

FeatureQFunction::FeatureQFunction(
    ActionSet *actions, StateProperties *discretizer) :
	    QFunction(actions),
	    _discretizer(discretizer),
	    _features(discretizer->getDiscreteStateSize()) {
	init();
}

FeatureQFunction::FeatureQFunction(
    FeatureVFunction *vfunction, AbstractFeatureStochasticModel *model,
    FeatureRewardFunction *rewardFunction, RealType gamma) :
	    QFunction(model->getActions()),
	    _discretizer((StateModifier *) vfunction->getStateProperties()),
	    _features(_discretizer->getDiscreteStateSize()) {
	init();
	initVFunctions(vfunction, model, rewardFunction, gamma);
}

void FeatureQFunction::init() {
	FeatureVFunction *vFunction = nullptr;

	for(ActionSet::iterator it = actions->begin(); it != actions->end(); it++) {
		vFunction = new FeatureVFunction(_discretizer);
		_featureVFunctions.push_back(vFunction);
		this->setVFunction(*it, vFunction);
	}
}

FeatureQFunction::~FeatureQFunction() {
	for(std::list<FeatureVFunction *>::iterator it = _featureVFunctions.begin();
	    it != _featureVFunctions.end(); it++) {
		delete *it;
	}
}

void FeatureQFunction::setFeatureCalculator(StateModifier *discretizer) {
	assert(
	    discretizer == nullptr
	        || discretizer->getDiscreteStateSize() == _features);
	_discretizer = discretizer;
}

StateProperties *FeatureQFunction::getFeatureCalculator() {
	return _discretizer;
}

int FeatureQFunction::getNumFeatures() {
	return _features;
}

void FeatureQFunction::initVFunctions(
    FeatureVFunction *vfunction, AbstractFeatureStochasticModel *model,
    FeatureRewardFunction *rewardFunction, RealType gamma) {
	std::list<Action*>::iterator itAction;
	State *discState = new State(new StateProperties(0, 1));

	for(int feature = 0; feature < getNumFeatures(); feature++) {
		discState->setDiscreteState(0, feature);
		for(itAction = actions->begin(); itAction != actions->end();
		    itAction++) {
			((FeatureVFunction *) (*vFunctions)[*itAction])->setFeature(feature,
			    DynamicProgramming::getActionValue(model, rewardFunction,
			        vfunction, discState, *itAction, gamma));
		}
	}

	delete discState->getStateProperties();
	delete discState;
}

void FeatureQFunction::updateValue(
    Feature *state, Action *action, RealType td, ActionData *) {
	((FeatureVFunction *) getVFunction(action))->updateFeature(state, td);
}

void FeatureQFunction::setValue(
    int state, Action *action, RealType qValue, ActionData *) {
	((FeatureVFunction *) getVFunction(action))->setFeature(state, qValue);
}

RealType FeatureQFunction::getValue(int feature, Action *action, ActionData *) {
	return ((FeatureVFunction *) getVFunction(action))->getFeature(feature);
}

void FeatureQFunction::saveFeatureActionValueTable(std::ostream& stream) {
	stream << "Q-FeatureActionValue Table" << std::endl;
	ActionSet::iterator it;

	for(unsigned int i = 0; i < _discretizer->getDiscreteStateSize(); i++) {
		stream << "State " << i << ": ";
		for(it = actions->begin(); it != actions->end(); it++) {
			stream << ((FeatureVFunction *) (*vFunctions)[*it])->getFeature(i)
			    << " ";
		}
		stream << std::endl;
	}
}

void FeatureQFunction::saveFeatureActionTable(std::ostream& stream) {
	stream << "Q-FeatureAction Table" << std::endl;
	ActionSet::iterator it;
	RealType max = 0.0;
	unsigned int maxIndex = 0;

	for(unsigned int i = 0; i < _discretizer->getDiscreteStateSize(); i++) {
		stream << "State " << i << ": ";

		it = actions->begin();
		max = ((FeatureVFunction *) (*vFunctions)[*it])->getFeature(i);
		it++;
		maxIndex = 0;
		for(unsigned int j = 1; it != actions->end(); it++, j++) {
			RealType qValue =
			    ((FeatureVFunction*) (*vFunctions)[*it])->getFeature(i);
			if(max < qValue) {
				max = qValue;
				maxIndex = j;
			}
		}

		stream << maxIndex << std::endl;
	}
}

ComposedQFunction::ComposedQFunction() :
	GradientQFunction(new ActionSet()) {
	this->qFunctions = new std::list<AbstractQFunction *>();
	//gradientFeatures = new FeatureList();
}

ComposedQFunction::~ComposedQFunction() {
	delete qFunctions;
	//delete gradientFeatures;
}

void ComposedQFunction::saveData(std::ostream& file) {
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	fmt::fprintf(file, "Composed QFunction (containing %d QFunctions)\n",
	    qFunctions->size());
	for(; it != qFunctions->end(); it++) {
		(*it)->saveData(file);
	}
}

void ComposedQFunction::loadData(std::istream& file) {
	int buf = 0;
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	xscanf(file, "Composed QFunction (containing %d QFunctions)\n", buf);
	for(; it != qFunctions->end(); it++) {
		(*it)->loadData(file);
	}
}

void ComposedQFunction::printValues() {
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		(*it)->printValues();
	}
}

void ComposedQFunction::getStatistics(
    StateCollection *state, Action *action, ActionSet *actions,
    ActionStatistics* statistics) {
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		if((*it)->getActions()->isMember(action)) {
			(*it)->getStatistics(state, action, actions, statistics);
		}
	}
}

void ComposedQFunction::updateValue(StateCollection *state, Action *action,
RealType td, ActionData *data) {
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		if((*it)->getActions()->isMember(action)) {
			(*it)->updateValue(state, action, td, data);
		}
	}
}

void ComposedQFunction::setValue(StateCollection *state, Action *action,
RealType qValue, ActionData *data) {
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		if((*it)->getActions()->isMember(action)) {
			(*it)->setValue(state, action, qValue, data);
		}
	}
}

RealType ComposedQFunction::getValue(
    StateCollection *state, Action *action, ActionData *data) {
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		if((*it)->getActions()->isMember(action)) {
			return (*it)->getValue(state, action, data);
		}
	}
	return 0;
}

void ComposedQFunction::addQFunction(AbstractQFunction *qFunction) {
	qFunctions->push_back(qFunction);

	actions->add(qFunction->getActions());

	if(!qFunction->isType(GRADIENTQFUNCTION)) {
		type = type & (~ GRADIENTQFUNCTION);
	}
	addParameters(qFunction);
}

std::list<AbstractQFunction *> *ComposedQFunction::getQFunctions() {
	return qFunctions;
}

int ComposedQFunction::getNumQFunctions() {
	return qFunctions->size();
}

AbstractQETraces *ComposedQFunction::getStandardETraces() {
	return new ComposedQETraces(this);
}

void ComposedQFunction::getGradient(
    StateCollection *stateCol, Action *action, ActionData *data,
    FeatureList *gradient) {
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();

	for(; it != qFunctions->end(); it++) {
		if((*it)->getActions()->isMember(action)) {
			if((*it)->isType(GRADIENTQFUNCTION)) {
				GradientQFunction *gradQFunc =
				    dynamic_cast<GradientQFunction *>(*it);
				gradQFunc->getGradient(stateCol, action, data, gradient);
				gradient->addIndexOffset(getWeightsOffset(action));
			}
		}
	}
}

void ComposedQFunction::updateWeights(FeatureList *features) {
	unsigned int featureBegin = 0;
	unsigned int featureEnd = 0;
	if(isType(GRADIENTQFUNCTION)) {
		std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
		FeatureList::iterator itFeat;

		for(; it != qFunctions->end(); it++) {
			GradientQFunction *gradQFunction =
			    dynamic_cast<GradientQFunction *>(*it);
			featureEnd += gradQFunction->getNumWeights();

			localGradientQFunctionFeatures->clear();

			for(itFeat = features->begin(); itFeat != features->end(); it++) {
				if((*itFeat)->featureIndex >= featureBegin
				    && (*itFeat)->featureIndex < featureEnd) {
					localGradientQFunctionFeatures->add(*itFeat);
				}
			}
			gradQFunction->updateGradient(localGradientQFunctionFeatures);
		}
	}
}

int ComposedQFunction::getNumWeights() {
	int nparams = 0;
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		GradientQFunction *gradQFunction =
		    dynamic_cast<GradientQFunction *>(*it);
		nparams += gradQFunction->getNumWeights();
	}
	return nparams;
}

int ComposedQFunction::getWeightsOffset(Action *action) {

	int nparams = 0;
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		GradientQFunction *gradQFunction =
		    dynamic_cast<GradientQFunction *>(*it);

		if((*it)->getActions()->isMember(action)) {
			break;
		}
		nparams += gradQFunction->getNumWeights();
	}
	return nparams;
}

void ComposedQFunction::getWeights(RealType *weights) {
	RealType *qFuncWeights = weights;
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		GradientQFunction *gradQFunction =
		    dynamic_cast<GradientQFunction *>(*it);
		gradQFunction->getWeights(qFuncWeights);
		qFuncWeights += gradQFunction->getNumWeights();
	}
}

void ComposedQFunction::setWeights(RealType *weights) {
	RealType *qFuncWeights = weights;
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		GradientQFunction *gradQFunction =
		    dynamic_cast<GradientQFunction *>(*it);
		gradQFunction->setWeights(qFuncWeights);
		qFuncWeights += gradQFunction->getNumWeights();
	}
}

void ComposedQFunction::resetData() {
	std::list<AbstractQFunction *>::iterator it = qFunctions->begin();
	for(; it != qFunctions->end(); it++) {
		(*it)->resetData();
	}
}

} //namespace rlearner
