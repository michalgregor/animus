#include "WorldBase.h"
#include "ril_debug.h"
#include "statecollection.h"

namespace rlearner {

void WorldBase::addStateModifier(StateModifier* modifier) {
	_modifierList.addStateModifier(modifier);
}

void WorldBase::removeStateModifier(StateModifier* modifier) {
	_modifierList.removeStateModifier(modifier);
}

void WorldBase::addModifierList(StateModifierList* modifierList) {
	_lastState->addModifierList(modifierList);
	_currentState->addModifierList(modifierList);

	for(auto& agent: _agents) {
		agent->addModifierList(modifierList);
	}
}

void WorldBase::removeModifierList(StateModifierList* modifierList) {
	_lastState->removeModifierList(modifierList);
	_currentState->removeModifierList(modifierList);

	for(auto& agent: _agents) {
		agent->removeModifierList(modifierList);
	}
}

const std::vector<Action*>& WorldBase::makeStep() {
	std::swap(_currentState, _lastState);
	writeState(_currentState.get());

	if(_lastActions.size()) {
		SYS_ASSERT(_agents.size() == _lastActions.size());

		auto ag = _agents.begin();
		auto ac = _lastActions.begin();

		for(; ag != _agents.end(); ag++, ac++) {
			(*ag)->sendNextStep(_lastState.get(), *ac, _currentState.get());
			*ac = (*ag)->computeAction(_currentState.get());
		}
	} else {
		_lastActions.resize(_agents.size());

		auto ag = _agents.begin();
		auto ac = _lastActions.begin();

		for(; ag != _agents.end(); ag++, ac++) {
			*ac = (*ag)->computeAction(_currentState.get());
		}
	}

	_currentSteps++;
	_totalSteps++;

	return _lastActions;
}

void WorldBase::writeState(StateCollectionImpl* stateCollection) {
	writeState(stateCollection->getState());
	stateCollection->newModelState();
}

void WorldBase::startNewEpisode() {
	DebugPrint('a', "Starting new Episode\n");

	for(auto& agent: _agents) {
		agent->startNewEpisode();
	}

	_currentSteps = 0;
	_currentEpisodeNumber++;

	_currentState->newModelState();
}

StateCollection* WorldBase::getCurrentState() {
	return _currentState.get();
}

void WorldBase::registerAgent(const shared_ptr<Agent>& agent) {
	_currentState->addModifierList(agent->getModifierList());
	_lastState->addModifierList(agent->getModifierList());
	agent->addModifierList(&_modifierList);
}

void WorldBase::unregisterAgent(const shared_ptr<Agent>& agent) {
	_currentState->removeModifierList(agent->getModifierList());
	_lastState->removeModifierList(agent->getModifierList());
	agent->removeModifierList(&_modifierList);
}

void WorldBase::addAgent(const shared_ptr<Agent>& agent) {
	// only register the agent if it has not been added already
	if(_agents.insert(agent).second) {
		registerAgent(agent);
	}

	_lastActions.clear();
}

void WorldBase::removeAgent(const shared_ptr<Agent>& agent) {
	auto iter = _agents.find(agent);

	// only unregister the agent if it has been added in the first place
	if(iter != _agents.end()) {
		unregisterAgent(agent);
		_agents.erase(iter);
	}

	_lastActions.clear();
}

WorldBase::WorldBase(
	StateProperties* properties,
	const std::set<shared_ptr<Agent> >& agentList
):
	_agents(agentList), _currentEpisodeNumber(0), _currentSteps(0),
	_totalSteps(0), _lastState(), _currentState(), _modifierList()
{
	assert(properties);

	_currentState = std::unique_ptr<StateCollectionImpl>(new StateCollectionImpl(properties, {&_modifierList}));
	_lastState = std::unique_ptr<StateCollectionImpl>(new StateCollectionImpl(properties, {&_modifierList}));

	for(auto& agent: _agents) {
		registerAgent(agent);
	}

	startNewEpisode();
	_currentEpisodeNumber = 0;
}

WorldBase::WorldBase(
	StateProperties* properties,
	std::set<shared_ptr<Agent> >&& agentList
):
	_agents(std::move(agentList)), _currentEpisodeNumber(0), _currentSteps(0),
	_totalSteps(0), _lastState(), _currentState(), _modifierList()
{
	assert(properties);

	_currentState = std::unique_ptr<StateCollectionImpl>(new StateCollectionImpl(properties, {&_modifierList}));
	_lastState = std::unique_ptr<StateCollectionImpl>(new StateCollectionImpl(properties, {&_modifierList}));

	for(auto& agent: _agents) {
		registerAgent(agent);
	}

	startNewEpisode();
	_currentEpisodeNumber = 0;
}

WorldBase::~WorldBase() {
	_currentState->removeModifierList(&_modifierList);
	_lastState->removeModifierList(&_modifierList);

	for(auto& agent: _agents) {
		_currentState->removeModifierList(agent->getModifierList());
		_lastState->removeModifierList(agent->getModifierList());
		agent->removeModifierList(&_modifierList);
	}
}

} // namespace rlearner
