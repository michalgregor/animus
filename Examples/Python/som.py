# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import orbis.annalyst as ann
import numpy as np

# Funkcia na vytvorenie zhluku.
def new_cluster(center, c_size, num_points):
    num_coordinates = len(center)
    
    if len(c_size) == 1:
        c_size = [c_size for i in range(num_coordinates)]
    
    return np.array(
        [center[i] + c_size[i]*np.random.randn(num_points)
        for i in range(num_coordinates)]
    )
   
# Vytvoríme zopár zhlukov.
X1 = new_cluster([1, 1], [0.8, 0.6], 15)
X2 = new_cluster([3, 4], [0.7, 0.3], 20)
X3 = new_cluster([7, 2], [0.4, 0.4], 10)
X4 = new_cluster([6, 6], [0.8, 0.3], 12)
X5 = new_cluster([0, 6], [0.5, 0.5], 12)
 
# Zložíme zo zhlukov maticu vstupných vektorov.
X = np.concatenate((X1, X2, X3, X4, X5), axis=1)
# Dátový typ musí byť float32 kvôli C++ rozhraniu.
X = np.array(X, dtype=np.float32).transpose()

# Vytvoríme vektor s číslami zhlukov.
labels = np.concatenate(
    ([1 for i in range(len(X1[0]))], [2 for i in range(len(X2[0]))],
    [3 for i in range(len(X3[0]))], [4 for i in range(len(X4[0]))],
    [5 for i in range(len(X5[0]))])
)
labels = [l-1 for l in labels]

# Vytvoríme samoorganizujúcu sa mapu.
som = ann.Network_SOM(X, 6, 4)
# Spustíme učenie.
som.fit(X, 200, 195, 6, 0, 0.1)

# Zobrazíme mapu zásahov.
som.hitmap(X)
plt.tight_layout()
plt.gca().get_figure().set_size_inches([5, 5])
plt.savefig('som_hitmap.pdf')

# Zobrazíme U-maticu.
som.umatrix()
plt.tight_layout()
plt.gca().get_figure().set_size_inches([5, 5])
plt.savefig('som_umatrix.pdf')

# Zobrazíme rozšírenú mapu zásahov.
colors = ['red', 'green', 'blue', 'magenta', 'black', 'orange']
som.hitmapCategory(X, labels=labels, colors=colors)
plt.tight_layout()
plt.gca().get_figure().set_size_inches([5, 5])
plt.savefig('som_hitmap_enhanced.pdf')

# Zobrazíme vstupný priestor.
som.spacePlot(X, labels, colors)
plt.tight_layout()
plt.gca().get_figure().set_size_inches([6, 5])
plt.savefig('som_input_space.pdf')

# Zobrazíme vstupný priestor bez označení zhlukov.
som.spacePlot(X, labels, colors, labelClusters=False)
plt.tight_layout()
plt.gca().get_figure().set_size_inches([6, 5])
plt.savefig('som_input_space_clean.pdf')
