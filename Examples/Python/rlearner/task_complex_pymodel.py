import orbis.rlearner as rl
from enum import IntEnum

class ActionType(IntEnum):
    move_to_warehouse = 1
    move_to_store = 2
    get_material = 3
    create_product = 4
    take_planks = 5
    drop_planks = 6
    get_water = 7
    water_plants = 8
    take_processor = 9
    replace_processor = 10
    move_to_garage = 11
    move_to_greenhouse = 12
    move_to_pccenter = 13
    move_to_serverroom = 14
     
class Location(IntEnum):
    store = 1
    warehouse = 2
    garage = 3
    greenhouse = 4
    pccenter = 5
    serverroom = 6
    
class ManuAction(rl.PrimitiveAction):
    def __init__(self, actionType):
        super().__init__()
        self.actionType = actionType
        
    def isAvailable(self, state):
        return True
        
    def equals(self, action):
        return action.__cast__().actionType == self.actionType

num_locations = len(Location)
capacity_material = 5
capacity_products = 5
capacity_processors = 5
capacity_water = 5
capacity_planks = 5

class ManuState:
    def __init__(self, state):
        self.state = state
    
    def getNumMaterial(self):
        return self.state.getDiscreteState(0)
        
    def setNumMaterial(self, num):
        self.state.setDiscreteState(0, num)
        
    def getLocation(self):
        return Location(self.state.getDiscreteState(1))
        
    def setLocation(self, loc):
        self.state.setDiscreteState(1, loc)
    
    def getNumProducts(self):
        return self.state.getDiscreteState(2)
        
    def setNumProducts(self, num):
        self.state.setDiscreteState(2, num)
        
    def getNumProcessors(self):
        return self.state.getDiscreteState(3)
        
    def setNumProcessors(self, num):
        self.state.setDiscreteState(3, num)
        
    def getNumWater(self):
        return self.state.getDiscreteState(4)
        
    def setNumWater(self, num):
        self.state.setDiscreteState(4, num)
        
    def getNumPlanks(self):
        return self.state.getDiscreteState(5)
        
    def setNumPlanks(self, num):
        self.state.setDiscreteState(5, num)
        
    def initialize(self):
        self.setNumMaterial(0)
        self.setLocation(Location.store)
        self.setNumProducts(0)
        self.setNumProcessors(0)
        self.setNumWater(0)
        self.setNumPlanks(0)

    def newProperties():
        prop = rl.StateProperties(0, 6)
        prop.setDiscreteStateSize(0, capacity_material + 1)
        prop.setDiscreteStateSize(1, num_locations + 1)
        prop.setDiscreteStateSize(3, capacity_processors + 1)
        prop.setDiscreteStateSize(4, capacity_water + 1)
        prop.setDiscreteStateSize(5, capacity_planks + 1)
        return prop
        
class ComplexTask(rl.TransitionFunction):
    def __init__(self, properties, actions):
        super().__init__(properties, actions)
    
    def transitionFunction(self, oldState, action, newState, data=None):
        newState.setState(oldState)
        oldState = ManuState(oldState)
        newState = ManuState(newState)
        action = action.__cast__()
        
        if action.actionType == ActionType.move_to_warehouse:
            newState.setLocation(Location.warehouse)
        elif action.actionType == ActionType.move_to_store:
            newState.setLocation(Location.store)
        elif action.actionType == ActionType.get_material:
            if oldState.getLocation() == Location.store:
                material = oldState.getNumMaterial()
                if material < capacity_material:
                    newState.setNumMaterial(material + 1)
        elif action.actionType == ActionType.create_product:
            if oldState.getLocation() == Location.warehouse:
                material = oldState.getNumMaterial()
                planks = oldState.getNumPlanks()
                
                if material >= 1 and planks >= 1:
                    newState.setNumProducts(oldState.getNumProducts() + 1)
                    newState.setNumMaterial(material - 1)
                    newState.setNumPlanks(planks - 1)
        elif action.actionType == ActionType.take_planks:
            if oldState.getLocation() == Location.store:
                planks = newState.getNumPlanks()
                if planks < capacity_planks:
                    newState.setNumPlanks(planks + 1)
        elif action.actionType == ActionType.drop_planks:
            planks = newState.getNumPlanks()
            if planks > 0:
                newState.setNumPlanks(planks - 1)
        elif action.actionType == ActionType.get_water:
            if oldState.getLocation() == Location.greenhouse:
                water = newState.getNumWater()
                if water < capacity_water:
                    newState.setNumWater(water + 1)
        elif action.actionType == ActionType.water_plants:
            if oldState.getLocation() == Location.greenhouse:
                water = newState.getNumWater()
                if water > 0:
                    newState.setNumWater(water - 1)
        elif action.actionType == ActionType.take_processor:
            if oldState.getLocation() == Location.pccenter:
                processors = newState.getNumProcessors()
                if processors < capacity_processors:
                    newState.setNumProcessors(processors + 1)
        elif action.actionType == ActionType.replace_processor:
            if oldState.getLocation() == Location.pccenter:
                processors = newState.getNumProcessors()
                if processors > 0:
                    newState.setNumProcessors(processors - 1)
        elif action.actionType == ActionType.move_to_garage:
            newState.setLocation(Location.garage)
        elif action.actionType == ActionType.move_to_greenhouse:
            newState.setLocation(Location.greenhouse)
        elif action.actionType == ActionType.move_to_pccenter:
            newState.setLocation(Location.pccenter)
        elif action.actionType == ActionType.move_to_serverroom:
            newState.setLocation(Location.serverroom)
    
    def getResetState(self, resetState):
        state = ManuState(resetState)
        state.initialize()
        
    def isResetState(self, state):
        return False
        
    def isFailedState(self, state):
        return False
        
class ComplexReward(rl.RewardFunction):
    def __init__(self):
        super().__init__()
    
    def getReward(self, oldState, action, newState):
        oldState = ManuState(oldState.getState())
        newState = ManuState(newState.getState())
        reward = max(newState.getNumProducts() - oldState.getNumProducts(), 0)
        return reward
    
actions = [ManuAction(at) for at in ActionType]
                      
properties = ManuState.newProperties()           
model = ComplexTask(properties, rl.ActionSet())
environmentModel = rl.TransitionFunctionEnvironment(model)
rewardFunction = ComplexReward()
discretizer = rl.ModelStateDiscretizer(properties, [0, 1, 3, 4, 5])

agent = rl.Agent(properties)

for a in actions:
    agent.addAction(a)

actionset = agent.getActions()

agent.addStateModifier(discretizer)

qFunction = rl.FeatureQFunction(actionset, discretizer)
qFunctionLearner = rl.QLearner(rewardFunction, qFunction)

egreedy = rl.EpsilonGreedyDistribution(0.1)
qLearnerPolicy = rl.QStochasticPolicy(agent.getActions(),
  egreedy, qFunction)

qFunctionLearner.setParameter("ReplacingETraces", 1.0)
qFunctionLearner.setParameter("Lambda", 0.95)

agent.addSemiMDPListener(qFunctionLearner)
agent.setController(qLearnerPolicy)
agent.setLogEpisode(False)

agentSimulator = rl.AgentSimulator(environmentModel, agent)

totalSteps = 0

for i in range(50):
    agentSimulator.startNewEpisode()
    steps = agentSimulator.doControllerEpisode(1000)
    totalSteps += steps
    
    products = rl.TaskComplex_ManuState(agentSimulator.getCurrentState().getState()).getNumProducts()
    
    if environmentModel.isFailed() or steps >= 1000:
        print("Episode {}; agent created {} products; in {} steps".format(
              i, products, steps))

del agentSimulator
del agent
del environmentModel
