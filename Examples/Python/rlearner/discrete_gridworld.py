import orbis.rlearner as rl
        
gridWorldModel = rl.GridWorldModel("Gridworld_10x10.txt", 50)
gridWorldModel.setRewardStandard(-0.2)
gridWorldModel.setRewardBounce(-0.75)
gridWorldModel.setRewardSuccess(100.0)

environmentModel = rl.TransitionFunctionEnvironment(gridWorldModel)
rewardFunction = gridWorldModel

agent = rl.Agent(environmentModel.getStateProperties())

actions = [
    rl.GridWorldAction(-1, 0),
    rl.GridWorldAction(1, 0),
    rl.GridWorldAction(0, -1),
    rl.GridWorldAction(0, 1)
]

for a in actions:
    agent.addAction(a)

actionset = agent.getActions()

globalGridworldstate = rl.GridWorldDiscreteState_Global(
    gridWorldModel.getSizeX(), gridWorldModel.getSizeY()
)

agent.addStateModifier(globalGridworldstate)

logger = rl.AgentLogger(gridWorldModel.getStateProperties(), actionset)
agent.addSemiMDPListener(logger)

qFunction = rl.FeatureQFunction(actionset, globalGridworldstate)
qFunctionLearner = rl.QLearner(rewardFunction, qFunction)

qFunctionLearner = rl.SarsaLearner(rewardFunction, qFunction, agent)

egreedy = rl.EpsilonGreedyDistribution(0.1)
qLearnerPolicy = rl.QStochasticPolicy(agent.getActions(),
  egreedy, qFunction)

qFunctionLearner.setParameter("ReplacingETraces", 1.0)
qFunctionLearner.setParameter("Lambda", 0.95)

agent.addSemiMDPListener(qFunctionLearner)
agent.setController(qLearnerPolicy)
agent.setLogEpisode(False)

agentSimulator = rl.AgentSimulator(environmentModel, agent)

totalSteps = 0
ges_failed = 0
ges_succeeded = 0
last_succeeded = 0

for i in range(50):
    agentSimulator.startNewEpisode()
    steps = agentSimulator.doControllerEpisode(1000)
    totalSteps += steps
    
    if environmentModel.isFailed() or steps >= 1000:
        ges_failed += 1
        last_succeeded = 0
        print("Episode {} failed with {} steps".format(i, steps))
    else:
        ges_succeeded += 1
        last_succeeded += 1
        
        print("Episode {} succeeded with {} steps, {} Episodes succeded in the row.".format(
              i, steps, last_succeeded))

del agentSimulator
del logger
del agent
del environmentModel
