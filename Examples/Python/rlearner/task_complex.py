import orbis.rlearner as rl

ActionTypes = [
    rl.TaskComplex_ActionType_CREATE_PRODUCT,
    rl.TaskComplex_ActionType_MOVE_TO_WAREHOUSE,
    rl.TaskComplex_ActionType_MOVE_TO_STORE,
    rl.TaskComplex_ActionType_GET_MATERIAL,
    rl.TaskComplex_ActionType_CREATE_PRODUCT,
    rl.TaskComplex_ActionType_TAKE_PLANKS,
    rl.TaskComplex_ActionType_DROP_PLANKS,
    rl.TaskComplex_ActionType_GET_WATER,
    rl.TaskComplex_ActionType_WATER_PLANTS,
    rl.TaskComplex_ActionType_TAKE_PROCESSOR,
    rl.TaskComplex_ActionType_REPLACE_PROCESSOR,
    rl.TaskComplex_ActionType_MOVE_TO_GARAGE,
    rl.TaskComplex_ActionType_MOVE_TO_GREENHOUSE,
    rl.TaskComplex_ActionType_MOVE_TO_PCCENTER,
    rl.TaskComplex_ActionType_MOVE_TO_SERVERROOM
]

actions = [rl.TaskComplex_ManuAction(at) for at in ActionTypes]   
task = rl.Task_Complex()
environmentModel = task.getEnvironment()
rewardFunction = task.getRewardFunction()
properties = environmentModel.getStateProperties()
discretizer = task.newDiscretizer()

agent = rl.Agent(properties)

for a in actions:
    agent.addAction(a)

actionset = agent.getActions()

agent.addStateModifier(discretizer)

qFunction = rl.FeatureQFunction(actionset, discretizer)
qFunctionLearner = rl.QLearner(rewardFunction, qFunction)

egreedy = rl.EpsilonGreedyDistribution(0.1)
qLearnerPolicy = rl.QStochasticPolicy(agent.getActions(),
  egreedy, qFunction)

qFunctionLearner.setParameter("ReplacingETraces", 1.0)
qFunctionLearner.setParameter("Lambda", 0.95)

agent.addSemiMDPListener(qFunctionLearner)
agent.setController(qLearnerPolicy)
agent.setLogEpisode(False)

agentSimulator = rl.AgentSimulator(environmentModel, agent)

totalSteps = 0

for i in range(50):
    agentSimulator.startNewEpisode()
    steps = agentSimulator.doControllerEpisode(1000)
    totalSteps += steps
    
    products = rl.TaskComplex_ManuState(agentSimulator.getCurrentState().getState()).getNumProducts()
    
    if environmentModel.isFailed() or steps >= 1000:
        print("Episode {}; agent created {} products; in {} steps".format(
              i, products, steps))

del agentSimulator
del agent
del environmentModel
