#include <iostream>

#include <Rlearner/benchmarks/taxidomain.h>
#include <Rlearner/state.h>
#include <Rlearner/tdlearner.h>
#include <Rlearner/policies.h>
#include <Rlearner/hierarchiccontroller.h>
#include <Rlearner/rewardmodel.h>
#include <Rlearner/AgentSimulator.h>
using namespace rlearner;

int main(void) {
	shared_ptr<TaxiDomain> taxi;
	shared_ptr<TransitionFunctionEnvironment> model;
	shared_ptr<Agent> agent;
	RewardFunction* rewardFunction = nullptr;

	std::cout << "-=<   Reinforcement Learning Benchmark - TaxiDomain   >=-\n\n";

	taxi = make_shared<TaxiDomain>("gridworld.txt");
	taxi->setRewardStandard(-1);
	taxi->setRewardBounce(-5.0);
	taxi->setRewardSuccess(100.0);

	model = make_shared<TransitionFunctionEnvironment>(taxi.get());

	// in order during the learning process, we will use 50 fixed start states
	StateList *stateList = new StateList(model->getStateProperties());
	State *state = new State(model->getStateProperties());

	for (int i = 0; i < 50; i++) {
		taxi->getResetState(state);
		stateList->addState(state);
	}

	model->setStartStates(stateList);

	rewardFunction = taxi.get();
	agent = make_shared<Agent>(model->getStateProperties());
	PrimitiveAction *left = new GridWorldAction(-1, 0);
	PrimitiveAction *right = new GridWorldAction(1, 0);
	PrimitiveAction *up = new GridWorldAction(0, -1);
	PrimitiveAction *down = new GridWorldAction(0, 1);
	PrimitiveAction *pickup = new PickupAction();
	PrimitiveAction *drop = new PutdownAction();

	agent->addAction(left);
	agent->addAction(right);
	agent->addAction(up);
	agent->addAction(down);
	agent->addAction(pickup);
	agent->addAction(drop);

	SemiMDPLastNRewardFunction* semiMDPRewardFunction = new SemiMDPLastNRewardFunction(rewardFunction, 0.95);
	agent->addSemiMDPListener(semiMDPRewardFunction);

	HierarchicalSemiMarkovDecisionProcess* hierarchicalRoot = new HierarchicalSemiMarkovDecisionProcess(agent->getCurrentEpisode());
	hierarchicalRoot->addAction(pickup);
	hierarchicalRoot->addAction(drop);

	AbstractStateDiscretizer *xyState = new ModelStateDiscretizer(model->getStateProperties(), std::vector<int>({0, 1}));
	agent->addStateModifier(xyState);

	std::vector<shared_ptr<TDLearner> > learners(taxi->getNumTargets());
	TaxiHierarchicalBehaviour** behaviours = new TaxiHierarchicalBehaviour*[taxi->getNumTargets()];
	QFunction** qfunctions = new QFunction*[taxi->getNumTargets()];
	AgentController** policies = new AgentController*[taxi->getNumTargets()];

	for(int i = 0; i < taxi->getNumTargets(); i++) {
		TaxiHierarchicalBehaviour* behaviour = new TaxiHierarchicalBehaviour(agent->getCurrentEpisode(), i, taxi.get());
		behaviour->addAction(left);
		behaviour->addAction(right);
		behaviour->addAction(up);
		behaviour->addAction(down);
		behaviour->addStateModifier(xyState);

		hierarchicalRoot->addAction(behaviour);
		behaviour->sendIntermediateSteps = false;

		QFunction* qFunc = new FeatureQFunction(behaviour->getActions(), xyState);
		auto tdLearner = make_shared<QLearner>(behaviour, qFunc);
		tdLearner->setParameter("QLearningRate", 0.1);
		behaviour->addSemiMDPListener(tdLearner.get());

		AgentController* policy = new QStochasticPolicy(behaviour->getActions(), new GreedyDistribution(), qFunc);
		behaviour->setController(policy);

		behaviours[i] = behaviour;
		policies[i] = policy;
		qfunctions[i] = qFunc;
		learners[i] = tdLearner;
	}

	ActionSet* allActions = new ActionSet();
	for(int i = 0; i < agent->getActions()->size(); i++) {
		allActions->add(agent->getActions()->get(i));
	}

	for (int i = 0; i < taxi->getNumTargets(); i++) {
		allActions->add(behaviours[i]);
	}

	HierarchicalController *hierarchicalController = new HierarchicalController(agent->getActions(), allActions, hierarchicalRoot);
	// The controller must be added to the listener list of the agent
	// (as the only hierarchical object)!
	agent->addSemiMDPListener(hierarchicalController);

	hierarchicalController->addHierarchicalStackListener(hierarchicalRoot);
	for(int i = 0; i < taxi->getNumTargets(); i++) {
		hierarchicalController->addHierarchicalStackListener(behaviours[i]);
	}

	auto discretizer = make_shared<ModelStateDiscretizer>(model->getStateProperties(), std::vector<int>({0, 1, 3, 4}));

	agent->addStateModifier(discretizer.get());
	hierarchicalRoot->addStateModifier(discretizer.get());

	shared_ptr<FeatureQFunction> qTable1 = make_shared<FeatureQFunction>(hierarchicalRoot->getActions(), discretizer.get());
	shared_ptr<TDLearner> learner1 = make_shared<QLearner>(semiMDPRewardFunction, qTable1.get());
	learner1->setParameter("QLearningRate", 0.1);
	hierarchicalRoot->addSemiMDPListener(learner1.get());

	shared_ptr<AgentController> policy = make_shared<QStochasticPolicy>(hierarchicalRoot->getActions(), new SoftMaxDistribution(100), qTable1.get());
	hierarchicalRoot->setController(policy.get());
	agent->setController(hierarchicalController);
	agent->setLogEpisode(true);

	auto agentSimulator = make_shared<AgentSimulator>(model, agent);

	int steps = 0;
	int totalSteps = 0;
	int ges_failed = 0;

	// Start Learning, Learn 50 Episodes
	for(int i = 0; i < 1000; i++) {
		// Start a new Episode, the agent gets reseted in one of the start states
		agentSimulator->startNewEpisode();
		// Learn 1 Episode with maximal 1000 steps
		steps = agentSimulator->doControllerEpisode(1000);

		totalSteps += steps;

		// Check if the Episode failed
		// The episode has failed if max_bounces has been reached (indicated through environmentModel->isFailed()),
		// or max_steps has been reached

		if(model->isFailed() || steps >= 1000) {
			ges_failed++;
		}

		if(i % 50 == 0 && i > 0) {
			std::cout << "Episode " << (i - 50) << " - " << i << " finished on average with " <<
				(totalSteps / 50) << " steps, (" << ges_failed << " failed)" << std::endl;

			ges_failed = 0;
			totalSteps = 0;
		}
	}

	return 0;
}
