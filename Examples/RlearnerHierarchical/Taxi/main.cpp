#include <iostream>

#include <Rlearner/benchmarks/taxidomain.h>
#include <Rlearner/state.h>
#include <Rlearner/tdlearner.h>
#include <Rlearner/policies.h>
#include <Rlearner/AgentSimulator.h>
#include <Rlearner/vfunction.h>
using namespace rlearner;

int main(void) {
	shared_ptr<TaxiDomain> taxi;
	shared_ptr<TransitionFunctionEnvironment> model;
	shared_ptr<Agent> agent;
	RewardFunction *rewardFunction = nullptr;

	std::cout << "-=<   Reinforcement Learning Benchmark - TaxiDomain   >=-\n\n";

	taxi = make_shared<TaxiDomain>("gridworld.txt");
	taxi->setRewardStandard(-1);
	taxi->setRewardBounce(-5.0);
	taxi->setRewardSuccess(100.0);

	model = make_shared<TransitionFunctionEnvironment>(taxi.get());

	StateList *stateList = new StateList(model->getStateProperties());
	State *state = new State(model->getStateProperties());

	for(int i = 0; i < 50; i++) {
		taxi->getResetState(state);
		stateList->addState(state);
	}

	model->setStartStates(stateList);

	rewardFunction = taxi.get();
	agent = make_shared<Agent>(model->getStateProperties());
	PrimitiveAction *left = new GridWorldAction(-1, 0);
	PrimitiveAction *right = new GridWorldAction(1, 0);
	PrimitiveAction *up = new GridWorldAction(0, -1);
	PrimitiveAction *down = new GridWorldAction(0, 1);
	PrimitiveAction *pickup = new PickupAction();
	PrimitiveAction *drop = new PutdownAction();

	agent->addAction(left);
	agent->addAction(right);
	agent->addAction(up);
	agent->addAction(down);
	agent->addAction(pickup);
	agent->addAction(drop);

	shared_ptr<AbstractStateDiscretizer> discretizer;
	shared_ptr<FeatureQFunction> qTable1;
	shared_ptr<TDLearner> learner1;
	shared_ptr<AgentController> policy;

	discretizer = make_shared<ModelStateDiscretizer>(model->getStateProperties(), std::vector<int>({0, 1, 3, 4}));
	agent->addStateModifier(discretizer.get());

	// Init Learner
	qTable1 = make_shared<FeatureQFunction>(agent->getActions(), discretizer.get());
	learner1 = make_shared<QLearner>(rewardFunction, qTable1.get());
	learner1->setParameter("QLearningRate", 0.1);
	learner1->setParameter("ReplacingETraces", 1.0);
	learner1->setParameter("Lambda", 0.9);

	agent->addSemiMDPListener(learner1.get());

	policy = make_shared<QStochasticPolicy>(agent->getActions(),
	    new SoftMaxDistribution(20), qTable1.get());

	agent->setController(policy.get());
	agent->setLogEpisode(true);

	int steps = 0;
	int totalSteps = 0;
	int ges_failed = 0;

	auto agentSimulator = make_shared<AgentSimulator>(model, agent);

	// Start Learning, Learn 50 Episodes
	for(int i = 0; i < 1000; i++) {
		// Start a new Episode, the agent gets reseted in one of the start states
		agentSimulator->startNewEpisode();
		// Learn 1 Episode with maximal 1000 steps
		steps = agentSimulator->doControllerEpisode(1000);

		totalSteps += steps;

		// Check if the Episode failed
		// The episode has failed if max_bounces has been reached (indicated through environmentModel->isFailed()),
		// or max_steps has been reached

		if(model->isFailed() || steps >= 1000) {
			ges_failed++;
		}

		if(i % 50 == 0 && i > 0) {
			std::cout << "Episode " << (i - 50) << " - " << i << " finished on average with " <<
				(totalSteps / 50) << " steps, (" << ges_failed << " failed)" << std::endl;

			ges_failed = 0;
			totalSteps = 0;
		}
	}

	return 0;
}
