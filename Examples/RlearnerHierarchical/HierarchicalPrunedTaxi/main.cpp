
//#include "ril_debug.h"
//#include "cagentlogger.h"
//#include "crewardmodel.h"
//#include "ctorchvfunction.h"
//#include "canalyzer.h"
//#include "caction.h"

#include <iostream>

#include <Rlearner/benchmarks/taxidomain.h>
#include <Rlearner/state.h>
#include <Rlearner/tdlearner.h>
#include <Rlearner/policies.h>
#include <Rlearner/AgentSimulator.h>
#include <Rlearner/hierarchiccontroller.h>

#include <Rlearner/agentlogger.h>
#include <Rlearner/rewardmodel.h>
#include <Rlearner/torchvfunction.h>
#include <Rlearner/analyzer.h>
#include <Rlearner/action.h>

using namespace rlearner;

int main() {
	shared_ptr<TaxiDomain> taxi;
	shared_ptr<TransitionFunctionEnvironment> model;
	shared_ptr<Agent> agent;
	RewardFunction *rewardFunction = nullptr;

	std::cout << "-=<   Reinforcement Learning Benchmark - TaxiDomain   >=-\n\n";

	taxi = make_shared<TaxiDomain>("gridworld.txt");
	taxi->setRewardStandard(-1);
	taxi->setRewardBounce(-5.0);
	taxi->setRewardSuccess(100.0);

	model = make_shared<TransitionFunctionEnvironment>(taxi.get());

	// in order during the learning process, we will use 50 fixed start states  
	StateList *stateList = new StateList(model->getStateProperties());
	State *state = new State(model->getStateProperties());

	for (int i = 0; i < 50; i++) {
		taxi->getResetState(state);
		stateList->addState(state);
	}

	model->setStartStates(stateList);

	rewardFunction = taxi.get();
	agent = make_shared<Agent>(model->getStateProperties());
	PrimitiveAction *left = new GridWorldAction(-1,0);
	PrimitiveAction *right = new GridWorldAction(1,0);
	PrimitiveAction *up = new GridWorldAction(0,-1);
	PrimitiveAction *down = new GridWorldAction(0,1);
	PrimitiveAction *pickup = new PickupAction();
	PrimitiveAction *drop = new PutdownAction();

	agent->addAction(left);
	agent->addAction(right);
	agent->addAction(up);
	agent->addAction(down);
	agent->addAction(pickup);
	agent->addAction(drop);
   
	auto semiMDPRewardFunction = make_shared<SemiMDPLastNRewardFunction>(rewardFunction, 0.95);
	agent->addSemiMDPListener(semiMDPRewardFunction.get());

	// Create 1st hierarchical Level (Pickup, Drop)
	auto hierarchicalRoot = make_shared<HierarchicalSemiMarkovDecisionProcess>(agent->getCurrentEpisode());
	hierarchicalRoot->addAction(pickup);
	hierarchicalRoot->addAction(drop);

	auto xyState = make_shared<ModelStateDiscretizer>(model->getStateProperties(), std::vector<int>({0, 1}));
	agent->addStateModifier(xyState.get());

	TDLearner** learners = new TDLearner*[taxi->getNumTargets()];
	TaxiHierarchicalBehaviour** behaviours = new TaxiHierarchicalBehaviour*[taxi->getNumTargets()];
	QFunction** qfunctions = new QFunction*[taxi->getNumTargets()];
	AgentController** policies = new AgentController*[taxi->getNumTargets()];

	for (int i = 0; i < taxi->getNumTargets(); i ++)
	{
		auto behaviour = new TaxiHierarchicalBehaviour(agent->getCurrentEpisode(), i, taxi.get());
		behaviour->addAction(left);
		behaviour->addAction(right);		
		behaviour->addAction(up);
		behaviour->addAction(down);
		behaviour->addStateModifier(xyState.get());
		hierarchicalRoot->addAction(behaviour);
		behaviour->sendIntermediateSteps = false;

		//hierarchicalController->addHierarchicalStackListener(behaviour);

		auto qFunc = new FeatureQFunction(behaviour->getActions(), xyState.get());
		auto tdLearner = new QLearner(behaviour, qFunc);
		tdLearner->setParameter("QLearningRate", 0.1);
		behaviour->addSemiMDPListener(tdLearner);

		auto policy = new QStochasticPolicy(behaviour->getActions(), new GreedyDistribution(), qFunc);
		behaviour->setController(policy);

		behaviours[i] = behaviour;
		policies[i] = policy;
		qfunctions[i] = qFunc;
		learners[i] = tdLearner;
	}

	auto allActions = new ActionSet();

	for (int i = 0; i < agent->getActions()->size(); i++) {
		allActions->add(agent->getActions()->get(i));
	}

	for (int i = 0; i < taxi->getNumTargets(); i++) {
		allActions->add(behaviours[i]);
	}

	auto hierarchicalController = new HierarchicalController(agent->getActions(), allActions, hierarchicalRoot.get());
	// The controller must be added to the listener list of the agent
	// (as the only hierarchical object)!
	agent->addSemiMDPListener(hierarchicalController);
	hierarchicalController->addHierarchicalStackListener(hierarchicalRoot.get());

	for (int i = 0; i < taxi->getNumTargets(); i++) {
		hierarchicalController->addHierarchicalStackListener(behaviours[i]);
	}

	auto passengerLocation = new ModelStateDiscretizer(model->getStateProperties(), std::vector<int>({3}));
	auto passengerDestination = new ModelStateDiscretizer(model->getStateProperties(), std::vector<int>({4}));
	passengerLocation->addStateSubstitution(taxi->getNumTargets(), passengerDestination);

	auto targets = new TaxiIsTargetDiscreteState(taxi.get());
	auto discretizer = new DiscreteStateOperatorAnd();
	discretizer->addStateModifier(passengerLocation);
	discretizer->addStateModifier(targets);
		
	agent->addStateModifier(discretizer);
	hierarchicalRoot->addStateModifier(discretizer);
	
	auto qTable1= new FeatureQFunction(hierarchicalRoot->getActions(), discretizer);
	auto learner1 = new QLearner(semiMDPRewardFunction.get(), qTable1);
	learner1->setParameter("QLearningRate", 0.1);
	hierarchicalRoot->addSemiMDPListener(learner1);

	auto policy = new QStochasticPolicy(hierarchicalRoot->getActions(), new SoftMaxDistribution(10), qTable1);
	hierarchicalRoot->setController(policy);
	
	agent->setController(hierarchicalController);

	auto agentSimulator = make_shared<AgentSimulator>(model, agent);

	int steps = 0;
	int totalSteps = 0;
	int ges_failed = 0;

	// Start Learning, Learn 50 Episodes
	for (int i = 0; i < 1000; i++) {
		// Start a new Episode, the agent gets reseted in one of the start states
		agentSimulator->startNewEpisode();
		// Learn 1 Episode with maximal 1000 steps 
		steps = agentSimulator->doControllerEpisode(1000);

		totalSteps += steps;

		// Check if the Episode failed
		// The episode has failed if max_bounces has been reached (indicated
		// through environmentModel->isFailed()), 
		// or max_steps has been reached

		if (model->isFailed() || steps >= 1000) {
			ges_failed++;
		}	

		if (i % 50 == 0 && i > 0) {
			std::cout << "Episode " << i - 50 << " - " << i << "finished on average with " << totalSteps / 50 << " steps, (" << ges_failed << " failed)" << std::endl;
			
			ges_failed = 0;
			totalSteps = 0;
		}

	}
}

