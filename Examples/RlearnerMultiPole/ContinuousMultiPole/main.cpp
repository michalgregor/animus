#include <ctime>
#include <Rlearner/ril_debug.h>
#include <Rlearner/benchmarks/multipolemodel.h>
#include <Rlearner/tdlearner.h>
#include <Rlearner/policies.h>
#include <Rlearner/agent.h>
#include <Rlearner/linearfafeaturecalculator.h>
#include <Rlearner/qfunction.h>
#include <Rlearner/qetraces.h>
#include <Rlearner/AgentSimulator.h>

using namespace rlearner;

#define one_degree 0.0174532	/* 2pi/360 */
#define six_degrees 0.1047192
#define twelve_degrees 0.2094384
#define fifty_degrees 0.87266

int main() {
	// initialize the random generator
	srand((unsigned int)time((time_t *)nullptr));

	std::cout << "-=<   Reinforcement Learning Benchmark - Pole Balancing with Q-Function Learning >=-" << std::endl << std::endl;

	// variable declaration
	AgentController* policy = nullptr;
	RewardFunction* rewardFunction = nullptr;

	unsigned int dimensions[] = {0, 1, 2, 3};
	unsigned int partitions[] = {5, 5, 10, 10};
	RealType offsets[] = {0.0, 0.0, 0.0, 0.0};
	RealType sigma[] = {0.1, 0.5, 0.05, 0.05};

	FeatureCalculator* rbfCalc = new RBFFeatureCalculator(4, dimensions, partitions, offsets, sigma);
	FeatureCalculator* tilingCalc = new TilingFeatureCalculator(4, dimensions, partitions, offsets);

	// create the model
	auto model = make_shared<MultiPoleModel>();
	// initialize the reward function
	/* Our environment model also implements the reward function interface */
	rewardFunction = model.get();

	// create the agent
	auto agent = make_shared<Agent>(model->getStateProperties());

	// create the 2 actions for accelerating the cart and add them to the agent's action set
	PrimitiveAction *primAction1 = new MultiPoleAction(10.0);
	PrimitiveAction *primAction2 = new MultiPoleAction(-10.0);

	agent->addAction(primAction1);
	agent->addAction(primAction2);

	// add the discrete state to the agent's state modifier
	// discState must not be modified (e.g. with a State-Substitution) by now
	agent->addStateModifier(rbfCalc);
	agent->addStateModifier(tilingCalc);

	// Create the learner and the Q-Function
	FeatureQFunction *qTable = new FeatureQFunction(agent->getActions(), rbfCalc);
	TDLearner *learner = new QLearner(rewardFunction, qTable);
	// initialise the learning algorithm parameters
	learner->setParameter("QLearningRate", 0.3);
	learner->setParameter("DiscountFactor", 0.95);
	learner->getETraces()->setReplacingETraces(false);
	learner->getETraces()->setLambda(0.95);
	learner->setParameter("ETraceMaxListSize", 400);
	learner->setParameter("ETraceThreshold", 0.0001);

	// add the Q-Learner to the listener list
	agent->addSemiMDPListener(learner);

	// Create the learners controller from the Q-Function, we use a SoftMaxPolicy
	policy = new QStochasticPolicy(agent->getActions(), new SoftMaxDistribution(10000.0), qTable);

	// set the policy as controller of the agent
	agent->setController(policy);

	// disable automatic logging of the current episode from the agent
	agent->setLogEpisode(false);

	// create the agent simulator
	auto agentSimulator = make_shared<AgentSimulator>(model, agent);

	int steps = 0;

	int max_Steps = 100000;
	// Learn for 500 Episodes
	for (int i = 0; i < 500; i++)
	{
		// Do one training trial, with max max_Steps steps
		steps = agentSimulator->doControllerEpisode(max_Steps);

		printf("Episode %d %s with %d steps\n", i, model->isFailed() ? "failed" : "succeded", steps);

		if (steps >= max_Steps)
		{
			printf("Learned to balance the pole after %d Episodes\n", i);
			break;
		}
	}

	delete policy;
	delete learner;
	delete qTable;

	printf("\n\n<< Press Enter >>\n");
	getchar();
}

