#-------------------------------------------------------------------------------
#							DiscreteMultiPole
#-------------------------------------------------------------------------------

FILE(GLOB SRCS
	DiscreteMultiPole/*.cpp
)

ADD_EXECUTABLE(DiscreteMultiPole EXCLUDE_FROM_ALL ${SRCS})
TARGET_LINK_LIBRARIES(DiscreteMultiPole Rlearner Systematic ${LINK_LIBRARIES})

#-------------------------------------------------------------------------------
#							ContinuousMultiPole
#-------------------------------------------------------------------------------

FILE(GLOB SRCS
	ContinuousMultiPole/*.cpp
)

ADD_EXECUTABLE(ContinuousMultiPole EXCLUDE_FROM_ALL ${SRCS})
TARGET_LINK_LIBRARIES(ContinuousMultiPole Rlearner Systematic ${LINK_LIBRARIES})

# A target that builds all examples.
add_custom_target(RlearnerMultiPole DEPENDS
	DiscreteMultiPole ContinuousMultiPole
)
