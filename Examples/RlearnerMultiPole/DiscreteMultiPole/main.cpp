#include <Rlearner/ril_debug.h>
#include <Rlearner/benchmarks/multipolemodel.h>
#include <Rlearner/tdlearner.h>
#include <Rlearner/policies.h>
#include <Rlearner/agent.h>
#include <Rlearner/actorcritic.h>
#include <Rlearner/qfunction.h>
#include <Rlearner/qetraces.h>
#include <Rlearner/AgentSimulator.h>
#include <Rlearner/agentlogger.h>

using namespace rlearner;

int main() {
	// initialize the random generator
	//srand((unsigned int)time((time_t *)nullptr));
	srand(1000);

	std::cout << "-=<   Reinforcement Learning Benchmark - Pole Balancing with Q-Function Learning >=-" << std::endl << std::endl;

	AgentController* policy = nullptr;
	AbstractStateDiscretizer* discState = nullptr;

	// create the model
	auto model = make_shared<MultiPoleModel>();
	// initialize the reward function
	/* Our environment model also implements the reward function interface */
	RewardFunction* rewardFunction = model.get();

	// create the agent
	auto agent = make_shared<Agent>(model->getStateProperties());

	// create the discretizer with the build in classes
	// create the partition arrays
	RealType partitions1[] = { -0.8, 0.8}; // partition for x
	RealType partitions2[] = {-0.5, 0.5}; // partition for x_dot
	RealType partitions3[] = {-MultiPoleModel::six_degrees, -MultiPoleModel::one_degree, 0, MultiPoleModel::one_degree, MultiPoleModel::six_degrees}; // partition for theta
	RealType partitions4[] = {-MultiPoleModel::fifty_degrees, MultiPoleModel::fifty_degrees}; // partition for theta_dot

	// Create the discretizer for the state variables
	AbstractStateDiscretizer* disc1 = new SingleStateDiscretizer(0, {-0.8, 0.8});
	AbstractStateDiscretizer* disc2 = new SingleStateDiscretizer(1, {-0.5, 0.5});

	AbstractStateDiscretizer* disc3 = new SingleStateDiscretizer(
		2,
		{	-MultiPoleModel::six_degrees,
			-MultiPoleModel::one_degree,
			0,
			MultiPoleModel::one_degree,
			MultiPoleModel::six_degrees
		}
	);

	AbstractStateDiscretizer* disc4 = new SingleStateDiscretizer(3, {-MultiPoleModel::fifty_degrees, MultiPoleModel::fifty_degrees});

	// Merge the 4 discretizer
	DiscreteStateOperatorAnd* andCalculator = new DiscreteStateOperatorAnd();

	andCalculator->addStateModifier(disc1);
	andCalculator->addStateModifier(disc2);
	andCalculator->addStateModifier(disc3);
	andCalculator->addStateModifier(disc4);

	discState = andCalculator;

	// add the discrete state to the agent's state modifier
	// discState must not be modified (e.g. with a State-Substitution) by now
	agent->addStateModifier(discState);

	// create the 2 actions for accelerating the cart and add them to the agent's action set
	PrimitiveAction *primAction1 = new MultiPoleAction(10.0);
	PrimitiveAction *primAction2 = new MultiPoleAction(-10.0);

	agent->addAction(primAction1);
	agent->addAction(primAction2);

	// Create the learner and the Q-Function
	FeatureQFunction* qTable = new FeatureQFunction(agent->getActions(), discState);

	TDLearner *learner = new QLearner(rewardFunction, qTable);
	// initialise the learning algorithm parameters
	learner->setParameter("QLearningRate", 0.1);
	learner->setParameter("DiscountFactor", 0.99);
	learner->setParameter("ReplacingETraces", 0.0);
	learner->setParameter("Lambda", 1.0);

	// Set the minimum value of a etrace, we need very small values
	learner->setParameter("ETraceThreshold", 0.00001);
	// Set the maximum size of the etrace list, standard is 100
	learner->setParameter("ETraceMaxListSize", 163);

	// add the Q-Learner to the listener list
	agent->addSemiMDPListener(learner);

	// Create the learners controller from the Q-Function, we use a SoftMaxPolicy
	policy = new QStochasticPolicy(agent->getActions(), new EpsilonGreedyDistribution(0.1), qTable);

	// set the policy as controller of the agent
	agent->setController(policy);

	// disable automatic logging of the current episode from the agent
	agent->setLogEpisode(true);

	// create the agent simulator
	auto agentSimulator = make_shared<AgentSimulator>(model, agent);

	int steps = 0;

	AgentLogger logger(model->getStateProperties(), agent->getActions());
	agent->addSemiMDPListener(&logger);
	logger.addStateModifier(discState);

	int max_Steps = 100000;
	// Learn for 500 Episodes
	for (int i = 0; i < 500; i++)
	{
		// set adaptive Epsilon
		policy->setParameter("EpsilonGreedy", 0.1 / (i + 1));
		// Do one training trial, with max max_Steps steps
		steps = agentSimulator->doControllerEpisode(max_Steps);

		printf("Episode %d %s with %d steps\n", i, model->isFailed() ? "failed" : "succeded", steps);

		if (steps >= max_Steps)
		{
			printf("Learned to balance the pole after %d Episodes\n", i);
			break;
		}
	}

	logger.saveDataToFile("agent.log");

	delete policy;
	delete learner;
//	delete agent;
	delete qTable;
//	delete model;
}

