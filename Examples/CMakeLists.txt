#-------------------------------------------------------------------------------
#							GAEvenNParity
#-------------------------------------------------------------------------------

FILE(GLOB SRCS
	GAEvenNParity/*.cpp
)

ADD_EXECUTABLE(GAEvenNParity EXCLUDE_FROM_ALL ${SRCS})
TARGET_LINK_LIBRARIES(GAEvenNParity AlgoTree Genetor Systematic ${LINK_LIBRARIES})

#-------------------------------------------------------------------------------
#							GAMaximization
#-------------------------------------------------------------------------------

FILE(GLOB SRCS
	GAMaximization/*.cpp
)

ADD_EXECUTABLE(GAMaximization EXCLUDE_FROM_ALL ${SRCS})
TARGET_LINK_LIBRARIES(GAMaximization Genetor Systematic ${LINK_LIBRARIES})

#-------------------------------------------------------------------------------
#							GPRegression
#-------------------------------------------------------------------------------

FILE(GLOB SRCS
	GPRegression/*.cpp
)

ADD_EXECUTABLE(GPRegression EXCLUDE_FROM_ALL ${SRCS})
TARGET_LINK_LIBRARIES(GPRegression AlgoTree Genetor Systematic ${LINK_LIBRARIES})

#-------------------------------------------------------------------------------
#							Add subdirectories
#-------------------------------------------------------------------------------

ADD_SUBDIRECTORY(RlearnerContinuous)
ADD_SUBDIRECTORY(RlearnerDiscrete)
ADD_SUBDIRECTORY(RlearnerHierarchical)
ADD_SUBDIRECTORY(RlearnerMultiPole)

#-------------------------------------------------------------------------------
#							Common targets
#-------------------------------------------------------------------------------

# A target that builds all Rlearner examples.
add_custom_target(ExamplesRlearner DEPENDS
	RlearnerContinuous RlearnerDiscrete RlearnerHierarchical RlearnerMultiPole
)

# A target that builds all examples.
add_custom_target(examples DEPENDS
	GAEvenNParity GAMaximization
	GPRegression ExamplesRlearner
)
