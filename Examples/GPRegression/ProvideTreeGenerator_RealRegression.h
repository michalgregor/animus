#ifndef REALREGRESSIONTREEGENERATORPROVIDER_H_
#define REALREGRESSIONTREEGENERATORPROVIDER_H_

#include "system.h"
#include <AlgoTree/TreeGenerator/TreeGenerator.h>

//These includes have to go here so that the classes can be properly serialized:
#include <AlgoTree/FunctorRegister.h>
#include <AlgoTree/TreeGenerator/TreeGenerator_Full.h>
#include <AlgoTree/TreeGenerator/TreeGenerator_Growth.h>
#include <AlgoTree/TreeGenerator/TreeGenerator_Hybrid.h>

#include <AlgoTree/NodeFunctor/NodeFunctor_Add.h>
#include <AlgoTree/NodeFunctor/NodeFunctor_Divide.h>
#include <AlgoTree/NodeFunctor/NodeFunctor_Exp.h>
#include <AlgoTree/NodeFunctor/NodeFunctor_Log.h>
#include <AlgoTree/NodeFunctor/NodeFunctor_Multiply.h>
#include <AlgoTree/NodeFunctor/NodeFunctor_Pow.h>
#include <AlgoTree/NodeFunctor/NodeFunctor_Subtract.h>

#include <AlgoTree/TreeGenerator/SubtreeGenerator_Growth.h>
#include <AlgoTree/TreeGenerator/SubtreeGenerator_Hybrid.h>
#include <AlgoTree/TreeGenerator/SubtreeGenerator_Full.h>
#include <AlgoTree/module/ModuleGenerator_Generic.h>
#include <AlgoTree/FunctorFactory/FunctorFactory_Module.h>
#include <AlgoTree/FunctorFactory/FunctorFactory_Adf.h>
#include <AlgoTree/FunctorFactory/FunctorFactory_BlockConstant.h>
#include <AlgoTree/FunctorFactory/FunctorFactory_NumericConstant.h>

namespace parity {

shared_ptr<algotree::TreeGenerator> ProvideTreeGenerator_RealRegression();

} //namespace parity

#endif /* REALREGRESSIONTREEGENERATORPROVIDER_H_ */
