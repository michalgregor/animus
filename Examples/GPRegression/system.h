#ifndef PARITY_SYSTEM_H_
#define PARITY_SYSTEM_H_

#include <Systematic/system.h>

namespace parity {
	//this is required so that shared_ptr and intrusive_ptr
	//are resolved unambiguously
	using systematic::shared_ptr;
	using systematic::intrusive_ptr;

	using namespace systematic;
} //namespace parity

#endif /* PARITY_SYSTEM_H_ */
