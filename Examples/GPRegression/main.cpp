#include <iostream>

#include "system.h"

#include "ProvideTreeGenerator_RealRegression.h"
#include "Fitness_RealRegress.h"

#include <AlgoTree/GP/GeneticProgram.h>
#include <Genetor/Reporter/Reporter_HallOfFame.h>
#include <Genetor/PopulationEvaluator_Basic.h>

#include <Genetor/SelectionMethod/SelectionMethod_FitnessRoulette.h>
#include <Genetor/SelectionMethod/SelectionMethod_FitnessRanking.h>
#include <Genetor/SelectionMethod/SelectionMethod_Greedy.h>

#include <Genetor/Reporter/FitnessReporter_GenOut.h>
#include <Genetor/Stopper/FitnessStopper_Max.h>

#include <Genetor/FitnessScaling/FitnessScaling_PowerLaw.h>

#include <Genetor/Reporter/Adaptor_AvgTrapDetector.h>
#include <Genetor/GeneticAlgorithm/GeneticAlgorithm_NewBlood.h>
#include <Genetor/GeneticAlgorithm/GeneticAlgorithm_Stable.h>

#include <AlgoTree/GP/GPCrosser.h>
#include <AlgoTree/GP/GPModuleCrosser.h>
#include <AlgoTree/GP/GPMutator.h>
#include <AlgoTree/GP/GPModuleMutator.h>

#include <AlgoTree/GP/GPCrosser_BlockConstant.h>
#include <AlgoTree/GP/GPMutator_BlockConstant.h>
#include <Genetor/Mutator/Mutator_Gene_Gaussian.h>

#include <cmath>

using namespace parity;
using namespace genetor;
using namespace algotree;

int main() {
	unsigned int num_epochs = 1;
	unsigned int epoch_num_generations[num_epochs];
	FitnessType epoch_best_fitness[num_epochs];

	unsigned int pop_size = 1000, max_generations = 10;
	unsigned int report_step = 1;
	FitnessType max_score = 10;

	unsigned int maxTreeDepth = 10;
	unsigned int maxModuleTreeDepth = 6;

	ProbabilityType pCrossoverRate = 1.0f;
	ProbabilityType pModuleCrossoverRate = 1.0f;
	ProbabilityType pMutationRate = 0.05f;
	ProbabilityType pPerModuleMutationRate = 0.005f;

	for(unsigned int i = 0; i < num_epochs; i++) {

		std::cout << "epoch " << i << std::endl;

		shared_ptr<algotree::TreeGenerator> generator = ProvideTreeGenerator_RealRegression();

		//GeneticProgram is identical to Tree, so TreeGenerator can be used as
		//an initializer for GeneticProgram
		shared_ptr<algotree::TreeGenerator> initializer = generator;

		shared_ptr<Reporter_HallOfFame<GeneticProgram> > history(new Reporter_HallOfFame<GeneticProgram>(10));

		shared_ptr<Fitness_RealRegress> fitness(new Fitness_RealRegress());
	//	shared_ptr<FitnessScaling_PowerLaw<> > scaling(new FitnessScaling_PowerLaw<>(3));

//		shared_ptr<SelectionMethod_FitnessRoulette<GeneticProgram> > method(new SelectionMethod_FitnessRoulette<GeneticProgram>(fitness));
//		shared_ptr<SelectionMethod_FitnessRanking<GeneticProgram> > method(new SelectionMethod_FitnessRanking<GeneticProgram>(fitness));
		shared_ptr<SelectionMethod_Greedy<GeneticProgram> > method(new SelectionMethod_Greedy<GeneticProgram>(fitness, 0.08));

		method->generationReporter.add(history);
		method->rawFitnessReporter.add(shared_ptr<FitnessReporter_GenOut>(new FitnessReporter_GenOut(report_step)));
		method->rawFitnessStopper.add(shared_ptr<FitnessStopper_Max>(new FitnessStopper_Max(max_score)));

	//	shared_ptr<Adaptor_AvgTrapDetector<GeneticProgram> > trapDetector(new Adaptor_AvgTrapDetector<GeneticProgram>(7, 0.01f));
	//	GeneticAlgorithm_NewBlood<GeneticProgram, PairMater<GeneticProgram, true> > ga(initializer, 50, 50, method, trapDetector, 20, 3, 0.7f);

		GeneticAlgorithm_Stable<GeneticProgram> ga(initializer, pop_size, method, 4);

		//add genetic operators:
		ga.addReproductionOperator(shared_ptr<GPCrosser>(new GPCrosser(maxTreeDepth, generator, pCrossoverRate)));
		ga.addReproductionOperator(shared_ptr<GPModuleCrosser>(new GPModuleCrosser(maxModuleTreeDepth, pModuleCrossoverRate)));
		ga.addReproductionOperator(shared_ptr<GPMutator>(new GPMutator(generator, pMutationRate)));
		ga.addReproductionOperator(shared_ptr<GPModuleMutator>(new GPModuleMutator(pPerModuleMutationRate)));

		ga.addReproductionOperator(shared_ptr<GPCrosser_BlockConstant<float> >(new GPCrosser_BlockConstant<float>(3, 0.7f)));
		shared_ptr<Mutator_Gene_Gaussian<float> > geneMutator(new Mutator_Gene_Gaussian<float>(3));
//		ga.addReproductionOperator(shared_ptr<GPMutator_BlockConstant<float> >(new GPMutator_BlockConstant<float>(geneMutator, 0.02f)));

		unsigned int numGenerations = ga.run(max_generations);

		std::multimap<FitnessType, GeneticProgram>::iterator iter;
		std::multimap<FitnessType, GeneticProgram>& best(history->getTheBest());

		std::cout << "The best:" << std::endl;

		for(iter = best.begin(); iter != best.end(); iter++) {
			std::cout << "\tstored fitness: " << iter->first << std::endl;
			std::cout << &(iter->second) << std::endl;
		}

		std::cout << std::endl;

		GeneticProgram& program = best.rbegin()->second;
		std::cout << "stored score: " << best.rbegin()->first << std::endl;
//		std::cout << "score: " << (*parityFitness)(program) << std::endl;
		std::cout << program.getSExpression() << std::endl;

		epoch_num_generations[i] = numGenerations;
		epoch_best_fitness[i] = best.rbegin()->first;
	}

	//how many times the solution has been reached in this generation
	std::vector<unsigned int> reached_times(max_generations, 0);

	std::cout << "Report:" << std::endl;

	for(unsigned int i = 0; i < num_epochs; i++) {
		std::cout << "Epoch " << i << " terminated in " <<  epoch_num_generations[i] <<
		" generations. The best individual scored fitness of " << epoch_best_fitness[i] <<
		"." << std::endl;

		if(epoch_best_fitness[i] >= max_score)
				(reached_times.at(epoch_num_generations[i]))++;
	}

	std::cout << std::endl << "Reporting number of correct solutions in a generation." << std::endl;
	for(unsigned int i = 0; i < max_generations; i++) {
		std::cout << i << "\t" << reached_times[i] << std::endl;
	}

	return 0;
}
