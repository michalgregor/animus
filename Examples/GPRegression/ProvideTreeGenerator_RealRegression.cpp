#include "ProvideTreeGenerator_RealRegression.h"

using namespace algotree;

namespace parity {

shared_ptr<algotree::TreeGenerator> ProvideTreeGenerator_RealRegression() {
	shared_ptr<SubtreeGenerator_Growth> generator1(new SubtreeGenerator_Growth(0.3));
	shared_ptr<SubtreeGenerator_Full> generator2(new SubtreeGenerator_Full);

	std::vector<ProbabilityType> probabilities;
	std::vector<shared_ptr<SubtreeGenerator> > generators;

	probabilities.reserve(2); generators.reserve(2);
	probabilities.push_back(0.5f); probabilities.push_back(0.5f);
	generators.push_back(generator1); generators.push_back(generator2);

	FunctorRegister functorRegister;

	functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NodeFunctor_Add<float>(2)));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NodeFunctor_Divide<float>(2)));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NodeFunctor_Exp<float>()));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NodeFunctor_Log<float>()));
	functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NodeFunctor_Multiply<float>(2)));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NodeFunctor_Pow<float>()));
	functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NodeFunctor_Subtract<float>(2)));

//----------------------------------Modules-------------------------------------

//	//module with 2 float args returning float
//
//	FunctorSignature module1Sig(type_id<float>(), TypeList(2, type_id<float>()));
//	shared_ptr<ModuleGenerator_Generic> _moduleGenerator1(
//		new ModuleGenerator_Generic(
//			functorRegister,
//			shared_ptr<SubtreeGenerator>(new SubtreeGenerator_Hybrid(generators, probabilities)),
//			AlgoType(float(1.0f)),
//			module1Sig,
//			4,
//			5
//		)
//	);
//
//	//module with 3 float args returning float
//
//	FunctorSignature module2Sig(type_id<float>(), TypeList(3, type_id<float>()));
//	shared_ptr<ModuleGenerator_Generic> _moduleGenerator2(
//		new ModuleGenerator_Generic(
//			functorRegister,
//			shared_ptr<SubtreeGenerator>(new SubtreeGenerator_Hybrid(generators, probabilities)),
//			AlgoType(float(1.0f)),
//			module2Sig,
//			4,
//			5
//		)
//	);
//
//	//add module factories now
//
//	unsigned int maxModules = 2;
//
//	functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_Module(module1Sig, _moduleGenerator1, maxModules)));
//	functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_Module(module2Sig, _moduleGenerator2, maxModules)));

//--------------------------------End Modules-----------------------------------
//------------------------------------ADFs--------------------------------------

//		//ADF with 2 float args returning float
//
//		FunctorSignature adf1Sig(type_id<float>(), TypeList(2, type_id<float>()));
//		shared_ptr<ModuleGenerator_Generic> _adfGenerator1(
//			new ModuleGenerator_Generic(
//				functorRegister,
//				shared_ptr<SubtreeGenerator>(new SubtreeGenerator_Hybrid(generators, probabilities)),
//				AlgoType(float(1.0f)),
//				adf1Sig,
//				4,
//				0
//			)
//		);
//
//		//ADF with 3 bool args returning bool
//
//		FunctorSignature adf2Sig(type_id<float>(), TypeList(3, type_id<float>()));
//		shared_ptr<ModuleGenerator_Generic> _adfGenerator2(
//			new ModuleGenerator_Generic(
//				functorRegister,
//				shared_ptr<SubtreeGenerator>(new SubtreeGenerator_Hybrid(generators, probabilities)),
//				AlgoType(float(1.0f)),
//				adf2Sig,
//				4,
//				0
//			)
//		);
//
//		//add module factories now
//
//		unsigned int maxADFs = 2;
//
//		functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_Adf(adf1Sig, _adfGenerator1, maxADFs)));
//		functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_Adf(adf2Sig, _adfGenerator2, maxADFs)));
//----------------------------------End ADFs------------------------------------

	functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_BlockConstant<float>(5, shared_ptr<Generator<float> >(new BoundaryGenerator<float>(0, 5)))));
//	functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_NumericConstant<float>(0, 5)));

	TypeList argList(2, systematic::type_id<float>());

	shared_ptr<TreeGenerator_Hybrid> generator(new TreeGenerator_Hybrid(functorRegister, generators, probabilities, systematic::type_id<float>(), argList, 4));

	return generator;
}

} /* namespace parity */
