#ifndef REALREGRESSFITNESS_H_
#define REALREGRESSFITNESS_H_

#include "system.h"
#include <Genetor/IndividualEvaluator.h>
#include <AlgoTree/GP/GeneticProgram.h>

#include <boost/thread/mutex.hpp>

namespace parity {

using genetor::IndividualEvaluator;
using genetor::GeneticProgram;

/**
* @class Fitness_RealRegress
* @author Michal Gregor
* Fitness function used to evaluate solutions of an Even-N-Parity problem with
* a simple non-list representation of arguments.
**/

class Fitness_RealRegress: public IndividualEvaluator<GeneticProgram> {
private:
	static const std::vector<std::vector<float> >& getInputs();
	static const std::vector<float>& getOutputs();

	static float v1(float a1, float a2);
	static float v2(float a1, float a2);
	static float v3(float a1, float a2, float a3);
	static float v4(float a1, float a2, float a3);

private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	virtual genetor::FitnessType operator()(const GeneticProgram& program) const;

	Fitness_RealRegress& operator=(const Fitness_RealRegress& obj);
	Fitness_RealRegress(const Fitness_RealRegress& obj);

	Fitness_RealRegress();
	virtual ~Fitness_RealRegress() {}
};

}//namespace parity

SYS_EXPORT_CLASS(parity::Fitness_RealRegress)

#endif /* REALREGRESSFITNESS_H_ */
