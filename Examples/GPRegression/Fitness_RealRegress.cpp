#include "Fitness_RealRegress.h"
#include <AlgoTree/Context/ProcessContext.h>
#include <cmath>

#include <boost/thread/locks.hpp>

namespace parity {

/**
* Boost serialization function.
**/

void Fitness_RealRegress::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<IndividualEvaluator<GeneticProgram> >(*this);
}

void Fitness_RealRegress::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<IndividualEvaluator<GeneticProgram> >(*this);
}

genetor::FitnessType Fitness_RealRegress::v1(float a1, float a2) {
	return a1 + a2;
}

genetor::FitnessType Fitness_RealRegress::v2(float a1, float a2) {
	return a1 - a2;
}

genetor::FitnessType Fitness_RealRegress::v3(float a1, float a2, float a3) {
	return a1 + a2 + a3;
}

genetor::FitnessType Fitness_RealRegress::v4(float a1, float a2, float a3) {
	return a1 - a2 - 3.5;
}

const std::vector<std::vector<float> >& Fitness_RealRegress::getInputs() {
	static shared_ptr<std::vector<std::vector<float> > > input_ptr;

	if(!input_ptr) {
		input_ptr = shared_ptr<std::vector<std::vector<float> > >(new std::vector<std::vector<float> >());
		std::vector<std::vector<float> >& inputs(*input_ptr);

		float lbound = 0.0f, ubound = 15.0f, step = 5.0f;
		unsigned int num_vars = 2;
		unsigned int num((ubound - lbound)/step);
		unsigned int total_num(pow(num, num_vars));
		inputs.reserve(total_num);



		std::vector<float> reg; reg.reserve(num_vars);
		for(unsigned int i = 0; i < num_vars; i++) {
			reg.push_back(lbound);
		}

		for(unsigned int i = 0; i < total_num; i++) {
			for(unsigned int j = 0; j < num_vars; j++) {
				reg[j] += step;
				if(reg[j] > ubound) reg[j] = 0;
				else break;
			}

			inputs.push_back(reg);
		}
	}

	return *input_ptr;
}

const std::vector<float>& Fitness_RealRegress::getOutputs() {
	static shared_ptr<std::vector<float> > output_ptr;

	if(!output_ptr) {
		output_ptr = shared_ptr<std::vector<float> >(new std::vector<float>());
		const std::vector<std::vector<float> >& inputs(getInputs());
		std::vector<float>& outputs(*output_ptr);

		unsigned int num(inputs.size());
		outputs.reserve(num);

		for(unsigned int i = 0; i < num; i++) {
			float a = inputs.at(i).at(0);
			float b = inputs.at(i).at(1);
//			float c = inputs.at(i).at(2);
//			float d = inputs.at(i).at(3);
//			outputs.push_back((v1(a, v3(b, a+b, b-a)) + v2(a, a))*v3(v1(b, a), a, v2(a + b, b)));
//			outputs.push_back((v1(a,v3(b, a+b, b-a)) + v2(a,a))*v3(a, b, v1(a + b, b)));
			outputs.push_back(a*3.15 + b*0.5 + 3.15 + a*0.5);
		}
	}

	return *output_ptr;
}

/**
* Evaluates fitness of the provided genetic program.
*/

float Fitness_RealRegress::operator()(const GeneticProgram& program) const {
	algotree::ProcessContext context(500, 2);

	const std::vector<std::vector<float> >& testingInputs(getInputs());
	const std::vector<float>& testingOutputs(getOutputs());

	float fitness(0);

	for(unsigned int i = 0; i < testingInputs.size(); i++) {
		context.arguments().clear();
		context.memory().clear();

		unsigned int sz(testingInputs[i].size());

		for(unsigned int j = 0; j < sz; j++) {
			context.arguments().addArgument(testingInputs[i][j]);
		}

		try {
			algotree::AlgoType poly = program.process(context);
			float data = poly.getData<float>();
			float error = std::abs(data - testingOutputs.at(i));
			//the greater the error, the lower the fitness. max. fitness is 10
			if(error < 15 && error >= 0)fitness += std::exp(-3*error);
		} catch(algotree::AlgoBreak& err) {
			std::cerr << "Limit reached: " << err.what() << std::endl;
		}

	}

	fitness = 10*fitness/testingInputs.size();

	return fitness;
}

/**
* Assignment operator.
**/

Fitness_RealRegress& Fitness_RealRegress::operator=(const Fitness_RealRegress& obj) {
	if(&obj != this) {
		IndividualEvaluator<GeneticProgram>::operator=(obj);
	}

	return *this;
}

/**
* Copy constructor.
**/

Fitness_RealRegress::Fitness_RealRegress(const Fitness_RealRegress& obj):
	IndividualEvaluator<GeneticProgram>(obj) {}

/**
* Constructor.
**/

Fitness_RealRegress::Fitness_RealRegress() {
	getInputs();
	getOutputs();
}

} /* namespace parity */
