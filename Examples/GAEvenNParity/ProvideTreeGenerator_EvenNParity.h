#ifndef EVENNPARITYTREEGENERATORPROVIDER_H_
#define EVENNPARITYTREEGENERATORPROVIDER_H_

#include "system.h"
#include <AlgoTree/TreeGenerator/TreeGenerator.h>

//These includes have to go here so that the classes can be properly serialized:

#include <AlgoTree/FunctorRegister.h>
#include <AlgoTree/NodeFunctor/LogicFunctors.h>
#include <AlgoTree/NodeFunctor/FormalParameter.h>

#include <AlgoTree/TreeGenerator/TreeGenerator_Full.h>
#include <AlgoTree/TreeGenerator/TreeGenerator_Growth.h>
#include <AlgoTree/TreeGenerator/TreeGenerator_Hybrid.h>

#include <AlgoTree/TreeGenerator/SubtreeGenerator_Growth.h>
#include <AlgoTree/TreeGenerator/SubtreeGenerator_Hybrid.h>
#include <AlgoTree/TreeGenerator/SubtreeGenerator_Full.h>

#include <AlgoTree/module/ModuleGenerator_Generic.h>
#include <AlgoTree/FunctorFactory/FunctorFactory_Module.h>
#include <AlgoTree/FunctorFactory/FunctorFactory_Adf.h>

namespace parity {

shared_ptr<algotree::TreeGenerator> ProvideTreeGenerator_EvenNParity(unsigned int N);

} //namespace parity

#endif /* EVENNPARITYTREEGENERATORPROVIDER_H_ */
