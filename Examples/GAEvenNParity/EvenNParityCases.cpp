#include "EvenNParityCases.h"

namespace parity {

/**
* Assignment operator.
**/

EvenNParityCases& EvenNParityCases::operator=(const EvenNParityCases& obj) {
	if(this != &obj) {
		_N = obj._N;
		_inputs = obj._inputs;
		_outputs = obj._outputs;
	}

	return *this;
}

/**
* Copy constructor.
**/

EvenNParityCases::EvenNParityCases(const EvenNParityCases& obj): _N(obj._N),
_inputs(obj._inputs), _outputs(obj._outputs) {}

/**
* Constructor.
**/

EvenNParityCases::EvenNParityCases(unsigned int N):
	_N(N),
	_inputs(new std::vector<std::vector<bool> >()),
	_outputs(new std::vector<bool>())
{
	if(N == 0) return;

	std::vector<std::vector<bool> >& inputs(*_inputs);
	std::vector<bool>& outputs(*_outputs);

	unsigned int num(pow(2, N)); inputs.resize(num);

	for(unsigned int i = 0; i < num; i++) {
		inputs[i].reserve(N);
		for(int j = N - 1; j >= 0; j--) {
			inputs[i].push_back(i & (1 << j));
		}
	}

	outputs.reserve(num);

	for(unsigned int i = 0; i < num; i++) {
		bool val = 0;

		for(unsigned int j = 0; j < N; j++) {
			val ^= inputs[i][j];
		}

		outputs.push_back(!val);
	}
}

}//namespace parity
