#include "Fitness_EvenNP.h"
#include <AlgoTree/Context/ProcessContext.h>

namespace parity {

/**
* Boost serialization function.
**/

void Fitness_EvenNP::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<IndividualEvaluator<GeneticProgram> >(*this);
	ar & _cases;
}

void Fitness_EvenNP::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<IndividualEvaluator<GeneticProgram> >(*this);
	ar & _cases;
}

/**
* Evaluates fitness of the provided genetic program.
*/

genetor::FitnessType Fitness_EvenNP::operator()(const GeneticProgram& program) const {
	algotree::ProcessContext context(500, 2);

	const std::vector<std::vector<bool> >& testingInputs(_cases.getInputs());
	const std::vector<bool>& testingOutputs(_cases.getOutputs());

	genetor::FitnessType fitness(0);

	for(unsigned int i = 0; i < testingInputs.size(); i++) {
		context.arguments().clear();
		context.memory().clear();

		unsigned int sz(testingInputs[i].size());

		for(unsigned int j = 0; j < sz; j++) {
			context.arguments().addArgument(testingInputs[i][j]);
		}

		try {
			if(program.process(context).getData<bool>() == testingOutputs[i]) fitness++;
		} catch(algotree::AlgoBreak& err) {
			std::cerr << "Limit reached: " << err.what() << std::endl;
		}
	}

	return fitness;
}

/**
* Assignment operator.
**/

Fitness_EvenNP& Fitness_EvenNP::operator=(const Fitness_EvenNP& obj) {
	if(&obj != this) {
		IndividualEvaluator<GeneticProgram>::operator=(obj);
		_cases = obj._cases;
	}

	return *this;
}

/**
* Copy constructor.
**/

Fitness_EvenNP::Fitness_EvenNP(const Fitness_EvenNP& obj):
	IndividualEvaluator<GeneticProgram>(obj), _cases(obj._cases) {}

/**
* Constructor.
* @param N Dimension N of the Even-N-Parity problem.
**/

Fitness_EvenNP::Fitness_EvenNP(unsigned int N): _cases(N) {}

}//namespace parity
