#ifndef EVENNPFITNESS_H_
#define EVENNPFITNESS_H_

#include "system.h"
#include <Genetor/IndividualEvaluator.h>
#include <AlgoTree/GP/GeneticProgram.h>
#include "EvenNParityCases.h"

namespace parity {

using genetor::IndividualEvaluator;
using genetor::GeneticProgram;

/**
* @class Fitness_EvenNP
* @author Michal Gregor
* Fitness function used to evaluate solutions of the Even-N-Parity problem with
* a simple non-list representation of arguments.
**/

class Fitness_EvenNP: public IndividualEvaluator<GeneticProgram> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

	Fitness_EvenNP(): _cases(0) {}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Even-N-Parity fitness cases.
	EvenNParityCases _cases;

public:
	virtual genetor::FitnessType operator()(const GeneticProgram& program) const;

	Fitness_EvenNP& operator=(const Fitness_EvenNP& obj);
	Fitness_EvenNP(const Fitness_EvenNP& obj);

	Fitness_EvenNP(unsigned int N);
	virtual ~Fitness_EvenNP() {}
};

}//namespace parity

SYS_EXPORT_CLASS(parity::Fitness_EvenNP)

#endif /* EVENNPFITNESS_H_ */
