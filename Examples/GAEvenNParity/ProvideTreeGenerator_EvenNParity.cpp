#include "ProvideTreeGenerator_EvenNParity.h"

using namespace algotree;

namespace parity {

shared_ptr<algotree::TreeGenerator> ProvideTreeGenerator_EvenNParity(unsigned int N) {
	shared_ptr<SubtreeGenerator_Growth> generator1(new SubtreeGenerator_Growth(0.3));
	shared_ptr<SubtreeGenerator_Full> generator2(new SubtreeGenerator_Full);

	std::vector<ProbabilityType> probabilities;
	std::vector<shared_ptr<SubtreeGenerator> > generators;

	probabilities.reserve(2); generators.reserve(2);
	probabilities.push_back(0.5f); probabilities.push_back(0.5f);
	generators.push_back(generator1); generators.push_back(generator2);

	FunctorRegister functorRegister;

	//propositional logic operators
	functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new AndFunctor));
	functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new OrFunctor));
	functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NandFunctor));
	functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NorFunctor));

//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new AndFunctor));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new OrFunctor));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NandFunctor));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NorFunctor));
//
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new AndFunctor));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new OrFunctor));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NandFunctor));
//		functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new NorFunctor));

//----------------------------------Modules-------------------------------------

		//module with 2 bool args returning bool

		FunctorSignature module1Sig(type_id<bool>(), TypeList(2, type_id<bool>()));
		shared_ptr<ModuleGenerator_Generic> _moduleGenerator1(
			new ModuleGenerator_Generic(
				functorRegister,
				shared_ptr<SubtreeGenerator>(new SubtreeGenerator_Hybrid(generators, probabilities)),
				AlgoType(bool(false)),
				module1Sig,
				4,
				5
			)
		);

		//module with 3 bool args returning bool

		FunctorSignature module2Sig(type_id<bool>(), TypeList(3, type_id<bool>()));
		shared_ptr<ModuleGenerator_Generic> _moduleGenerator2(
			new ModuleGenerator_Generic(
				functorRegister,
				shared_ptr<SubtreeGenerator>(new SubtreeGenerator_Hybrid(generators, probabilities)),
				AlgoType(bool(false)),
				module2Sig,
				4,
				5
			)
		);

		//add module factories now

		unsigned int maxModules = 1;

		functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_Module(module1Sig, _moduleGenerator1, maxModules, 0.4f, true)));
		functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_Module(module2Sig, _moduleGenerator2, maxModules, 0.4f, true)));

//--------------------------------End Modules-----------------------------------
//------------------------------------ADFs--------------------------------------

//		//ADF with 2 bool args returning bool
//
//		FunctorSignature adf1Sig(type_id<bool>(), TypeList(2, type_id<bool>()));
//		shared_ptr<ModuleGenerator_Generic> _adfGenerator1(
//			new ModuleGenerator_Generic(
//				functorRegister,
//				shared_ptr<SubtreeGenerator>(new SubtreeGenerator_Hybrid(generators, probabilities)),
//				AlgoType(bool(false)),
//				adf1Sig,
//				4,
//				0
//			)
//		);
//
//		//ADF with 3 bool args returning bool
//
//		FunctorSignature adf2Sig(type_id<bool>(), TypeList(3, type_id<bool>()));
//		shared_ptr<ModuleGenerator_Generic> _adfGenerator2(
//			new ModuleGenerator_Generic(
//				functorRegister,
//				shared_ptr<SubtreeGenerator>(new SubtreeGenerator_Hybrid(generators, probabilities)),
//				AlgoType(bool(false)),
//				adf2Sig,
//				4,
//				0
//			)
//		);
//
//		//add module factories now
//
//		unsigned int maxADFs = 1;
//
//		functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_Adf(adf1Sig, _adfGenerator1, maxADFs)));
//		functorRegister.addFactory(intrusive_ptr<FunctorFactory>(new FunctorFactory_Adf(adf2Sig, _adfGenerator2, maxADFs)));
//----------------------------------End ADFs------------------------------------

	TypeList argList(N, type_id<bool>());
	shared_ptr<TreeGenerator_Hybrid> generator(new TreeGenerator_Hybrid(functorRegister, generators, probabilities, type_id<bool>(), argList, 4));

	return generator;
}

} /* namespace parity */
