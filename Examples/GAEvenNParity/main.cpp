#include <iostream>
#include <fstream>

#include "system.h"

#include "ProvideTreeGenerator_EvenNParity.h"
#include "Fitness_EvenNP.h"

#include <AlgoTree/GP/GeneticProgram.h>
#include <Genetor/Reporter/Reporter_HallOfFame.h>
#include <Genetor/PopulationEvaluator_Basic.h>

#include <Genetor/SelectionMethod/SelectionMethod_FitnessRoulette.h>
#include <Genetor/SelectionMethod/SelectionMethod_FitnessRanking.h>
#include <Genetor/SelectionMethod/SelectionMethod_Greedy.h>

#include <Genetor/Reporter/FitnessReporter_GenOut.h>
#include <Genetor/Stopper/FitnessStopper_Max.h>

#include <Genetor/FitnessScaling/FitnessScaling_PowerLaw.h>

#include <Genetor/Reporter/Adaptor_AvgTrapDetector.h>
#include <Genetor/GeneticAlgorithm/GeneticAlgorithm_NewBlood.h>
#include <Genetor/GeneticAlgorithm/GeneticAlgorithm_Stable.h>

#include <AlgoTree/GP/GPCrosser.h>
#include <AlgoTree/GP/GPModuleCrosser.h>
#include <AlgoTree/GP/GPMutator.h>
#include <AlgoTree/GP/GPModuleMutator.h>

#include <cmath>

using namespace parity;
using namespace genetor;
using namespace algotree;

int main() {
	unsigned int num_epochs = 60;
	unsigned int epoch_num_generations[num_epochs];
	FitnessType epoch_best_fitness[num_epochs];

	unsigned int max_generations = 51;

	for(unsigned int i = 0; i < num_epochs; i++) {

		std::cout << "epoch " << i << std::endl;

		unsigned int N = 4;
		unsigned int pop_size = 1000;
		unsigned int report_step = 1;
		FitnessType max_score = pow(2, N);

		unsigned int maxTreeDepth = 10;
		unsigned int maxModuleTreeDepth = 6;

		ProbabilityType pCrossoverRate = 1.0f;
		ProbabilityType pModuleCrossoverRate = 1.0f;
		ProbabilityType pMutationRate = 0.05f;
		ProbabilityType pPerModuleMutationRate = 0.005f;

		shared_ptr<algotree::TreeGenerator> generator = ProvideTreeGenerator_EvenNParity(N);

		//GeneticProgram is identical to Tree, so TreeGenerator can be used as
		//an initializer for GeneticProgram
		shared_ptr<algotree::TreeGenerator> initializer = generator;

		shared_ptr<Reporter_HallOfFame<GeneticProgram> > history(new Reporter_HallOfFame<GeneticProgram>(10));

		shared_ptr<Fitness_EvenNP> fitness(new Fitness_EvenNP(N));
	//	shared_ptr<FitnessScaling_PowerLaw<> > scaling(new FitnessScaling_PowerLaw<>(3));

//		shared_ptr<SelectionMethod_FitnessRoulette<GeneticProgram> > method(new SelectionMethod_FitnessRoulette<GeneticProgram>(fitness));
//		shared_ptr<SelectionMethod_FitnessRanking<GeneticProgram> > method(new SelectionMethod_FitnessRanking<GeneticProgram>(fitness));
		shared_ptr<SelectionMethod_Greedy<GeneticProgram> > method(new SelectionMethod_Greedy<GeneticProgram>(fitness, 0.08));

		method->generationReporter.add(history);
		method->rawFitnessReporter.add(shared_ptr<FitnessReporter_GenOut>(new FitnessReporter_GenOut(report_step)));
		method->rawFitnessStopper.add(shared_ptr<FitnessStopper_Max>(new FitnessStopper_Max(max_score)));

	//	shared_ptr<Adaptor_AvgTrapDetector<GeneticProgram> > trapDetector(new Adaptor_AvgTrapDetector<GeneticProgram>(7, 0.01f));
	//	GeneticAlgorithm_NewBlood<GeneticProgram, PairMater<GeneticProgram, true> > ga(initializer, 50, 50, method, trapDetector, 20, 3, 0.7f);

		GeneticAlgorithm_Stable<GeneticProgram> ga(initializer, pop_size, method, 4);

		//add genetic operators:
		ga.addReproductionOperator(shared_ptr<GPCrosser>(new GPCrosser(maxTreeDepth, generator, pCrossoverRate)));
		ga.addReproductionOperator(shared_ptr<GPModuleCrosser>(new GPModuleCrosser(maxModuleTreeDepth, pModuleCrossoverRate)));
		ga.addReproductionOperator(shared_ptr<GPMutator>(new GPMutator(generator, pMutationRate)));
		ga.addReproductionOperator(shared_ptr<GPModuleMutator>(new GPModuleMutator(pPerModuleMutationRate)));

		unsigned int numGeneration = ga.run(max_generations);

		std::multimap<FitnessType, GeneticProgram>::iterator iter;
		std::multimap<FitnessType, GeneticProgram>& best(history->getTheBest());

		std::cout << "The best:" << std::endl;

		for(iter = best.begin(); iter != best.end(); iter++) {
			std::cout << "\tstored fitness: " << iter->first << std::endl;
			std::cout << &(iter->second) << std::endl;
		}

		std::cout << std::endl;

//		GeneticProgram& program = best.rbegin()->second;
//		std::cout << "stored score: " << best.rbegin()->first << std::endl;
//		std::cout << "score: " << (*fitness)(program) << std::endl;
//		std::cout << program.getTree()->getSExpression() << std::endl;

		epoch_num_generations[i] = numGeneration;
		epoch_best_fitness[i] = best.rbegin()->first;
	}

	//how many times the solution has been reached in this generation
	std::vector<unsigned int> reached_times(max_generations, 0);
	FitnessType maxFitness = 16;

	std::cout << "Report:" << std::endl;

	for(unsigned int i = 0; i < num_epochs; i++) {
		std::cout << "Epoch " << i << " terminated in " <<  epoch_num_generations[i] <<
		" generations. The best individual scored fitness of " << epoch_best_fitness[i] <<
		"." << std::endl;

		if(epoch_best_fitness[i] >= maxFitness)
		(reached_times.at(epoch_num_generations[i]))++;
	}

	std::cout << std::endl << "Reporting number of correct solutions in a generation." << std::endl;
	for(unsigned int i = 0; i < max_generations; i++) {
		std::cout << i << "\t" << reached_times[i] << std::endl;
	}

	return 0;
}
