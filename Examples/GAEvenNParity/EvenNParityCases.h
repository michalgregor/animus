#ifndef EVENNPARITYCASES_H_
#define EVENNPARITYCASES_H_

#include "system.h"
#include <cmath>

namespace parity {

/**
* @class EvenNParityCases
* @author Michal Gregor
* Provides the fitness cases for the Even-N-Parity problem.
**/

class EvenNParityCases {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & _inputs;
		ar & _outputs;
	}

	EvenNParityCases(): _N(0), _inputs(), _outputs() {}

private:
	//! Dimension N of the Even-N-Parity problem.
	unsigned int _N;
	//! Stores the domain of the Even-N-Parity problem.
	shared_ptr<std::vector<std::vector<bool> > > _inputs;
	//! Stores values that the elements from inputs should map to.
	shared_ptr<std::vector<bool> > _outputs;

public:
	const std::vector<std::vector<bool> >& getInputs() const {return *_inputs;}
	const std::vector<bool>& getOutputs() const {return *_outputs;}

	EvenNParityCases& operator=(const EvenNParityCases& obj);
	EvenNParityCases(const EvenNParityCases& obj);
	EvenNParityCases(unsigned int N);
};

}//namespace parity

#endif /* EVENNPARITYCASES_H_ */
