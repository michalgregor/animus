#ifndef PAIRCODEFITNESS_H_
#define PAIRCODEFITNESS_H_

#include <Genetor/IndividualEvaluator.h>
#include "PairCode.h"

namespace paircode {

/**
 * @class PairCodeFitness
 * An implementation of a fitness function for PairCode individuals and function
 * (1.0f - pow(sin(sqrt(z)), 2))/(1.0f + 0.001f*z), where z = x*x + y*y and
 * x, y are two real numbers specified in the chromosome of PairCode.
 */

class PairCodeFitness: public genetor::IndividualEvaluator<PairCode> {
private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	virtual genetor::FitnessType operator() (const PairCode& in) const;
	virtual ~PairCodeFitness() {}
};

} //namespace paircode

#endif /* PAIRCODEFITNESS_H_ */
