#include "PairCodeInitializer.h"
#include <Systematic/generator/BoundaryGenerator.h>

namespace paircode {

PairCodeInitializer::PairCodeInitializer():
systematic::VectorGenerator<float>(
	2, shared_ptr<BoundaryGenerator<float> >(new BoundaryGenerator<float>(-100, 100))
) {}

} //namespace paircode
