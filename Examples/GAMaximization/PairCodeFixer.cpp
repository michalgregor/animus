#include "PairCodeFixer.h"

namespace paircode {

void PairCodeFixer::operator()(PairCode& individual) const {
	if(individual.size() < 2) throw systematic::TracedError_InvalidArgument("The individuals has less than 2 genes.");
	if(individual.size() > 2) individual.resize(2);

	individual.at(0) = float(int(individual.at(0)*1e5f))*1e-5f;
	individual.at(1) = float(int(individual.at(1)*1e5f))*1e-5f;
}

} //namespace paircode
