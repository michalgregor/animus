#ifndef PAIRCODE_H_INCLUDED
#define PAIRCODE_H_INCLUDED

#include <Genetor/IndividualFixer.h>
#include "PairCode.h"

namespace paircode {

class PairCodeFixer: public genetor::IndividualFixer<PairCode> {
private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	virtual void operator()(PairCode& individual) const;

	virtual ~PairCodeFixer() {}
};

} //namespace paircode

#endif // PAIRCODE_H_INCLUDED
