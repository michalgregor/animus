#ifndef PAIRCODEINITIALIZER_H_
#define PAIRCODEINITIALIZER_H_

#include <Systematic/generator/VectorGenerator.h>
#include "PairCode.h"

namespace paircode {

/**
* @class PairCodeInitializer
* @author michalgregor
**/

class PairCodeInitializer: public systematic::VectorGenerator<float> {
private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	PairCodeInitializer();
	virtual ~PairCodeInitializer() {}
};

} //namespace paircode

#endif /* PAIRCODEINITIALIZER_H_ */
