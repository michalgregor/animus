#ifndef PAIRCODE_H_
#define PAIRCODE_H_

#include <vector>
#include <Systematic/system.h>

namespace paircode {
	typedef std::vector<float> PairCode;

	using namespace systematic;
} //namespace paircode

#endif /* PAIRCODE_H_ */
