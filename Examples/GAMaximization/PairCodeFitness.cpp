#include "PairCodeFitness.h"
#include <cmath>

namespace paircode {

genetor::FitnessType PairCodeFitness::operator() (const PairCode& in) const {
	float x = in.at(0), y = in.at(1);

	float z = x*x + y*y;
	return (1.0f - std::pow(std::sin(std::sqrt(z)), 2))/(1.0f + 0.001f*z);
}

} //namespace paircode
