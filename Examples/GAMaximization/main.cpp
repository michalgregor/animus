/*
 * This source file contains an implementation of a function maximization task
 * using the Genetor Genetic Algorithm framework.
 *
 * In order to apply Genetic Algorithms to any given task, several issues must
 * be addressed:
 *
 * (a) Selecting an appropriate representation of the solution (the individual).
 *
 * In our example, the solution is represented by a GeneticString containing 2
 * real numbers. Therefore, our representation, defined in class PairCode is
 * based on class template genetor::Individual_GeneString, which represents
 * a generic genetic string of arbitrary length. Type of Genes (float in this
 * case) is itself a template parameter.
 *
 * (b) Selecting appropriate genetic operators - crossover and mutation.
 *
 * The default way of applying these two basic operators in Genetor is to call
 * cross() and mutate() member functions of the class representing
 * the Individual. Thus, crossover and mutation are considered a property of
 * the Individual by default, which allows for more complex
 *
 */

#include <iostream>

#include "PairCode.h"
#include "PairCodeFitness.h"
#include "PairCodeFixer.h"
#include "PairCodeInitializer.h"

#include <Genetor/Crosser/Crosser_Gene_Arithmetic.h>
#include <Genetor/Crosser/Crosser_Gene_BinaryNPoint.h>
#include <Genetor/Crosser/Crosser_GeneString_BinaryNPoint.h>
#include <Genetor/Crosser/Crosser_GeneString_NPoint.h>
#include <Genetor/Crosser/Crosser_GeneString_NPointArithmetic.h>
#include <Genetor/Crosser/Crosser_GeneString_SingularArithmetic.h>
#include <Genetor/Crosser/Crosser_GeneString_WholeArithmetic.h>

#include <Genetor/Mutator/Mutator_Gene_Gaussian.h>
#include <Genetor/Mutator/Mutator_Gene_Random.h>
#include <Genetor/Mutator/Mutator_GeneString_Gaussian.h>
#include <Genetor/Mutator/Mutator_GeneString_OneGene.h>
#include <Genetor/Mutator/Mutator_GeneString_Random.h>

#include <Genetor/GeneticAlgorithm/GeneticAlgorithm_Stable.h>
#include <Genetor/GeneticAlgorithm/GeneticAlgorithm_Dynamic.h>
#include <Genetor/GeneticAlgorithm/GeneticAlgorithm_Flood.h>
#include <Genetor/GeneticAlgorithm/GeneticAlgorithm_NewBlood.h>

#include <Genetor/Reporter/Adaptor_AvgTrapDetector.h>
#include <Genetor/Reporter/Adaptor_TrapDetector.h>
#include <Genetor/Reporter/FitnessReporter_GenOut.h>

#include <Genetor/Stopper/FitnessStopper.h>
#include <Genetor/Stopper/FitnessStopper_Delta.h>
#include <Genetor/Stopper/FitnessStopper_Max.h>

#include <Genetor/TrendLine/TrendLine_BoundedExp.h>

#include <Genetor/SelectionMethod/SelectionMethod_FitnessRanking.h>
#include <Genetor/SelectionMethod/SelectionMethod_FitnessRoulette.h>
#include <Genetor/SelectionMethod/SelectionMethod_Greedy.h>
#include <Genetor/SelectionMethod/SelectionMethod_SelectAll.h>

#include <Genetor/FitnessScaling/FitnessScaling_Linear.h>
#include <Genetor/FitnessScaling/FitnessScaling_PowerLaw.h>

using namespace genetor;
using namespace paircode;

int main() {
	//PairCode initializer
	shared_ptr<PairCodeInitializer> initializer(new PairCodeInitializer);

	//fitness scaling
	shared_ptr<FitnessScaling> fitnessScaling;
//	shared_ptr<FitnessScaling_Linear> fitnessScaling(new FitnessScaling_Linear(0.8));
//	shared_ptr<FitnessScaling_PowerLaw> fitnessScaling(new FitnessScaling_PowerLaw(3));

	//fitness evaluator
	shared_ptr<IndividualEvaluator<PairCode> > fitnessFunction(new PairCodeFitness);

	//selection methods
	shared_ptr<SelectionMethod_FitnessRanking<PairCode> > selectionMethod(new SelectionMethod_FitnessRanking<PairCode>(fitnessFunction, fitnessScaling));

	//reporters
	selectionMethod->scaledFitnessReporter.add(shared_ptr<FitnessReporter_GenOut>(new FitnessReporter_GenOut(1)));

	//parameters
	unsigned int populationSize = 200;
	unsigned int maxGenerations = 100;
	unsigned int elitism = 1;

	shared_ptr<GeneticAlgorithm_Stable<PairCode> > ga(
		new GeneticAlgorithm_Stable<PairCode>(
			initializer,
			populationSize,
			selectionMethod,
			elitism
		)
	);

//	shared_ptr<GeneticAlgorithm_Dynamic<PairCode> > ga(
//		new GeneticAlgorithm_Dynamic<PairCode>(
//			initializer,
//			shared_ptr<TrendLine_BoundedExp>(new TrendLine_BoundedExp(400, 50, 75)),
//			selectionMethod,
//			elitism
//		)
//	);

//	unsigned int numToSurvive = 20;
//	float powerOfScaling = 0.001f;
//	shared_ptr<Adaptor_AvgTrapDetector<PairCode> > trapDetector(new Adaptor_AvgTrapDetector<PairCode>);
//
//	shared_ptr<GeneticAlgorithm_Flood<PairCode> > ga(
//		new GeneticAlgorithm_Flood<PairCode>(
//			initializer,
//			populationSize,
//			selectionMethod,
//			trapDetector,
//			numToSurvive,
//			powerOfScaling,
//			elitism
//		)
//	);

//	unsigned int numToSurvive = 20;
//	shared_ptr<Adaptor_AvgTrapDetector<PairCode> > trapDetector(new Adaptor_AvgTrapDetector<PairCode>(10));
//
//	shared_ptr<GeneticAlgorithm_NewBlood<PairCode> > ga(
//		new GeneticAlgorithm_NewBlood<PairCode>(
//			initializer,
//			populationSize,
//			selectionMethod,
//			trapDetector,
//			numToSurvive,
//			elitism
//		)
//	);

	//crossers
//	shared_ptr<Crosser_Gene_Arithmetic<float> > crosser(new Crosser_Gene_Arithmetic<float>(0.3));
//	shared_ptr<Crosser_Gene_BinaryNPoint<float> > crosser(new Crosser_Gene_BinaryNPoint<float>());
//	shared_ptr<Crosser_GeneString_BinaryNPoint<float> > crosser(new Crosser_GeneString_BinaryNPoint<float>());
//	shared_ptr<Crosser_GeneString_NPoint<float> > crosser(new Crosser_GeneString_NPoint<float>());
	shared_ptr<Crosser_GeneString_NPointArithmetic<std::vector<float> > > crosser(new Crosser_GeneString_NPointArithmetic<std::vector<float> >());
//	shared_ptr<Crosser_GeneString_SingularArithmetic<float> > crosser(new Crosser_GeneString_SingularArithmetic<float>());
//	shared_ptr<Crosser_GeneString_WholeArithmetic<float> > crosser(new Crosser_GeneString_WholeArithmetic<float>());

	//mutators
//	shared_ptr<Mutator_Gene_Gaussian<float> > mutator(new Mutator_Gene_Gaussian<float>());
//	shared_ptr<Mutator_Gene_Random<float> > mutator(new Mutator_Gene_Random<float>(shared_ptr<Generator<float> >(new systematic::BoundaryGenerator<float>(0, 15))));
	shared_ptr<Mutator_GeneString_Gaussian<float> > mutator(new Mutator_GeneString_Gaussian<float>());
//	shared_ptr<Mutator_GeneString_OneGene<float> > mutator(new Mutator_GeneString_OneGene<float>(shared_ptr<Generator<float> >(new systematic::BoundaryGenerator<float>(0, 15))));
//	shared_ptr<Mutator_GeneString_Random<float> > mutator(new Mutator_GeneString_Random<float>(shared_ptr<Generator<float> >(new systematic::BoundaryGenerator<float>(0, 15))));

	ga->addReproductionOperator(crosser);
	ga->addReproductionOperator(mutator);
	ga->addReproductionOperator(shared_ptr<PairCodeFixer>(new PairCodeFixer));

	ga->run(maxGenerations);
}
