#-------------------------------------------------------------------------------
#						cartpoleQRBFLearning
#-------------------------------------------------------------------------------

FILE(GLOB SRCS
	cartpoleQRBFLearning/*.cpp
)

ADD_EXECUTABLE(cartpoleQRBFLearning EXCLUDE_FROM_ALL ${SRCS})
TARGET_LINK_LIBRARIES(cartpoleQRBFLearning Rlearner Systematic ${LINK_LIBRARIES})

#-------------------------------------------------------------------------------
#						cartpoleVRBFLearning
#-------------------------------------------------------------------------------

FILE(GLOB SRCS
	cartpoleVRBFLearning/*.cpp
)

ADD_EXECUTABLE(cartpoleVRBFLearning EXCLUDE_FROM_ALL ${SRCS})
TARGET_LINK_LIBRARIES(cartpoleVRBFLearning Rlearner Systematic ${LINK_LIBRARIES})

#-------------------------------------------------------------------------------
#						pendulumQRBFLearning
#-------------------------------------------------------------------------------

FILE(GLOB SRCS
	pendulumQRBFLearning/*.cpp
)

ADD_EXECUTABLE(pendulumQRBFLearning EXCLUDE_FROM_ALL ${SRCS})
TARGET_LINK_LIBRARIES(pendulumQRBFLearning Rlearner Systematic ${LINK_LIBRARIES})

#-------------------------------------------------------------------------------
#						pendulumVRBFLearning
#-------------------------------------------------------------------------------

FILE(GLOB SRCS
	pendulumVRBFLearning/*.cpp
)

ADD_EXECUTABLE(pendulumVRBFLearning EXCLUDE_FROM_ALL ${SRCS})
TARGET_LINK_LIBRARIES(pendulumVRBFLearning Rlearner Systematic ${LINK_LIBRARIES})

#-------------------------------------------------------------------------------
#							Common target
#-------------------------------------------------------------------------------

# A target that builds all the examples.
add_custom_target(RlearnerContinuous DEPENDS
	cartpoleQRBFLearning cartpoleVRBFLearning
	pendulumQRBFLearning pendulumVRBFLearning
)
