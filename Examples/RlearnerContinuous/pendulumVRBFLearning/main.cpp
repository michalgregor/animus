// Copyright (C) 2003
// Gerhard Neumann (gneumann@gmx.net)
// Stephan Neumann (sneumann@gmx.net) 
//                
// This file is part of RL Toolbox.
// http://www.igi.tugraz.at/ril_toolbox
//
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// USE OF THE EXAMPLE
// pendulumVRBFLearning [-d debugfile]
// 
// This example shows how to learn to swing up a pendulum with V-Function Learning using RBFs

#include <iostream>

#include <fstream>
#include <Rlearner/ril_debug.h>
#include <Rlearner/tdlearner.h>
#include <Rlearner/policies.h>
#include <Rlearner/agent.h>
#include <Rlearner/analyzer.h>
#include <Rlearner/AgentSimulator.h>
#include <Rlearner/benchmarks/pendulummodel.h>
#include <Rlearner/linearfafeaturecalculator.h>
#include <Rlearner/vfunctionlearner.h>
#include <Rlearner/rewardmodel.h>
#include <Rlearner/torchvfunction.h>
#include <Rlearner/montecarlo.h>
#include <Rlearner/continuousactions.h>

using namespace rlearner;

// This is the entry point for this application
int main(int argc, char **argv) {
	std::cout << "-=<   Reinforcement Learning Benchmark - Pendulum Swing Up with V-Function Learning using RBFs >=-\n\n";
	int arg = 1;

	// Console Input Processing
	while (arg < argc - 1) {
		if (strcmp(argv[arg], "-d") == 0) {
			// The "-d" option enables debugging
			arg++;
			char *debugFile = argv[arg];
			DebugInit(debugFile, "+", false);
		}
		arg++;
	}

	// Create our Pendulum Model with the timestep 0,05. All other physical values are taken by default (see Docu or source).
	auto pendulumModel = new PendulumModel(0.05);

	// Use 100 simulation steps of the model for one time step. This helps to overcome the numerical inaccurateness.
	pendulumModel->setSimulationSteps(15);
	// Take random reset states, for the pendulum model, the only the angle is choosen randomly, the velocity is always 0 at the beginning
	pendulumModel->setResetType(DM_RESET_TYPE_RANDOM);

	// Create the environment for the agent, the environment saves the current state of the agent.
	auto environmentModel = make_shared<TransitionFunctionEnvironment>(pendulumModel);
	// Create our reward function for the pendulum (the reward depends on the height of the pendulum)
	auto rewardFunction = new PendulumRewardFunction(pendulumModel);
	// Create the agent in our environmentModel.
	auto agent = make_shared<Agent>(environmentModel->getStateProperties());

	// Now we can already create our RBF network
	// Therefore we will use a CRBFFeatureCalculator, our feature calculator uses both dimensions of the model state 
	// (the angel and the angular velocity) and lays a 20x20 RBF grid over the state space. For each dimension the given sigmas are used.
	// For the calculation of useful sigmas we have to consider that the CRBFFeatureCalculator always uses the 
	// normalized state representation, so the state variables are scaled to the intervall [0,1]
	unsigned int dimensions[] = {0, 1};
	unsigned int partitions[] = {20, 20};
	RealType offsets[] = {0.0, 0.0};
	RealType sigma[] = {0.025, 0.025};

	// Now we can create our Feature Calculator
	auto rbfCalc = new RBFFeatureCalculator(2, dimensions, partitions, offsets, sigma);
	// Of course we have to add the feature calculator to the agent state modifiers list.
	agent->addStateModifier(rbfCalc);

	// Now we can create our possible actions, we give the agent 3 actions to choose : -uMax, 0, uMax
	// Since we deal with continuous actions we have to define the action values ourself.
	RealType action1[] = {-pendulumModel->uMax};
	RealType action2[] = {+pendulumModel->uMax};
	RealType action3[] = {0};

	auto minContAction = new StaticContinuousAction(pendulumModel->getContinuousAction(), action1);
	auto maxContAction = new StaticContinuousAction(pendulumModel->getContinuousAction(), action2);
	auto nullContAction = new StaticContinuousAction(pendulumModel->getContinuousAction(), action3);

	// Add all actoins to the agent
	agent->addAction(minContAction);
	agent->addAction(maxContAction);
	agent->addAction(nullContAction);

	// Disable Episode logging
	agent->setLogEpisode(false);

	// Create our feature V-Function using our RBF net as feature calculator.
	auto vFunction = new FeatureVFunction(rbfCalc);

	// Create the V-Function Learner, we use a standard TD-Learner here.
	// The V-Function Learner needs the reward function and the V-Function objects.
	auto learner = new VFunctionLearner(rewardFunction, vFunction);
	// Add the learner to the agent listeners list
	agent->addSemiMDPListener(learner);

	// Create the Controller for the agent from the VFunction and the pendulum model as state predictor. 
	// Additionally to the V-Function the policy needs the pendulum model to calculate the next states for all actions 
	// and the reward function to calculate the rewards for that states. It also needs the state modifiers list from the agent to get 
	// the value from the V-Function.
	// We will use a SoftMax-Policy for exploration.
	auto vFunctionPolicy = new VMStochasticPolicy(agent->getActions(), new SoftMaxDistribution(10.0), vFunction, pendulumModel, rewardFunction, std::set<StateModifierList*>({agent->getModifierList()}));
	// Set the policy
	agent->setController(vFunctionPolicy);

	// Set the learningrate parameter
	learner->setParameter("VLearningRate", 0.25);

	// To trace the learning process we will use a policy evaluator. Here we will use average reward policy evaluator, 
	// which calculates the average reward of 10 episodes, each taking 10 seconds. In order not to falsify the learning process, we have to
	// disable learning before each policy evaluation.
	auto agentSimulator = make_shared<AgentSimulator>(environmentModel, agent);
	auto policyEvaluator = new AverageRewardCalculator(agentSimulator.get(), rewardFunction, 10, my_round(10.0 / pendulumModel->getTimeIntervall()));
	
	std::cout << "Learner: ";
	learner->saveParameters(std::cout);
	std::cout << "Policy: ";
	vFunctionPolicy->saveParameters(std::cout);
	
	// Start Learning now, learn 100 Episodes	
	for (int i = 0; i < 100; i++) {
		// Start new Episode, the agent's angle will be choosen randomly and the velocity is set to 0.
		agentSimulator->startNewEpisode();
		// Learn 1 Episode with 200 steps 
		agentSimulator->doControllerEpisode(200);

		std::cout << "Episode " << i << " (Steps " << agentSimulator->getTotalSteps() << "): Average Reward (Height of the Pendulum): " << policyEvaluator->evaluatePolicy() << "\n";
	}

	// Save the learned data
	std::ofstream results("VFunctionPendulum.table");
	vFunction->saveData(results);

	// We are also interested in the shape of the value function, therefore we analyze the value function with a VFunctionAnalyzer
	std::ofstream shape("VFunctionPendulumShape.table");

	// Create the VFunctionAnalyzer which needs the vFunction itself, the properties of the model state and the agent's state modifier list.
	auto analyzer = new VFunctionAnalyzer(vFunction, pendulumModel->getStateProperties(), std::set<StateModifierList*>({agent->getModifierList()}));

	// Lay a 50x50 grid over the state space and calculate the Values of the V-Function on the grid locations.
	// The initState is needed, but only useful if the model state has more than 2 dimensions.
	auto initstate = new State(pendulumModel->getStateProperties());
	analyzer->save2DValues(shape, initstate, 0, 50, 1, 50);

	std::cout << "Finished Learning\n";
	
	// Cleaning up
//	delete pendulumModel;
//	delete vFunction;
//	delete learner;
//	delete vFunctionPolicy;
//	delete initstate;
//	delete analyzer;

}
