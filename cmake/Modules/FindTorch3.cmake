# - Try to find Torch3
# This module reads hints about search locations from variables:
# Torch3_DIR Hint about the location of the entire project.
# Torch3_INCLUDEDIR Hint about the location of the includes.
# Torch3_LIBRARYDIR Hint about the location of the libraries.
#
# Once done this will define
#  Torch3_FOUND - System has Torch3
#  Torch3_INCLUDE_DIR - The Torch3 include directories
#  Torch3_LIBRARIES - The libraries needed to use Torch3
#  Torch3_DEFINITIONS - Compiler switches required for using Torch3

macro(_FIND_Torch3_LIBRARY _var)
	find_library(${_var}
		NAMES
			${ARGN}
		HINTS 
			${PC_Torch3_LIBDIR}
			${PC_Torch3_LIBRARY_DIRS}
	)
	mark_as_advanced(${_var})
endmacro()

find_package(PkgConfig)
pkg_check_modules(PC_Torch3 QUIET Torch3)
set(Torch3_DEFINITIONS ${PC_Torch3_CFLAGS_OTHER})

find_path(Torch3_INCLUDE_DIR general.h
          HINTS ${PC_Torch3_INCLUDEDIR} ${PC_Torch3_INCLUDE_DIRS}
          PATH_SUFFIXES torch )

_FIND_Torch3_LIBRARY(Torch3_Library	torch)

set(Torch3_LIBRARIES
		${Torch3_Library}
)

set(Torch3_INCLUDE_DIRS ${Torch3_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set Torch3_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Torch3  DEFAULT_MSG
	Torch3_Library
	Torch3_INCLUDE_DIR)

mark_as_advanced(Torch3_INCLUDE_DIR Torch3_LIBRARY )
