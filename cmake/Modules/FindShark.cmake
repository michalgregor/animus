# - Try to find Shark
# Once done this will define
#  Shark_FOUND - System has Shark
#  Shark_INCLUDE_DIRS - The Shark include directories
#  Shark_LIBRARIES - The libraries needed to use Shark
#  Shark_DEFINITIONS - Compiler switches required for using Shark

macro(_FIND_SHARK_LIBRARY _var)
	find_library(${_var}
		NAMES
			${ARGN}
		HINTS 
			${PC_Shark_LIBDIR}
			${PC_Shark_LIBRARY_DIRS}
	)
	mark_as_advanced(${_var})
endmacro()

find_package(PkgConfig)
pkg_check_modules(PC_Shark QUIET Shark)
set(Shark_DEFINITIONS ${PC_Shark_CFLAGS_OTHER})

find_path(Shark_INCLUDE_DIR shark/SharkDefs.h
          HINTS ${PC_Shark_INCLUDEDIR} ${PC_Shark_INCLUDE_DIRS}
          PATH_SUFFIXES Shark )

_FIND_SHARK_LIBRARY(Shark_Library shark)

if(Shark_Library)
	set(Shark_LIBRARIES
		${Shark_LIBRARIES}
		${Shark_Library}
	)
endif(Shark_Library)

set(Shark_INCLUDE_DIRS ${Shark_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set Shark_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Shark  DEFAULT_MSG
	Shark_Library
	Shark_INCLUDE_DIR)

if(Shark_Library AND Shark_INCLUDE_DIR)
	SET(Shark_FOUND 1)
endif()
