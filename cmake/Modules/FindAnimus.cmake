# - Try to find Animus
# This module reads hints about search locations from variables:
# Animus_DIR Hint about the location of the entire project.
# Animus_INCLUDEDIR Hint about the location of the includes.
# Animus_LIBRARYDIR Hint about the location of the libraries.
#
# Once done this will define
#  Animus_FOUND - System has Animus
#  Animus_INCLUDE_DIR - The Animus include directories
#  Animus_LIBRARIES - The libraries needed to use Animus
#  Animus_DEFINITIONS - Compiler switches required for using Animus

set(Animus_DIR "" CACHE PATH "Hint about the location of the Animus project.")
set(Animus_INCLUDEDIR "" CACHE PATH "Hint about the location of Animus includes.")
set(Animus_LIBRARYDIR "" CACHE PATH "Hint about the location of Animus libraries.")

macro(_FIND_ANIMUS_LIBRARY _var)
	find_library(${_var}
		NAMES
			${ARGN}
		HINTS 
			${PC_Animus_LIBDIR}
			${PC_Animus_LIBRARY_DIRS}
			${Animus_LIBRARYDIR}
			${Animus_DIR}
			${Animus_DIR}/*
	)
	mark_as_advanced(${_var})
endmacro()

find_package(PkgConfig)
pkg_check_modules(PC_Animus QUIET Animus)
set(Animus_DEFINITIONS ${PC_Animus_CFLAGS_OTHER})

find_path(Animus_INCLUDE_DIR Systematic/system.h
          HINTS ${PC_Animus_INCLUDEDIR} ${PC_Animus_INCLUDE_DIRS} ${Animus_DIR} ${Animus_INCLUDEDIR}
          PATH_SUFFIXES Animus )

_FIND_ANIMUS_LIBRARY(Animus_Rlearner_Library	Rlearner)
_FIND_ANIMUS_LIBRARY(Animus_Motivator_Library	Motivator)
_FIND_ANIMUS_LIBRARY(Animus_Annalyst_Library	Annalyst)
_FIND_ANIMUS_LIBRARY(Animus_AlgoTree_Library	AlgoTree)
_FIND_ANIMUS_LIBRARY(Animus_Genetor_Library		Genetor)
_FIND_ANIMUS_LIBRARY(Animus_Systematic_Library	Systematic)

set(Animus_LIBRARIES
		${Animus_Rlearner_Library}
		${Animus_Motivator_Library}
		${Animus_Annalyst_Library}
		${Animus_AlgoTree_Library}
		${Animus_Genetor_Library}
		${Animus_Systematic_Library}
)

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set Animus_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Animus  DEFAULT_MSG
	Animus_Rlearner_Library
	Animus_Motivator_Library
	Animus_Annalyst_Library
	Animus_AlgoTree_Library
	Animus_Genetor_Library
	Animus_Systematic_Library
	Animus_INCLUDE_DIR)

mark_as_advanced(Animus_INCLUDEDIR Animus_LIBRARYDIR Animus_DIR)
