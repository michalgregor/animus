#####################################################################
#           Building Doxygen documentation
#####################################################################

#-- Add an Option to toggle the generation of the API documentation
option(BUILD_DOCUMENTATION "Use Doxygen to create the HTML-based documentation." OFF)
option(DOCUMENTATION_ALL_TARGET "Whether to make documentation part of the default target." OFF)

if(DOCUMENTATION_ALL_TARGET)
	SET(DOCUMENTATION_TARGET ALL)
else(DOCUMENTATION_ALL_TARGET)
	SET(DOCUMENTATION_TARGET)
endif(DOCUMENTATION_ALL_TARGET)

if(BUILD_DOCUMENTATION)
  FIND_PACKAGE(Doxygen)
  if (NOT DOXYGEN_FOUND)
    message(FATAL_ERROR "Doxygen is needed to build the documentation.")
  endif()

  SET(HTML_OUTPUT .)

  #-- Configure the Template Doxyfile for our specific project
  configure_file(docs/Animus.doxy.in 
                 ${PROJECT_BINARY_DIR}/Animus.doxy  @ONLY IMMEDIATE)
  #-- Add a custom target to run Doxygen when ever the project is built
  add_custom_target (docs ${DOCUMENTATION_TARGET}
		COMMAND ${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/Animus.doxy
		SOURCES ${PROJECT_BINARY_DIR}/Animus.doxy)
endif()

