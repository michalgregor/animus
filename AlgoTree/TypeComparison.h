#ifndef ALGO_TYPECOMPARISON_H_INCLUDED
#define ALGO_TYPECOMPARISON_H_INCLUDED

namespace algotree {

enum TypeComparison {
	TC_Strict,
	TC_Compatible,
	TC_Convertible
};

} //namespace algotree

#endif // ALGO_TYPECOMPARISON_H_INCLUDED
