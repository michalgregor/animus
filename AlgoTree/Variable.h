#ifndef ALGO_VARIABLE_H_INCLUDED
#define ALGO_VARIABLE_H_INCLUDED

#include <Systematic/dynamic_typing/ConverterRegister.h>
#include <Systematic/utils/StorageToken.h>

#include "system.h"
#include "Context/ProcessContext.h"

namespace algotree {

//forward declarations
template<class Type>
class Var2TypeConverter;
template<class Type>
class VariableAccessor;

/**
* @class Variable
* For use with syntactic trees as a variable type. It stores the actual data in
* a similar way that AlgoType does, but keeps the type invariable, which
* is rather vital for a variable. As type of the object held inside does not
* change, no reallocations take place and thus Variable can be used as a handle
* except that it is context-specific.
*
* Non-const methods are not thread-safe.
**/

template<class Type>
class Variable {
private:
	//! A static variable from initialization of which the Variable is auto
	//! registered with the systematic::ConverterRegister.
	static const char _constructor;
	//! Storage token that serves as the identifier for the data stored in the
	//! MemoryContext.
	StorageToken _storageToken;

private:
	friend class VariableAccessor<Type>;

private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & _storageToken;
	}

public:
	VariableAccessor<Type> operator()(ProcessContext& processContext) {
		return VariableAccessor<Type>(processContext, _storageToken);
	}

	/**
	* Assignment.
	**/

	Variable<Type>& operator=(const Variable<Type>& var) {
		if(this != &var) {
			_storageToken = var._storageToken;
		}
		return *this;
	}

	/**
	* Copy constructor.
	**/

	Variable(const Variable<Type>& var): _storageToken(var._storageToken) {}

	//! Default constructor for use in serialization.
	Variable(): _storageToken() {}

	//! Constructor with explicit storage token specification. If it is NULL
	//! an TracedError_InvalidPointer is thrown.
	explicit Variable(StorageToken storageToken):
	_storageToken(storageToken)
	{
		if(!_storageToken.isValid()) throw TracedError_InvalidPointer("A NULL storage token passed.");
	}

	/**
	* Destructor.
	**/

	~Variable() {(void)_constructor;}
};

//initialization

template<class Type>
const char Variable<Type>::_constructor = (
	(AlgoConverterRegister().makeConvertible<Variable<Type>, Type>(shared_ptr<Var2TypeConverter<Type> >(new Var2TypeConverter<Type>()))),
0);

//------------------------------VariableAccessor--------------------------------

template<class Type>
class VariableAccessor {
private:
	//! Reference tp tje ProcessContext that is to be used to store the value.
	ProcessContext& _contextMemory;
	//! Storage token that serves as the identifier for the data stored in the
	//! ProcessContext.
	StorageToken _storageToken;

private:
	friend class Variable<Type>;

	//! Loads the data pointer from the context. If there are no data stored in
	//! the context so far, a new entry is created.
    Type& loadFromContext() {
		if(_contextMemory.memory().exists(_storageToken)) {
			return _contextMemory.memory().getData<Type>(_storageToken);
		} else {
			_contextMemory.memory().storeData(_storageToken, Type());
			return _contextMemory.memory().getData<Type>(_storageToken);
		}
    }

	VariableAccessor();
    VariableAccessor(const VariableAccessor<Type>&);

    /**
	* Constructor - creates an object of Type using its default constructor.
	**/

	explicit VariableAccessor(
		ProcessContext& processContext,
		StorageToken storageToken
	): _contextMemory(processContext), _storageToken(storageToken) {}

public:

	/**
	* Assignment - rewrites the stored data by those provided by the parameter.
	**/

	VariableAccessor<Type>& operator=(const Type& data) {
		loadFromContext() = data;
		return *this;
	}

	/**
	* Cast to a Type reference.
	**/

	operator Type&() {
		return loadFromContext();
	}

	/**
	* Cast to a const Type reference.
	**/

	operator const Type&() const {
		return loadFromContext();
	}

	/**
	* Creates a copy of the pointed-to data and returns a new Variable pointing
	* to it.
	**/

	Variable<Type> copy() {
		Variable<Type> var;
		var(_contextMemory) = *loadFromContext();
		return var;
	}
};

//------------------------------------------------------------------------------
//---------------------------Var2TypeConverter----------------------------------
//------------------------------------------------------------------------------

/**
* @class Var2TypeConverter
* This class is used as a TypeConverter from Variable<Type> to Type.
**/

template<class Type>
class Var2TypeConverter: public systematic::TypeConverter<ProcessContext> {
private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	/**
	* Converts object obj from certain data type to another. The returned
	* void pointer should always point to newly allocated memory. The original
	* object is not deleted.
	**/

	virtual systematic::Handler* operator()(systematic::Handler* obj, ProcessContext& context) const {
		Variable<Type>* var = static_cast<Variable<Type>* >(obj->getPtr());
		return new systematic::GenericHandler<Type>((*var)(context));
	}

	/**
	* Empty body of the virtual destructor.
	**/

	virtual ~Var2TypeConverter() {}
};

} //namespace algotree

SYS_REGISTER_TEMPLATENAME(algotree::Variable, 1)

#endif // ALGO_VARIABLE_H_INCLUDED
