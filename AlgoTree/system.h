#ifndef ALGO_SYSTEM_INCLUDED
#define ALGO_SYSTEM_INCLUDED

#include <Systematic/system.h>
#include <Genetor/system.h>
#include <Systematic/configure.h>

namespace algotree {
	//this is required so that shared_ptr and intrusive_ptr
	//are resolved unambiguously
	using systematic::shared_ptr;
	using systematic::intrusive_ptr;

	using genetor::RealType;

	using namespace systematic;
} //namespace algotree

#endif // ALGO_SYSTEM_INCLUDED
