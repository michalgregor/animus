#ifndef ALGO_ALGOTYPE_H_INCLUDED
#define ALGO_ALGOTYPE_H_INCLUDED

#include <Systematic/dynamic_typing/PolyType.h>
#include <Systematic/dynamic_typing/PolyContainer.h>
#include "system.h"

namespace algotree {

class ProcessContext;

typedef systematic::ConverterRegister<ProcessContext> AlgoConverterRegisterType;
AlgoConverterRegisterType& AlgoConverterRegister();

namespace detail {

struct Aux_ConverterProvider {
	static AlgoConverterRegisterType* provide() {
		return &AlgoConverterRegister();
	}
};

} //namespace detail

typedef systematic::PolyType<systematic::PolyContainer, false, detail::Aux_ConverterProvider, ProcessContext> AlgoType;

} //namespace algotree

#endif // ALGO_ALGOTYPE_H_INCLUDED
