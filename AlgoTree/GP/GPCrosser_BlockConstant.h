#ifndef Genetor_GPCrosser_BlockConstant_H_
#define Genetor_GPCrosser_BlockConstant_H_

#include "../system.h"
#include <Genetor/Crosser/Crosser.h>
#include <Systematic/generator/HybridSeqGenerator.h>

#include "GeneticProgram.h"
#include "../Context/ContextBlock_Constant.h"

namespace genetor {

/**
* @class GPCrosser_BlockConstant
* @author Michal Gregor
* A Crosser for block constants (see ContextBlock_Constant).
*
* @tparam Type Type of the constant.
**/

template<class Type>
class GPCrosser_BlockConstant: public Crosser<GeneticProgram> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Crosser<GeneticProgram> >(*this);
		ar & _numPoints;
		ar & _alpha;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Number of crossing points. Should be even.
	unsigned int _numPoints;
	//! The arithmetic crossover coefficient.
	RealType _alpha;

public:
	//! Returns the number of crossing points.
	unsigned int GetNumPoints() const {
		return _numPoints;
	}

	//! Sets the number of crossing points.
	void SetNumPoints(unsigned int numPoints) {
		_numPoints = numPoints;
	}

	virtual void operator()(GeneticProgram& i1, GeneticProgram& i2);

	GPCrosser_BlockConstant<Type>& operator=(const GPCrosser_BlockConstant<Type>& obj);
	GPCrosser_BlockConstant(const GPCrosser_BlockConstant<Type>& obj);

	GPCrosser_BlockConstant(
		unsigned int numPoints = 2,
		RealType alpha = 0.5f,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~GPCrosser_BlockConstant();
};

/**
* The crossing operation itself. The crossover happens in-place.
* @param i1 The first partner individual.
* @param i2 The second partner individual.
**/

template<class Type>
void GPCrosser_BlockConstant<Type>::operator()(GeneticProgram& i1, GeneticProgram& i2) {
	unsigned int i, numPoints(_numPoints);

	algotree::Context::iterator bi1 = i1.context()->find<algotree::ContextBlock_Constant<Type> >();
	algotree::Context::iterator bi2 = i2.context()->find<algotree::ContextBlock_Constant<Type> >();

	if(bi1 != i1.context()->end() && bi2 != i2.context()->end()) {
		algotree::ContextBlock_Constant<Type>& b1(bi1->block<algotree::ContextBlock_Constant<Type> >());
		algotree::ContextBlock_Constant<Type>& b2(bi2->block<algotree::ContextBlock_Constant<Type> >());

		unsigned int size = b1.size();
		if(size != b2.size()) throw TracedError_InvalidArgument("Chromosomes are not equally long.");

		//if neither chromosome is long enough to accomodate all
		if(numPoints > (size+1)/2) numPoints = (size+1)/2;
		if(numPoints == 0) return;

		HybridSeqGenerator<unsigned int> generator(numPoints, 0, size);
		std::vector<unsigned int> seq;

		//if odd, begin is selected as a crossing point to make the number even
		if(numPoints & 1) {
			numPoints++;

			seq = generator(numPoints, false);
			//sort the points ascendingly
			std::sort(seq.begin(), seq.end());

			seq[0] = 0;
		} else {
			seq = generator(numPoints, false);
			//sort the points ascendingly
			std::sort(seq.begin(), seq.end());
		}

		for(i = 0; i < numPoints; i+=2) {
			for(unsigned int j = seq[i]; j < seq[i+1]; j++) {
				typename algotree::ContextBlock_Constant<Type>::iterator iter1 = b1.begin(), iter2 = b2.begin();
				std::advance(iter1, j); std::advance(iter2, j);

				typename algotree::ContextBlock_Constant<Type>::iterator::value_type::second_type x(iter1->second);
				iter1->second = _alpha*(iter2->second) + (1 - _alpha)*x;
				iter2->second = _alpha*x + (1 - _alpha)*(iter2->second);
			}
		}
	}
}

/**
* Assignment operator.
**/

template<class Type>
GPCrosser_BlockConstant<Type>& GPCrosser_BlockConstant<Type>::operator=(const GPCrosser_BlockConstant<Type>& obj) {
	if(&obj != this) {
		Crosser<GeneticProgram>::operator=(obj);
		_numPoints = obj._numPoints;
		_alpha = obj._alpha;
	}

	return *this;
}

/**
* Copy constructor.
**/

template<class Type>
GPCrosser_BlockConstant<Type>::
GPCrosser_BlockConstant(const GPCrosser_BlockConstant<Type>& obj):
	Crosser<GeneticProgram>(obj), _numPoints(obj._numPoints),
	_alpha(obj._alpha) {}

/**
* Constructor.
**/

template<class Type>
GPCrosser_BlockConstant<Type>::GPCrosser_BlockConstant(
	unsigned int numPoints,
	RealType alpha,
	ProbabilityType crossoverRate,
	bool useOpenMP
): Crosser<GeneticProgram>(crossoverRate, useOpenMP), _numPoints(numPoints),
_alpha(alpha) {}

/**
 * Destructor.
 */

template<class Type>
GPCrosser_BlockConstant<Type>::~GPCrosser_BlockConstant() {
	SYS_EXPORT_INSTANCE();
}

}//namespace genetor

SYS_EXPORT_TEMPLATE(genetor::GPCrosser_BlockConstant, 1)

#endif //Genetor_GPCrosser_BlockConstant_H_
