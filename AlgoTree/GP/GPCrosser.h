#ifndef Genetor_GPCrosser_H_
#define Genetor_GPCrosser_H_

#include "../system.h"
#include <Genetor/Crosser/Crosser.h>
#include "../TreeGenerator/TreeGenerator.h"
#include "GeneticProgram.h"

namespace genetor {

/**
* @class GPCrosser
* @author Michal Gregor
* The standard Crosser for GeneticPrograms based on swapping randomly selected
* subtrees (in such way as to always produce syntactically correct trees
* of course). The Crosses can optionally enforce a depth limit on the trees.
* This is applied by cutting the tree after the crossover has taken place.
**/
class ALGOTREE_API GPCrosser: public Crosser<GeneticProgram> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The TreeGenerator used when cutting the tree.
	shared_ptr<algotree::TreeGenerator> _treeGenerator;
	//! The maximum depth of the tree. Cutting is applied after crossover to
	//! maintain this. -1 means no limit.
	unsigned int _maxDepth;

protected:
	void cutTree(algotree::Tree& tree);

public:
	shared_ptr<algotree::TreeGenerator> getCutterGenerator();
	shared_ptr<const algotree::TreeGenerator> getCutterGenerator() const;
	unsigned int getCutterMaxDepth() const;

	void setCutter(unsigned int maxDepth, const shared_ptr<algotree::TreeGenerator>& generator);
	void resetCutter();

	virtual void operator()(GeneticProgram& i1, GeneticProgram& i2);

	GPCrosser& operator=(const GPCrosser& obj);
	GPCrosser(const GPCrosser& obj);

	explicit GPCrosser(ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE, bool useOpenMP = true);

	GPCrosser(
		unsigned int maxDepth,
		const shared_ptr<algotree::TreeGenerator>& generator,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~GPCrosser() {}
};

}//namespace genetor

SYS_EXPORT_CLASS(genetor::GPCrosser)

#endif //Genetor_GPCrosser_H_
