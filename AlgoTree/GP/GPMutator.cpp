#include "GPMutator.h"
#include "../index/NodeSimpleIndex.h"

namespace genetor {

/**
 * This method applies in-place mutation to a single individual.
 */

void GPMutator::operator()(GeneticProgram& individual) {
	algotree::NodeSimpleIndex i(individual, true);
	algotree::Node* node = i.randNode();

	//if root node has been chosen, do not replace its functor, only
	//generate new children nodes
	if(node == individual.getRoot()) {
		node->resetChildren();
		_treeGenerator->generate(*node);
	} else {
		_treeGenerator->replace(*node);
	}
}

/**
* Boost serialization function.
**/

void GPMutator::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Mutator<GeneticProgram> >(*this);
	ar & _treeGenerator;
}

void GPMutator::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Mutator<GeneticProgram> >(*this);
	ar & _treeGenerator;
}

/**
* Assignment operator.
**/

GPMutator& GPMutator::operator=(const GPMutator& obj) {
	if(&obj != this) {
		Mutator<GeneticProgram>::operator=(obj);
		_treeGenerator = shared_ptr<algotree::TreeGenerator>(clone(_treeGenerator));
	}

	return *this;
}

/**
* Copy constructor.
**/

GPMutator::GPMutator(const GPMutator& obj):
Mutator<GeneticProgram> (obj),
_treeGenerator(clone(obj._treeGenerator)) {}

/**
 * Default constructor. For use in serialization.
 */

GPMutator::GPMutator(): _treeGenerator() {}

/**
* Constructor.
**/

GPMutator::GPMutator(
	const shared_ptr<algotree::TreeGenerator>& generator,
	ProbabilityType mutationRate,
	bool useOpenMP
): Mutator<GeneticProgram>(mutationRate, useOpenMP),
_treeGenerator(generator) {}

}//namespace genetor
