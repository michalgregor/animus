#ifndef Genetor_GPModuleMutator_H_
#define Genetor_GPModuleMutator_H_

#include "../system.h"
#include <Genetor/Mutator/Mutator.h>
#include "GeneticProgram.h"

namespace genetor {

/**
* @class GPModuleMutator
* @author Michal Gregor
* A Mutator for GeneticPrograms that mutates its Modules. This operator should
* generally be applied after GPMutator, which is an implementation
* of mutation for the main tree.
**/
class ALGOTREE_API GPModuleMutator: public Mutator<GeneticProgram> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! This is a per-Module mutation rate, that is the Mutator goes over all
	//! Modules and applies mutation to each of them with probability
	//! perModuleMutationRate.
	ProbabilityType _perModuleMutationRate;

public:
	virtual void operator()(GeneticProgram& individual);

	GPModuleMutator& operator=(const GPModuleMutator& obj);
	GPModuleMutator(const GPModuleMutator& obj);

	explicit GPModuleMutator(
			ProbabilityType perModuleMutationRate = 0.005f,
		bool useOpenMP = true
	);

	virtual ~GPModuleMutator() {}
};

}//namespace genetor

SYS_EXPORT_CLASS(genetor::GPModuleMutator)

#endif //Genetor_GPModuleMutator_H_
