#include "GPModuleMutator.h"
#include "../index/NodeSimpleIndex.h"

namespace genetor {

/**
 * This method applies in-place mutation to a single individual.
 */

void GPModuleMutator::operator()(GeneticProgram& individual) {
	algotree::ContextBlock_Modules &moduleList(individual.context()->modules());

	for(algotree::ContextBlock_Modules::iterator iter = moduleList.begin(); iter != moduleList.end(); iter++) {
		if(Mutator<GeneticProgram>::_randomGenerator() <= _perModuleMutationRate) {
			algotree::NodeSimpleIndex i(iter->second.modify(), true);
			algotree::Node* node = i.randNode();

			//if root node has been chosen, do not replace its functor, only
			//generate new children nodes
			if(node == individual.getRoot()) {
				node->resetChildren();
				iter->second.getGenerator()->generate(*node);
			} else {
				iter->second.getGenerator()->replace(*node);
			}
		}
	}
}

/**
* Boost serialization function.
**/

void GPModuleMutator::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Mutator<GeneticProgram> >(*this);
	ar & _perModuleMutationRate;
}

void GPModuleMutator::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Mutator<GeneticProgram> >(*this);
	ar & _perModuleMutationRate;
}

/**
* Assignment operator.
**/

GPModuleMutator& GPModuleMutator::operator=(const GPModuleMutator& obj) {
	if(&obj != this) {
		Mutator<GeneticProgram>::operator=(obj);
		_perModuleMutationRate = obj._perModuleMutationRate;
	}

	return *this;
}

/**
* Copy constructor.
**/

GPModuleMutator::GPModuleMutator(const GPModuleMutator& obj):
Mutator<GeneticProgram>(obj),
_perModuleMutationRate(obj._perModuleMutationRate) {}

/**
* Constructor.
* @parameter perModuleMutationRate This is a per-Module mutation rate, that is
* the Mutator goes over all Modules and applies mutation to each of them with
* probability perModuleMutationRate.
**/

GPModuleMutator::GPModuleMutator(ProbabilityType perModuleMutationRate, bool useOpenMP):
Mutator<GeneticProgram>(1.0f, useOpenMP),
_perModuleMutationRate(perModuleMutationRate) {}

}//namespace genetor
