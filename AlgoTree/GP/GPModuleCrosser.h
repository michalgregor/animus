#ifndef Genetor_GPModuleCrosser_H_
#define Genetor_GPModuleCrosser_H_

#include "../system.h"
#include <Genetor/Crosser/Crosser.h>
#include "GeneticProgram.h"

namespace genetor {

/**
* @class GPModuleCrosser
* @author Michal Gregor
* A Crosser for GeneticPrograms that crosses its Modules with another GP. This
* operator should generally be applied after GPCrosser, which is an implementation
* of crossover for the main tree.
**/
class ALGOTREE_API GPModuleCrosser: public Crosser<GeneticProgram> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The maximum depth of the tree. Cutting is applied after crossover to
	//! maintain this. -1 means no limit.
	unsigned int _maxDepth;

protected:
	void cutModule(algotree::Node& root, const shared_ptr<algotree::ModuleGenerator>& generator);

public:
	unsigned int getCutterMaxDepth() const;
	void setCutter(unsigned int maxDepth);
	void resetCutter();

	virtual void operator()(GeneticProgram& i1, GeneticProgram& i2);

	GPModuleCrosser& operator=(const GPModuleCrosser& obj);
	GPModuleCrosser(const GPModuleCrosser& obj);

	explicit GPModuleCrosser(
		unsigned int maxDepth = -1,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~GPModuleCrosser() {}
};

}//namespace genetor

SYS_EXPORT_CLASS(genetor::GPModuleCrosser)

#endif //Genetor_GPModuleCrosser_H_
