#include "GPCrosser.h"
#include "detail/gp_crossover.h"

namespace genetor {

/**
* Boost serialization function.
**/

void GPCrosser::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Crosser<GeneticProgram> >(*this);
	ar & _treeGenerator;
	ar & _maxDepth;
}

void GPCrosser::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Crosser<GeneticProgram> >(*this);
	ar & _treeGenerator;
	ar & _maxDepth;
}

/**
 * Returns the TreeGenerator that is to be used in cutting the Tree (as set
 * using setCutter(). Defaults to NULL pointer.
 */

shared_ptr<algotree::TreeGenerator> GPCrosser::getCutterGenerator() {
	return _treeGenerator;
}

/**
 * Returns the TreeGenerator that is to be used in cutting the Tree (as set
 * using setCutter(). Defaults to NULL pointer.
 */

shared_ptr<const algotree::TreeGenerator> GPCrosser::getCutterGenerator() const {
	return _treeGenerator;
}

/**
 * Returns the maximum depth that the contained tree is allowed to have.
 * Cutting is applied after crossover to maintain this. -1 means no limit.
 */

unsigned int GPCrosser::getCutterMaxDepth() const {
	return _maxDepth;
}

/**
 * Specifies the cutting policy. Cutting of the tree is applied after crossover
 * to maintain a specified maximum depth.
 *
 * @param maxDepth The maximum depth that a tree is allowed to have.
 * -1 means no limit.
 * @param generator A TreeGenerator used for cutting (to replace subtrees by
 * terminals).
 */

void GPCrosser::setCutter(unsigned int maxDepth, const shared_ptr<algotree::TreeGenerator>& generator) {
	_maxDepth = maxDepth;
	_treeGenerator = generator;
}

/**
 * Resets the cutting policy. The cutting TreeGenerator pointer is reset to NULL
 * and the maximum depth is set to -1 (no limit).
 */

void GPCrosser::resetCutter() {
	_treeGenerator.reset();
	_maxDepth = (unsigned int)-1;
}

/**
 * Cuts the Tree making sure that its maximum depth is as specified unless
 * the maximum depth is set to -1, which means no limit.
 */

void GPCrosser::cutTree(algotree::Tree& tree) {
	if(_maxDepth != (unsigned int)-1) {
		algotree::Node& root(*(tree.getRoot()));

		algotree::Traverser t, tend = root.end();
		for(t = root.begin(); t != tend; t++) {
			if(t.depth() >= _maxDepth) _treeGenerator->replace(*t, 0);
		}
	}
}

/**
 * This method applies in-place crossover to a single pair of individuals.
 */

void GPCrosser::operator()(GeneticProgram& i1, GeneticProgram& i2) {
	detail::gp_crossover(*(i1.getRoot()), *(i2.getRoot()), false);

	cutTree(i1);
	cutTree(i2);
}


/**
* Assignment operator.
**/

GPCrosser& GPCrosser::operator=(const GPCrosser& obj) {
	if(&obj != this) {
		Crosser<GeneticProgram>::operator=(obj);
		_treeGenerator = shared_ptr<algotree::TreeGenerator>(clone(obj._treeGenerator));
		_maxDepth = obj._maxDepth;
	}

	return *this;
}

/**
* Copy constructor.
**/

GPCrosser::GPCrosser(const GPCrosser& obj):
Crosser<GeneticProgram>(obj),
_treeGenerator(clone(obj._treeGenerator)),
_maxDepth(obj._maxDepth) {}

/**
* Constructor.
*
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param  Set to true if OpenMP is to be used to parallelise the application
* of the operator.
*
* Tree cutting (to ensure that tree depth does not exceed the maximum) is
* disabled by default when using this constructor. It can be enabled using
* setCutter().
**/

GPCrosser::GPCrosser(ProbabilityType crossoverRate, bool useOpenMP):
Crosser<GeneticProgram>(crossoverRate, useOpenMP), _treeGenerator(),
_maxDepth((unsigned int)-1) {}

/**
* Constructor.
*
* This version of the Constructor enables the tree cutting mechanism which
* ensures that the depth of the tree will not exceed a preset maximum after
* the crossover has been applied.
*
* @param maxDepth The maximum depth that a tree is allowed to have.
* -1 means no limit.
* @param generator A TreeGenerator used for cutting (to replace subtrees by
* terminals).
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param  Set to true if OpenMP is to be used to parallelise the application
* of the operator.
**/

GPCrosser::GPCrosser(
	unsigned int maxDepth,
	const shared_ptr<algotree::TreeGenerator>& generator,
	ProbabilityType crossoverRate,
	bool useOpenMP
): Crosser<GeneticProgram>(crossoverRate, useOpenMP), _treeGenerator(generator),
_maxDepth(maxDepth) {}

}//namespace genetor
