#ifndef Genetor_GPMutator_BlockConstant_H_
#define Genetor_GPMutator_BlockConstant_H_

#include "../system.h"

#include <Genetor/Mutator/Mutator.h>
#include <Systematic/generator/BoundaryGenerator.h>

#include "GeneticProgram.h"
#include "../Context/ContextBlock_Constant.h"

namespace genetor {

/**
* @class GPMutator_BlockConstant
* @author Michal Gregor
* A mutator for block constants (see ContextBlock_Constant).
*
* Applies the specified gene mutator to every constant with a predefined
* probability.
*
* @tparam Type Type of the constant.
**/
template<class Type>
class GPMutator_BlockConstant: public Mutator<GeneticProgram> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Mutator<GeneticProgram> >(*this);
		ar & _constMutator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Mutator of the underlying constant type, e.g. Mutator_Gene_Gaussian.
	shared_ptr<Mutator<Type> > _constMutator;
	//! A probability that any given constant will undergo mutation.
	ProbabilityType _perConstMutationRate;
	//! A generator used to decide whether to apply mutation or not.
	systematic::BoundaryGenerator<ProbabilityType> _gen;

public:
	virtual void operator()(GeneticProgram& individual);

	GPMutator_BlockConstant<Type>& operator=(const GPMutator_BlockConstant<Type>& obj);
	GPMutator_BlockConstant(const GPMutator_BlockConstant<Type>& obj);

	GPMutator_BlockConstant(
		shared_ptr<Mutator<Type> > constMutator = shared_ptr<Mutator<Type> >(),
		ProbabilityType mutationRate = GEN_DEF_MUTATION_RATE,
		bool useOpenMP = true
	);

	virtual ~GPMutator_BlockConstant();
};

/**
 * Applies mutation as implemented by the supplied constant mutator to a
 * randomly selected constant.
 */

template<class Type>
void GPMutator_BlockConstant<Type>::operator()(GeneticProgram& individual) {
	algotree::Context::iterator block_iter = individual.context()->find<algotree::ContextBlock_Constant<Type> >();

	if(block_iter != individual.context()->end()) {
		algotree::ContextBlock_Constant<Type>& block = block_iter->block<algotree::ContextBlock_Constant<Type> >();

		typename algotree::ContextBlock_Constant<Type>::iterator iter = block.begin();

		for(; iter != block.end(); iter++) {
			if(_gen() < _perConstMutationRate) {
				(*_constMutator)(iter->second);
			}
		}
	}
}

/**
* Assignment operator.
**/

template<class Type>
GPMutator_BlockConstant<Type>& GPMutator_BlockConstant<Type>::operator=(const GPMutator_BlockConstant<Type>& obj) {
	if(&obj != this) {
		Mutator<GeneticProgram>::operator=(obj);
		_constMutator = shared_ptr<Mutator<Type> >(clone(obj._constMutator));
		_perConstMutationRate = obj._perConstMutationRate;
		_gen = obj._gen;
	}

	return *this;
}

/**
* Copy constructor.
**/

template<class Type>
GPMutator_BlockConstant<Type>::
GPMutator_BlockConstant(const GPMutator_BlockConstant<Type>& obj):
	Mutator<GeneticProgram>(obj),
	_constMutator(clone(obj._constMutator)),
	_perConstMutationRate(obj._perConstMutationRate), _gen(obj._gen) {}

/**
* Constructor.
* @param constMutator The gene mutator used to mutate the constant.
*
**/

template<class Type>
GPMutator_BlockConstant<Type>::
GPMutator_BlockConstant(
	shared_ptr<Mutator<Type> > constMutator,
	ProbabilityType perConstMutationRate,
	bool useOpenMP
): Mutator<GeneticProgram>(1.0f, useOpenMP),
_constMutator(constMutator), _perConstMutationRate(perConstMutationRate),
_gen(0.0f, 1.0f) {}

/**
 * Destructor.
 */

template<class Type>
GPMutator_BlockConstant<Type>::~GPMutator_BlockConstant() {
	SYS_EXPORT_INSTANCE();
}

}//namespace genetor

SYS_EXPORT_TEMPLATE(genetor::GPMutator_BlockConstant, 1)

#endif //Genetor_GPMutator_BlockConstant_H_
