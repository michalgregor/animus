#include "gp_crossover.h"

#include "../../Node.h"
#include "../../index/NodeByTypeIndex.h"

namespace genetor {
namespace detail {

void ALGOTREE_API gp_crossover(algotree::Node& root1, algotree::Node& root2, bool includeRoot) {
	//create the indexes
	algotree::NodeByTypeIndex i1(root1, includeRoot);
	algotree::NodeByTypeIndex i2(root2, includeRoot);
	i1.intersect(i2, algotree::TC_Compatible);

	algotree::Node *n1(NULL), *n2(NULL);

	//if there are no compatible types in the two trees an exception may be thrown
	try {
		//select 2 random nodes with compatible types
		n1 = i1.randNode();
		n2 = i2.randNode(n1->getReturnType(), algotree::TC_Compatible);

		//swap subtrees
		if(n1 && n2) n1->swap(*n2, false, false);

	} catch(algotree::TracedError& err) {
		if(i1.size() && i2.size()) std::cerr << err.what() << std::endl;
	}
}

} //namespace detail
} //namespace genetor
