#ifndef Genetor_gp_crossover_H_
#define Genetor_gp_crossover_H_

#include "../../system.h"

//forward declaration
namespace algotree {class Node;}

namespace genetor {
namespace detail {

/**
 * Swaps randomly selected subtrees of nodes root1 and root2.
 * @param includeRoot If set to true, nodes root1 and root2 are themselves
 * subject to the random selection of crossover points. If set to false,
 * the randomly selected nodes are children nodes of root1 and root2 but never
 * root1 and root2 themselves.
 */

void ALGOTREE_API gp_crossover(algotree::Node& root1, algotree::Node& root2, bool includeRoot);

} //namespace detail
} //namespace genetor

#endif //Genetor_gp_crossover_H_
