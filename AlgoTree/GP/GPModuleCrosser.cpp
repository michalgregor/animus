#include "GPModuleCrosser.h"
#include "detail/gp_crossover.h"

#include "../module/ModuleGenerator.h"
#include "../module/ContextBlock_Modules.h"

namespace genetor {

/**
* Boost serialization function.
**/

void GPModuleCrosser::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Crosser<GeneticProgram> >(*this);
	ar & _maxDepth;
}

void GPModuleCrosser::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Crosser<GeneticProgram> >(*this);
	ar & _maxDepth;
}

/**
 * Returns the maximum depth that the contained tree is allowed to have.
 * Cutting is applied after crossover to maintain this. -1 means no limit.
 */

unsigned int GPModuleCrosser::getCutterMaxDepth() const {
	return _maxDepth;
}

/**
 * Specifies the cutting policy. Cutting of the tree is applied after crossover
 * to maintain a specified maximum depth.
 *
 * @param maxDepth The maximum depth that a tree is allowed to have.
 * -1 means no limit.
 * @ param generator A TreeGenerator used for cutting (to replace subtrees by
 * terminals).
 */

void GPModuleCrosser::setCutter(unsigned int maxDepth) {
	_maxDepth = maxDepth;
}

/**
 * Resets the cutting policy. The cutting TreeGenerator pointer is reset to NULL
 * and the maximum depth is set to -1 (no limit).
 */

void GPModuleCrosser::resetCutter() {
	_maxDepth = (unsigned int)-1;
}

/**
 * Cuts the Tree making sure that its maximum depth is as specified unless
 * the maximum depth is set to -1 (default), which means no limit.
 */

void GPModuleCrosser::cutModule(algotree::Node& root, const shared_ptr<algotree::ModuleGenerator>& generator) {
	if(_maxDepth != (unsigned int)-1) {
		algotree::Traverser t, tend = root.end();
		for(t = root.begin(); t != tend; t++) {
			if(t.depth() >= _maxDepth) generator->replace(*t, 0);
		}
	}
}

/**
 * This method applies in-place crossover to a single pair of individuals.
 */

void GPModuleCrosser::operator()(GeneticProgram& i1, GeneticProgram& i2) {
	algotree::ContextBlock_Modules& list1(i1.context()->modules());
	algotree::ContextBlock_Modules& list2(i2.context()->modules());

	//cross the modules; try at most 5 times to select modules signatures of which match
	for(unsigned int i = 0; i < 5; i++) {
		algotree::ContextBlock_Modules::iterator iter1 = list1.randEntry();
		//if there are no modules, break
		if(iter1 == list1.end()) break;
		algotree::ContextBlock_Modules::iterator iter2 = list2.randEntry(iter1->second.getSignature());
		if(iter2 != list2.end()) {
			//retrieve root nodes
			algotree::Node &root1(iter1->second.modify()), &root2(iter2->second.modify());
			//cross the two ModuleHandler trees
			detail::gp_crossover(root1, root2, true);
			//cut the trees
			cutModule(root1, iter1->second.getGenerator());
			cutModule(root2, iter2->second.getGenerator());
			break;
		}
	}
}


/**
* Assignment operator.
**/

GPModuleCrosser& GPModuleCrosser::operator=(const GPModuleCrosser& obj) {
	if(&obj != this) {
		Crosser<GeneticProgram>::operator=(obj);
		_maxDepth = obj._maxDepth;
	}

	return *this;
}

/**
* Copy constructor.
**/

GPModuleCrosser::GPModuleCrosser(const GPModuleCrosser& obj):
Crosser<GeneticProgram>(obj), _maxDepth(obj._maxDepth) {}

/**
* Constructor.
*
* The constructor also sets up the tree cutting mechanism. The mechanism
* ensures that depth of the tree does not exceed the maximum depth after the
* operator has been applied. The mechanism does not activate if the maximumDepth
* is set to -1 (default).
*
* @param maxDepth The maximum depth that a tree is allowed to have.
* -1 means no limit.
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param  Set to true if OpenMP is to be used to parallelise the application
* of the operator.
**/

GPModuleCrosser::GPModuleCrosser(unsigned int maxDepth, ProbabilityType crossoverRate, bool useOpenMP):
Crosser<GeneticProgram>(crossoverRate, useOpenMP), _maxDepth(maxDepth) {}

}//namespace genetor
