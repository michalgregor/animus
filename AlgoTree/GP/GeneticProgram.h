#ifndef GEN_GP_GENETICPROGRAM_H_INCLUDED
#define GEN_GP_GENETICPROGRAM_H_INCLUDED

#include "../Tree.h"

namespace genetor {

typedef algotree::Tree GeneticProgram;

} //namespace genetor

#endif // GEN_GP_GENETICPROGRAM_H_INCLUDED
