#ifndef Genetor_GPMutator_H_
#define Genetor_GPMutator_H_

#include "../system.h"
#include <Genetor/Mutator/Mutator.h>
#include "GeneticProgram.h"
#include "../TreeGenerator/TreeGenerator.h"

namespace genetor {

/**
* @class GPMutator
* @author Michal Gregor
* The standard Mutator for GeneticPrograms based on replacing a randomly
* selected subtree by a newly generated one.
**/
class ALGOTREE_API GPMutator: public Mutator<GeneticProgram> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	GPMutator();

private:
	//! The TreeGenerator used by the Mutator.
	shared_ptr<algotree::TreeGenerator> _treeGenerator;

public:
	virtual void operator()(GeneticProgram& individual);

	GPMutator& operator=(const GPMutator& obj);
	GPMutator(const GPMutator& obj);

	explicit GPMutator(
		const shared_ptr<algotree::TreeGenerator>& generator,
		ProbabilityType mutationRate = GEN_DEF_MUTATION_RATE,
		bool useOpenMP = true
	);

	virtual ~GPMutator() {}
};

}//namespace genetor

SYS_EXPORT_CLASS(genetor::GPMutator)

#endif //Genetor_GPMutator_H_
