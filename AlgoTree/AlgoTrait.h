#ifndef ALGOTRAIT_H_
#define ALGOTRAIT_H_

#include "system.h"
#include "Context/ProcessContext.h"
#include <Systematic/dynamic_typing/ConverterRegister.h>
#include <Systematic/serialization/PtrSerialize.h>

namespace algotree {

template<class Type, class Context>
class AlgoTrait2TypeConverter;

/**
* @class AlgoTrait
* @author Michal Gregor
*
* This class can be used to wrap a type so that it is viewed as distinct by the
* AlgoTree library, but in such way that a converter is provided which can
* automatically convert it to the original type. That way functors returning
* the AlgoTrait wrapper type cannot be substituted by those returning
* the original type, but it does work vice-versa.
*
* @tparam Type The original type.
* @tparam Context Type of the context. Context is necessary in order to
* AlgoTrait<Type, Context> convertible to Type using ConverterRegister.
**/

template<class Type, class Context = ProcessContext>
class AlgoTrait {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		systematic::PtrSerialize<Type, Archive>::serialize(ar, _data);
	}

private:
	//! Used to register converter AlgoTrait<Type, Context> -> Type.
	static const char _constructor;
	//! Stores the actual data.
	Type* _data;

public:
	//! Type of the contained data.
	typedef Type value_type;
	//! Type of the context used in conjunction with the
	typedef Context context_type;

public:
	operator Type&();
	operator const Type&() const;

	AlgoTrait<Type, Context>& operator=(const Type& data);
	AlgoTrait<Type, Context>& operator=(const AlgoTrait<Type, Context>& obj);

	AlgoTrait();
	AlgoTrait(const AlgoTrait<Type, Context>& obj);
	AlgoTrait(const Type& data);

	virtual ~AlgoTrait();
};

/**
* Cast operator.
**/

template<class Type, class Context>
AlgoTrait<Type, Context>::operator Type&() {
	if(!_data) throw TracedError_InvalidPointer("Invalid pointer to data.");
	return *_data;
}

/**
* Cast operator.
**/

template<class Type, class Context>
AlgoTrait<Type, Context>::operator const Type&() const {
	if(!_data) throw TracedError_InvalidPointer("Invalid pointer to data.");
	return *_data;
}

/**
* Assignment.
**/

template<class Type, class Context>
AlgoTrait<Type, Context>& AlgoTrait<Type, Context>::operator=(const Type& data) {
	if(_data) delete _data;
	_data = new Type(data);
}

/**
* Assignment.
**/

template<class Type, class Context>
AlgoTrait<Type, Context>& AlgoTrait<Type, Context>::operator=(const AlgoTrait<Type, Context>& obj) {
	if(this != &obj) {
		if(_data) delete _data;
		_data = new Type(*obj._data);
	}

	return *this;
}

/**
* Copy constructor.
**/

template<class Type, class Context>
AlgoTrait<Type, Context>::AlgoTrait(const AlgoTrait<Type, Context>& obj): _data((obj._data)?(new Type(*obj._data)):(NULL)) {}

/**
* Constructor.
**/

template<class Type, class Context>
AlgoTrait<Type, Context>::AlgoTrait(const Type& data): _data(new Type(data)) {}

/**
* Default constructor.
**/

template<class Type, class Context>
AlgoTrait<Type, Context>::AlgoTrait(): _data(NULL) {}

/**
* Destructor. Makes sure that converter AlgoTrait<Type, Context> -> Type is
* registered.
**/

template<class Type, class Context>
AlgoTrait<Type, Context>::~AlgoTrait() {
	(void)_constructor;
	if(_data) delete _data;
}

/**
* Registers converter AlgoTrait<Type, Context> -> Type.
**/

template<class Type, class Context>
const char AlgoTrait<Type, Context>::_constructor = (
	(AlgoConverterRegister().makeConvertible<AlgoTrait<Type, Context>, Type>(
		new AlgoTrait2TypeConverter<Type, Context>()
	)),
0);

//------------------------------------------------------------------------------
//------------------------AlgoTrait2TypeConverter-------------------------------
//------------------------------------------------------------------------------

/**
* @class AlgoTrait2TypeConverter
* This class is used as a TypeConverter from AlgoTrait<Type> to Type.
**/

template<class Type, class Context = ProcessContext>
class AlgoTrait2TypeConverter: public systematic::TypeConverter<Context> {
private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	/**
	* Converts object obj from certain data type to another. The returned
	* void pointer should always point to newly allocated memory. The original
	* object is not deleted.
	**/

	virtual systematic::Handler* convert(systematic::Handler* obj, Context& context) const {
		AlgoTrait<Type, Context>& trait = *static_cast<AlgoTrait<Type, Context>* >(obj->getPtr());
		return new systematic::GenericHandler<Type>(trait);
	}

	/**
	* Empty body of the virtual destructor.
	**/

	virtual ~AlgoTrait2TypeConverter() {}
};

}//namespace algotree

SYS_REGISTER_TEMPLATENAME(algotree::AlgoTrait, 2)

#endif /* ALGOTRAIT_H_ */
