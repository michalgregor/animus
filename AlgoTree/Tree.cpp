#include "Tree.h"
#include "Context/ProcessContext.h"

namespace algotree {

/**
* Boost serialization function.
**/

void Tree::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _treeContext;
	ar & _signature;
	ar & _root;
}

void Tree::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _treeContext;
	ar & _signature;
	ar & _root;
}

/**
* Returns the return type of the Tree - this has the same effect as asking the
* root node directly.
**/

systematic::TypeID Tree::getReturnType() const {
	return _signature.getReturnType();
}

/**
* Returns a begin traverser (traverser goes over all Nodes; used as an iterator).
**/

Traverser Tree::begin() {
	return Traverser(_root);
}

/**
* Returns an end traverser (traverser goes over all Nodes; used as an iterator).
**/

Traverser Tree::end() {
	return Traverser(NULL, 2);
}

/**
* Returns a traverser pointing to the reverse end (element right before
* the beginning).
**/

Traverser Tree::rend() {
	return Traverser(_root, 1);
}

/**
* Returns a begin traverser (traverser goes over all Nodes; used as an iterator).
**/

ConstTraverser Tree::begin() const {
	return ConstTraverser(_root);
}

/**
* Returns an end traverser (traverser goes over all Nodes; used as an iterator).
**/

ConstTraverser Tree::end() const {
	return ConstTraverser(NULL, 2);
}

/**
* Returns a traverser pointing to the reverse end (element right before
* the beginning).
**/

ConstTraverser Tree::rend() const {
	return ConstTraverser(_root, 1);
}

/**
* Returns types of arguments that are to be passed to process() (within a
* ProcessContext object).
**/

const TypeList& Tree::getArgTypes() const {
	return _signature.getArgTypes();
}

/**
* Returns FunctorSignature of the Tree, that is its return type and argument
* types.
*/

const FunctorSignature& Tree::getSignature() const {
	return _signature;
}

/**
* Calls process of the root Node and returns the result. This may throw an
* AlgoBreak in case that the execution does not finish in time, etc.
**/

AlgoType Tree::process(ProcessContext& context_) const {
	RecursionHolder(context_.execGuard().startRecursion());
	AlgoType data = _root->process(context_);
	return data;
}

/**
* Returns a lisp-like representation of the tree.
**/

std::string Tree::getSExpression() const {
	std::string sexp = "Tree:\n\n" + _root->getSExpression();

	ContextBlock_Modules::const_iterator iter, iend(_treeContext->modules().end());
	for(iter = _treeContext->modules().begin(); iter != iend; iter++) {
		sexp += "\n\nModule " + std::string(iter->second.getID()) + " :\n";
		sexp += iter->second.access().getSExpression();
	}

	return sexp;
}

/**
 * Swaps contents of the Trees.
 */

void Tree::swap(Tree& tree) {
	TreeContext* tmpContext = _treeContext;
	_treeContext = tree._treeContext;
	tree._treeContext = tmpContext;

	_signature.swap(tree._signature);

	Node* tmpNode = _root;
	_root = tree._root;
	tree._root = tmpNode;
}

/**
* Assignment.
**/

Tree& Tree::operator=(const Tree& tree) {
	if(this != &tree) {
		*_treeContext = *tree._treeContext;
		_signature = tree._signature;
		*_root = *tree._root;

		_root->setTreeContext(_treeContext, true, true);
	}
	return *this;
}

/**
* Copy constructor.
**/

Tree::Tree(const Tree& tree): _treeContext(new TreeContext(*tree._treeContext)),
_signature(tree._signature), _root(new Node(*tree._root))
 {
	_root->setTreeContext(_treeContext, true, true);
}

/**
* Constructor.
* @param returnType The return type of the Tree (return type of the root Node).
* @param argTypes Types of arguments that are to be passed to Tree on process
* (within a ProcessContext object); defaults to an empty vector which means
* no parameters; parameters are only copied from the register, reference to the
* register is not stored.
**/

Tree::Tree(
	systematic::TypeID returnType,
	const TypeList& argList
): _treeContext(new TreeContext()), _signature(returnType, argList),
_root(new Node(_treeContext, returnType)) {}

/**
* Constructor.
* @param signature Signature of the tree, that is its return type and
* argument types.
**/

Tree::Tree(const FunctorSignature& signature):
_treeContext(new TreeContext()), _signature(signature),
_root(new Node(_treeContext, signature.getReturnType())) {}

/**
* Constructor.
* @param rootNodeFunctor Functor for the root Node.
* @param argTypes Types of arguments that are to be passed to Tree on process
* (within a ProcessContext object); defaults to an empty vector which means
* no parameters.
**/

Tree::Tree(
	const intrusive_ptr<NodeFunctor>& rootNodeFunctor,
	const TypeList& argList
): _treeContext(new TreeContext()), _signature(rootNodeFunctor->getReturnType(), argList),
_root(new Node(_treeContext, rootNodeFunctor)) {}

/**
* Constructor.
* @param rootNodeFunctor Functor for the root Node.
* @param signature Signature of the tree, that is its return type and
* argument types. Return type of the signature is set
* to rootNodeFunctor.getReturnType() so that these are consistent.
**/

Tree::Tree(
	const intrusive_ptr<NodeFunctor>& rootNodeFunctor,
	const FunctorSignature& signature
): _treeContext(new TreeContext()),
_signature(rootNodeFunctor->getReturnType(), signature.getArgTypes()),
_root(new Node(_treeContext, rootNodeFunctor)) {}

/**
 * Destructor.
 */

Tree::~Tree() {
	delete _root;
	delete _treeContext;
}

} //namesapce algotree
