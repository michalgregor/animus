#include "FunctorRegister.h"
#include <Systematic/dynamic_typing/ConverterRegister.h>
#include <Systematic/generator/BoundaryGenerator.h>

namespace algotree {

/**
* Boost serialization function.
**/

void FunctorRegister::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _branchRegister;
	ar & _leafRegister;
	ar & _treeContextInits;
}

void FunctorRegister::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _branchRegister;
	ar & _leafRegister;
	ar & _treeContextInits;
}

/**
* Adds a new functor to the register and files it under branch (functor with
* arguments) or leaf (functor with no arguments). The functor pointer must not
* be NULL.
**/

void FunctorRegister::addFunctor(const intrusive_ptr<NodeFunctor>& functor) {
	if(!functor) throw TracedError_InvalidPointer("NULL pointer to functor.");
	if(functor->getNumArgs() > 0) //add as a branch
		_branchRegister[functor->getReturnType()].push_back(intrusive_ptr<FunctorFactory>(new FunctorFactory_CloneFunctor(functor)));
	else //add as a leaf
		_leafRegister[functor->getReturnType()].push_back(intrusive_ptr<FunctorFactory>(new FunctorFactory_CloneFunctor(functor)));
}

/**
* Adds a functor factory.
**/

void FunctorRegister::addFactory(const intrusive_ptr<FunctorFactory>& factory) {
	if(!factory) throw TracedError_InvalidPointer("NULL pointer to functor.");
	if(factory->isBranch()) //add as a branch
		_branchRegister[factory->getReturnType()].push_back(factory);
	else //add as a leaf
		_leafRegister[factory->getReturnType()].push_back(factory);

	if(factory->hasTreeContextInitializer()) _treeContextInits.push_back(factory->getTreeContextIntializer());
}

/**
* Clears the container of branches and the container of leaves.
**/

void FunctorRegister::clear() {
	_branchRegister.clear();
	_leafRegister.clear();
}

/**
* Clears the container of branches.
**/

void FunctorRegister::clearBranches() {
	_branchRegister.clear();
}

/**
* Clears the container of leaves.
**/

void FunctorRegister::clearLeaves() {
	_leafRegister.clear();
}

/**
* Makes a new copy of a randomly chosen functor (either a functor with
* arguments - branch, or without arguments - leaf) and returns the pointer.
* If no functor is found an AlgoError_NoFunctorFound is thrown.
*
* Before any of the newRand functions is used with a TreeContext, the TreeContext
* should be initialized using initTreeContext().
**/

intrusive_ptr<NodeFunctor> FunctorRegister::newRandFunctor(TreeContext* treeContext) const {
	unsigned int branchCount = numBranches(), leafCount = numLeaves();
	FunctorIterator iter;

	//select a random functor
	unsigned int sel = BoundaryGenerator<unsigned int>(0, branchCount + leafCount - 1)();
	//iterate to the random functor and return a clone
	unsigned int count = 0;

	if(sel < branchCount) {
		for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
			count += iter->second.size();
			if(sel < count) {
				sel -= count - iter->second.size();
				return (*iter->second.at(sel))(treeContext);
			}
		}
	} else {
		sel -= branchCount;
		for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
			count += iter->second.size();
			if(sel < count) {
				sel -= count - iter->second.size();
				return (*iter->second.at(sel))(treeContext);
			}
		}
	}

	throw AlgoError_NoFunctorFound("No such functor found.");
}

/**
* Makes a new copy of a randomly chosen branch (functor with arguments).
* If no functor is found an AlgoError_NoFunctorFound is thrown.
*
* Before any of the newRand functions is used with a TreeContext, the TreeContext
* should be initialized using initTreeContext().
**/

intrusive_ptr<NodeFunctor> FunctorRegister::newRandBranch(TreeContext* treeContext) const {
	FunctorIterator iter;
	//select a random functor
	unsigned int sel = BoundaryGenerator<unsigned int>(0, numBranches() - 1)();
	unsigned int count = 0;

	for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
		count += iter->second.size();
		if(sel < count) {
			sel -= count - iter->second.size();
			return (*iter->second.at(sel))(treeContext);
		}
	}

	throw AlgoError_NoFunctorFound("No such functor found.");
}

/**
* Makes a new copy of a randomly chosen leaf (functor without arguments).
* If no functor is found an AlgoError_NoFunctorFound is thrown.
*
* Before any of the newRand functions is used with a TreeContext, the TreeContext
* should be initialized using initTreeContext().
**/

intrusive_ptr<NodeFunctor> FunctorRegister::newRandLeaf(TreeContext* treeContext) const {
	FunctorIterator iter;
	//select a random functor
	unsigned int sel = BoundaryGenerator<unsigned int>(0, numLeaves() - 1)();
	unsigned int count = 0;

	for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
		count += iter->second.size();
		if(sel < count) {
			sel -= count - iter->second.size();
			return (*iter->second.at(sel))(treeContext);
		}
	}

	throw AlgoError_NoFunctorFound("No such functor found.");
}

/**
* Makes a new copy of a randomly chosen functor filed under the specified type.
* If no functor is found an AlgoError_NoFunctorFound is thrown.
* @param tc Determines whether types will be compared strictly (equality),
* by one-way convertibility, or compatibility (both-way convertibility).
*
* Before any of the newRand functions is used with a TreeContext, the TreeContext
* should be initialized using initTreeContext().
**/

intrusive_ptr<NodeFunctor> FunctorRegister::
newRandFunctor(TreeContext* treeContext, TypeID type, TypeComparison tc) const {
	FunctorIterator iter;
	if(tc == TC_Strict) {
		unsigned int count1 = numBranches(type, tc), count2 = numLeaves(type, tc);
		//if no functor of such type exists throw AlgoError_NoFunctorFound
		if(!count1 && !count2) throw AlgoError_NoFunctorFound("No such functor found (" + type.name() + ").");
		//select a functor and return its clone
		unsigned int sel = BoundaryGenerator<unsigned int>(0, count1 + count2 - 1)();
		if(sel < count1) return (*_branchRegister.find(type)->second.at(sel))(treeContext);
		else return (*_leafRegister.find(type)->second.at(sel - count1))(treeContext);
	} else {
		unsigned int count1 = numBranches(type, tc), count2 = numLeaves(type, tc);
		unsigned int count = 0;
		//if no functor of such type exists throw AlgoError_NoFunctorFound
		if(!count1 && !count2) throw AlgoError_NoFunctorFound("No such functor found (" + type.name() + ").");
		//select a functor and return its clone
		unsigned int sel = BoundaryGenerator<unsigned int>(0, count1 + count2 - 1)();

		if(tc == TC_Convertible) {
			//select a functor and return its clone
			if(sel < count1) {
				for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
					if(AlgoConverterRegister().convertible(iter->first, type)) {
						count += iter->second.size();
						if(sel < count) {
							sel -= count - iter->second.size();
							return (*iter->second.at(sel))(treeContext);
						}
					}
				}
			} else {
				sel -= count1;
				for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
					if(AlgoConverterRegister().convertible(iter->first, type)) {
						count += iter->second.size();
						if(sel < count) {
							sel -= count - iter->second.size();
							return (*iter->second.at(sel))(treeContext);
						}
					}
				}
			}
		} else {
			//select a functor and return its clone
			if(sel < count1) {
				for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
					if(AlgoConverterRegister().compatible(iter->first, type)) {
						count += iter->second.size();
						if(sel < count) {
							sel -= count - iter->second.size();
							return (*iter->second.at(sel))(treeContext);
						}
					}
				}
			} else {
				sel -= count1;
				for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
					if(AlgoConverterRegister().compatible(iter->first, type)) {
						count += iter->second.size();
						if(sel < count) {
							sel -= count - iter->second.size();
							return (*iter->second.at(sel))(treeContext);
						}
					}
				}
			}
		}

		throw AlgoError_NoFunctorFound("No such functor found (" + type.name() + ").");
	}
}

/**
* Makes a new copy of a randomly chosen branch (functor with arguments) filed
* under the specified type.
* If no functor is found an AlgoError_NoFunctorFound is thrown.
* @param tc Determines whether types will be compared strictly (equality),
* by one-way convertibility, or compatibility (both-way convertibility).
*
* Before any of the newRand functions is used with a TreeContext, the TreeContext
* should be initialized using initTreeContext().
**/

intrusive_ptr<NodeFunctor> FunctorRegister::
newRandBranch(TreeContext* treeContext, TypeID type, TypeComparison tc) const {
	if(tc == TC_Strict) {
		FunctorIterator iter = _branchRegister.find(type);
		if(iter == _branchRegister.end() || iter->second.size() == 0) throw AlgoError_NoFunctorFound("No such functor found (" + type.name() + ").");
		unsigned int sel = BoundaryGenerator<unsigned int>(0, iter->second.size() - 1)();
		return (*iter->second.at(sel))(treeContext);
	} else {
		FunctorIterator iter;
		//select a random functor
		unsigned int sel = BoundaryGenerator<unsigned int>(0, numBranches(type, tc) - 1)();
		unsigned int count = 0;

		if(tc == TC_Convertible) {
			for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
				if(AlgoConverterRegister().convertible(iter->first, type)) {
					count += iter->second.size();
					if(sel < count) {
						sel -= count - iter->second.size();
						return (*iter->second.at(sel))(treeContext);
					}
				}
			}
		} else {
			for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
				if(AlgoConverterRegister().compatible(iter->first, type)) {
					count += iter->second.size();
					if(sel < count) {
						sel -= count - iter->second.size();
						return (*iter->second.at(sel))(treeContext);
					}
				}
			}
		}

		throw AlgoError_NoFunctorFound("No such functor found (" + type.name() + ").");
	}
}

/**
* Makes a new copy of a randomly chosen leaf (functor without arguments) filed
* under the specified type.
* If no functor is found an AlgoError_NoFunctorFound is thrown.
* @param tc Determines whether types will be compared strictly (equality),
* by one-way convertibility, or compatibility (both-way convertibility).
*
* Before any of the newRand functions is used with a TreeContext, the TreeContext
* should be initialized using initTreeContext().
**/

intrusive_ptr<NodeFunctor> FunctorRegister::
newRandLeaf(TreeContext* treeContext, TypeID type, TypeComparison tc) const {
	if(tc == TC_Strict) {
		FunctorIterator iter = _leafRegister.find(type);
		if(iter == _leafRegister.end() || iter->second.size() == 0) throw AlgoError_NoFunctorFound("No such functor found (" + type.name() + ").");
		unsigned int sel = BoundaryGenerator<unsigned int>(0, iter->second.size() - 1)();
		return (*iter->second.at(sel))(treeContext);
	} else {
		FunctorIterator iter;
		//select a random functor
		unsigned int sel = BoundaryGenerator<unsigned int>(0, numLeaves(type, tc) - 1)();
		unsigned int count = 0;

		if(tc == TC_Convertible) {
			for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
				if(AlgoConverterRegister().convertible(iter->first, type)) {
					count += iter->second.size();
					if(sel < count) {
						sel -= count - iter->second.size();
						return (*iter->second.at(sel))(treeContext);
					}
				}
			}
		} else {
			for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
				if(AlgoConverterRegister().compatible(iter->first, type)) {
					count += iter->second.size();
					if(sel < count) {
						sel -= count - iter->second.size();
						return (*iter->second.at(sel))(treeContext);
					}
				}
			}
		}

		throw AlgoError_NoFunctorFound("No such functor found (" + type.name() + ").");
	}
}

/**
 * Initializes the provided TreeContext using the list of initializers extracted
 * from the factories.
 */

void FunctorRegister::initTreeContext(TreeContext* treeContext) const {
	std::list<intrusive_ptr<TreeContextInitializer> >::const_iterator iter;
	for(iter = _treeContextInits.begin(); iter != _treeContextInits.end(); iter++) {
		(*iter)->initTreeContext(treeContext);
	}
}

/**
* Alias of numFunctors(). Returns the number of all functors (branches and
* leaves).
* (Iterates through all functor types counting.)
**/

unsigned int FunctorRegister::size() const {
	return numFunctors();
}

/**
* Returns the number of all functors (branches and leaves).
* (Iterates through all functor types counting.)
**/

unsigned int FunctorRegister::numFunctors() const {
	FunctorIterator iter;
	unsigned int count = 0;

	//count branches
	for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
		count += iter->second.size();
	}

	//count leaves
	for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
		count += iter->second.size();
	}

	return count;
}

/**
* Returns the number of branches. (Iterates through all functor types counting.)
**/

unsigned int FunctorRegister::numBranches() const {
	FunctorIterator iter;
	unsigned int count = 0;

	//count branches
	for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
		count += iter->second.size();
	}

	return count;
}

/**
* Returns the number of leaves. (Iterates through all functor types counting.)
**/

unsigned int FunctorRegister::numLeaves() const {
	FunctorIterator iter;
	unsigned int count = 0;

	//count leaves
	for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
		count += iter->second.size();
	}

	return count;
}

/**
* Returns the number of all functors (branches and leaves) filed under
* the specified type.
* @param tc Determines whether types will be compared strictly (equality),
* by one-way convertibility, or compatibility (both-way convertibility).
**/

unsigned int FunctorRegister::
numFunctors(TypeID type, TypeComparison tc) const {
	FunctorIterator iter;
	unsigned int count = 0;

	if(tc == TC_Strict) {
		//assign number of branches filed under type to count1
		iter = _branchRegister.find(type);
		if(iter != _branchRegister.end()) count += iter->second.size();

		//assign number of leaves filed under type to count2
		iter = _leafRegister.find(type);
		if(iter != _leafRegister.end()) count += iter->second.size();
	} else {
		if(tc == TC_Convertible) {
			//count branches
			for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
				if(AlgoConverterRegister().convertible(iter->first, type))
					count += iter->second.size();
			}

			//count leaves
			for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
				if(AlgoConverterRegister().convertible(iter->first, type))
					count += iter->second.size();
			}
		} else {
			//count branches
			for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
				if(AlgoConverterRegister().compatible(iter->first, type))
					count += iter->second.size();
			}

			//count leaves
			for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
				if(AlgoConverterRegister().compatible(iter->first, type))
					count += iter->second.size();
			}
		}
	}

	return count;
}

/**
* Returns the number of all branches filed under the specified type.
* @param tc Determines whether types will be compared strictly (equality),
* by one-way convertibility, or compatibility (both-way convertibility).
**/

unsigned int FunctorRegister::
numBranches(TypeID type, TypeComparison tc) const {
	FunctorIterator iter;
	unsigned int count = 0;

	if(tc == TC_Strict) {
		iter = _branchRegister.find(type);
		if(iter != _branchRegister.end()) count += iter->second.size();
	} else {
		if(tc == TC_Convertible) {
			for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
				if(AlgoConverterRegister().convertible(iter->first, type))
					count += iter->second.size();
			}
		} else {
			for(iter = _branchRegister.begin(); iter != _branchRegister.end(); iter++) {
				if(AlgoConverterRegister().compatible(iter->first, type))
					count += iter->second.size();
			}
		}
	}

	return count;
}

/**
* Returns the number of all leaves filed under the specified type.
* @param tc Determines whether types will be compared strictly (equality),
* by one-way convertibility, or compatibility (both-way convertibility).
**/

unsigned int FunctorRegister::
numLeaves(TypeID type, TypeComparison tc) const {
	FunctorIterator iter;
	unsigned int count = 0;

	if(tc == TC_Strict) {
		iter = _leafRegister.find(type);
		if(iter != _leafRegister.end()) count += iter->second.size();
	} else {
		if(tc == TC_Convertible) {
			for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
				if(AlgoConverterRegister().convertible(iter->first, type))
					count += iter->second.size();
			}
		} else {
			for(iter = _leafRegister.begin(); iter != _leafRegister.end(); iter++) {
				if(AlgoConverterRegister().compatible(iter->first, type))
					count += iter->second.size();
			}
		}
	}

	return count;
}

/**
* Returns cons reference to the container of added branch functors.
**/

const std::map<TypeID, std::vector<intrusive_ptr<FunctorFactory> > >&
FunctorRegister::getBranchFactories() const {
	return _branchRegister;
}

/**
* Returns cons reference to the container of added leaf functors.
**/

const std::map<TypeID, std::vector<intrusive_ptr<FunctorFactory> > >&
FunctorRegister::getLeafFactories() const {
	return _leafRegister;
}

/**
* Default contructor.
**/

FunctorRegister::FunctorRegister():
_branchRegister(), _leafRegister(), _treeContextInits() {}

} //namespace algotree
