#ifndef ALGO_NODE_H_INCLUDED
#define ALGO_NODE_H_INCLUDED

#include <vector>
#include "system.h"
#include "AlgoType.h"

namespace algotree {

//forward declaration
class Tree;
class Traverser;
class ConstTraverser;
class NodeFunctor;
class TreeContext;
class ProcessContext;

/**
* @class Node
* @author Michal Gregor
* The basic element of an algorithmic tree.
* @tparam TypeID A template-based type id provider. See AlgoTypeID for
* an instance.
**/
class ALGOTREE_API Node {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	friend class Tree;
	//! Return type of the node.
	systematic::TypeID _type;
	//! Pointer to the associated functor. The object is owned by the Node and
	//! gets deleted from the destructor.
	intrusive_ptr<NodeFunctor> _functor;
	//! Pointer to the parent Node. Set to NULL if this is the root node.
	Node* _parent;
	//! Children nodes.
	std::vector<Node> _children;
	//! Pointer back to the tree context of the tree that contains the Node.
	TreeContext* _treeContext;

	void parentTheChildren();

public:
	//! Returns pointer to the tree of which the Node forms a part.
	const TreeContext* getTreeContext() const {return _treeContext;}
	//! Returns pointer to the tree of which the Node forms a part.
	TreeContext* getTreeContext() {return _treeContext;}
	void setTreeContext(TreeContext* treeContext, bool setForChildren = true, bool strictCopy = true);

	Node* parent() const;
	Node* next() const;
	Node* prev() const;
	Node* child() const;
	Node* eldest() const;
	Node* youngest() const;

	unsigned int size() const;
	unsigned int depth() const;
	unsigned int subtreeDepth() const;

	Traverser begin();
	Traverser end();

	ConstTraverser begin() const;
	ConstTraverser end() const;

	systematic::TypeID getReturnType() const;
	intrusive_ptr<NodeFunctor> getFunctor();
	AlgoType process(ProcessContext& context) const;

	bool isNull() const;

	void setFunctor(const intrusive_ptr<NodeFunctor>& functor);
	void reset();

	void resetChildren();

	std::string getSExpression() const;

	void swap(Node& node, bool swapParents = false, bool strictCopy = true);
	Node& operator=(const Node& node);

	Node();
	Node(const Node& node);
	explicit Node(TreeContext* treeContext, systematic::TypeID returnType = systematic::type_id<void>(), Node* parentNode = NULL);
	explicit Node(TreeContext* treeContext, const intrusive_ptr<NodeFunctor>& functor, Node* parentNode = NULL);
	~Node();
};

} //namespace algotree

SYS_REGISTER_TYPENAME(algotree::Node)

#endif // ALGO_NODE_H_INCLUDED

