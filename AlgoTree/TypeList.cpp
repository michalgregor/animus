#include "TypeList.h"
namespace algotree {

/**
* Boost serialization function.
**/

void TypeList::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<RawList>(*this);
	ar & handles;
}

void TypeList::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<RawList>(*this);
	ar & handles;
}

/**
* Returns a 0 if both TypeLists are equal, a negative value if this is less
* than typeList and a positive value if this is greater than typeList.
 */

int TypeList::compare(const TypeList& typeList) const {
	bool thisShorter(size() < typeList.size());
	size_type sz = (thisShorter)?(size()):(typeList.size());

	for(size_type i = 0; i < sz; i++) {
		if(entry(i) < typeList.entry(i)) return -1;
		else if(entry(i) > typeList.entry(i)) return 1;
	}

	if(thisShorter) return -1;
	else if(size() == typeList.size()) return 0;
	else return 1;
}

bool TypeList::operator<(const TypeList& typeList) const {
	return (compare(typeList) < 0);
}

bool TypeList::operator<=(const TypeList& typeList) const {
	return (compare(typeList) <= 0);
}

bool TypeList::operator>(const TypeList& typeList) const {
	return (compare(typeList) > 0);
}

bool TypeList::operator>=(const TypeList& typeList) const {
	return (compare(typeList) >= 0);
}

/**
* Compares *this to the specified TypeList.
**/

bool TypeList::operator==(const TypeList& typeList) const {
	size_type sz(size());
	if(sz != typeList.size()) return false;

	for(size_type i = 0; i < sz; i++)
		if(entry(i) != typeList.entry(i)) return false;

	return true;
}

/**
* Compares *this to the specified TypeList.
**/

bool TypeList::operator!=(const TypeList& typeList) const {
	size_type sz(size());
	if(sz != typeList.size()) return true;

	for(size_type i = 0; i < sz; i++)
		if(entry(i) != typeList.entry(i)) return true;

	return false;
}

/**
* Adds an argument of the specified type and returns the corresponding BlockHandle_Argument.
**/

BlockHandle_Argument TypeList::addEntry(TypeID type) {
	RawList::push_back(type);
	return BlockHandle_Argument(type, size() - 1);
}

/**
* Adds an argument of the specified type.
**/

void TypeList::push_back(TypeID type) {
	RawList::push_back(type);
}

/**
* Retrieves an BlockHandle_Argument corresponding to the specified index.
**/

BlockHandle_Argument TypeList::handle(TypeList::size_type index) {
	return BlockHandle_Argument(RawList::at(index), index);
}

/**
* Returns a reference to a TypeID corresponding to the specified index.
**/

systematic::TypeID& TypeList::entry(TypeList::size_type index) {
	return RawList::at(index);
}

/**
* Returns a const reference to a TypeID corresponding to the specified index.
**/

const systematic::TypeID& TypeList::entry(TypeList::size_type index) const {
	return RawList::at(index);
}

/**
* Assignment.
**/

TypeList& TypeList::operator=(const TypeList& typeList) {
	RawList::operator=(typeList);
	return *this;
}

/**
* Copy constructor.
**/

TypeList::TypeList(const TypeList& typeList): TypeList::RawList(typeList),
handles(this) {}

/**
* Construct from a container of the underlying type.
**/

TypeList::TypeList(const RawList& rawList): TypeList::RawList(rawList), handles(this) {}

/**
* Default constructor.
**/

TypeList::TypeList(): TypeList::RawList(), handles(this) {}

/**
 * Initializes the TypeList with num entries containing value.
 */

TypeList::TypeList(unsigned int num, const systematic::TypeID& value):
TypeList::RawList(num, value), handles(this) {}

} //namespace algotree
