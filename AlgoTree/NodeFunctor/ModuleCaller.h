#ifndef MODULECALLER_H_INCLUDED
#define MODULECALLER_H_INCLUDED

#include "../system.h"
#include "../module/ModuleHandler.h"
#include "../module/ContextBlock_Modules.h"
#include "NodeFunctor.h"

namespace algotree {

/**
* @class ModuleCaller
* @author Michal Gregor
* This functor wraps in a ModuleHandler upon which it calls to do the processing. The
* signature of the NodeFunctor copies that of the supplied ModuleHandler.
**/
class ALGOTREE_API ModuleCaller: public NodeFunctor {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	ModuleCaller();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The contained module.
	ModuleHandler _module;
	//! Stores the FunctorSignature of the module.
	FunctorSignature _signature;
	//! ID store used to hold id for use in getSExpression().
	ModuleID _id;

public:
	virtual TypeID getReturnType() const;

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* treeContext,
		const std::vector<Node>& children
	) const;

	virtual unsigned int getNumArgs() const;
	virtual const TypeList& getArgTypes() const;
	virtual std::string getSExpression() const;

	virtual void moveData(TreeContext* oldContext, TreeContext* newContext, bool strictCopy);
	virtual void clearData(TreeContext* treeContext);

	ModuleCaller& operator=(const ModuleCaller& obj);
	ModuleCaller(const ModuleCaller& obj);

	ModuleCaller(TreeContext* treeContext, const ModuleHandler& module);
	virtual ~ModuleCaller() {}
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::ModuleCaller)

#endif // MODULECALLER_H_INCLUDED
