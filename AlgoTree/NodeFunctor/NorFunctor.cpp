#include "NorFunctor.h"

namespace algotree {

/**
* Boost serialization function.
**/

void NorFunctor::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _argTypes;
}

void NorFunctor::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _argTypes;
}

AlgoType NorFunctor::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	if(!(children.at(0).process(context).getData<bool>() || children.at(1).process(context).getData<bool>())) {
		return AlgoType(true);
	} else {
		return AlgoType(false);
	}
}

std::string NorFunctor::getSExpression() const {
	return "NOR";
}

NorFunctor::NorFunctor(): _argTypes() {
	_argTypes.reserve(2);
	_argTypes.push_back(systematic::type_id<bool>());
	_argTypes.push_back(systematic::type_id<bool>());
}

} //namespace algotree
