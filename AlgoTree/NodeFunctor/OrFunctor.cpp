#include "OrFunctor.h"

namespace algotree {

/**
* Boost serialization function.
**/

void OrFunctor::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _argTypes;
}

void OrFunctor::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _argTypes;
}

AlgoType OrFunctor::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	if(children.at(0).process(context).getData<bool>() || children.at(1).process(context).getData<bool>()) {
		return AlgoType(true);
	} else {
		return AlgoType(false);
	}
}

std::string OrFunctor::getSExpression() const {
	return "OR";
}

OrFunctor::OrFunctor(): _argTypes() {
	_argTypes.reserve(2);
	_argTypes.push_back(systematic::type_id<bool>());
	_argTypes.push_back(systematic::type_id<bool>());
}

} //namespace algotree
