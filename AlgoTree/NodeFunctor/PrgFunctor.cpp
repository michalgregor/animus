#include "PrgFunctor.h"

namespace algotree {

/**
* Boost serialization function.
**/

void PrgFunctor::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _args;
}

void PrgFunctor::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _args;
}

//! Returns what type the process() method returns. This is actually an
//! identifier (got by mapping through Type2Item) of the internal type
//! contained in class Value.
TypeID PrgFunctor::getReturnType() const {
	return systematic::type_id<void>();
}

//! The main processing function. It is called from Node. Types of values
//! passed in args must be in accordance with the getArgTypes() vector.
AlgoType PrgFunctor::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	if(_args.size() != children.size()) throw TracedError_InvalidArgument("Number of arguments is not as prescribed.");
	TypeID voidType = systematic::type_id<void>();
	for(unsigned int i = 0; i < children.size(); i++) {
		AlgoType val = children[i].process(context);
		if(!val.convertible(voidType)) throw TracedError_InvalidArgument("Type mismatch.");
	}
	return AlgoType();
}

//! Returns the number of arguments expected - that is length of args vector
//! passed to process().
unsigned int PrgFunctor::getNumArgs() const {
	return _args.size();
}

//! Returns what what types should the the args vector passed to process()
//! contain (see documentation of process()).
const TypeList& PrgFunctor::getArgTypes() const {
	return _args;
}

//! Returns an s-expression describing the NodeFunctor. This is usually the
//! class name, but e.g. for constants it could be something like "Const(14.5f)".
std::string PrgFunctor::getSExpression() const {
	return "PRG";
}

/**
* Changes the number of arguments. This should not be done once the functor is
* inside a Node as Node will only check the number on call to setFunctor().
**/

void PrgFunctor::setNumArgs(unsigned int num) {
	_args.resize(num, systematic::type_id<void>());
}

/**
* Default constructor. You may want to set the number of arguments using
* setNumArgs() if you use this constructor.
**/

PrgFunctor::PrgFunctor(): _args() {}

/**
* Constructor.
* @param numArgs  Number of arguments that the functor will take.
**/

PrgFunctor::PrgFunctor(unsigned int numArgs):
_args(numArgs, systematic::type_id<void>()) {}

} //namespace algotree
