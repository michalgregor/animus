#ifndef NOTFUNCTOR_H_INCLUDED
#define NOTFUNCTOR_H_INCLUDED

#include <AlgoTree/NodeFunctor/NodeFunctor.h>

namespace algotree {

using systematic::TypeID;

class ALGOTREE_API NotFunctor: public algotree::NodeFunctor {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Vector of argument types.
	TypeList _argTypes;

public:
	virtual TypeID getReturnType() const {return systematic::type_id<bool>();}

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	virtual unsigned int getNumArgs() const {return _argTypes.size();}
	virtual const TypeList& getArgTypes() const {return _argTypes;}

	virtual std::string getSExpression() const;

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	NotFunctor();
	virtual ~NotFunctor() {}
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::NotFunctor)

#endif // NOTFUNCTOR_H_INCLUDED
