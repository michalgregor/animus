#ifndef VARIABLEFUNCTOR_H_INCLUDED
#define VARIABLEFUNCTOR_H_INCLUDED

#include "LeafFunctor.h"
#include <Systematic/type_info.h>
#include <Systematic/utils/ToString.h>
#include <Systematic/utils/StorageToken.h>

#include "../Variable.h"

namespace algotree {

/**
* @class VariableFunctor
* Stores a variable of a certain type and returns it in a AlgoType.
**/

template<class Type>
class VariableFunctor: public LeafFunctor {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<LeafFunctor>(*this);
		ar & _storageToken;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Stores the variable storage token.
	StorageToken _storageToken;

public:
	const StorageToken& getStorageToken() {return _storageToken;}
	void setStorageToken(const StorageToken& token) {_storageToken = token;}

	virtual systematic::TypeID getReturnType() const {
		return systematic::type_id<Variable<Type> >();
	}

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	virtual std::string getSExpression() const;

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	explicit VariableFunctor(StorageToken storageToken = StorageToken()):
	_storageToken(storageToken) {}

	~VariableFunctor() {
		SYS_EXPORT_INSTANCE()
	}
};

template<class Type>
AlgoType VariableFunctor<Type>::process(
	ProcessContext& UNUSED(context),
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& UNUSED(children)
) const {
	return AlgoType(Variable<Type>(_storageToken));
}

template<class Type>
std::string VariableFunctor<Type>::getSExpression() const {
	return "Var(" + systematic::ToString(_storageToken) + ")";
}

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::VariableFunctor, 1)

#endif // VARIABLEFUNCTOR_H_INCLUDED
