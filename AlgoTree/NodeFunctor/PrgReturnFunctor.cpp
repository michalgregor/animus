#include "PrgReturnFunctor.h"

namespace algotree {

/**
* Boost serialization function.
**/

void PrgReturnFunctor::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _args;
}

void PrgReturnFunctor::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _args;
}

//! Returns what type the process() method returns. This is actually an
//! identifier (got by mapping through Type2Item) of the internal type
//! contained in class Value.
TypeID PrgReturnFunctor::getReturnType() const {
	return _args.back();
}

//! The main processing function. It is called from a Node. Types of values
//! passed in args must be in accordance with the getArgTypes() vector.
AlgoType PrgReturnFunctor::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	if(_args.size() != children.size()) throw TracedError_InvalidArgument("Number of arguments is not as prescribed.");
	TypeID voidType = systematic::type_id<void>();
	for(unsigned int i = 0; i < children.size() - 1; i++) {
		AlgoType val = children[i].process(context);
		if(!val.convertible(voidType)) throw TracedError_InvalidArgument("Type mismatch.");
	}
	AlgoType val = children.back().process(context);
	if(!val.convertible(_args.back())) throw TracedError_InvalidArgument("Type mismatch.");

	return val;
}

//! Returns the number of arguments expected - that is length of args vector
//! passed to process().
unsigned int PrgReturnFunctor::getNumArgs() const {
	return _args.size();
}

//! Returns what what types should the the args vector passed to process()
//! contain (see documentation of process()).
const TypeList& PrgReturnFunctor::getArgTypes() const {
	return _args;
}

//! Returns an s-expression describing the NodeFunctor. This is usually the
//! class name, but e.g. for constants it could be something like "Const(14.5f)".
std::string PrgReturnFunctor::getSExpression() const {
	return "PRG_RETURN";
}

/**
 * Default constructor. For use in serialization.
 */

PrgReturnFunctor::PrgReturnFunctor(): _args() {}

/**
* Constructor.
* @param returnType The return type that the functor should have.
* @param numVoidArgs  Number of void arguments that the functor will take.
**/

PrgReturnFunctor::PrgReturnFunctor(TypeID returnType, unsigned int numVoidArgs):
_args() {
	_args.reserve(numVoidArgs + 1);
	_args.resize(numVoidArgs, systematic::type_id<void>());
	_args.push_back(returnType);
}

} //namespace algotree
