#ifndef PRGFUNCTOR_H_INCLUDED
#define PRGFUNCTOR_H_INCLUDED

#include "NodeFunctor.h"

namespace algotree {

/**
* @class PrgFunctor
* Takes in a preset number of void arguments. The return type of this is void.
**/
class ALGOTREE_API PrgFunctor: public NodeFunctor {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The list of argument types.
	TypeList _args;

public:
	virtual TypeID getReturnType() const;

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	virtual unsigned int getNumArgs() const;
	virtual const TypeList& getArgTypes() const;
	virtual std::string getSExpression() const;

	void setNumArgs(unsigned int num);

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	PrgFunctor();
	PrgFunctor(unsigned int numArgs);
	virtual ~PrgFunctor() {}
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::PrgFunctor)

#endif // PRGFUNCTOR_H_INCLUDED
