#include "ConstFunctor.h"

namespace algotree {

/**
 * Virtual destructor.
 */

ConstFunctor<void>::~ConstFunctor() {
	SYS_EXPORT_INSTANCE()
}

} //namespace algotree
