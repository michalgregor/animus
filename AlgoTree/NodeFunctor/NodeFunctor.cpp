#include "NodeFunctor.h"

namespace algotree {

/**
* Boost serialization function.
**/

void NodeFunctor::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<systematic::RefCounterVirtual>(*this);
}

void NodeFunctor::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<systematic::RefCounterVirtual>(*this);
}

/**
* A utility function that can be used to call process for all children nodes and
* assemble the results storing them in a single vector of arguments which is
* then returned.
**/

std::vector<AlgoType> NodeFunctor::assembleArgs(
	ProcessContext& context,
	const std::vector<Node>& children
) const {
	const TypeList& types = getArgTypes();
	unsigned int sz(children.size());

	if(types.size() != children.size()) {
		std::cerr << types.size() << std::endl;
		std::cerr << children.size() << std::endl;

		throw TracedError_InvalidArgument("ArgTypes and children node count mismatch.");
	}


	std::vector<AlgoType> args; args.reserve(sz);

	for(unsigned int i = 0; i < sz; i++) {
		context.execGuard().guard();
		args.push_back(children[i].process(context));
	}

	return args;
}

/**
* Empty body of the pure virtual destructor.
**/

NodeFunctor::~NodeFunctor() {}

} //namespace algotree
