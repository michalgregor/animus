#ifndef FORMALPARAMETER_H_INCLUDED
#define FORMALPARAMETER_H_INCLUDED

#include "../system.h"
#include "../BlockHandle/BlockHandle_Argument.h"
#include "LeafFunctor.h"

namespace algotree {

/**
* @class FormalParameter
* The process() method returns value that the associated argument (passed into
* constructor) in the context has.
**/
class ALGOTREE_API FormalParameter: public LeafFunctor {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! BlockHandle_Argument.
	BlockHandle_Argument _arg;

public:
	virtual TypeID getReturnType() const;

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	virtual std::string getSExpression() const;

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	FormalParameter();
	FormalParameter(BlockHandle_Argument arg);
	virtual ~FormalParameter() {}
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::FormalParameter)

#endif // FORMALPARAMETER_H_INCLUDED
