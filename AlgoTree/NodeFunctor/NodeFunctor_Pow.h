#ifndef NODEFUNCTOR_POW_H_
#define NODEFUNCTOR_POW_H_

#include "../system.h"
#include "NodeFunctor.h"
#include <Systematic/type_info.h>

namespace algotree {

using systematic::TypeID;

/**
* @class NodeFunctor_Pow
* @author Michal Gregor
* A NodeFunctor used to raise argument 0 to the power of argument 2 (pow function).
* @tparam Type The type of values.
**/

template<class Type>
class NodeFunctor_Pow: public NodeFunctor {
private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
    	ar & boost::serialization::base_object<NodeFunctor>(*this);
    	ar & _argTypes;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Vector of argument types.
	TypeList _argTypes;

public:
	virtual TypeID getReturnType() const;

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	virtual unsigned int getNumArgs() const;
	virtual const TypeList& getArgTypes() const;

	virtual std::string getSExpression() const {
		return "Pow";
	}

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	NodeFunctor_Pow& operator=(const NodeFunctor_Pow& obj) {
		_argTypes = obj._argTypes;
		return *this;
	}

	NodeFunctor_Pow(const NodeFunctor_Pow& obj): _argTypes(obj._argTypes) {}

	explicit NodeFunctor_Pow();
	virtual ~NodeFunctor_Pow() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Returns what type the process() method returns. This is actually an
* identifier (got by mapping through TypeInfo) of the internal type
* contained in class Value.
**/

template<class Type>
TypeID NodeFunctor_Pow<Type>::getReturnType() const {
	return systematic::type_id<Type>();
}

/**
* BlockHandle_Arguments must be of type Type (internal type contained in Value). This method
* computes their sum and returns the result as Value.
**/

template<class Type>
AlgoType NodeFunctor_Pow<Type>::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	std::vector<AlgoType> args = assembleArgs(context, children);
	unsigned int size = _argTypes.size();
	if(args.size() != size) TracedError_InvalidArgument("Wrong number of arguments.");
	AlgoType val; val.setData(Type(pow(args[0].getData<Type>(), args[1].getData<Type>())));
	return val;
}

/**
* Returns the number of arguments expected - that is length of args vector
* passed to process().
**/

template<class Type>
unsigned int NodeFunctor_Pow<Type>::getNumArgs() const {
	return _argTypes.size();
}

/**
* Returns what what types should the the args vector passed to process()
* contain (see documentation of process()).
**/

template<class Type>
const TypeList& NodeFunctor_Pow<Type>::getArgTypes() const {
	return _argTypes;
}

/**
* Constructor.
**/

template<class Type>
NodeFunctor_Pow<Type>::NodeFunctor_Pow(): _argTypes(2, systematic::type_id<Type>()) {}

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::NodeFunctor_Pow, 1)

#endif /* NODEFUNCTOR_POW_H_ */
