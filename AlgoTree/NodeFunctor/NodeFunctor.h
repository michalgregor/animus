#ifndef NODEFUNCTOR_H_INCLUDED
#define NODEFUNCTOR_H_INCLUDED

#include <boost/intrusive_ptr.hpp>
#include <vector>

#include <Systematic/utils/RefCounterVirtual.h>

#include "../system.h"
#include "../AlgoType.h"
#include "../Node.h"
#include "../Context/ProcessContext.h"
#include "../TypeList.h"

namespace algotree {

/**
* @class NodeFunctor
* @author Michal Gregor
* Node functor base class. Node functors are assigned to Nodes and act as their
* computation cores (Nodes provide the arguments to process() and collect the
* result).
* @tparam TypeID A template-based type id provider. See AlgoTypeID for
* an instance.
**/

class ALGOTREE_API NodeFunctor: public systematic::RefCounterVirtual {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

protected:
	std::vector<AlgoType> assembleArgs(
		ProcessContext& context,
		const std::vector<Node>& children
	) const;

public:
	//! Returns what type the process() method returns. This is actually an
	//! identifier (got by mapping through Type2Item) of the internal type
	//! contained in class Value.
	virtual systematic::TypeID getReturnType() const = 0;

	//! The main processing function. It is called from a Node. Types of values
	//! passed in args must be in accordance with the getArgTypes() vector.
	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* treeContext,
		const std::vector<Node>& children
	) const = 0;

	//! Returns the number of arguments expected - that is length of args vector
	//! passed to process().
	virtual unsigned int getNumArgs() const = 0;

	//! Returns what what types should the the args vector passed to process()
	//! contain (see documentation of process()).
	virtual const TypeList& getArgTypes() const = 0;

	//! Returns an s-expression describing the NodeFunctor. This is usually the
	//! class name, but e.g. for constants it could be something like "Const(14.5f)".
	virtual std::string getSExpression() const = 0;

	/**
	* This function is called when moving Nodes between trees. If the functor
	* stores any data in TreeContext this method should move the data to the
	* TreeContext of the new tree.
	* @param strictCopy Specifies whether to create a strict copy (true) or whether
	* we can remap to existing data even though it is not identical. This concept
	* is used by Modules - when moving them in the course of copying a Tree
	* a strict copy has to be created. When moving them in the course of crossover,
	* they can be remaped to other related Modules.
	**/

	virtual void moveData(
		TreeContext* UNUSED(oldTreeContext),
		TreeContext* UNUSED(newTreeContext),
		bool UNUSED(strictCopy)
	) {}

	/**
	* When this method is called, the functor should delete all content that it
	* stores in Tree's context like Modules, etc. (this is of course not true
	* of data that is shared with other NodeFunctors in which case the data
	* should be removed by the last NodeFunctor).
	*/
	virtual void clearData(TreeContext* treeContext) = 0;

	NodeFunctor& operator=(const NodeFunctor& functor) {
		systematic::RefCounterVirtual::operator=(functor);
		return *this;
	}

	NodeFunctor(const NodeFunctor& functor):
		systematic::RefCounterVirtual(functor) {}

	NodeFunctor(): systematic::RefCounterVirtual() {}

	virtual ~NodeFunctor() = 0;
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::NodeFunctor)

#endif // NODEFUNCTOR_H_INCLUDED
