#include "AndFunctor.h"

namespace algotree {

/**
* Boost serialization function.
**/

void AndFunctor::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _argTypes;
}

void AndFunctor::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _argTypes;
}

AlgoType AndFunctor::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	if(children.at(0).process(context).getData<bool>() && children.at(1).process(context).getData<bool>()) {
		return AlgoType(true);
	} else {
		return AlgoType(false);
	}
}

std::string AndFunctor::getSExpression() const {
	return "AND";
}

AndFunctor::AndFunctor(): _argTypes() {
	_argTypes.reserve(2);
	_argTypes.push_back(systematic::type_id<bool>());
	_argTypes.push_back(systematic::type_id<bool>());
}

} //namespace algotree
