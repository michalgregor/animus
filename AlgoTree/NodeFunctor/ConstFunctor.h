#ifndef CONSTFUNCTOR_H_INCLUDED
#define CONSTFUNCTOR_H_INCLUDED

#include "LeafFunctor.h"
#include "../system.h"
#include <Systematic/type_info.h>
#include <Systematic/utils/ToString.h>
#include <type_traits>

namespace algotree {

/**
* @class ConstFunctor
* Stores a constant value of a certain type and returns it in a AlgoType.
* The type is specified upon construction and then fixed.
**/

template<class Type>
class ConstFunctor: public LeafFunctor {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<LeafFunctor>(*this);
		ar & _constant;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Stores the constant.
	AlgoType _constant;

protected:
	//! For use in serialization.
	ConstFunctor(): _constant(0) {}

public:
	AlgoType getConstant();
	void setConstant(AlgoType constant);

	virtual TypeID getReturnType() const {
		return systematic::type_id<Type>();
	}

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	virtual std::string getSExpression() const;

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	ConstFunctor<Type>& operator=(const ConstFunctor<Type>& functor);
	ConstFunctor(const ConstFunctor<Type>& functor);

	explicit ConstFunctor(const Type& constant);

	virtual ~ConstFunctor() {
		SYS_EXPORT_INSTANCE()
	}
};

namespace detail {

	/**
	* Implementation of getSExpression() for ConstFunctor.
	**/

	template<class Type, class Enable = void>
	struct Sexp {
		static std::string getSExpression(const AlgoType& UNUSED(constant)) {
			return systematic::type_id<Type>().name();
		}

		static std::string getSExpression() {
			return systematic::type_id<Type>().name();
		}
	};

	/**
	* Implementation of getSExpression() for ConstFunctor with a fundamental type.
	**/

	template<class Type>
	struct Sexp<Type, typename std::enable_if<
		(std::is_fundamental<Type>::value and !std::is_same<void, Type>::value) or std::is_enum<Type>::value
	>::type> {
		static std::string getSExpression(const AlgoType& constant) {
			return std::string(systematic::type_id<Type>().name()) + "(" + systematic::ToString(constant.getData<Type>()) + ")";
		}
	};

} //namespace detail

/**
* Returns the stored constant.
**/

template<class Type>
AlgoType ConstFunctor<Type>::getConstant() {
	return _constant;
}

/**
* Converts the specified constant to the appropriate type and stores it. If not
* convertible an exception is thrown.
**/

template<class Type>
void ConstFunctor<Type>::setConstant(AlgoType constant) {
	if(getReturnType() != constant.getType()) throw TracedError_InvalidArgument("Constant of a different type expected.");
	_constant = constant;
}

//! The main processing function. It is called from a Node. Types of values
//! passed in args must be in accordance with the getArgTypes() vector.

template<class Type>
AlgoType ConstFunctor<Type>::process(
	ProcessContext& UNUSED(context),
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& UNUSED(children)
) const {
	return _constant;
}

//! Returns an s-expression describing the NodeFunctor. This is usually the
//! class name, but e.g. for constants it could be something like "Const(14.5f)".

template<class Type>
std::string ConstFunctor<Type>::getSExpression() const {
	return detail::Sexp<Type>::getSExpression(_constant);
}

/**
* Assignment operator. Creates a deep copy of the AlgoType as it would
* happen anyway on the first access.
**/

template<class Type>
ConstFunctor<Type>& ConstFunctor<Type>::operator=(const ConstFunctor<Type>& functor) {
	if(this != &functor) {
		LeafFunctor::operator=(functor);
		_constant = functor._constant.copy();
	}
	return *this;
}

/**
* Copy Contructor. Creates a deep copy of the AlgoType as it would
* happen anyway on the first access.
**/

template<class Type>
ConstFunctor<Type>::ConstFunctor(const ConstFunctor<Type>& functor):
LeafFunctor(functor), _constant(functor._constant.copy()) {}

/**
* Constructor.
* @param constant The constant to be stored and returned by process().
**/

template<class Type>
ConstFunctor<Type>::ConstFunctor(const Type& constant):
	_constant(constant) {}

//------------------------------Specialization for void-------------------------

template<>
class ConstFunctor<void>: public LeafFunctor {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<LeafFunctor>(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	AlgoType getConstant() {
		return AlgoType();
	}

	void setConstant(AlgoType constant) {
		if(constant.getType() != systematic::type_id<void>()) throw systematic::TracedError_InvalidArgument("Constant is not of type void.");
	}

	virtual TypeID getReturnType() const {
		return systematic::type_id<void>();
	}

	virtual AlgoType process(
		ProcessContext& UNUSED(context),
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& UNUSED(children)
	) const {
		return AlgoType();
	}

	virtual std::string getSExpression() const {
		return detail::Sexp<void>::getSExpression();
	}

	ConstFunctor<void>& operator=(const ConstFunctor<void>& functor) {
		LeafFunctor::operator=(functor);
		return *this;
	}

	ConstFunctor(const ConstFunctor<void>& UNUSED(functor)) {}

	ConstFunctor() {}
	virtual ~ConstFunctor();
};

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::ConstFunctor, 1)

#endif // CONSTFUNCTOR_H_INCLUDED
