#ifndef AlgoTree_NodeFunctor_BlockConstant_H_
#define AlgoTree_NodeFunctor_BlockConstant_H_

#include "../system.h"
#include "LeafFunctor.h"
#include "../BlockHandle/BlockHandle_Constant.h"
#include <Systematic/utils/StorageToken.h>
#include <Systematic/utils/ToString.h>

namespace algotree {

/**
* @class NodeFunctor_BlockConstant
* @author Michal Gregor
*
* A functor that returns constants retrieved from ContextBlock_Constant using
* BlockHandle_Constant.
*
* @tparam Type Type of the constant.
**/

template<class Type>
class NodeFunctor_BlockConstant: public LeafFunctor {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<LeafFunctor>(*this);
		ar & _handle;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Handle to the constant stored in ContextBlock_Constant.
	BlockHandle_Constant<Type> _handle;

public:
	//! Returns the underlying BlockHandle_Constant.
	const BlockHandle_Constant<Type>& getConstantHandle() const {
		return _handle;
	}

	//! Sets the underlying BlockHandle_Constant.
	void setConstantHandle(const BlockHandle_Constant<Type>& handle) const {
		_handle = handle;
	}

	//! Returns what type the process() method returns. This is actually an
	//! identifier (got by mapping through Type2Item) of the internal type
	//! contained in class Value.
	virtual systematic::TypeID getReturnType() const {
		return systematic::type_id<Type>();
	}

	//! Returns an s-expression describing the NodeFunctor. This is usually the
	//! class name, but e.g. for constants it could be something like "Const(14.5f)".
	virtual std::string getSExpression() const {
		return "BlockConstant<" + systematic::type_id<Type>().name() + ">(" + systematic::ToString(_handle.getStorageToken()) + ")";
	}

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* treeContext,
		const std::vector<Node>& children
	) const;

	NodeFunctor_BlockConstant<Type>& operator=(const NodeFunctor_BlockConstant<Type>& obj);
	NodeFunctor_BlockConstant(const NodeFunctor_BlockConstant<Type>& obj);

	NodeFunctor_BlockConstant(BlockHandle_Constant<Type> handle = BlockHandle_Constant<Type>());
	virtual ~NodeFunctor_BlockConstant();
};

/**
* The main processing function. It is called from a Node. Types of values
* passed in args must be in accordance with the getArgTypes() vector.
*/

template<class Type>
AlgoType NodeFunctor_BlockConstant<Type>::process(
	ProcessContext& UNUSED(context),
	TreeContext* treeContext,
	const std::vector<Node>& UNUSED(children)
) const {
	return AlgoType(_handle(treeContext));
}

/**
* Assignment operator.
**/

template<class Type>
NodeFunctor_BlockConstant<Type>& NodeFunctor_BlockConstant<Type>::operator=(const NodeFunctor_BlockConstant<Type>& obj) {
	if(&obj != this) {
		LeafFunctor::operator=(obj);
		_handle = obj._handle;
	}

	return *this;
}

/**
* Copy constructor.
**/

template<class Type>
NodeFunctor_BlockConstant<Type>::NodeFunctor_BlockConstant(const NodeFunctor_BlockConstant<Type>& obj):
	LeafFunctor(obj), _handle(obj._handle) {}

/**
* Constructor.
**/

template<class Type>
NodeFunctor_BlockConstant<Type>::
NodeFunctor_BlockConstant(BlockHandle_Constant<Type> handle): _handle(handle) {}

/**
 * Destructor.
 */

template<class Type>
NodeFunctor_BlockConstant<Type>::~NodeFunctor_BlockConstant() {
	SYS_EXPORT_INSTANCE();
}

}//namespace algotree

SYS_EXPORT_TEMPLATE(algotree::NodeFunctor_BlockConstant, 1)

#endif //AlgoTree_NodeFunctor_BlockConstant_H_
