#ifndef OSTREAMFUNCTOR_H_INCLUDED
#define OSTREAMFUNCTOR_H_INCLUDED

#include "../system.h"
#include "NodeFunctor.h"
#include <Systematic/type_info.h>
#include <ostream>

namespace algotree {

/**
* @class OstreamFunctor
* A functor that writes a value of certain type into the provided stream
* (cout by default).
**/

template<class Type>
class OstreamFunctor: public NodeFunctor {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<NodeFunctor>(*this);
		ar & _out;
		ar & _argTypes;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//!Ostream pointer.
	std::ostream* _out;
	//! Vector of argument types.
	TypeList _argTypes;

public:
	virtual TypeID getReturnType() const {
		return systematic::type_id<void>();
	}

	virtual AlgoType process(
		ProcessContext& context,
		const std::vector<Node>& children
	) const;

	virtual unsigned int getNumArgs() const {return 1;}
	virtual const TypeList& getArgTypes() const {return _argTypes;}

	//! Returns an s-expression describing the NodeFunctor. This is usually the
	//! class name, but e.g. for constants it could be something like "Const(14.5f)".
	virtual std::string getSExpression() const {
		return "Ostream";
	}

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	OstreamFunctor(std::ostream* out = &std::cout): _out(out),
		_argTypes(1, systematic::type_id<Type>()) {}

	virtual ~OstreamFunctor() {
		SYS_EXPORT_INSTANCE()
	}
};

template<class Type>
AlgoType OstreamFunctor<Type>::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	std::vector<AlgoType> args = assembleArgs(context, children);
	*_out << args.at(0).getData<Type>() << std::endl;
	return AlgoType();
}

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::OstreamFunctor, 1)

#endif // OSTREAMFUNCTOR_H_INCLUDED
