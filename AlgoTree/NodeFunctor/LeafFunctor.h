#ifndef LEAFFUNCTOR_H_INCLUDED
#define LEAFFUNCTOR_H_INCLUDED

#include "NodeFunctor.h"

namespace algotree {

/**
* @class LeafFunctor
* A common base class for NodeFunctors that are leaves (terminals). Provides
* a common implementation of getNumArgs(), which is always 0 and for
* getArgTypes() which always returns an empty vector.
**/
class ALGOTREE_API LeafFunctor: public NodeFunctor {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	static TypeList _argTypes;

public:
	//! Returns what type the process() method returns. This is actually an
	//! identifier (got by mapping through Type2Item) of the internal type
	//! contained in class Value.
	virtual systematic::TypeID getReturnType() const = 0;

	//! The main processing function. It is called from a Node. Types of values
	//! passed in args must be in accordance with the getArgTypes() vector.
	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* treeContext,
		const std::vector<Node>& children
	) const = 0;

	//! Returns an s-expression describing the NodeFunctor. This is usually the
	//! class name, but e.g. for constants it could be something like "Const(14.5f)".
	virtual std::string getSExpression() const = 0;

	//! Returns the number of arguments expected - that is length of args vector
	//! passed to process().
	virtual unsigned int getNumArgs() const {
		return 0;
	}

	//! Returns what what types should the the args vector passed to process()
	//! contain (see documentation of process()).
	virtual const TypeList& getArgTypes() const {
		return _argTypes;
	}

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	virtual ~LeafFunctor() {}
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::LeafFunctor)

#endif // LEAFFUNCTOR_H_INCLUDED
