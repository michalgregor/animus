#include "IfFunctor.h"

namespace algotree {

/**
* Boost serialization function.
**/

void IfFunctor::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _argTypes;
}

void IfFunctor::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _argTypes;
}

/**
* Evaluates the condition. If true, calls process() on the first child node,
* if false calls process() on the second child node.
**/

AlgoType IfFunctor::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	if(children.at(0).process(context).getData<bool>()) return children.at(1).process(context);
	else return children.at(2).process(context);
}

//! Returns an s-expression describing the NodeFunctor. This is usually the
//! class name, but e.g. for constants it could be something like "Const(14.5f)".
std::string IfFunctor::getSExpression() const {
	return "IF";
}

/**
* Constructor.
**/

IfFunctor::IfFunctor(): _argTypes() {
	_argTypes.reserve(3);
	_argTypes.push_back(systematic::type_id<bool>());
	_argTypes.push_back(systematic::type_id<void>());
	_argTypes.push_back(systematic::type_id<void>());
}

} //namespace algotree
