#ifndef COMPAREFUNCTOR_H_INCLUDED
#define COMPAREFUNCTOR_H_INCLUDED

#include "NodeFunctor.h"
#include <Systematic/math/Compare.h>

namespace algotree {

/**
* @class CompareFunctor
* This functor implements operator== for use in AlgoTrees. The Type in question
* obviously has to have operator== defined for this to compile, or alternatively
* a specialization of systematic::Copmare must be available for that type.
**/

template<class Type>
class CompareFunctor: public NodeFunctor {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<NodeFunctor>(*this);
		ar & _argTypes;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	TypeList _argTypes;

public:
	//! Returns what type the process() method returns. This is actually an
	//! identifier (got by mapping through Type2Item) of the internal type
	//! contained in class Value.
	virtual TypeID getReturnType() const {
		return systematic::type_id<bool>();
	}

	//! The main processing function. It is called from a Node. Types of values
	//! passed in args must be in accordance with the getArgTypes() vector.
	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const {
		return AlgoType(Compare(children.at(0).process(context).getData<Type>(), children.at(1).process(context).getData<Type>()));
	}

	//! Returns the number of arguments expected - that is length of args vector
	//! passed to process().
	virtual unsigned int getNumArgs() const {
		return _argTypes.size();
	}

	//! Returns what what types should the the args vector passed to process()
	//! contain (see documentation of process()).
	virtual const TypeList& getArgTypes() const {
		return _argTypes;
	}

	//! Returns an s-expression describing the NodeFunctor. This is usually the
	//! class name, but e.g. for constants it could be something like "Const(14.5f)".
	virtual std::string getSExpression() const {
		return "Equals";
	}

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	CompareFunctor(): _argTypes(2, systematic::type_id<Type>()) {}

	virtual ~CompareFunctor() {
		SYS_EXPORT_INSTANCE()
	}
};

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::CompareFunctor, 1)

#endif // COMPAREFUNCTOR_H_INCLUDED
