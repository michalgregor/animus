#ifndef NODEFUNCTORALGOTRAIT_H_
#define NODEFUNCTORALGOTRAIT_H_

#include "../system.h"
#include "NodeFunctor.h"

namespace algotree {

/**
* @class NodeFunctor_AlgoTrait
* @author Michal Gregor
* Takes in a NodeFunctor and rewraps its return value as an AlgoTraitType.
* It is required of course that the return type of the NodeFunctor be the same
* as AlgoTraitType::value_type.
* @tparam AlgoTraitType Type of the AlgoTrait.
**/

template<class AlgoTraitType>
class NodeFunctor_AlgoTrait: public NodeFunctor {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<NodeFunctor>(*this);
		ar & _nodeFunctor;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The contained NodeFunctor.
	intrusive_ptr<NodeFunctor> _nodeFunctor;

public:
	virtual systematic::TypeID getReturnType() const;

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* treeContext,
		const std::vector<Node>& children
	) const;

	virtual unsigned int getNumArgs() const;
	virtual const TypeList& getArgTypes() const;
	virtual std::string getSExpression() const;

	virtual void moveData(
		TreeContext* oldTreeContext,
		TreeContext* newTreeContext,
		bool strictCopy
	);

	virtual void clearData(TreeContext* treeContext);

	NodeFunctor_AlgoTrait<AlgoTraitType>& operator=(const NodeFunctor_AlgoTrait<AlgoTraitType>& obj);
	NodeFunctor_AlgoTrait(const NodeFunctor_AlgoTrait<AlgoTraitType>& obj);

	NodeFunctor_AlgoTrait(const intrusive_ptr<NodeFunctor>& nodeFunctor = intrusive_ptr<NodeFunctor>());
	virtual ~NodeFunctor_AlgoTrait() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Returns what type the process() method returns. This is actually an
* identifier (got by mapping through Type2Item) of the internal type
* contained in class Value.
*/

template<class AlgoTraitType>
systematic::TypeID NodeFunctor_AlgoTrait<AlgoTraitType>::getReturnType() const {
	return systematic::type_id<AlgoTraitType>();
}

/**
* The main processing function. It is called from Node. Types of values
* passed in args must be in accordance with the getArgTypes() vector.
*/

template<class AlgoTraitType>
AlgoType NodeFunctor_AlgoTrait<AlgoTraitType>::process(
	ProcessContext& context,
	TreeContext* treeContext,
	const std::vector<Node>& children
) const {
	AlgoType data(_nodeFunctor->process(context, treeContext, children));
	data = AlgoTraitType(data.getData<typename AlgoTraitType::value_type>());
	return data;
}

/**
* Returns the number of arguments expected - that is length of args vector
* passed to process().
*/

template<class AlgoTraitType>
unsigned int NodeFunctor_AlgoTrait<AlgoTraitType>::getNumArgs() const {
	return _nodeFunctor->getNumArgs();
}

/**
* Returns what what types should the the args vector passed to process()
* contain (see documentation of process()).
*/

template<class AlgoTraitType>
const TypeList& NodeFunctor_AlgoTrait<AlgoTraitType>::getArgTypes() const {
	return _nodeFunctor->getArgTypes();
}

/**
* Returns an s-expression describing the NodeFunctor. This is usually the
* class name, but e.g. for constants it could be something like "Const(14.5f)".
*/

template<class AlgoTraitType>
std::string NodeFunctor_AlgoTrait<AlgoTraitType>::getSExpression() const {
	return _nodeFunctor->getSExpression();
}

/**
* This function is called when moving Nodes between trees. If the functor
* stores any data in TreeContext this method should move the data to the
* TreeContext of the new tree.
* @param strictCopy Specifies whether to create a strict copy (true) or whether
* we can remap to existing data even though it is not identical. This concept
* is used by Modules - when moving them in the course of copying a Tree
* a strict copy has to be created. When moving them in the course of crossover,
* they can be remaped to other related Modules.
**/

template<class AlgoTraitType>
void NodeFunctor_AlgoTrait<AlgoTraitType>::moveData(
	TreeContext* oldTreeContext,
	TreeContext* newTreeContext,
	bool strictCopy
) {
	_nodeFunctor->moveData(oldTreeContext, newTreeContext, strictCopy);
}

/**
* When this method is called, the functor should delete all content that
* stores in Tree's context like Modules, etc. (this is of course not true
* of data that is stored with other NodeFunctors in which case the data
* should be removed by the last NodeFunctor).
*/

template<class AlgoTraitType>
void NodeFunctor_AlgoTrait<AlgoTraitType>::clearData(TreeContext* treeContext) {
	_nodeFunctor->clearData(treeContext);
}

/**
* Assignment operator.
**/

template<class AlgoTraitType>
NodeFunctor_AlgoTrait<AlgoTraitType>& NodeFunctor_AlgoTrait<AlgoTraitType>::operator=(const NodeFunctor_AlgoTrait<AlgoTraitType>& obj) {
	if(&obj != this) {
		NodeFunctor::operator=(obj);
		_nodeFunctor = clone(obj._nodeFunctor);
	}

	return *this;
}

/**
* Copy constructor.
**/

template<class AlgoTraitType>
NodeFunctor_AlgoTrait<AlgoTraitType>::NodeFunctor_AlgoTrait(const NodeFunctor_AlgoTrait<AlgoTraitType>& obj):
	NodeFunctor(obj), _nodeFunctor(clone(obj._nodeFunctor))
{}

/**
* Constructor.
* @param nodeFunctor The contained NodeFunctor. This argument takes a default
* value of a NULL pointer so as to provide a default constructor for purposes
* of serialization and similar. However, unless the pointer is initialized
* through other means, the object is unusable.
**/

template<class AlgoTraitType>
NodeFunctor_AlgoTrait<AlgoTraitType>::NodeFunctor_AlgoTrait(
	const intrusive_ptr<NodeFunctor>& nodeFunctor
): _nodeFunctor(nodeFunctor) {}

}//namespace algotree

SYS_EXPORT_TEMPLATE(algotree::NodeFunctor_AlgoTrait, 1)

#endif /* NODEFUNCTORALGOTRAIT_H_ */
