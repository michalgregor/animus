#ifndef PRGRETURNFUNCTOR_H_INCLUDED
#define PRGRETURNFUNCTOR_H_INCLUDED

#include "NodeFunctor.h"

namespace algotree {

/**
* @class PrgReturnFunctor
* Takes in a preset number of void arguments; the last argument is mandatory and
* is of a predefined type. The last argument is returned by the functor.
**/
class ALGOTREE_API PrgReturnFunctor: public NodeFunctor {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	PrgReturnFunctor();

private:
	//! The list of argument types.
	TypeList _args;

public:
	virtual TypeID getReturnType() const;

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	virtual unsigned int getNumArgs() const;
	virtual const TypeList& getArgTypes() const;
	virtual std::string getSExpression() const;

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	PrgReturnFunctor(TypeID returnType, unsigned int numVoidArgs);
	virtual ~PrgReturnFunctor() {}
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::PrgReturnFunctor)

#endif // PRGRETURNFUNCTOR_H_INCLUDED
