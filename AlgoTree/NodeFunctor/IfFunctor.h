#ifndef IFFUNCTOR_H_INCLUDED
#define IFFUNCTOR_H_INCLUDED

#include "../system.h"
#include "NodeFunctor.h"
#include <Systematic/type_info.h>

namespace algotree {

using systematic::TypeID;

/**
* @class IfFunctor
* @author Michal Gregor
* A node functor that contains three inputs - first of type bool containing the
* condition; second of type void that represents the action if the condition
* evaluates to true; and the third of type void, which corresponds to the action
* to take if false.
**/
class ALGOTREE_API IfFunctor: public NodeFunctor {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Vector of argument types.
	TypeList _argTypes;

public:
	virtual TypeID getReturnType() const {return systematic::type_id<void>();}

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	virtual unsigned int getNumArgs() const {return _argTypes.size();}
	virtual const TypeList& getArgTypes() const {return _argTypes;}

	virtual std::string getSExpression() const;

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	IfFunctor();
	virtual ~IfFunctor() {}
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::IfFunctor)

#endif // IFFUNCTOR_H_INCLUDED
