#ifndef ATOPERATOR_H_INCLUDED
#define ATOPERATOR_H_INCLUDED

#include "NodeFunctor.h"
#include <Systematic/type_info.h>

namespace algotree {

using systematic::type_id;

/**
* @class AtOperator
* This functor is used with random access containers. It allows the elements of
* the container to be referenced. It takes in two parameters - the container
* and the index of the element (as unsigned int).
**/

template<class Container, class Element>
class AtOperator: public NodeFunctor {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<NodeFunctor>(*this);
		ar & _argTypes;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Vector of argument types.
	TypeList _argTypes;

public:

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	//! Returns what type the process() method returns. This is actually an
	//! identifier (got by mapping through Type2Item) of the internal type
	//! contained in class Value.

	virtual TypeID getReturnType() const {return type_id<Element>();}

	//! Returns the number of arguments expected - that is length of args vector
	//! passed to process().
	virtual unsigned int getNumArgs() const {return _argTypes.size();}

	//! Returns what what types should the the args vector passed to process()
	//! contain (see documentation of process()).
	virtual const TypeList& getArgTypes() const {return _argTypes;}

	//! Returns an s-expression describing the NodeFunctor. This is usually the
	//! class name, but e.g. for constants it could be something like "Const(14.5f)".
	virtual std::string getSExpression() const {
		return "operator[]";
	}

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	AtOperator();

	virtual ~AtOperator() {
		SYS_EXPORT_INSTANCE();
	}
};

//! The main processing function. It is called from a Node. Types of values
//! passed in args must be in accordance with the getArgTypes() vector.

template<class Container, class Element>
AlgoType AtOperator<Container, Element>::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	std::vector<AlgoType> args = assembleArgs(context, children);
	const Container& container((const Container&)args.at(0));
	unsigned int index = args.at(1);
	//we apply modulo in order to map any overflowing indexes to the valid range
	index %= container.size();
	return AlgoType(container[index]);
}

/**
* Default onstructor.
**/

template<class Container, class Element>
AtOperator<Container, Element>::AtOperator(): _argTypes() {
	_argTypes.reserve(2);
	_argTypes.push_back(type_id<Container>());
	_argTypes.push_back(type_id<unsigned int>());
}

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::AtOperator, 2)

#endif // ATOPERATOR_H_INCLUDED
