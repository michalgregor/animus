#ifndef NODEFUNCTOR_MULTIPLY_H_INCLUDED
#define NODEFUNCTOR_MULTIPLY_H_INCLUDED

#include "../system.h"
#include "NodeFunctor.h"
#include <Systematic/type_info.h>

namespace algotree {

using systematic::TypeID;

/**
* @class NodeFunctor_Multiply
* @author Michal Gregor
* A NodeFunctor used to compute a product of several values of a defined type.
* @tparam Type The type of values.
**/

template<class Type>
class NodeFunctor_Multiply: public NodeFunctor {
private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
    	ar & boost::serialization::base_object<NodeFunctor>(*this);
    	ar & _argTypes;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Vector of argument types.
	TypeList _argTypes;

public:
	virtual TypeID getReturnType() const;

	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const;

	virtual unsigned int getNumArgs() const;
	virtual const TypeList& getArgTypes() const;

	virtual std::string getSExpression() const {
		return "Multiply";
	}

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	NodeFunctor_Multiply& operator=(const NodeFunctor_Multiply& obj) {
		_argTypes = obj._argTypes;
		return *this;
	}

	NodeFunctor_Multiply(const NodeFunctor_Multiply& obj): _argTypes(obj._argTypes) {}

	explicit NodeFunctor_Multiply(unsigned int numArgs = 2);
	virtual ~NodeFunctor_Multiply() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
* Returns what type the process() method returns. This is actually an
* identifier (got by mapping through TypeInfo) of the internal type
* contained in class Value.
**/

template<class Type>
TypeID NodeFunctor_Multiply<Type>::getReturnType() const {
	return systematic::type_id<Type>();
}

/**
* BlockHandle_Arguments must be of type Type (internal type contained in Value). This method
* computes their sum and returns the result as Value.
**/

template<class Type>
AlgoType NodeFunctor_Multiply<Type>::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& children
) const {
	std::vector<AlgoType> args = assembleArgs(context, children);
	unsigned int size = _argTypes.size();
	if(args.size() != size) TracedError_InvalidArgument("Wrong number of arguments.");
	Type sum = 1;

	for(unsigned int i = 0; i < size; i++) {
		sum *= args[i].getData<Type>();
	}

	AlgoType val; val.setData(sum);
	return val;
}

/**
* Returns the number of arguments expected - that is length of args vector
* passed to process().
**/

template<class Type>
unsigned int NodeFunctor_Multiply<Type>::getNumArgs() const {
	return _argTypes.size();
}

/**
* Returns what what types should the the args vector passed to process()
* contain (see documentation of process()).
**/

template<class Type>
const TypeList& NodeFunctor_Multiply<Type>::getArgTypes() const {
	return _argTypes;
}

/**
* Constructor.
* @param numArgs Number of arguments that process() expects - that is length
* of args vector passed to process().
**/

template<class Type>
NodeFunctor_Multiply<Type>::NodeFunctor_Multiply(unsigned int numArgs):
_argTypes(numArgs, systematic::type_id<Type>()) {}

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::NodeFunctor_Multiply, 1)

#endif // NODEFUNCTOR_MULTIPLY_H_INCLUDED
