#include "ModuleCaller.h"

namespace algotree {

/**
* Boost serialization function.
**/

void ModuleCaller::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _module;
	ar & _signature;
	ar & _id;
}

void ModuleCaller::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	ar & _module;
	ar & _signature;
	ar & _id;
}

TypeID ModuleCaller::getReturnType() const {
	return _signature.getReturnType();
}

AlgoType ModuleCaller::process(
	ProcessContext& context,
	TreeContext* treeContext,
	const std::vector<Node>& children
) const {
	std::vector<AlgoType> args(assembleArgs(context, children));
	ProcessContext local_context(context.newScope());
	local_context.arguments().swapInArgs(args);
	return _module(treeContext).process(local_context);
}

unsigned int ModuleCaller::getNumArgs() const {
	return _signature.getArgTypes().size();
}

const TypeList& ModuleCaller::getArgTypes() const {
	return _signature.getArgTypes();
}

std::string ModuleCaller::getSExpression() const {
	std::string sexp("ModuleCaller<");

	sexp += _signature.getReturnType().name() + ", ";

	for(unsigned int i = 0; i < _signature.getArgTypes().size(); i++) {
		sexp += _signature.getArgTypes()[i].name() + ", ";
	}

	sexp += ">";
	sexp += _id;

	return sexp;
}

/**
* This function is called when moving Nodes between trees. If the functor
* stores any data in TreeContext this method should move the data to the
* TreeContext of the new tree.
* @param strictCopy Specifies whether to create a strict copy (true) or whether
* we can remap to existing data even though it is not identical. This concept
* is used by Modules - when moving them in the course of copying a Tree
* a strict copy has to be created. When moving them in the course of crossover,
* they can be remaped to other related Modules.
**/

void ModuleCaller::moveData(TreeContext* oldContext, TreeContext* newContext, bool strictCopy) {
	_module.move(oldContext, newContext, strictCopy);
	_id = _module(newContext).getID();
}

/**
* When this method is called, the functor should delete all content that
* stores in Tree's context like Modules, etc. (this is of course not true
* of data that is shared with other NodeFunctors in which case the data
* should be removed by the last NodeFunctor).
*/
void ModuleCaller::clearData(TreeContext* treeContext) {
	_module.reset(treeContext);
}

/**
* Assignment operator.
**/

ModuleCaller& ModuleCaller::operator=(const ModuleCaller& obj) {
	if(&obj != this) {
		NodeFunctor::operator=(obj);
		_module = obj._module;
		_signature = obj._signature;
		_id = obj._id;
	}

	return *this;
}

/**
* Copy constructor.
**/

ModuleCaller::ModuleCaller(const ModuleCaller& obj):
	NodeFunctor(obj), _module(obj._module), _signature(obj._signature),
	_id(obj._id) {}

/**
* Constructor.
**/

ModuleCaller::ModuleCaller(): NodeFunctor(), _module(), _signature(), _id() {}

/**
* Constructor.
* @param tree The tree in which the ModuleCaller resides and into which the
* ModuleHandler has been added.
**/

ModuleCaller::ModuleCaller(TreeContext* treeContext, const ModuleHandler& module): _module(module),
_signature(), _id() {
	ModuleContent& mc(_module(treeContext));
	_signature = mc.getSignature();
	_id = mc.getID();
}

} //namespace algotree
