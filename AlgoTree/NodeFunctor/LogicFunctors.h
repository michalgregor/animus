#ifndef LOGICFUNCTORS_H_INCLUDED
#define LOGICFUNCTORS_H_INCLUDED

#include "AndFunctor.h"
#include "OrFunctor.h"
#include "NotFunctor.h"
#include "NandFunctor.h"
#include "NorFunctor.h"

#endif // LOGICFUNCTORS_H_INCLUDED
