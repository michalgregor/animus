#include "FormalParameter.h"
#include <Systematic/utils/ToString.h>
#include "../Context/ProcessContext.h"

namespace algotree {

/**
* Boost serialization function.
**/

void FormalParameter::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<LeafFunctor>(*this);
	ar & _arg;
}

void FormalParameter::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<LeafFunctor>(*this);
	ar & _arg;
}

/**
* Returns what type the process() method returns. This is actually an
* identifier (got by mapping through TypeID) of the internal type
* contained in class Value.
**/

TypeID FormalParameter::getReturnType() const {
	return _arg.getType();
}

/**
* Returns value of the associated argument from the context.
**/

AlgoType FormalParameter::process(
	ProcessContext& context,
	TreeContext* UNUSED(treeContext),
	const std::vector<Node>& UNUSED(children)
) const {
	return _arg.getValue(context);
}

//! Returns an s-expression describing the NodeFunctor. This is usually the
//! class name, but e.g. for constants it could be something like "Const(14.5f)".
std::string FormalParameter::getSExpression() const {
	return "Arg(" + ToString(_arg.getIndex()) + ")";
}

/**
 * Default constructor. For use in serialization.
 */

FormalParameter::FormalParameter(): _arg() {}

/**
* Constructor.
* @param arg BlockHandle_Argument that the formal parameter reads from the context.
**/

FormalParameter::FormalParameter(BlockHandle_Argument arg): _arg(arg) {}

} //namespace algotree
