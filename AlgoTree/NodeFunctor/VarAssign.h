#ifndef VARASSIGN_H_INCLUDED
#define VARASSIGN_H_INCLUDED

#include "NodeFunctor.h"
#include "../Variable.h"

namespace algotree {

/**
* @class VarAssign
* A functor that assigns value of the second argument to the variable provided
* by the first argument.
**/

template<class Type>
class VarAssign: public NodeFunctor {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<NodeFunctor>(*this);
		ar & _argTypes;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	TypeList _argTypes;

public:
	//! Returns what type the process() method returns. This is actually an
	//! identifier (got by mapping through Type2Item) of the internal type
	//! contained in class Value.
	virtual systematic::TypeID getReturnType() const {
		return systematic::type_id<void>();
	}

	//! The main processing function. It is called from Node. Types of values
	//! passed in args must be in accordance with the getArgTypes() vector.
	virtual AlgoType process(
		ProcessContext& context,
		TreeContext* UNUSED(treeContext),
		const std::vector<Node>& children
	) const {
		Variable<Type> var = children.at(0).process(context).getData<Variable<Type> >();
		var(context) = children.at(1).process(context).getData<Type>();
		return AlgoType();
	}

	//! Returns the number of arguments expected - that is length of args vector
	//! passed to process().
	virtual unsigned int getNumArgs() const {
		return _argTypes.size();
	}

	//! Returns what what types should the the args vector passed to process()
	//! contain (see documentation of process()).
	virtual const TypeList& getArgTypes() const {
		return _argTypes;
	}

	//! Returns an s-expression describing the NodeFunctor. This is usually the
	//! class name, but e.g. for constants it could be something like "Const(14.5f)".
	virtual std::string getSExpression() const {
		return "Assign";
	}

	virtual void clearData(TreeContext* UNUSED(treeContext)) {}

	VarAssign(): _argTypes() {
		_argTypes.reserve(2);
		_argTypes.push_back(systematic::type_id<Variable<Type> >());
		_argTypes.push_back(systematic::type_id<Type>());
	}

	virtual ~VarAssign() {
		SYS_EXPORT_INSTANCE()
	}
};

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::VarAssign, 1)

#endif // VARASSIGN_H_INCLUDED
