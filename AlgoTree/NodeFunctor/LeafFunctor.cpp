#include "LeafFunctor.h"

namespace algotree {

/**
* Boost serialization function.
**/

void LeafFunctor::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	//we do not serialize _argTypes, which is just a statically declared empty TypeList
}

void LeafFunctor::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NodeFunctor>(*this);
	//we do not serialize _argTypes, which is just a statically declared empty TypeList
}

TypeList LeafFunctor::_argTypes = std::vector<TypeID>();

} //namespace algotree
