#ifndef ALGOTIMEOUTBREAK_H_
#define ALGOTIMEOUTBREAK_H_

#include "AlgoBreak.h"

namespace algotree {

/**
* @class AlgoTimeoutBreak
* This object is thrown in order to stop process() of an AlgoTree once the
* predefined time interval, number of step, etc. has been reached.
**/
class ALGOTREE_API AlgoTimeoutBreak: public AlgoBreak {
public:
	AlgoTimeoutBreak(): AlgoBreak("AlgoTimeoutBreak") {}
	virtual ~AlgoTimeoutBreak() throw() {};
};

} //namespace algotree

#endif /* ALGOTIMEOUTBREAK_H_ */
