#ifndef ALGO_ALGOBREAK_H_INCLUDED
#define ALGO_ALGOBREAK_H_INCLUDED

#include <Systematic/exception/TracedError.h>
#include "../system.h"

namespace algotree {

/**
* @class AlgoBreak
* The base class of exceptions thrown from process() - especially by ExecutionGuard
* in order to signalize that the time limit has run out, the recursion limit has
* been exceeded, etc.
**/
class ALGOTREE_API AlgoBreak: public systematic::TracedError {
public:
	using std::runtime_error::what;

	AlgoBreak(std::string msg): TracedError(msg) {}
	virtual ~AlgoBreak() throw() {};
};

} //namespace algotree

#endif // ALGO_ALGOBREAK_H_INCLUDED
