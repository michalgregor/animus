#ifndef ALGOERROR_NOFUNCTORFOUND_H_
#define ALGOERROR_NOFUNCTORFOUND_H_

#include "../system.h"

namespace algotree {

/**
 * @class AlgoError_NoFunctorFound
 * @brief An exception class based on systematic::TracedError.
 * @author Michal Gregor
 */
class ALGOTREE_API AlgoError_NoFunctorFound: public systematic::TracedError {
public:
	AlgoError_NoFunctorFound(const std::string &__arg): TracedError(__arg) {}
	virtual ~AlgoError_NoFunctorFound() throw() {};
};

#define AlgoError_NoFunctorFound(arg) AlgoError_NoFunctorFound(std::string(_SYS_FUNC_SIG_) + ": " + arg)

} //namespace algotree

#endif /* ALGOERROR_NOFUNCTORFOUND_H_ */
