#ifndef ALGORECURSIONBREAK_H_
#define ALGORECURSIONBREAK_H_

#include "AlgoBreak.h"

namespace algotree {

/**
* @class AlgoRecursionBreak
* This object is thrown by ExecutionGuard when recursion limit is exceeded.
**/
class ALGOTREE_API AlgoRecursionBreak: public AlgoBreak {
public:
	AlgoRecursionBreak(): AlgoBreak("AlgoRecursionBreak") {}
	virtual ~AlgoRecursionBreak() throw() {};
};

} //namespace algotree

#endif /* ALGORECURSIONBREAK_H_ */
