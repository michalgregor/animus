#ifndef ALGO_TYPELIST_H_INCLUDED
#define ALGO_TYPELIST_H_INCLUDED

#include "system.h"
#include "BlockHandle/BlockHandle_Argument.h"

#include <vector>
#include <iterator>
#include <boost/type_traits/is_const.hpp>
#include <boost/type_traits/remove_reference.hpp>
#include <boost/type_traits/conditional.hpp>

namespace algotree {

SYS_WNONVIRT_DTOR_OFF
class ALGOTREE_API TypeList: private std::vector<systematic::TypeID> {
SYS_WNONVIRT_DTOR_ON
private:
	typedef std::vector<systematic::TypeID> RawList;

private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	template<class IterType>
	struct ProxyIterator_ValueType {
		typedef typename boost::conditional<
			boost::is_const<typename boost::remove_reference<
				typename IterType::reference>::type>::value,
			const BlockHandle_Argument,
			BlockHandle_Argument
		>::type value_type;
	};

	template<class RawList, class IterType>
	class ProxyIterator {
	public:
		typedef typename std::iterator_traits<IterType>::value_type value_type;
		typedef typename std::iterator_traits<IterType>::difference_type difference_type;
		typedef typename ProxyIterator_ValueType<IterType>::value_type pointer;
		typedef typename ProxyIterator_ValueType<IterType>::value_type reference;
		typedef std::random_access_iterator_tag  iterator_category;

	private:
		friend class TypeList;

		// Pointer to the underlying list. This is neccessary in order to obtain
		// subscripts using std::distance.
		RawList* _container;
		// An iterator of the underlying list.
		IterType _rawIter;
		//! Used to store a BlockHandle_Argument so that operator->() can be implemented.
		value_type* _handle;

	public:
		bool operator==(const ProxyIterator& iter) const {
			return _rawIter == iter._rawIter && _container == iter._container;
		}

		bool operator!=(const ProxyIterator& iter) const {
			return _rawIter != iter._rawIter || _container != iter._container;
		}

		//! The pointer points to a temporary object which is associated with
		//! the iterator and will be deleted upon its destruction.
		pointer operator->() const {
			if(!_container) throw TracedError_InvalidPointer("Trying to dereference a NULL iterator.");
			if(!_handle) _handle = new value_type(*_rawIter, std::distance(_container->begin(), _rawIter));
			else *_handle = value_type(*_rawIter, std::distance(_container->begin(), _rawIter));
			return _handle;
		}

		reference operator*() const {
			return reference(*_rawIter, std::distance(_container->begin(), _rawIter));
		}

		ProxyIterator& operator++() {
			++_rawIter;
			return *this;
		}

		ProxyIterator& operator--() {
			--_rawIter;
			return *this;
		}

		ProxyIterator operator++(int) {
			return ProxyIterator(_container, _rawIter++);
		}

		ProxyIterator operator--(int) {
			return ProxyIterator(_container, _rawIter--);
		}

		ProxyIterator operator+(difference_type dist) const {
			return ProxyIterator(_container, _rawIter + dist);
		}

		ProxyIterator operator-(difference_type dist) const {
			return ProxyIterator(_container, _rawIter - dist);
		}

		bool operator<(const ProxyIterator& iter) const {
			return _rawIter < iter._rawIter;
		}

		bool operator>(const ProxyIterator& iter) const {
			return _rawIter > iter._rawIter;
		}

		bool operator<=(const ProxyIterator& iter) const {
			return _rawIter <= iter._rawIter;
		}

		bool operator>=(const ProxyIterator& iter) const {
			return _rawIter >= iter._rawIter;
		}

		ProxyIterator& operator+=(difference_type dist) {
			_rawIter += dist;
			return *this;
		}

		ProxyIterator& operator-=(difference_type dist) {
			_rawIter -= dist;
			return *this;
		}

		BlockHandle_Argument operator[](difference_type index) const {
			return typename IterType::value_type(_rawIter[index], std::distance(_container->begin(), _rawIter) + index);
		}

		template<class OtherIterType>
		ProxyIterator& operator=(const ProxyIterator<RawList, OtherIterType>& iter) {
			if(this != &iter) {
				_container = iter._container;
				_rawIter = iter._rawIter;
				//we do not copy _handle
			}
			return *this;
		}

		template<class OtherIterType>
		ProxyIterator(const ProxyIterator<RawList, OtherIterType>& iter):
		_container(iter._container), _rawIter(iter._rawIter), _handle(NULL) {}

		ProxyIterator(
			RawList* container,
			const IterType& iter
		): _container(container), _rawIter(iter), _handle(NULL) {}

		ProxyIterator(): _container(0), _rawIter(), _handle(NULL) {}

		~ProxyIterator() {
			if(_handle) delete _handle;
		}
	};

public:

	/**
	* @class Handles
	* This class acts as a proxy – it provides an interface that allows access
	* to BlockHandle_Argument iterators instead of TypeID iterators as provided by TypeList
	* by default.
	**/

	class Handles {
	private:
		friend class boost::serialization::access;

		template<class Archive>
		void serialize(Archive& ar, const unsigned int UNUSED(version)) {
			ar & _vec;
		}

	private:
		RawList* _vec;

	private:
		friend class TypeList;

		Handles& operator=(const Handles&);

		Handles(): _vec() {}
		Handles(RawList* vec): _vec(vec) {}

	public:
		typedef ProxyIterator<RawList, RawList::iterator> iterator;
		typedef ProxyIterator<RawList, RawList::const_iterator> const_iterator;

		typedef std::reverse_iterator<iterator> reverse_iterator;
		typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

		typedef RawList::size_type size_type;
		typedef RawList::difference_type difference_type;

		typedef BlockHandle_Argument value_type;

	public:
		//! Returns an BlockHandle_Argument to the first argument in the container. Throws
		//! TracedError_OutOfRange if there are no arguments.
		value_type front() {
			if(!_vec->size()) throw TracedError_OutOfRange("There are no elements in the container.");
			return value_type(_vec->front(), 0);
		}

		//! Returns an BlockHandle_Argument to the last argument in the container. Throws
		//! TracedError_OutOfRange if there are no arguments.
		value_type back() {
			if(!_vec->size()) throw TracedError_OutOfRange("There are no elements in the container.");
			static_assert(sizeof(RawList::size_type) <= sizeof(BlockHandle_Argument::size_type), "sizeof(RawList::size_type) <= sizeof(BlockHandle_Argument::size_type)");
			return value_type(_vec->back(), static_cast<BlockHandle_Argument::size_type>(_vec->size() - 1));
		}

		//forward iterators

		iterator begin() {
			return iterator(_vec, _vec->begin());
		}

		iterator end() {
			return iterator(_vec, _vec->end());
		}

		const_iterator begin() const {
			return const_iterator(_vec, _vec->begin());
		}

		const_iterator end() const {
			return const_iterator(_vec, _vec->end());
		}

		//reverse iterators

		reverse_iterator rbegin() {
			return reverse_iterator(iterator(_vec, _vec->rbegin().base()));
		}

		reverse_iterator rend() {
			return reverse_iterator(iterator(_vec, _vec->rend().base()));
		}

		const_reverse_iterator rbegin() const {
			return const_reverse_iterator(const_iterator(_vec, _vec->rbegin().base()));
		}

		const_reverse_iterator rend() const {
			return const_reverse_iterator(const_iterator(_vec, _vec->rend().base()));
		}
	};

public:
	//import selected interfaces from std::vector
	using RawList::iterator;
	using RawList::const_iterator;
	using RawList::reverse_iterator;
	using RawList::const_reverse_iterator;

	using RawList::clear;
	using RawList::empty;
	using RawList::size;
	using RawList::max_size;
	using RawList::erase;
	using RawList::reserve;
	using RawList::resize;
	using RawList::capacity;

	using RawList::begin;
	using RawList::end;
	using RawList::rbegin;
	using RawList::rend;

	using RawList::front;
	using RawList::back;

	using RawList::size_type;
	using RawList::difference_type;
	using RawList::value_type;

	using RawList::assign;
	using RawList::insert;

	//! Implementation of erase for the Handles::iterator type.
	RawList::iterator erase(Handles::iterator iter) {
		return RawList::erase(iter._rawIter);
	}

	//! Implementation of erase for the Handles::iterator type.
	RawList::iterator erase(Handles::iterator first, Handles::iterator last) {
		return RawList::erase(first._rawIter, last._rawIter);
	}

	//! Wraps the at() member of the underlying container.
	systematic::TypeID& operator[](size_type index) {
		return RawList::at(index);
	}

	//! Wraps the at() member of the underlying container.
	const systematic::TypeID& operator[](size_type index) const {
		return RawList::at(index);
	}

	//! Exchanges contents with the specified TypeList.
	void swap(TypeList& typeList) {
		RawList::swap(typeList);
	}

public:
	//! The interface used to provide iterators to type BlockHandle_Argument.
	Handles handles;

	int compare(const TypeList& typeList) const;

	bool operator<(const TypeList& typeList) const;
	bool operator<=(const TypeList& typeList) const;
	bool operator>(const TypeList& typeList) const;
	bool operator>=(const TypeList& typeList) const;
	bool operator==(const TypeList& typeList) const;
	bool operator!=(const TypeList& typeList) const;

	BlockHandle_Argument addEntry(systematic::TypeID type);
	void push_back(systematic::TypeID type);

	template<class Type>
	BlockHandle_Argument addEntry();

	template<class Type>
	void push_back();

	BlockHandle_Argument handle(size_type index);
	systematic::TypeID& entry(size_type index);
	const systematic::TypeID& entry(size_type index) const;

	TypeList& operator=(const TypeList& typeList);

	TypeList(const TypeList& typeList);
	TypeList(const RawList& rawList);
	TypeList(unsigned int num, const systematic::TypeID& value);
	TypeList();
};

/**
* Adds an argument of of the specified type (Type) and returns the corresponding
* BlockHandle_Argument.
**/

template<class Type>
BlockHandle_Argument TypeList::addEntry() {
	RawList::push_back(systematic::type_id<Type>());
	return BlockHandle_Argument(systematic::type_id<Type>(), size() - 1);
}

/**
* Adds an argument of the specified type (Type).
**/

template<class Type>
void TypeList::push_back() {
	RawList::push_back(systematic::type_id<Type>());
}

} //namespace algotree

#endif // ALGO_TYPELIST_H_INCLUDED
