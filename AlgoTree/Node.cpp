#include "Node.h"
#include <Systematic/dynamic_typing/ConverterRegister.h>

#include "Traverser.h"
#include "Context/ProcessContext.h"
#include "NodeFunctor/NodeFunctor.h"

#include "Tree.h"

namespace algotree {

/**
* Boost serialization function.
**/

void Node::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _type;
	ar & _functor;
	ar & _parent;
	ar & _children;
	ar & _treeContext;
}

void Node::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _type;
	ar & _functor;
	ar & _parent;
	ar & _children;
	ar & _treeContext;
}

/**
* Sets pointer to the Tree that contains this Node. If setForChildren is true
* (default) the pointer is also set for every child Node.
*/

void Node::setTreeContext(TreeContext* treeContext, bool setForChildren, bool strictCopy) {
	if(_treeContext != treeContext) {
		if(_functor) _functor->moveData(_treeContext, treeContext, strictCopy);
		_treeContext = treeContext;
	}

	if(setForChildren) {
		for(unsigned int i = 0; i < _children.size(); i++) {
			_children[i].setTreeContext(treeContext, true, strictCopy);
		}
	}
}

/**
* Returns a pointer to the parent Node or NULL if this is the root node.
**/

Node* Node::parent() const {
	return _parent;
}

/**
* Returns the next child node of the parent (if any), or NULL (if such node
* does not exist).
**/

Node* Node::next() const {
	//check if we have a parent
	if(_parent) {
		//Check that this is not the last child (the parent must obviously have
		//at least a single child so we can reference [0] without a check).
		if((unsigned)((this - &(_parent->_children[0]) + 1)) < _parent->_children.size())
			return (Node*) this + 1;
	}
	//return null if anything goes wrong
	return NULL;
}

/**
* Returns the previous child node of the parent (if any), or NULL (if such node
* does not exist).
**/

Node* Node::prev() const {
	//check if we have a parent
	if(_parent) {
		//Check that this is not the first child (the parent must obviously have
		//at least a single child so we can reference [0] without a check).
		if(this != &(_parent->_children[0])) return (Node*) this - 1;
	}
	//return null if anything goes wrong
	return NULL;
}

/**
* Returns the first child node (that is the leftmost node). This is the same
* as eldest() returns. Returns NULL if there are no children.
**/

Node* Node::child() const {
	//check if we have any children in the first place
	if(_children.size()) return (Node*) &(_children[0]);
	else return NULL;
}

/**
* Returns the eldest child node (that is the leftmost node). This is the same
* as child() returns. Returns NULL if there are no children.
**/

Node* Node::eldest() const {
	//check if we have any children in the first place
	if(_children.size()) return (Node*) &(_children[0]);
	else return NULL;
}

/**
* Returns the youngest child node (that is the rightmost node). Returns NULL if
* there are no children.
**/

Node* Node::youngest() const {
	//check if we have any children in the first place
	if(_children.size()) return (Node*) &(_children.back());
	else return NULL;
}

/**
* Returns the number of children.
**/

unsigned int Node::size() const {
	return _children.size();
}

/**
* Returns the depth in which the node resides. Root node is considered to be in
* depth = 0.
**/

unsigned int Node::depth() const {
	Node* par = _parent; unsigned int d = 0;
	while(par != NULL) {d++; par = par->_parent;}
	return d;
}

/**
* Returns the depth of Node's subtree. This Node's depth is considered to be 0.
**/

unsigned int Node::subtreeDepth() const {
	ConstTraverser trav, tend = end();
	unsigned int maxDepth = 0;

	for(trav = begin(); trav != tend; trav++) {
		unsigned int d(trav.depth());
		if(d > maxDepth) maxDepth = d;
	}

	return maxDepth - depth(); //subtract depth of this node
}

/**
* Returns a traverser to the Node.
**/

Traverser Node::begin() {
	return Traverser(this);
}

/**
* Returns a traverser of the next Node, which can be used as end in a for loop
* to traverse a subtree.
**/

Traverser Node::end() {
	if(next()) return Traverser(next());
	else return Traverser(this, 2);
}

/**
* Returns a traverser to the Node.
**/

ConstTraverser Node::begin() const {
	return ConstTraverser(this);
}

/**
* Returns a traverser of the next Node, which can be used as end in a for loop
* to traverse a subtree.
**/

ConstTraverser Node::end() const {
	if(next()) return ConstTraverser(next());
	return ConstTraverser(this, 2);
}

/**
* Returns the return type of the node. Only a functor with the same return type
* can be set by calling initNode.
**/

TypeID Node::getReturnType() const {
	return _type;
}

/**
* Returns a pointer to the Node's functor or NULL if the functor has not been set.
**/

intrusive_ptr<NodeFunctor> Node::getFunctor() {
	return _functor;
}

/**
* Assembles arguments from the subnodes and makes the functor process them
* returning the result.
**/

AlgoType Node::process(ProcessContext& context) const {
	if(!_functor) throw TracedError_InvalidPointer("Functor not set.");
	context.execGuard().guard();

	return _functor->process(context, _treeContext, _children).convert(_type, context);
}

/**
* Returns true if the functor pointer is NULL.
**/

bool Node::isNull() const {
	return !_functor;
}

/**
* Sets the functor. This also adapts all aspects of the Node to the new functor
* (like creating empty subnodes that will accomodate functor's inputs). Functor
* must however have the same return type as the Node otherwise an exception is
* thrown.
**/

void Node::setFunctor(const intrusive_ptr<NodeFunctor>& functor) {
	if(!functor) throw TracedError_InvalidPointer("NULL pointer to functor passed.");

	if(AlgoConverterRegister().convertible(functor->getReturnType(), _type)) {
		_children.clear();

		if(_functor) _functor->clearData(_treeContext);

		_functor = functor;
		unsigned int sz = _functor->getNumArgs();
		const TypeList& type(_functor->getArgTypes());
		_children.reserve(sz);

		for(unsigned int i = 0; i < sz; i++) {
			_children.push_back(Node(_treeContext, type[i], this));
		}
	} else throw TracedError_Generic("Functor return type is not convertible to Node's return type.");
}

/**
* Deletes the functor and all subnodes. Keeps the return type and parent.
**/

void Node::reset() {
	if(_functor) {
		_functor->clearData(_treeContext);
		_functor.reset();
	}
	_children.clear();
}

/**
* Calls reset on children nodes.
**/

void Node::resetChildren() {
	for(unsigned int i = 0; i < _children.size(); i++)
		_children[i].reset();
}

/**
* Returns an s-expression corresponding to the syntactic tree. If no functor is
* set it returns NO_FUNCTOR instead.
**/

std::string Node::getSExpression() const {
	if(!_functor) return "NO_FUNCTOR";

	std::string tab; for(unsigned int i = 0; i < depth(); i++) tab += "\t";
	std::string sexp = tab + std::string(_functor->getSExpression());
	if(_children.size()) {
		sexp += "(\n";
		unsigned int i;
		for(i = 0; i < _children.size() - 1; i++) {
			sexp += _children[i].getSExpression() + ",\n";
		}
		sexp += _children[i].getSExpression() + "\n" + tab + ")";
	}
	return sexp;
}

/**
* Swap content with another Node. If return types of both nodes are not
* convertible to each other, an TracedError_Generic is thrown.
* @param node The node to swap content with.
* @param swapParents Controls whether to also swap parent pointers of the nodes.
* This is set to false by default as that is what makes sense when swapping
* subtrees.
* @param strictCopy NodeFunctors may need to copy their associated content
* from one TreeContext to another, e.g. Modules, this specifies whether this
* is a normal copy or swap operation or one that happens during crossover
* (as in such swaps less strict behaviour such as remapping from one ModuleHandler
* to another may be allowed.)
**/

void Node::swap(Node& node, bool swapParents, bool strictCopy) {
	//check first that both types are convertible to each other
	if(!AlgoConverterRegister().convertible(_type, node._type) ||
		!AlgoConverterRegister().convertible(node._type, _type)
	) throw TracedError_Generic("Return types of the nodes (" + _type.name() + " " + node._type.name() + ") are not convertible to each other.");

	//swap functor pointers
	intrusive_ptr<NodeFunctor> tmpf = _functor;
	_functor = node._functor;
	node._functor = tmpf;

	if(_functor) _functor->moveData(node._treeContext, _treeContext, strictCopy);
	if(node._functor) node._functor->moveData(_treeContext, node._treeContext, strictCopy);

	//if swapParents, swap parent pointers
	if(swapParents) {
		Node* tmpp = _parent;
		_parent = node._parent;
		node._parent = tmpp;
	}

	//swap children
	_children.swap(node._children);
	parentTheChildren();
	node.parentTheChildren();

	//swap _type
	systematic::TypeID tmpt = _type;
	_type = node._type;
	node._type = tmpt;

	this->setTreeContext(_treeContext, true, strictCopy);
	node.setTreeContext(node._treeContext, true, strictCopy);
}

/**
* Go over all children and set this node to be their parent. This is called from
* copy constructor and assignment operator.
**/

void Node::parentTheChildren() {
	unsigned int sz = _children.size();
	for(unsigned int i = 0; i < sz; i++)
		_children[i]._parent = this;
}

/**
* Assignment operator.
**/

Node& Node::operator=(const Node& node) {
	if(&node != this) {
		_type = node._type;
		if(_functor) _functor->clearData(_treeContext);
		_functor = intrusive_ptr<NodeFunctor>(clone(node._functor));
		_parent = node._parent;
		_children = node._children;
		_treeContext = node._treeContext;
		parentTheChildren();
//		setTreeContext(node._treeContext, true, true);
	}
	return *this;
}

/**
* Copy constructor. Copies all members. For functor the actual object is copied,
* not just the pointer.
**/


Node::Node(const Node& node):
_type(node._type), _functor(clone(node._functor)),
_parent(node._parent), _children(node._children), _treeContext(node._treeContext)
{
	parentTheChildren();
//	setTreeContext(node._treeContext, true, true);
}

/**
* Constructor.
* @param parentNode Pointer to the parent Node. Set to NULL if this is the root node.
* @param returnType Return type that the Node should have. Only functors with
* the same return type will be allowed when setFunctor is called later.
**/

Node::Node(TreeContext* treeContext, systematic::TypeID returnType, Node* parentNode):
	_type(returnType), _functor(), _parent(parentNode), _children(), _treeContext(treeContext) {}

/**
* Constructor.
* @param parentNode Pointer to the parent Node. Set to NULL if this is the root node.
* @param functor The element that does the actual processing once the arguments
* have been assembled from the subnodes. The object is owned by the Node and
* gets deleted from the destructor.
**/

Node::Node(TreeContext* treeContext, const intrusive_ptr<NodeFunctor>& functor, Node* parentNode):
	_type(), _functor(), _parent(parentNode), _children(), _treeContext(treeContext)
{
	if(!functor) throw TracedError_InvalidPointer("NULL pointer to functor passed.");
	_type = functor->getReturnType();
	setFunctor(functor);
}

/**
 * Default constructor for use by serialization.
 */

Node::Node(): _type(), _functor(), _parent(NULL), _children(), _treeContext(NULL) {}

/**
* Destructor.
* Deletes the functor if any.
**/

Node::~Node() {
	if(_functor) _functor->clearData(_treeContext);
}

} //namespace algotree
