#ifndef ALGO_TRAVERSER_H_INCLUDED
#define ALGO_TRAVERSER_H_INCLUDED

#include "Node.h"

namespace algotree {

template<class Node, class Trav>
class TraverserImpl {
private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & _direction;
		ar & _node;
		ar & _depth;
    }

protected:
	//! Set to 0 normally, set to 1 (past to the left) or 2 (past to the right)
	//! if invalid.
	char _direction;
	//! Node.
	Node* _node;
	//! Depth - traverser keeps track of the depth so that one does not have
	//! to ask the node (which has to go up until it reaches the root node
	//! every time to calculate it).
	unsigned int _depth;

public:
	bool isValid() const;
	void setDirection(char direction);

	void assign(const Trav& traverser);
	bool operator==(const Trav& traverser) const;
	bool operator!=(const Trav& traverser) const;

	void prev();
	void next();

	Node& operator*();
	Node* operator->();

	unsigned int depth();
	void refreshDepth();

	TraverserImpl();
	TraverserImpl(const TraverserImpl<Node, Trav>& traverser);
	explicit TraverserImpl(Node* node, char direction = 0);
};

/**
* Returns true if the traverser is valid (not left or right of the valid nodes
* and with a non-NULL node pointer).
**/

template<class Node, class Trav>
bool TraverserImpl<Node, Trav>::isValid() const {
	return(_node && !_direction);
}

/**
* Sets the direction flag. 0 means that traverser is in the range of valid
* nodes, 1 means it is to the left of valid nodes and 2 means it is to the right
of valid nodes; other values result in undefined behaviour.
**/

template<class Node, class Trav>
void TraverserImpl<Node, Trav>::setDirection(char direction) {
	_direction = direction;
}

/**
* Assignment.
**/

template<class Node, class Trav>
void TraverserImpl<Node, Trav>::assign(const Trav& traverser) {
	if(&traverser != this) {
		_direction = traverser._direction;
		_node = traverser._node;
		_depth = traverser._depth;
	}
}

/**
* Compares two traversers. In case the traverser is past valid nodes to the left
* or past valid nodes to the right, pointer to node is not part
* of the comparison. This allows for comparison with end().
**/

template<class Node, class Trav>
bool TraverserImpl<Node, Trav>::operator==(const Trav& traverser) const {
	if(_direction) {
		if(_direction == traverser._direction) return true;
	} else {
		if(_direction == traverser._direction && _node == traverser._node) return true;
	}

	return false;
}

/**
* Compares two traversers. In case the traverser is past valid nodes to the left
* or past valid nodes to the right, pointer to node is not part
* of the comparison. This allows for comparison with end().
**/

template<class Node, class Trav>
bool TraverserImpl<Node, Trav>::operator!=(const Trav& traverser) const {
	return !(*this == traverser);
}

/**
* Decrements the traverser - moves it to the previous node. If the traverser is
* already past valid nodes to the left this does nothing.
**/

template<class Node, class Trav>
void TraverserImpl<Node, Trav>::prev() {
	if(!_node && !_direction)
		throw TracedError_InvalidPointer("Pointer to Node is NULL and traverser is not pre or past the tree.");

	switch(_direction) {
	case 0:
		if(_node->prev()) {
			_node = _node->prev();
		} else if(_node->parent()) {
			_node = _node->parent();
			_depth--;
		} else {
			_direction = 1;
		}
	break;
	case 2:
		if(_node) _direction = 0;
	break;
	case 1:
	default:
	break;
	}
}

/**
* Increments the traverser - moves it to the next node. If the traverser is
* already past valid nodes to the right this does nothing.
**/

template<class Node, class Trav>
void TraverserImpl<Node, Trav>::next() {
	if(!_node && !_direction)
		throw TracedError_InvalidPointer("Pointer to Node is NULL and traverser is not pre or past the tree.");

	switch(_direction) {
	case 0:
		if(_node->child()) {
			_node = _node->child();
			_depth++;
		} else if(_node->next()) {
			_node = _node->next();
		} else {
			bool found = false;
			while(_node->parent()) {
				_node = _node->parent();
				_depth--;
				if(_node->next()) {
					_node = _node->next();
					found = true;
					break;
				}
			}
			if(!found) _direction = 2;
		}
	break;
	case 1:
		if(_node) _direction = 0;
	break;
	case 2:
	default:
	break;
	}
}

/**
* Returns a reference to Node if isValid(). If not an TracedError_InvalidPointer is thrown.
**/

template<class Node, class Trav>
Node& TraverserImpl<Node, Trav>::operator*() {
	if(!isValid()) throw TracedError_InvalidPointer("Cannot dereference an invalid traverser.");
	return *_node;
}

/**
* Arrow operator.
**/

template<class Node, class Trav>
Node* TraverserImpl<Node, Trav>::operator->() {
	if(!isValid()) throw TracedError_InvalidPointer("Cannot dereference an invalid traverser.");
	return _node;
}

/**
* Returns current depth as tracked by the traverser. You may call refreshDepth()
* to refresh the count.
**/

template<class Node, class Trav>
unsigned int TraverserImpl<Node, Trav>::depth() {
	return _depth;
}

/**
* If a node is set, the Traverser refreshes the tracking of depth by asking
* the Node.
**/

template<class Node, class Trav>
void TraverserImpl<Node, Trav>::refreshDepth() {
	if(_node) _depth = _node->depth();
}

/**
* Constructor. Initializes direction to 0 and node pointer to NULL.
**/

template<class Node, class Trav>
TraverserImpl<Node, Trav>::TraverserImpl(): _direction(0), _node(NULL), _depth(0) {}

/**
* Copy constructor.
**/

template<class Node, class Trav>
TraverserImpl<Node, Trav>::TraverserImpl(const TraverserImpl<Node, Trav>& traverser):
_direction(traverser._direction), _node(traverser._node), _depth(traverser._depth) {}

/**
* Constructor.
* @param Node Node to which the traverser points.
* @param direction Direction (1 means it is to the left of valid nodes
* and 2 means it is to the right of valid nodes; other values result
* in undefined behaviour).
**/

template<class Node, class Trav>
TraverserImpl<Node, Trav>::TraverserImpl(Node* node, char direction):
_direction(direction), _node(node), _depth((_node)?(_node->depth()):(0)) {}

/**
* @class Traverser
* @author Michal Gregor
* Traverses all nodes of a tree. In certain aspects it may be compared
* to an iterator.
*
* When traverser gets left or right of the valid range of nodes (by calling
* -- to the root node or ++ to the last node) it gets into a special state,
* where it retains its Node pointer. If left, operator-- does nothing and
* operator++ brings traverser back to the root node. If right, operator++ does
* nothing and operator-- brings traverser back to the last node.
**/

SYS_WNONVIRT_DTOR_OFF
class ALGOTREE_API Traverser: private TraverserImpl<Node, Traverser> {
SYS_WNONVIRT_DTOR_ON
private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
    	ar & boost::serialization::base_object<TraverserImpl<Node, Traverser> >(*this);
	}

private:
	friend class ConstTraverser;
	friend class TraverserImpl<Node, Traverser>;

public:
	using TraverserImpl<Node, Traverser>::isValid;
	using TraverserImpl<Node, Traverser>::setDirection;
	using TraverserImpl<Node, Traverser>::operator==;
	using TraverserImpl<Node, Traverser>::operator!=;
	using TraverserImpl<Node, Traverser>::operator*;
	using TraverserImpl<Node, Traverser>::operator->;
	using TraverserImpl<Node, Traverser>::depth;
	using TraverserImpl<Node, Traverser>::refreshDepth;

	/**
	* Assignment.
	**/

	Traverser& operator=(const Traverser& traverser) {
		assign(traverser);
		return *this;
	}

	/**
	* Decrements the traverser - moves it to the previous node. If the traverser is
	* already past valid nodes to the left this does nothing.
	**/

	Traverser& operator--() {
		prev();
		return *this;
	}

	/**
	* Decrements the traverser - moves it to the previous node. If the traverser is
	* already past valid nodes to the left this does nothing.
	**/

	Traverser operator--(int) {
		Traverser traverser = *this;
		prev();
		return traverser;
	}

	/**
	* Increments the traverser - moves it to the next node. If the traverser is
	* already past valid nodes to the right this does nothing.
	**/

	Traverser& operator++() {
		next();
		return *this;
	}

	/**
	* Increments the traverser - moves it to the next node. If the traverser is
	* already past valid nodes to the right this does nothing.
	**/

	Traverser operator++(int) {
		Traverser traverser = *this;
		next();
		return traverser;
	}

	Traverser(): TraverserImpl<Node, Traverser>() {}
	Traverser(const Traverser& traverser): TraverserImpl<Node, Traverser>(traverser) {}
	explicit Traverser(Node* node, char direction = 0):
		TraverserImpl<Node, Traverser>(node, direction) {}
};

/**
* @class ConstTraverser
* @author Michal Gregor
* Traverses all nodes of a tree. In certain aspects it may be compared
* to a const_iterator.
*
* When traverser gets left or right of the valid range of nodes (by calling
* -- to the root node or ++ to the last node) it gets into a special state,
* where it retains its Node pointer. If left, operator-- does nothing and
* operator++ brings traverser back to the root node. If right, operator++ does
* nothing and operator-- brings traverser back to the last node.
**/

SYS_WNONVIRT_DTOR_OFF
class ALGOTREE_API ConstTraverser: private TraverserImpl<const Node, ConstTraverser> {
SYS_WNONVIRT_DTOR_ON
private:
	friend class boost::serialization::access;

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
    	ar & boost::serialization::base_object<TraverserImpl<Node, ConstTraverser> >(*this);
	}

private:
	friend class TraverserImpl<const Node, ConstTraverser>;

public:
	using TraverserImpl<const Node, ConstTraverser>::isValid;
	using TraverserImpl<const Node, ConstTraverser>::setDirection;
	using TraverserImpl<const Node, ConstTraverser>::operator==;
	using TraverserImpl<const Node, ConstTraverser>::operator!=;
	using TraverserImpl<const Node, ConstTraverser>::operator*;
	using TraverserImpl<const Node, ConstTraverser>::operator->;
	using TraverserImpl<const Node, ConstTraverser>::depth;
	using TraverserImpl<const Node, ConstTraverser>::refreshDepth;

	/**
	* Assignment.
	**/

	ConstTraverser& operator=(const ConstTraverser& traverser) {
		assign(traverser);
		return *this;
	}

	/**
	* Decrements the traverser - moves it to the previous node. If the traverser is
	* already past valid nodes to the left this does nothing.
	**/

	ConstTraverser& operator--() {
		prev();
		return *this;
	}

	/**
	* Decrements the traverser - moves it to the previous node. If the traverser is
	* already past valid nodes to the left this does nothing.
	**/

	ConstTraverser operator--(int) {
		ConstTraverser traverser = *this;
		prev();
		return traverser;
	}

	/**
	* Increments the traverser - moves it to the next node. If the traverser is
	* already past valid nodes to the right this does nothing.
	**/

	ConstTraverser& operator++() {
		next();
		return *this;
	}

	/**
	* Increments the traverser - moves it to the next node. If the traverser is
	* already past valid nodes to the right this does nothing.
	**/

	ConstTraverser operator++(int) {
		ConstTraverser traverser = *this;
		next();
		return traverser;
	}

	ConstTraverser(): TraverserImpl<const Node, ConstTraverser>() {}

	ConstTraverser(const ConstTraverser& traverser):
		TraverserImpl<const Node, ConstTraverser>(traverser) {}

	ConstTraverser(const Traverser& traverser):
	TraverserImpl<const Node, ConstTraverser>()
	{
		_direction = traverser._direction;
		_node = traverser._node;
		_depth = traverser._depth;
	}

	explicit ConstTraverser(const Node* node, char direction = 0):
		TraverserImpl<const Node, ConstTraverser>(node, direction) {}
};

} //namespace algotree

#endif // ALGO_TRAVERSER_H_INCLUDED
