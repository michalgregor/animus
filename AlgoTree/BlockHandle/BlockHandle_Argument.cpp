#include "BlockHandle_Argument.h"
#include <Systematic/dynamic_typing/ConverterRegister.h>
#include "../Context/ProcessContext.h"

namespace algotree {

/**
* Boost serialization function.
**/

void BlockHandle_Argument::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _index;
	ar & _type;
}

void BlockHandle_Argument::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _index;
	ar & _type;
}

/**
* Returns is of BlockHandle_Argument's type (that is of template parameter Type) as
* identified by the specified TypeID.
**/

systematic::TypeID BlockHandle_Argument::getType() const {
	return _type;
}

/**
* Retrieves the argument from the context (throws std::out_of_range if no such
* argument exists within the context), checks its type (throws
* TracedError_InvalidArgument in case of conflict) and returns it.
**/

AlgoType BlockHandle_Argument::getValue(ProcessContext& context) const {
	AlgoType val;
	try {
		val = context.arguments().getArgs().at(_index);
	} catch(std::out_of_range&) {throw TracedError_OutOfRange("No such argument is available.");}

	if(!AlgoConverterRegister().convertible(val.getType(), _type))
		throw TracedError_InvalidArgument("Value is not of a compatible type.");

	return val.convert(_type, context);
}

/**
* Returns index of the argument in the list of arguments.
**/

BlockHandle_Argument::size_type BlockHandle_Argument::getIndex() const {
	return _index;
}

/**
 * Default constructor. For use in serialization.
 */

BlockHandle_Argument::BlockHandle_Argument(): _index(), _type() {}

/**
* Constructor.
* @param type TypeID of the argument's type.
* @param index Index of the argument in the vector of arguments.
**/

BlockHandle_Argument::BlockHandle_Argument(systematic::TypeID type, size_type index):
_index(index), _type(type) {}

} //namespace algotree
