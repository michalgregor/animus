#ifndef AlgoTree_BlockHandle_Constant_H_
#define AlgoTree_BlockHandle_Constant_H_

#include "../system.h"
#include "../Context/TreeContext.h"
#include "../Context/ContextBlock_Constant.h"

namespace algotree {

/**
* @class BlockHandle_Constant
* @author Michal Gregor
* A handle for constants stored in the tree context.
* @tparam Type Type of the constant.
**/

template<class Type>
class BlockHandle_Constant {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	void serialize(IArchive& ar, const unsigned int UNUSED(version)) {
		ar & _token;
	}

	/**
	* Boost serialization function.
	**/
	void serialize(OArchive& ar, const unsigned int UNUSED(version)) {
		ar & _token;
	}

private:
	//! The storage token associated with the constant.
	StorageToken _token;

public:
	/**
	* Retrieves the constant value from the context. If there is no value
	* stored under such token, a TracedError_NotAvailable is thrown.
	*/

	const Type& operator()(TreeContext* context) const {
		ContextBlock_Constant<Type>& block = context->getBlock<ContextBlock_Constant<Type> >();
		return block[_token];
	}

	//! Returns the underlying StorageToken.
	const StorageToken& getStorageToken() const {return _token;}
	//! Sets the underlying StorageToken.
	void setStorageToken(const StorageToken& token) {_token = token;}

	BlockHandle_Constant<Type>& operator=(const BlockHandle_Constant<Type>& obj);
	BlockHandle_Constant(const BlockHandle_Constant<Type>& obj);
	BlockHandle_Constant(const StorageToken& token);
	BlockHandle_Constant();
};

/**
* Assignment operator.
**/

template<class Type>
BlockHandle_Constant<Type>& BlockHandle_Constant<Type>::operator=(const BlockHandle_Constant<Type>& obj) {
	if(&obj != this) {
		_token = obj._token;
	}

	return *this;
}

/**
* Copy constructor.
**/

template<class Type>
BlockHandle_Constant<Type>::BlockHandle_Constant(const BlockHandle_Constant<Type>& obj):
	_token(obj._token) {}

/**
 * Constructor.
 */

template<class Type>
BlockHandle_Constant<Type>::BlockHandle_Constant(const StorageToken& token):
	_token(token) {}

/**
* Default constructor.
**/

template<class Type>
BlockHandle_Constant<Type>::BlockHandle_Constant(): _token(0) {}

}//namespace algotree

SYS_REGISTER_TEMPLATENAME(algotree::BlockHandle_Constant, 1)

#endif //AlgoTree_BlockHandle_Constant_H_
