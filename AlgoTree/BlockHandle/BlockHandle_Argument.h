#ifndef ALGO_ARGUMENT_H_INCLUDED
#define ALGO_ARGUMENT_H_INCLUDED

#include "../system.h"
#include "../AlgoType.h"
#include <Systematic/type_info.h>

namespace algotree {

class ProcessContext;

/**
* @class BlockHandle_Argument.
* @author Michal Gregor
* Used to reference variables from ProcessContext in FormalParameter and other
* functors.
**/

class ALGOTREE_API BlockHandle_Argument {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	typedef long unsigned int size_type;

private:
	//! Index of the argument within the list of arguments.
	size_type _index;
	//! TypeID of argument's type.
	systematic::TypeID _type;

public:
	systematic::TypeID getType() const;
	AlgoType getValue(ProcessContext& context) const;
	size_type getIndex() const;

	BlockHandle_Argument();
	BlockHandle_Argument(systematic::TypeID type, size_type index);
};

} //namespace algotree

SYS_REGISTER_TYPENAME(algotree::BlockHandle_Argument)

#endif // ALGO_ARGUMENT_H_INCLUDED
