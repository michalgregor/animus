#ifndef ALGO_TREE_H_INCLUDED
#define ALGO_TREE_H_INCLUDED

#include "system.h"

#include "Node.h"
#include "NodeFunctor/NodeFunctor.h"
#include "FunctorSignature.h"
#include "Traverser.h"

#include <map>
#include <string>
#include <vector>

#include "Context/TreeContext.h"

namespace algotree {

class ProcessContext;

/**
* @class Tree
* Simple implementation of the algorithmic tree.
*
* It is not safe to use the same Tree object from multiple threads concurrently.
**/
class ALGOTREE_API Tree {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! TreeContext. This pointer will never be NULL.
	TreeContext* _treeContext;
	//! FunctorSignature of the tree: its return type and argument types.
	FunctorSignature _signature;
	//! Root Node. This pointer will never be NULL.
	Node* _root;

public:
	TreeContext* context() {return _treeContext;}
	const TreeContext* context() const {return _treeContext;}

	//! Returns a pointer to the root Node.
	Node* getRoot() {return _root;}
	//! Returns a pointer to the root Node.
	const Node* getRoot() const {return _root;}

	Traverser begin();
	Traverser end();
	Traverser rend();

	ConstTraverser begin() const;
	ConstTraverser end() const;
	ConstTraverser rend() const;

	systematic::TypeID getReturnType() const;
	const TypeList& getArgTypes() const;
	const FunctorSignature& getSignature() const;

	AlgoType process(ProcessContext& context) const;

	std::string getSExpression() const;

	void swap(Tree& tree);

	Tree& operator=(const Tree& tree);
	Tree(const Tree& tree);

	explicit Tree(
		systematic::TypeID returnType = systematic::type_id<void>(),
		const TypeList& argList = TypeList()
	);

	explicit Tree(const FunctorSignature& signature);

	explicit Tree(
		const intrusive_ptr<NodeFunctor>& rootNodeFunctor,
		const TypeList& argList
	);

	explicit Tree(
		const intrusive_ptr<NodeFunctor>& rootNodeFunctor,
		const FunctorSignature& signature
	);

	~Tree();
};

} //namespace algotree

SYS_REGISTER_TYPENAME(algotree::Tree)

#endif // ALGO_TREE_H_INCLUDED
