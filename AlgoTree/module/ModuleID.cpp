#include "ModuleID.h"

namespace algotree {

/**
* Boost serialization function.
**/
void ModuleID::Token::serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

void ModuleID::Token::serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

/**
* Boost serialization function.
**/
void ModuleID::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _token;
}

void ModuleID::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _token;
}

void ModuleID::removeInstance() {
	if(_token->_count == 1) delete _token;
	else --(_token->_count);
}

} //namespace algotree
