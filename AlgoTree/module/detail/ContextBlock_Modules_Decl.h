#ifndef MODULELIST_DECL_H_
#define MODULELIST_DECL_H_

#include "../../system.h"
#include "../../FunctorSignature.h"
#include "../ModuleID.h"
#include "../ModuleHandler.h"

#include <Systematic/utils/StorageToken.h>
#include <map>

namespace algotree {

class ContextBlock_Modules;
class ModuleContent;
class ModuleHandler;

// StorageList type.
typedef std::map<ModuleHandler, ModuleContent, ModuleHandlerCompare> TokenMap;

// Type of the map mapping from return type to StorageList iterators.
typedef std::multimap<systematic::TypeID, TokenMap::iterator> RetMap;

// Type of the map mapping from FunctorSignature to StorageList iterators.
typedef std::multimap<FunctorSignature, TokenMap::iterator> SigMap;

// Type of the map mapping from ModuleIDs of the ancestors to StorageList iterators.
typedef std::multimap<ModuleID, TokenMap::iterator> AncMap;

// Type of the map mapping from ModuleIDs of the ancestors to StorageList iterators.
typedef std::map<ModuleID, TokenMap::iterator> IDMap;

} //namespace algotree

#endif /* MODULELIST_DECL_H_ */
