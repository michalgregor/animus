#ifndef MODULEID_H_
#define MODULEID_H_

#include "../system.h"
#include <Systematic/utils/ToString.h>
#include <boost/smart_ptr/detail/atomic_count.hpp>

namespace algotree {

/**
* A class used to provide Modules with unique IDs.
**/
class ALGOTREE_API ModuleID {
private:
	class Token {
	private:
		friend class boost::serialization::access;

		void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

	public:
		//! The reference counter.
		boost::detail::atomic_count _count;

		Token(): _count(1) {}
	};

private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! A pointer shared by all instances of the same token: used for comparison checks.
	Token* _token;

private:
	void removeInstance();

public:
	bool operator==(const ModuleID& token) const {return _token == token._token;}
	bool operator!=(const ModuleID& token) const {return _token != token._token;}
	bool operator<(const ModuleID& token) const {return _token < token._token;}
	bool operator<=(const ModuleID& token) const {return _token <= token._token;}
	bool operator>(const ModuleID& token) const {return _token > token._token;}
	bool operator>=(const ModuleID& token) const {return _token >= token._token;}

public:
	//! cast to std::string so that we can used IDs in S-expressions
	operator std::string() const {return "ModuleID(" + systematic::ToString(_token) + ")";}

	//! Returns the number for StorageTokens associated with the particular storage entry.
	long int count() const {return _token->_count;}

	/**
	* Assignment operator.
	*/

	ModuleID& operator=(const ModuleID& token) {
		if(this != &token) {
			removeInstance();
			_token = token._token;
			++(_token->_count);
		}
		return *this;
	}

	/**
	* Copy constructor.
	*/

	ModuleID(const ModuleID& module): _token(module._token) {
		++(_token->_count);
	}

	/**
	* Default constructor.
	*/

	ModuleID(): _token(new Token) {}

	~ModuleID() {
		removeInstance();
	}
};

} //namespace algotree

SYS_REGISTER_TYPENAME(algotree::ModuleID)

#endif /* MODULEID_H_ */
