#ifndef MODULEGENERATORCOMMON_H_
#define MODULEGENERATORCOMMON_H_

#include "../system.h"
#include "../FunctorRegister.h"
#include "../FunctorSignature.h"
#include "ModuleGenerator.h"

namespace algotree {

/**
* @class ModuleGenerator_Common
* @author Michal Gregor
**/
class ALGOTREE_API ModuleGenerator_Common:
	public ModuleGenerator,
	public enable_shared_from_this<ModuleGenerator_Common>
{
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	ModuleGenerator_Common();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

protected:
	//! A shared_ptr to the functor register (may be NULL).
	FunctorRegister _functorRegister;
	//! Stores the function signature that the generated Modules should have.
	FunctorSignature _signature;
	//! Stores functor for root nodes. Unless this is a NULL pointer, root node
	//! of every generated tree should use a copy of this functor.
	intrusive_ptr<NodeFunctor> _rootFunctor;
	//! The default depth.
	unsigned int _defaultDepth;
	//! Stores the default return value of the ModuleHandler.
	AlgoType _defaultValue;
	//! Stores the number of ancestors for the ModuleHandler to track.
	unsigned int _numAncestors;

protected:
	void addArgFunctorsToRegister();

public:
	//! Returns the preset return type of the root node.
	TypeID getRootType() const {return _signature.getReturnType();}
	void setRootType(TypeID type);

	const FunctorSignature& getSignature() const {return _signature;}

	void setFunctorRegister(const FunctorRegister& functorRegister);

	intrusive_ptr<NodeFunctor> getRootFunctor() {return _rootFunctor;}
	void setRootFunctor(const intrusive_ptr<NodeFunctor>& rootFunctor);
	void resetRootFunctor() {_rootFunctor.reset();}

	virtual ModuleHandler operator()(TreeContext* treeContext);

	virtual ModuleHandler generate(TreeContext* treeContext);
	virtual ModuleHandler generate(TreeContext* treeContext, unsigned int depth);

	virtual void generate(Node& parent);
	virtual void generate(Node& parent, unsigned int depth) = 0;

	virtual void replace(Node& parent);
	virtual void replace(Node& parent, unsigned int depth) = 0;

	virtual unsigned int getDefaultDepth() const {return _defaultDepth;}
	virtual void setDefaultDepth(unsigned int depth) {_defaultDepth = depth;}

	ModuleGenerator_Common& operator=(const ModuleGenerator_Common& obj);
	ModuleGenerator_Common(const ModuleGenerator_Common& obj);

	ModuleGenerator_Common(
		AlgoType defaultValue,
		const FunctorSignature& signature,
		unsigned int defaultDepth = 5,
		unsigned int numAncestors = 5
	);

	ModuleGenerator_Common(
		const FunctorRegister& functorRegister,
		AlgoType defaultValue,
		const FunctorSignature& signature,
		unsigned int defaultDepth = 5,
		unsigned int numAncestors = 5
	);

	virtual ~ModuleGenerator_Common() {}
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::ModuleGenerator_Common)

#endif /* MODULEGENERATORCOMMON_H_ */
