#include "ContextBlock_Modules.h"
#include <Systematic/generator/BoundaryGenerator.h>

namespace algotree {

/**
 * Boost serialization.
 */
void ContextBlock_Modules::serialize(OArchive& ar, const unsigned int UNUSED(version)) const {
	ar & _tokenMap;
}

/**
 * Boost deserialization.
 */
void ContextBlock_Modules::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _tokenMap;

	_retMap.clear();
	_sigMap.clear();
	_ancMap.clear();
	_idMap.clear();

	TokenMap::iterator iter;

	for(iter = _tokenMap.begin(); iter != _tokenMap.end(); iter++) {
		iter->second.setContextBlock_Modules(this, iter);
	}
}

/**
 * Swaps contents of the ContextBlock_Moduless.
 */

void ContextBlock_Modules::swap(ContextBlock_Modules& module) {
	_tokenMap.swap(module._tokenMap);
	_retMap.swap(module._retMap);
	_sigMap.swap(module._sigMap);
	_ancMap.swap(module._ancMap);
	_idMap.swap(module._idMap);
}

ModuleContent& ContextBlock_Modules::operator[](StorageToken token) {
	TokenMap::iterator iter(_tokenMap.find(ModuleHandler(token)));
	if(iter == _tokenMap.end()) throw TracedError_OutOfRange("No module stored under such token.");
	return iter->second;
}

const ModuleContent& ContextBlock_Modules::operator[](StorageToken token) const {
	TokenMap::const_iterator iter(_tokenMap.find(ModuleHandler(token)));
	if(iter == _tokenMap.end()) throw TracedError_OutOfRange("No module stored under such token.");
	return iter->second;
}

ContextBlock_Modules::iterator ContextBlock_Modules::entry(StorageToken token) {
	return _tokenMap.find(ModuleHandler(token));
}

ContextBlock_Modules::const_iterator ContextBlock_Modules::entry(StorageToken token) const {
	return _tokenMap.find(ModuleHandler(token));
}

ContextBlock_Modules::iterator ContextBlock_Modules::insert(const ModuleContent& module) {
	return insert(module, StorageToken());
}

ContextBlock_Modules::iterator ContextBlock_Modules::insert(const ModuleContent& module, StorageToken token) {
	TokenMap::iterator iter = _tokenMap.insert(value_type(ModuleHandler(token), module)).first;
	iter->second.setContextBlock_Modules(this, iter);
	return iter;
}

void ContextBlock_Modules::erase(ContextBlock_Modules::iterator position) {
	position->second.eraseFromIndex();
	_tokenMap.erase(position);
}

ContextBlock_Modules::size_type ContextBlock_Modules::erase(StorageToken token) {
	TokenMap::iterator iter = _tokenMap.find(ModuleHandler(token));
	if(iter != _tokenMap.end()) {
		iter->second.eraseFromIndex();
		_tokenMap.erase(iter);
		return 1;
	} else return 0;
}

void ContextBlock_Modules::erase(iterator first, iterator last) {
	for(iterator iter = first; iter != last;) {
		iter->second.eraseFromIndex();
		erase(iter++);
	}
}

void ContextBlock_Modules::clear() {
	_tokenMap.clear();
	_retMap.clear();
	_sigMap.clear();
	_ancMap.clear();
	_idMap.clear();
}

/**
// * Returns the number of elements with such return type.
 */

unsigned int ContextBlock_Modules::count(systematic::TypeID return_type) const {
	return _retMap.count(return_type);
}

/**
 * Returns the number of elements with such signature.
 */

unsigned int ContextBlock_Modules::count(FunctorSignature signature) const {
	return _sigMap.count(signature);
}

/**
 * Returns the number of elements with such ancestor.
 */

unsigned int ContextBlock_Modules::count(ModuleID ancestor) const {
	return _ancMap.count(ancestor);
}

/**
 * Returns true if ModuleHandler with such token exists.
 */

bool ContextBlock_Modules::exists(StorageToken token) const {
	return _tokenMap.find(ModuleHandler(token)) != _tokenMap.end();
}

/**
 * Returns true if ModuleHandler with such return type exists.
 */

bool ContextBlock_Modules::exists(systematic::TypeID return_type) const {
	return _retMap.find(return_type) != _retMap.end();
}

/**
 * Returns true if ModuleHandler with such signature exists.
 */

bool ContextBlock_Modules::exists(FunctorSignature signature) const {
	return _sigMap.find(signature) != _sigMap.end();
}

/**
 * Returns true if ModuleHandler with such ModuleID exists.
 */

bool ContextBlock_Modules::existsID(ModuleID id) const {
	IDMap::const_iterator iter = _idMap.find(id);
	return iter != _idMap.end();
}

/**
 * Returns true if ModuleHandler with such ancestor ModuleHandler exists.
 */

bool ContextBlock_Modules::existsAncestor(ModuleID ancestor) const {
	AncMap::const_iterator iter = _ancMap.find(ancestor);
	return iter != _ancMap.end();
}

ContextBlock_Modules::const_iterator ContextBlock_Modules::entry(ModuleID id) const {
	IDMap::const_iterator iter = _idMap.find(id);
	if(iter == _idMap.end()) return end();
	else return iter->second;
}

ContextBlock_Modules::iterator ContextBlock_Modules::entry(ModuleID id) {
	IDMap::iterator iter = _idMap.find(id);
	if(iter == _idMap.end()) return end();
	else return iter->second;
}

ContextBlock_Modules::const_iterator ContextBlock_Modules::randEntry() const {
	if(!_tokenMap.size()) return end();

	systematic::BoundaryGenerator<unsigned int> generator(0, _tokenMap.size() - 1);
	TokenMap::const_iterator iter = _tokenMap.begin();
	std::advance(iter, generator());

	return iter;
}

ContextBlock_Modules::iterator ContextBlock_Modules::randEntry() {
	if(!_tokenMap.size()) return end();

	systematic::BoundaryGenerator<unsigned int> generator(0, _tokenMap.size() - 1);
	TokenMap::iterator iter = _tokenMap.begin();
	std::advance(iter, generator());

	return iter;
}

ContextBlock_Modules::const_iterator ContextBlock_Modules::randEntry(systematic::TypeID return_type) const {
	std::pair<RetMap::const_iterator, RetMap::const_iterator> range(_retMap.equal_range(return_type));

	unsigned int sz = std::distance(range.first, range.second);
	if(!sz) return end();

	systematic::BoundaryGenerator<unsigned int> generator(0, sz - 1);
	RetMap::const_iterator iter = range.first;
	std::advance(iter, generator());

	return iter->second;
}

ContextBlock_Modules::iterator ContextBlock_Modules::randEntry(systematic::TypeID return_type) {
	std::pair<RetMap::iterator, RetMap::iterator> range(_retMap.equal_range(return_type));
	unsigned int sz = std::distance(range.first, range.second);

	if(!sz) return end();

	systematic::BoundaryGenerator<unsigned int> generator(0, sz - 1);

	RetMap::iterator iter = range.first;
	std::advance(iter, generator());

	return iter->second;
}

ContextBlock_Modules::const_iterator ContextBlock_Modules::randEntry(FunctorSignature signature) const {
	std::pair<SigMap::const_iterator, SigMap::const_iterator> range(_sigMap.equal_range(signature));
	unsigned int sz = std::distance(range.first, range.second);

	if(!sz) return end();

	systematic::BoundaryGenerator<unsigned int> generator(0, sz - 1);
	SigMap::const_iterator iter = range.first;
	std::advance(iter, generator());

	return iter->second;
}

ContextBlock_Modules::iterator ContextBlock_Modules::randEntry(FunctorSignature signature) {
	std::pair<SigMap::iterator, SigMap::iterator> range(_sigMap.equal_range(signature));
	unsigned int sz = std::distance(range.first, range.second);

	if(!sz) return end();

	systematic::BoundaryGenerator<unsigned int> generator(0, sz - 1);
	SigMap::iterator iter = range.first;
	std::advance(iter, generator());

	return iter->second;
}

ContextBlock_Modules::const_iterator ContextBlock_Modules::randEntry(ModuleID relative) const {
	std::pair<AncMap::const_iterator, AncMap::const_iterator> range(_ancMap.equal_range(relative));
	unsigned int sz = std::distance(range.first, range.second);

	if(!sz) return end();

	systematic::BoundaryGenerator<unsigned int> generator(0, sz - 1);

	AncMap::const_iterator iter = range.first;
	std::advance(iter, generator());

	return iter->second;
}

ContextBlock_Modules::iterator ContextBlock_Modules::randEntry(ModuleID ancestor) {
	std::pair<AncMap::iterator, AncMap::iterator> range(_ancMap.equal_range(ancestor));
	unsigned int sz = std::distance(range.first, range.second);

	if(!sz) return end();

	systematic::BoundaryGenerator<unsigned int> generator(0, sz - 1);

	AncMap::iterator iter = range.first;
	std::advance(iter, generator());

	return iter->second;
}

/**
 * Assignment. ModuleContent is copied, ModuleHandlers need to be moved using
 * move() if they are to point into this new copy.
 */

ContextBlock_Modules& ContextBlock_Modules::operator=(const ContextBlock_Modules& moduleList) {
	if(this != &moduleList) {
		clear();

		for(TokenMap::const_iterator iter = moduleList._tokenMap.begin(); iter != moduleList._tokenMap.end(); iter++) {
			std::pair<TokenMap::iterator, bool> pr = _tokenMap.insert(TokenMap::value_type(ModuleHandler(), iter->second));
			if(pr.second) pr.first->second.setContextBlock_Modules(this, pr.first);
			else throw  TracedError_Generic("Such StorageToken is already stored in the index.");
		}
	}

	return *this;
}

ContextBlock_Modules::ContextBlock_Modules(const ContextBlock_Modules& moduleList):
_tokenMap(), _retMap(), _sigMap(), _ancMap(), _idMap() {
	for(TokenMap::const_iterator iter = moduleList._tokenMap.begin(); iter != moduleList._tokenMap.end(); iter++) {
		std::pair<TokenMap::iterator, bool> pr = _tokenMap.insert(TokenMap::value_type(ModuleHandler(), iter->second));
		if(pr.second) pr.first->second.setContextBlock_Modules(this, pr.first);
		else throw  TracedError_Generic("Such StorageToken is already stored in the index.");
	}
}

} //namespace algotree
