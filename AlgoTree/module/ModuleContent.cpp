#include "ModuleContent.h"

#include "../TreeGenerator/TreeGenerator.h"
#include "../Context/ProcessContext.h"
#include "../Node.h"

#include "ContextBlock_Modules.h"
#include <algorithm>

#include <Systematic/serialization/BoostCircularBuffer.h>

namespace algotree {

/**
* Storage-related data is not serialized. It needs to be rebuilt by calling
* setContextBlock_Modules() from serialization function of the ContextBlock_Modules.
**/
void ModuleContent::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _moduleID;
	ar & _ancestry;
	ar & _defaultData;
	ar & _root;
	ar & _signature;
	ar & _isAdf;
	ar & _generator;

	//we do not serialize storage-related data
}

/**
* Storage-related data is not serialized. It needs to be rebuilt by calling
* setContextBlock_Modules() from serialization function of the ContextBlock_Modules.
**/
void ModuleContent::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _moduleID;
	ar & _ancestry;
	ar & _defaultData;
	ar & _root;
	ar & _signature;
	ar & _isAdf;
	ar & _generator;

	//we do not serialize storage-related data
	eraseFromIndex();
	_moduleList = NULL;
	_moduleContentIter = TokenMap::iterator();
}

/**
 * This should be used to set ContextBlock_Modules. If the ModuleContent has previously
 * resided in a ContextBlock_Modules, it erases its index entries from its index and then
 * adds the same to the new index. If the ModuleContents old pointer to ContextBlock_Modules
 * is NULL, there are no entries to erase from any previous ContextBlock_Modules.
 */

void ModuleContent::setContextBlock_Modules(ContextBlock_Modules* moduleList, TokenMap::iterator moduleContentIter) {
	eraseFromIndex();
	_moduleList = moduleList;
	_moduleContentIter = moduleContentIter;
	addToIndex();
}

/**
 * Adds relevant entries into the index of the ContextBlock_Modules.
 * If the ContextBlock_Modules pointer is NULL, does nothing.
 */

void ModuleContent::addToIndex() {
	if(_moduleList) {
		_retIter = _moduleList->_retMap.insert(RetMap::value_type(_signature.getReturnType(), _moduleContentIter));
		_sigIter = _moduleList->_sigMap.insert(SigMap::value_type(_signature.getReturnType(), _moduleContentIter));

		unsigned int num(_ancestry.size());

		for(unsigned int i = 0; i < num; i++) {
			_ancIters.push_back(_moduleList->_ancMap.insert(AncMap::value_type(_ancestry[i], _moduleContentIter)));
		}

		std::pair<IDMap::iterator, bool> pr = _moduleList->_idMap.insert(IDMap::value_type(_moduleID, _moduleContentIter));

		if(pr.second) _idIter = pr.first;
		else throw TracedError_Generic("Such ModuleID is already stored in the index.");
	}
}

/**
 * Erases relevant entries from the index of the ContextBlock_Modules.
 * If the ContextBlock_Modules pointer is NULL, does nothing.
 */

void ModuleContent::eraseFromIndex() {
	if(_moduleList) {
		_moduleList->_retMap.erase(_retIter);
		_moduleList->_sigMap.erase(_sigIter);

		for(typename std::list<AncMap::iterator>::iterator iter = _ancIters.begin(); iter != _ancIters.end(); iter++) {
			_moduleList->_ancMap.erase(*iter);
		}

		_ancIters.clear();

		_moduleList->_idMap.erase(_idIter);
	}
}

/**
 * Updates the ancestor index of the ContextBlock_Modules with current ancestor data.
* If the ContextBlock_Modules pointer is NULL, does nothing.
 */

void ModuleContent::updateAncestorsIndex() {
	if(_moduleList) {
		AncMap& ancMap(_moduleList->_ancMap);

		//delete the old ancestor ids
		for(std::list<AncMap::iterator>::iterator iter = _ancIters.begin(); iter != _ancIters.end(); iter++) {
			ancMap.erase(*iter);
		}

		_ancIters.clear();

		//add the updated ancestors
		for(unsigned int i = 0; i < _ancestry.size(); i++) {
			_ancIters.push_back(ancMap.insert(AncMap::value_type(_ancestry[i], _moduleContentIter)));
		}
	}
}

/**
* Updates the ModuleID index of the ContextBlock_Modules with current ModuleID.
* If the ContextBlock_Modules pointer is NULL, does nothing.
*/

void ModuleContent::updateIDIndex() {
	if(_moduleList) {
		//delete the old module id and add the new one
		_moduleList->_idMap.erase(_idIter);
		std::pair<IDMap::iterator, bool> pr = _moduleList->_idMap.insert(IDMap::value_type(_moduleID, _moduleContentIter));
		if(pr.second) _idIter = pr.first;
		else throw TracedError_Generic("Such ModuleID is already stored in the index.");
	}
}

/**
* Updates the ModuleID index and the ancestor index of the ContextBlock_Modules with
* current ModuleID and ancestors.
* If the ContextBlock_Modules pointer is NULL, does nothing.
*/

void ModuleContent::updateBothIDIndices() {
	updateIDIndex();
	updateAncestorsIndex();
}

/**
 * Sets how many ancestor ModuleIDs the ModuleHandler is supposed to store. Should
 * the ModuleContent already store more ModuleIDs than the new limit, the
 * excessive ModuleIDs are erased.
 */

void ModuleContent::setNumAncestors(unsigned int numAncestors) {
	_ancestry.set_capacity(numAncestors);
	updateAncestorsIndex();
}

void ModuleContent::resetAncestors() {
	_ancestry.clear();
	updateAncestorsIndex();
}

void ModuleContent::setID(ModuleID id) {
	_moduleID = id;
	updateIDIndex();
}

/**
* Calls process of the root Node and returns the result. This may throw an
* AlgoBreak in case that the execution does not finish in time, etc.
**/

AlgoType ModuleContent::process(ProcessContext& context) const {
	AlgoType data;

	try {
		RecursionHolder holder(context.execGuard().startRecursion());
		data = _root.process(context);
	} catch(AlgoRecursionBreak&) {
		if(
			_defaultData.getType() == systematic::type_id<void>() &&
			_root.getReturnType() != systematic::type_id<void>()
		) throw AlgoBreak("Recursion limit exceeded, no default data specified.");
		else data = _defaultData;
	}

	return data;
}

/**
* Returns a reference to root node so that the contained tree can be modified.
* You should not modify the return type of the Node unless you want to break
* everything.
*
* Calls to modify also have the side-effect of modifying ModuleID of the
* ModuleHandler so that we can keep track of changes. If a ModuleHandler is an ADF though,
* ModuleID is never incremented.
*
* Note also that ModuleID is not incremented when the root Node is still null
* (Node::isNull() is true) so that the tree can be initialized without first
* incrementing the ID.
*/

Node& ModuleContent::modify() {
	if(!(_root.isNull() || _isAdf)) {
		_ancestry.push_back(_moduleID);
		_moduleID = ModuleID();
		updateBothIDIndices();
	}
	return _root;
}

/**
* Returns true if the moduleID is stored in ModuleContent's ancestry list.
*/

bool ModuleContent::isAncestor(ModuleID moduleID) const {
	return (std::find(_ancestry.begin(), _ancestry.end(), moduleID) != _ancestry.end());
}

/**
* Returns true if the moduleID is stored in ModuleContent's ancestry list.
*/

bool ModuleContent::isRelative(ModuleID moduleID) const {
	return (_moduleID == moduleID) || (std::find(_ancestry.begin(), _ancestry.end(), moduleID) != _ancestry.end());
}

/**
* Assignment operator. Copies module related data. ContextBlock_Modules pointer remains
* unchanged. ContextBlock_Modules index is update accordingly (unless ModuleHandler pointer is
* NULL in which case this step is skipped).
**/

ModuleContent& ModuleContent::operator=(const ModuleContent& module) {
	if(&module != this) {
		eraseFromIndex();

		_moduleID = module._moduleID;
		_ancestry = module._ancestry;
		_defaultData = module._defaultData;
		_root = module._root;
		_signature = module._signature;
		_isAdf = module._isAdf;
		_generator = module._generator;

		addToIndex();

		//_refcount should not be copied obviously
	}
	return *this;
}

/**
* Copy constructor.
**/

ModuleContent::ModuleContent(const ModuleContent& module):
_moduleID(module._moduleID), _ancestry(module._ancestry),
_defaultData(module._defaultData), _root(module._root),
_signature(module._signature), _isAdf(module._isAdf),
_generator(module._generator), _moduleList(NULL),
_moduleContentIter(), _retIter(), _sigIter(), _ancIters(), _idIter() {}

/**
* Default constructor. This should be used for serialization and
* initialization by assignment only.
**/

ModuleContent::ModuleContent(): _moduleID(), _ancestry(5), _defaultData(),
_root(NULL), _signature(), _isAdf(false), _generator(), _moduleList(NULL),
_moduleContentIter(), _retIter(), _sigIter(), _ancIters(), _idIter() {}

/**
* Constructor.
* @param moduleGenerator ModuleHandler generator for use in crossover and mutation.
* @param defaultValue Data returned when maximum recursion limit prevents
* the module from executing. If the contained data is of void type and
* the return type of the Node is not void, an exception is thrown instead
* of returning the default value.
* @param signature Signature of the tree, that is its return type and
* argument types. If defaultData.getType() is not convertible to the return
* type as specified in the signature a TracedError_InvalidArgument is thrown.
* @param numAncestors The number of ancestor IDs to store in the ancestry list.
**/

ModuleContent::ModuleContent(
	const shared_ptr<ModuleGenerator>& moduleGenerator,
	TreeContext* treeContext,
	AlgoType defaultValue,
	const FunctorSignature& signature,
	unsigned int numAncestors,
	ModuleID moduleID
): _moduleID(moduleID), _ancestry(numAncestors), _defaultData(defaultValue),
_root(treeContext, defaultValue.getType()), _signature(signature),
_isAdf(false), _generator(moduleGenerator), _moduleList(NULL),
_moduleContentIter(), _retIter(), _sigIter(), _ancIters(), _idIter() {
	if(!AlgoConverterRegister().convertible(_defaultData.getType(), _signature.getReturnType()))
		throw TracedError_InvalidArgument("The provided default data is not convertible to the return type as specified in FunctorSignature.");
}

/**
* Constructor.
* @param defaultValue Data returned when maximum recursion limit prevents
* the module from executing. If the contained data is of void type and
* the return type of the Node is not void, an exception is thrown instead
* of returning the default value.
* @param signature Signature of the tree, that is its return type and
* argument types. If defaultData.getType() is not convertible to the return
* type as specified in the signature a TracedError_InvalidArgument is thrown.
* @param numAncestors The number of ancestor IDs to store in the ancestry list.
**/

ModuleContent::ModuleContent(
	TreeContext* treeContext,
	AlgoType defaultValue,
	const FunctorSignature& signature,
	unsigned int numAncestors,
	ModuleID moduleID
): _moduleID(moduleID), _ancestry(numAncestors), _defaultData(defaultValue),
_root(treeContext, defaultValue.getType()), _signature(signature),
_isAdf(false), _generator(), _moduleList(NULL), _moduleContentIter(),
_retIter(), _sigIter(), _ancIters(), _idIter() {
	if(!AlgoConverterRegister().convertible(_defaultData.getType(), _signature.getReturnType()))
		throw TracedError_InvalidArgument("The provided default data is not convertible to the return type as specified in FunctorSignature.");
}

} //namespace algotree
