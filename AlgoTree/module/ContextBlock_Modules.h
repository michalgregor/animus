#ifndef ALGO_MODULELIST_H_INCLUDED
#define ALGO_MODULELIST_H_INCLUDED

#include "../system.h"
#include "../FunctorSignature.h"
#include "../NodeFunctor/NodeFunctor.h"

#include "detail/ContextBlock_Modules_Decl.h"
#include "ModuleContent.h"
#include "ModuleHandler.h"

#include <map>
#include <boost/serialization/map.hpp>

namespace algotree {

/**
* @class ContextBlock_Modules
* @author Michal Gregor
* Used as a centralized store for modules.
*
* It is not safe to use the same Tree object from multiple threads concurrently.
**/
class ALGOTREE_API ContextBlock_Modules {
private:
	friend class boost::serialization::access;

    void serialize(OArchive& ar, const unsigned int version) const;
    void serialize(IArchive& ar, const unsigned int version);

private:
	//! The list of modules stored by storage tokens.
	TokenMap _tokenMap;
	//! The mapping from return type to _tokenMap iterators.
	RetMap _retMap;
	//! The mapping from input argument types to _tokenMap iterators.
	SigMap _sigMap;
	//! The mapping from ModuleIDs of the relatives to _tokenMap iterators.
	AncMap _ancMap;
	//! The map mapping from ModuleIDs of the ModuleContents to _tokenMap iterators.
	IDMap _idMap;

public:
	typedef TokenMap::iterator iterator;
	typedef TokenMap::const_iterator const_iterator;
	typedef TokenMap::reverse_iterator reverse_iterator;
	typedef TokenMap::const_reverse_iterator const_reverse_iterator;

	typedef TokenMap::key_type key_type;
	typedef TokenMap::mapped_type mapped_type;
	typedef TokenMap::value_type value_type;
	typedef TokenMap::size_type size_type;
	typedef TokenMap::difference_type difference_type;

public:

	iterator begin() {
		return _tokenMap.begin();
	}

	const_iterator begin() const {
		return _tokenMap.begin();
	}

	iterator end() {
		return _tokenMap.end();
	}

	const_iterator end() const {
		return _tokenMap.end();
	}

	reverse_iterator rbegin() {
		return _tokenMap.rbegin();
	}

	const_reverse_iterator rbegin() const {
		return _tokenMap.rbegin();
	}

	reverse_iterator rend() {
		return _tokenMap.rend();
	}

	const_reverse_iterator rend() const {
		return _tokenMap.rend();
	}

private:
	friend class ModuleContent;
	friend class ModuleHandler;

	//interface for use by Module objects
	ModuleContent& operator[](StorageToken token);
	const ModuleContent& operator[](StorageToken token) const;

	iterator entry(StorageToken token);
	const_iterator entry(StorageToken token) const;

	iterator insert(const ModuleContent& module);
	iterator insert(const ModuleContent& module, StorageToken token);

	size_type erase(StorageToken token);
	bool exists(StorageToken token) const;

public:
	void swap(ContextBlock_Modules& module);

	const_iterator entry(ModuleID id) const;
	iterator entry(ModuleID id);

	bool empty() const {
		return _tokenMap.empty();
	}

	size_type size() const {
		return _tokenMap.size();
	}

	size_type erase(ModuleID moduleID);

	void erase(iterator position);
	void erase(iterator first, iterator last);

	void clear();

	unsigned int count(systematic::TypeID return_type) const;
	unsigned int count(FunctorSignature signature) const;
	unsigned int count(ModuleID ancestor) const;

	bool exists(systematic::TypeID return_type) const;
	bool exists(FunctorSignature signature) const;
	bool existsID(ModuleID id) const;
	bool existsAncestor(ModuleID ancestor) const;

	const_iterator randEntry() const;
	iterator randEntry();

	const_iterator randEntry(systematic::TypeID return_type) const;
	iterator randEntry(systematic::TypeID return_type);

	const_iterator randEntry(FunctorSignature signature) const;
	iterator randEntry(FunctorSignature signature);

	const_iterator randEntry(ModuleID ancestor) const;
	iterator randEntry(ModuleID ancestor);

	ContextBlock_Modules& operator=(const ContextBlock_Modules& moduleList);
	ContextBlock_Modules(const ContextBlock_Modules& moduleList);

	ContextBlock_Modules(): _tokenMap(), _retMap(), _sigMap(), _ancMap(), _idMap() {}
};

} //namespace algotree

SYS_REGISTER_TYPENAME(algotree::ContextBlock_Modules)

#endif // ALGO_MODULELIST_H_INCLUDED
