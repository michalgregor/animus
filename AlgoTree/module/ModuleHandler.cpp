#include "ModuleHandler.h"
#include "ContextBlock_Modules.h"
#include "ModuleContent.h"
#include "../Tree.h"

namespace algotree {

/**
* Boost serialization function.
**/

void ModuleHandler::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _moduleToken;
}

void ModuleHandler::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _moduleToken;
}

void ModuleHandler::reset(
#ifndef ALGO_MODULE_PERSISTENT
	TreeContext* treeContext
#else
	TreeContext* UNUSED(treeContext)
#endif
) {
	if(_moduleToken.isValid()) {
		#ifndef ALGO_MODULE_PERSISTENT
		ContextBlock_Modules::iterator iter(treeContext->modules().entry(_moduleToken));
		if(iter == treeContext->modules().end()) return;
		ModuleContent& moduleContent(iter->second);
		if(!moduleContent.getIsAdf() && _moduleToken.count() == 2) treeContext->modules().erase(_moduleToken);
		#endif
		_moduleToken.reset();
	}
}

/**
* Copies the data associated with the ModuleHandler from the old TreeContext
* (ContextBlock_Modules) to a new TreeContext (ContextBlock_Modules). If there are no other ModuleHandler
* objects pointing at that data, the data is removed from the old TreeContext.
*
* @param strictCopy Specifies whether to create a strict copy (true) or whether
* we can remap to existing data even though it is not identical. This concept
* is used by Modules - when moving them in the course of copying a Tree
* a strict copy has to be created. When moving them in the course of crossover,
* they can be remaped to other related Modules.
*
* Note also that if isAdf is set to true, strict copying is always active.
*/

void ModuleHandler::move(TreeContext* oldTreeContext, TreeContext* newTreeContext, bool strictRemap) {
	if(!_moduleToken.isValid()) TracedError_InvalidPointer("The ModuleHandler is not valid.");

	StorageToken newStorageToken;
	ContextBlock_Modules::iterator iter(oldTreeContext->modules().entry(_moduleToken));
	if(iter == oldTreeContext->modules().end()) {
		std::cerr << oldTreeContext->modules().size() << std::endl;
		throw TracedError_ResourcesMissing("No ModuleContent stored under such StorageToken.");
	}

	const ModuleContent& moduleContent(iter->second);

	//If strict remap is set, we only need to retrieve the new storage token for
	//the associated module (which has already been copied). We look the module
	//up using its ModuleID.
	if(strictRemap || moduleContent.getIsAdf()) {
		iter = newTreeContext->modules().entry(iter->second.getID());
		//if there is no ModuleContent with such ModuleID, throw an exception
		if(iter == newTreeContext->modules().end()) throw TracedError_ResourcesMissing("There is no ModuleContent with such ModuleID in the ContextBlock_Modules.");
		newStorageToken = iter->first._moduleToken;
	} else {
		//look for the same module first
		iter = newTreeContext->modules().entry(iter->second.getID());

		if(iter == newTreeContext->modules().end()) {
			//look for descendants
			iter = newTreeContext->modules().randEntry(moduleContent.getID());
		}

		#ifdef ALGO_MODULE_ANY_RELATIVE_REMAP
		//if no descendant is found, look for relatives
		if(iter == newTreeContext->modules().end()) {
			const circular_buffer<ModuleID>& ancestors(moduleContent.getAncestors());
			circular_buffer<ModuleID>::const_iterator aiter;

			for(aiter = ancestors.begin(); aiter != ancestors.end(); aiter++) {
				if((iter = newTreeContext->modules().randEntry(*aiter)) != newTreeContext->modules().end()) break;
			}
		}
		#endif

		//remap by signature if such behaviour is activated
		#ifdef ALGO_MODULE_SIGNATURE_REMAP
		if(iter == newTreeContext->modules().end()) iter = newTreeContext->modules().randEntry(moduleContent.getSignature()));
		#endif

		if(iter != newTreeContext->modules().end()) {
			newStorageToken = iter->first._moduleToken;
		} else {
			//if the module has not been remapped, copy it now
			iter = newTreeContext->modules().insert(moduleContent);
			iter->second._root.setTreeContext(newTreeContext, false);
			newStorageToken = iter->first._moduleToken;
		}
	}

	#ifndef ALGO_MODULE_PERSISTENT
	//if there are two instances of the storage token, that is to say one
	//in the ModuleHandler and the other one in the ContextBlock_Modules, the associated data
	//can be safely erased
	if(!moduleContent.getIsAdf() && _moduleToken.count() == 2) oldTreeContext->modules().erase(_moduleToken);
	#endif

	_moduleToken = newStorageToken;
}

/**
* Provides the ModuleHandler with a context, which enables access to the actual data
* associated with the ModuleHandler.
*/

ModuleContent& ModuleHandler::operator()(TreeContext* treeContext) {
	if(!_moduleToken.isValid()) TracedError_InvalidPointer("The ModuleHandler is not valid.");
	return treeContext->modules()[_moduleToken];
}

/**
* Provides the ModuleHandler with a context, which enables access to the actual data
* associated with the ModuleHandler.
*/

const ModuleContent& ModuleHandler::operator()(const TreeContext* treeContext) const {
	if(!_moduleToken.isValid()) TracedError_InvalidPointer("The ModuleHandler is not valid.");
	return treeContext->modules()[_moduleToken];
}

/**
* Assignment operator.
**/

ModuleHandler& ModuleHandler::operator=(const ModuleHandler& obj) {
	if(&obj != this) {
		_moduleToken = obj._moduleToken;
	}

	return *this;
}

/**
* Copy constructor.
**/

ModuleHandler::ModuleHandler(const ModuleHandler& obj):
_moduleToken(obj._moduleToken) {}

/**
* Constructor.
**/

ModuleHandler::ModuleHandler(): _moduleToken() {}

/**
* Constructor.
* @param moduleGenerator ModuleHandler generator for use in crossover and mutation.
* @param context The context that is to store the associated data.
* @param defaultValue Data returned when maximum recursion limit prevents
* the module from executing.
* @param signature Signature of the tree, that is its return type and
* argument types. Return type of the signature is set
* to defaultValue.getType() so that these are consistent.
* @param numAncestors The number of ancestor IDs to store in the ancestry list.
**/

ModuleHandler::ModuleHandler(
	const shared_ptr<ModuleGenerator>& moduleGenerator,
	TreeContext* treeContext,
	AlgoType defaultValue,
	const FunctorSignature& signature,
	unsigned int numAncestors,
	ModuleID moduleID
): _moduleToken((treeContext->modules().insert(ModuleContent(moduleGenerator, treeContext, defaultValue, signature, numAncestors, moduleID)))->first._moduleToken) {}

/**
* Constructor.
* @param context The context that is to store the associated data.
* @param defaultValue Data returned when maximum recursion limit prevents
* the module from executing.
* @param signature Signature of the tree, that is its return type and
* argument types. Return type of the signature is set
* to defaultValue.getType() so that these are consistent.
* @param numAncestors The number of ancestor IDs to store in the ancestry list.
**/

ModuleHandler::ModuleHandler(
		TreeContext* treeContext,
	AlgoType defaultValue,
	const FunctorSignature& signature,
	unsigned int numAncestors,
	ModuleID moduleID
): _moduleToken((treeContext->modules().insert(ModuleContent(treeContext, defaultValue, signature, numAncestors, moduleID)))->first._moduleToken) {}

/**
* Constructor.
* @param moduleToken StorageToken under which the associated data is stored in
* the ContextBlock_Modules.
* @param ContextBlock_Modules The ContextBlock_Modules in which the associated data is stored.
**/

ModuleHandler::ModuleHandler(
	StorageToken moduleToken
): _moduleToken(moduleToken) {}

/**
* Destructor. If there is a valid pointer to ContextBlock_Modules in which this ModuleHandler
* stores the associated data, these data are erased.
*/

ModuleHandler::~ModuleHandler() {}

} //namespace algotree
