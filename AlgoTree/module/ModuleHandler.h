#ifndef MODULE_H_
#define MODULE_H_

#include "../system.h"
#include "../AlgoType.h"
#include "../FunctorSignature.h"
#include "ModuleID.h"

#include <Systematic/utils/StorageToken.h>

namespace algotree {

//forward declarations
class TreeContext;
class Node;
class ProcessContext;
class ModuleContent;
class ModuleGenerator;

/**
* @class ModuleHandler
* @author Michal Gregor
* A handle class that wraps ModuleContent and takes care of storing it in and
* retrieving it from a TreeContext. Thus it works on a context-based principle
* similar to that used in Variables. Functors that contain Modules should, when
* moved to another Tree call move(oldTreeContext, newTreeContext) so that the
* ModuleHandler is moved/copied into the new TreeContext.
**/
class ALGOTREE_API ModuleHandler {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	friend class ModuleHandlerCompare;

	//! Module token that helps to keep track of how many ModuleHandlers
	//! keep reference to a particular ModuleContent.
	StorageToken _moduleToken;

public:
	void move(TreeContext* oldTreeContext, TreeContext* newTreeContext, bool strictRemap = true);
	ModuleContent& operator()(TreeContext* treeContext);
	const ModuleContent& operator()(const TreeContext* treeContext) const;

	ModuleHandler& operator=(const ModuleHandler& obj);
	ModuleHandler(const ModuleHandler& obj);

	//! Returns true if the token has been reset (NULL ptr).
	bool isValid() const {return _moduleToken.isValid();}

	//! Resets the token, thus making the StorageToken invalid.
	void reset() {_moduleToken.reset();}
	//! Resets the token and in addition, deletes the associated Module content
	//! when appropriate (not used by other handlers, not an ADF, ...).
	void reset(TreeContext* treeContext);

	ModuleHandler();

	ModuleHandler(
		const shared_ptr<ModuleGenerator>& moduleGenerator,
		TreeContext* treeContext,
		AlgoType defaultValue,
		const FunctorSignature& signature,
		unsigned int numAncestors = 5,
		ModuleID moduleID = ModuleID()
	);

	ModuleHandler(
		TreeContext* treeContext,
		AlgoType defaultValue,
		const FunctorSignature& signature,
		unsigned int numAncestors = 5,
		ModuleID moduleID = ModuleID()
	);

	explicit ModuleHandler(StorageToken moduleToken);

	~ModuleHandler();
};

//! This implements comparison required for ordering ModuleHandler objects.
struct ModuleHandlerCompare {
	bool operator()(const ModuleHandler& first, const ModuleHandler& second) const {
		return first._moduleToken < second._moduleToken;
	}
};

}//namespace algotree

#endif /* MODULE_H_ */
