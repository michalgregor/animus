#include "ModuleGenerator.h"

namespace algotree {

/**
* Boost serialization function.
**/

void ModuleGenerator::serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

void ModuleGenerator::serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

/**
 * Empty body of the pure virtual destructor.
 */

ModuleGenerator::~ModuleGenerator() {}

} //namespace algotree
