#include "ModuleGenerator_Common.h"
#include "../NodeFunctor/FormalParameter.h"

namespace algotree {

/**
* Boost serialization function.
**/

void ModuleGenerator_Common::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<ModuleGenerator>(*this);
	ar & _functorRegister;
	ar & _signature;
	ar & _rootFunctor;
	ar & _defaultDepth;
	ar & _defaultValue;
	ar & _numAncestors;
}

void ModuleGenerator_Common::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<ModuleGenerator>(*this);
	ar & _functorRegister;
	ar & _signature;
	ar & _rootFunctor;
	ar & _defaultDepth;
	ar & _defaultValue;
	ar & _numAncestors;
}

/**
 * Adds FormalArguments of types specified in the argument list to the
 * FunctorRegister.
 */

void ModuleGenerator_Common::addArgFunctorsToRegister() {
	TypeList::Handles& argHandles(_signature.getArgTypes().handles);

	for(TypeList::Handles::iterator iter = argHandles.begin(); iter != argHandles.end(); iter++) {
		_functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new FormalParameter(*iter)));
	}
}

/**
 * Sets the return type of the root node. This also has the effect of reseting
 * the root functor.
 */

void ModuleGenerator_Common::setRootType(TypeID type) {
	_signature.setReturnType(type);
	_rootFunctor = intrusive_ptr<NodeFunctor>();
}

/**
* Sets the functor register. FormalParameter functor should not be added
* manually to the functorRegister as this is handled automatically, using
* the specified argument list.
**/

void ModuleGenerator_Common::setFunctorRegister(const FunctorRegister& functorRegister) {
	_functorRegister = functorRegister;
	addArgFunctorsToRegister();
}

/**
 * Sets the root functor. Root type information is modified accordingly.
 */

void ModuleGenerator_Common::setRootFunctor(const intrusive_ptr<NodeFunctor>& rootFunctor) {
	_rootFunctor = rootFunctor;
	if(_rootFunctor) _signature.setReturnType(_rootFunctor->getReturnType());
}

ModuleHandler ModuleGenerator_Common::operator()(TreeContext* treeContext) {
	return generate(treeContext, _defaultDepth);
}

ModuleHandler ModuleGenerator_Common::generate(TreeContext* treeContext) {
	return generate(treeContext, _defaultDepth);
}

ModuleHandler ModuleGenerator_Common::generate(TreeContext* treeContext, unsigned int depth) {
	ModuleHandler module(shared_from_this(), treeContext, _defaultValue, _signature, _numAncestors);
	Node& root(module(treeContext).modify());

	if(_rootFunctor) {
		root.setFunctor(_rootFunctor);
		generate(root, depth);
	} else {
		replace(root, depth);
	}

	return module;
}

void ModuleGenerator_Common::generate(Node& parent) {
	generate(parent, _defaultDepth);
}

void ModuleGenerator_Common::replace(Node& parent) {
	replace(parent, _defaultDepth);
}

/**
* Assignment operator.
**/

ModuleGenerator_Common& ModuleGenerator_Common::operator=(const ModuleGenerator_Common& obj) {
	if(&obj != this) {
		ModuleGenerator::operator=(obj);
		enable_shared_from_this<ModuleGenerator_Common>::operator=(obj);

		_functorRegister = obj._functorRegister;
		_signature = obj._signature;

		_rootFunctor = intrusive_ptr<NodeFunctor>(clone(obj._rootFunctor));

		_defaultDepth = obj._defaultDepth;
		_defaultValue = obj._defaultValue;
		_numAncestors = obj._numAncestors;
	}

	return *this;
}

/**
* Copy constructor.
**/

ModuleGenerator_Common::ModuleGenerator_Common(const ModuleGenerator_Common& obj):
	ModuleGenerator(obj), enable_shared_from_this<ModuleGenerator_Common>(obj),
	_functorRegister(obj._functorRegister), _signature(obj._signature),
	_rootFunctor(clone(obj._rootFunctor)),
	_defaultDepth(obj._defaultDepth), _defaultValue(obj._defaultValue),
	_numAncestors(obj._numAncestors) {}

/**
* Constructor.
* @param defaultValue Data returned when maximum recursion limit prevents
* the module from executing.
* @param signature Signature of the tree, that is its return type and
* argument types. If defaultData.getType() is not convertible to the return
* type as specified in the signature a TracedError_InvalidArgument is thrown.
* @param numAncestors The number of ancestor IDs to store in the ancestry list.
* @param defaultDepth The default depth of a tree - used for trees generated
* using operator()().
**/

ModuleGenerator_Common::ModuleGenerator_Common(
	AlgoType defaultValue,
	const FunctorSignature& signature,
	unsigned int defaultDepth,
	unsigned int numAncestors
): ModuleGenerator(), enable_shared_from_this<ModuleGenerator_Common>(),
_functorRegister(), _signature(signature), _rootFunctor(),
_defaultDepth(defaultDepth), _defaultValue(defaultValue),
_numAncestors(numAncestors) {
	if(!AlgoConverterRegister().convertible(_defaultValue.getType(), _signature.getReturnType()))
			throw TracedError_InvalidArgument("The provided default data is not convertible to the return type as specified in FunctorSignature.");
}

/**
* Constructor.
* @param defaultValue Data returned when maximum recursion limit prevents
* the module from executing.
* @param signature Signature of the tree, that is its return type and
* argument types. If defaultData.getType() is not convertible to the return
* type as specified in the signature a TracedError_InvalidArgument is thrown.
* @param numAncestors The number of ancestor IDs to store in the ancestry list.
* @param functorRegister A shared_ptr to the functor register. FormalParameter
* functors should not be added manually to the functorRegister as
* this is handled automatically, using the specified argument list.
* @param defaultDepth The default depth of a tree - used for trees generated
* using operator()().
**/

ModuleGenerator_Common::ModuleGenerator_Common(
	const FunctorRegister& functorRegister,
	AlgoType defaultValue,
	const FunctorSignature& signature,
	unsigned int defaultDepth,
	unsigned int numAncestors
): ModuleGenerator(), enable_shared_from_this<ModuleGenerator_Common>(),
_functorRegister(functorRegister), _signature(signature), _rootFunctor(),
_defaultDepth(defaultDepth), _defaultValue(defaultValue),
_numAncestors(numAncestors) {
	if(!AlgoConverterRegister().convertible(_defaultValue.getType(), _signature.getReturnType()))
		throw TracedError_InvalidArgument("The provided default data is not convertible to the return type as specified in FunctorSignature.");

	addArgFunctorsToRegister();
}

/**
* Default constructor for use by serialization.
**/

ModuleGenerator_Common::ModuleGenerator_Common(): ModuleGenerator(),
enable_shared_from_this<ModuleGenerator_Common>(), _functorRegister(),
_signature(), _rootFunctor(), _defaultDepth(), _defaultValue(),
_numAncestors() {}

}//namespace algotree
