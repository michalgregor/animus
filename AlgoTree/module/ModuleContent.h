#ifndef ALGO_MODULE_H_INCLUDED
#define ALGO_MODULE_H_INCLUDED

#include "../system.h"
#include "../Node.h"

#include "detail/ContextBlock_Modules_Decl.h"
#include "ModuleGenerator.h"

#include <Systematic/container/circular_buffer.h>
#include <list>

namespace algotree {

class ProcessContext;
class ModuleHandler;

/**
 * Class used as storage unit in ContextBlock_Modules. It stores the basic data required
 * by modules (signature, the code itself, ...). It also handles updating indices
 * within ContextBlock_Modules that allow for fast lookup by return type, signature, etc.
 *
 * To provide a ModuleContent with a pointer to itself, ContextBlock_Modules calls
 * setContextBlock_Modules(). When assigning, pointer to ContextBlock_Modules remains unchanged, only the
 * data is copied and the index updated accordingly. When swapping, everything
 * is swapped and the index updated accordingly. When constructing whether by
 * specifying parameters of the module or using copy-construction, ContextBlock_Modules
 * pointer remains NULL until set using setContextBlock_Modules().
 */
class ALGOTREE_API ModuleContent {
private:
	friend class boost::serialization::access;

    void serialize(OArchive& ar, const unsigned int version);
	void serialize(IArchive& ar, const unsigned int version);

public:
	typedef circular_buffer<ModuleID> IDContainer;
	typedef IDContainer::size_type size_type;

private: //module-related data
	//! ID of the module.
	ModuleID _moduleID;
	//! Ancestry of the module.
	IDContainer _ancestry;
	//! Data returned when maximum recursion limit prevents the module from
	//! executing. If the contained data is of void type and the return type
	//! of the Node is not void, an exception is thrown instead of returning
	//! the default value.
	AlgoType _defaultData;

	//! Root Node.
	Node _root;
	//! FunctorSignature of the tree: its return type and argument types.
	FunctorSignature _signature;
	//! Whether to modify ID when incrementID is called. Non-modifying version
	// is
	//! useful when working in ADF mode. Defaults to true however.
	bool _isAdf;

	//! ModuleGenerator that can be used when crossing/mutating the module.
	shared_ptr<ModuleGenerator> _generator;

private: //storage-related data
	//! Pointer back to the ContextBlock_Modules in which this ModuleContent is stored.
	//! This is required in order for ModuleContent to be able to update the
	//! index once any of its features changes.
	ContextBlock_Modules* _moduleList;
	//! Iterator to this ModuleContent.
	TokenMap::iterator _moduleContentIter;

	//! Iterator to an entry in RetMap.
	RetMap::iterator _retIter;
	//! Iterator to an entry in SigMap.
	SigMap::iterator _sigIter;
	//! Iterators to entries in AncMap.
	std::list<AncMap::iterator> _ancIters;
	//! Iterator to an entry in IDMap.
	IDMap::iterator _idIter;

private:
	friend class ContextBlock_Modules;
	friend class ModuleHandler;

	void setContextBlock_Modules(ContextBlock_Modules* moduleList, TokenMap::iterator moduleContentIter);

	void addToIndex();
	void eraseFromIndex();

	void updateAncestorsIndex();
	void updateIDIndex();
	void updateBothIDIndices();

public:
	//!Returns how many ancestor ModuleIDs the ModuleHandler is supposed to store.
	size_type getNumAncestors() const {return _ancestry.capacity();}

	void setNumAncestors(unsigned int numAncestors);
	void resetAncestors();

	ModuleID getID() const {return _moduleID;}
	void setID(ModuleID id);

	bool getIsAdf() const {return _isAdf;}
	void setIsAdf(bool isAdf) {_isAdf = isAdf;}

	systematic::TypeID getReturnType() const {return _signature.getReturnType();}
	const TypeList& getArgTypes() const {return _signature.getArgTypes();}
	const FunctorSignature& getSignature() const {return _signature;}

	shared_ptr<ModuleGenerator> getGenerator() {return _generator;}
	shared_ptr<const ModuleGenerator> getGenerator() const {return _generator;}
	void setGenerator(const shared_ptr<ModuleGenerator>& generator) {_generator = generator;}

	AlgoType process(ProcessContext& context) const;

	Node& modify();

	/**
	 * Provides constant access to the contained subtree. This method has no
	 * side-effects.
	 */

	const Node& access() const {return _root;}

	bool isAncestor(ModuleID moduleID) const;
	bool isRelative(ModuleID moduleID) const;

	const circular_buffer<ModuleID >& getAncestors() const {return _ancestry;}

	void swap(ModuleContent& module);

	ModuleContent& operator=(const ModuleContent& module);

	ModuleContent();
	ModuleContent(const ModuleContent& module);

	ModuleContent(TreeContext* treeContext, const ModuleContent& module);

	ModuleContent(
		const shared_ptr<ModuleGenerator>& moduleGenerator,
		TreeContext* treeContext,
		AlgoType defaultValue,
		const FunctorSignature& signature,
		unsigned int numAncestors = 5,
		ModuleID moduleID = ModuleID()
	);

	ModuleContent(
		TreeContext* treeContext,
		AlgoType defaultValue,
		const FunctorSignature& signature,
		unsigned int numAncestors = 5,
		ModuleID moduleID = ModuleID()
	);
};

} //namespace algotree

#endif // ALGO_MODULE_H_INCLUDED
