#ifndef MODULEGENERATOR_H_
#define MODULEGENERATOR_H_

#include <Systematic/generator/Generator.h>

#include "../system.h"
#include "ModuleHandler.h"
#include "ModuleContent.h"

namespace algotree {

/**
* @class ModuleGenerator
* @author Michal Gregor
**/
class ALGOTREE_API ModuleGenerator {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:

	/**
	* Generates an algorithmic tree from the set of previously added node functors
	* and with the specified depth.
	**/

	virtual ModuleHandler operator()(TreeContext* treeContext) = 0;

	/**
	* Generates an algorithmic tree from the set of previously added node functors
	* and with the specified depth.
	**/

	virtual ModuleHandler generate(TreeContext* treeContext) = 0;

	/**
	* Generates an algorithmic tree from the set of previously added node functors
	* and with the specified depth.
	**/

	virtual ModuleHandler generate(TreeContext* treeContext, unsigned int depth) = 0;

	/**
	* Generates a subtree.
	* @param depth Depth of the subtree. 0 means that the functor should have no
	* subnodes.
	* @param parent The parent node that the subtree should grow from.
	**/

	virtual void generate(Node& parent) = 0;

	/**
	* Generates a subtree. Depth should be set to default minus depth of the parent.
	* @param parent The parent node that the subtree should grow from.
	**/

	virtual void generate(Node& parent, unsigned int depth) = 0;

	/**
	* Generates a subtree. Replaces the node's functor by a new randomly chosen
	* functor of the same type.
	**/

	virtual void replace(Node& parent) = 0;

	/**
	* Generates a subtree. Replaces the node's functor by a new randomly chosen
	* functor of the same type. Depth should be set to default minus depth of the parent.
	**/

	virtual void replace(Node& parent, unsigned int depth) = 0;

	/**
	* Returns the default depth.
	**/

	virtual unsigned int getDefaultDepth() const = 0;

	/**
	* Sets the default depth.
	**/

	virtual void setDefaultDepth(unsigned int depth) = 0;

	virtual ~ModuleGenerator() = 0;
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::ModuleGenerator)

#endif /* MODULEGENERATOR_H_ */
