#include "ModuleGenerator_Generic.h"

namespace algotree {

/**
* Boost serialization function.
**/

void ModuleGenerator_Generic::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<ModuleGenerator_Common>(*this);
	ar & _generator;
}

void ModuleGenerator_Generic::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<ModuleGenerator_Common>(*this);
	ar & _generator;
}

void ModuleGenerator_Generic::generate(Node& parent, unsigned int depth) {
	_generator->generate(_functorRegister, parent, depth);
}

void ModuleGenerator_Generic::replace(Node& parent, unsigned int depth) {
	_generator->replace(_functorRegister, parent, depth);
}

/**
* Assignment operator.
**/

ModuleGenerator_Generic& ModuleGenerator_Generic::operator=(const ModuleGenerator_Generic& obj) {
	if(&obj != this) {
		ModuleGenerator::operator=(obj);
		_generator = shared_ptr<SubtreeGenerator>(clone(_generator));
	}

	return *this;
}

/**
* Copy constructor.
**/

ModuleGenerator_Generic::ModuleGenerator_Generic(const ModuleGenerator_Generic& obj):
	ModuleGenerator_Common(obj), _generator(obj._generator) {}

/**
* Constructor.
* @param generator The underlying subtree generator.
* @param defaultValue Data returned when maximum recursion limit prevents
* the module from executing.
* @param signature Signature of the tree, that is its return type and
* argument types. If defaultData.getType() is not convertible to the return
* type as specified in the signature a TracedError_InvalidArgument is thrown.
* @param numAncestors The number of ancestor IDs to store in the ancestry list.
* @param defaultDepth The default depth of a tree - used for trees generated
* using operator()().
**/

ModuleGenerator_Generic::ModuleGenerator_Generic(
	const shared_ptr<SubtreeGenerator>& generator,
	AlgoType defaultValue,
	const FunctorSignature& signature,
	unsigned int defaultDepth,
	unsigned int numAncestors
):
ModuleGenerator_Common(
	defaultValue,
	signature,
	defaultDepth,
	numAncestors
), _generator(generator) {}

/**
* Constructor.
* @param generator The underlying subtree generator.
* @param defaultValue Data returned when maximum recursion limit prevents
* the module from executing.
* @param signature Signature of the tree, that is its return type and
* argument types. If defaultData.getType() is not convertible to the return
* type as specified in the signature a TracedError_InvalidArgument is thrown.
* @param numAncestors The number of ancestor IDs to store in the ancestry list.
* @param functorRegister A shared_ptr to the functor register. FormalParameter
* functors should not be added manually to the functorRegister as
* this is handled automatically, using the specified argument list.
* @param defaultDepth The default depth of a tree - used for trees generated
* using operator()().
**/

ModuleGenerator_Generic::ModuleGenerator_Generic(
	const FunctorRegister& functorRegister,
	const shared_ptr<SubtreeGenerator>& generator,
	AlgoType defaultValue,
	const FunctorSignature& signature,
	unsigned int defaultDepth,
	unsigned int numAncestors
):
ModuleGenerator_Common(
	functorRegister,
	defaultValue,
	signature,
	defaultDepth,
	numAncestors
), _generator(generator) {}

/**
* Default constructor for use by serialization.
**/

ModuleGenerator_Generic::ModuleGenerator_Generic():
ModuleGenerator_Common(), _generator() {}

}//namespace algotree
