#ifndef MODULEGENERATORGENERIC_H_
#define MODULEGENERATORGENERIC_H_

#include "../system.h"
#include "../TreeGenerator/SubtreeGenerator.h"
#include "ModuleGenerator_Common.h"

namespace algotree {

/**
* @class ModuleGenerator_Generic
* @author Michal Gregor
* A ModuleGenerator based on using a specified SubtreeGenerator.
**/
class ALGOTREE_API ModuleGenerator_Generic: public ModuleGenerator_Common {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	ModuleGenerator_Generic();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The underlying subtree generator.
	shared_ptr<SubtreeGenerator> _generator;

public:
	shared_ptr<SubtreeGenerator> getSubtreeGenerator() {return _generator;}
	void setSubtreeGenerator(const shared_ptr<SubtreeGenerator>& generator) {_generator = generator;}

	virtual void generate(Node& parent, unsigned int depth);
	virtual void replace(Node& parent, unsigned int depth);

	ModuleGenerator_Generic& operator=(const ModuleGenerator_Generic& obj);
	ModuleGenerator_Generic(const ModuleGenerator_Generic& obj);

	ModuleGenerator_Generic(
		const shared_ptr<SubtreeGenerator>& generator,
		AlgoType defaultValue,
		const FunctorSignature& signature,
		unsigned int defaultDepth = 5,
		unsigned int numAncestors = 5
	);

	ModuleGenerator_Generic(
		const FunctorRegister& functorRegister,
		const shared_ptr<SubtreeGenerator>& generator,
		AlgoType defaultValue,
		const FunctorSignature& signature,
		unsigned int defaultDepth = 5,
		unsigned int numAncestors = 5
	);

	virtual ~ModuleGenerator_Generic() {}
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::ModuleGenerator_Generic)

#endif /* MODULEGENERATORGENERIC_H_ */
