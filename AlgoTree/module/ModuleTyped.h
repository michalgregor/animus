#ifndef MODULE_H_
#define MODULE_H_

#include "../system.h"
#include "../AlgoTrait.h"

#include "ModuleHandler.h"

#include <Systematic/type_info.h>
#include <type_traits>

namespace algotree {

namespace detail {
//! Used to signify that the ModuleHandler has no more parameters. This class should
//! never be used directly. Just leave the parameters empty.
class NoParam {};
} //namespace detail

template<
	class ReturnType,
	class Arg1 = detail::NoParam,
	class Arg2 = detail::NoParam,
	class Arg3 = detail::NoParam,
	class Arg4 = detail::NoParam,
	class Arg5 = detail::NoParam
> class ModuleTyped: public AlgoTrait<ModuleHandler> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<AlgoTrait<ModuleHandler> >(*this);
	}

private:
	static const FunctorSignature& getSignature() {
		static FunctorSignature moduleSignature(systematic::type_id<ReturnType>());
		if(!(std::is_same<Arg1, detail::NoParam>::value || moduleSignature.getArgTypes().size())) {
			TypeList& list(moduleSignature.getArgTypes());
			list.push_back<Arg1>();
			if(!std::is_same<Arg2, detail::NoParam>::value) {
				list.push_back<Arg2>();
				if(!std::is_same<Arg3, detail::NoParam>::value) {
					list.push_back<Arg3>();
					if(!std::is_same<Arg4, detail::NoParam>::value) {
						list.push_back<Arg4>();
						if(!std::is_same<Arg5, detail::NoParam>::value) {
							list.push_back<Arg5>();
						}
					}
				}
			}
		}
		return moduleSignature;
	}

public:
	ModuleTyped(const ModuleHandler& module): AlgoTrait<ModuleHandler>(module) {
		if(getSignature() != module.getSignature())
			throw TracedError_InvalidArgument("Signature of the ModuleHandler does not correspond to that of the ModuleTyped.");
	}

	virtual ~ModuleTyped() {
		SYS_EXPORT_INSTANCE();
	}
};

} /* namespace algotree */

SYS_EXPORT_TEMPLATE(algotree::ModuleTyped, 6)
SYS_REGISTER_TYPENAME(algotree::detail::NoParam)

#endif /* MODULE_H_ */
