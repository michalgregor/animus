#ifndef FUNCTORFACTORY_ALGOTRAIT_H_
#define FUNCTORFACTORY_ALGOTRAIT_H_

#include "FunctorFactory.h"
#include "../NodeFunctor/ConstFunctor.h"
#include "../NodeFunctor/NodeFunctor_AlgoTrait.h"

namespace algotree {

/**
* @class FunctorFactory_AlgoTrait
* @author Michal Gregor
* Takes in a FunctorFactory and wraps its return value using
* a NodeFunctor_AlgoTrait<AlgoTraitType>.
* It is required of course that the return type of the NodeFunctor be the same
* as AlgoTraitType::value_type.
* @tparam AlgoTraitType Type of the AlgoTrait.
**/
template<class AlgoTraitType>
class FunctorFactory_AlgoTrait: public FunctorFactory {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<FunctorFactory>(*this);
		ar & _functorFactory;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The contained FunctorFactory.
	intrusive_ptr<FunctorFactory> _functorFactory;

public:
	virtual intrusive_ptr<NodeFunctor> operator()(TreeContext* treeContext);

	virtual bool isBranch() const;
	virtual TypeID getReturnType() const;



	FunctorFactory_AlgoTrait(const intrusive_ptr<FunctorFactory>& functorFactory);
	virtual ~FunctorFactory_AlgoTrait() {
		SYS_EXPORT_INSTANCE()
	}
};

template<class AlgoTraitType>
intrusive_ptr<NodeFunctor> FunctorFactory_AlgoTrait<AlgoTraitType>::
operator()(TreeContext* treeContext) {
	return intrusive_ptr<NodeFunctor>(new NodeFunctor_AlgoTrait<AlgoTraitType>((*_functorFactory)(treeContext)));
}

template<class AlgoTraitType>
bool FunctorFactory_AlgoTrait<AlgoTraitType>::isBranch() const {
	return _functorFactory->isBranch();
}

template<class AlgoTraitType>
TypeID FunctorFactory_AlgoTrait<AlgoTraitType>::getReturnType() const {
	return systematic::type_id<AlgoTraitType>();
}

template<class AlgoTraitType>
FunctorFactory_AlgoTrait<AlgoTraitType>::FunctorFactory_AlgoTrait(
	const intrusive_ptr<FunctorFactory>& functorFactory
): _functorFactory(functorFactory) {}

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::FunctorFactory_AlgoTrait, 1)

#endif /* FUNCTORFACTORY_ALGOTRAIT_H_ */
