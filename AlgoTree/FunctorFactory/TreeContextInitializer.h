#ifndef TREECONTEXTINTIALIZER_H_
#define TREECONTEXTINTIALIZER_H_

#include "../system.h"
#include <boost/intrusive_ptr.hpp>
#include <Systematic/utils/RefCounterVirtual.h>

namespace algotree {

class TreeContext;

/**
* @class TreeContextInitializer
* @author Michal Gregor
*
* This can be used by certain factories to initialize TreeContext. If such
* factory is present in a FunctorRegister, tree generator always calls this
* for the TreeContext of every newly generated Tree. This is useful for
* pre-generation of ADFs and similar.
**/
class ALGOTREE_API TreeContextInitializer: public systematic::RefCounterVirtual {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	virtual void initTreeContext(TreeContext* treeContext) = 0;

	TreeContextInitializer(): systematic::RefCounterVirtual() {}
	virtual ~TreeContextInitializer() = 0;
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::TreeContextInitializer)

#endif /* TREECONTEXTINTIALIZER_H_ */
