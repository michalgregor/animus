#ifndef AlgoTree_FunctorFactory_BlockConstant_H_
#define AlgoTree_FunctorFactory_BlockConstant_H_

#include "../system.h"
#include "FunctorFactory.h"
#include "../NodeFunctor/NodeFunctor_BlockConstant.h"

#include <vector>
#include <Systematic/generator/BoundaryGenerator.h>

#include "TreeContextInitializer.h"

namespace algotree {

//------------------------------------------------------------------------------
//---------------------TreeContextInitializer_BlockConstant---------------------
//------------------------------------------------------------------------------

/**
* @class TreeContextInitializer_BlockConstant
 * @author Michal Gregor
 * A TreeContextInitializer for block constants.
* @tparam Type Type of the constant.
**/
template<class Type>
class TreeContextInitializer_BlockConstant: public TreeContextInitializer {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<TreeContextInitializer>(*this);
		ar & _tokens;
		ar & _constGenerator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! A vector of StorageTokens to be employed by ConstantHandles.
	std::vector<StorageToken> _tokens;
	//! A generator used to generate the constants.
	shared_ptr<Generator<Type> > _constGenerator;

public:
	virtual void initTreeContext(TreeContext* treeContext);

	TreeContextInitializer_BlockConstant<Type>& operator=(const TreeContextInitializer_BlockConstant<Type>& obj);
	TreeContextInitializer_BlockConstant(const TreeContextInitializer_BlockConstant<Type>& obj);

	TreeContextInitializer_BlockConstant();

	TreeContextInitializer_BlockConstant(
		const std::vector<StorageToken>& tokens,
		shared_ptr<Generator<Type> > constGenerator
	);

	virtual ~TreeContextInitializer_BlockConstant();
};

template<class Type>
void TreeContextInitializer_BlockConstant<Type>::
initTreeContext(TreeContext* treeContext) {
	ContextBlock_Constant<Type>& block = treeContext->getAddBlock<ContextBlock_Constant<Type> >();
	unsigned int num(_tokens.size());

	for(unsigned int i = 0; i < num; i++) {
		block.insert(_tokens[i], (*_constGenerator)());
	}
}

/**
* Assignment operator.
**/

template<class Type>
TreeContextInitializer_BlockConstant<Type>& TreeContextInitializer_BlockConstant<Type>::operator=(const TreeContextInitializer_BlockConstant<Type>& obj) {
	if(&obj != this) {
		TreeContextInitializer::operator=(obj);
		_tokens = obj._tokens;
		_constGenerator = obj._constGenerator;
	}

	return *this;
}

/**
* Copy constructor.
**/

template<class Type>
TreeContextInitializer_BlockConstant<Type>::
TreeContextInitializer_BlockConstant(const TreeContextInitializer_BlockConstant<Type>& obj):
	TreeContextInitializer(obj), _tokens(obj._tokens), _constGenerator(obj._constGenerator) {}

/**
* Default constructor.
**/

template<class Type>
TreeContextInitializer_BlockConstant<Type>::TreeContextInitializer_BlockConstant():
_tokens(), _constGenerator() {}

/**
* Constructor.
**/

template<class Type>
TreeContextInitializer_BlockConstant<Type>::
TreeContextInitializer_BlockConstant(
	const std::vector<StorageToken>& tokens,
	shared_ptr<Generator<Type> > constGenerator
): _tokens(tokens), _constGenerator(constGenerator) {}

/**
 * Destructor.
 */

template<class Type>
TreeContextInitializer_BlockConstant<Type>::~TreeContextInitializer_BlockConstant() {
	SYS_EXPORT_INSTANCE();
}

//------------------------------------------------------------------------------
//------------------------FunctorFactory_BlockConstant--------------------------
//------------------------------------------------------------------------------

/**
* @class FunctorFactory_BlockConstant
* @author Michal Gregor
* A functor factory generating constants stored in ContextBlock_Constant.
* @tparam Type Type of the constant.
**/
template<class Type>
class FunctorFactory_BlockConstant: public FunctorFactory {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<FunctorFactory>(*this);
		ar & _tokens;
		ar & _selector;
		ar & _constGenerator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! A vector of StorageTokens to be employed by ConstantHandles.
	std::vector<StorageToken> _tokens;
	//! A random generator used to select a random token.
	systematic::BoundaryGenerator<unsigned int> _selector;
	//! A generator used to generate the constants.
	shared_ptr<Generator<Type> > _constGenerator;

public:
	virtual intrusive_ptr<NodeFunctor> operator()(TreeContext* UNUSED(treeContext));

	/**
	* Returns true if the generated functors take inputs and thus need children
	* nodes and false if they don't.
	**/

	virtual bool isBranch() const {return false;}

	/**
	* Returns TypeID of the type returned by the generated functors.
	**/

	virtual systematic::TypeID getReturnType() const {
		return systematic::type_id<Type>();
	}

	/**
	 * Returns true if this factory implements a TreeContextInitializer.
	 */

	virtual bool hasTreeContextInitializer() const {return true;}

	virtual intrusive_ptr<TreeContextInitializer> getTreeContextIntializer() const;

	FunctorFactory_BlockConstant<Type>& operator=(const FunctorFactory_BlockConstant<Type>& obj);
	FunctorFactory_BlockConstant(const FunctorFactory_BlockConstant<Type>& obj);

	FunctorFactory_BlockConstant(
		unsigned int numConstants = 0,
		shared_ptr<Generator<Type> > constGenerator = shared_ptr<Generator<Type> >()
	);

	virtual ~FunctorFactory_BlockConstant();
};

/**
* The factory interface used to generate new objects.
**/

template<class Type>
intrusive_ptr<NodeFunctor> FunctorFactory_BlockConstant<Type>::
operator()(TreeContext* UNUSED(treeContext)) {
	return new NodeFunctor_BlockConstant<Type>(BlockHandle_Constant<Type>(_tokens.at(_selector())));
}

/**
 * Returns an instance of a TreeContextInitializer. If
 * hasTreeContextInitializer() is false, this should return a NULL ptr.
 */

template<class Type>
intrusive_ptr<TreeContextInitializer> FunctorFactory_BlockConstant<Type>::
getTreeContextIntializer() const {
	return new TreeContextInitializer_BlockConstant<Type>(_tokens, _constGenerator);
}

/**
* Assignment operator.
**/

template<class Type>
FunctorFactory_BlockConstant<Type>& FunctorFactory_BlockConstant<Type>::
operator=(const FunctorFactory_BlockConstant<Type>& obj) {
	if(&obj != this) {
		FunctorFactory::operator=(obj);
		_tokens = obj._tokens;
		_selector = obj._selector;
		_constGenerator = obj._constGenerator;
	}

	return *this;
}

/**
* Copy constructor.
**/

template<class Type>
FunctorFactory_BlockConstant<Type>::
FunctorFactory_BlockConstant(const FunctorFactory_BlockConstant<Type>& obj):
	FunctorFactory(obj), _tokens(obj._tokens), _selector(obj._selector),
	_constGenerator(obj._constGenerator) {}

/**
* Constructor.
**/

template<class Type>
FunctorFactory_BlockConstant<Type>::
FunctorFactory_BlockConstant(
	unsigned int numConstants,
	shared_ptr<Generator<Type> > constGenerator
): _tokens(), _selector(0, numConstants - 1), _constGenerator(constGenerator) {
	_tokens.reserve(numConstants);
	for(unsigned int i = 0; i < numConstants; i++) {
		_tokens.push_back(StorageToken());
	}
}

/**
 * Destructor.
 */

template<class Type>
FunctorFactory_BlockConstant<Type>::~FunctorFactory_BlockConstant() {
	SYS_EXPORT_INSTANCE();
}

}//namespace algotree

SYS_EXPORT_TEMPLATE(algotree::TreeContextInitializer_BlockConstant, 1)
SYS_EXPORT_TEMPLATE(algotree::FunctorFactory_BlockConstant, 1)

#endif //AlgoTree_FunctorFactory_BlockConstant_H_
