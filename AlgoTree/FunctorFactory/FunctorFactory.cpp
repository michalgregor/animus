#include "FunctorFactory.h"

namespace algotree {

/**
* Boost serialization function.
**/

void FunctorFactory::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<systematic::RefCounterVirtual>(*this);
}

void FunctorFactory::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<systematic::RefCounterVirtual>(*this);
}

/**
 * Empty body of the pure virtual destructor.
 */

FunctorFactory::~FunctorFactory() {}

}//namespace algotree
