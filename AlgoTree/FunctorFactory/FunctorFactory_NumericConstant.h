#ifndef ALGO_FACTORY_NUMERICCONSTFACTORY_H_INCLUDED
#define ALGO_FACTORY_NUMERICCONSTFACTORY_H_INCLUDED

#include <Systematic/generator/BoundaryGenerator.h>
#include "FunctorFactory_Constant.h"

namespace algotree {

template<class Type>
class FunctorFactory_NumericConstant: public FunctorFactory_Constant<Type> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<FunctorFactory_Constant<Type> >(*this);
	}

public:
	FunctorFactory_NumericConstant(Type lbound, Type ubound);

	virtual ~FunctorFactory_NumericConstant() {
		SYS_EXPORT_INSTANCE();
	}
};

template<class Type>
FunctorFactory_NumericConstant<Type>::FunctorFactory_NumericConstant(Type lbound, Type ubound):
FunctorFactory_Constant<Type>(shared_ptr<systematic::Generator<Type> >(new systematic::BoundaryGenerator<Type>(lbound, ubound))) {}

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::FunctorFactory_NumericConstant, 1)

#endif // ALGO_FACTORY_NUMERICCONSTFACTORY_H_INCLUDED
