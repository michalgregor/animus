#ifndef ALGO_FACTORY_CONSTFACTORY_H_INCLUDED
#define ALGO_FACTORY_CONSTFACTORY_H_INCLUDED

#include "FunctorFactory.h"
#include "../NodeFunctor/ConstFunctor.h"
#include <Systematic/generator/Generator.h>

namespace algotree {

/**
* @class FunctorFactory_Constant
* @author Michal Gregor
* Implements the ephemeral constant concept - a new random constant is generated
* every time the factory is asked to create one.
**/
template<class DataType>
class FunctorFactory_Constant: public FunctorFactory {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<FunctorFactory>(*this);
		ar & _generator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The underlying generator.
	shared_ptr<systematic::Generator<DataType> > _generator;

public:
	virtual intrusive_ptr<NodeFunctor> operator()(TreeContext* UNUSED(treeContext));

	virtual bool isBranch() const;
	virtual TypeID getReturnType() const;

	shared_ptr<systematic::Generator<DataType> > getGenerator();
	shared_ptr<const systematic::Generator<DataType> > getGenerator() const;
	void setGenerator(const shared_ptr<systematic::Generator<DataType> >& generator);

	FunctorFactory_Constant(const shared_ptr<systematic::Generator<DataType> >& generator);
	virtual ~FunctorFactory_Constant();
};

/**
* Generates a new random constant every time.
**/

template<class DataType>
intrusive_ptr<NodeFunctor> FunctorFactory_Constant<DataType>::operator()(TreeContext* UNUSED(treeContext)) {
	if(!_generator) throw TracedError_InvalidPointer("NULL pointer to _generator.");
	return intrusive_ptr<NodeFunctor>(new ConstFunctor<DataType>((*_generator)()));
}

/**
* Returns true if the generated functors take inputs and thus need children
* nodes and false if they don't.
**/

template<class DataType>
bool FunctorFactory_Constant<DataType>::isBranch() const {
	return false;
}

/**
* Returns TypeID of the type returned by the generated functors.
**/

template<class DataType>
TypeID FunctorFactory_Constant<DataType>::getReturnType() const {
	return systematic::type_id<DataType>();
}

/**
* Returns a pointer to the underlying generator.
**/

template<class DataType>
shared_ptr<systematic::Generator<DataType> > FunctorFactory_Constant<DataType>::
getGenerator() {
	return _generator;
}

/**
* Returns a pointer to the underlying generator.
**/

template<class DataType>
shared_ptr<const systematic::Generator<DataType> > FunctorFactory_Constant<DataType>::
getGenerator() const {
	return _generator;
}

/**
* Sets the underlying generator.
**/

template<class DataType>
void FunctorFactory_Constant<DataType>::
setGenerator(const shared_ptr<systematic::Generator<DataType> >& generator) {
	_generator = generator;
}

/**
* Constructor.
**/

template<class DataType>
FunctorFactory_Constant<DataType>::
FunctorFactory_Constant(const shared_ptr<systematic::Generator<DataType> >& generator):
_generator(generator) {}

/**
* Virtual destructor.
**/

template<class DataType>
FunctorFactory_Constant<DataType>::~FunctorFactory_Constant() {
	SYS_EXPORT_INSTANCE()
}

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::FunctorFactory_Constant, 1)

#endif // ALGO_FACTORY_CONSTFACTORY_H_INCLUDED
