#include "TreeContextInitializer.h"

namespace algotree {

/**
* Boost serialization function.
**/

void TreeContextInitializer::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<systematic::RefCounterVirtual>(*this);
}

void TreeContextInitializer::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<systematic::RefCounterVirtual>(*this);
}

/**
 * Empty body of the pure virtual destructor.
 */

TreeContextInitializer::~TreeContextInitializer() {}

}//namespace algotree
