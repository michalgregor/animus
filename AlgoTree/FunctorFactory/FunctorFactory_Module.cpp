#include "FunctorFactory_Module.h"
#include "../Context/TreeContext.h"

namespace algotree {

//------------------------------------------------------------------------------
//-------------------------TreeContextInitializer_Module------------------------
//------------------------------------------------------------------------------

/**
* Boost serialization function.
**/

void TreeContextInitializer_Module::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeContextInitializer>(*this);
	ar & _signature;
	ar & _moduleGenerator;
	ar & _numModules;
}

void TreeContextInitializer_Module::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeContextInitializer>(*this);
	ar & _signature;
	ar & _moduleGenerator;
	ar & _numModules;
}

void TreeContextInitializer_Module::initTreeContext(TreeContext* treeContext) {
	for(unsigned int i = 0; i < _numModules; i++) {
		ModuleHandler handler = (*_moduleGenerator)(treeContext);
	}
}

/**
 * Assignment operator.
 */

TreeContextInitializer_Module& TreeContextInitializer_Module::
operator=(const TreeContextInitializer_Module& obj) {
	if(this != &obj) {
		TreeContextInitializer::operator=(obj);
		_signature = obj._signature;
		_moduleGenerator = obj._moduleGenerator;
		_numModules = obj._numModules;
	}

	return *this;
}

/**
 * Copy constructor.
 */

TreeContextInitializer_Module::
TreeContextInitializer_Module(const TreeContextInitializer_Module& obj):
TreeContextInitializer(obj), _signature(obj._signature),
_moduleGenerator(clone(obj._moduleGenerator)), _numModules(obj._numModules) {}

/**
 * Default constructor. For use by serialization.
 */

TreeContextInitializer_Module::TreeContextInitializer_Module(): _signature(),
_moduleGenerator(), _numModules() {}

/**
 * Constructor.
 * @param signature Function signature of the generated modules.
 * @param moduleGenerator The underlying module generator.
 * @param numAdfs Number of ADFs to create.
 */

TreeContextInitializer_Module::TreeContextInitializer_Module(
	FunctorSignature signature,
	const shared_ptr<ModuleGenerator>& moduleGenerator,
	unsigned int numModules
): _signature(signature), _moduleGenerator(moduleGenerator),
_numModules(numModules) {}

//------------------------------------------------------------------------------
//-------------------------FunctorFactory_Module--------------------------------
//------------------------------------------------------------------------------

/**
* Boost serialization function.
**/

void FunctorFactory_Module::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FunctorFactory>(*this);
	ar & _signature;
	ar & _moduleGenerator;
	ar & _maxModules;
	ar & _pGenerateNew;
	ar & _generator;
}

void FunctorFactory_Module::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FunctorFactory>(*this);
	ar & _signature;
	ar & _moduleGenerator;
	ar & _maxModules;
	ar & _pGenerateNew;
	ar & _generator;
}

intrusive_ptr<NodeFunctor> FunctorFactory_Module::operator()(TreeContext* treeContext) {
	unsigned int count_ = treeContext->modules().count(_signature);
	intrusive_ptr<NodeFunctor> functor;

	//if is not zero and there are either more modules than max. or the random
	//value says not to generate, we do not create a new module, we use an existing one
	if(count_ >= _maxModules || (count_ && _generator() > _pGenerateNew)) {
		functor = intrusive_ptr<NodeFunctor>(new ModuleCaller(treeContext, (treeContext->modules().randEntry(_signature))->first));
	} else {
	//if none of the previously specified conditions applied, generate a new module
		functor = intrusive_ptr<NodeFunctor>(new ModuleCaller(treeContext, (*_moduleGenerator)(treeContext)));
	}

	return functor;
}

bool FunctorFactory_Module::hasTreeContextInitializer() const {
	return static_cast<bool>(_treeContextInitializer);
}

intrusive_ptr<TreeContextInitializer> FunctorFactory_Module::
getTreeContextIntializer() const {
	return _treeContextInitializer;
}

bool FunctorFactory_Module::isBranch() const {
	return _signature.getArgTypes().size();
}

TypeID FunctorFactory_Module::getReturnType() const {
	return _signature.getReturnType();
}

/**
 * Sets whether a TreeContextInitializer is to be used to pre-create modules.
 */

void FunctorFactory_Module::setPrecreateModules(bool precreateModules) {
	if(precreateModules) _treeContextInitializer = intrusive_ptr<TreeContextInitializer_Module>(new TreeContextInitializer_Module(_signature, _moduleGenerator, _maxModules));
	else _treeContextInitializer.reset();
}

/**
* Assignment operator.
**/

FunctorFactory_Module& FunctorFactory_Module::operator=(const FunctorFactory_Module& obj) {
	if(&obj != this) {
		FunctorFactory::operator=(obj);
		_signature = obj._signature;

		_moduleGenerator = shared_ptr<ModuleGenerator>(clone(obj._moduleGenerator));

		_maxModules = obj._maxModules;
		_pGenerateNew = obj._pGenerateNew;
		_generator = obj._generator;

		if(obj._treeContextInitializer) _treeContextInitializer = intrusive_ptr<TreeContextInitializer_Module>(clone(obj._treeContextInitializer));
		else _treeContextInitializer.reset();
	}	
	return *this;
}

/**
* Copy constructor.
**/

FunctorFactory_Module::FunctorFactory_Module(const FunctorFactory_Module& obj):
FunctorFactory(obj), _signature(obj._signature),
_moduleGenerator(clone(obj._moduleGenerator)),
_maxModules(obj._maxModules), _pGenerateNew(obj._pGenerateNew), _generator(0, 1),
_treeContextInitializer(clone(obj._treeContextInitializer)) {}

/**
* Constructor.
* @param signature Function signature of the generated modules.
* @param moduleGenerator The underlying module generator.
**/	

FunctorFactory_Module::FunctorFactory_Module(
	FunctorSignature signature,
	const shared_ptr<ModuleGenerator>& moduleGenerator,
	unsigned int maxModules,
	ProbabilityType pGenerateNew,
	bool precreateModules
): _signature(signature), _moduleGenerator(moduleGenerator),
_maxModules(maxModules), _pGenerateNew(pGenerateNew), _generator(0, 1),
_treeContextInitializer((precreateModules)?(new TreeContextInitializer_Module(_signature, _moduleGenerator, _maxModules)):(NULL)) {}

/**
* Constructor.
**/

FunctorFactory_Module::FunctorFactory_Module(): _signature(), _moduleGenerator(),
_maxModules(3), _pGenerateNew(0.4f), _generator(0, 1), _treeContextInitializer() {}

}//namespace algotree
