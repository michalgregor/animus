#ifndef ALGO_FACTORY_CLONEFUNCTORFACTORY_H_INCLUDED
#define ALGO_FACTORY_CLONEFUNCTORFACTORY_H_INCLUDED

#include "FunctorFactory.h"

namespace algotree {

/**
* @class FunctorFactory_CloneFunctor
* @author Michal Gregor
* Makes a new NodeFunctor by copying the provided template object.
* This can only be used with types that are clone-able using clone().
**/
class ALGOTREE_API FunctorFactory_CloneFunctor: public FunctorFactory {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//!Stores the object.
	intrusive_ptr<NodeFunctor> _functor;

public:
	/**
	* Returns the stored template object.
	**/

	intrusive_ptr<NodeFunctor> getObject() {
		return _functor;
	}

	/**
	* Sets the template object.
	**/

	void setObject(const intrusive_ptr<NodeFunctor>& functor) {
		_functor = functor;
	}

	virtual bool isBranch() const;
	virtual TypeID getReturnType() const;
	virtual intrusive_ptr<NodeFunctor> operator()(TreeContext* UNUSED(treeContext));

	FunctorFactory_CloneFunctor();
	explicit FunctorFactory_CloneFunctor(const intrusive_ptr<NodeFunctor>& functor);

	virtual ~FunctorFactory_CloneFunctor();
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::FunctorFactory_CloneFunctor)

#endif // ALGO_FACTORY_CLONEFUNCTORFACTORY_H_INCLUDED
