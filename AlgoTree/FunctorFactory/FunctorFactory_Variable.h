#ifndef ALGO_FACTORY_VARIABLEFACTORY_H_INCLUDED
#define ALGO_FACTORY_VARIABLEFACTORY_H_INCLUDED

#include <vector>
#include <Systematic/type_info.h>
#include <Systematic/generator/BoundaryGenerator.h>
#include <Systematic/utils/StorageToken.h>
#include "FunctorFactory.h"
#include "../NodeFunctor/VariableFunctor.h"

namespace algotree {

/**
* @class FunctorFactory_Variable
* @author Michal Gregor
* Creates ConstFunctors carrying Variable objects. The probability that a new
* variable will be generated can be specified. If a new variable is not
* generated, one of the existing variables (previously created) is chosen.
**/

template<class Type>
class FunctorFactory_Variable: public FunctorFactory {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<FunctorFactory>(*this);
		ar & _vars;
		ar & _pNewVar;
		ar & _generator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Vector of existing storage tokens that variables will use.
	mutable std::vector<StorageToken> _vars;
	//! Probability that a new variable will be generated. If a new variable is
	//! not generated, one of the existing variables (previously created) is
	//! chosen. Should be very low or 0 not to generate too many variables.
	ProbabilityType _pNewVar;
	//! A generator used to decide whether to generate a new variable.
	BoundaryGenerator<ProbabilityType> _generator;

public:

//-------------------inherited from FunctorFactory------------------------------

	/**
	* Returns true if the generated functors take inputs and thus need children
	* nodes and false if they don't.
	**/

	virtual bool isBranch() const {
		return false;
	}

	/**
	* Returns TypeID of the type returned by the generated functors.
	**/

	virtual TypeID getReturnType() const {
		return systematic::type_id<Variable<Type> >();
	}

//----------------------the actual generation function--------------------------

	/**
	* Creates a new object and returns it.
	**/

	virtual intrusive_ptr<NodeFunctor> operator()(TreeContext* UNUSED(treeContext)) {
		if(_vars.size() && _generator() >= _pNewVar) {
			systematic::BoundaryGenerator<unsigned int> selector(0, _vars.size() - 1);
			return intrusive_ptr<NodeFunctor>(new VariableFunctor<Type>(_vars[selector()]));
		} else {
			_vars.push_back(StorageToken());
			return intrusive_ptr<NodeFunctor>(new VariableFunctor<Type>(_vars.back()));
		}
	}

//-----------------------FunctorFactory_Variable-specific-------------------------------

	/**
	* Sets the probability that a new Variable will be generated. If a new
	* variable is not generated, one of the existing variables (previously
	* created) is chosen.
	**/

	void setNewVarProbability(ProbabilityType pNewVar) {
		_pNewVar = pNewVar;
	}

	/**
	* Returns the probability that a new Variable will be generated. If a new
	* variable is not generated, one of the existing variables (previously
	* created) is chosen.
	**/

	ProbabilityType getNewVarProbability() {
		return _pNewVar;
	}

	/**
	* Constructor.
	* @param numInitialVars The number of variables to pre-generate. Other may
	* be generated at selection later with probability pNewVar.
	* @param pNewVar The probability that a new Variable will be generated.
	* If a new variable is not generated, one of the existing variables
	* (previously created) is chosen. Probability pNewVar should not be very
	* high so as to prevent generation of a multitude of variables most of which
	* will only be used once.
	**/

	FunctorFactory_Variable(unsigned int numInitialVars, ProbabilityType pNewVar = 0.0f):
	_vars(), _pNewVar(pNewVar), _generator(0.0f, 1.0f) {
		_vars.reserve(numInitialVars);
		for(unsigned int i = 0; i < numInitialVars; i++)
			_vars.push_back(StorageToken());
	}

	/**
	* Virtual destructor.
	**/

	virtual ~FunctorFactory_Variable() {
		SYS_EXPORT_INSTANCE()
	}
};

} //namespace algotree

SYS_EXPORT_TEMPLATE(algotree::FunctorFactory_Variable, 1)

#endif // ALGO_FACTORY_VARIABLEFACTORY_H_INCLUDED
