#ifndef ALGO_FACTORY_FUNCTORFACTORY_H_INCLUDED
#define ALGO_FACTORY_FUNCTORFACTORY_H_INCLUDED

#include <boost/intrusive_ptr.hpp>
#include "../NodeFunctor/NodeFunctor.h"
#include <Systematic/utils/RefCounterVirtual.h>
#include "TreeContextInitializer.h"

namespace algotree {

using boost::intrusive_ptr;
class Tree;

class ALGOTREE_API FunctorFactory: public systematic::RefCounterVirtual {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:

	/**
	* The factory interface used to generate new objects.
	**/

	virtual intrusive_ptr<NodeFunctor> operator()(TreeContext* UNUSED(treeContext)) = 0;

	/**
	* Returns true if the generated functors take inputs and thus need children
	* nodes and false if they don't.
	**/

	virtual bool isBranch() const = 0;

	/**
	* Returns TypeID of the type returned by the generated functors.
	**/

	virtual systematic::TypeID getReturnType() const = 0;

	/**
	 * Returns true if this factory implements a TreeContextInitializer.
	 */

	virtual bool hasTreeContextInitializer() const {return false;}

	/**
	 * Returns an instance of a TreeContextInitializer. If
	 * hasTreeContextInitializer() is false, this should return a NULL ptr.
	 */

	virtual intrusive_ptr<TreeContextInitializer> getTreeContextIntializer() const {return intrusive_ptr<TreeContextInitializer>();}

	/**
	* Virtual destructor.
	**/

	virtual ~FunctorFactory() = 0;
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::FunctorFactory)

#endif // ALGO_FACTORY_FUNCTORFACTORY_H_INCLUDED
