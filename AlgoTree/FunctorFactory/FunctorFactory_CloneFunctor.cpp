#include "FunctorFactory_CloneFunctor.h"

namespace algotree {

/**
* Boost serialization function.
**/

void FunctorFactory_CloneFunctor::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FunctorFactory>(*this);
	ar & _functor;
}

void FunctorFactory_CloneFunctor::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FunctorFactory>(*this);
	ar & _functor;
}

/**
* Returns true if the generated functors take inputs and thus need children
* nodes and false if they don't.
**/

bool FunctorFactory_CloneFunctor::isBranch() const {
	if(!_functor) throw TracedError_InvalidPointer("NULL pointer to object.");
	return _functor->getNumArgs();
}

/**
* Returns TypeID of the type returned by the generated functors.
**/

TypeID FunctorFactory_CloneFunctor::getReturnType() const {
	if(!_functor) throw TracedError_InvalidPointer("NULL pointer to object.");
	return _functor->getReturnType();
}

/**
* Creates a new object and returns it.
**/

intrusive_ptr<NodeFunctor> FunctorFactory_CloneFunctor::
operator()(TreeContext* UNUSED(treeContext)) {
	if(!_functor) throw TracedError_InvalidPointer("NULL pointer to object.");
	return intrusive_ptr<NodeFunctor>(clone(_functor));
}

/**
* Default constructor.
**/

FunctorFactory_CloneFunctor::FunctorFactory_CloneFunctor(): _functor() {}

/**
* Constructor.
* @param obj The template object that will be cloned when operator() is
* called.
**/

FunctorFactory_CloneFunctor::
FunctorFactory_CloneFunctor(const intrusive_ptr<NodeFunctor>& functor):
_functor(functor) {}

/**
* Virtual destructor.
**/

FunctorFactory_CloneFunctor::~FunctorFactory_CloneFunctor() {}

} //namespace algotree
