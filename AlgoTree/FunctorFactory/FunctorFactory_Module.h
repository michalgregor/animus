#ifndef ALGO_FACTORY_MODULEFACTORY_H_INCLUDED
#define ALGO_FACTORY_MODULEFACTORY_H_INCLUDED

#include "../system.h"
#include "FunctorFactory.h"
#include "../NodeFunctor/ModuleCaller.h"
#include "../module/ModuleGenerator.h"

#include <Systematic/generator/BoundaryGenerator.h>

namespace algotree {

/**
* @class TreeContextInitializer_Module
* @author Michal Gregor
* Pre-creates a given number of Modules in the supplied TreeContext.
**/
class ALGOTREE_API TreeContextInitializer_Module: public TreeContextInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Function signature of the generated modules.
	FunctorSignature _signature;
	//! The underlying module generator.
	shared_ptr<ModuleGenerator> _moduleGenerator;
	//! Stores the number of modules to generate.
	unsigned int _numModules;

public:
	const FunctorSignature& getSignature() const {return _signature;}
	shared_ptr<ModuleGenerator> getModuleGenerator() {return _moduleGenerator;}
	unsigned int getNumModules() const {return _numModules;}

	virtual void initTreeContext(TreeContext* treeContext);

	TreeContextInitializer_Module& operator=(const TreeContextInitializer_Module& obj);
	TreeContextInitializer_Module(const TreeContextInitializer_Module& obj);

	TreeContextInitializer_Module();

	TreeContextInitializer_Module(
		FunctorSignature signature,
		const shared_ptr<ModuleGenerator>& moduleGenerator,
		unsigned int numModules
	);

	virtual ~TreeContextInitializer_Module() {}
};

/**
* @class FunctorFactory_Module
* @author Michal Gregor
**/

class ALGOTREE_API FunctorFactory_Module: public FunctorFactory {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	FunctorFactory_Module();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Function signature of the generated modules.
	FunctorSignature _signature;
	//! The underlying module generator.
	shared_ptr<ModuleGenerator> _moduleGenerator;
	//! Maximum number of Modules to generate. After that, reuse the existing
	//! Modules. (unsigned int)-1 means no limit.
	unsigned int _maxModules;
	//! The probability that a new ModuleHandler will be generated (of course
	//! the probability is not applied when the maximum number of Modules has
	//! been reached or when there are no Modules with such signature available).
	ProbabilityType _pGenerateNew;
	//! The generator associated with the probability that a new ModuleHandler will
	//! be generated.
	BoundaryGenerator<ProbabilityType> _generator;
	//! This is either NULL or stores the TreeContextInitializer.
	intrusive_ptr<TreeContextInitializer_Module> _treeContextInitializer;

public:
	virtual intrusive_ptr<NodeFunctor> operator()(TreeContext* treeContext);

	virtual bool isBranch() const;
	virtual TypeID getReturnType() const;

	bool getPrecreateModules() const {return static_cast<bool>(_treeContextInitializer);}
	void setPrecreateModules(bool precreateModules);

	virtual bool hasTreeContextInitializer() const;
	virtual intrusive_ptr<TreeContextInitializer> getTreeContextIntializer() const;

	FunctorFactory_Module& operator=(const FunctorFactory_Module& obj);
	FunctorFactory_Module(const FunctorFactory_Module& obj);

	FunctorFactory_Module(
		FunctorSignature signature,
		const shared_ptr<ModuleGenerator>& moduleGenerator,
		unsigned int maxModules = 3,
		ProbabilityType pGenerateNew = 0.4f,
		bool precreateModules = false
	);

	virtual ~FunctorFactory_Module() {}
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::FunctorFactory_Module)

#endif // ALGO_FACTORY_MODULEFACTORY_H_INCLUDED
