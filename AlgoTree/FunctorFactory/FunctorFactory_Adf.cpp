#include "FunctorFactory_Adf.h"
#include "../Context/TreeContext.h"

namespace algotree {

//------------------------------------------------------------------------------
//---------------------------TreeContextInitializer_Adf-------------------------
//------------------------------------------------------------------------------

/**
* Boost serialization function.
**/

void TreeContextInitializer_Adf::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeContextInitializer>(*this);
	ar & _signature;
	ar & _moduleGenerator;
	ar & _moduleIDs;
}

void TreeContextInitializer_Adf::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeContextInitializer>(*this);
	ar & _signature;
	ar & _moduleGenerator;
	ar & _moduleIDs;
}

void TreeContextInitializer_Adf::initTreeContext(TreeContext* treeContext) {
	std::vector<ModuleID>::const_iterator iter;
	for(iter = _moduleIDs.begin(); iter != _moduleIDs.end(); iter++) {
		ModuleHandler handler = (*_moduleGenerator)(treeContext);
		ModuleContent& module(handler(treeContext));
		module.setID(*iter);
		module.setIsAdf(true);
	}
}

/**
 * Assignment operator.
 */

TreeContextInitializer_Adf& TreeContextInitializer_Adf::operator=(const TreeContextInitializer_Adf& obj) {
	if(this != &obj) {
		TreeContextInitializer::operator=(obj);
		_signature = obj._signature;
		_moduleGenerator = obj._moduleGenerator;
		_moduleIDs = obj._moduleIDs;
	}

	return *this;
}

/**
 * Copy constructor.
 */

TreeContextInitializer_Adf::TreeContextInitializer_Adf(const TreeContextInitializer_Adf& obj):
TreeContextInitializer(obj), _signature(obj._signature),
_moduleGenerator(clone(obj._moduleGenerator)), _moduleIDs(obj._moduleIDs) {}

/**
 * Default constructor. For use by serialization.
 */

TreeContextInitializer_Adf::TreeContextInitializer_Adf(): _signature(),
_moduleGenerator(), _moduleIDs() {}

/**
 * Constructor.
 * @param signature Function signature of the generated modules.
 * @param moduleGenerator The underlying module generator.
 * @param numAdfs Number of ADFs to create.
 */

TreeContextInitializer_Adf::TreeContextInitializer_Adf(
	FunctorSignature signature,
	const shared_ptr<ModuleGenerator>& moduleGenerator,
	unsigned int numAdfs
): _signature(signature), _moduleGenerator(moduleGenerator), _moduleIDs() {
	_moduleIDs.reserve(numAdfs);
	for(unsigned int i = 0; i < numAdfs; i++) {
		_moduleIDs.push_back(ModuleID());
	}
}

//------------------------------------------------------------------------------
//-------------------------FunctorFactory_Adf-----------------------------------
//------------------------------------------------------------------------------

/**
* Boost serialization function.
**/

void FunctorFactory_Adf::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FunctorFactory>(*this);
	ar & _treeContextInitializer;
	ar & _adfSelector;
}

void FunctorFactory_Adf::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FunctorFactory>(*this);
	ar & _treeContextInitializer;
	ar & _adfSelector;
}


intrusive_ptr<NodeFunctor> FunctorFactory_Adf::operator()(TreeContext* treeContext) {
	//select a random ModuleID and retrieve the corresponding module.
	ModuleID adfID = _adfSelector();
	ContextBlock_Modules::iterator iter = treeContext->modules().entry(adfID);

	if(iter == treeContext->modules().end()) throw TracedError_OutOfRange("No such ADF exists. Perhaps the TreeContext has not been initialized.");

	return intrusive_ptr<NodeFunctor>(new ModuleCaller(treeContext, iter->first));
}

bool FunctorFactory_Adf::isBranch() const {
	return _treeContextInitializer->getSignature().getArgTypes().size();
}

TypeID FunctorFactory_Adf::getReturnType() const {
	return _treeContextInitializer->getSignature().getReturnType();
}

/**
 * Returns true because this factory implements a TreeContextInitializer.
 */

bool FunctorFactory_Adf::hasTreeContextInitializer() const {
	return true;
}

/**
 * Returns an instance of a TreeContextInitializer. If
 * hasTreeContextInitializer() is false, this should return a NULL ptr.
 */

intrusive_ptr<TreeContextInitializer> FunctorFactory_Adf::getTreeContextIntializer() const {
	return _treeContextInitializer;
}

/**
* Assignment operator.
**/

FunctorFactory_Adf& FunctorFactory_Adf::operator=(const FunctorFactory_Adf& obj) {
	if(&obj != this) {
		FunctorFactory::operator=(obj);
		_treeContextInitializer = clone(_treeContextInitializer);
		_adfSelector = obj._adfSelector;
	}
	return *this;
}

/**
* Copy constructor.
**/

FunctorFactory_Adf::FunctorFactory_Adf(const FunctorFactory_Adf& obj):
FunctorFactory(obj), _treeContextInitializer(clone(obj._treeContextInitializer)),
_adfSelector(obj._adfSelector) {}

/**
* Constructor.
* @param signature Function signature of the generated modules.
* @param moduleGenerator The underlying module generator.
**/

FunctorFactory_Adf::FunctorFactory_Adf(
	FunctorSignature signature,
	const shared_ptr<ModuleGenerator>& moduleGenerator,
	unsigned int numAdfs
): _treeContextInitializer(new TreeContextInitializer_Adf(signature, moduleGenerator, numAdfs)),
_adfSelector(_treeContextInitializer->getModuleIDs()) {}

/**
* Constructor.
**/

FunctorFactory_Adf::FunctorFactory_Adf(): _treeContextInitializer(), _adfSelector() {}

}//namespace algotree
