#ifndef ADFFACTORY_H_
#define ADFFACTORY_H_

#include "../system.h"
#include "FunctorFactory.h"
#include "../NodeFunctor/ModuleCaller.h"
#include "../module/ModuleGenerator.h"

#include <Systematic/generator/SpaceSeqGenerator.h>

namespace algotree {

/**
* @class TreeContextInitializer_Adf
* @author Michal Gregor
* Pre-creates a given number of ADFs in the supplied TreeContext.
**/
class ALGOTREE_API TreeContextInitializer_Adf: public TreeContextInitializer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Function signature of the generated modules.
	FunctorSignature _signature;
	//! The underlying module generator.
	shared_ptr<ModuleGenerator> _moduleGenerator;
	//! Stores ModuleIDs of the associated ADF modules.
	std::vector<ModuleID> _moduleIDs;

public:
	const FunctorSignature& getSignature() const {return _signature;}
	const std::vector<ModuleID>& getModuleIDs() const {return _moduleIDs;}
	shared_ptr<ModuleGenerator> getModuleGenerator() {return _moduleGenerator;}
	unsigned int getNumAdfs() const {return _moduleIDs.size();}

	virtual void initTreeContext(TreeContext* treeContext);

	TreeContextInitializer_Adf& operator=(const TreeContextInitializer_Adf& obj);
	TreeContextInitializer_Adf(const TreeContextInitializer_Adf& obj);

	TreeContextInitializer_Adf();

	TreeContextInitializer_Adf(
		FunctorSignature signature,
		const shared_ptr<ModuleGenerator>& moduleGenerator,
		unsigned int numAdfs
	);

	virtual ~TreeContextInitializer_Adf() {}
};

/**
* @class FunctorFactory_Adf
* @author Michal Gregor
* A factory producing Modules ids of which are fixed and common for all trees.
* The number of Modules is also fixed. The contents of an ADF are stored in the
* TreeContext just as normal Modules. The contents of an ADF are generated once
* a caller to it is first inserted.
**/

class ALGOTREE_API FunctorFactory_Adf: public FunctorFactory {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	FunctorFactory_Adf();

private:
	//! The TreeContextInitializer.
	intrusive_ptr<TreeContextInitializer_Adf> _treeContextInitializer;
	//! Stores ModuleIDs of the associated ADF modules in a space-based
	//! generator used to randomly select from these.
	SpaceSeqGenerator<ModuleID> _adfSelector;

public:
	virtual intrusive_ptr<NodeFunctor> operator()(TreeContext* UNUSED(treeContext));

	virtual bool isBranch() const;
	virtual TypeID getReturnType() const;

	virtual bool hasTreeContextInitializer() const;
	virtual intrusive_ptr<TreeContextInitializer> getTreeContextIntializer() const;

	FunctorFactory_Adf& operator=(const FunctorFactory_Adf& obj);
	FunctorFactory_Adf(const FunctorFactory_Adf& obj);

	FunctorFactory_Adf(
		FunctorSignature signature,
		const shared_ptr<ModuleGenerator>& moduleGenerator,
		unsigned int numAdfs = 1
	);

	virtual ~FunctorFactory_Adf() {}
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::FunctorFactory_Adf)
SYS_EXPORT_CLASS(algotree::TreeContextInitializer_Adf)

#endif /* ADFFACTORY_H_ */
