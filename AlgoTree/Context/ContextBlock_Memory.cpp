#include "ContextBlock_Memory.h"

namespace algotree {

/**
* Boost serialization function.
**/

void ContextBlock_Memory::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _dataStore;
}

void ContextBlock_Memory::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _dataStore;
}

/**
* Returns true if there is any data filed under the specified key in the data
 store, false if not.
**/

bool ContextBlock_Memory::exists(const StorageToken& key) const {
	DataStore::const_iterator iter = _dataStore.find(key);
	return iter != _dataStore.end();
}

/**
* Erases the data filed under the specified key (if any) from the data store.
**/

void ContextBlock_Memory::erase(const StorageToken& key) {
	_dataStore.erase(key);
}

/**
* Clears the data store.
**/

void ContextBlock_Memory::clear() {
	_dataStore.clear();
}

/**
* Returns type of the stored data. If no data stored under that key, an
* exception is thrown.
**/

systematic::TypeID ContextBlock_Memory::getDataType(const StorageToken& key) const {
	DataStore::const_iterator iter = _dataStore.find(key);
	if(iter == _dataStore.end()) throw TracedError_Generic("No data has been filed under the specified key.");
	return iter->second.getType();
}

//! Creates a copy of the data store (to deep-copy a PolyHandler, its copy()
//! member has to be invoked), storing the copied data in the provided data
//! store (argument space).

void ContextBlock_Memory::copyDataStoreTo(DataStore& space) const {
	for(DataStore::const_iterator iter = _dataStore.begin(); iter != _dataStore.end(); iter++) {
		space.insert(DataStore::value_type(iter->first, iter->second.copy()));
	}
}

/**
* Assignment. Creates a deep copy of the contained data.
*/

ContextBlock_Memory& ContextBlock_Memory::operator=(const ContextBlock_Memory& storage) {
	if(this != &storage) storage.copyDataStoreTo(_dataStore);
	return *this;
}

/**
* Copy constructor. Creates a deep copy of the contained data.
*/

ContextBlock_Memory::ContextBlock_Memory(const ContextBlock_Memory& storage): _dataStore() {
	storage.copyDataStoreTo(_dataStore);
}

ContextBlock_Memory::ContextBlock_Memory(): _dataStore() {}

} //namespace algotree
