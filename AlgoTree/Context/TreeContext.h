#ifndef ALGO_TREECONTEXT_H_INCLUDED
#define ALGO_TREECONTEXT_H_INCLUDED

#include <Systematic/serialization.h>
#include "../system.h"

#include "Context.h"
#include "../module/ContextBlock_Modules.h"

namespace algotree {

class ALGOTREE_API Tree;

/**
* Used to provide Tree related context such as the list of module to functors
* and functor factories.
*
* ContextBlock_Modules is added automatically upon construction.
*
* It is not safe to use the same Tree object from multiple threads concurrently.
**/

SYS_WNONVIRT_DTOR_OFF
class ALGOTREE_API TreeContext: private Context {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//import interface from Context
	using Context::value_type;
	using Context::difference_type;
	using Context::iterator;
	using Context::const_iterator;
	using Context::reverse_iterator;
	using Context::const_reverse_iterator;
	using Context::begin;
	using Context::end;
	using Context::rbegin;
	using Context::rend;
	using Context::find;
	using Context::insert;
	using Context::erase;
	using Context::clear;
	using Context::addBlock;
	using Context::getAddBlock;
	using Context::getBlock;
	using Context::eraseBlock;

public:
	//! Returns a reference to the list of modules.
	const ContextBlock_Modules& modules() const {return getBlock<ContextBlock_Modules>();}
	//! Returns a reference to the list of modules.
	ContextBlock_Modules& modules() {return getBlock<ContextBlock_Modules>();}

	TreeContext& operator=(const TreeContext& context);
	TreeContext(const TreeContext& context);
	TreeContext();
};
SYS_WNONVIRT_DTOR_ON

} //namespace algotree

SYS_EXPORT_CLASS(algotree::TreeContext)

#endif // ALGO_TREECONTEXT_H_INCLUDED
