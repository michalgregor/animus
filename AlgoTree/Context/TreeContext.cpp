#include "TreeContext.h"

namespace algotree {

/**
* Boost serialization function.
**/

void TreeContext::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & static_cast<Context&>(*this);
}

void TreeContext::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & static_cast<Context&>(*this);
}

/**
* Assignment operator. ContextBlock_Modules is not copied - its contents should be moved by
* calling ModuleHandler::move from the particular NodeFunctors.
**/

TreeContext& TreeContext::operator=(const TreeContext& context) {
	if(this != &context) {
		Context::operator=(context);
	}
	return *this;
}

/**
* Copy constructor. ContextBlock_Modules is not copied - its contents should be moved by
* calling ModuleHandler::move from the particular NodeFunctors.
**/

TreeContext::TreeContext(const TreeContext& context):
Context(context) {}

/**
 * Default constructor.
 */

TreeContext::TreeContext() {
	addBlock(new ContextBlock_Modules);
}

} //namespace algotree
