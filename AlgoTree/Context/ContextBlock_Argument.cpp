#include "ContextBlock_Argument.h"
#include "ProcessContext.h"
namespace algotree {

/**
* Boost serialization function.
**/

void ContextBlock_Argument::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _args;
}

void ContextBlock_Argument::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _args;
}

/**
* Clears the arguments.
**/

void ContextBlock_Argument::clear() {
	_args.clear();
}

/**
* Returns a constant reference to the vector of arguments.
**/

const std::vector<AlgoType >& ContextBlock_Argument::getArgs() const {
	return _args;
}

/**
* Returns a reference to the vector of arguments.
**/

std::vector<AlgoType>& ContextBlock_Argument::getArgs() {
	return _args;
}

/**
* Swaps in arguments from the args vector: the original arguments vector is
* cleared and then its contents are exchanged with args.
**/

void ContextBlock_Argument::swapInArgs(std::vector<AlgoType>& args) {
	_args.swap(args);
}

/**
* Swaps in arguments from the args vector: the original arguments vector is
* cleared and then its contents are exchanged with args.
**/

void ContextBlock_Argument::setArgs(const std::vector<AlgoType>& args) {
	_args = args;
}

/**
* Assignment.
**/

ContextBlock_Argument& ContextBlock_Argument::operator=(const ContextBlock_Argument& context) {
	_args = context._args;
	return *this;
}

/**
* Copy constructor.
**/

ContextBlock_Argument::ContextBlock_Argument(const ContextBlock_Argument& context): _args(context._args) {}

/**
* Constructor.
**/

ContextBlock_Argument::ContextBlock_Argument(): _args() {}

/**
* Constructor.
* @param args Vector of Tree's formal arguments.
**/

ContextBlock_Argument::ContextBlock_Argument(const std::vector<AlgoType>& args): _args(args) {}

} //namespace algotree
