#ifndef AlgoTree_ContextBlock_Constant_H_
#define AlgoTree_ContextBlock_Constant_H_

#include "../system.h"
#include <Systematic/utils/StorageToken.h>

#include <map>

namespace algotree {

/**
* @class ContextBlock_Constant
* @author Michal Gregor
* A context block used for centrally stored (and potentially evolved) constants.
**/

template<class Type>
class ContextBlock_Constant {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int UNUSED(version)) {
		ar & _constants;
	}

	void serialize(OArchive& ar, const unsigned int UNUSED(version)) {
		ar & _constants;
	}

private:
	//! A container that stores the actual data.
	std::map<StorageToken, Type> _constants;

public:
	typedef typename std::map<StorageToken, Type>::iterator iterator;
	typedef typename std::map<StorageToken, Type>::const_iterator const_iterator;
	typedef typename std::map<StorageToken, Type>::reverse_iterator reverse_iterator;
	typedef typename std::map<StorageToken, Type>::const_reverse_iterator const_reverse_iterator;

	//! The type of constants that are stored in the block.
	typedef Type value_type;
	typedef typename std::map<StorageToken, Type>::size_type size_type;

public:
	//! Returns an iterator to the first element stored in the block.
	iterator begin() {return _constants.begin();}
	//! Returns an iterator to the place just *after* the last element stored in
	//! the block.
	iterator end() {return _constants.end();}

	//! Returns a reverse iterator to the last element stored in the block.
	reverse_iterator rbegin() {return _constants.rbegin();}
	//! Returns a reverse iterator to the place just *before* the first element
	//! stored in the block.
	reverse_iterator rend() {return _constants.rend();}

	//! Returns a constant iterator to the first element stored in the block.
	const_iterator begin() const {return _constants.begin();}
	//! Returns a constant iterator to the place just *after* the last element
	//! stored in the block.
	const_iterator end() const {return _constants.end();}

	//! Returns a constant reverse iterator to the last element stored in the
	//! block.
	const_reverse_iterator rbegin() const {return _constants.rbegin();}
	//! Returns a constant reverse iterator to the place just *before* the first
	//! element stored in the block.
	const_reverse_iterator rend() const {return _constants.rend();}

public:
	//! Returns whether the block is empty, i.e. whether its size is 0.
	bool empty() const {return _constants.empty();}

	//! Returns the number of elements stored in the block.
	size_type size() const {return _constants.size();}

	//! Returns an iterator to an element stored under the specified storage
	//! token. If there is no element with such storage token, returns end().
	iterator find(const StorageToken& token) {
		return iterator(_constants.find(token));
	}

	//! Returns an iterator to an element stored under the specified storage
	//! token. If there is no element with such storage token, returns end().
	const_iterator find(const StorageToken& token) const {
		return const_iterator(_constants.find(token));
	}

	//! Erases the element identified by the specified iterator.
	void erase(const iterator& iter) {_constants.erase(iter.getRawIterator());}
	//! Erases the element stored under the specified storage token (if any).
	//! Returns the number of items erased.
	size_type erase(const StorageToken& token) {return _constants.erase(token);}
	//! Erases all elements from range [first, last).
	void erase(iterator first, iterator last) {
		_constants.erase(first.getRawIterator(), last.getRawIterator());
	}

	//! Swaps contents of the two blocks.
	void swap(ContextBlock_Constant& block) {
		_constants.swap(block._constants);
	}

	//! Erases all elements from the block.
	void clear() {_constants.clear();}

public:
	//! Adds the specified element to the block and returns its storage token
	//! and interator in an std::pair.
	std::pair<StorageToken, iterator> insert(const Type& constant) {
		StorageToken token;
		iterator iter = _constants.insert(std::pair<StorageToken, Type>(token, constant)).first;
		return std::pair<StorageToken, iterator>(token, iter);
	}

	//! Adds the specified element to the block under the specified storage
	//! token. If such storage token already exists previous value of the
	//! constant is overwritten. An std::pair is returned, where first is an
	//! iterator to the element and second is a bool that is either true if
	//! a new element was inserted or false if not.
	std::pair<iterator, bool> insert(StorageToken token, const Type& constant) {
		std::pair<typename std::map<StorageToken, Type>::iterator, bool> p = _constants.insert(std::pair<StorageToken, Type>(token, constant));
		return std::pair<iterator, bool>(iterator(p.first), p.second);
	}

	//! Inserts elements from range [first, last) into the block.
	template<class InputIterator>
	void insert(InputIterator first, InputIterator last) {
		_constants.insert(first, last);
	}

	//! Returns an element from the block stored under the specified token. If
	//! there is no element stored under such token, a TracedError_NotAvailable
	//! is thrown.
	const Type& operator[](const StorageToken& token) const {
		typename std::map<StorageToken, Type>::const_iterator iter = _constants.find(token);
		if(iter != _constants.end()) return iter->second;
		else throw TracedError_NotAvailable("There is no element stored under the specified storage token.");
	}

	//! Returns an element from the block stored under the specified token. If
	//! there is no element stored under such token, a TracedError_NotAvailable
	//! is thrown.
	Type& operator[](const StorageToken& token) {
		typename std::map<StorageToken, Type>::iterator iter = _constants.find(token);
		if(iter != _constants.end()) return iter->second;
		else throw TracedError_NotAvailable("There is no element stored under the specified storage token.");
	}

	//! Assignment.
	ContextBlock_Constant& operator=(const ContextBlock_Constant& obj) {
		_constants = obj._constants;
		return *this;
	}

	//! Copy constructor.
	ContextBlock_Constant(const ContextBlock_Constant& obj):
		_constants(obj._constants) {}

	//! Default constructor.
	ContextBlock_Constant(): _constants() {}
};

}//namespace algotree

SYS_REGISTER_TEMPLATENAME(algotree::ContextBlock_Constant, 1)

#endif //AlgoTree_ContextBlock_Constant_H_
