#include "ProcessContext.h"
namespace algotree {

/**
* Boost serialization function.
**/

void ProcessContext::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & static_cast<Context&>(*this);
	ar & _execGuard;
}

void ProcessContext::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & static_cast<Context&>(*this);
	ar & _execGuard;
}

/**
* Creates a new ProcessContext with an independent MemoryContext and
* ContextBlock_Argument but with the same execution guard. This is useful when
* creating a new local scope of the ProcessContext for Modules, Boxes
* and similar.
*/

ProcessContext ProcessContext::newScope() const {
	return ProcessContext(_execGuard);
}

/**
 * Private constructor used when creating new scope (newScope()).
 */

ProcessContext::ProcessContext(const shared_ptr<ExecutionGuard>& execGuard_):
_execGuard(execGuard_) {
	addBlock(new ContextBlock_Memory);
	addBlock(new ContextBlock_Argument);
}

/**
* Constructor
* @param max_runtime The maximum runtime setting. (unsigned int)-1 means no limit.
** @param maxRecursion Sets the maximum level of recursion allowed: (unsigned int)-1 means infinity.
*/

ProcessContext::ProcessContext(unsigned int max_runtime, unsigned int maxRecursion):
_execGuard(new ExecutionGuard(max_runtime, maxRecursion)) {
	addBlock(new ContextBlock_Memory);
	addBlock(new ContextBlock_Argument);
}

/**
* Constructor
* @param args The input arguments (forwarded to ContextBlock_Argument(args)).
* @param max_runtime The maximum runtime setting. (unsigned int)-1 means no limit.
* @param maxRecursion Sets the maximum level of recursion allowed: (unsigned int)-1 means infinity.
*/

ProcessContext::ProcessContext(const std::vector<AlgoType>& args, unsigned int max_runtime, unsigned int maxRecursion):
_execGuard(new ExecutionGuard(max_runtime, maxRecursion)) {
	addBlock(new ContextBlock_Memory);
	addBlock(new ContextBlock_Argument(args));
}

} //namespace algotree
