#include "ExecutionGuard.h"
#include <boost/thread.hpp>
#include <Systematic/math/numeric_cast.h>

namespace algotree {

using boost::posix_time::microsec_clock;

/**
* Boost serialization function.
**/

void ExecutionGuard::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _startTime;
	ar & _timeout;
	ar & _recursion;
	ar & _maxRecursion;
}

void ExecutionGuard::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _startTime;
	ar & _timeout;
	ar & _recursion;
	ar & _maxRecursion;
}

/**
* Methods startRecursion() is used to keep track of recursive calls,
* i.e. calling other trees, modules, ... from the main program.
* Method process() of every such Tree and ModuleHandler calls startRecursion() before
* it executes any of its actions and stores the returned RecursionHolder object
* until its processing is done.
*
* If value of the recursion counter reaches a predefined limit,
* an AlgoRecursionBreak is thrown upon calling startRecursion(). This can be
* forwarded on to the enclosing function or caught by the ModuleHandler/Tree which can
* then return a predefined value instead of the actual result.
*/

RecursionHolder ExecutionGuard::startRecursion() {
	if(_maxRecursion != (unsigned int)-1 && _recursion >= _maxRecursion) throw AlgoRecursionBreak();
	else return RecursionHolder(_recursion);
}

/**
* Resets the recursion counter back to zero. Use with extreme care as this will
* usually disturb the recursion tracking mechanism.
*/

void ExecutionGuard::resetRecursionCounter() {
	_recursion = 0;
}

/**
 * Returns the current recursion level (how many Tree/ModuleHandler calls deep we are).
 */

unsigned int ExecutionGuard::getRecursionLevel() const {
	return _recursion;
}

/**
 * Returns the maximum level of recursion allowed: (unsigned int)-1 means infinity.
 */

unsigned int ExecutionGuard::getMaxRecursion() const {
	return _maxRecursion;
}

/**
 * Sets the maximum level of recursion allowed: (unsigned int)-1 means infinity.
 */

void ExecutionGuard::setMaxRecursion(unsigned int maxRecursion) {
	_maxRecursion = maxRecursion;
}

/**
* This method throws an exception if execution is to be stopped. It is called
* from Node level and should also be called from NodeFunctors with time-intesive
* processing in order to provide a more granular way to stop execution.
*
* The execution is stopped by throwing AlgoBreak. Boost function
* boost::this_thread::interruption_point() is also called prior to throwing
* AlgoBreak.
**/

void ExecutionGuard::guard() const {
	if(_timeout == (unsigned int)-1) return;
	ptime now(microsec_clock::universal_time());

	if((now - _startTime).total_milliseconds() > _timeout) {
//		boost::this_thread::interruption_point();
		throw AlgoTimeoutBreak();
	}
}

/**
* Used to stop execution explicitly.
**/

void ExecutionGuard::stop() const {
//	boost::this_thread::interruption_point();
	throw AlgoTimeoutBreak();
}

/**
* Resets the timer - the countdown stars anew.
**/

void ExecutionGuard::reset() {
	_startTime = ptime(microsec_clock::universal_time());
}

/**
* Sets the maximum time of execution [ms]. -1 means no execution time limit.
**/

void ExecutionGuard::setTimeout(unsigned int timeout) {
	_timeout = timeout;
}

/**
* Returns the maximum time of execution [ms]. -1 means no execution time limit.
**/

unsigned int ExecutionGuard::getTimeout() const {
	return _timeout;
}

/**
* Returns the time left for execution to finish.
**/

unsigned int ExecutionGuard::getRemaining() const {
	ptime now(microsec_clock::universal_time());
	auto gone = (now - _startTime).total_milliseconds();
	if(gone > numeric_cast<decltype(gone)>(_timeout) || gone < 0) return 0;
	else return numeric_cast<unsigned int>(_timeout - gone);
}

/**
 * Assignment operator.
 */

ExecutionGuard& ExecutionGuard::operator=(const ExecutionGuard& execGuard) {
	if(this != & execGuard) {
		_startTime = execGuard._startTime;
		_timeout = execGuard._timeout;
		_recursion = execGuard._recursion;
		_maxRecursion = execGuard._maxRecursion;
	}
	return *this;
}

/**
 * Copy constructor.
 */

ExecutionGuard::ExecutionGuard(const ExecutionGuard& execGuard):
	_startTime(execGuard._startTime), _timeout(execGuard._timeout),
	_recursion(execGuard._recursion), _maxRecursion(execGuard._maxRecursion) {}

/**
* Constructor.
* @param timeout The maximum time of execution [ms]. If set to -1, execution time
* is not limited.
* @param maxRecursion Sets the maximum level of recursion allowed: (unsigned int)-1 means infinity.
**/

ExecutionGuard::ExecutionGuard(unsigned int timeout, unsigned int maxRecursion):
_startTime(ptime(microsec_clock::universal_time())),
_timeout(timeout), _recursion(0), _maxRecursion(maxRecursion) {}

} //namespace algotree
