#ifndef ContextBlock_Argument_H_
#define ContextBlock_Argument_H_

#include "../system.h"
#include <vector>
#include "../AlgoType.h"

namespace algotree {

/**
* @class ContextBlock_Argument
* This class forms part of the context passed into the process() method of Trees.
* This particular context is used to convey arguments.
*
* It is not safe to use the same Tree object from multiple threads concurrently.
**/
class ALGOTREE_API ContextBlock_Argument {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! Reference to the vector of Tree's formal arguments.
	std::vector<AlgoType> _args;

public:
	const std::vector<AlgoType>& getArgs() const;
	std::vector<AlgoType>& getArgs();
	void clear();

	template<class Type>
	void addArgument(Type value);

	void swapInArgs(std::vector<AlgoType>& args);
	void setArgs(const std::vector<AlgoType>& args);

	ContextBlock_Argument& operator=(const ContextBlock_Argument& context);
	ContextBlock_Argument(const ContextBlock_Argument& context);
	ContextBlock_Argument();
	explicit ContextBlock_Argument(const std::vector<AlgoType>& args);
};

/**
* Adds an argument. If Type does not match that specified in the list of
* argument types, or the number of arguments exceeds that specified by the
* list of arguments a TracedError may be thrown.
**/

template<class Type>
void ContextBlock_Argument::addArgument(Type value) {
	AlgoType val(value);
	_args.push_back(val);
}

} //namespace algotree

SYS_REGISTER_TYPENAME(algotree::ContextBlock_Argument)

#endif /* ContextBlock_Argument_H_ */
