#ifndef AlgoTree_Context_H_
#define AlgoTree_Context_H_

#include "../system.h"
#include <map>
#include "detail/ContextHandler.h"
#include "detail/ContextHandler_Generic.h"

namespace algotree {

/**
 * @class Context
 * @author Michal Gregor
 * The base context class. Implements addition and retrieval of context lists.
 *
 * ContextBlock types used with this class are required to be
 * default-constructible copy-constructible.
 **/
class ALGOTREE_API Context {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	typedef std::map<systematic::TypeID, intrusive_ptr<detail::ContextHandler> > MapType;

	//! A map of ContextLists stored by their type IDs.
	MapType _blocks;

private:
	/**
	 * @class ValueType
	 * The type that Context::iterator points at.
	 */

	class ValueType {
	private:
		ValueType& operator=(const ValueType&);
		ValueType(const ValueType&);

	private:
		//! The underlying raw iterator.
		MapType::iterator _rawIter;

	public:
		//! Returns the TypeID of the context block.
		const systematic::TypeID& type() const {
			return _rawIter->first;
		}

		//! Return a reference to the associated block. Throw TracedError_InvalidArgument
		//! if the block is not of type ContextBlock.
		template<class ContextBlock>
		ContextBlock& block() {
			if(systematic::type_id<ContextBlock>() != _rawIter->first)
				throw TracedError_InvalidArgument("The context block is not of the specified type.");

			ContextBlock* ptr = static_cast<ContextBlock*>(_rawIter->second->getPtr());
				if(!ptr) throw TracedError_InvalidPointer("Invalid pointer to a block.");

			return *ptr;
		}

		//! Return a reference to the associated block. Throw TracedError_InvalidArgument
		//! if the block is not of type ContextBlock.
		template<class ContextBlock>
		const ContextBlock& block() const {
			if(systematic::type_id<ContextBlock>() != _rawIter->first)
				throw TracedError_InvalidArgument("The context block is not of the specified type.");

			const ContextBlock* ptr = static_cast<const ContextBlock*>(_rawIter->second->getPtr());
				if(!ptr) throw TracedError_InvalidPointer("Invalid pointer to a block.");

			return *ptr;
		}

		ValueType(MapType::iterator rawIter): _rawIter(rawIter) {}
	};

	/**
	 * Iterator type.
	 */

	template<class Type>
	class IteratorImpl {
	private:
		friend class Context;

		template<class IterType>
		friend class IteratorImpl;

	private:
		MapType::iterator _rawIter;
		mutable ValueType* _obj;

	private:
		void deleteObj() const {
			if(_obj) {
				delete _obj;
				_obj = NULL;
			}
		}

		void createObj() const {
			if(!_obj) {
				_obj = new ValueType(_rawIter);
			}
		}

	public:
		typedef Type value_type;
		typedef typename MapType::iterator::difference_type difference_type;
		typedef Type* pointer;
		typedef Type& reference;
		typedef std::bidirectional_iterator_tag iterator_category;

	public:
		bool operator==(const IteratorImpl& iter) const {
			return _rawIter == iter._rawIter;
		}

		bool operator!=(const IteratorImpl& iter) const {
			return _rawIter != iter._rawIter;
		}

		reference operator*() const {
			createObj();
			return *_obj;
		}

		pointer operator->() const {
			createObj();
			return _obj;
		}

		IteratorImpl& operator++() {
			deleteObj();
			_rawIter++;
			return *this;
		}

		IteratorImpl operator++(int) {
			deleteObj();
			IteratorImpl tmpiter(_rawIter);
			_rawIter++;
			return tmpiter;
		}

		IteratorImpl& operator--() {
			deleteObj();
			_rawIter--;
			return *this;
		}

		IteratorImpl operator--(int) {
			deleteObj();
			IteratorImpl tmpiter(_rawIter);
			_rawIter--;
			return tmpiter;
		}

		template<class IterType>
		IteratorImpl& operator=(const IterType& iter) {
			if(static_cast<void*>(this) != static_cast<void*>(&iter)) {
				_rawIter = iter._rawIter;
				deleteObj();
			}

			return *this;
		}

		IteratorImpl& operator=(const MapType::iterator& rawIter) {
			_rawIter = rawIter;
			deleteObj();

			return *this;
		}

		template<class IterType>
		IteratorImpl(const IterType& iter):
			_rawIter(iter._rawIter), _obj(NULL) {}

		IteratorImpl(const MapType::iterator& rawIter):
			_rawIter(rawIter), _obj(NULL) {}

		IteratorImpl(): _rawIter(), _obj(NULL) {}

		~IteratorImpl() {
			if(_obj) delete _obj;
		}
	};

public:
	typedef ValueType value_type;
	typedef typename MapType::iterator::difference_type difference_type;

	typedef IteratorImpl<ValueType> iterator;
	typedef IteratorImpl<const ValueType> const_iterator;
	typedef std::reverse_iterator<iterator> reverse_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

	//! Returns an iterator to the first block of the Context.
	iterator begin() {return iterator(_blocks.begin());}
	//! Returns an iterator to the first block of the Context.
	const_iterator begin() const {return const_iterator(const_cast<MapType&>(_blocks).begin());}

	//! Returns an iterator to an element just past the end of the Context.
	iterator end() {return iterator(_blocks.end());}
	//! Returns a const_iterator to an element just past the end of the Context.
	const_iterator end() const {return const_iterator(const_cast<MapType&>(_blocks).end());}

	//! Returns a reverse_iterator to the last element in the Context.
	reverse_iterator rbegin() {return reverse_iterator(_blocks.rbegin());}
	//! Returns a const_reverse_iterator to the last element in the Context.
	const_reverse_iterator rbegin() const {return const_reverse_iterator(const_cast<MapType&>(_blocks).rbegin());}

	//! Returns a reverse_iterator to an element just before the start of the Context.
	reverse_iterator rend() {return reverse_iterator(_blocks.rbegin());}
	//! Returns a const_reverse_iterator to an element just before the start of the Context.
	const_reverse_iterator rend() const {return const_reverse_iterator(const_cast<MapType&>(_blocks).rend());}

	//! Finds a block with the specified TypeID in the context and returns its
	//! iterator. If there is no such block, return end().
	iterator find(const systematic::TypeID& typeID) {
		MapType::iterator rawIter = _blocks.find(typeID);
		return iterator(rawIter);
	}

	//! Finds a block with the specified TypeID in the context and returns its
	//! iterator. If there is no such block, return end().
	const_iterator find(const systematic::TypeID& typeID) const {
		MapType::iterator rawIter = const_cast<MapType&>(_blocks).find(typeID);
		return const_iterator(rawIter);
	}

	//! Finds a block of the specified type in the context and returns its
	//! iterator. If there is no such block, return end().
	template<class ContextBlock>
	iterator find() {return find(systematic::type_id<ContextBlock>());}

	//! Finds a block of the specified type in the context and returns its
	//! iterator. If there is no such block, return end().
	template<class ContextBlock>
	const_iterator find() const {return find(systematic::type_id<ContextBlock>());}

	template<class ContextBlock>
	inline iterator insert();

	template<class ContextBlock>
	iterator insert(ContextBlock* contextBlock);

	//! Erases the element represented by the specified iterator.
	void erase(iterator iter) {_blocks.erase(iter._rawIter);}
	//! Erases all elements from the Context.
	void clear() {_blocks.clear();}

public:
	template<class ContextBlock>
	inline ContextBlock& addBlock();

	template<class ContextBlock>
	ContextBlock& addBlock(ContextBlock* contextBlock);

	template<class ContextBlock>
	ContextBlock& getAddBlock();

	template<class ContextBlock>
	ContextBlock& getBlock();

	template<class ContextBlock>
	const ContextBlock& getBlock() const;

	template<class ContextBlock>
	void eraseBlock();

	Context& operator=(const Context& obj);
	Context(const Context& obj);

	Context(): _blocks() {}
};

/**
* This method can be used to add ContextBlocks that are default
* constructible. Use the other version of add() to add
* pre-constructed lists.
*
* If another instance of type ContextBlock has already been added a
* TracedError_AlreadyExists is thrown.
*
* Returns an iterator to the added ContextBlock.
**/

template<class ContextBlock>
inline Context::iterator Context::insert() {
	return insert(new ContextBlock());
}

/**
* Adds a ContextBlock to the Context.
*
* If another instance of type ContextBlock has already been added a
* TracedError_AlreadyExists is thrown.
*
* Returns an iterator to the added ContextBlock.
**/

template<class ContextBlock>
Context::iterator Context::insert(ContextBlock* contextBlock) {
	intrusive_ptr<detail::ContextHandler> handler(new detail::ContextHandler_Generic<ContextBlock>(contextBlock));

	std::pair<MapType::iterator, bool> ret = _blocks.insert(
		MapType::value_type(
			systematic::type_id<ContextBlock>(),
			handler
		)
	);

	if (!ret.second) throw TracedError_AlreadyExists(
			"A block of type " + systematic::type_id<ContextBlock>().name()
					+ " has already been added into the context.");

	return iterator(ret.first);
}

/**
* This method can be used to add ContextBlocks that are default
* constructible. Use the other version of add() to add
* pre-constructed lists.
*
* If another instance of type ContextBlock has already been added a
* TracedError_AlreadyExists is thrown.
*
* Returns a reference to the added ContextBlock.
**/

template<class ContextBlock>
inline ContextBlock& Context::addBlock() {
	return addBlock(new ContextBlock());
}

/**
* Adds a ContextBlock to the Context.
*
* If another instance of type ContextBlock has already been added a
* TracedError_AlreadyExists is thrown.
*
* Returns a reference to the added ContextBlock.
**/

template<class ContextBlock>
ContextBlock& Context::addBlock(ContextBlock* contextBlock) {
	intrusive_ptr<detail::ContextHandler> handler(new detail::ContextHandler_Generic<ContextBlock>(contextBlock));

	std::pair<MapType::iterator, bool> ret = _blocks.insert(
		MapType::value_type(
			systematic::type_id<ContextBlock>(),
			handler
		)
	);

	if (!ret.second) throw TracedError_AlreadyExists(
			"A block of type " + systematic::type_id<ContextBlock>().name()
					+ " has already been added into the context.");

	ContextBlock* ptr = static_cast<ContextBlock*>(ret.first->second->getPtr());
	SYS_ASSERT_MSG(ptr, "NULL pointer to a ContextBlock that has just been added.");

	return *ptr;
}

/**
* Returns a ContextBlock of the specified type. If there is no such
* type available a default-constructed instance of it is added and
* reference to it returned.
**/

template<class ContextBlock>
ContextBlock& Context::getAddBlock() {
	MapType::iterator iter;
	iter = _blocks.find(systematic::type_id<ContextBlock>());

	if(iter == _blocks.end()) {
		return addBlock<ContextBlock>();
	} else {
		ContextBlock* ptr = static_cast<ContextBlock*>(iter->second->getPtr());
		if(!ptr) throw TracedError_InvalidPointer("Invalid pointer to a block.");
		return *ptr;
	}
}

/**
* Returns a ContextBlock of the specified type. If there is no such
* type available, throws TracedError_NotAvailable.
**/

template<class ContextBlock>
ContextBlock& Context::getBlock() {
	MapType::iterator iter;
	iter = _blocks.find(systematic::type_id<ContextBlock>());
	if(iter == _blocks.end()) throw TracedError_NotAvailable("No block of such type has been added to the context.");

	ContextBlock* ptr = static_cast<ContextBlock*>(iter->second->getPtr());
	if(!ptr) throw TracedError_InvalidPointer("Invalid pointer to a block.");

	return *ptr;
}

/**
* Returns a ContextBlock of the specified type. If there is no such
* type available, throws TracedError_NotAvailable.
**/

template<class ContextBlock>
const ContextBlock& Context::getBlock() const {
	MapType::const_iterator iter;
	iter = _blocks.find(systematic::type_id<ContextBlock>());
	if(iter == _blocks.end()) throw TracedError_NotAvailable("No block of such type has been added to the context.");

	const ContextBlock* ptr = static_cast<const ContextBlock*>(iter->second->getPtr());
	if(!ptr) throw TracedError_InvalidPointer("Invalid pointer to a block.");

	return *ptr;
}

/**
* Deletes the block of the specified type from the context. If there is no
* block of such type in the context this does nothing.
**/

template<class ContextBlock>
void Context::eraseBlock() {
	MapType::iterator iter;
	iter = _blocks.find(systematic::type_id<ContextBlock>());
	if(iter != _blocks.end()) _blocks.erase(iter);
}

} //namespace algotree

#endif //AlgoTree_Context_H_
