#ifndef ALGO_INPUTCONTEXT_H_INCLUDED
#define ALGO_INPUTCONTEXT_H_INCLUDED

#include "../system.h"

#include "Context.h"
#include "ContextBlock_Argument.h"
#include "ContextBlock_Memory.h"
#include "ExecutionGuard.h"

namespace algotree {

/**
* @class ProcessContext
* This class forms part of the context passed into the process() method of Trees.
* This particular context is used to assemble ContextBlock_Argument and MemoryContext.
*
* ContextBlocks can be added and retrieved using the interface from class Context.
* ContextBlock_Memory and ContextBlock_Argument are added by default.
**/
SYS_WNONVIRT_DTOR_OFF
class ALGOTREE_API ProcessContext: private Context {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! Stores the execution guard used to track time limits.
	shared_ptr<ExecutionGuard> _execGuard;

private:
	ProcessContext(const shared_ptr<ExecutionGuard>& execGuard);

public:
	//import interface from Context
	using Context::value_type;
	using Context::difference_type;
	using Context::iterator;
	using Context::const_iterator;
	using Context::reverse_iterator;
	using Context::const_reverse_iterator;
	using Context::begin;
	using Context::end;
	using Context::rbegin;
	using Context::rend;
	using Context::find;
	using Context::insert;
	using Context::erase;
	using Context::clear;
	using Context::addBlock;
	using Context::getAddBlock;
	using Context::getBlock;
	using Context::eraseBlock;

	//! Returns a reference to the contained ExecutionGuard.
	ExecutionGuard& execGuard() {return *_execGuard;}
	//! Returns a reference to the contained ExecutionGuard.
	const ExecutionGuard& execGuard() const {return *_execGuard;}

	//! Returns a reference to the contained ContextBlock_Argument.
	ContextBlock_Argument& arguments() {return getBlock<ContextBlock_Argument>();}
	//! Returns a reference to the contained ContextBlock_Argument.
	const ContextBlock_Argument& arguments() const {return getBlock<ContextBlock_Argument>();}

	//! Returns a reference to the contained MemoryContext.
	ContextBlock_Memory& memory() {return getBlock<ContextBlock_Memory>();}
	//! Returns a reference to the contained MemoryContext.
	const ContextBlock_Memory& memory() const {return getBlock<ContextBlock_Memory>();}

	ProcessContext newScope() const;

	//! Returns the maximum runtime setting. (unsigned int)-1 means no limit.
	unsigned int getMaxRuntime() const {return _execGuard->getTimeout();}
	//! Sets the maximum runtime setting. (unsigned int)-1 means no limit.
	void setMaxRuntime(unsigned int max_runtime) {_execGuard->setTimeout(max_runtime);}

	ProcessContext(unsigned int max_runtime = -1, unsigned int maxRecursion = 5);
	explicit ProcessContext(const std::vector<AlgoType>& args, unsigned int max_runtime = -1, unsigned int maxRecursion = 5);
};
SYS_WNONVIRT_DTOR_ON

} //namespace algotree

SYS_REGISTER_TYPENAME(algotree::ProcessContext)

#endif // ALGO_INPUTCONTEXT_H_INCLUDED
