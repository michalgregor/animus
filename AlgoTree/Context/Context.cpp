#include "Context.h"
#include <boost/serialization/map.hpp>

namespace algotree {

/**
* Boost serialization function.
**/

void Context::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _blocks;
}

void Context::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _blocks;
}

/**
* Assignment operator.
**/

Context& Context::operator=(const Context& obj) {
	if(&obj != this) {
		_blocks.clear();
		MapType::const_iterator iter;
		for(iter = obj._blocks.begin(); iter != obj._blocks.end(); iter++) {
			_blocks.insert(MapType::value_type(iter->first, intrusive_ptr<detail::ContextHandler>(clone(iter->second))));
		}
	}

	return *this;
}

/**
* Copy constructor.
**/

Context::Context(const Context& obj): _blocks() {
	MapType::const_iterator iter;
	for(iter = obj._blocks.begin(); iter != obj._blocks.end(); iter++) {
		_blocks.insert(MapType::value_type(iter->first, intrusive_ptr<detail::ContextHandler>(clone(iter->second))));
	}
}

}//namespace algotree
