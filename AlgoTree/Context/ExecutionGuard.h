#ifndef ALGO_EXECUTIONGUARD_H_INCLUDED
#define ALGO_EXECUTIONGUARD_H_INCLUDED

#include "../exception/AlgoTimeoutBreak.h"
#include "../exception/AlgoRecursionBreak.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/time_serialize.hpp>

#include "../system.h"

namespace algotree {

using boost::posix_time::ptime;

/**
* @class RecursionHolder
* Returned by ExecutionGuard::startRecursion(). While this object exists the
* recursion counter of ExecutionGuard is incremented by one. It is decremented\
* back once the RecursionHolder is deleted.
*/
class ALGOTREE_API RecursionHolder {
private:
	//! Reference to the recursion counter. The counter is incremented upon
	//! construction and decremented upon destruction.
	unsigned int* _recursion;

public:
	RecursionHolder& operator=(const RecursionHolder& obj) {(*_recursion)--; _recursion = obj._recursion; (*_recursion)++; return *this;}
	RecursionHolder(const RecursionHolder& obj): _recursion(obj._recursion) {(*_recursion)++;}
	RecursionHolder(unsigned int& recursionCounter): _recursion(&recursionCounter) {(*_recursion)++;}
	~RecursionHolder() {(*_recursion)--;}
};

/**
* @class ExecutionGuard
* Used by ProcessContext (and by AlgoTrees) to assign time limit to execution of
* a tree. See execGuard() documentation for more details.
**/

class ExecutionGuard {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! Stores the time when the countdown has started.
	ptime _startTime;
	//! The maximum time of execution [ms]. -1 means no execution time limit.
	unsigned int _timeout;
	//! Stores the current level of recursion.
	unsigned int _recursion;
	//! The maximum level of recursion allowed. See startRecursion() documentation
	//! for details. (unsigned int)-1 means infinity.
	unsigned int _maxRecursion;

public:
	RecursionHolder startRecursion();
	void resetRecursionCounter();
	unsigned int getRecursionLevel() const;
	unsigned int getMaxRecursion() const;
	void setMaxRecursion(unsigned int maxRecursion);

	void guard() const;
	void stop() const;
	void reset();
	void setTimeout(unsigned int timeout);
	unsigned int getTimeout() const;
	unsigned int getRemaining() const;

	ExecutionGuard& operator=(const ExecutionGuard& execGuard);
	ExecutionGuard(const ExecutionGuard& execGuard);
	ExecutionGuard(unsigned int timeout = -1, unsigned int maxRecursion = 5);
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::ExecutionGuard)

#endif // ALGO_EXECUTIONGUARD_H_INCLUDED
