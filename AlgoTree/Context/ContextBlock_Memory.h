#ifndef ALGO_MEMORYCONTEXT_H_INCLUDED
#define ALGO_MEMORYCONTEXT_H_INCLUDED

#include "../system.h"
#include <map>
#include <boost/serialization/map.hpp>

#include <Systematic/utils/StorageToken.h>
#include <Systematic/dynamic_typing/PolyHandler.h>

namespace algotree {

/**
* @class ContextBlock_Memory
* This class forms part of the context passed into the process() method of Trees.
*
* This particular context is used to store an arbitrary type of data using
* a PolyHandler under a given token of systematic::Object* type. The supplied
* pointer is never dereferenced or deleted from this class.
*/
class ALGOTREE_API ContextBlock_Memory {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! Type of the container in which the data is stored.
	typedef std::map<StorageToken, systematic::PolyHandler> DataStore;

	//! Data store for use by Objects.
	DataStore _dataStore;

private:
	void copyDataStoreTo(DataStore& space) const;

public:
	template<class Type>
	void storeData(const StorageToken& key, const Type& data);

	template<class Type>
	const Type& getData(const StorageToken& key) const;

	template<class Type>
	Type& getData(const StorageToken& key);

	systematic::TypeID getDataType(const StorageToken& key) const;

	bool exists(const StorageToken& key) const;
	void erase(const StorageToken& key);
	void clear();

	ContextBlock_Memory& operator=(const ContextBlock_Memory& storage);
	ContextBlock_Memory(const ContextBlock_Memory& storage);
	ContextBlock_Memory();
};

/**
* Adds specified data to the context data store. The data is filed under the
* provided key, that is the pointer to the functor.
**/

template<class Type>
void ContextBlock_Memory::storeData(const StorageToken& key, const Type& data) {
	_dataStore[key] = systematic::PolyHandler(data);
}

/**
* Retrieves the corresponding data from the store. If there is no data filed
* under the specified key, a TracedError_Generic is thrown. If wrong data type is
* specified, throws an exception.
**/

template<class Type>
const Type& ContextBlock_Memory::getData(const StorageToken& key) const {
	DataStore::const_iterator iter = _dataStore.find(key);
	if(iter == _dataStore.end()) throw TracedError_Generic("No data has been filed under the specified key.");
	return iter->second.getData<Type>();
}

/**
* Retrieves the corresponding data from the store. If there is no data filed
* under the specified key, a TracedError_Generic is thrown. If wrong data type is
* specified, throws an exception.
**/

template<class Type>
Type& ContextBlock_Memory::getData(const StorageToken& key) {
	DataStore::iterator iter = _dataStore.find(key);
	if(iter == _dataStore.end()) throw TracedError_Generic("No data has been filed under the specified key.");
	return iter->second.getData<Type>();
}

} //namespace algotree

SYS_REGISTER_TYPENAME(algotree::ContextBlock_Memory)

#endif // ALGO_MEMORYCONTEXT_H_INCLUDED
