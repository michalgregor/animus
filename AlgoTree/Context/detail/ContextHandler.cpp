#include "ContextHandler.h"

namespace algotree {
namespace detail {
	
/**
* Boost serialization function.
**/

void ContextHandler::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<RefCounterVirtual>(*this);
}

void ContextHandler::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<RefCounterVirtual>(*this);
}

/**
 * Body of the pure virtual destructor.
 */

ContextHandler::~ContextHandler() {}

}//namespace detail
}//namespace algotree
