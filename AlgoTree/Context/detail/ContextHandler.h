#ifndef AlgoTree_ContextHandler_H_
#define AlgoTree_ContextHandler_H_

#include "../../system.h"
#include <Systematic/utils/RefCounterVirtual.h>

namespace algotree {
namespace detail {

/*
* @class ContextHandler
* @author Michal Gregor
**/
class ALGOTREE_API ContextHandler: public systematic::RefCounterVirtual {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/*
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	// Returns the pointer to the contained data.
	virtual void* getPtr() = 0;
	// Returns the pointer to the contained data.
	virtual const void* getPtr() const = 0;

	virtual ~ContextHandler() = 0;
};

}//namespace detail
}//namespace algotree

SYS_EXPORT_CLASS(algotree::detail::ContextHandler)

#endif //AlgoTree_ContextHandler_H_
