#ifndef AlgoTree_ContextHandler_Generic_H_
#define AlgoTree_ContextHandler_Generic_H_

#include "../../system.h"
#include "ContextHandler.h"

#include <boost/type_traits/is_fundamental.hpp>

namespace algotree {
namespace detail {

/*
* @class ContextHandler_Generic
* @author Michal Gregor
*/

template<class Type>
class ContextHandler_Generic: public ContextHandler {
	static_assert(!boost::is_fundamental<Type>::value, "Must be a fundamental type.");

private:
	friend class boost::serialization::access;

	/*
	* Boost serialization function.
	*/
	void serialize(systematic::OArchive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<ContextHandler>(*this);
		ar & _data;
	}

	void serialize(systematic::IArchive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<ContextHandler>(*this);
		ar & _data;
	}

private:
	/*
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	// A pointer to the contained data.
	Type* _data;

public:
	// Returns the pointer to the contained data.
	virtual void* getPtr() {
		return _data;
	}

	// Returns the pointer to the contained data.
	virtual const void* getPtr() const {
		return _data;
	}

	ContextHandler_Generic<Type>& operator=(const ContextHandler_Generic<Type>& obj);
	ContextHandler_Generic(const ContextHandler_Generic<Type>& obj);

	ContextHandler_Generic();
	ContextHandler_Generic(Type* data);
	virtual ~ContextHandler_Generic();
};

/*
* Assignment operator.
*/

template<class Type>
ContextHandler_Generic<Type>& ContextHandler_Generic<Type>::operator=(const ContextHandler_Generic<Type>& obj) {
	if(&obj != this) {
		ContextHandler::operator=(obj);
		if(_data) delete _data;
		if(obj._data) _data = new Type(*obj._data);
		else _data = NULL;
	}

	return *this;
}

/*
* Copy constructor.
*/

template<class Type>
ContextHandler_Generic<Type>::ContextHandler_Generic(const ContextHandler_Generic<Type>& obj):
	ContextHandler(obj), _data((obj._data)?(new Type(*obj._data)):(NULL)) {}

/*
* Default constructor.
*/

template<class Type>
ContextHandler_Generic<Type>::ContextHandler_Generic(): _data(NULL) {}

/*
* Constructor.
*/

template<class Type>
ContextHandler_Generic<Type>::ContextHandler_Generic(Type* data):
_data(data) {}

/*
 * Destructor.
 */

template<class Type>
ContextHandler_Generic<Type>::~ContextHandler_Generic() {
	SYS_EXPORT_INSTANCE();
	if(_data) delete _data;
}

}//namespace detail
}//namespace algotree

SYS_EXPORT_TEMPLATE(algotree::detail::ContextHandler_Generic, 1)

#endif //AlgoTree_ContextHandler_Generic_H_
