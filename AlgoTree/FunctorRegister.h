#ifndef ALGO_FUNCTORREGISTER_H_INCLUDED
#define ALGO_FUNCTORREGISTER_H_INCLUDED

#include <map>
#include <vector>
#include <boost/serialization/list.hpp>

#include "NodeFunctor/NodeFunctor.h"
#include "FunctorFactory/FunctorFactory.h"
#include "FunctorFactory/FunctorFactory_Variable.h"
#include "FunctorFactory/FunctorFactory_CloneFunctor.h"
#include "NodeFunctor/VarAssign.h"

#include "TypeComparison.h"

#include "exception/AlgoError_NoFunctorFound.h"

namespace algotree {

class Tree;

/**
* @class FunctorRegister
* @author Michal Gregor
*
* Register of node functors. TreeGenerators use this class as a register of node
* functors.
*
* Before any of the newRand functions is used with a TreeContext, the TreeContext
* should be initialized using initTreeContext().
**/
class ALGOTREE_API FunctorRegister {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	typedef std::map<TypeID, std::vector<intrusive_ptr<FunctorFactory> > >::const_iterator FunctorIterator;

	//! Register of branch functors.
	std::map<TypeID, std::vector<intrusive_ptr<FunctorFactory> > > _branchRegister;
	//! Register of leaf functors.
	std::map<TypeID, std::vector<intrusive_ptr<FunctorFactory> > > _leafRegister;
	//! A list of TreeContextInitializer objects. These should be called by
	//! TreeGenerator for every newly generated tree.
	std::list<intrusive_ptr<TreeContextInitializer> > _treeContextInits;

public:
	void addFunctor(const intrusive_ptr<NodeFunctor>& functor);
	void addFactory(const intrusive_ptr<FunctorFactory>& factory);

	void clear();
	void clearBranches();
	void clearLeaves();

	intrusive_ptr<NodeFunctor> newRandFunctor(TreeContext* treeContext) const;
	intrusive_ptr<NodeFunctor> newRandBranch(TreeContext* treeContext) const;
	intrusive_ptr<NodeFunctor> newRandLeaf(TreeContext* treeContext) const;

	intrusive_ptr<NodeFunctor> newRandFunctor(TreeContext* treeContext, TypeID type, TypeComparison tc) const;
	intrusive_ptr<NodeFunctor> newRandBranch(TreeContext* treeContext, TypeID type, TypeComparison tc) const;
	intrusive_ptr<NodeFunctor> newRandLeaf(TreeContext* treeContext, TypeID type, TypeComparison tc) const;

	void initTreeContext(TreeContext* treeContext) const;

	unsigned int size() const;

	unsigned int numFunctors() const;
	unsigned int numBranches() const;
	unsigned int numLeaves() const;

	unsigned int numFunctors(TypeID type, TypeComparison tc) const;
	unsigned int numBranches(TypeID type, TypeComparison tc) const;
	unsigned int numLeaves(TypeID type, TypeComparison tc) const;

	const std::map<TypeID, std::vector<intrusive_ptr<FunctorFactory> > >& getBranchFactories() const;
	const std::map<TypeID, std::vector<intrusive_ptr<FunctorFactory> > >& getLeafFactories() const;

	FunctorRegister();
};

} //namespace algotree

#endif // ALGO_FUNCTORREGISTER_H_INCLUDED
