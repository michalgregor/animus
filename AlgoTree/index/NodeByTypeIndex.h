#ifndef ALGO_NODEBYTYPEINDEX_H_INCLUDED
#define ALGO_NODEBYTYPEINDEX_H_INCLUDED

#include <vector>
#include <map>
#include <set>

#include <Systematic/type_info.h>

#include "../Node.h"
#include "../Tree.h"

#include "../TypeComparison.h"

namespace algotree {

/**
* @class NodeByTypeIndex
* @author Michal Gregor
* Constructs an index of Tree's nodes according to their return type.
**/
class ALGOTREE_API NodeByTypeIndex {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The actual map containing the index.
	std::map<TypeID, std::vector<Node*> > _index;

public:
	typedef std::vector<Node*>::size_type size_type;

public:
	const std::map<TypeID, std::vector<Node*> >& getNodes() const;
	const std::vector<Node*>& getNodes(TypeID type) const;

	Node* randNode() const;
	Node* randNode(TypeID type, TypeComparison tc) const;

	size_type size() const;
	size_type numBranches() const;
	size_type numBranches(TypeID type, TypeComparison tc) const;

	void compute(Tree& tree, bool includeRoot = true) {compute(*tree.getRoot(), includeRoot);}
	void compute(Node& root, bool includeRoot = true);

	void clear();

	void intersect(NodeByTypeIndex& indexB, TypeComparison tc);
	void intersect(const std::set<TypeID>& types);

	void erase(TypeID type);
	void erase(Node* node);

	NodeByTypeIndex& operator=(const NodeByTypeIndex& index);

	NodeByTypeIndex();
	NodeByTypeIndex(const NodeByTypeIndex& index);
	NodeByTypeIndex(Node& root, bool includeRoot = true);
	NodeByTypeIndex(Tree& tree, bool includeRoot = true);
};

} //namespace algotree

#endif // ALGO_NODEBYTYPEINDEX_H_INCLUDED
