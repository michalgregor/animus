#include "NodeByTypeIndex.h"
#include <Systematic/generator/BoundaryGenerator.h>
#include <Systematic/dynamic_typing/ConverterRegister.h>
#include <functional>

#include "../exception/AlgoError_NoFunctorFound.h"

namespace algotree {

/**
* Boost serialization function.
**/

void NodeByTypeIndex::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _index;
}

void NodeByTypeIndex::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _index;
}

/**
* Returns a reference to the map containing the index of nodes
* by their return type.
**/

const std::map<TypeID, std::vector<Node*> >& NodeByTypeIndex::getNodes() const {
	return _index;
}

/**
* Returns a reference to a vector of Node pointer with return type
* specified by the type parameter. Throws TracedError_Generic if no nodes with such return
* type exist.
**/

const std::vector<Node*>& NodeByTypeIndex::getNodes(TypeID type) const {
	std::map<TypeID, std::vector<Node*> >::const_iterator iter = _index.find(type);
	if(iter == _index.end()) throw TracedError_Generic("No nodes with the specified return"
	"type found.");
	return iter->second;
}

/**
* Returns a pointer to a random node.
**/

Node* NodeByTypeIndex::randNode() const {
	std::map<TypeID, std::vector<Node*> >::const_iterator iter;

	size_type sel = BoundaryGenerator<size_type>(0, numBranches() - 1)();
	size_type count = 0;

	for(iter = _index.begin(); iter != _index.end(); iter++) {
		count += iter->second.size();
		if(sel < count) {
			sel -= count - iter->second.size();
			return iter->second.at(sel);
		}
	}

	throw TracedError_Generic("No such Node found.");
}

/**
* Returns a pointer to a random node of the specified type. If no node of such
* type is found an TracedError_Generic is thrown.
* @param strict If set to false the function also looks for functors with types
* convertible to type, if set to true only functors with type equal to the
* specified type are considered.
**/

Node* NodeByTypeIndex::randNode(TypeID type, TypeComparison tc) const {
	if(tc == TC_Strict) {
		std::map<TypeID, std::vector<Node*> >::const_iterator iter = _index.find(type);
		if(iter == _index.end() || iter->second.size() == 0) throw TracedError_Generic("No such Node found.");
		size_type sel = BoundaryGenerator<size_type>(0, iter->second.size() - 1)();
		return iter->second.at(sel);
	} else {
		std::map<TypeID, std::vector<Node*> >::const_iterator iter;
		size_type sel = BoundaryGenerator<size_type>(0, numBranches(type, tc) - 1)();
		size_type count = 0;

		if(tc == TC_Convertible) {
			for(iter = _index.begin(); iter != _index.end(); iter++) {
				if(AlgoConverterRegister().convertible(iter->first, type)) {
					count += iter->second.size();
					if(sel < count) {
						sel -= count - iter->second.size();
						return iter->second.at(sel);
					}
				}
			}
		} else {
			for(iter = _index.begin(); iter != _index.end(); iter++) {
				if(AlgoConverterRegister().compatible(iter->first, type)) {
					count += iter->second.size();
					if(sel < count) {
						sel -= count - iter->second.size();
						return iter->second.at(sel);
					}
				}
			}
		}

		throw AlgoError_NoFunctorFound("No such functor found.");
	}
}

/**
* Returns the total number of nodes in the index [alias of numBranches()].
**/

NodeByTypeIndex::size_type NodeByTypeIndex::size() const {
	return numBranches();
}

/**
* Returns the total number of nodes in the index.
**/

NodeByTypeIndex::size_type NodeByTypeIndex::numBranches() const {
	size_type sz = 0;
	std::map<TypeID, std::vector<Node*> >::const_iterator iter;
	for(iter = _index.begin(); iter != _index.end(); iter++) {
		sz += iter->second.size();
	}

	return sz;
}

/**
* Returns the number of nodes filed under the specified type.
* @param strict If set to false the function also looks for functors with types
* convertible to type, if set to true only functors with type equal to the
* specified type are considered.
**/

NodeByTypeIndex::size_type NodeByTypeIndex::numBranches(TypeID type, TypeComparison tc) const {
	if(tc == TC_Strict) {
		std::map<TypeID, std::vector<Node*> >::const_iterator iter = _index.find(type);
		if(iter == _index.end()) return 0;
		else return iter->second.size();
	} else {
		size_type count = 0;
		std::map<TypeID, std::vector<Node*> >::const_iterator iter;

		if(tc == TC_Convertible) {
			for(iter = _index.begin(); iter != _index.end(); iter++) {
				if(AlgoConverterRegister().convertible(iter->first, type))
					count += iter->second.size();
			}
		} else {
			for(iter = _index.begin(); iter != _index.end(); iter++) {
				if(AlgoConverterRegister().compatible(iter->first, type))
					count += iter->second.size();
			}
		}

		return count;
	}
}

/**
* Computes an index of Nodes by their return type. This may take some time
* for large trees.
**/

void NodeByTypeIndex::compute(Node& root, bool includeRoot) {
	_index.clear();
	Traverser t = root.begin();
	if(!includeRoot) t++;
	for(; t != root.end(); t++) {
		_index[t->getReturnType()].push_back(&(*t));
	}
}

/**
* Clears the index.
**/

void NodeByTypeIndex::clear() {
	_index.clear();
}

/**
* Removes all Nodes filed under types that are not in both indexes from the
* indexes (that is from *this and from parameter index).
**/

void NodeByTypeIndex::intersect(NodeByTypeIndex& indexB, TypeComparison tc) {
	std::map<TypeID, std::vector<Node*> >::iterator iter, titer;
	std::set<TypeID> toLeaveInB;

	if(tc == TC_Strict) {
		//go over all types in this index
		for(iter = _index.begin(); iter != _index.end();) {
			//check if indexB contains type; if so, keep it and mark it to be
			//kept in indexB as well
			if(indexB._index.find(iter->first) != indexB._index.end()) {
				toLeaveInB.insert(iter->first);
				iter++;
			} else _index.erase(iter++);
		}
	} else {
		if(tc == TC_Convertible) {
			//go over all types in this index
			for(iter = _index.begin(); iter != _index.end();) {
				//find all types that iter->first is both-way convertible to
				const TypeID& tid = iter->first;

				std::vector<TypeID> conv = AlgoConverterRegister().convertible(tid);

				unsigned int count = 0;
				//check whether indexB contains any of the compatible types;
				//if so iter->first is to be kept as well as any compatible types in indexB
				for(unsigned int i = 0; i < conv.size(); i++) {
					if(indexB._index.find(conv[i]) != indexB._index.end()) {
						count++;
						toLeaveInB.insert(conv[i]);
					}
				}
				if(count == 0) _index.erase(iter++);
				else iter++;
			}
		} else {
			//go over all types in this index
			for(iter = _index.begin(); iter != _index.end();) {
				//find all types that iter->first is both-way convertible to
				std::vector<TypeID> conv = AlgoConverterRegister().compatible(iter->first);

				unsigned int count = 0;
				//check whether indexB contains any of the compatible types;
				//if so iter->first is to be kept as well as any compatible types in indexB
				for(unsigned int i = 0; i < conv.size(); i++) {
					if(indexB._index.find(conv[i]) != indexB._index.end()) {
						count++;
						toLeaveInB.insert(conv[i]);
					}
				}
				if(count == 0) _index.erase(iter++);
				else iter++;
			}
		}
	}

	//erase all types except those marked to be kept from indexB
	indexB.intersect(toLeaveInB);
}

/**
* A strict intersection of types is performed, that is all entries key of which
* is not part of the types parameter are removed from the index.
**/

void NodeByTypeIndex::intersect(const std::set<TypeID>& types) {
	std::map<TypeID, std::vector<Node*> >::iterator iter;
	for(iter = _index.begin(); iter != _index.end();) {
		if(types.find(iter->first) == types.end()) _index.erase(iter++);
		else iter++;
	}
}

/**
* Erases the specified type from the index.
**/

void NodeByTypeIndex::erase(TypeID type) {
	std::map<TypeID, std::vector<Node*> >::iterator iter = _index.find(type);
	if(iter != _index.end()) _index.erase(iter);
}

/**
* Erases the specified node from the index. Use with caution as the procedure
* is not a very fast one as it has to search through one vector per type.
**/

void NodeByTypeIndex::erase(Node* node) {
	std::map<TypeID, std::vector<Node*> >::iterator miter;
	for(miter = _index.begin(); miter != _index.end(); miter++) {
		std::vector<Node*>::iterator iter = std::find(miter->second.begin(), miter->second.end(), const_cast<Node*>(node));
		if(iter != miter->second.end()) {
			miter->second.erase(iter);
			break;
		}
	}
}

/**
* Assignment operator.
**/

NodeByTypeIndex& NodeByTypeIndex::operator=(const NodeByTypeIndex& index) {
	if(&index != this) _index = index._index;
	return *this;
}

/**
* Copy constructor.
**/

NodeByTypeIndex::NodeByTypeIndex(const NodeByTypeIndex& index):
_index(index._index) {}

/**
* Constructor.
**/

NodeByTypeIndex::NodeByTypeIndex(): _index() {}

/**
* Constructor. Calls compute.
* @param tree The root node of a subtree to compute an index for.
**/

NodeByTypeIndex::NodeByTypeIndex(Node& root, bool includeRoot): _index() {
	compute(root, includeRoot);
}

/**
* Constructor. Calls compute.
* @param tree The tree to compute an index for.
**/

NodeByTypeIndex::NodeByTypeIndex(Tree& tree, bool includeRoot): _index() {
	compute(tree, includeRoot);
}

} //namespace algotree
