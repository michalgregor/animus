#include "NodeSimpleIndex.h"
#include <Systematic/generator/BoundaryGenerator.h>

namespace algotree {

/**
* Boost serialization function.
**/

void NodeSimpleIndex::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _index;
}

void NodeSimpleIndex::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _index;
}

/**
* Returns a reference to the container actually storing the pointers.
**/

const std::vector<Node*>& NodeSimpleIndex::getNodes() const {
	return _index;
}

/**
* Returns node at the specified position in the index.
**/

const Node* NodeSimpleIndex::getNode(unsigned int pos) const {
	return _index.at(pos);
}

/**
* Returns a pointer to a random Node. If the index contains no nodes
* an TracedError_OutOfRange is thrown.
**/

Node* NodeSimpleIndex::randNode() const {
	if(!_index.size()) throw TracedError_OutOfRange("No nodes to choose from.");
	return _index.at(systematic::BoundaryGenerator<unsigned int>(0, _index.size() - 1)());
}

/**
* Erases the specified node from the index. Use with caution - the nodes are
* stored in a vector so binary search cannot be employed.
**/

void NodeSimpleIndex::erase(const Node* node) {
	std::vector<Node*>::iterator iter = std::find(_index.begin(), _index.end(), const_cast<Node*>(node));
	if(iter != _index.end()) _index.erase(iter);
}

/**
* Returns the number of nodes indexed.
**/

unsigned int NodeSimpleIndex::size() const {
	return _index.size();
}

/**
* Clears the current index and computes a new one for the specified tree.
**/

void NodeSimpleIndex::compute(const Node& root, bool includeRoot)  {
	_index.clear();
	ConstTraverser t = root.begin();
	if(!includeRoot) t++;
	for(; t != root.end(); t++) {
		_index.push_back(const_cast<Node*>(&(*t)));
	}
}


/**
* Clears the current index and computes a new one for the specified tree.
**/

void NodeSimpleIndex::compute(const Tree& tree, bool includeRoot)  {
	compute(*tree.getRoot(), includeRoot);
}

/**
* Clears the index.
**/

void NodeSimpleIndex::clear()  {
	_index.clear();
}

} //namespace algotree
