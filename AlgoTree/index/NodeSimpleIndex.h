#ifndef ALGO_NODESIMPLEINDEX_H_INCLUDED
#define ALGO_NODESIMPLEINDEX_H_INCLUDED

#include "../Node.h"
#include "../Tree.h"
#include <vector>

namespace algotree {

/**
* @class NodeSimpleIndex
* @author Michal Gregor
* A class that creates a simple linear list of nodes to facilitate easy random
* selection of a node.
**/
class ALGOTREE_API NodeSimpleIndex {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The actual index.
	std::vector<Node*> _index;

public:
	const std::vector<Node*>& getNodes() const;
	const Node* getNode(unsigned int pos) const;

	Node* randNode() const;

	void erase(const Node*);

	unsigned int size() const;
	void compute(const Node& root, bool includeRoot = true);
	void compute(const Tree& tree, bool includeRoot = true);
	void clear();

	NodeSimpleIndex(): _index() {}

	NodeSimpleIndex(const Node& root, bool includeRoot = true):
	_index() {compute(root, includeRoot);}

	NodeSimpleIndex(const Tree& tree, bool includeRoot = true):
	_index() {compute(tree, includeRoot);}
};

} //namespace algotree

#endif // ALGO_NODESIMPLEINDEX_H_INCLUDED
