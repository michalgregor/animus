#include "FunctorSignature.h"
namespace algotree {

/**
* Boost serialization function.
**/

void FunctorSignature::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _args;
	ar & _returnType;
}

void FunctorSignature::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _args;
	ar & _returnType;
}

/**
* Returns a 0 if both FunctorSignatures are equal, a negative value if this is less
* than typeList and a positive value if this is greater than signature.
 */

int FunctorSignature::compare(const FunctorSignature& signature) const {
	int cmpArgs(_args.compare(signature._args)), cmpTypeID(signature._returnType.compare(signature._returnType));

	if(cmpArgs < 0) return -1;
	else if(cmpArgs > 0) return 1;
	else {
		if(cmpTypeID < 0) return -1;
		else if(cmpTypeID > 0) return 1;
		else return 0;
	}
}

/**
 * Assignment operator.
 **/

FunctorSignature& FunctorSignature::operator=(const FunctorSignature& obj) {
	if(this != &obj) {
		_args = obj._args;
		_returnType = obj._returnType;
	}
	return *this;
}

/**
 * Copy constructor.
 **/

FunctorSignature::FunctorSignature(const FunctorSignature& obj) :
		_args(obj._args), _returnType(obj._returnType) {
}

/**
 * Constructor.
 **/

FunctorSignature::FunctorSignature(systematic::TypeID returnType, const TypeList& args) :
		_args(args), _returnType(returnType) {
}

} //namespace algotree
