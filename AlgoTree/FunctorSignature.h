#ifndef FUNCTORSIGNATURE_H_INCLUDED
#define FUNCTORSIGNATURE_H_INCLUDED

#include "system.h"
#include <Systematic/type_info.h>
#include "TypeList.h"

namespace algotree {

/**
* @class FunctorSignature
* @author Michal Gregor
* This class represents a functor signature:
**/
class ALGOTREE_API FunctorSignature {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	TypeList _args;
	systematic::TypeID _returnType;

public:
	int compare(const FunctorSignature& signature) const;

	bool operator<(const FunctorSignature& signature) const {
		return (compare(signature) < 0);
	}

	bool operator<=(const FunctorSignature& signature) const {
		return (compare(signature) <= 0);
	}

	bool operator>(const FunctorSignature& signature) const {
		return (compare(signature) > 0);
	}

	bool operator>=(const FunctorSignature& signature) const {
		return (compare(signature) >= 0);
	}

	bool operator==(const FunctorSignature& signature) const {
		return (_args == signature._args && _returnType == signature._returnType);
	}

	bool operator!=(const FunctorSignature& signature) const {
		return (_args != signature._args || _returnType != signature._returnType);
	}

	TypeList& getArgTypes() {
		return _args;
	}

	void setArgTypes(const TypeList& argTypes) {
		_args = argTypes;
	}

	const TypeList& getArgTypes() const {
		return _args;
	}

	systematic::TypeID& getReturnType() {
		return _returnType;
	}

	void setReturnType(const systematic::TypeID& type) {
		_returnType = type;
	}

	const systematic::TypeID& getReturnType() const {
		return _returnType;
	}

	void swap(FunctorSignature& obj) {
		if(this != &obj) {
			_args.swap(obj._args);
			systematic::TypeID tmpID(_returnType);
			_returnType = obj._returnType;
			obj._returnType = tmpID;
		}
	}

	FunctorSignature& operator=(const FunctorSignature& obj);
	FunctorSignature(const FunctorSignature& obj);

	FunctorSignature(): _args(), _returnType() {}
	FunctorSignature(systematic::TypeID returnType, const TypeList& args = TypeList());
};

}//namespace algotree

#endif // FUNCTORSIGNATURE_H_INCLUDED
