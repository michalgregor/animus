#ifndef ALGO_GROWNTREEGENERATOR_H_INCLUDED
#define ALGO_GROWNTREEGENERATOR_H_INCLUDED

#include "TreeGenerator_Common.h"
#include "SubtreeGenerator_Growth.h"

namespace algotree {

/**
* @class TreeGenerator_Growth
* @author Michal Gregor
* Generates grown trees of specified maximum depth. A grown tree is such tree
* in which there is a specified probability that a branch or a leaf will be
* chosen; the exception being when the specified maximum depth is reached
* in which case a leaf is always chosen.
*
* It is required that the FunctorRegister contain both non-terminals and
* and terminals for every type that may appear at depth d < maximum depth and
* terminal for every type that may appear at the maximum depth.
*
* The generator can be set into a less strict mode that allows a non-terminal to
* be substituted by a terminal with the same return type if no non-terminal with
* that return type is available (normally an exception is thrown). This
* behaviour can be activated using setSubstitutionAllowed().
**/
class ALGOTREE_API TreeGenerator_Growth: public TreeGenerator_Common {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

	TreeGenerator_Growth();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	SubtreeGenerator_Growth _generator;

public:
	bool getSubstitutionAllowed() const;
	void setSubstitutionAllowed(bool allowed);

	virtual void generate(Node& parent, unsigned int depth);
	virtual void replace(Node& parent, unsigned int depth);

	/**
	* Returns the pBranch probability, that is - the probability that a randomly
	* chosen node at any depth except the maximum depth will be a branch.
	* The probability of a leaf is 1 - _pBranch.
	**/

	ProbabilityType getPBranch() const {return _generator.getPBranch();}

	/**
	* Sets the pBranch probability, that is - the probability that a randomly
	* generated node at any depth except the maximum depth will be a branch.
	* The probability of a leaf is 1 - _pBranch. The range of pBranch values is
	* obviously [0.0f, 1.0f], this, however, is not enforced. The behaviour
	* is not defined for other values.
	**/

	void setPBranch(ProbabilityType pBranch) {_generator.setPBranch(pBranch);}

	TreeGenerator_Growth& operator=(const TreeGenerator_Growth& generator);
	TreeGenerator_Growth(const TreeGenerator_Growth& generator);

	explicit TreeGenerator_Growth(
		TypeID type,
		TypeList argList,
		unsigned int maxDepth = 5,
		ProbabilityType pBranch = 0.5f
	);

	explicit TreeGenerator_Growth(
		const FunctorRegister& functorRegister,
		TypeID type,
		TypeList argList,
		unsigned int maxDepth = 5,
		ProbabilityType pBranch = 0.5f
	);

	virtual ~TreeGenerator_Growth() {}
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::TreeGenerator_Growth)

#endif // ALGO_GROWNTREEGENERATOR_H_INCLUDED
