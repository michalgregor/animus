#include "SubtreeGenerator.h"

namespace algotree {

/**
* Boost serialization function.
**/

void SubtreeGenerator::serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

void SubtreeGenerator::serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

/**
 * Empty body of the pure virtual destructor.
 */

SubtreeGenerator::~SubtreeGenerator() {}

} //namespace algotree
