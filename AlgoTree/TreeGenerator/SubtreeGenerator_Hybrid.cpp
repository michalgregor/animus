#include "SubtreeGenerator_Hybrid.h"

namespace algotree {

/**
* Boost serialization function.
**/

void SubtreeGenerator_Hybrid::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<SubtreeGenerator>(*this);
	ar & _generatorChooser;
}

void SubtreeGenerator_Hybrid::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<SubtreeGenerator>(*this);
	ar & _generatorChooser;
}

/**
* Generates a subtree for Node parent from a specified set of node functors
* and with a specified depth.
**/

void SubtreeGenerator_Hybrid::generate(
	const FunctorRegister& functorRegister,
	Node& parent,
	unsigned int depth
) {
	_generatorChooser()->generate(functorRegister, parent, depth);
}

/**
* Generates a subtree for Node parent from a specified set of node functors
* and with a specified depth. The parent Node's NodeFunctor is also replaced
* by one randomly chosen from the FunctorRegister (with a compatible return
* type).
**/

void SubtreeGenerator_Hybrid::replace(
	const FunctorRegister& functorRegister,
	Node& parent,
	unsigned int depth
) {
	_generatorChooser()->replace(functorRegister, parent, depth);
}

/**
* Assignment operator.
**/

SubtreeGenerator_Hybrid& SubtreeGenerator_Hybrid::operator=(const SubtreeGenerator_Hybrid& obj) {
	if(&obj != this) {
		SubtreeGenerator::operator=(obj);
		_generatorChooser = obj._generatorChooser;
	}
	return *this;
}

/**
* Copy constructor.
**/

SubtreeGenerator_Hybrid::SubtreeGenerator_Hybrid(const SubtreeGenerator_Hybrid& obj):
SubtreeGenerator(obj), _generatorChooser(obj._generatorChooser) {}

/**
* Default constructor.
*/

SubtreeGenerator_Hybrid::SubtreeGenerator_Hybrid():
SubtreeGenerator(), _generatorChooser() {}

/**
* Constructor.
* @param generators The list of generators to choose from when generating subtrees.
* @param probabilities The list probabilities that a particular generator will be selected.
**/

SubtreeGenerator_Hybrid::SubtreeGenerator_Hybrid(
	const std::vector<shared_ptr<SubtreeGenerator> >& generators,
	const std::vector<ProbabilityType>& probabilities
): SubtreeGenerator(), _generatorChooser(generators, probabilities) {}

}//namespace algotree

