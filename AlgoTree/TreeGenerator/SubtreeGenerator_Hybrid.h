#ifndef SUBTREEGENERATORHYBRID_H_
#define SUBTREEGENERATORHYBRID_H_

#include "../system.h"
#include "SubtreeGenerator.h"
#include <Systematic/generator/RouletteObjectGenerator.h>

namespace algotree {

/**
* @class SubtreeGenerator_Hybrid
* @author Michal Gregor
* A SubtreeGenerator containing a list of several SubtreeGenerators and a
* probability distribution specifying how often any of these will be used.
**/
class ALGOTREE_API SubtreeGenerator_Hybrid: public SubtreeGenerator {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//!Roulette generator used to select a tree generator.
	RouletteObjectGenerator<shared_ptr<SubtreeGenerator>, ProbabilityType> _generatorChooser;

public:
	virtual void generate(const FunctorRegister& functorRegister, Node& parent, unsigned int depth);
	virtual void replace(const FunctorRegister& functorRegister, Node& parent, unsigned int depth);

	SubtreeGenerator_Hybrid& operator=(const SubtreeGenerator_Hybrid& generator);
	SubtreeGenerator_Hybrid(const SubtreeGenerator_Hybrid& obj);

	SubtreeGenerator_Hybrid();

	SubtreeGenerator_Hybrid(
		const std::vector<shared_ptr<SubtreeGenerator> >& generators,
		const std::vector<ProbabilityType>& probabilities
	);

	virtual ~SubtreeGenerator_Hybrid() {}
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::SubtreeGenerator_Hybrid)

#endif /* SUBTREEGENERATORHYBRID_H_ */
