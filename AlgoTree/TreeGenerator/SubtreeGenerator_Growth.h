#ifndef SUBTREEGENERATORGROWTH_H_
#define SUBTREEGENERATORGROWTH_H_

#include "../system.h"
#include "SubtreeGenerator.h"
#include <Systematic/generator/BoundaryGenerator.h>

namespace algotree {

/**
* @class SubtreeGenerator_Growth
* @author Michal Gregor
*
* Generates grown subtrees of specified maximum depth. A grown tree is a tree
* in which there is a specified probability that a branch or a leaf will be
* chosen at any given depth; with the exception of the maximum depth. At the
* maximum depth a leaf is always chosen.
*
* It is required that the FunctorRegister contain both non-terminals and
* and terminals for every type that may appear at depth d < maximum depth and
* terminal for every type that may appear at the maximum depth.
*
* The generator can be set to a less strict mode that allows a non-terminal to
* be substituted by a terminal with the same return type if no non-terminal with
* that return type is available (normally an exception is thrown). This
* behaviour can be activated using setSubstitutionAllowed().
**/
class ALGOTREE_API SubtreeGenerator_Growth: public SubtreeGenerator {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The probability that a randomly chosen node at any depth except the
	//! maximum depth will be a branch. The probability of a leaf is
	//! 1 - _pBranch. The range is obviously [0.0f, 1.0f].
	ProbabilityType _pBranch;
	//! Random generator.
	systematic::BoundaryGenerator<ProbabilityType> _randGenerator;
	//! If set to true, substitution of terminal for a non-terminal is allowed.
	//! This means that if there is no terminal with the required return type
	//! available, the non-terminal can be substituted by a terminal with the
	//! same return type (as opposed to throwing an exception).
	bool _substitutionAllowed;

	void _generate(const FunctorRegister& functorRegister, Node& parent, unsigned int depth);

public:
	virtual void generate(const FunctorRegister& functorRegister, Node& parent, unsigned int depth);
	virtual void replace(const FunctorRegister& functorRegister, Node& parent, unsigned int depth);

	bool getSubstitutionAllowed() const;
	void setSubstitutionAllowed(bool allowed);

	/**
	* Returns the pBranch probability, that is - the probability that a randomly
	* generated node at any depth except the maximum depth will be a branch.
	* The probability of a leaf is 1 - _pBranch.
	**/

	ProbabilityType getPBranch() const {return _pBranch;}

	/**
	* Sets the pBranch probability, that is - the probability that a randomly
	* generated node at any depth except the maximum depth will be a branch.
	* The probability of a leaf is 1 - _pBranch. The range of pBranch values
	* is obviously [0.0f, 1.0f], this is, however, not enforced. The behaviour
	* is not defined for other values.
	**/

	void setPBranch(ProbabilityType pBranch) {_pBranch = pBranch;}

	SubtreeGenerator_Growth& operator=(const SubtreeGenerator_Growth& generator);
	SubtreeGenerator_Growth(const SubtreeGenerator_Growth& obj);

	explicit SubtreeGenerator_Growth(ProbabilityType pBranch = 0.5f);
	virtual ~SubtreeGenerator_Growth() {}
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::SubtreeGenerator_Growth)

#endif /* SUBTREEGENERATORGROWTH_H_ */
