#ifndef SUBTREEGENERATORFULL_H_
#define SUBTREEGENERATORFULL_H_

#include "../system.h"
#include "SubtreeGenerator.h"

namespace algotree {

/**
* @class SubtreeGenerator_Full
* @author Michal Gregor
*
* A full subtree generator generates a full subtree, that is, it always selects
* a non-terminal (a branch vs. a leaf) unless the predefined maximum depth has
* been reached. It is therefore required that the FunctorRegister contain
* a non-terminal for every type which may appear at depth d; d < maximum depth.
* It is also required that the FunctorRegister contain a terminal for every
* type which may appear at the maximum depth.
*
* The generator can be set into a less strict mode that allows a non-terminal to
* be substituted by a terminal with the same return type if no non-terminal with
* that return type is available (normally an exception is thrown). This
* behaviour can be activated using setSubstitutionAllowed().
**/
class ALGOTREE_API SubtreeGenerator_Full: public SubtreeGenerator {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! If set to true, substitution of terminal for a non-terminal is allowed.
	//! This means that if there is no terminal with the required return type
	//! available, the non-terminal can be substituted by a terminal with the
	//! same return type (as opposed to throwing an exception).
	bool _substitutionAllowed;

	void _generate(const FunctorRegister& functorRegister, Node& parent, unsigned int depth);

public:
	bool getSubstitutionAllowed() const;
	void setSubstitutionAllowed(bool allowed);

	virtual void generate(const FunctorRegister& functorRegister, Node& parent, unsigned int depth);
	virtual void replace(const FunctorRegister& functorRegister, Node& parent, unsigned int depth);

	SubtreeGenerator_Full();
	virtual ~SubtreeGenerator_Full() {}
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::SubtreeGenerator_Full)

#endif /* SUBTREEGENERATORFULL_H_ */
