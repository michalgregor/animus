#include "TreeGenerator_Full.h"

namespace algotree {

/**
* Boost serialization function.
**/

void TreeGenerator_Full::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeGenerator_Common>(*this);
	ar & _generator;
}

void TreeGenerator_Full::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeGenerator_Common>(*this);
	ar & _generator;
}

/**
* Return true if substitution of a terminal for a non-terminal is allowed.
* [Defaults to false.]
*
* If there is no non-terminal with the required return type available,
* an exception is normally thrown. If substition is allowed, the non-terminal
* can instead be substituted by a terminal with the same return type.
*/

bool TreeGenerator_Full::getSubstitutionAllowed() const {
	return _generator.getSubstitutionAllowed();
}

/**
* Sets whether substitution of a terminal for a non-terminal is allowed.
* [Defaults to false.]
*
* If there is no non-terminal with the required return type available,
* an exception is normally thrown. If substition is allowed, the non-terminal
* can instead be substituted by a terminal with the same return type.
*/

void TreeGenerator_Full::setSubstitutionAllowed(bool allowed) {
	_generator.setSubstitutionAllowed(allowed);
}

/**
* Generates a subtree.
* @param depth Depth of the subtree. 0 means that the functor should have no
* subnodes.
* @param parent The parent node that the subtree should grow from.
**/

void TreeGenerator_Full::generate(Node& parent, unsigned int depth) {
	_generator.generate(_functorRegister, parent, depth);
}

/**
* Generates a subtree. Replaces the node's functor by a new randomly chosen
* functor of the same type.
**/

void TreeGenerator_Full::replace(Node& parent, unsigned int depth) {
	_generator.replace(_functorRegister, parent, depth);
}

/**
* Constructor.
* @param type Return type that the root functor should have.
* @param depth Depth that the generated trees are to have.
**/

TreeGenerator_Full::TreeGenerator_Full(TypeID type, TypeList argList, unsigned int depth):
TreeGenerator_Common(type, argList, depth) {}

/**
* Constructor.
* @param type Return type that the root functor should have.
* @param depth Depth that the generated trees are to have.
* @param functorRegister A shared_ptr to the functor register.
**/

TreeGenerator_Full::TreeGenerator_Full(
	const FunctorRegister& functorRegister,
	TypeID type,
	TypeList argList,
	unsigned int depth
): TreeGenerator_Common(functorRegister, type, argList, depth) {}

/**
 * Default constructor for use by serialization.
 */

TreeGenerator_Full::TreeGenerator_Full(): TreeGenerator_Common() {}

} //namespace algotree
