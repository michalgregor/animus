#include "TreeGenerator_Common.h"

namespace algotree {

/**
* Boost serialization function.
**/

void TreeGenerator_Common::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeGenerator>(*this);
	ar & _functorRegister;
	ar & _rootType;
	ar & _rootFactory;
	ar & _argList;
	ar & _defaultDepth;
}

void TreeGenerator_Common::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeGenerator>(*this);
	ar & _functorRegister;
	ar & _rootType;
	ar & _rootFactory;
	ar & _argList;
	ar & _defaultDepth;
}

/**
 * Adds FormalArguments of types specified in the argument list to the
 * FunctorRegister.
 */

void TreeGenerator_Common::addArgFunctorsToRegister() {
	for(TypeList::Handles::iterator iter = _argList.handles.begin(); iter != _argList.handles.end(); iter++) {
		_functorRegister.addFunctor(intrusive_ptr<NodeFunctor>(new FormalParameter(*iter)));
	}
}

/**
* Generates an algorithmic tree from the set of previously added node functors
* and with the specified depth.
**/

Tree TreeGenerator_Common::operator()() {
	return generate(_defaultDepth);
}

/**
* Generates an algorithmic tree from the set of previously added node functors
* and with the specified depth. TracedError_InvalidArgument is thrown for depth
* equal to 0.
**/

Tree TreeGenerator_Common::generate(unsigned int depth) {
	if(!depth) throw TracedError_InvalidArgument("Generate called with depth == 0.");
	Tree ret_tree;

	if(_rootFactory) {
		Tree tree(_rootFactory->getReturnType(), _argList);
		tree.getRoot()->setFunctor((*_rootFactory)(tree.context()));
		_functorRegister.initTreeContext(tree.context());
		generate(*(tree.getRoot()), depth - 1);

		ret_tree.swap(tree);
	} else {
		Tree tree(_rootType, _argList);
		_functorRegister.initTreeContext(tree.context());
		replace(*(tree.getRoot()), depth);

		ret_tree.swap(tree);
	}

	return ret_tree;
}

/**
* Calls generate(context, parent, depth), where depth is the default depth
* minus depth of the parent. If parent's depth is already greater than the
* default, zero depth is used.
**/

void TreeGenerator_Common::generate(Node& parent) {
	unsigned int depth(parent.depth());

	if(_defaultDepth > depth) depth = _defaultDepth - depth;
	else depth = 0;

	generate(parent, depth);
}

/**
* Calls replace(context, parent, depth), where depth is the default depth
* minus depth of the parent. If parent's depth is already greater than the
* default, zero depth is used.
**/

void TreeGenerator_Common::replace(Node& parent) {
	unsigned int depth(parent.depth());

	if(_defaultDepth > depth) depth = _defaultDepth - depth;
	else depth = 0;

	replace(parent, depth);
}

/**
* Sets the functor register. FormalParameter functor should not be added
* manually to the functorRegister as this is handled automatically, using
* the specified argument list.
**/

void TreeGenerator_Common::setFunctorRegister(const FunctorRegister& functorRegister) {
	_functorRegister = functorRegister;
	addArgFunctorsToRegister();
}

/**
* Sets _rootType [see information on that member]. Resets the _rootFactory.
**/

void TreeGenerator_Common::setRootType(TypeID type) {
	_rootType = type;
	_rootFactory = intrusive_ptr<FunctorFactory>();
}

/**
* Returns _rootType [see information on that member].
**/

TypeID TreeGenerator_Common::getRootType() const {
	return _rootType;
}

/**
* Sets _rootFactory [see information on that member] by wrapping rootFunctor in
* FunctorFactory_CloneFunctor.
**/

void TreeGenerator_Common::setRootFunctor(const intrusive_ptr<NodeFunctor>& rootFunctor) {
	_rootFactory = intrusive_ptr<FunctorFactory>(new FunctorFactory_CloneFunctor(rootFunctor));
	if(_rootFactory) _rootType = _rootFactory->getReturnType();
}

/**
 * Sets _rootFactory [see information on that member] to rootFactory.
 */

void TreeGenerator_Common::setRootFactory(const intrusive_ptr<FunctorFactory>& rootFactory) {
	_rootFactory = rootFactory;
	if(_rootFactory) _rootType = _rootFactory->getReturnType();
}

/**
* Returns _rootFactory [see information on that member].
**/

intrusive_ptr<FunctorFactory> TreeGenerator_Common::getRootFactory() {
	return _rootFactory;
}

/**
* Resets root factory to NULL.
**/

void TreeGenerator_Common::resetRootFactory() {
	_rootFactory = intrusive_ptr<FunctorFactory>();
}

/**
* Returns the argument list.
**/

const TypeList& TreeGenerator_Common::getTypeList() const {
	return _argList;
}

/**
* Returns the default depth.
**/

unsigned int TreeGenerator_Common::getDefaultDepth() const {
	return _defaultDepth;
}

/**
* Sets the default depth.
**/

void TreeGenerator_Common::setDefaultDepth(unsigned int depth) {
	_defaultDepth = depth;
}

/**
* Assignment operator.
**/

TreeGenerator_Common&
TreeGenerator_Common::operator=(const TreeGenerator_Common& generator) {
	if(this != &generator) {
		TreeGenerator::operator=(generator);

		_functorRegister = generator._functorRegister;
		_rootType = generator._rootType;

		_rootFactory = generator._rootFactory;

		_argList = generator._argList;
		_defaultDepth = generator._defaultDepth;
	}
	return *this;
}

/**
* Copy constructor.
**/

TreeGenerator_Common::TreeGenerator_Common(const TreeGenerator_Common& generator):
TreeGenerator(generator),
_functorRegister(generator._functorRegister),
_rootType(generator._rootType),
_rootFactory(generator._rootFactory),
_argList(generator._argList), _defaultDepth(generator._defaultDepth) {}

/**
* Constructor.
* @param type Return type that the root functor should have.
* @param defaultDepth The default depth of a tree - used for trees generated
* using operator()().
* @param argList The argument list the Tree is to have.
**/

TreeGenerator_Common::TreeGenerator_Common(
	TypeID type,
	TypeList argList,
	unsigned int defaultDepth
): _functorRegister(), _rootType(type), _rootFactory(), _argList(argList),
_defaultDepth(defaultDepth) {}

/**
* Constructor.
* @param functorRegister A shared_ptr to the functor register. FormalParameter
* functors should not be added manually to the functorRegister as
* this is handled automatically, using the specified argument list.
* @param type Return type that the root functor should have.
* @param defaultDepth The default depth of a tree - used for trees generated
* using operator()().
* @param argList The argument list the Tree is to have.
**/

TreeGenerator_Common::TreeGenerator_Common(
	const FunctorRegister& functorRegister,
	TypeID type,
	TypeList argList,
	unsigned int defaultDepth
): _functorRegister(functorRegister), _rootType(type), _rootFactory(),
_argList(argList), _defaultDepth(defaultDepth) {
	addArgFunctorsToRegister();
}

/**
 * Default constructor for use by serialization.
 */

TreeGenerator_Common::TreeGenerator_Common(): _functorRegister(),
_rootType(), _rootFactory(), _argList(), _defaultDepth(5) {}

} //namespace algotree
