#ifndef ALGO_FULLTREEGENERATOR_H_INCLUDED
#define ALGO_FULLTREEGENERATOR_H_INCLUDED

#include "TreeGenerator_Common.h"
#include "SubtreeGenerator_Full.h"

namespace algotree {

/**
* @class TreeGenerator_Full
* @author Michal Gregor
* Generates full trees of specified depth. A full tree is such tree in which
* every node is a branch until the specified depth is reached.
*
* It is required that the FunctorRegister contain a non-terminal for every type
* which may appear at depth d; d < maximum depth. It is also required that
* the FunctorRegister contain a terminal for every type which may appear
* at the maximum depth.
*
* The generator can be set into a less strict mode that allows a non-terminal to
* be substituted by a terminal with the same return type if no non-terminal with
* that return type is available (normally an exception is thrown). This
* behaviour can be activated using setSubstitutionAllowed().
**/
class ALGOTREE_API TreeGenerator_Full: public TreeGenerator_Common {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

	TreeGenerator_Full();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	SubtreeGenerator_Full _generator;

public:
	bool getSubstitutionAllowed() const;
	void setSubstitutionAllowed(bool allowed);

	virtual void generate(Node& parent, unsigned int depth);
	virtual void replace(Node& node, unsigned int depth);

	explicit TreeGenerator_Full(
		TypeID type,
		TypeList argList,
		unsigned int depth = 5
	);

	explicit TreeGenerator_Full(
		const FunctorRegister& functorRegister,
		TypeID type,
		TypeList argList,
		unsigned int depth = 5
	);

	virtual ~TreeGenerator_Full() {}
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::TreeGenerator_Full)

#endif // ALGO_FULLTREEGENERATOR_H_INCLUDED
