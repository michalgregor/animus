#ifndef SUBTREEGENERATOR_H_
#define SUBTREEGENERATOR_H_

#include "../system.h"
#include "../Node.h"
#include "../FunctorRegister.h"
#include "../Tree.h"

namespace algotree {

/**
* @class SubtreeGenerator
* @author Michal Gregor
* A base of classes implementing subtree generator mechanisms such as the full
* method and the growth method.
**/
class ALGOTREE_API SubtreeGenerator {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	/**
	* Generates a subtree for Node parent from a specified set of node functors
	* and with a specified depth.
	**/

	virtual void generate(const FunctorRegister& functorRegister, Node& parent, unsigned int depth) = 0;

	/**
	* Generates a subtree for Node parent from a specified set of node functors
	* and with a specified depth. The parent Node's NodeFunctor is also replaced
	* by one randomly chosen from the FunctorRegister (with a compatible return
	* type).
	**/

	virtual void replace(const FunctorRegister& functorRegister, Node& parent, unsigned int depth) = 0;

	virtual ~SubtreeGenerator() = 0;
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::SubtreeGenerator)

#endif /* SUBTREEGENERATOR_H_ */
