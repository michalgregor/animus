#ifndef ALGO_TREEGENERATOR_H_INCLUDED
#define ALGO_TREEGENERATOR_H_INCLUDED

#include <Systematic/generator/Generator.h>
#include "../system.h"
#include "../Tree.h"

namespace algotree {

class TreeContext;

/**
* @class TreeGenerator
* @author Michal Gregor
* This class is to be used to auto-construct an algorithmic tree.
*
* All TreeGenerators MUST be thread-safe for use in multiple threads.
**/
class ALGOTREE_API TreeGenerator: public systematic::Generator<Tree> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	using systematic::Generator<Tree>::operator();

	/**
	* Generates an algorithmic tree from the set of previously added node functors
	* and with the specified depth.
	**/

	virtual Tree operator()() = 0;

	/**
	* Generates an algorithmic tree from the set of previously added node functors
	* and with the specified depth.
	**/

	virtual Tree generate(unsigned int depth) = 0;

	/**
	* Generates a subtree.
	* @param depth Depth of the subtree. 0 means that the functor should have no
	* subnodes.
	* @param parent The parent node that the subtree should grow from.
	**/

	virtual void generate(Node& parent, unsigned int depth) = 0;

	/**
	* Generates a subtree. Depth should be set to default minus depth of the parent.
	* @param parent The parent node that the subtree should grow from.
	**/

	virtual void generate(Node& parent) = 0;

	/**
	* Generates a subtree. Replaces the node's functor by a new randomly chosen
	* functor of the same type.
	**/

	virtual void replace(Node& parent, unsigned int depth) = 0;

	/**
	* Generates a subtree. Replaces the node's functor by a new randomly chosen
	* functor of the same type. Depth should be set to default minus depth of the parent.
	**/

	virtual void replace(Node& parent) = 0;

	/**
	* Returns the default depth.
	**/

	virtual unsigned int getDefaultDepth() const = 0;

	/**
	* Sets the default depth.
	**/

	virtual void setDefaultDepth(unsigned int depth) = 0;

	virtual ~TreeGenerator() = 0;
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::TreeGenerator)

#endif // ALGO_TREEGENERATOR_H_INCLUDED
