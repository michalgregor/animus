#include "TreeGenerator_Hybrid.h"

namespace algotree {

/**
* Boost serialization function.
**/

void TreeGenerator_Hybrid::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeGenerator_Common>(*this);
	ar & _generator;
}

void TreeGenerator_Hybrid::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeGenerator_Common>(*this);
	ar & _generator;
}

/**
* Generates a subtree.
* @param depth Depth of the subtree. 0 means that the functor should have no
* subnodes.
* @param parent The parent node that the subtree should grow from.
**/

void TreeGenerator_Hybrid::generate(Node& parent, unsigned int depth) {
	_generator.generate(_functorRegister, parent, depth);
}

/**
* Generates a subtree. Replaces the node's functor by a new randomly chosen
* functor of the same type.
**/

void TreeGenerator_Hybrid::replace(Node& parent, unsigned int depth) {
	_generator.replace(_functorRegister, parent, depth);
}

/**
 * Assignment.
 */

TreeGenerator_Hybrid& TreeGenerator_Hybrid::operator=(const TreeGenerator_Hybrid& obj) {
	if(this != &obj) {
		TreeGenerator_Common::operator=(obj);
		_generator = obj._generator;
	}

	return *this;
}

/**
 * Copy constructor.
 */

TreeGenerator_Hybrid::TreeGenerator_Hybrid(const TreeGenerator_Hybrid& obj):
TreeGenerator_Common(obj), _generator(obj._generator) {}

/**
* Default constructor for use by serialization.
*/

TreeGenerator_Hybrid::TreeGenerator_Hybrid():
TreeGenerator_Common(), _generator() {}

/**
* Constructor.
* @param type Return type that the root functor should have.
* @param depth Depth that the generated trees are to have.
**/

TreeGenerator_Hybrid::TreeGenerator_Hybrid(
	const std::vector<shared_ptr<SubtreeGenerator> >& generators,
	const std::vector<ProbabilityType>& probabilities,
	TypeID type,
	TypeList argList,
	unsigned int depth
): TreeGenerator_Common(type, argList, depth),
_generator(generators, probabilities) {}

/**
* Constructor.
* @param type Return type that the root functor should have.
* @param depth Depth that the generated trees are to have.
* @param functorRegister A shared_ptr to the functor register.
**/

TreeGenerator_Hybrid::TreeGenerator_Hybrid(
	const FunctorRegister& functorRegister,
	const std::vector<shared_ptr<SubtreeGenerator> >& generators,
	const std::vector<ProbabilityType>& probabilities,
	TypeID type,
	TypeList argList,
	unsigned int depth
): TreeGenerator_Common(functorRegister, type, argList, depth),
_generator(generators, probabilities) {}

} //namespace algotree
