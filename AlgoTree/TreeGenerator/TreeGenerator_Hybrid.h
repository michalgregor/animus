#ifndef RAMPEDPORTIONGENERATOR_H_
#define RAMPEDPORTIONGENERATOR_H_

#include "../system.h"
#include "TreeGenerator_Common.h"
#include "SubtreeGenerator_Hybrid.h"

namespace algotree {

/**
* @class TreeGenerator_Hybrid
* @author Michal Gregor
* A TreeGenerator containing a list of several TreeGenerator and a probability
* distribution specifying how often any of these will be used.
**/
class ALGOTREE_API TreeGenerator_Hybrid: public TreeGenerator_Common {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The underlying SubtreeGenerator.
	SubtreeGenerator_Hybrid _generator;

public:
	virtual void generate(Node& parent, unsigned int depth);
	virtual void replace(Node& node, unsigned int depth);

	TreeGenerator_Hybrid& operator=(const TreeGenerator_Hybrid& obj);
	TreeGenerator_Hybrid(const TreeGenerator_Hybrid& obj);

	TreeGenerator_Hybrid();

	explicit TreeGenerator_Hybrid(
		const std::vector<shared_ptr<SubtreeGenerator> >& generators,
		const std::vector<ProbabilityType>& probabilities,
		TypeID type,
		TypeList argList,
		unsigned int depth = 5
	);

	explicit TreeGenerator_Hybrid(
		const FunctorRegister& functorRegister,
		const std::vector<shared_ptr<SubtreeGenerator> >& generators,
		const std::vector<ProbabilityType>& probabilities,
		TypeID type,
		TypeList argList,
		unsigned int depth = 5
	);
};

}//namespace algotree

SYS_EXPORT_CLASS(algotree::TreeGenerator_Hybrid)

#endif /* RAMPEDPORTIONGENERATOR_H_ */
