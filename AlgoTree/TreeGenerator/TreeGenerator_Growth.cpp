#include "TreeGenerator_Growth.h"

namespace algotree {

void TreeGenerator_Growth::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeGenerator_Common>(*this);
	ar & _generator;
}

void TreeGenerator_Growth::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TreeGenerator_Common>(*this);
	ar & _generator;
}

/**
* Return true if substitution of a terminal for a non-terminal is allowed.
* [Defaults to false.]
*
* If there is no non-terminal with the required return type available,
* an exception is normally thrown. If substition is allowed, the non-terminal
* can instead be substituted by a terminal with the same return type.
*/

bool TreeGenerator_Growth::getSubstitutionAllowed() const {
	return _generator.getSubstitutionAllowed();
}

/**
* Sets whether substitution of a terminal for a non-terminal is allowed.
* [Defaults to false.]
*
* If there is no non-terminal with the required return type available,
* an exception is normally thrown. If substition is allowed, the non-terminal
* can instead be substituted by a terminal with the same return type.
*/

void TreeGenerator_Growth::setSubstitutionAllowed(bool allowed) {
	_generator.setSubstitutionAllowed(allowed);
}

/**
* Generates a subtree.
* @param depth Depth of the subtree. 0 means that the functor should have no
* subnodes.
* @param parent The parent node that the subtree should grow from.
**/

void TreeGenerator_Growth::generate(Node& parent, unsigned int depth) {
	_generator.generate(_functorRegister, parent, depth);
}

/**
* Generates a subtree. Replaces the node's functor by a new randomly chosen
* functor of the same type.
**/

void TreeGenerator_Growth::replace(Node& parent, unsigned int depth) {
	_generator.replace(_functorRegister, parent, depth);
}

/**
 * Assignment operator.
 */

TreeGenerator_Growth& TreeGenerator_Growth::operator=(const TreeGenerator_Growth& generator) {
	if(this != &generator) {
		TreeGenerator_Common::operator=(generator);
		_generator = generator._generator;
	}

	return *this;
}

/**
 * Copy constructor.
 */

TreeGenerator_Growth::TreeGenerator_Growth(const TreeGenerator_Growth& generator):
TreeGenerator_Common(generator), _generator(generator._generator) {}

/**
* Constructor.
* @param type Return type that the root functor should have.
* @param maxDepth How deep should the generated tree be.
* @param pBranch The probability that a randomly generated node at any depth
*  except the maximum depth will be a branch. The probability of a leaf is
* 1 - _pBranch. The range is obviously [0.0f, 1.0f].
**/

TreeGenerator_Growth::
TreeGenerator_Growth(TypeID type, TypeList argList, unsigned int maxDepth, ProbabilityType pBranch):
TreeGenerator_Common(type, argList, maxDepth), _generator(pBranch) {}

/**
* Constructor.
* @param functorRegister A shared_ptr to the functor register.
* @param type Return type that the root functor should have.
* @param maxDepth How deep should the generated tree be.
* @param pBranch The probability that a randomly generated node at any depth
* except the maximum depth will be a branch. The probability of a leaf is
* 1 - _pBranch. The range is obviously [0.0f, 1.0f].
**/

TreeGenerator_Growth::TreeGenerator_Growth(
	const FunctorRegister& functorRegister,
	TypeID type,
	TypeList argList,
	unsigned int maxDepth,
	ProbabilityType pBranch
): TreeGenerator_Common(functorRegister, type, argList, maxDepth),
_generator(pBranch) {}

TreeGenerator_Growth::TreeGenerator_Growth(): TreeGenerator_Common(), _generator() {}

} //namespace algotree
