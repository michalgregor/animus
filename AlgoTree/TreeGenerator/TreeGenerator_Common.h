#ifndef ALGO_TREEGENERATORCOMMON_H_INCLUDED
#define ALGO_TREEGENERATORCOMMON_H_INCLUDED

#include "TreeGenerator.h"
#include "../FunctorRegister.h"
#include "../FunctorFactory/FunctorFactory.h"
#include "../NodeFunctor/FormalParameter.h"

namespace algotree {

/**
* @class SubtreeTreeGenerator_Common
* @author Michal Gregor
* This class provides an implementation of some of the virtual methods that is
* common for all TreeGenerators based on functor registers, etc.
*
* FormalParameter functors should not be added manually to the functorRegister as
* this is handled automatically, using the specified argument list.
*
* Get functions that are non-constant are not thread safe. Use with caution.
**/
class ALGOTREE_API TreeGenerator_Common: public TreeGenerator {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	TreeGenerator_Common();

protected:
	//! A shared_ptr to the functor register (may be NULL).
	FunctorRegister _functorRegister;
	//! Stores root functor type. If _rootFunctor != NULL, this is ignored.
	TypeID _rootType;
	//! Stores factory used to generate functors for root nodes. Unless this is
	//! a NULL pointer, root node of every generated tree will use this to
	//! generate its root functor.
	intrusive_ptr<FunctorFactory> _rootFactory;
	//! Stores the argument list.
	TypeList _argList;
	//! The default depth.
	unsigned int _defaultDepth;

protected:
	void addArgFunctorsToRegister();

public:
	virtual Tree operator()();
	virtual Tree generate(unsigned int depth);

	virtual void generate(Node& parent);
	virtual void replace(Node& parent);

	virtual void generate(Node& parent, unsigned int depth) = 0;
	virtual void replace(Node& parent, unsigned int depth) = 0;

	void setFunctorRegister(const FunctorRegister& functorRegister);

	void setRootType(TypeID type);
	TypeID getRootType() const;

	void setRootFunctor(const intrusive_ptr<NodeFunctor>& rootFunctor);
	void setRootFactory(const intrusive_ptr<FunctorFactory>& rootFactory);

	intrusive_ptr<FunctorFactory> getRootFactory();
	void resetRootFactory();

	const TypeList& getTypeList() const;

	virtual unsigned int getDefaultDepth() const;
	virtual void setDefaultDepth(unsigned int depth);

	TreeGenerator_Common& operator=(const TreeGenerator_Common& generator);
	TreeGenerator_Common(const TreeGenerator_Common& generator);

	TreeGenerator_Common(
		TypeID type,
		TypeList argList,
		unsigned int defaultDepth = 5
	);

	TreeGenerator_Common(
		const FunctorRegister& functorRegister,
		TypeID type,
		TypeList argList,
		unsigned int defaultDepth = 5
	);

	virtual ~TreeGenerator_Common() {}
};

} //namespace algotree

SYS_EXPORT_CLASS(algotree::TreeGenerator_Common)

#endif // ALGO_TREEGENERATORCOMMON_H_INCLUDED
