#include "TreeGenerator.h"

namespace algotree {

/**
* Boost serialization function.
**/

void TreeGenerator::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Generator<Tree> >(*this);
}

void TreeGenerator::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Generator<Tree> >(*this);
}

/**
 * Empty body of the pure virtual destructor.
 */

TreeGenerator::~TreeGenerator() {}

}//namespace algotree
