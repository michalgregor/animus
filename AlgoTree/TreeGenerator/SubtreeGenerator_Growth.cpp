#include "SubtreeGenerator_Growth.h"

namespace algotree {

/**
* Boost serialization function.
**/

void SubtreeGenerator_Growth::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<SubtreeGenerator>(*this);
	ar & _pBranch;
	ar & _randGenerator;
	ar & _substitutionAllowed;
}

void SubtreeGenerator_Growth::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<SubtreeGenerator>(*this);
	ar & _pBranch;
	ar & _randGenerator;
	ar & _substitutionAllowed;
}

/**
* Return true if substitution of a terminal for a non-terminal is allowed.
* [Defaults to false.]
*
* If there is no non-terminal with the required return type available,
* an exception is normally thrown. If substition is allowed, the non-terminal
* can instead be substituted by a terminal with the same return type.
*/

bool SubtreeGenerator_Growth::getSubstitutionAllowed() const {
	return _substitutionAllowed;
}

/**
* Sets whether substitution of a terminal for a non-terminal is allowed.
* [Defaults to false.]
*
* If there is no non-terminal with the required return type available,
* an exception is normally thrown. If substition is allowed, the non-terminal
* can instead be substituted by a terminal with the same return type.
*/

void SubtreeGenerator_Growth::setSubstitutionAllowed(bool allowed) {
	_substitutionAllowed = allowed;
}

/**
* Generates a subtree.
*
* When depth is 0 and generate is called, this indicates that there is a node
* which requires children (otherwise generate would not get called), but these
* cannot be generated. A TracedError_InvalidArgument is thrown.
*
* @param depth Depth of the subtree. 0 means that the functor should have no
* subnodes.
* @param parent The parent node that the subtree should grow from.
**/

void SubtreeGenerator_Growth::_generate(
	const FunctorRegister& functorRegister,
	Node& parent,
	unsigned int depth
) {
	if(!depth) throw TracedError_InvalidArgument("Generate called with depth == 0.");
	TreeContext* treeContext(parent.getTreeContext());

	Node* node = parent.child();
	if(depth == 1) {
		while(node != NULL) {
			node->setFunctor(functorRegister.newRandLeaf(treeContext, node->getReturnType(), TC_Convertible));
			node = node->next();
		}
	} else {
		while(node != NULL) {
			if(_randGenerator() < _pBranch) {
				if(_substitutionAllowed) {
					try {
						node->setFunctor(functorRegister.newRandBranch(treeContext, node->getReturnType(), TC_Convertible));
						_generate(functorRegister, *node, depth-1);
					} catch(AlgoError_NoFunctorFound&) {
						node->setFunctor(functorRegister.newRandLeaf(treeContext, node->getReturnType(), TC_Convertible));
					}
				} else {
					node->setFunctor(functorRegister.newRandBranch(treeContext, node->getReturnType(), TC_Convertible));
					_generate(functorRegister, *node, depth-1);
				}
			} else {
				node->setFunctor(functorRegister.newRandLeaf(treeContext, node->getReturnType(), TC_Convertible));
			}
			node = node->next();
		}
	}
}

/**
* Generates a subtree for Node parent from a specified set of node functors
* and with a specified depth.
*
* When depth is 0 and generate is called, this indicates that there is a node
* which requires children (otherwise generate would not get called), but these
* cannot be generated. A TracedError_InvalidArgument is thrown.
**/

void SubtreeGenerator_Growth::generate(
	const FunctorRegister& functorRegister,
	Node& parent,
	unsigned int depth
) {
	_generate(functorRegister, parent, depth);
}

/**
* Generates a subtree for Node parent from a specified set of node functors
* and with a specified depth. The parent Node's NodeFunctor is also replaced
* by one randomly chosen from the FunctorRegister (with a compatible return
* type).
**/

void SubtreeGenerator_Growth::replace(
	const FunctorRegister& functorRegister,
	Node& parent,
	unsigned int depth
) {
	TreeContext* treeContext(parent.getTreeContext());

	if(depth == 0 || _randGenerator() >= _pBranch) {
		parent.setFunctor(functorRegister.newRandLeaf(treeContext, parent.getReturnType(), TC_Convertible));
	} else {
		if(_substitutionAllowed) {
			try {
				parent.setFunctor(functorRegister.newRandBranch(parent.getTreeContext(), parent.getReturnType(), TC_Convertible));
			} catch(AlgoError_NoFunctorFound&) {
				parent.setFunctor(functorRegister.newRandLeaf(parent.getTreeContext(), parent.getReturnType(), TC_Convertible));
			}
		} else {
			parent.setFunctor(functorRegister.newRandBranch(parent.getTreeContext(), parent.getReturnType(), TC_Convertible));
		}
		_generate(functorRegister, parent, depth);
	}
}

/**
* Assignment operator.
**/	

SubtreeGenerator_Growth& SubtreeGenerator_Growth::operator=(const SubtreeGenerator_Growth& obj) {
	if(&obj != this) {
		SubtreeGenerator::operator=(obj);	
		_pBranch = obj._pBranch;
	}	
	return *this;
}

/**
* Copy constructor.
**/

SubtreeGenerator_Growth::SubtreeGenerator_Growth(const SubtreeGenerator_Growth& obj):
SubtreeGenerator(obj), _pBranch(obj._pBranch), _randGenerator(0.0f, 1.0f),
_substitutionAllowed(false) {}

/**
* Constructor.
**/

SubtreeGenerator_Growth::SubtreeGenerator_Growth(ProbabilityType pBranch):
SubtreeGenerator(), _pBranch(pBranch), _randGenerator(0.0f, 1.0f),
_substitutionAllowed(false) {}

}//namespace algotree
