#include "AlgoType.h"
#include "Context/ProcessContext.h"

namespace algotree {
	AlgoConverterRegisterType& AlgoConverterRegister() {
		static AlgoConverterRegisterType converterRegister;
		return converterRegister;
	}
} //namespace algotree
