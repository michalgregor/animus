#ifndef GEN_INDIVIDUALEVALUATOR_H_INCLUDED
#define GEN_INDIVIDUALEVALUATOR_H_INCLUDED

#include "system.h"

namespace genetor {

template<class I>
class IndividualEvaluator {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& UNUSED(ar), const unsigned int UNUSED(version)) {}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:

	/**
	* Used to compute fitness score of a single individual. This must be a
	* constant function so that a single functor can simultaneously be used
	* from multiple threads thus allowing for parallelism.
	**/

	virtual FitnessType operator()(const I& individual) const = 0;
	virtual ~IndividualEvaluator() = 0;
};

/**
* Empty body of the pure virtual destructor.
**/

template<class I>
IndividualEvaluator<I>::~IndividualEvaluator() {
	SYS_EXPORT_INSTANCE()
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::IndividualEvaluator, 1)

#endif // GEN_INDIVIDUALEVALUATOR_H_INCLUDED
