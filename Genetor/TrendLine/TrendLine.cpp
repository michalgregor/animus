#include "TrendLine.h"

namespace genetor {

/**
* Boost serialization function.
**/

void TrendLine::serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

void TrendLine::serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

/**
 * Empty body of the pure virtual destructor.
 */

TrendLine::~TrendLine() {}

}//namespace genetor
