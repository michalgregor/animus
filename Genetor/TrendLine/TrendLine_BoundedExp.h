#ifndef GEN_TRENDBOUNDEDEXP_H_INCLUDED
#define GEN_TRENDBOUNDEDEXP_H_INCLUDED

#include "TrendLine.h"

namespace genetor {

/**
* @class TrendLine_BoundedExp
* This is an exponential decreasing or increasing from a predefined initial
* value to until it reaches a stable value at a defined point after which it
* remains constant.
**/
class GENETOR_API TrendLine_BoundedExp: public TrendLine {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	TrendLine_BoundedExp();

private:
	//! The x-point at which the trend reaches the stable value.
	float _stableXPoint;
	//! The value that the trend holds after x reaches _stableXPoint.
	float _stableYPoint;
	//! Coefficients of the exponential function Ae^(bx).
	float A, b;

public:
	virtual float operator()(float x) const;
	TrendLine_BoundedExp(float yIntercept, float stableYPoint, float stableXPoint);
	virtual ~TrendLine_BoundedExp() {}
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::TrendLine_BoundedExp)

#endif // GEN_TRENDBOUNDEDEXP_H_INCLUDED
