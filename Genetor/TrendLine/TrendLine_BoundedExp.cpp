#include "TrendLine_BoundedExp.h"
#include <cmath>

namespace genetor {

void TrendLine_BoundedExp::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TrendLine>(*this);
	ar & _stableXPoint;
	ar & _stableYPoint;
	ar & A;
	ar & b;
}

void TrendLine_BoundedExp::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<TrendLine>(*this);
	ar & _stableXPoint;
	ar & _stableYPoint;
	ar & A;
	ar & b;
}

float TrendLine_BoundedExp::operator()(float x) const {
	if(x < _stableXPoint) return A*std::exp(x*b);
	else return _stableYPoint;
}

/**
 * Default constructor. For use in serialization.
 */

TrendLine_BoundedExp::TrendLine_BoundedExp(): _stableXPoint(), _stableYPoint(),
A(), b() {}

/**
 * Constructor.
 * @param yIntercept The value of y at x = 0.
 * @param stableYPoint The final value of y (the value of y after the transition
 * is complete).
 * @param stableXPoint How long the transition should take. The value of x
 * at which y will reach value stableYPoint.
 */

TrendLine_BoundedExp::
TrendLine_BoundedExp(float yIntercept, float stableYPoint, float stableXPoint):
_stableXPoint(stableXPoint), _stableYPoint(stableYPoint), A(yIntercept),
b(1/stableXPoint*std::log(stableYPoint/yIntercept)) {}

} //namespace genetor
