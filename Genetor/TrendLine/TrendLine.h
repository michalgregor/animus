#ifndef GEN_TRENDLINE_H_INCLUDED
#define GEN_TRENDLINE_H_INCLUDED

#include "../system.h"

namespace genetor {

/**
* Trend line represents a float -> float mapping. It is currently used in
* genetor::GeneticAlgorithm_Dynamic to control the size of population based
* on the number of generation.
**/
class GENETOR_API TrendLine {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	virtual float operator()(float x) const = 0;
	virtual ~TrendLine() = 0;
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::TrendLine)

#endif // GEN_TRENDLINE_H_INCLUDED
