#ifndef Genetor_npoint_division_H_
#define Genetor_npoint_division_H_

namespace genetor {

std::vector<unsigned int> GENETOR_API npoint_division(unsigned int n, unsigned int lbound, unsigned int ubound);

} //namespace genetor

#endif //Genetor_npoint_division_H_
