#ifndef Genetor_arithmetic_crossover_H_
#define Genetor_arithmetic_crossover_H_

namespace genetor {

template<class Type>
void arithmetic_crossover(Type& gene1, Type& gene2, Type alpha) {
	Type x(gene1);
	gene1 = alpha*gene2 + (1 - alpha)*x;
	gene2 = alpha*x + (1 - alpha)*gene2;
}

} //namespace genetor

#endif //Genetor_arithmetic_crossover_H_
