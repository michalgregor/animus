#include <Systematic/generator/HybridSeqGenerator.h>
#include "../system.h"
#include "npoint_division.h"

namespace genetor {

/**
 * Returns a number of points from range [lbound, ubound] that divide
 * the range into subranges such that certain parts of the original range
 * may be excluded, but the points are order, so that p1 < p2 < p3 ...
 * The number of points returned is always even and every pair represents
 * bounds of a single subrange.
 *
 * An TracedError_InvalidArgument is thrown when the number of points requested
 * is greater than available in [lbound, ubound].
 *
 * @param n The number of points used to divide the range. If this is an odd
 * number, an additional point is added, which is equal to lbound. In this case
 * p1 <= p2 < p3 ... as p1 and p2 may both equal lbound.
 */

std::vector<unsigned int> npoint_division(unsigned int n, unsigned int lbound, unsigned int ubound) {
	if((n > ubound - lbound + 1) || ((n & 1) && (n > ubound - lbound)))
		throw TracedError_InvalidArgument("The number of points requested is greater than the number of points available in range [lbound, ubound].");

	systematic::HybridSeqGenerator<unsigned int> generator(n, lbound, ubound);
	std::vector<unsigned int> points;

	//if odd, lbound is selected as a crossing point to make the number even
	if(n & 1) {
		n++;
		points = generator(n, false);
		//sort the points ascendingly
		std::sort(points.begin(), points.end());
		points[0] = lbound;
	} else {
		points = generator(n, false);
		//sort the points ascendingly
		std::sort(points.begin(), points.end());
	}

	return points;
}

} //namespace genetor
