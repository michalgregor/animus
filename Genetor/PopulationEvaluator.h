#ifndef GEN_POPULATIONEVALUATOR_H_INCLUDED
#define GEN_POPULATIONEVALUATOR_H_INCLUDED

#include "system.h"
#include <vector>

namespace genetor {

/**
* @class PopulationEvaluator
* @brief Generation-wide fitness evaluator.
*
* Computes fitness for a list of Individuals. This generation-wide level of
* evaluation is provided so that the process may be parallelized.
**/

template<class Individual>
class PopulationEvaluator {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& UNUSED(ar), const unsigned int UNUSED(version)) {}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:

	virtual std::vector<FitnessType> operator()(const std::vector<Individual>& individual) = 0;
	virtual ~PopulationEvaluator() = 0;
};

/**
* Empty body of the pure virtual destructor.
**/

template<class Individual>
PopulationEvaluator<Individual>::~PopulationEvaluator() {
	SYS_EXPORT_INSTANCE()
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::PopulationEvaluator, 1)

#endif // GEN_POPULATIONEVALUATOR_H_INCLUDED
