#ifndef GEN_BASICPOPULATIONEVALUATOR_H_INCLUDED
#define GEN_BASICPOPULATIONEVALUATOR_H_INCLUDED

#include "system.h"
#include "PopulationEvaluator.h"
#include "IndividualEvaluator.h"
#include <Systematic/exception/OpenMPException.h>

namespace genetor {

using systematic::OpenMPException;

/**
* @class PopulationEvaluator_Basic
* @brief Simple generation-wide fitness functor.
*
* The most straight-forward implementation of PopulationEvaluator, which simply
* takes (one by one) every Individual and computes its fitness using the
* provided fitness functor.
*
* @tparam I Type of individual.
* @tparam FitnessType Type of the fitness value.
* @tparam OpenMP Whether or not OpenMP parallelization should be employed.
**/

template<class I, bool OpenMP = true>
class PopulationEvaluator_Basic: public PopulationEvaluator<I> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<PopulationEvaluator<I> >(*this);
		ar & _fitness;
	}

	PopulationEvaluator_Basic();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The functor used to compute fitness of an individual.
	shared_ptr<IndividualEvaluator<I> > _fitness;

public:
	virtual std::vector<FitnessType> operator()(const std::vector<I>& individuals);

	shared_ptr<IndividualEvaluator<I> > getIndividualEvaluator();
	void setIndividualEvaluator(const shared_ptr<IndividualEvaluator<I> >& individualEvaluator);

	PopulationEvaluator_Basic(const shared_ptr<IndividualEvaluator<I> >& individualEvaluator);

	virtual ~PopulationEvaluator_Basic() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Computes fitness of every individual.
**/

template<class I, bool OpenMP>
std::vector<FitnessType> PopulationEvaluator_Basic<I, OpenMP>::operator()(const std::vector<I>& individuals) {
	unsigned int size = individuals.size();
	OpenMPException<TracedError_Generic> openMPException;

	std::vector<FitnessType> eval;
	eval.resize(size);

	#pragma omp parallel for if(OpenMP)
	for(unsigned int i = 0; i < size; i++) {
		try {
			eval[i] = (*_fitness)(individuals[i]);
		} catch(std::exception& err) {
			openMPException << err;
		} catch(...) {
			openMPException << "Unknown exception.";
		}
	}

	openMPException.rethrow();

	return eval;
}

/**
* Returns the IndividualEvaluator functor.
**/

template<class I, bool OpenMP>
shared_ptr<IndividualEvaluator<I> > PopulationEvaluator_Basic<I, OpenMP>::
getIndividualEvaluator() {
	return _fitness;
}

/**
* Sets the IndividualEvaluator functor.
**/

template<class I, bool OpenMP>
void PopulationEvaluator_Basic<I, OpenMP>::
setIndividualEvaluator(const shared_ptr<IndividualEvaluator<I> >& individualEvaluator) {
	_fitness = individualEvaluator;
}

/**
 * Default constructor.
 * This should be used for purposes of serialization only.
 */

template<class I, bool OpenMP>
PopulationEvaluator_Basic<I, OpenMP>::PopulationEvaluator_Basic():
_fitness() {}

/**
* Constructor.
* @param individualEvaluator A functor that computes fitness of an individual.
**/

template<class I, bool OpenMP>
PopulationEvaluator_Basic<I, OpenMP>::PopulationEvaluator_Basic(
	const shared_ptr<IndividualEvaluator<I> >& individualEvaluator
): _fitness(individualEvaluator) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE_DETAILED(genetor::PopulationEvaluator_Basic, 2, (SYS_TPARAM_CLASS, SYS_TPARAM_BOOL))

#endif // GEN_BASICPOPULATIONEVALUATOR_H_INCLUDED
