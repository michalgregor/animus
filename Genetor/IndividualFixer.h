#ifndef GEN_INDIVIDUALFIXER_H_INCLUDED
#define GEN_INDIVIDUALFIXER_H_INCLUDED

#include "system.h"
#include "ReproductionOperator.h"
#include <Systematic/exception/OpenMPException.h>

namespace genetor {

/**
* @class IndividualFixer
* @author Michal Gregor
*
* The common interface for ReproductionOperators that carry out fixing of single
* individuals so that represent permissible solutions.
*
* @tparam Individual Type of the Individual that the IndividualFixer
* operates on.
**/
template<class Individual>
class IndividualFixer: public ReproductionOperator<Individual> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<ReproductionOperator<Individual> >(*this);
		ar & _useOpenMP;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

private:
	//! Set to true if OpenMP is to be used to parallelise the application
	//! of the operator.
	bool _useOpenMP;

private:
	virtual void operate(
		const typename ReproductionOperator<Individual>::iterator& first,
		const typename ReproductionOperator<Individual>::iterator& last
	);

public:
	//! Returns true if the operator is set to use OpenMP-based parallelization.
	bool getUseOpenMP() {return _useOpenMP;}
	//! Sets whether the operator is to use OpenMP-based parallelization.
	void setUseOpenMP(bool useOpenMP) {_useOpenMP = useOpenMP;}

	//! This method applies in-place fixing operation to a single individual.
	//! Fixing should ensure that the Individual represents a permissible solution.
	virtual void operator()(Individual& individual) const = 0;

	explicit IndividualFixer(bool useOpenMP = true);
	virtual ~IndividualFixer() = 0;
};

/**
 * Implementation of the ReproductionOperator interface.
 */

template<class Individual>
void IndividualFixer<Individual>::operate(
	const typename ReproductionOperator<Individual>::iterator& first,
	const typename ReproductionOperator<Individual>::iterator& last
) {
	systematic::OpenMPException<TracedError_Generic> openMPException;
	typename ReproductionOperator<Individual>::iterator iter;

	#pragma omp parallel for if(_useOpenMP)
	for(iter = first; iter < last; iter++) {
		try {
			operator()(*iter);
		} catch(std::exception& err) {
			openMPException << err;
		} catch(...) {
			openMPException << "Unknown exception.";
		}
	}

	openMPException.rethrow();
}

/**
 * Constructor.
 * @param useOpenMP Set to true if OpenMP is to be used to parallelise
 * the application of the operator.
 */

template<class Individual>
IndividualFixer<Individual>::IndividualFixer(bool useOpenMP):
_useOpenMP(useOpenMP) {}

/**
* Empty body of the pure virtual destructor.
**/

template<class Individual>
IndividualFixer<Individual>::~IndividualFixer() {
	SYS_EXPORT_INSTANCE()
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::IndividualFixer, 1)

#endif // GEN_INDIVIDUALFIXER_H_INCLUDED
