#ifndef GEN_MAXFITNESSSTOPPER_H_INCLUDED
#define GEN_MAXFITNESSSTOPPER_H_INCLUDED

#include "FitnessStopper.h"

namespace genetor {

/**
* @class FitnessStopper_Max
* @author Michal Gregor
* This is a special type of stop criterion based on values of fitness.
* The criterion triggers a stop if a specified level of fitness has been
* achieved.
**/
class GENETOR_API FitnessStopper_Max: public FitnessStopper {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	FitnessStopper_Max();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	/**
	* Target fitness - If the provided vector of fitness values contains
	* a value greater or equal to the target value, a stop is triggered.
	**/
	FitnessType _targetFitness;

public:
	virtual bool toStop(const FitnessContainer& fitness);

	FitnessType getTargetFitness();
	void setTargetFitness(FitnessType value);

	FitnessStopper_Max(FitnessType targetFitness);
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::FitnessStopper_Max)

#endif // GEN_MAXFITNESSSTOPPER_H_INCLUDED
