#ifndef GEN_FITNESSSTOPPER_H_INCLUDED
#define GEN_FITNESSSTOPPER_H_INCLUDED

#include "../FitnessContainer.h"
#include "Stopper.h"

namespace genetor {

/**
* @class FitnessStopper
* @author Michal Gregor
* This is a special type of stop criterion based on values of fitness.
**/
class GENETOR_API FitnessStopper: public Stopper<FitnessContainer> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	/**
	* This is to return true if the evolution is to be stopped, false if not.
	**/
	virtual bool toStop(const FitnessContainer& fitness) = 0;

	virtual ~FitnessStopper() = 0;
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::FitnessStopper)

#endif // GEN_FITNESSSTOPPER_H_INCLUDED
