#include "FitnessStopper_Max.h"

namespace genetor {

/**
* Boost serialization function.
**/

void FitnessStopper_Max::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FitnessStopper>(*this);
	ar & _targetFitness;
}

void FitnessStopper_Max::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FitnessStopper>(*this);
	ar & _targetFitness;
}

/**
* If the provided vector of fitness values contains a value greater or equal to
* the target value, this returns true. If not, returns false.
**/

bool FitnessStopper_Max::
toStop(const FitnessContainer& fitness) {
	if(fitness.getMaxFitness() >= _targetFitness) return true;
	else return false;
}

/**
* Returns the target fitness.
**/

FitnessType FitnessStopper_Max::getTargetFitness() {
	return _targetFitness;
}

/**
* Sets the target fitness.
**/

void FitnessStopper_Max::setTargetFitness(FitnessType value) {
	_targetFitness = value;
}

/**
 * Default constructor.
 * This should be used for purposes of serialization only.
 */

FitnessStopper_Max::FitnessStopper_Max(): _targetFitness(0) {}

/**
* Constructor.
* @param targetFitness The target fitness.
**/

FitnessStopper_Max::FitnessStopper_Max(FitnessType targetFitness):
_targetFitness(targetFitness) {}

} //namespace genetor
