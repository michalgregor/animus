#include "Stopper_Manual.h"

namespace genetor {

/**
* Boost serialization function.
**/

void Stopper_Manual::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Stopper<void> >(*this);
	boost::shared_lock<boost::shared_mutex> lock(_mut);
	ar & _toStop;
}

void Stopper_Manual::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Stopper<void> >(*this);
	boost::shared_lock<boost::shared_mutex> lock(_mut);
	ar & _toStop;
}

/**
 * Makes the Stopper return true the next time toStop() is called (and until
 * reset() is called).
 */

void Stopper_Manual::stop() {
	boost::unique_lock<boost::shared_mutex> lock(_mut);
	_toStop = true;
}

/**
* This is to return true if the evolution is to be stopped, false if not.
**/

bool Stopper_Manual::toStop() {
	boost::shared_lock<boost::shared_mutex> lock(_mut);
	return _toStop;
}

/**
* Resets the stopper to its initial state (e.g. resets the generation counter).
**/

void Stopper_Manual::reset() {
	boost::unique_lock<boost::shared_mutex> lock(_mut);
	_toStop = false;
}

/**
 * Assignment.
 */

Stopper_Manual& Stopper_Manual::operator=(const Stopper_Manual& obj) {
	if(this != &obj) {
		Stopper<void>::operator=(obj);
		boost::unique_lock<boost::shared_mutex> lock1(_mut);
		boost::unique_lock<boost::shared_mutex> lock2(obj._mut);
		_toStop = obj._toStop;
		//we do not copy _mut
	}
	return *this;
}

/**
 * Copy constructor.
 */

Stopper_Manual::Stopper_Manual(const Stopper_Manual& obj):
Stopper<void>(obj), _toStop(false), _mut() {
	boost::shared_lock<boost::shared_mutex> lock(obj._mut);
	/*we do not copy mut*/
}

/**
 * Constructor.
 */

Stopper_Manual::Stopper_Manual(): _toStop(false), _mut() {}

} //namespace genetor
