#include "FitnessStopper_Delta.h"

namespace genetor {

/**
* Boost serialization function.
**/

void FitnessStopper_Delta::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FitnessStopper>(*this);
	ar & _started;
	ar & _previousFitness;
	ar & _numDeltaGenerations;
	ar & _minDeltaGenerations;
	ar & _targetDelta;
	ar & _fitnessMetric;
}

void FitnessStopper_Delta::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<FitnessStopper>(*this);
	ar & _started;
	ar & _previousFitness;
	ar & _numDeltaGenerations;
	ar & _minDeltaGenerations;
	ar & _targetDelta;
	ar & _fitnessMetric;
}

/**
* Returns the target fitness delta.
**/

FitnessType FitnessStopper_Delta::getTargetDelta() {
	return _targetDelta;
}

/**
* Sets the target delta. This resets the number of delta generations.
**/

void FitnessStopper_Delta::setTargetDelta(FitnessType targetDelta) {
	_targetDelta = targetDelta;
}

/**
* Returns the current number of generations for which the criterion is satisfied.
**/

unsigned int FitnessStopper_Delta::getNumDeltaGenerations() {
	return _numDeltaGenerations;
}

/**
* Sets the minimum number of generations for which the criterion has to be
* satisfied.
**/

void FitnessStopper_Delta::
setMinDeltaGenerations(unsigned int minDeltaGenerations) {
	_minDeltaGenerations = minDeltaGenerations;
}

/**
* Returns the minimum number of generations for which the criterion has to be
* satisfied.
**/

unsigned int FitnessStopper_Delta::getMinDeltaGenerations() {
	return _minDeltaGenerations;
}

/**
* If the provided vector of fitness values contains a value greater or equal to
* the target value, this returns true. If not, returns false.
**/

bool FitnessStopper_Delta::toStop(const FitnessContainer& fitness) {
	FitnessType currentFitness;

	switch(_fitnessMetric) {
	case FM_MAX:
		currentFitness = fitness.getMaxFitness();
	break;
	case FM_AVERAGE:
	default:
		currentFitness = fitness.getAvgFitness();
	break;
	}

	if(!_started) {
		_started = true;
		_numDeltaGenerations = 0;
		_previousFitness = currentFitness;
		return false;
	} else {
		if(
			currentFitness > _previousFitness &&
			currentFitness - _previousFitness < _targetDelta
		) {
			_numDeltaGenerations++;
			if(_numDeltaGenerations >= _minDeltaGenerations) {
				_started = false;
				return true;
			}
		} else _numDeltaGenerations = 0;
	}

	_previousFitness = currentFitness;

	return false;
}

/**
* Reset. The counting starts anew. Target delta, fitness metric and min delta
* generations remain unmodified.
**/

void FitnessStopper_Delta::reset() {
	_started = 0;
}

/**
 * Default constructor.
 * This should be used for purposes of serialization only.
 */

FitnessStopper_Delta::FitnessStopper_Delta(): _started(false),
_previousFitness(0), _numDeltaGenerations(0), _minDeltaGenerations(0),
_targetDelta(0), _fitnessMetric(FM_AVERAGE) {}

/**
* Constructor.
* @param targetDelta The target delta fitness.
* @param minDeltaGenerations
* @param use
**/

FitnessStopper_Delta::FitnessStopper_Delta(
	FitnessType targetDelta,
	unsigned int minDeltaGenerations,
	FitnessMetric fitnessMetric
): _started(false), _previousFitness(0), _numDeltaGenerations(0),
_minDeltaGenerations(minDeltaGenerations), _targetDelta(targetDelta),
_fitnessMetric(fitnessMetric) {}

} //namespace genetor
