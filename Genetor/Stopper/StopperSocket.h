#ifndef GEN_STOPPERSOCKET_H_INCLUDED
#define GEN_STOPPERSOCKET_H_INCLUDED

#include "../system.h"
#include <Systematic/utils/FunctorSocketBase.h>
#include "Stopper.h"

namespace genetor {

/**
* @class StopperSocket
* @author Michal Gregor
* The connection point to which multiple Stoppers can be added. If at least one
* of these activates, the stopper socket activates as well. It is guaranteed
* that every stopper is called even if the first one activates (some stoppers
* may rely on being called in every generation).
**/

SYS_DIAG_OFF(effc++)
template<class Data = void>
class StopperSocket: private FunctorSocketBase<Stopper<Data> > {
SYS_DIAG_ON(effc++)
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<FunctorSocketBase<Stopper<Data> > >(*this);
	}

public:
	using FunctorSocketBase<Stopper<Data> >::add;
	using FunctorSocketBase<Stopper<Data> >::erase;
	using FunctorSocketBase<Stopper<Data> >::clear;

	void operator()(const Data& data);
	bool toStop(const Data& data);
	void reset();
};

/**
* Calls operator() of the next connected functor until it reaches the end of the
* list or until one of the functors throws.
**/

template<class Data>
void StopperSocket<Data>::operator()(const Data& data) {
	typename FunctorSocketBase<Stopper<Data> >::iterator iter;
	for(iter = this->begin(); iter != this->end(); iter++) {
		(**iter)(data);
	}
}

/**
* This is to return true if the evolution is to be stopped, false if not.
* True is returned if at least one of the contained stoppers returns true.
**/

template<class Data>
bool StopperSocket<Data>::toStop(const Data& data) {
	typename FunctorSocketBase<Stopper<Data> >::iterator iter;
	for(iter = this->begin(); iter != this->end(); iter++) {
		if((*iter)->toStop(data)) return true;
	}
	return false;
}

/**
* Calls reset() for every connected Stopper.
**/

template<class Data>
void StopperSocket<Data>::reset() {
	typename FunctorSocketBase<Stopper<Data> >::iterator iter;
	for(iter = this->begin(); iter != this->end(); iter++) {
		(*iter)->reset();
	}
}

//------------------------Specialization for void-------------------------------

SYS_DIAG_OFF(effc++)
template<>
class GENETOR_API StopperSocket<void>: private FunctorSocketBase<Stopper<void> > {
SYS_DIAG_ON(effc++)
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<FunctorSocketBase<Stopper<void> > >(*this);
	}

public:
	using FunctorSocketBase<Stopper<void> >::add;
	using FunctorSocketBase<Stopper<void> >::erase;
	using FunctorSocketBase<Stopper<void> >::clear;

	void operator()();
	bool toStop();
	void reset();
};

} //namespace genetor

#endif // GEN_STOPPERSOCKET_H_INCLUDED
