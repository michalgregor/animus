#include "StopperSocket.h"

namespace genetor {

/**
* Calls operator() of the next connected functor until it reaches the end of the
* list or until one of the functors throws.
**/

void StopperSocket<void>::operator()() {
	iterator iter;
	for(iter = begin(); iter != end(); iter++) {
		(**iter)();
	}
}

/**
* This is to return true if the evolution is to be stopped, false if not.
* True is returned if at least one of the contained stoppers returns true.
**/

bool StopperSocket<void>::toStop() {
	iterator iter;
	for(iter = begin(); iter != end(); iter++) {
		if((*iter)->toStop()) return true;
	}
	return false;
}

/**
* Calls reset() for every connected Stopper.
**/

void StopperSocket<void>::reset() {
	iterator iter;
	for(iter = begin(); iter != end(); iter++) {
		(*iter)->reset();
	}
}

} //namespace genetor
