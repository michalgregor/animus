#include "Stopper.h"

namespace genetor {

/**
 * Body of the pure virtual destructor.
 */

Stopper<void>::~Stopper() {
	SYS_EXPORT_INSTANCE()
}

} //namespace genetor
