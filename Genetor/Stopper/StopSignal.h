#ifndef GEN_STOPSIGNAL_H_INCLUDED
#define GEN_STOPSIGNAL_H_INCLUDED

namespace genetor {

/**
* @class StopSignal
* Used so that evolution can be stopped from various components of the system
* without special interfaces.
**/
class GENETOR_API StopSignal {
private:
	friend class boost::serialization::access;

	template<class Archive>
	void serialize(Archive& UNUSED(ar), const unsigned int UNUSED(version)) {}
};

} //namespace genetor

#endif // GEN_STOPSIGNAL_H_INCLUDED
