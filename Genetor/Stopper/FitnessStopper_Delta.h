#ifndef GEN_DELTAFITNESSSTOPPER_H_INCLUDED
#define GEN_DELTAFITNESSSTOPPER_H_INCLUDED

#include "../system.h"
#include "FitnessStopper.h"

namespace genetor {

enum FitnessMetric {
	FM_AVERAGE,
	FM_MAX
};

/**
* @class FitnessStopper_Delta
* @author Michal Gregor
* This is a special type of stop criterion based on values of fitness.
* The criterion triggers a stop if fitness increase per generation is lower than
* the specified targetDelta value for at least minDeltaGenerations generations
* in row. It is possible to configure whether the average fitness or the maximum
* fitness should be used in calculations.
**/
class GENETOR_API FitnessStopper_Delta: public FitnessStopper {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	FitnessStopper_Delta();

private:
	//! Set to 1, once the first fitness value has been recorded.
	bool _started;
	//! Fitness of the previous generation.
	FitnessType _previousFitness;
	//! The number of generations (in a row) satisfied.
	unsigned int _numDeltaGenerations;
	//! The minimum number of satisfied generations required to stop.
	unsigned int _minDeltaGenerations;
	//! The target delta fitness.
	FitnessType _targetDelta;
	//! Specifies whether to use average fitness or maximum fitness metric.
	FitnessMetric _fitnessMetric;

public:
	virtual bool toStop(const FitnessContainer& fitness);
	virtual void reset();

	FitnessType getTargetDelta();
	void setTargetDelta(FitnessType targetDelta);

	unsigned int getNumDeltaGenerations();
	void setMinDeltaGenerations(unsigned int minDeltaGenerations);
	unsigned int getMinDeltaGenerations();

	FitnessStopper_Delta(
		FitnessType targetDelta,
		unsigned int minDeltaGenerations = 3,
		FitnessMetric fitnessMetric = FM_AVERAGE
	);
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::FitnessStopper_Delta)

#endif // GEN_DELTAFITNESSSTOPPER_H_INCLUDED
