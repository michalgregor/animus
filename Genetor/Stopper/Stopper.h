#ifndef GEN_STOPPER_H_INCLUDED
#define GEN_STOPPER_H_INCLUDED

#include "../system.h"
#include "StopSignal.h"

namespace genetor {

/**
* @class Stopper
* @author Michal Gregor
* The base class of stop criterion classes. By overriding the operator()
* various stop criteria may be implemented.
**/

template<class Data = void>
class Stopper {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& UNUSED(ar), const unsigned int UNUSED(version)) {}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	/**
	* Throws StopSignal if toStop() is true. If throwing is not acceptable,
	* the toStop() function may be queried directly.
	**/

	void operator()(const Data& data) {
		if(toStop(data)) throw StopSignal();
	}

	/**
	* This is to return true if the evolution is to be stopped, false if not.
	**/

	virtual bool toStop(const Data& data) = 0;

	/**
	* Resets the stopper to its initial state (e.g. resets the generation counter).
	**/

	virtual void reset() {}

	virtual ~Stopper() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* @class Stopper
* @author Michal Gregor
* The base class of stop criterion classes. By overriding the operator()
* various stop criteria may be implemented.
**/

template<>
class GENETOR_API Stopper<void> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& UNUSED(ar), const unsigned int UNUSED(version)) {}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	/**
	* Throws StopSignal if toStop() is true. If throwing is not acceptable,
	* the toStop() function may be queried directly.
	**/

	void operator()() {
		if(toStop()) throw StopSignal();
	}

	/**
	* This is to return true if the evolution is to be stopped, false if not.
	**/

	virtual bool toStop() = 0;

	/**
	* Resets the stopper to its initial state (e.g. resets the generation counter).
	**/

	virtual void reset() {}

	virtual ~Stopper();
};

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Stopper, 1)

#endif // GEN_STOPPER_H_INCLUDED
