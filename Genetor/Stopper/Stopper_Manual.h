#ifndef STOPPER_MANUAL_H_
#define STOPPER_MANUAL_H_

#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/locks.hpp>

#include "Stopper.h"

namespace genetor {

/**
* @class Stopper_Manual
* @author Michal Gregor
* A Stopper that is triggered manually by calling stop() (usually from another
* thread). It is thread-safe.
**/
class GENETOR_API Stopper_Manual: public Stopper<void> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! This boolean value is returned by toStop(). It is switched to true when
	//! the Stopper is triggered manually. It is false by default and can be
	//! switched back to false using reset().
	bool _toStop;
	//! Boost mutex.
	mutable boost::shared_mutex _mut;

public:
	void stop();

	virtual bool toStop();
	virtual void reset();

	Stopper_Manual& operator=(const Stopper_Manual& obj);
	Stopper_Manual(const Stopper_Manual& obj);

	Stopper_Manual();
	virtual ~Stopper_Manual() {}
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::Stopper_Manual)

#endif /* STOPPER_MANUAL_H_ */
