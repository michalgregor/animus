#include "FitnessStopper.h"

namespace genetor {

/**
* Boost serialization function.
**/

void FitnessStopper::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Stopper<FitnessContainer> >(*this);
}

void FitnessStopper::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Stopper<FitnessContainer> >(*this);
}

/**
 * Empty body of the pure virtual destructor.
 */

FitnessStopper::~FitnessStopper() {}

}//namespace genetor
