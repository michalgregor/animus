#include "FitnessScaling.h"

namespace genetor {

/**
 * Boost serialization.
 */

void FitnessScaling::serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

void FitnessScaling::serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

/**
 * Empty body of the pure virtual destructor.
 */

FitnessScaling::~FitnessScaling() {}

} //namespace genetor
