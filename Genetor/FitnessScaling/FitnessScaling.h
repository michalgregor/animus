#ifndef GEN_FITNESSSCALING_H_INCLUDED
#define GEN_FITNESSSCALING_H_INCLUDED

#include "../system.h"
#include <vector>

namespace genetor {

class GENETOR_API FitnessScaling {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	virtual void operator() (std::vector<FitnessType>& fitness) = 0;
	virtual ~FitnessScaling() = 0;
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::FitnessScaling)

#endif // GEN_FITNESSSCALING_H_INCLUDED
