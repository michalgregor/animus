#ifndef GEN_LINEARFITNESSSCALING_H_INCLUDED
#define GEN_LINEARFITNESSSCALING_H_INCLUDED

#include "FitnessScaling.h"

namespace genetor {

/**
* @class FitnessScaling_Linear
* @author Michal Gregor
* @brief Implements linear fitness scaling.
*
* Linear fitness scaling by coefficient c. Scaling is based on equation:
* f' = f*a + b, where f is the original fitness and f' denotes the scaled fitness.
* Coefficients a and b are determined as follows: a = favg*(c - 1)/(fmax - favg);
* b = favg - a*favg, where fmax and favg are the maximum and average fitness.
* Scaled fitness values may be negative.
**/
class GENETOR_API FitnessScaling_Linear: public FitnessScaling {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//Scaling coefficients.
	FitnessType _c;

public:

	virtual void operator() (std::vector<FitnessType>& fitness);

	explicit FitnessScaling_Linear(FitnessType c = 1.5);
	virtual ~FitnessScaling_Linear() {}
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::FitnessScaling_Linear)

#endif // GEN_LINEARFITNESSSCALING_H_INCLUDED
