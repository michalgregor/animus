#include "FitnessScaling_Window.h"
#include "scaling_routines.h"

namespace genetor {

/**
 * Boost serialization.
 */

void FitnessScaling_Window::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _lbound;
}

void FitnessScaling_Window::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _lbound;
}

/**
 * Applies the scaling. See class description for more information.
 */

void FitnessScaling_Window::operator()(vector<FitnessType>& fitness) {
	MoveUpTo(fitness, _lbound);
}

/**
 * Constructor.
 * @param lbound The lower bound of fitness, that is what the minimum fitness
 * should be after the scaling has been applied.
 */

FitnessScaling_Window::FitnessScaling_Window(FitnessType lbound):
_lbound(lbound) {}

} //namespace genetor
