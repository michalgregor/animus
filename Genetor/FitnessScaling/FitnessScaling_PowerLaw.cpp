#include "FitnessScaling_PowerLaw.h"
#include <cmath>

namespace genetor {

/**
 * Boost serialization.
 */

void FitnessScaling_PowerLaw::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _c;
}

void FitnessScaling_PowerLaw::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _c;
}

/**
 * Applies power law scaling.
 */

void FitnessScaling_PowerLaw::operator() (std::vector<FitnessType>& fitness) {
	unsigned int size(fitness.size());
	if(!size) return;

	for(unsigned int i = 1; i < size; i++) {
		fitness[i] = std::pow(fitness[i] , _c);
	}
}

/**
* Constructor.
* @param c The scaling parameter. See class description for more information.
**/

FitnessScaling_PowerLaw::FitnessScaling_PowerLaw(FitnessType c):
_c(c) {}

} //namespace genetor
