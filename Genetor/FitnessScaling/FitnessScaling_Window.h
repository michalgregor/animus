#ifndef GEN_WINDOWSCALING_H_INCLUDED
#define GEN_WINDOWSCALING_H_INCLUDED

#include "FitnessScaling.h"

namespace genetor {

/**
 * @class FitnessScaling_Window
 * @author Michal Gregor
 * Scales the fitness values by moving them all up so that the minimum fitness
 * value equals a specified value after the scaling has been applied.
 */
class GENETOR_API FitnessScaling_Window: public FitnessScaling {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The lower bound of fitness, that is what the minimum fitness should be
	//! after the scaling has been applied.
	FitnessType _lbound;

public:
	virtual void operator() (std::vector<FitnessType>& fitness);

	//! Returns the lower bound of fitness, that is what the minimum fitness
	//! should be after the scaling has been applied.
	FitnessType getLbound() {return _lbound;}

	//! Sets the lower bound of fitness, that is what the minimum fitness
	//! should be after the scaling has been applied.
	void setLbound(FitnessType lbound) {_lbound = lbound;}

	explicit FitnessScaling_Window(FitnessType lbound = 1);
	virtual ~FitnessScaling_Window() {}
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::FitnessScaling_Window)

#endif // GEN_WINDOWSCALING_H_INCLUDED
