#ifndef GEN_POWERLAWSCALING_H_INCLUDED
#define GEN_POWERLAWSCALING_H_INCLUDED

#include "FitnessScaling.h"

namespace genetor {

/**
 * @class FitnessScaling_PowerLaw
 * @author Michal Gregor
 *
 * Applies power law fitness scaling with the power of c. Such scaling is based
 * on equation: f' = pow(f, c), where f is the original fitness and f' denotes
 * the scaled fitness.
 */
class GENETOR_API FitnessScaling_PowerLaw: public FitnessScaling {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The scaling coefficient c. See class description for more information.
	FitnessType _c;

public:
	virtual void operator() (std::vector<FitnessType>& fitness);

	FitnessScaling_PowerLaw(FitnessType c = 3);
	virtual ~FitnessScaling_PowerLaw() {}
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::FitnessScaling_PowerLaw)

#endif // GEN_POWERLAWSCALING_H_INCLUDED
