#include "FitnessScaling_Linear.h"

namespace genetor {

/**
 * Boost serialization.
 */

void FitnessScaling_Linear::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _c;
}

void FitnessScaling_Linear::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _c;
}

/**
 * Applies linear scaling.
 */

void FitnessScaling_Linear::operator() (std::vector<FitnessType>& fitness) {
	unsigned int size(fitness.size());

	if(!size) return;

	FitnessType favg(fitness[0]/static_cast<FitnessType>(size)), fmax(fitness[0]), fmin(fitness[0]), a, b;

	for(unsigned int i = 1; i < size; i++) {
		favg += fitness[i]/static_cast<FitnessType>(size);
		if(fitness[i] > fmax) fmax = fitness[i];
		if(fitness[i] < fmin) fmin = fitness[i];
	}

	fmin--;

	if(fmin > fmax - fmax/_c - 1) {
		a = favg*(_c - 1)/(fmax - favg);
		b = favg - a*favg;
	} else {
		a = favg/(favg - fmin);
		b = -fmin*a;
	}

	for(unsigned int i = 1; i < size; i++) {
		fitness[i] = a*fitness[i] + b;
	}
}

/**
* Constructor.
* @param c The scaling parameter. See class description for more information.
**/

FitnessScaling_Linear::FitnessScaling_Linear(FitnessType c): _c(c) {}

}
