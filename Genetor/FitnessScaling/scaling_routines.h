#ifndef GEN_SCALING_ROUTINES_H_INCLUDED
#define GEN_SCALING_ROUTINES_H_INCLUDED

#include <vector>
#include <algorithm>

namespace genetor {

using std::vector;

/**
* Transforms add diff to every value in data.
**/
template<class Type>
inline void MoveUpBy(vector<Type>& data, Type diff) {
	unsigned int size(data.size());
	for(unsigned int i = 0; i < size; i++)
		data[i] += diff;
}

/**
* Transforms given values so that the minimum element equals level.
**/
template<class Type>
inline void MoveUpTo(vector<Type>& data, Type level) {
	if(!data.size()) return;
	MoveUpBy(data, level - *(std::min_element(data.begin(), data.end())));
}

/**
* Transforms given values so that the minimum element equals level.
* This version of the function is given the minimum value so that it does
* not have to seek for it.
**/
template<class Type>
inline void MoveUpTo(vector<Type>& data, Type level, Type min) {
	if(!data.size()) return;
	MoveUpBy(data, level - min);
}

} //namespace genetor

#endif // GEN_SCALING_ROUTINES_H_INCLUDED
