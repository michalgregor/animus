#ifndef GEN_AGAMECHANISM_H_INCLUDED
#define GEN_AGAMECHANISM_H_INCLUDED

#include "../system.h"
#include "../GenerationContainer.h"
#include "Reporter.h"

namespace genetor {

/**
* An adaptive mechanism developed by Srinivas, M., Patnaik, L. M. and presented
* in article Adaptive Probabilities of crossover and Mutation in Genetic
* Algorithms (1994).
*
* It is required that the mutation rate is stored in the individual and can
* be set using member function void setPMutation(ProbabilityType mutationRate);
**/

template<class I>
class Adaptor_AGAMechanism: public Reporter<GenerationContainer<I> > {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Reporter<GenerationContainer<I> > >(*this);
		ar & _k1;
		ar & _k2;
		ar & _k3;
		ar & _k4;
	}

private:
	ProbabilityType _k1, _k2, _k3, _k4;

public:
	virtual void operator()(const GenerationContainer<I>& UNUSED(data)) {}
	virtual void operator()(GenerationContainer<I>& data);

	Adaptor_AGAMechanism(ProbabilityType k1, ProbabilityType k2, ProbabilityType k3, ProbabilityType k4);

	virtual ~Adaptor_AGAMechanism() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Applies the AGA adaptive rule.
**/

template<class I>
void Adaptor_AGAMechanism<I>::operator()(GenerationContainer<I>& data) {
	FitnessType avg = data.getAvgFitness(), max = data.getMaxFitness();
	std::vector<FitnessType>& fitness = data.getFitness();
	std::vector<I>& individuals = data.getIndividuals();

	if(fitness.size() != individuals.size()) throw TracedError_InvalidArgument("fitness.size() != individuals.size().");

	for(unsigned int i; i < individuals.size(); i++) {
		ProbabilityType p = (fitness[i] > avg)?(_k1*(max - fitness[i])/(max - avg)):(_k3);
		individuals[i].setPcrossover(p);
		individuals[i].setPMutation((p >= 0.005)?(p):(0.005));
	}
}

template<class I>
Adaptor_AGAMechanism<I>::
Adaptor_AGAMechanism(ProbabilityType k1, ProbabilityType k2, ProbabilityType k3, ProbabilityType k4):
_k1(k1), _k2(k2), _k3(k3), _k4(k4) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Adaptor_AGAMechanism, 1)

#endif // GEN_AGAMECHANISM_H_INCLUDED
