#ifndef GEN_TRAPDETECTOR_H_INCLUDED
#define GEN_TRAPDETECTOR_H_INCLUDED

#include "../GenerationContainer.h"
#include "Reporter.h"

namespace genetor {

/**
* Detects whether the search has become trapped in which case isTrapped()
* evaluates to true. Otherwise it evaluates to false.
**/

template<class I>
class Adaptor_TrapDetector: public Reporter<GenerationContainer<I> > {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Reporter<GenerationContainer<I> > >(*this);
	}

public:

	/**
	* Used to report individuals and their fitness evaluation so that
	* the detector can decide whether the search is trapped or not.
	**/

	virtual void operator()(const GenerationContainer<I>& data) = 0;

	/**
	* Resets the detector into the initial state.
	**/
	virtual void reset() = 0;

	/**
	* Returns true if trapped in a local extreme, false if not.
	**/
	virtual bool isTrapped() = 0;

	virtual ~Adaptor_TrapDetector() {
		SYS_EXPORT_INSTANCE()
	}
};

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Adaptor_TrapDetector, 1)

#endif // GEN_TRAPDETECTOR_H_INCLUDED
