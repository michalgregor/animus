#ifndef GEN_HALLOFFAME_H_INCLUDED
#define GEN_HALLOFFAME_H_INCLUDED

#include <map>
#include <vector>
#include "../GenerationContainer.h"
#include "Reporter.h"

#include <boost/serialization/map.hpp>

namespace genetor {

/**
 * @class Reporter_HallOfFame
 * @author Michal Gregor
 * Stores a predefined number of best individuals and their corresponding
 * fitness values.
 */

template<class Individual>
class Reporter_HallOfFame: public Reporter<GenerationContainer<Individual> > {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Reporter<GenerationContainer<Individual> > >(*this);
		ar & _pickOffTheCrop;
		ar & _capacity;
	}

protected:
	//! Stores the best individuals.
	std::multimap<FitnessType, Individual> _pickOffTheCrop;
	//! The number of best individuals to store.
	unsigned int _capacity;

public:
	virtual void operator()(const GenerationContainer<Individual>& generation);

	std::multimap<FitnessType, Individual>& getTheBest();
	unsigned int getCapacity();
	void setCapacity(unsigned int capacity);

	Reporter_HallOfFame(unsigned int capacity = 10);

	virtual ~Reporter_HallOfFame() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Reporter interface used to report individuals and their corresponding fitness.
**/

template<class Individual>
void Reporter_HallOfFame<Individual>::
operator()(const GenerationContainer<Individual>& generation) {
	// if stats are already computed, check the max value, if it is lower
	// than currently stored minimum and initial filling is over, skip
	// the processing altogether
	if(generation.statsComputed() && _capacity <= _pickOffTheCrop.size() &&
	generation.getMaxFitness() <= _pickOffTheCrop.begin()->first) return;

	const std::vector<Individual>& individuals = generation.getIndividuals();
	const std::vector<FitnessType>& fitness = generation.getFitness();

	unsigned int size(fitness.size());
	if(size != individuals.size()) throw TracedError_InvalidArgument("individuals.size() != fitness.size()");
	typename std::multimap<FitnessType, Individual>::iterator iter;
	unsigned int i(0);

	//initial filling

	if(_capacity > _pickOffTheCrop.size()) {
		unsigned int num(_capacity - _pickOffTheCrop.size());
		num = (num > size)?(size):(num);
		for(; i < num; i++) {
			_pickOffTheCrop.insert(std::pair<FitnessType, Individual>(fitness[i], individuals[i]));
		}
	}

	//in case the capacity is all used

	for(; i < size; i++) {
		iter = _pickOffTheCrop.begin();

		if(fitness[i] > iter->first) {
			_pickOffTheCrop.erase(iter);
			_pickOffTheCrop.insert(std::pair<FitnessType, Individual>(fitness[i], individuals[i]));
		}
	}
}

/**
* Returns the number of best individuals that will be stored.
**/

template<class Individual>
unsigned int Reporter_HallOfFame<Individual>::getCapacity() {
	return _capacity;
}

/**
* Sets the number of best individuals that will be stored.
**/

template<class Individual>
void Reporter_HallOfFame<Individual>::setCapacity(unsigned int capacity) {
	if(!capacity) throw TracedError_InvalidArgument("A non-zero capacity expected.");
	_capacity = capacity;
}

/**
* Returns the best individuals.
**/

template<class Individual>
std::multimap<FitnessType, Individual>& Reporter_HallOfFame<Individual>::getTheBest() {
	return _pickOffTheCrop;
}

/**
* Constructor.
**/

template<class Individual>
Reporter_HallOfFame<Individual>::Reporter_HallOfFame(unsigned int capacity):
	_pickOffTheCrop(), _capacity(capacity)
{
	if(_capacity == 0) throw TracedError_InvalidArgument("A non-zero capacity expected.");
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Reporter_HallOfFame, 1)

#endif // GEN_HALLOFFAME_H_INCLUDED
