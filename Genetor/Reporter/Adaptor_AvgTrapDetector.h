#ifndef GEN_AVGTRAPDETECTOR_H_INCLUDED
#define GEN_AVGTRAPDETECTOR_H_INCLUDED

#include "Adaptor_TrapDetector.h"
#include <Systematic/container/circular_buffer.h>

namespace genetor {

template<class I>
class Adaptor_AvgTrapDetector: public Adaptor_TrapDetector<I> {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Adaptor_TrapDetector<I> >(*this);
		ar & _fitnessThreshold;
		ar & _prevFitness;
		ar & _activation;
	}

private:
	//trigger settings
	FitnessType _fitnessThreshold;
	//! Stores a certain number of previous values of fitness.
	circular_buffer<FitnessType> _prevFitness;
	//trigger values
	bool _activation;

public:
	virtual void operator()(const GenerationContainer<I>& data);

	virtual void reset();
	virtual bool isTrapped();

	Adaptor_AvgTrapDetector(
		unsigned int numGenerations = 5,
		FitnessType fitnessThreshold = 0.1f
	);

	virtual ~Adaptor_AvgTrapDetector() {
		SYS_EXPORT_INSTANCE()
	}
};

template<class I>
void Adaptor_AvgTrapDetector<I>::operator()(const GenerationContainer<I>& data) {
	FitnessType avg = data.getAvgFitness();
	_prevFitness.push_back(avg);

	// if the previous fitness container is not filled, fill it
	// without evaluating anything
	if(_prevFitness.size() < _prevFitness.capacity()) _activation = false;
	else {
		RealType deltaSum = 0;
		for(unsigned int i = 1; i < _prevFitness.size(); i++) {
			deltaSum += _prevFitness[i] - _prevFitness[i-1];
		}

		sysinfo << "trap detector: _deltaSum = " << deltaSum/avg << std::endl;

		if(deltaSum/avg < _fitnessThreshold) {
			_activation = true;
			_prevFitness.clear();
		} else _activation = false;
	}
}

template<class I>
void Adaptor_AvgTrapDetector<I>::reset() {
	_activation = 0;
	_prevFitness.clear();
}

template<class I>
bool Adaptor_AvgTrapDetector<I>::
isTrapped() {
	return _activation;
}

template<class I>
Adaptor_AvgTrapDetector<I>::Adaptor_AvgTrapDetector(
	unsigned int numGenerations,
	FitnessType fitnessThreshold
): _fitnessThreshold(fitnessThreshold), _prevFitness(numGenerations),
_activation(0) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Adaptor_AvgTrapDetector, 1)

#endif // GEN_AVGTRAPDETECTOR_H_INCLUDED
