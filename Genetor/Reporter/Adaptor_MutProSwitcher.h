#ifndef GEN_MUTPROSWITCHER_H_INCLUDED
#define GEN_MUTPROSWITCHER_H_INCLUDED

#include "../GenerationContainer.h"
#include "Reporter.h"

namespace genetor {

/**
* Used to adapt the mutation probability the differences between the values of
* average between generations are summed up. If the sum does not exceed
* _avgFitnessDelta for at least _numGenerations, the mutation probability is set
* to _highMutP until the sum exceeds _avgFitnessDelta again at which point the
* probability is reset back to it initial value _basicMutP.
*
* It is required that the mutation rate is stored in the individual and can
* be set using member function void setPMutation(ProbabilityType mutationRate);
**/

template<class I>
class Adaptor_MutProSwitcher: public Reporter<GenerationContainer<I> > {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Reporter<GenerationContainer<I> > >(*this);
		ar & _basicMutP;
		ar & _highMutP;
		ar & _numGenerations;
		ar & _avgFitnessDelta;
		ar & _generationCounter;
		ar & _lastAvgFitness;
		ar & _lastMaxFitness;
		ar & _deltaSum;
		ar & _activationLimit;
		ar & _feedBackGain;
	}

private:
	//mutation settings
	ProbabilityType _basicMutP;
	ProbabilityType _highMutP;

	//trigger settings
	unsigned int _numGenerations;
	FitnessType _avgFitnessDelta;

	//trigger values
	unsigned int _generationCounter;
	FitnessType _lastAvgFitness;
	FitnessType _lastMaxFitness;
	FitnessType _deltaSum;

	//! Activation limit.
	unsigned int _activationLimit;
	//! Gain of the feedback connection which is part of the delta sum.
	FitnessType _feedBackGain;

private:
	void setPMutation(GenerationContainer<I>& data, ProbabilityType pMutation) const;

public:
	virtual void operator()(const GenerationContainer<I>& UNUSED(data)) {}
	virtual void operator()(GenerationContainer<I>& data);

	Adaptor_MutProSwitcher(
		ProbabilityType basicMutP = 0.15f,
		ProbabilityType highMutP = 0.9f,
		unsigned int numGenerations = 3,
		FitnessType avgFitnessDelta = 0.1f,
		unsigned int activationLimit = 5,
		ProbabilityType feedBackGain = 0.4f
	);

	virtual ~Adaptor_MutProSwitcher() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* This method exhibits unsocial behaviour - it casts away const-ness to modify
* mutation probability.
**/

template<class I>
void Adaptor_MutProSwitcher<I>::
setPMutation(GenerationContainer<I>& data, ProbabilityType pMutation) const {
	std::vector<I>& individuals (data.getIndividuals());
	unsigned int size(individuals.size());
	for(unsigned int i = 0; i < size; i++) {
		individuals[i].setPMutation(pMutation);
	}
}

template<class I>
void Adaptor_MutProSwitcher<I>::operator()(GenerationContainer<I>& data) {
	if(_generationCounter == (unsigned int)-1) {
		_generationCounter = 0;
		_lastAvgFitness = data.getAvgFitness();
		_lastMaxFitness = data.getMaxFitness();

		setPMutation(data, _basicMutP);

		sysinfo << "setter initialized" << std::endl;
	} else {
		FitnessType avg = data.getAvgFitness();

		_deltaSum = _deltaSum * _feedBackGain + (avg - _lastAvgFitness)/avg;
		_lastAvgFitness = avg;

		if(_deltaSum < _avgFitnessDelta && _generationCounter < _numGenerations + _activationLimit) _generationCounter++;
		else {
			if(_generationCounter) _deltaSum = _avgFitnessDelta*5;
			_generationCounter = 0;
		}

		if(data.getMaxFitness() > _lastMaxFitness) {
			if(_generationCounter) _deltaSum = _avgFitnessDelta*3;
			_generationCounter = 0;
		}

		_lastMaxFitness = data.getMaxFitness();

		if(_generationCounter >= _numGenerations) {
			setPMutation(data, _highMutP);
			sysinfo << "setter: mutation probability = " << _highMutP << std::endl;
		} else {
			sysinfo << "setter: mutation probability = " << _basicMutP << std::endl;
			setPMutation(data, _basicMutP);
		}

		sysinfo << "setter: _deltaSum = " << _deltaSum << std::endl;
		sysinfo << "setter: _generationCounter = " << _generationCounter << std::endl;
	}
}

template<class I>
Adaptor_MutProSwitcher<I>::Adaptor_MutProSwitcher(
	ProbabilityType basicMutP,
	ProbabilityType highMutP,
	unsigned int numGenerations,
	FitnessType avgFitnessDelta,
	unsigned int activationLimit,
	FitnessType feedBackGain
): _basicMutP(basicMutP), _highMutP(highMutP), _numGenerations(numGenerations),
_avgFitnessDelta(avgFitnessDelta), _generationCounter((unsigned int) -1),
_lastAvgFitness(0), _lastMaxFitness(0), _deltaSum(0),
_activationLimit(activationLimit), _feedBackGain(feedBackGain) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Adaptor_MutProSwitcher, 1)

#endif // GEN_MUTPROSWITCHER_H_INCLUDED
