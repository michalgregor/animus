#ifndef GEN_COUTFITNESSREPORTER_H_INCLUDED
#define GEN_COUTFITNESSREPORTER_H_INCLUDED

#include "../FitnessContainer.h"
#include "../system.h"
#include "Reporter.h"

#include <ostream>

namespace genetor {

//! The stream into which reports from FitnessReporter_GenOut are written.
extern std::ostream GenOut;

/**
* @class FitnessReporter_GenOut
* @author Michal Gregor
* Writes the reported fitness values into a std::ostream GenOut. This stream is
* redirected to std::cout by default. The fitness values are written with
* the step of n generations, where n is specified from the constructor or
* using setGenerationStep().
**/
class GENETOR_API FitnessReporter_GenOut: public Reporter<FitnessContainer> {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	//! Stores the generation step - if generationStep == c, then the data
	//! will be reported in every c-th generation.
	unsigned int _generationStep;
	//! Stores the number of generation.
	unsigned int _generationCounter;

public:
	virtual void operator()(const FitnessContainer& data);

	unsigned int getGenerationStep() const;
	void setGenerationStep(unsigned int generationStep);

	unsigned int getGeneration() const;
	void resetGeneration();

	FitnessReporter_GenOut(unsigned int generationStep = 5);

	virtual ~FitnessReporter_GenOut() {}
};

} //namespace genetor

SYS_EXPORT_CLASS(genetor::FitnessReporter_GenOut)

#endif // GEN_COUTFITNESSREPORTER_H_INCLUDED
