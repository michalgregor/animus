#include "FitnessReporter_GenOut.h"

namespace genetor {

std::ostream GenOut(std::cout.rdbuf());

/**
 * Boost serialization.
 */

void FitnessReporter_GenOut::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Reporter<FitnessContainer> >(*this);
	ar & _generationStep;
	ar & _generationCounter;
}

void FitnessReporter_GenOut::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Reporter<FitnessContainer> >(*this);
	ar & _generationStep;
	ar & _generationCounter;
}

/**
* This writes a report of the given generation into a predefined stream.
**/

void FitnessReporter_GenOut::operator()(const FitnessContainer& data) {
	if(_generationCounter % _generationStep == 0) {
		GenOut << "Generation " << _generationCounter << ": ";
		GenOut << data.size() << " individuals. ";
		GenOut << "Average fitness = " << data.getAvgFitness() << "; maximum fitness = " << data.getMaxFitness() << "." << std::endl;
		_generationCounter++;
	}
}

/**
* Returns the generation step. If generationStep == c, then the data will be
* reported in every c-th generation.
**/

unsigned int FitnessReporter_GenOut::getGenerationStep() const {
	return _generationStep;
}

/**
* Sets the generation step. If generationStep == c, then the data will be
* reported in every c-th generation.
**/

void FitnessReporter_GenOut::setGenerationStep(unsigned int generationStep) {
	_generationStep = generationStep;
}

/**
* Returns the number of current generation (it is assumed that a report is
* done once per generation).
**/

unsigned int FitnessReporter_GenOut::getGeneration() const {
	return _generationCounter;
}

/**
* Resets the generation counter by setting its value to 0.
**/

void FitnessReporter_GenOut::resetGeneration() {
	_generationCounter = 0;
}

/**
* Constructor.
* @param generationStep The generation step - if generationStep == c, then
* the data will be reported in every c-th generation.
* @param out  The stream into which the reports should be written.
**/

FitnessReporter_GenOut::FitnessReporter_GenOut(unsigned int generationStep):
_generationStep(generationStep), _generationCounter(0) {}

} //namespace genetor
