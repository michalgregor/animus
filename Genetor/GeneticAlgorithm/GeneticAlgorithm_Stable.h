#ifndef GEN_STABLEGA_H_INCLUDED
#define GEN_STABLEGA_H_INCLUDED

#include "GeneticAlgorithm.h"

namespace genetor {

/**
* @class GeneticAlgorithm_Stable
* @author Michal Gregor
* GeneticAlgorithm_Stable is a type of GeneticAlgorithm in which there is in every generation
* the same number of individuals.
**/

template<class I>
class GeneticAlgorithm_Stable: public GeneticAlgorithm<I> {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<GeneticAlgorithm<I> >(*this);
	}

protected:
	//! Default constructor. For use in serialization.
	GeneticAlgorithm_Stable() {}

protected:
	/**
	* Implementation of EvolutionaryAlgorithm<I>::nextGeneration().
	*
	* Implements the process of creating the next generation. Returns true if the
	* next generation has been created and false if the evolution should be stopped,
	* that is - if any of the stop criteria has activated.
	**/

	virtual void nextGeneration() {
		this->produceGeneration(this->_individuals.size());
	}

public:

	/**
	* Constructor.
	* @param initializer Population initializer.
	* @param populationSize Size of the initial population.
	* @param maxGenerations The maximum number of generations to go through.
	* @param elitism How many individuals should be selected elitisticly.
	**/

	GeneticAlgorithm_Stable(
		const shared_ptr<systematic::Generator<I> >& initializer,
		unsigned int populationSize,
		const shared_ptr<SelectionMethod_Fitness<I> >& selectionMethod,
		unsigned int elitism = 1
	): GeneticAlgorithm<I>(initializer, populationSize, selectionMethod, elitism) {}

	virtual ~GeneticAlgorithm_Stable() {
		SYS_EXPORT_INSTANCE()
	}
};

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::GeneticAlgorithm_Stable, 1)

#endif // GEN_STABLEGA_H_INCLUDED
