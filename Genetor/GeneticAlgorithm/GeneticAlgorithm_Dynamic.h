#ifndef GEN_DYNAMICGA_H_INCLUDED
#define GEN_DYNAMICGA_H_INCLUDED

#include "GeneticAlgorithm.h"
#include "../TrendLine/TrendLine.h"

namespace genetor {

/**
* @class GeneticAlgorithm_Dynamic
* @author Michal Gregor
* GeneticAlgorithm_Dynamic is a type of GeneticAlgorithm in which the number of individuals in
* generation is controlled by a specified trend line.
**/

template<class I>
class GeneticAlgorithm_Dynamic: public GeneticAlgorithm<I> {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<GeneticAlgorithm<I> >(*this);
		ar & _trendLine;
		ar & _numGeneration;
	}

protected:
	//! Default constructor. For use in serialization.
	GeneticAlgorithm_Dynamic(): _trendLine(), _numGeneration(0) {}

private:
	//! The trend line that controls the relation of the population size and
	//! the number of generation.
	shared_ptr<TrendLine> _trendLine;
	//! Number of current generation.
	unsigned int _numGeneration;

protected:
	/**
	* Implementation of EvolutionaryAlgorithm<I>::nextGeneration().
	*
	* Implements the process of creating the next generation. Returns true if the
	* next generation has been created and false if the evolution should be stopped,
	* that is - if any of the stop criteria has activated.
	**/

	virtual void nextGeneration() {
		GeneticAlgorithm<I>::produceGeneration((*_trendLine)(_numGeneration + 1));
		_numGeneration++;
	}

public:
	//! Return the current generation number.
	unsigned int getNumGeneration() const {return _numGeneration;}
	//! Resets the current generation number.
	void resetNumGeneration() {_numGeneration = 0;}

	/**
	* Constructor.
	* @param initializer Population initializer.
	* @param trendLine The trend line that controls the relation of
	* the population size and the number of generation.
	* @param maxGenerations The maximum number of generations to go through.
	* @param elitism How many individuals should be selected elitisticly.
	**/

	GeneticAlgorithm_Dynamic(
		const shared_ptr<systematic::Generator<I> >& initializer,
		const shared_ptr<TrendLine>& trendLine,
		const shared_ptr<SelectionMethod_Fitness<I> >& selectionMethod,
		unsigned int elitism = 1
	): GeneticAlgorithm<I>(initializer, (*trendLine)(0),
		selectionMethod, elitism), _trendLine(trendLine), _numGeneration(0) {}

	virtual ~GeneticAlgorithm_Dynamic() {
		SYS_EXPORT_INSTANCE()
	}
};

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::GeneticAlgorithm_Dynamic, 1)

#endif // GEN_DYNAMICGA_H_INCLUDED
