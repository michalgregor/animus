#ifndef GEN_NOEGA_H_INCLUDED
#define GEN_NOEGA_H_INCLUDED

#include "../Reporter/Adaptor_TrapDetector.h"
#include "../FitnessScaling/FitnessScaling_PowerLaw.h"
#include "GeneticAlgorithm.h"

namespace genetor {

template<class I>
class GeneticAlgorithm_Flood: public GeneticAlgorithm<I> {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<GeneticAlgorithm<I> >(*this);
		ar & _trapDetector;
		ar & _numToSurvive;
		ar & _lpFitnessScaling;
		ar & _tmpFitnessScaling;
		ar & _scalingCounter;
	}

protected:
	//! Default constructor. For use in serialization.
	GeneticAlgorithm_Flood(): _trapDetector(), _numToSurvive(),
	_lpFitnessScaling(), _tmpFitnessScaling(), _scalingCounter() {}

private:
	//! The trap detector mechanism; when it activates, the flood mechanism
	//! is applied.
	shared_ptr<Adaptor_TrapDetector<I> > _trapDetector;
	//! Number of individuals that should survive the flood.
	unsigned int _numToSurvive;
	//! The low-pressure fitness scaling.
	shared_ptr<FitnessScaling> _lpFitnessScaling;
	//! A temporary store for the original fitness scaling.
	shared_ptr<FitnessScaling> _tmpFitnessScaling;

	unsigned int _scalingCounter;

protected:

	/**
	* Returns the number of individuals to select.
	**/

	unsigned int num2Select(unsigned int nextGenSize) const {
		if(_trapDetector->isTrapped()) {
			if(this->_elitism > _numToSurvive) throw TracedError_Generic("Inconsistent query: _elitism > _numToSurvive.");
			return (nextGenSize >= _numToSurvive)?(_numToSurvive):(nextGenSize);
		} else {
			if(this->_elitism > nextGenSize) throw TracedError_Generic("Inconsistent query: _elitism > num.");
			return nextGenSize;
		}
	}

	/**
	* Reimplements select to provide for the flood mechanism.
	**/

	std::vector<I> select(unsigned int nextGenSize) {
		std::vector<unsigned int> selection, elite;
		std::vector<I> result; result.reserve(nextGenSize);

		if(this->_elitism) {
			shared_ptr<SelectionPool_Elite<I> > pool(
				this->_selectionMethod->createSelectionPool_Elite(this->_individuals, this->_elitism)
			);
			selection = pool->select(num2Select(nextGenSize) - this->_elitism);
			elite = pool->getElite();
		} else {
			shared_ptr<SelectionPool<I> > pool(
				this->_selectionMethod->createPool(this->_individuals)
			);
			selection = pool->select(num2Select(nextGenSize));
		}

		//report selection
		this->selectionReporter(selection);

		for(unsigned int i = 0; i < elite.size(); i++) {
			result.push_back(this->_individuals.at(elite[i]));
		}

		for(unsigned int i = 0; i < selection.size(); i++) {
			result.push_back(this->_individuals.at(selection[i]));
		}

		return result;
	}

	/**
	* Implementation of EvolutionaryAlgorithm<I>::nextGeneration().
	*
	* Implements the process of creating the next generation. Returns true if the
	* next generation has been created and false if the evolution should be stopped,
	* that is - if any of the stop criteria has activated.
	**/

	virtual void nextGeneration() {
		if(_scalingCounter) {
			if(_scalingCounter <= 2) _scalingCounter++;
			else {
				this->_selectionMethod->setFitnessScale(_tmpFitnessScaling);
				_tmpFitnessScaling.reset();
				std::cout << "normal scaling" << std::endl;
				_scalingCounter = 0;
			}
		}

		unsigned int current_size = this->_individuals.size();
		GeneticAlgorithm<I>::produceGeneration(current_size);

		if(_trapDetector->isTrapped()) {
			sysinfo << "And the flood was forty days upon the earth." << std::endl;
			std::vector<I> tmp((*this->_initializer)(current_size - _numToSurvive));
			this->_individuals.insert(this->_individuals.end(), tmp.begin(), tmp.end());
			_tmpFitnessScaling = this->_selectionMethod->getFitnessScale();
			this->_selectionMethod->setFitnessScale(_lpFitnessScaling);
			_scalingCounter = 1;
			std::cout << "low pressure scaling" << std::endl;
		}
	}

public:

	/**
	* This method is reimplemented to assure that the trap detector is added
	* (and removed when appropriate) as a reporter.
	**/

	virtual void setSelectionMethod(const shared_ptr<SelectionMethod_Fitness<I> >& method) {
		if(this->_selectionMethod) {
			this->_selectionMethod->generationReporter.erase(_trapDetector);
		}
		this->_selectionMethod = method;
		this->_selectionMethod->generationReporter.add(_trapDetector);
	}

	/**
	* Constructor.
	* @param individuals A population of individuals that the algorithm operates on.
	* If a non-NULL reporter is provided, the individuals are reported.
	* @param maxGenerations The maximum number of generations to go through.
	* @param power Power for the power law scaling. The idea is to lower the
	* selection pressure so this should be lower than 1.
	* @param elitism How many individuals should be selected elitisticly.
	**/

	GeneticAlgorithm_Flood(
		const shared_ptr<systematic::Generator<I> >& initializer,
		unsigned int populationSize,
		const shared_ptr<SelectionMethod_Fitness<I> >& selectionMethod,
		const shared_ptr<Adaptor_TrapDetector<I> >& trapDetector,
		unsigned int numToSurvive,
		RealType power = 0.001f,
		unsigned int elitism = 1
	): GeneticAlgorithm<I>(initializer, populationSize,
	selectionMethod, elitism), _trapDetector(trapDetector),
	_numToSurvive(numToSurvive), _lpFitnessScaling(new FitnessScaling_PowerLaw(power)),
	_tmpFitnessScaling(), _scalingCounter(0) {
		//add trap detector to the selection method
		if(this->_selectionMethod) {
			this->_selectionMethod->generationReporter.add(_trapDetector);
		}
	}

	/**
	* Removes the trap reporter from the selection method's reporter socket.
	**/

	virtual ~GeneticAlgorithm_Flood() {
		SYS_EXPORT_INSTANCE()

		if(this->_selectionMethod) {
			this->_selectionMethod->generationReporter.erase(_trapDetector);
		}
	}
};

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::GeneticAlgorithm_Flood, 1)

#endif // GEN_NOEGA_H_INCLUDED
