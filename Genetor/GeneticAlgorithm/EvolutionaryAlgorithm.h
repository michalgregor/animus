#ifndef GEN_EVOLUTIONARYALGORITHM_H_INCLUDED
#define GEN_EVOLUTIONARYALGORITHM_H_INCLUDED

#include "../Reporter/ReporterSocket.h"
#include "../Stopper/StopperSocket.h"
#include "../Stopper/Stopper.h"
#include "../ReproductionOperator.h"
#include <vector>

#include <boost/serialization/list.hpp>

namespace genetor {

/**
* @class EvolutionaryAlgorithm
* @author Michal Gregor
* The class with those features common for evolutionary algorithms - that is
* a population of individuals and iterative approach, where a new generation
* of individuals is generated in every step.
* @tparam I The type of individuals.
**/

template<class I>
class EvolutionaryAlgorithm {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & _reproductionOperators;
		ar & _individuals;
		ar & individualReporter;
		ar & stopper;
		ar & individualStopper;
	}

protected:
	EvolutionaryAlgorithm();

private:
	typedef std::list<shared_ptr<ReproductionOperator<I> > > RpOperatorList;

	//! A list of reproduction operators. These can be applied to a list
	//! of individuals using applyReproductionOperators().
	RpOperatorList _reproductionOperators;

protected:
	//! Stores the individuals.
	std::vector<I> _individuals;

protected:
	/**
	* Called from Step() and Run(). This method actually implements the
	* process of creating the next generation.
	**/

	virtual void nextGeneration() = 0;

	template<class IterType>
	void applyReproductionOperators(IterType first, IterType last);

public:
	//! Socket for reporters of individuals.
	ReporterSocket<std::vector<I> > individualReporter;

	//! Socket for simple stoppers.
	StopperSocket<void> stopper;
	//! Socket for individual-based stoppers.
	StopperSocket<std::vector<I> > individualStopper;

public:
	void addReproductionOperator(const shared_ptr<ReproductionOperator<I> >& reproductionOperator);
	void clearReproductionOperators();

	std::vector<I>& getIndividuals();
	void setIndividuals(const std::vector<I>& individuals);
	unsigned int size();

	void initialize(const std::vector<I>& individuals);
	void initialize(std::vector<I>& individuals);

	void step();
	unsigned int run(unsigned int maxGenerations);

	explicit EvolutionaryAlgorithm(const std::vector<I>& individuals);

	virtual ~EvolutionaryAlgorithm() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
 * Applies every ReproductionOperator that has been added to
 * the EvolutionaryAlgorithm to the individuals in range [first, last).
 * The operators are guaranteed to be applied in the order in which they were
 * added.
 */

template<class I>
template<class IterType>
void EvolutionaryAlgorithm<I>::
applyReproductionOperators(IterType first, IterType last) {
	typename RpOperatorList::iterator iter;
	for(iter = _reproductionOperators.begin(); iter != _reproductionOperators.end(); iter++) {
		(*iter)->operate(first, last);
	}
}

/**
 * Adds a ReproductionOperator to the list of operators that are applied
 * by a class deriving from EvolutionaryAlgorithm when producing the next
 * generation (that is to say when it calls EvolutionaryAlgorithm<I>::
 * applyReproductionOperators()).
 */

template<class I>
void EvolutionaryAlgorithm<I>::
addReproductionOperator(const shared_ptr<ReproductionOperator<I> >& reproductionOperator) {
	_reproductionOperators.push_back(reproductionOperator);
}

/**
 * Erases all reproduction operators.
 */

template<class I>
void EvolutionaryAlgorithm<I>::clearReproductionOperators() {
	_reproductionOperators.clear();
}

/**
* Returns individuals.
**/

template<class I>
std::vector<I>& EvolutionaryAlgorithm<I>::getIndividuals() {
	return _individuals;
}

/**
* Copies individuals from the input vector.
**/

template<class I>
void EvolutionaryAlgorithm<I>::setIndividuals(const std::vector<I>& individuals) {
	_individuals = individuals;
}

/**
* Initializes the algorithm. This sets the individuals, reports the individuals
and resets the generation counter.
**/

template<class I>
void EvolutionaryAlgorithm<I>::initialize(const std::vector<I>& individuals) {
	setIndividuals(individuals);
	individualReporter(_individuals);
}

/**
* Initializes the algorithm. This sets the individuals, reports the individuals
* and resets the generation counter. The individuals are swapped from the input
* vector. The input vector is cleared.
**/

template<class I>
void EvolutionaryAlgorithm<I>::initialize(std::vector<I>& individuals) {
	setIndividuals(individuals);
	individualReporter(_individuals);
}

/**
* Returns size of the population (number of individuals).
**/

template<class I>
unsigned int EvolutionaryAlgorithm<I>::size() {
	return _individuals->size();
}

/**
* Calls on nextGeneration() to produce the next generation of individuals.
* Method step() performs a single generation step. It does not check stoppers.
**/

template<class I>
void EvolutionaryAlgorithm<I>::step() {
	try {
		nextGeneration();
		individualReporter(_individuals);
	} catch(StopSignal&) {}
}

/**
* Runs nextGeneration until a StopSignal is thrown or maxGeneration is reached.
* Resets stopper and individualStopper at the beginning of the run. Returns the
* number of generations actually run.
**/

template<class I>
unsigned int EvolutionaryAlgorithm<I>::run(unsigned int maxGenerations) {
	unsigned int generation = 0;
	stopper.reset();
	individualStopper.reset();
	try {
		while(generation < maxGenerations && !stopper.toStop() && !individualStopper.toStop(_individuals)) {
			nextGeneration();
			generation++;
			individualReporter(_individuals);
		}
	} catch(StopSignal&) {}

	return generation;
}

/**
 * Default constructor. For use in serialization.
 */

template<class I>
EvolutionaryAlgorithm<I>::EvolutionaryAlgorithm(): _individuals(),
individualReporter(), stopper(), individualStopper() {}

/**
* Constructor.
* @param individuals The individuals that the population is to be
* initialized with. If a non-NULL reporter is provided, the individuals are
* reported.
**/

template<class I>
EvolutionaryAlgorithm<I>::EvolutionaryAlgorithm(const std::vector<I>& individuals):
_individuals(individuals), individualReporter(), stopper(), individualStopper() {
	individualReporter(_individuals);
}

/**
* ostream operator overload.
**/

template<class I>
std::ostream& operator<<(std::ostream& os, genetor::EvolutionaryAlgorithm<I>& population) {
	typename std::vector<I>::iterator iter;
	std::vector<I>& individuals = population.GetIs();

	os << "Population {" << std::endl;

	for(iter = individuals.begin(); iter != individuals.end(); iter++) {
		os << "\t" << *iter << std::endl;
	}

	os << "}" << std::endl;

	return os;
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::EvolutionaryAlgorithm, 1)

#endif // GEN_EVOLUTIONARYALGORITHM_H_INCLUDED
