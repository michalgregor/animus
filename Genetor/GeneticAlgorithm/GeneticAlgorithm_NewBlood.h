#ifndef GEN_NEWBLOODGA_H_INCLUDED
#define GEN_NEWBLOODGA_H_INCLUDED

#include "../Reporter/Adaptor_TrapDetector.h"
#include "GeneticAlgorithm.h"

namespace genetor {

template<class I>
class GeneticAlgorithm_NewBlood: public GeneticAlgorithm<I> {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<GeneticAlgorithm<I> >(*this);
		ar & _trapDetector;
		ar & _numToSurvive;
	}

protected:
	//! Default constructor. For use in serialization.
	GeneticAlgorithm_NewBlood(): _trapDetector(), _numToSurvive() {}

private:
	//! The trap detector mechanism; when it activates, the flood mechanism
	//! is applied.
	shared_ptr<Adaptor_TrapDetector<I> > _trapDetector;
	//! Number of individuals that should survive the flood.
	unsigned int _numToSurvive;

protected:

	/**
	* Returns the number of individuals to select.
	**/

	unsigned int num2Select(unsigned int nextGenSize) const {
		if(_trapDetector->isTrapped()) {
			if(this->_elitism > _numToSurvive) throw TracedError_Generic("Inconsistent query: _elitism > _numToSurvive.");
			return (nextGenSize >= _numToSurvive)?(_numToSurvive):(nextGenSize);
		} else {
			if(this->_elitism > nextGenSize) throw TracedError_Generic("Inconsistent query: _elitism > num.");
			return nextGenSize;
		}
	}

	/**
	* Reimplements select to provide for the flood mechanism.
	**/

	std::vector<I> select(unsigned int nextGenSize) {
		std::vector<unsigned int> selection, elite;
		std::vector<I> result; result.reserve(nextGenSize);

		if(this->_elitism) {
			shared_ptr<SelectionPool_Elite<I> > pool(
				this->_selectionMethod->createSelectionPool_Elite(this->_individuals, this->_elitism)
			);
			selection = pool->select(num2Select(nextGenSize) - this->_elitism);
			elite = pool->getElite();
		} else {
			shared_ptr<SelectionPool<I> > pool(
				this->_selectionMethod->createPool(this->_individuals)
			);
			selection = pool->select(num2Select(nextGenSize));
		}

		//report selection
		this->selectionReporter(selection);

		for(unsigned int i = 0; i < elite.size(); i++) {
			result.push_back(this->_individuals.at(elite[i]));
		}

		if(_trapDetector->isTrapped()) {
			unsigned int sz = (nextGenSize >= selection.size()*2 + this->_elitism)?(selection.size()):((nextGenSize - this->_elitism)/2);

			sysinfo << "And the flood was forty days upon the earth." << std::endl;

			//push in selected individuals and assign a mate to each
			for(unsigned int i = 0; i < sz; i++) {
				result.push_back(this->_individuals.at(selection[i]));
				//provide the individual with a newly generated mate
				result.push_back((*this->_initializer)());
			}

			//fill the rest of the generation with newly generated individuals
			for(unsigned int i = result.size(); i < nextGenSize; i++) {
				result.push_back((*this->_initializer)());
			}
		} else {
			for(unsigned int i = 0; i < selection.size(); i++) {
				result.push_back(this->_individuals.at(selection[i]));
			}
		}

		return result;
	}

	/**
	* Implementation of EvolutionaryAlgorithm<I>::nextGeneration().
	*
	* Implements the process of creating the next generation. Returns true if the
	* next generation has been created and false if the evolution should be stopped,
	* that is - if any of the stop criteria has activated.
	**/

	virtual void nextGeneration() {
		GeneticAlgorithm<I>::produceGeneration(this->_individuals.size());
	}

public:

	/**
	* This method is reimplemented to assure that the trap detector is added
	* (and removed when appropriate) as a reporter.
	**/

	virtual void setSelectionMethod(const shared_ptr<SelectionMethod_Fitness<I> >& method) {
		if(this->_selectionMethod) {
			this->_selectionMethod->generationReporter.erase(_trapDetector);
		}
		this->_selectionMethod = method;
		this->_selectionMethod->generationReporter.add(_trapDetector);
	}

	/**
	* Constructor.
	* @param individuals A population of individuals that the algorithm operates on.
	* If a non-NULL reporter is provided, the individuals are reported.
	* @param maxGenerations The maximum number of generations to go through.
	* @param power Power for the power law scaling. The idea is to lower the
	* selection pressure so this should be lower than 1.
	* @param elitism How many individuals should be selected elitisticly.
	**/

	GeneticAlgorithm_NewBlood(
		const shared_ptr<systematic::Generator<I> >& initializer,
		unsigned int populationSize,
		const shared_ptr<SelectionMethod_Fitness<I> >& selectionMethod,
		const shared_ptr<Adaptor_TrapDetector<I> >& trapDetector,
		unsigned int numToSurvive,
		unsigned int elitism = 1
	): GeneticAlgorithm<I>(initializer, populationSize, selectionMethod, elitism),
	_trapDetector(trapDetector), _numToSurvive(numToSurvive) {
		//add trap detector to the selection method
		if(this->_selectionMethod) {
			this->_selectionMethod->generationReporter.add(_trapDetector);
		}
	}

	/**
	* Removes the trap reporter from the selection method's reporter socket.
	**/

	virtual ~GeneticAlgorithm_NewBlood() {
		SYS_EXPORT_INSTANCE()

		if(this->_selectionMethod) {
			this->_selectionMethod->generationReporter.erase(_trapDetector);
		}
	}
};

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::GeneticAlgorithm_NewBlood, 1)

#endif // GEN_NEWBLOODGA_H_INCLUDED
