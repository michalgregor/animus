#ifndef GEN_GENETICALGORITHM_H_INCLUDED
#define GEN_GENETICALGORITHM_H_INCLUDED

#include "EvolutionaryAlgorithm.h"
#include "../SelectionMethod/SelectionMethod_Fitness.h"
#include "../Stopper/FitnessStopper.h"
#include "../Stopper/StopperSocket.h"
#include "../PopulationEvaluator_Basic.h"
#include "../FitnessContainer.h"
#include "../Reporter/ReporterSocket.h"

#include <Systematic/generator/Generator.h>

namespace genetor {

template<class I>
class GeneticAlgorithm: public EvolutionaryAlgorithm<I> {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<EvolutionaryAlgorithm<I> >(*this);
		ar & _selectionMethod;
		ar & _elitism;
		ar & _initializer;
		ar & selectionReporter;
	}

protected:
	GeneticAlgorithm();

protected:
	//! The method used to select Is that will produce the next generation.
	shared_ptr<SelectionMethod_Fitness<I> > _selectionMethod;
	//! The number of best individuals to apply elitism on.
	unsigned int _elitism;
	//! Population initializer.
	shared_ptr<systematic::Generator<I> > _initializer;

public:
	//! Socket used to report which individuals have been selected for reproduction.
	ReporterSocket<std::vector<unsigned int> > selectionReporter;

public:
	unsigned int getElitism();
	void setElitism(unsigned int elitism);

	virtual shared_ptr<SelectionMethod_Fitness<I> > getSelectionMethod();
	virtual void setSelectionMethod(const shared_ptr<SelectionMethod_Fitness<I> >& method);

	void produceGeneration(unsigned int gsize);
	virtual std::vector<I> select(unsigned int num);

	GeneticAlgorithm<I>& operator=(const GeneticAlgorithm<I>& obj);
	GeneticAlgorithm(const GeneticAlgorithm<I>& obj);

	GeneticAlgorithm(
		const shared_ptr<systematic::Generator<I> >& initializer,
		unsigned int populationSize,
		const shared_ptr<SelectionMethod_Fitness<I> >& selectionMethod,
		unsigned int elitism = 1
	);

	virtual ~GeneticAlgorithm() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Returns the number of best individuals that elitism is to be applied on.
**/

template<class I>
unsigned int GeneticAlgorithm<I>::getElitism() {
	return _elitism;
}

/**
* Sets the number of best individuals that elitism is to be applied on.
**/

template<class I>
void GeneticAlgorithm<I>::setElitism(unsigned int elitism) {
	_elitism = elitism;
}

/**
* Returns the selection method.
**/

template<class I>
shared_ptr<SelectionMethod_Fitness<I> > GeneticAlgorithm<I>::
getSelectionMethod() {
	return _selectionMethod;
}

/**
* Sets the selection method.
**/

template<class I>
void GeneticAlgorithm<I>::
setSelectionMethod(const shared_ptr<SelectionMethod_Fitness<I> >& method) {
	_selectionMethod = method;
}

/**
* Provides a default implementation of nextGeneration() that only requires
* the size of the next generation to be specified. Derived classes may
* simply call this method from their implementations, or provide alternative
* implementations.
**/

template<class I>
void GeneticAlgorithm<I>::produceGeneration(unsigned int gsize) {
	std::vector<I> result(select(gsize));
	if(!result.size()) {
		sysdebug << "GeneticAlgorithm<I>::produceGeneration: No individuals selected." << std::endl;
		this->_individuals.clear();
	} else {
		this->applyReproductionOperators(result.begin() + _elitism, result.end());
		this->_individuals.swap(result);
	}
}

/**
* Returns a vector of selected individuals. At the beginning of the vector,
* there are _numElite elite individuals. These are not undergo crossover
* and mutation.
*
* If a fitness target is specified, this function checks for it. When the target
* fitness has been achieved, selection is not performed and an empty vector
* is returned.
**/

template<class I>
std::vector<I> GeneticAlgorithm<I>::select(unsigned int num) {
	if(_elitism > num) throw TracedError_Generic("Inconsistent query: _elitism > num.");

	std::vector<I> result; std::vector<unsigned int> selection, elite;
	result.reserve(num);

	if(_elitism) {
		shared_ptr<SelectionPool_Elite<I> > pool(
			_selectionMethod->createSelectionPool_Elite(this->_individuals, _elitism)
		);
		selection = pool->select(num - _elitism);
		elite = pool->getElite();
	} else {
		shared_ptr<SelectionPool<I> > pool(
			_selectionMethod->createPool(this->_individuals)
		);
		selection = pool->select(num);
	}

	//report selection
	selectionReporter(selection);

	for(unsigned int i = 0; i < elite.size(); i++) {
		result.push_back(this->_individuals.at(elite[i]));
	}

	for(unsigned int i = 0; i < selection.size(); i++) {
		result.push_back(this->_individuals.at(selection[i]));
	}

	return result;
}

/**
 * Assignment.
 */

template<class I>
GeneticAlgorithm<I>&
GeneticAlgorithm<I>::operator=(const GeneticAlgorithm<I>& obj) {
	if(this != &obj) {
		EvolutionaryAlgorithm<I>::operator=(obj);
		_selectionMethod = shared_ptr<SelectionMethod_Fitness<I> >(clone(obj._selectionMethod));
		_elitism = obj._elitism;
		_initializer = shared_ptr<systematic::Generator<I> >(clone(obj._initializer));
		selectionReporter = obj.selectionReporter;
	}
	return *this;
}

/**
 * Copy constructor.
 */

template<class I>
GeneticAlgorithm<I>::GeneticAlgorithm(const GeneticAlgorithm<I>& obj):
EvolutionaryAlgorithm<I>(obj),
_selectionMethod(clone(obj._selectionMethod)),
_elitism(obj._elitism),
_initializer(clone(obj._initializer)),
selectionReporter(obj.selectionReporter) {}

/**
 * Default constructor. For use in serialization.
 */

template<class I>
GeneticAlgorithm<I>::GeneticAlgorithm(): _selectionMethod(), _elitism(),
_initializer(), selectionReporter()  {}

/**
* Constructor.
* @param initializer Population initializer.
* @param populationSize Size of the initial population.
* @param maxGenerations The maximum number of generations to go through.
* @param elitism How many individuals should be selected elitisticly.
**/

template<class I>
GeneticAlgorithm<I>::GeneticAlgorithm(
	const shared_ptr<systematic::Generator<I> >& initializer,
	unsigned int populationSize,
	const shared_ptr<SelectionMethod_Fitness<I> >& selectionMethod,
	unsigned int elitism
): EvolutionaryAlgorithm<I>((*initializer)(populationSize)),
_selectionMethod(selectionMethod), _elitism(elitism), _initializer(initializer),
selectionReporter() {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::GeneticAlgorithm, 1)

#endif // GEN_GENETICALGORITHM_H_INCLUDED
