#include "FitnessContainer.h"

namespace genetor {

/**
* Boost serialization function.
**/

void FitnessContainer::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & _statsComputed;
	ar & _averageFitness;
	ar & _bestIndividual;
	ar & _fitness;
}

void FitnessContainer::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & _statsComputed;
	ar & _averageFitness;
	ar & _bestIndividual;
	ar & _fitness;
}

/**
 * Computes max and average fitness.
 **/

void FitnessContainer::computeStats() const {
	unsigned int fsize = _fitness->size();
	if (fsize) {
		_averageFitness = (*_fitness)[0] / static_cast<FitnessType>(fsize);
		_bestIndividual = 0;
		for (unsigned int i = 1; i < fsize; i++) {
			if ((*_fitness)[i] > (*_fitness)[_bestIndividual]) _bestIndividual =
					i;
			_averageFitness += (*_fitness)[i] / static_cast<FitnessType>(fsize);
		}
	} else {
		_averageFitness = 0;
		_bestIndividual = 0;
	}
	_statsComputed = true;
}

/**
 * Sets the fitness vector. A TracedError_InvalidPointer is throw on passing a NULL pointer.
 **/

void FitnessContainer::setFitness(
		std::vector<FitnessType>* fitness) {
	if (!fitness) throw TracedError_InvalidPointer(
			"NULL pointer to fitness vector.");
	_fitness = fitness;
	_statsComputed = false;
}

/**
 * Returns a reference to the FitnessType vector. Use with caution as this
 * is a non-const access and as such invalidates the computed stats, which means
 * they will have to be recomputed when asked for. If all you need is a const
 * reference, use the const version of this function instead.
 **/

std::vector<FitnessType>& FitnessContainer::getFitness() {
	_statsComputed = false;
	return *_fitness;
}

/**
 * Returns a const reference to the FitnessType vector.
 **/

const std::vector<FitnessType>& FitnessContainer::getFitness() const {
	return *_fitness;
}

/**
 * Returns the greatest fitness evaluation.
 **/

FitnessType FitnessContainer::getMaxFitness() const {
	if (!_statsComputed) computeStats();
	if (!_fitness->size()) return 0;
	else return _fitness->at(_bestIndividual);
}

/**
 * Returns average fitness evaluation.
 **/

FitnessType FitnessContainer::getAvgFitness() const {
	if (!_statsComputed) computeStats();
	return _averageFitness;
}

/**
 * Returns index of the individual with greatest fitness evaluation.
 **/

unsigned int FitnessContainer::getMaxFitnessIndex() const {
	if (!_statsComputed) computeStats();
	return _bestIndividual;
}

/**
 * Operator assign.
 */

FitnessContainer& FitnessContainer::operator=(const FitnessContainer& fc) {
	if (this != &fc) {
		_statsComputed = fc._statsComputed;
		_averageFitness = fc._averageFitness;
		_bestIndividual = fc._bestIndividual;
		_fitness = fc._fitness;
	}
	return *this;
}

/**
 * Copy constructor.
 */

FitnessContainer::FitnessContainer(const FitnessContainer& fc):
_statsComputed(fc._statsComputed), _averageFitness(fc._averageFitness),
_bestIndividual(fc._bestIndividual), _fitness(fc._fitness) {}

/**
 * Default constructor. FOr use in serialization.
 */

FitnessContainer::FitnessContainer(): _statsComputed(false), _averageFitness(0),
_bestIndividual(0), _fitness(NULL) {}

/**
 * Constructor.
 * @param fitness Pointer to the vector of fitness values.
 * If NULL a TracedError_InvalidPointer is thrown.
 **/

FitnessContainer::FitnessContainer(std::vector<FitnessType>* fitness):
_statsComputed(false), _averageFitness(0), _bestIndividual(0),
_fitness(fitness) {
	if (!_fitness) throw TracedError_InvalidPointer("NULL pointer to fitness vector.");
}

} //namespace genetor
