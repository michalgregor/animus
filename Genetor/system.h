#ifndef GEN_SYSTEM_INCLUDED
#define GEN_SYSTEM_INCLUDED

#include <Systematic/configure.h>
#include <Systematic/system.h>
#include <Systematic/math/RealType.h>

namespace genetor {
	using systematic::RealType;

	//this is required so that shared_ptr and intrusive_ptr
	//are resolved unambiguously
	using systematic::shared_ptr;
	using systematic::intrusive_ptr;

	using namespace systematic;

	typedef float FitnessType;
} //namespace genetor

#endif // GEN_SYSTEM_INCLUDED
