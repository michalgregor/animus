#ifndef GEN_SELECTALLMETHOD_H_INCLUDED
#define GEN_SELECTALLMETHOD_H_INCLUDED

#include "SelectionMethod.h"

namespace genetor {

/**
* @class SelectionMethod_SelectAll
* @author Michal Gregor
* A dummy selection method that simply selects individuals that come first
* in the list.
**/

template<class I>
class SelectionMethod_SelectAll: public SelectionMethod<I> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<SelectionMethod<I> >(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	class Pool: public SelectionPool<I> {
	private:
		//! Pointer to the pool of individuals.
		const std::vector<I>& _individuals;
	public:
		virtual std::vector<unsigned int> select(unsigned int num);
		Pool(const std::vector<I>& individuals);
		virtual ~Pool() {}
	};

public:
	/**
	* Creates a pool of individuals with method Select which does the actual
	* selection.
	* @param individuals The individuals to choose from.
	**/

	virtual shared_ptr<SelectionPool<I> > createPool(std::vector<I>& individuals);

	virtual ~SelectionMethod_SelectAll() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Creates a pool of individuals with method Select which does the actual
* selection.
* @param individuals The individuals to choose from.
**/

template<class I>
shared_ptr<SelectionPool<I> >
SelectionMethod_SelectAll<I>::createPool(std::vector<I>& individuals) {
	return shared_ptr<SelectionPool<I> >(new Pool(individuals));
}

template<class I>
SelectionMethod_SelectAll<I>::Pool::Pool(const std::vector<I>& individuals):
	_individuals(individuals) {}

/**
* Select individuals for mating and returns their indexes in a vector.
* @param num The number of Individuals to choose.
**/

template<class I>
std::vector<unsigned int> SelectionMethod_SelectAll<I>::Pool::select(unsigned int num){
	if(!_individuals) throw TracedError_InvalidPointer("Pool of individuals not set.");
	num = (num > _individuals->size())?(_individuals->size()):(num);
	std::vector<unsigned int> sel; sel.reserve(num);
	for(unsigned int i = 0; i < num; i++)
		sel.push_back(i);
	return sel;
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::SelectionMethod_SelectAll, 1)

#endif // GEN_SELECTALLMETHOD_H_INCLUDED
