#ifndef GEN_ELITESELECTIONMETHOD_H_INCLUDED
#define GEN_ELITESELECTIONMETHOD_H_INCLUDED

#include "SelectionMethod.h"
#include "SelectionPool_Elite.h"

namespace genetor {

/**
* @class SelectionMethod_Elite
* @author Michal Gregor
* An interface that provides indexes of the num best individuals. May be used to
* implement elitism (hence the name) or similar mechanisms.
**/

template<class I>
class SelectionMethod_Elite: public SelectionMethod<I> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<SelectionMethod<I> >(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	/**
	* Creates a pool of individuals with method Select which does the actual
	* selection.
	* @param individuals The individuals to choose from.
	* @param num_elite Number of the best individuals to keep track of.
	**/

	virtual shared_ptr<SelectionPool_Elite<I> > createSelectionPool_Elite(
		std::vector<I>& individuals,
		unsigned int numElite
	) = 0;

	/**
	* Virtual destructor.
	**/

	virtual ~SelectionMethod_Elite() {
		SYS_EXPORT_INSTANCE()
	}
};

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::SelectionMethod_Elite, 1)

#endif // GEN_ELITESELECTIONMETHOD_H_INCLUDED
