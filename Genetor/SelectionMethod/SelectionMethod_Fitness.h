#ifndef GEN_FITNESSSELECTIONMETHOD_H_INCLUDED
#define GEN_FITNESSSELECTIONMETHOD_H_INCLUDED

#include "SelectionMethod_Elite.h"

#include "../PopulationEvaluator_Basic.h"
#include "../FitnessScaling/FitnessScaling.h"
#include "../GenerationContainer.h"
#include "../Reporter/ReporterSocket.h"
#include "../Stopper/StopperSocket.h"
#include "../system.h"

namespace genetor {

/**
* @class SelectionMethod_Fitness
* @brief Virtual base class; used to select Individuals from a list of
* Individuals based on fitness functor.
**/

template <class I>
class SelectionMethod_Fitness: public SelectionMethod_Elite<I> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<SelectionMethod_Elite<I> >(*this);
		ar & _fitnessEvaluator;
		ar & _fitnessScale;
		ar & rawFitnessReporter;
		ar & scaledFitnessReporter;
		ar & generationReporter;
		ar & rawFitnessStopper;
		ar & scaledFitnessStopper;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

protected:
	SelectionMethod_Fitness();

private:
	//!The fitness functor used to assess individuals.
	shared_ptr<PopulationEvaluator<I> > _fitnessEvaluator;
	//!The fitness-scaling functor.
	shared_ptr<FitnessScaling> _fitnessScale;

	std::vector<unsigned int> computeElite(const std::vector<FitnessType>& fitness, unsigned int num);

protected:
	std::vector<FitnessType> computeFitness(std::vector<I>& individuals);

	std::vector<FitnessType> computeFitness(
		std::vector<I>& individuals,
		std::vector<unsigned int>& elite,
		unsigned int numElite
	);

public:
	//! Reporter socket used to report raw fitness values.
	ReporterSocket<FitnessContainer> rawFitnessReporter;
	//! Reporter socket used to report scaled fitness values.
	ReporterSocket<FitnessContainer> scaledFitnessReporter;

	//! Reporter socket used to report raw fitness values together with the vector of individuals.
	ReporterSocket<GenerationContainer<I> > generationReporter;

	//! Stop criterion base on raw fitness.
	StopperSocket<FitnessContainer> rawFitnessStopper;
	//! Stop criterion base on scaled fitness.
	StopperSocket<FitnessContainer> scaledFitnessStopper;

public:
	shared_ptr<PopulationEvaluator<I> > getFitnessEvaluator();
	void setFitnessEvaluator(const shared_ptr<PopulationEvaluator<I> >& evaluator);

	shared_ptr<FitnessScaling> getFitnessScale();
	void setFitnessScale(const shared_ptr<FitnessScaling>& fitnessScale);

	SelectionMethod_Fitness<I>& operator=(const SelectionMethod_Fitness<I>& obj);
	SelectionMethod_Fitness(const SelectionMethod_Fitness<I>& obj);

	SelectionMethod_Fitness(
		const shared_ptr<PopulationEvaluator<I> >& fitnessEvaluator,
		const shared_ptr<FitnessScaling>& fitnessScaling = shared_ptr<FitnessScaling>()
	);

	SelectionMethod_Fitness(
		const shared_ptr<IndividualEvaluator<I> >& fitnessFunctor,
		const shared_ptr<FitnessScaling>& fitnessScaling = shared_ptr<FitnessScaling>()
	);

	virtual ~SelectionMethod_Fitness() = 0;
};

/**
* Computes elite individuals.
* @param fitness The fitness values - raw fitness should be used.
* @param num The number of elite individuals to select. If numElite >
* individuals.size() an exception is thrown as such behaviour would stop
* evolution anyway.
**/

template<class I>
std::vector<unsigned int> SelectionMethod_Fitness<I>::
computeElite(const std::vector<FitnessType>& fitness, unsigned int num) {
	unsigned int size(fitness.size()); if(num > size) throw TracedError_InvalidArgument("Number of elite individuals greater than population size.");
	std::vector<unsigned int> elite; elite.reserve(size);
	for(unsigned int i = 0; i < size; i++) elite.push_back(i);
	if(num == size) std::sort(elite.begin(), elite.end(), IndexSortDescending<std::vector<FitnessType> >(fitness));
	else std::partial_sort(elite.begin(), elite.begin() + num, elite.end(), IndexSortDescending<std::vector<FitnessType> >(fitness));
	return std::vector<unsigned int>(elite.begin(), elite.begin() + num);
}

/**
* Computes scaled fitness and reports to fitness reporter and scaled fitness
* reporter.
* @param individuals The vector of individuals to compute the fitness for.
**/

template<class I>
std::vector<FitnessType> SelectionMethod_Fitness<I>::
computeFitness(std::vector<I>& individuals) {
	std::vector<unsigned int> empty;
	return computeFitness(individuals, empty, 0);
}

/**
* Computes scaled fitness and reports to fitness reporter and scaled fitness
* reporter.
* @param individuals The vector of individuals to compute the fitness for.
* @param elite Reference to a vector that is to hold indexes of the elite
* individuals. The vector contents are cleared.
* @param numElite The number of elite individuals to select. If numElite >
* individuals.size() an exception is thrown as such behaviour would stop
* evolution anyway.
**/

template<class I>
std::vector<FitnessType> SelectionMethod_Fitness<I>::computeFitness(
	std::vector<I>& individuals,
	std::vector<unsigned int>& elite,
	unsigned int numElite
) {
	std::vector<FitnessType> fitness((*_fitnessEvaluator)(individuals));
	std::vector<unsigned int> tmp(computeElite(fitness, numElite));
	elite.swap(tmp);

	GenerationContainer<I> cont(&individuals, &fitness);

	rawFitnessReporter(cont);
	generationReporter(cont);

	//check the stopper - we cannot throw yet, scaled fitness has to be reported first
	bool stop = rawFitnessStopper.toStop(cont);

	// scale fitness if scale provided
	if(_fitnessScale)(*_fitnessScale)(fitness);

	// report scaled fitness if reporter provided
	scaledFitnessReporter(cont);

	//check the scaled fitness stopper & throw if appropriate
	if(stop || scaledFitnessStopper.toStop(cont)) throw StopSignal();

	return fitness;
}

/**
* Returns fitness evaluator.
**/

template<class I>
shared_ptr<PopulationEvaluator<I> >
SelectionMethod_Fitness<I>::getFitnessEvaluator() {
	return _fitnessEvaluator;
}

/**
* Sets fitness evaluator.
**/

template<class I>
void SelectionMethod_Fitness<I>::
setFitnessEvaluator(const shared_ptr<PopulationEvaluator<I> >& evaluator) {
	_fitnessEvaluator = evaluator;
}

/**
* Returns fitness scale.
**/

template<class I>
shared_ptr<FitnessScaling>
SelectionMethod_Fitness<I>::getFitnessScale() {
	return _fitnessScale;
}

/**
* Sets fitness scale.
**/

template<class I>
void SelectionMethod_Fitness<I>::
setFitnessScale(const shared_ptr<FitnessScaling>& scale) {
	_fitnessScale = scale;
}

/**
 * Assignment.
 */

template<class I>
SelectionMethod_Fitness<I>& SelectionMethod_Fitness<I>::
operator=(const SelectionMethod_Fitness<I>& obj) {
	if(this = &obj) {
		SelectionMethod_Elite<I>::operator=(obj);
		_fitnessEvaluator = shared_ptr<PopulationEvaluator<I> >(clone(obj._fitnessEvaluator));
		_fitnessScale = shared_ptr<FitnessScaling>(clone(obj._fitnessScale));
		rawFitnessReporter = obj.rawFitnessReporter;
		scaledFitnessReporter = obj.scaledFitnessReporter;
		generationReporter = obj.generationReporter;
		rawFitnessStopper = obj.rawFitnessStopper;
		scaledFitnessStopper = obj.scaledFitnessStopper;
	}
	return *this;
}

/**
 * Copy constructor.
 */

template<class I> SelectionMethod_Fitness<I>::
SelectionMethod_Fitness(const SelectionMethod_Fitness<I>& obj):
SelectionMethod_Elite<I>(obj),
_fitnessEvaluator(clone(obj._fitnessEvaluator)),
_fitnessScale(clone(obj._fitnessScale)),
rawFitnessReporter(obj.rawFitnessReporter),
scaledFitnessReporter(obj.scaledFitnessReporter),
generationReporter(obj.generationReporter),
rawFitnessStopper(obj.rawFitnessStopper),
scaledFitnessStopper(obj.scaledFitnessStopper) {}

/**
 * Default constructor. Should only be used by serialization.
 */

template<class I>
SelectionMethod_Fitness<I>::SelectionMethod_Fitness(): _fitnessEvaluator(),
_fitnessScale(), rawFitnessReporter(), scaledFitnessReporter(),
generationReporter(), rawFitnessStopper(), scaledFitnessStopper() {}

/**
* Constructor.
* @param fitnessEvaluator The element used to calculate fitness on a
* per generation level.
* @param fitnessScaling Functor implementing fitness scaling.
**/

template<class I>
SelectionMethod_Fitness<I>::SelectionMethod_Fitness (
	const shared_ptr<PopulationEvaluator<I> >& fitnessEvaluator,
	const shared_ptr<FitnessScaling>& fitnessScaling
): _fitnessEvaluator(fitnessEvaluator), _fitnessScale(fitnessScaling),
	rawFitnessReporter(), scaledFitnessReporter(), generationReporter(),
	rawFitnessStopper(), scaledFitnessStopper() {}

/**
* Constructor.
* @param fitnessFunctor The element used to calculate fitness on a
* per individual level.
* @param fitnessScaling Functor implementing fitness scaling.
**/

template<class I>
SelectionMethod_Fitness<I>::SelectionMethod_Fitness (
	const shared_ptr<IndividualEvaluator<I> >& fitnessFunctor,
	const shared_ptr<FitnessScaling>& fitnessScaling
): _fitnessEvaluator(new PopulationEvaluator_Basic<I>(fitnessFunctor)),
	_fitnessScale (fitnessScaling), rawFitnessReporter(), scaledFitnessReporter(),
	generationReporter(), rawFitnessStopper(), scaledFitnessStopper() {}

/**
* Body of the pure virtual destructor.
**/

template<class I>
SelectionMethod_Fitness<I>::~SelectionMethod_Fitness() {
	SYS_EXPORT_INSTANCE()
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::SelectionMethod_Fitness, 1)

#endif // GEN_FITNESSSELECTIONMETHOD_H_INCLUDED

