#ifndef GEN_SELECTIONMETHOD_GREEDY_H_INCLUDED
#define GEN_SELECTIONMETHOD_GREEDY_H_INCLUDED

#include "SelectionMethod_Fitness.h"
#include <Systematic/generator/RouletteGenerator.h>
#include <algorithm>
#include <Systematic/utils/IndexSortAscending.h>

namespace genetor {

/**
* @class SelectionMethod_Greedy
* @author Michal Gregor
* @brief Implements SelectionMethod_Greedy as described by John Koza in
* Genetic Programming: On the Programming of Computers by Means of Natural
* Selection.
**/

template<class I>
class SelectionMethod_Greedy: public SelectionMethod_Fitness<I> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<SelectionMethod_Fitness<I> >(*this);
		ar & _percentageC;
		ar & _probability4GI;
	}

	SelectionMethod_Greedy();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
template<class Parent>
class GreedyPool: public Parent {
private:
	//! The underlying roulette generator.
	RouletteGenerator<FitnessType> _generator;

	/** Computes a vector of ranks corresponding to the individuals using the
	* fitness vector.
	* @param percentageC A portion of individuals to be assigned to group I
	* (the favoured group with greater selection probability). This should be
	* from [0, 1].
	* @param probability4GI Probability that the selected individuals is from
	* group I (the favoured group).
	**/

	static std::vector<FitnessType> computeProbabilities(ProbabilityType percentageC, ProbabilityType probability4GI, const std::vector<FitnessType>& fitness) {
		if(percentageC < 0) percentageC = 0;
		unsigned int size(fitness.size()), sizeOfI(percentageC*size);

		if(probability4GI > 1.0f) probability4GI = 1.0f;
		ProbabilityType probability4GII = 1.0f - probability4GI;

		std::vector<unsigned int> sorted; sorted.reserve(size);
		std::vector<FitnessType> fitnessRanking; fitnessRanking.resize(size);
		for(unsigned int i = 0; i < size; i++) sorted.push_back(i);
		std::sort(sorted.begin(), sorted.end(), IndexSortDescending<std::vector<FitnessType> >(fitness));

		//assign probability to individuals from group I
		for(unsigned int i = 0; i < sizeOfI; i++) {
			fitnessRanking.at(sorted[i]) = 2 * probability4GI * fitness.at(sorted[i]);
		}

		//assign probability to individuals from group II
		for(unsigned int i = sizeOfI; i < size; i++) {
			fitnessRanking.at(sorted[i]) = 2 * probability4GII * fitness.at(sorted[i]);
		}

		return fitnessRanking;
	}

public:
	virtual std::vector<unsigned int> select(unsigned int num) {
		return _generator(num);
	}

	/**
	* Constructor.
	* @param percentageC A portion of individuals to be assigned to group I
	* (the favoured group with greater selection probability). This should be
	* from [0, 1].
	* @param probability4GI Probability that the selected individuals is from
	* group I (the favoured group).
	 */

	GreedyPool(
		ProbabilityType percentageC,
		ProbabilityType probability4GI,
		const std::vector<FitnessType>& fitness,
		vector<unsigned int>& elite
	): Parent(elite), _generator(computeProbabilities(percentageC, probability4GI, fitness)) {}

	virtual ~GreedyPool() {}
};

private:
	//! A portion of individuals to be assigned to group I  (the favoured group
	//! with greater selection probability). This should be from [0, 1].
	ProbabilityType _percentageC;
	//! Probability that the selected individuals is from group I (the favoured group).
	ProbabilityType _probability4GI;

public:
	virtual shared_ptr<SelectionPool<I> > createPool(std::vector<I>& individuals);

	virtual shared_ptr<SelectionPool_Elite<I> > createSelectionPool_Elite(
		std::vector<I>& individuals,
		unsigned int numElite
	);

	SelectionMethod_Greedy(
		const shared_ptr<PopulationEvaluator<I> >& fitnessEvaluator,
		ProbabilityType percentageC,
		ProbabilityType probability4GI = 0.8f,
		const shared_ptr<FitnessScaling>& fitnessScaling = shared_ptr<FitnessScaling>()
	);

	SelectionMethod_Greedy(
		const shared_ptr<IndividualEvaluator<I> >& fitnessFunctor,
		ProbabilityType percentageC,
		ProbabilityType probability4GI = 0.8f,
		const shared_ptr<FitnessScaling>& fitnessScaling = shared_ptr<FitnessScaling>()
	);

	virtual ~SelectionMethod_Greedy() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Creates a pool of individuals with method Select which does the actual
* selection.
* @param individuals The individuals to choose from.
**/

template<class I>
shared_ptr<SelectionPool<I> > SelectionMethod_Greedy<I>::
createPool(std::vector<I>& individuals) {
	std::vector<unsigned int> empty;
	//compute fitness
	std::vector<FitnessType> fitness(this->computeFitness(individuals));
	//check that we have received fitness value for every individual
	if(individuals.size() != fitness.size()) throw TracedError_Generic("fitness.size() != individuals.size()");
	return shared_ptr<SelectionPool<I> >(new GreedyPool<CommonSelectionPool<I> >(_percentageC, _probability4GI, fitness, empty));
}

/**
* Creates a pool of individuals with method Select which does the actual
* selection.
* @param individuals The individuals to choose from.
* @param num_elite Number of the best individuals to keep track of.
**/

template<class I>
shared_ptr<SelectionPool_Elite<I> > SelectionMethod_Greedy<I>::
createSelectionPool_Elite(std::vector<I>& individuals, unsigned int numElite) {
	std::vector<unsigned int> elite;
	//compute fitness
	std::vector<FitnessType> fitness(this->computeFitness(individuals, elite, numElite));
	//check that we have received fitness value for every individual
	if(individuals.size() != fitness.size()) throw TracedError_Generic("fitness.size() != individuals.size()");
	return shared_ptr<SelectionPool_Elite<I> >(new GreedyPool<CommonSelectionPool_Elite<I> >(_percentageC, _probability4GI, fitness, elite));
}

/**
 * Default constructor. For use in serialization.
 */

template<class I>
SelectionMethod_Greedy<I>::SelectionMethod_Greedy():
SelectionMethod_Fitness<I>(), _percentageC(), _probability4GI() {}

/**
* Constructor.
* @param percentageC A portion of individuals to be assigned to group I
* (the favoured group with greater selection probability). This should be
* from [0, 1].
* @param probability4GI Probability that the selected individuals is from
* group I (the favoured group).
* @param fitnessEvaluator The element used to calculate fitness on a
* per generation level.
* @param fitnessScaling Functor implementing fitness scaling.
* @param engine Engine of the random generator used to spin the roulette.
**/

template<class I>
SelectionMethod_Greedy<I>::SelectionMethod_Greedy(
	const shared_ptr<PopulationEvaluator<I> >& fitnessEvaluator,
	ProbabilityType percentageC,
	ProbabilityType probability4GI,
	const shared_ptr<FitnessScaling>& fitnessScaling
): SelectionMethod_Fitness<I>(fitnessEvaluator, fitnessScaling),
_percentageC(percentageC), _probability4GI(probability4GI) {}

/**
* Constructor.
* @param percentageC A portion of individuals to be assigned to group I
* (the favoured group with greater selection probability). This should be
* from [0, 1].
* @param probability4GI Probability that the selected individuals is from
* group I (the favoured group).
* @param fitnessFunctor The element used to calculate fitness on a
* per individual level.
* @param fitnessScaling Functor implementing fitness scaling.
* @param engine Engine of the random generator used to spin the roulette.
**/

template<class I>
SelectionMethod_Greedy<I>::SelectionMethod_Greedy(
	const shared_ptr<IndividualEvaluator<I> >& fitnessFunctor,
	ProbabilityType percentageC,
	ProbabilityType probability4GI,
	const shared_ptr<FitnessScaling>& fitnessScaling
): SelectionMethod_Fitness<I>(fitnessFunctor, fitnessScaling),
_percentageC(percentageC), _probability4GI(probability4GI) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::SelectionMethod_Greedy, 1)

#endif // GEN_SELECTIONMETHOD_GREEDY_H_INCLUDED
