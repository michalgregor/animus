#ifndef GEN_SELECTIONPOOL_H_INCLUDED
#define GEN_SELECTIONPOOL_H_INCLUDED

namespace genetor {

/**
* @class SelectionPool
* Selection methods initialize and return an object of this class. The pool
* then carries out the selection itself.
**/

template<class I>
class SelectionPool {
public:
	/**
	* Select individuals for mating and returns their indexes in a vector
	* (don't forget to call Process() first).
	* @param num The number of Individuals to choose.
	**/

	virtual std::vector<unsigned int> select(unsigned int num) = 0;
	virtual ~SelectionPool() {}
};

/**
* An interface with common costructor - to allow for straight-forward
* implementation on higher levels.
**/

template<class I>
class CommonSelectionPool: public SelectionPool<I> {
public:

	/**
	* Don't really care about these parameters. They are unused and this empty
	* constructor is only provided so as to avoid specialization at higher
	* levels - this way SelectionPool_Elite, SelectionPool_Fitness and SelectionPool all have common
	* constructor interface.
	**/

	CommonSelectionPool(
		std::vector<unsigned int>& UNUSED(elite)
	) {}

	/**
	* Empty virtual destructor.
	**/

	virtual ~CommonSelectionPool() {}
};

} //namespace genetor

#endif // GEN_SELECTIONPOOL_H_INCLUDED
