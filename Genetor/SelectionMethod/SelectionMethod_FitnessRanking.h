#ifndef GEN_FITNESSRANKING_H_INCLUDED
#define GEN_FITNESSRANKING_H_INCLUDED

#include "SelectionMethod_Fitness.h"
#include <Systematic/generator/RouletteGenerator.h>
#include <algorithm>
#include <Systematic/utils/IndexSortAscending.h>

namespace genetor {

/**
* @class SelectionMethod_FitnessRanking
* @author Michal Gregor
* @brief SelectionMethod implemented using the fitness ranking principle.
**/
template<class I>
class SelectionMethod_FitnessRanking: public SelectionMethod_Fitness<I> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<SelectionMethod_Fitness<I> >(*this);
	}

	SelectionMethod_FitnessRanking();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:

template<class Parent>
class RankingPool: public Parent {
private:
	//! The underlying roulette generator.
	RouletteGenerator<FitnessType> _generator;

	//! Computes a vector of ranks corresponding to the individuals using the
	//! fitness vector.
	static std::vector<FitnessType> computeRanking(const std::vector<FitnessType>& fitness) {
		unsigned int size(fitness.size());
		std::vector<unsigned int> sorted; sorted.reserve(size);
		std::vector<FitnessType> fitnessRanking; fitnessRanking.resize(size);
		for(unsigned int i = 0; i < size; i++) sorted.push_back(i);
		std::sort(sorted.begin(), sorted.end(), IndexSortAscending<std::vector<FitnessType> >(fitness));

		//assign rank to every individual
		for(unsigned int i = 0; i < size; i++) {
			fitnessRanking.at(sorted[i]) = i + 1;
		}

		return fitnessRanking;
	}

public:
	virtual std::vector<unsigned int> select(unsigned int num) {
		return _generator(num);
	}

	RankingPool(
		const std::vector<FitnessType>& fitness,
		vector<unsigned int>& elite
	): Parent(elite), _generator(computeRanking(fitness)) {}

	virtual ~RankingPool() {}
};

public:
	virtual shared_ptr<SelectionPool<I> > createPool(std::vector<I>& individuals);

	virtual shared_ptr<SelectionPool_Elite<I> > createSelectionPool_Elite(
		std::vector<I>& individuals,
		unsigned int numElite
	);

	explicit SelectionMethod_FitnessRanking(
		const shared_ptr<PopulationEvaluator<I> >& fitnessEvaluator,
		const shared_ptr<FitnessScaling>& fitnessScaling = shared_ptr<FitnessScaling>()
	);

	explicit SelectionMethod_FitnessRanking(
		const shared_ptr<IndividualEvaluator<I> >& fitnessFunctor,
		const shared_ptr<FitnessScaling>& fitnessScaling = shared_ptr<FitnessScaling>()
	);

	virtual ~SelectionMethod_FitnessRanking() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Creates a pool of individuals with method Select which does the actual
* selection.
* @param individuals The individuals to choose from.
**/
template<class I>
shared_ptr<SelectionPool<I> > SelectionMethod_FitnessRanking<I>::
createPool(std::vector<I>& individuals) {
	std::vector<unsigned int> empty;
	//compute fitness
	std::vector<FitnessType> fitness(this->computeFitness(individuals));
	//check that we have received fitness value for every individual
	if(individuals.size() != fitness.size()) throw TracedError_Generic("fitness.size() != individuals.size()");
	return shared_ptr<SelectionPool<I> >(new RankingPool<CommonSelectionPool<I> >(fitness, empty));
}

/**
* Creates a pool of individuals with method Select which does the actual
* selection.
* @param individuals The individuals to choose from.
* @param num_elite Number of the best individuals to keep track of.
**/
template<class I>
shared_ptr<SelectionPool_Elite<I> > SelectionMethod_FitnessRanking<I>::
createSelectionPool_Elite(std::vector<I>& individuals, unsigned int numElite) {
	std::vector<unsigned int> elite;
	//compute fitness
	std::vector<FitnessType> fitness(this->computeFitness(individuals, elite, numElite));
	//check that we have received fitness value for every individual
	if(individuals.size() != fitness.size()) throw TracedError_Generic("fitness.size() != individuals.size()");
	return shared_ptr<SelectionPool_Elite<I> >(new RankingPool<CommonSelectionPool_Elite<I> >(fitness, elite));
}

/**
 * Default constructor. For use in serialization.
 */
template<class I>
SelectionMethod_FitnessRanking<I>::SelectionMethod_FitnessRanking():
SelectionMethod_Fitness<I>() {}

/**
* Constructor.
* @param fitnessEvaluator The element used to calculate fitness on a
* per generation level.
* @param fitnessScaling Functor implementing fitness scaling.
* @param engine Engine of the random generator used to spin the roulette.
**/
template<class I>
SelectionMethod_FitnessRanking<I>::SelectionMethod_FitnessRanking(
	const shared_ptr<PopulationEvaluator<I> >& fitnessEvaluator,
	const shared_ptr<FitnessScaling>& fitnessScaling
): SelectionMethod_Fitness<I>(fitnessEvaluator, fitnessScaling) {}

/**
* Constructor.
* @param fitnessFunctor The element used to calculate fitness on a
* per individual level.
* @param fitnessScaling Functor implementing fitness scaling.
* @param engine Engine of the random generator used to spin the roulette.
**/
template<class I>
SelectionMethod_FitnessRanking<I>::SelectionMethod_FitnessRanking(
	const shared_ptr<IndividualEvaluator<I> >& fitnessFunctor,
	const shared_ptr<FitnessScaling>& fitnessScaling
): SelectionMethod_Fitness<I>(fitnessFunctor, fitnessScaling) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::SelectionMethod_FitnessRanking, 1)

#endif // GEN_FITNESSRANKING_H_INCLUDED
