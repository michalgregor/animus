#ifndef FITNESSPOOL_H_INCLUDED
#define FITNESSPOOL_H_INCLUDED

#include "SelectionPool_Elite.h"

namespace genetor {

/**
* @class SelectionPool_Fitness
* Selection methods initialize and return an object of this class. The pool
* then carries out the selection itself. The Fitness selection pool provides
* an interface which returns the actual values of fitness as returned by the
* fitness function.
**/

template <class I>
class SelectionPool_Fitness: public SelectionPool_Elite<I> {
public:
	/**
	* Returns fitness of the individuals.
	**/

	virtual const std::vector<FitnessType>& getFitness() = 0;

	/**
	* Virtual destructor.
	**/

	virtual ~SelectionPool_Fitness() {}
};

/**
* A simple implementation of a SelectionPool_Fitness.
**/

template <class I>
class CommonSelectionPool_Fitness: public SelectionPool_Fitness<I> {
private:
	//! Indexes of several of the best individuals.
	std::vector<unsigned int> _elite;
	//! Vector storing the fitness values.
	std::vector<FitnessType> _fitness;

public:
	/**
	* Returns the elite individuals. Number of elite individuals to return is
	* specified by selection method.
	**/

	virtual std::vector<unsigned int> getElite() {
		return _elite;
	}

	/**
	* Returns fitness of the individuals.
	**/

	virtual const std::vector<FitnessType>& getFitness() {
		return _fitness;
	}

	/**
	* Constructor.
	* @param fitness Fitness values of individuals.
	* @param elite Number of elite individuals to return from getElite().
	**/

	CommonSelectionPool_Fitness(
		const std::vector<FitnessType>& fitness,
		unsigned int num
	): _elite(computeElite(fitness, num)), _fitness(fitness) {}

	/**
	* Constructor.
	* @param fitness Fitness values of individuals. Fitness values are
	* swapped out of the vector. If that is not acceptable, pass a const
	* reference instead.
	* @param elite Number of elite individuals to return from getElite().
	**/

	CommonSelectionPool_Fitness(
		std::vector<FitnessType>& fitness,
		unsigned int num
	): _elite(computeElite(fitness, num)), _fitness() {
		_fitness.swap(fitness);
	}

	/**
	* Empty body of the virtual destructor.
	**/

	virtual ~CommonSelectionPool_Fitness() {}
};

} //namespace genetor

#endif // FITNESSPOOL_H_INCLUDED
