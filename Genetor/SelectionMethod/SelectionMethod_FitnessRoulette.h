#ifndef GEN_FITNESSROULETTE_H_INCLUDED
#define GEN_FITNESSROULETTE_H_INCLUDED

#include "../system.h"
#include "../FitnessScaling/scaling_routines.h"
#include "SelectionMethod_Fitness.h"
#include <Systematic/generator/RouletteGenerator.h>
#include <algorithm>

namespace genetor {

/**
* @class SelectionMethod_FitnessRoulette
* @author Michal Gregor
* @brief SelectionMethod implemented using the fitness roulette principle.
**/

template<class I>
class SelectionMethod_FitnessRoulette: public SelectionMethod_Fitness<I> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<SelectionMethod_Fitness<I> >(*this);
		ar & _useFitnessScaling_Window;
	}

protected:
	SelectionMethod_FitnessRoulette();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:

	template<class Parent>
	class RoulettePool: public Parent {
	private:
		//! Random generator used to roll the roulette.
		RouletteGenerator<FitnessType> _generator;

	public:
		virtual std::vector<unsigned int> select(unsigned int num) {
			return _generator(num);
		}

		RoulettePool(
			const std::vector<FitnessType>& fitness,
			vector<unsigned int>& elite
		): Parent(elite), _generator(fitness) {}

		virtual ~RoulettePool() {}
	};

private:
	//! If set to true (default), window scaling will be applied to the
	//! fitness values to prevent them from being negative or zero.
	bool _useFitnessScaling_Window;

	void ApplyFitnessScaling_Window(std::vector<FitnessType>& fitness);

public:
	virtual shared_ptr<SelectionPool<I> > createPool(std::vector<I>& individuals);

	virtual shared_ptr<SelectionPool_Elite<I> > createSelectionPool_Elite(
		std::vector<I>& individuals,
		unsigned int numElite
	);

	bool getUseFitnessScaling_Window();
	void setUseFitnessScaling_Window(bool flag);

	explicit SelectionMethod_FitnessRoulette(
		const shared_ptr<PopulationEvaluator<I> >& fitnessEvaluator,
		const shared_ptr<FitnessScaling>& fitnessScaling = shared_ptr<FitnessScaling>()
	);

	explicit SelectionMethod_FitnessRoulette(
		const shared_ptr<IndividualEvaluator<I> >& fitnessFunctor,
		const shared_ptr<FitnessScaling>& fitnessScaling = shared_ptr<FitnessScaling>()
	);

	virtual ~SelectionMethod_FitnessRoulette() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Creates a pool of individuals with method Select which does the actual
* selection.
* @param individuals The individuals to choose from.
**/

template<class I>
shared_ptr<SelectionPool<I> > SelectionMethod_FitnessRoulette<I>::
createPool(std::vector<I>& individuals) {
	vector<unsigned int> empty;
	//compute fitness
	std::vector<FitnessType> fitness(computeFitness(individuals));
	//aplly additional scaling if set to (to prevent negative fitness values)
	if(_useFitnessScaling_Window) ApplyFitnessScaling_Window(fitness);
	//check that we have received fitness value for every individual
	if(individuals.size() != fitness.size()) throw TracedError_Generic("fitness.size() != individuals.size()");
	return shared_ptr<SelectionPool<I> >(new RoulettePool<CommonSelectionPool<I> >(fitness, empty));
}

/**
* Creates a pool of individuals with method Select which does the actual
* selection.
* @param individuals The individuals to choose from.
* @param num_elite Number of the best individuals to keep track of.
**/

template<class I>
shared_ptr<SelectionPool_Elite<I> > SelectionMethod_FitnessRoulette<I>::
createSelectionPool_Elite(std::vector<I>& individuals, unsigned int numElite) {
	std::vector<unsigned int> elite;
	//compute fitness
	std::vector<FitnessType> fitness(computeFitness(individuals, elite, numElite));
	//aplly additional scaling if set to (to prevent negative fitness values)
	if(_useFitnessScaling_Window) ApplyFitnessScaling_Window(fitness);
	//check that we have received fitness value for every individual
	if(individuals.size() != fitness.size()) throw TracedError_Generic("fitness.size() != individuals.size()");
	return shared_ptr<SelectionPool_Elite<I> >(new RoulettePool<CommonSelectionPool_Elite<I> >(fitness, elite));
}

/**
* This method processes the fitness values so that they satisfy requirements of
* the fitness roulette - fitness values need to be positive in order to create
* a correct cumulative fitness vector. This processing is applied right after
* the fitness scaling.
**/

template<class I>
void SelectionMethod_FitnessRoulette<I>::
ApplyFitnessScaling_Window(std::vector<FitnessType>& fitness) {
	FitnessType minimum(*(std::min_element(fitness.begin(), fitness.end())));
	if(minimum <= 0) MoveUpTo<FitnessType>(fitness, 1, minimum);
}

/**
* Returns whether true if the SelectionMethod_FitnessRoulette is set to use window scaling
* where necessary to prevent negative or zero fitness values.
**/

template<class I>
bool SelectionMethod_FitnessRoulette<I>::getUseFitnessScaling_Window() {
	return _useFitnessScaling_Window;
}

/**
* Sets whether the SelectionMethod_FitnessRoulette should use window scaling (true) or not
* (false) where necessary to prevent negative or zero fitness values.
**/

template<class I>
void SelectionMethod_FitnessRoulette<I>::setUseFitnessScaling_Window(bool flag) {
	_useFitnessScaling_Window = flag;
}

/**
 * Default constructor. For use in serialization only.
 */

template<class I>
SelectionMethod_FitnessRoulette<I>::SelectionMethod_FitnessRoulette():
SelectionMethod_Fitness<I>(), _useFitnessScaling_Window(true) {}

/**
* Constructor.
* @param fitnessEvaluator The element used to calculate fitness on a
* per generation level.
* @param fitnessScaling Functor implementing fitness scaling.
* @param engine Engine of the random generator used to spin the roulette.
**/

template<class I>
SelectionMethod_FitnessRoulette<I>::SelectionMethod_FitnessRoulette(
	const shared_ptr<PopulationEvaluator<I> >& fitnessEvaluator,
	const shared_ptr<FitnessScaling>& fitnessScaling
): SelectionMethod_Fitness<I>(fitnessEvaluator, fitnessScaling),
_useFitnessScaling_Window(true) {}

/**
* Constructor.
* @param fitnessFunctor The element used to calculate fitness on a
* per individual level.
* @param fitnessScaling Functor implementing fitness scaling.
* @param engine Engine of the random generator used to spin the roulette.
**/

template<class I>
SelectionMethod_FitnessRoulette<I>::SelectionMethod_FitnessRoulette(
	const shared_ptr<IndividualEvaluator<I> >& fitnessFunctor,
	const shared_ptr<FitnessScaling>& fitnessScaling
): SelectionMethod_Fitness<I>(fitnessFunctor, fitnessScaling),
_useFitnessScaling_Window(true) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::SelectionMethod_FitnessRoulette, 1)

#endif // GEN_FITNESSROULETTE_H_INCLUDED
