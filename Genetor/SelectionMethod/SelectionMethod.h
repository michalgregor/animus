#ifndef GEN_SELECTIONMETHOD_H_INCLUDED
#define GEN_SELECTIONMETHOD_H_INCLUDED

#include "../system.h"
#include "SelectionPool.h"
#include <vector>

namespace genetor {

/**
* @class SelectionMethod
* @brief Virtual base class; used to select Individuals from a list of Individuals.
**/

template<class I>
class SelectionMethod {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& UNUSED(ar), const unsigned int UNUSED(version)) {}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	/**
	* Creates a pool of individuals with method Select which does the actual
	* selection.
	* @param individuals The individuals to choose from.
	**/

	virtual shared_ptr<SelectionPool<I> > createPool(std::vector<I>& individuals) = 0;

	virtual ~SelectionMethod() = 0;
};

/**
* Body of the pure virtual destructor.
**/

template<class I>
SelectionMethod<I>::~SelectionMethod() {
	SYS_EXPORT_INSTANCE()
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::SelectionMethod, 1)

#endif // GEN_SELECTIONMETHOD_H_INCLUDED

