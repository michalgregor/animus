#ifndef GEN_ELITEPOOL_H_INCLUDED
#define GEN_ELITEPOOL_H_INCLUDED

#include "SelectionPool.h"
#include <Systematic/utils/IndexSortDescending.h>

namespace genetor {

/**
* @class SelectionPool_Elite
* Selection methods initialize and return an object of this class. The pool
* then carries out the selection itself. Elitistic pool also has an interface
* which returns the preset number of elite individuals.
**/

template<class I>
class SelectionPool_Elite: public SelectionPool<I> {
public:
	/**
	* Returns the elite individuals. Number of elite individuals to return is
	* specified by selection method.
	**/

	virtual std::vector<unsigned int> getElite() = 0;

	/**
	* Virtual destructor.
	**/

	virtual ~SelectionPool_Elite() {}
};

/**
* A simple implementation of an SelectionPool_Elite.
**/

template<class I>
class CommonSelectionPool_Elite: public SelectionPool_Elite<I> {
private:
	//! Indexes of several of the best individuals.
	std::vector<unsigned int> _elite;

public:

	/**
	* Returns the elite individuals. Number of elite individuals to return is
	* specified by selection method.
	**/

	virtual std::vector<unsigned int> getElite() {
		return _elite;
	}

	/**
	* Constructor.
	* @param fitness Fitness values of individuals.
	* @param elite Indexes of the elite individuals; the contents are swapped
	* away from the vector. The original vector is cleared.
	**/

	CommonSelectionPool_Elite(
		std::vector<unsigned int>& elite
	): _elite() {
		_elite.swap(elite);
	}

	/**
	* Empty body of the virtual destructor.
	**/

	virtual ~CommonSelectionPool_Elite() {}
};

} //namespace genetor

#endif // GEN_ELITEPOOL_H_INCLUDED
