#ifndef GEN_RANDOMMUTATOR_H_INCLUDED
#define GEN_RANDOMMUTATOR_H_INCLUDED

#include <Systematic/generator/Generator.h>
#include "Mutator.h"

namespace genetor {

using systematic::Generator;
using systematic::BoundaryGenerator;

/**
* @class Mutator_Gene_Random
* @brief Carries out mutation of a gene with a given probability.
* @author Michal Gregor
**/

template<class Gene>
class Mutator_Gene_Random: public Mutator<Gene> {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Mutator<Gene> >(*this);
		ar & _alleleGenerator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Generator of alleles used to select a new random value.
	shared_ptr<Generator<Gene> > _alleleGenerator;

public:
	virtual void operator()(Gene& gene);

	Mutator_Gene_Random(
		const shared_ptr<Generator<Gene> >& alleleGenerator,
		ProbabilityType mutationRate = GEN_DEF_MUTATION_RATE,
		bool useOpenMP = true
	);

	virtual ~Mutator_Gene_Random() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* This method applies mutation. The application is in-place. The mutation
* probability is specified through the constructor or through the
* corresponding accessor.
**/

template<class Gene>
void Mutator_Gene_Random<Gene>::operator()(Gene& gene) {
	gene = (*_alleleGenerator)();
}

/**
* Constructor.
* @param alleleGenerator The generator that will be used to produce new random
* gene alleles.
* @param mutationRate The probability that mutation, as implemented by
* operator(), will be applied to any given individual.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class Gene>
Mutator_Gene_Random<Gene>::Mutator_Gene_Random(
	const shared_ptr<Generator<Gene> >& alleleGenerator,
	ProbabilityType mutationRate,
	bool useOpenMP
): Mutator<Gene>(mutationRate, useOpenMP), _alleleGenerator(alleleGenerator) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Mutator_Gene_Random, 1)

#endif // GEN_RANDOMMUTATOR_H_INCLUDED
