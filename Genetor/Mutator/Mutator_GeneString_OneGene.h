#ifndef GEN_ONEGENEVMUTATOR_H_INCLUDED
#define GEN_ONEGENEVMUTATOR_H_INCLUDED

#include <Systematic/generator/Generator.h>
#include <Systematic/generator/BoundaryGenerator.h>
#include "Mutator.h"

namespace genetor {

using systematic::Generator;
using systematic::BoundaryGenerator;

/**
* @class Mutator_GeneString_OneGene
* @brief Carries out mutation of a randomly chosen gene with a given probability.
* @author Michal Gregor
**/

template<class Gene>
class Mutator_GeneString_OneGene: public Mutator<std::vector<Gene> > {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Mutator<std::vector<Gene> > >(*this);
		ar & _generator;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Generator of alleles used to select a new random value.
	shared_ptr<Generator<Gene> > _generator;

public:
	virtual void operator()(std::vector<Gene>& genes);

	Mutator_GeneString_OneGene(
		const shared_ptr<Generator<Gene> >& generator,
		ProbabilityType mutationRate = GEN_DEF_MUTATION_RATE,
		bool useOpenMP = true
	);

	virtual ~Mutator_GeneString_OneGene() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* This method applies mutation. The application is in-place. The mutation
* probability is specified through the constructor or through the
* corresponding accessor.
**/

template<class Gene>
void Mutator_GeneString_OneGene<Gene>::operator()(std::vector<Gene>& genes) {
	BoundaryGenerator<unsigned int> geneSelector(0, genes.size() - 1);
	typename vector<Gene>::iterator iter = genes.begin() + geneSelector();
	*iter = (*_generator)();
}

/**
* Constructor.
* @param mutationRate The probability that mutation, as implemented by
* operator(), will be applied to any given individual.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
* @param generator The generator that will be used to produce new random
* gene alleles.
* @param seed A single gene is randomly selected to undergo mutation. This
* parameter specifies the seed of the corresponding random generator.
**/

template<class Gene>
Mutator_GeneString_OneGene<Gene>::Mutator_GeneString_OneGene(
	const shared_ptr<Generator<Gene> >& generator,
	ProbabilityType mutationRate,
	bool useOpenMP
): Mutator<std::vector<Gene> >(mutationRate, useOpenMP), _generator(generator) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Mutator_GeneString_OneGene, 1)

#endif // GEN_ONEGENEVMUTATOR_H_INCLUDED
