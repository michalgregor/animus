#ifndef GEN_GAUSSIANVMUTATOR_H_INCLUDED
#define GEN_GAUSSIANVMUTATOR_H_INCLUDED

#include <random>
#include <boost/type_traits/is_floating_point.hpp>
#include <boost/mpl/assert.hpp>

#include "Mutator.h"

namespace genetor {

/**
* @class Mutator_GeneString_Gaussian
* Carries out random mutation with a given probability using a normal
* distribution random generator, where mean is the current value of the gene
* and standard deviation is specified.
* @author Michal Gregor
**/

template<class RealType>
class Mutator_GeneString_Gaussian: public Mutator<std::vector<RealType> > {
	BOOST_MPL_ASSERT((boost::is_floating_point<RealType>));

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Sigma parameter (standard deviation) of the normal distribution.
	RealType _sigma;
	//! The probability that mutation will be applied TO ANY GENE of
	//! the provided gene string. This is a per-gene mutation rate!
	ProbabilityType _perGeneMutationRate;

public:
	virtual void operator()(std::vector<RealType>& genes);

	Mutator_GeneString_Gaussian(
		RealType sigma = 3.0f,
		ProbabilityType perGeneMutationRate = GEN_DEF_MUTATION_RATE,
		bool useOpenMP = true
	);

	virtual ~Mutator_GeneString_Gaussian() {}
};

/**
* This method applies mutation. The application is in-place. The mutation
* probability is specified through the constructor or through the
* corresponding accessor.
**/

template<class RealType>
void Mutator_GeneString_Gaussian<RealType>::operator()(std::vector<RealType>& genes) {
	unsigned int size(genes.size());
	for(unsigned int i = 0; i < size; i++) {
		if(this->_randomGenerator() < _perGeneMutationRate) {
			auto engine = NewDefaultEngine();
			genes[i] = std::normal_distribution<RealType>(genes[i] -_sigma/2, _sigma)(*engine);
		}
	}
}

/**
* Constructor.
* @param sigma Sigma parameter (standard deviation) of the normal distribution.
* @param perGeneMutationRate The probability that mutation will be applied TO
* ANY GENE of the provided gene string. This is a per-gene mutation rate!
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class RealType>
Mutator_GeneString_Gaussian<RealType>::Mutator_GeneString_Gaussian(
	RealType sigma,
	ProbabilityType perGeneMutationRate,
	bool useOpenMP
): Mutator<std::vector<RealType> >(1.0f, useOpenMP), _sigma(sigma),
_perGeneMutationRate(perGeneMutationRate) {}

} //namespace genetor

#endif // GEN_GAUSSIANVMUTATOR_H_INCLUDED
