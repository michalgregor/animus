#ifndef GEN_MUTATOR_H_INCLUDED
#define GEN_MUTATOR_H_INCLUDED

#include "../system.h"
#include "../ReproductionOperator.h"
#include <Systematic/generator/BoundaryGenerator.h>
#include <Systematic/exception/OpenMPException.h>

namespace genetor {

#define GEN_DEF_MUTATION_RATE 0.05f

/**
* @class Mutator
* @author Michal Gregor
*
* The common interface for ReproductionOperators that carry out mutation.
* The mutation is defined by implementing operator(). It is applied to every
* individual with the specified probability.
*
* @tparam Individual Type of the Individual that the Mutaror is to operate on.
**/

template<class Individual>
class Mutator: public ReproductionOperator<Individual> {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<ReproductionOperator<Individual> >(*this);
		ar & _mutationRate;
		ar & _useOpenMP;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

private:
	//! The probability that mutation as implemented by operator() will
	//! be applied to any given individual.
	ProbabilityType _mutationRate;
	//! Set to true if OpenMP is to be used to parallelise the application
	//! of the operator.
	bool _useOpenMP;

protected:
	//! Generator used to decide whether to apply mutation or not.
	systematic::BoundaryGenerator<ProbabilityType> _randomGenerator;

private:
	virtual void operate(
		const typename ReproductionOperator<Individual>::iterator& first,
		const typename ReproductionOperator<Individual>::iterator& last
	);

public:
	//! Returns true if the operator is set to use OpenMP-based parallelization.
	bool getUseOpenMP() {return _useOpenMP;}
	//! Sets whether the operator is to use OpenMP-based parallelization.
	void setUseOpenMP(bool useOpenMP) {_useOpenMP = useOpenMP;}

	//! This method applies in-place mutation to a single individual.
	virtual void operator()(Individual& individual) = 0;

	explicit Mutator(ProbabilityType mutationRate = GEN_DEF_MUTATION_RATE, bool useOpenMP = true);
	virtual ~Mutator() = 0;
};

/**
 * Implementation of the ReproductionOperator interface.
 */

template<class Individual>
void Mutator<Individual>::operate(
	const typename ReproductionOperator<Individual>::iterator& first,
	const typename ReproductionOperator<Individual>::iterator& last
) {
	systematic::OpenMPException<TracedError_Generic> openMPException;
	typename ReproductionOperator<Individual>::iterator iter;

	//Some Mutators implement per-gene mutation rates in which case they will
	//set this probability to 1.0. In such case it would be inefficient to
	//ask the generator every time.

	if(_mutationRate < 1.0f) {
		#pragma omp parallel for if(_useOpenMP)
		for(iter = first; iter < last; iter++) {
			try {
				if(_randomGenerator() < _mutationRate) operator()(*iter);
			} catch(std::exception& err) {
				openMPException << err;
			} catch(...) {
				openMPException << "Unknown exception.";
			}
		}
	} else {
		#pragma omp parallel for if(_useOpenMP)
		for(iter = first; iter < last; iter++) {
			try {
				operator()(*iter);
			} catch(std::exception& err) {
				openMPException << err;
			} catch(...) {
				openMPException << "Unknown exception.";
			}
		}
	}

	openMPException.rethrow();
}

/**
 * Constructor.
 * @param mutationRate The probability that mutation, as implemented by
 * operator(), will be applied to any given individual.
 * @param useOpenMP Set to true if OpenMP is to be used to parallelise
 * the application of the operator.
 */

template<class Individual>
Mutator<Individual>::Mutator(ProbabilityType mutationRate, bool useOpenMP):
_mutationRate(mutationRate), _useOpenMP(useOpenMP) ,_randomGenerator(0, 1) {}

/**
* Empty body of the pure virtual destructor.
**/

template<class Individual>
Mutator<Individual>::~Mutator() {
	SYS_EXPORT_INSTANCE()
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Mutator, 1)

#endif // GEN_MUTATOR_H_INCLUDED
