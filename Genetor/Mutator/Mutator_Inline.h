#ifndef MUTATOR_INLINE_H_
#define MUTATOR_INLINE_H_

#include "Mutator.h"

namespace genetor {

/**
* @class Mutator_Inline
* @author Michal Gregor
* Implementation of a Mutator for types that have their own inline definition
* of mutation that resides in Individual::mutate member function.
**/

template<class Individual>
class Mutator_Inline: public Mutator<Individual> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Mutator<Individual> >(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	virtual void operator()(Individual& individual);

	Mutator_Inline(bool useOpenMP = true);

	virtual ~Mutator_Inline() {
		SYS_EXPORT_INSTANCE();
	}
};

/**
 * This method applies in-place mutation to a single individual.
 */

template<class Individual>
void Mutator_Inline<Individual>::operator()(Individual& individual) {
	individual.mutate();
}

/**
* Constructor.
**/

template<class Individual>
Mutator_Inline<Individual>::Mutator_Inline(bool useOpenMP):
Mutator<Individual>(1.0f, useOpenMP) {}

}//namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Mutator_Inline, 1)

#endif /* MUTATOR_INLINE_H_ */
