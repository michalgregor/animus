#ifndef GEN_RANDOMVMUTATOR_H_INCLUDED
#define GEN_RANDOMVMUTATOR_H_INCLUDED

#include <Systematic/generator/Generator.h>
#include "Mutator.h"

namespace genetor {

/**
* @class Mutator_GeneString_Random
* @author Michal Gregor
* Carries out random mutation. For every gene there is a probability
* that it will undergo mutation. The gene in question will be set to a new
* random value.
**/

template<class Gene>
class Mutator_GeneString_Random: public Mutator<std::vector<Gene> > {
private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Mutator<std::vector<Gene> > >(*this);
		ar & _alleleGenerator;
		ar & _perGeneMutationRate;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Generator of alleles used to select a new random value.
	shared_ptr<Generator<Gene> > _alleleGenerator;
	//! The probability that mutation will be applied TO ANY GENE of
	//! the provided gene string. This is a per-gene mutation rate!
	ProbabilityType _perGeneMutationRate;

public:
	//! Returns the probability that mutation will be applied TO ANY GENE of
	//! the provided gene string. This is a per-gene mutation rate!
	ProbabilityType getPerGeneMutationRate() const {
		return _perGeneMutationRate;
	}

	//! Sets the probability that mutation will be applied TO ANY GENE of
	//! the provided gene string. This is a per-gene mutation rate!
	void setPerGeneMutationRate(ProbabilityType perGeneMutationRate) {
		_perGeneMutationRate = perGeneMutationRate;
	}

	virtual void operator()(std::vector<Gene>& genes);

	Mutator_GeneString_Random(
		const shared_ptr<Generator<Gene> >& alleleGenerator,
		ProbabilityType mutationRate = GEN_DEF_MUTATION_RATE,
		bool useOpenMP = true
	);

	virtual ~Mutator_GeneString_Random() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* This method applies mutation. The application is in-place. The mutation
* probability is specified through the constructor or through the
* corresponding accessor.
**/

template<class Gene>
void Mutator_GeneString_Random<Gene>::operator()(std::vector<Gene>& genes) {
	unsigned int size(genes.size());
	for(unsigned int i = 0; i < size; i++) {
		if(this->_randomGenerator() < _perGeneMutationRate) {
			genes[i] = (*_alleleGenerator)();
		}
	}
}

/**
* Constructor.
* @param alleleGenerator The generator that will be used to produce new random
* gene alleles.
* @param perGeneMutationRate The probability that mutation will be applied TO
* ANY GENE of the provided gene string. This is a per-gene mutation rate!
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class Gene>
Mutator_GeneString_Random<Gene>::Mutator_GeneString_Random(
	const shared_ptr<Generator<Gene> >& alleleGenerator,
	ProbabilityType perGeneMutationRate,
	bool useOpenMP
): Mutator<std::vector<Gene> >(1.0f, useOpenMP),
_alleleGenerator(alleleGenerator), _perGeneMutationRate(perGeneMutationRate) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Mutator_GeneString_Random, 1)

#endif // GEN_RANDOMVMUTATOR_H_INCLUDED
