#ifndef GEN_GAUSSIANMUTATOR_H_INCLUDED
#define GEN_GAUSSIANMUTATOR_H_INCLUDED

#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/type_traits/is_floating_point.hpp>
#include <boost/mpl/assert.hpp>

#include "Mutator.h"

namespace genetor {

/**
* @class Mutator_Gene_Gaussian
* Carries out random mutation with a given probability using a normal
* distribution random generator, where mean is the current value of the RealType
* and standard deviation is specified.
* @author Michal Gregor
**/

template<class RealType>
class Mutator_Gene_Gaussian: public Mutator<RealType> {

BOOST_MPL_ASSERT((boost::is_floating_point<RealType>));

private:
	friend class boost::serialization::access;

	/**
	 * Boost serialization.
	 */

	template<class Archive>
    void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Mutator<RealType> >(*this);
		ar & _sigma;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Sigma parameter (standard deviation) of the normal distribution.
	RealType _sigma;

public:
	virtual void operator()(RealType& individual);

	//! Returns sigma of the normal distribution.
	RealType getSigma() const {return _sigma;}
	//! Sets sigma of the normal distribution.
	void setSigma(RealType sigma) {_sigma = sigma;}

	Mutator_Gene_Gaussian(RealType sigma = 3.0f, ProbabilityType mutationRate = GEN_DEF_MUTATION_RATE, bool useOpenMP = true);

	virtual ~Mutator_Gene_Gaussian() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* This method applies mutation on the given individual. The mutation happens
* in-place. The per RealType mutation probability is specified through the
* constructor or through the corresponding accessor.
**/

template<class RealType>
void Mutator_Gene_Gaussian<RealType>::operator()(RealType& individual) {
	auto engine = NewDefaultEngine();
	individual = std::normal_distribution<RealType>(individual -_sigma/2, _sigma)(*engine);
}

/**
* Constructor.
 * @param mutationRate The probability that mutation, as implemented by
 * operator(), will be applied to any given individual.
 * @param useOpenMP Set to true if OpenMP is to be used to parallelise
 * the application of the operator.
**/

template<class RealType>
Mutator_Gene_Gaussian<RealType>::Mutator_Gene_Gaussian(
	RealType sigma,
	ProbabilityType mutationRate,
	bool useOpenMP
): Mutator<RealType>(mutationRate, useOpenMP), _sigma(sigma) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Mutator_Gene_Gaussian, 1)

#endif // GEN_GAUSSIANMUTATOR_H_INCLUDED
