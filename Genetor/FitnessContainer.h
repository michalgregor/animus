#ifndef GEN_FITNESSCONTAINER_H_INCLUDED
#define GEN_FITNESSCONTAINER_H_INCLUDED

#include "system.h"
#include <vector>

namespace genetor {

/**
 * @class FitnessContainer
 * @author Michal Gregor
 * Fitness values container. The actual values are stored in a vector.
 * The purpose of this class is to cache average and maximum values so that
 * they are only recomputed when the values change.
 */
class GENETOR_API FitnessContainer {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	FitnessContainer();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	//! Set to true if stats have been computed for current data. The stats
	//! are invalidated once the contained data is accessed in a non-const way.
	mutable bool _statsComputed;
	//! Stores average fitness.
	mutable FitnessType _averageFitness;
	//! Stores index of the best individual.
	mutable unsigned int _bestIndividual;
	//! Stores actual fitness values. Is not allowed to be NULL.
	std::vector<FitnessType>* _fitness;

protected:
	void computeStats() const;

public:
	void setFitness(std::vector<FitnessType>* fitness);

	std::vector<FitnessType>& getFitness();
	const std::vector<FitnessType>& getFitness() const;

	FitnessType getMaxFitness() const;
	FitnessType getAvgFitness() const;
	unsigned int getMaxFitnessIndex() const;

	//! Returns the number of fitness values stored.
	unsigned int size() const {
		return _fitness->size();
	}

	//! Returns true if the stats (average, maximum, ...) are computed for the
	//! current values.
	bool statsComputed() const {
		return _statsComputed;
	}

	FitnessContainer& operator=(const FitnessContainer& fc);
	FitnessContainer(const FitnessContainer& fc);
	FitnessContainer(std::vector<FitnessType>* fitness);
	virtual ~FitnessContainer() {}
};

} //namespace genetor

SYS_REGISTER_TYPENAME(genetor::FitnessContainer)

#endif // GEN_FITNESSCONTAINER_H_INCLUDED
