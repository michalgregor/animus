#ifndef GEN_GENERATIONREPORTER_H_INCLUDED
#define GEN_GENERATIONREPORTER_H_INCLUDED

#include "FitnessContainer.h"

namespace genetor {

/**
* @class GenerationContainer
* Derives from FitnessContainer and adds means to pass the vector of individuals
* inside it as well.
**/

template<class I>
class GenerationContainer: public FitnessContainer {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<FitnessContainer>(*this);
		ar & _individuals;
	}

protected:
	GenerationContainer();

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	//! Stores the vector of individuals. Is not allowed to be NULL.
	std::vector<I>* _individuals;

public:
	const std::vector<I>& getIndividuals() const;
	std::vector<I>& getIndividuals();

	void setIndividuals(std::vector<I>* individuals);

	const I& getBestIndividual() const;

	GenerationContainer& operator=(const GenerationContainer& gc);
	GenerationContainer(const GenerationContainer& gc);
	GenerationContainer(std::vector<I>* individuals, std::vector<FitnessType>* fitness);

	virtual ~GenerationContainer() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Returns a reference to the vector of individuals.
**/

template<class I>
const std::vector<I>& GenerationContainer<I>::getIndividuals() const {
	return *_individuals;
}

/**
* Returns a reference to the vector of individuals.
**/

template<class I>
std::vector<I>& GenerationContainer<I>::getIndividuals() {
	return *_individuals;
}

/**
* Sets the vector of individuals. A TracedError_InvalidPointer is throw
* on passing a NULL pointer.
**/

template<class I>
void GenerationContainer<I>::setIndividuals(std::vector<I>* individuals) {
	if(!individuals) throw TracedError_InvalidPointer("NULL pointer to individuals vector.");
	_individuals = individuals;
}

/**
* Returns a reference to the best individual.
**/

template<class I>
const I& GenerationContainer<I>::getBestIndividual() const {
	if(!this->_statsComputed) this->computeStats();
	return _individuals->at(this->_bestIndividual);
}

/**
 * Assignment.
 */

template<class I>
GenerationContainer<I>& GenerationContainer<I>::
operator=(const GenerationContainer<I>& gc) {
	if(this != &gc) {
		FitnessContainer::operator=(gc);
		_individuals = gc._individuals;
	}
	return *this;
}

/**
 * Copy constructor.
 */

template<class I>
GenerationContainer<I>::
GenerationContainer(const GenerationContainer<I>& gc):
FitnessContainer(gc), _individuals(gc._individuals) {}

/**
 * Default constructor. For use in serialization.
 */

template<class I>
GenerationContainer<I>::GenerationContainer(): _individuals(NULL) {}

/**
* Constructor.
* @param individuals Pointer to the vector of individuals. A TracedError_InvalidPointer is throw
* on passing a NULL pointer.
* @param fitness Pointer to the vector of fitness values. If NULL a TracedError_InvalidPointer is
* thrown.
**/

template<class I>
GenerationContainer<I>::
GenerationContainer(std::vector<I>* individuals, std::vector<FitnessType>* fitness):
FitnessContainer(fitness), _individuals(individuals) {
	if(!_individuals) throw TracedError_InvalidPointer("NULL pointer to individuals vector.");
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::GenerationContainer, 1)

#endif // GEN_GENERATIONREPORTER_H_INCLUDED
