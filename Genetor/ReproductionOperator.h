#ifndef REPRODUCTIONOPERATOR_H_
#define REPRODUCTIONOPERATOR_H_

#include "system.h"
#include <vector>

namespace genetor {

/**
* @class ReproductionOperator
* @author Michal Gregor
*
* This class provides a common interface for genetic operators.
* A GeneticOperator takes in all Individuals selected for reproduction and
* operates on them.
*
* Most conventionally, a ReproductionOperator is applied to every single
* individual separately (in which case the operation is typically called
* mutation), or to pairs of individuals (in which case it is typically called
* crossover).
*
* However, the interface can also be used by operators that will fix
* the individual (after mutation and crossover have been applied) in order to
* ensure that all individuals in fact represent permissible solutions.
*
* ANY DERIVED CLASS IS REQUIRED TO BE THREAD SAFE AND ABLE TO OPERATE PARALLELY!
*
* @tparam Individual Type of individual on which the GeneticOperator can be
* applied.
**/

template<class Individual>
class ReproductionOperator {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& UNUSED(ar), const unsigned int UNUSED(version)) {}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	typedef Individual individual_type;
	typedef typename std::vector<Individual>::iterator iterator;

public:

	/**
	 * Operates on individuals in the range of [first, last).
	 */

	virtual void operate(
		const iterator& first,
		const iterator& last
	) = 0;

	virtual ~ReproductionOperator() = 0;
};

/**
 * Body of the pure virtual destructor.
 */

template<class Individual>
ReproductionOperator<Individual>::~ReproductionOperator()  {
	SYS_EXPORT_INSTANCE();
}

}//namespace genetor

SYS_EXPORT_TEMPLATE(genetor::ReproductionOperator, 1)

#endif /* REPRODUCTIONOPERATOR_H_ */
