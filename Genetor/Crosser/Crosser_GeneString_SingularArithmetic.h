#ifndef GEN_Crosser_GeneString_SingularArithmetic_H_INCLUDED
#define GEN_Crosser_GeneString_SingularArithmetic_H_INCLUDED

#include "Crosser.h"
#include <Systematic/generator/BoundaryGenerator.h>
#include <vector>

namespace genetor {

/**
* @class Crosser_GeneString_SingularArithmetic
* @author Michal Gregor
* Applies arithmetic crossover (new_gene_a = alpha*old_gene_a + (1-alpha)*old_gene_b)
* to a single randomly selected gene.
**/

template<class Gene>
class Crosser_GeneString_SingularArithmetic: public Crosser<std::vector<Gene> > {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Crosser<std::vector<Gene> > >(*this);
		ar & _alpha;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The arithmetic crossover coefficient.
	RealType _alpha;

protected:
	virtual void operator()(std::vector<Gene>& i1, std::vector<Gene>& i2);

public:
	explicit Crosser_GeneString_SingularArithmetic(
		RealType alpha = 0.5f,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~Crosser_GeneString_SingularArithmetic() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* The crossing operation itself. The crossover happens in-place.
* @param i1 The first partner individual.
* @param i2 The second partner individual.
**/

template<class Gene>
void Crosser_GeneString_SingularArithmetic<Gene>::operator()(std::vector<Gene>& i1, std::vector<Gene>& i2) {
	unsigned int size = i1.size() - 1;
	if(size != i2.size() - 1) throw TracedError_InvalidArgument("Chromosomes are not equally long.");

	BoundaryGenerator<unsigned int> generator(0, size);
	unsigned int pos = generator();

	Gene x(i1[pos]);
	i1[pos] = _alpha*i2[pos] + (1 - _alpha)*x;
	i2[pos] = _alpha*x + (1 - _alpha)*i2[pos];
}

/**
* Constructor.
* @param alpha The alpha value of the arithmetic crossover (see class info).
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class Gene>
Crosser_GeneString_SingularArithmetic<Gene>::Crosser_GeneString_SingularArithmetic(
	RealType alpha,
	ProbabilityType crossoverRate,
	bool useOpenMP
):
	Crosser<std::vector<Gene> >(crossoverRate, useOpenMP), _alpha(alpha) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Crosser_GeneString_SingularArithmetic, 1)

#endif // GEN_Crosser_GeneString_SingularArithmetic_H_INCLUDED
