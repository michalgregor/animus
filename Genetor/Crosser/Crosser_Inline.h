#ifndef CROSSER_INLINE_H_
#define CROSSER_INLINE_H_

#include "Crosser.h"

namespace genetor {

/**
* @class Crosser_Inline
* @author Michal Gregor
* Implementation of a Crosser for types that have their own inline definition
* of crossover that resides in Individual::cross member function.
**/

template<class Individual>
class Crosser_Inline: public Crosser<Individual> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Crosser<Individual> >(*this);
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	virtual void operator()(Individual& i1, Individual& i2);

	explicit Crosser_Inline(
		bool useOpenMP = true
	);

	virtual ~Crosser_Inline() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Applies in-place crossover by calling i1.cross(i2).
* @param i1 The first partner individual.
* @param i2 The second partner individual.
**/

template<class Individual>
void Crosser_Inline<Individual>::operator()(Individual& i1, Individual& i2) {
	i1.cross(i2);
}

/**
* Constuctor.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class Individual>
Crosser_Inline<Individual>::Crosser_Inline(
	bool useOpenMP
): Crosser<Individual>(1.0f, useOpenMP) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Crosser_Inline, 1)

#endif /* CROSSER_INLINE_H_ */
