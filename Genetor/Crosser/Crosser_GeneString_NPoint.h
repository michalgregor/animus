#ifndef GEN_NPOINTVCROSSER_H_INCLUDED
#define GEN_NPOINTVCROSSER_H_INCLUDED

#include "Crosser.h"
#include <Systematic/generator/HybridSeqGenerator.h>
#include <vector>

namespace genetor {

/**
* @class Crosser_GeneString_NPoint
* @author Michal Gregor
* @brief Provides crossing of 2 Individuals at a defined (N) number of points.
**/

template<class Gene>
class Crosser_GeneString_NPoint: public Crosser<std::vector<Gene> > {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Crosser<std::vector<Gene> > >(*this);
		ar & _numPoints;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Number of crossing points. Should be even.
	unsigned int _numPoints;

public:
	virtual void operator()(std::vector<Gene>& i1, std::vector<Gene>& i2);

	//! Returns the number of crossing points.
	unsigned int GetNumPoints() const {
		return _numPoints;
	}

	//! Sets the number of crossing points.
	void SetNumPoints(unsigned int numPoints) {
		_numPoints = numPoints;
	}

	explicit Crosser_GeneString_NPoint(
		unsigned int numPoints = 2,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~Crosser_GeneString_NPoint() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* The crossing operation itself. It happens in-place.
* @param i1 The first partner individual.
* @param i2 The second partner individual.
**/

template<class Gene>
void Crosser_GeneString_NPoint<Gene>::operator()(std::vector<Gene>& i1, std::vector<Gene>& i2) {
	unsigned int size = i1.size();
	unsigned int i, numPoints(_numPoints);

	if(size != i2.size()) throw TracedError_InvalidArgument("Chromosomes are not equally long.");

	//if neither chromosome is long enough to accomodate all
	if(numPoints == 0) return;

	HybridSeqGenerator<unsigned int> generator(numPoints, 0, size);
	std::vector<unsigned int> seq;

	//if odd, begin is selected as a crossing point to make the number even
	if(numPoints & 1) {
		numPoints++;

		seq = generator(numPoints, false);
		//sort the points ascendingly
		std::sort(seq.begin(), seq.end());

		seq[0] = 0;
	} else {
		seq = generator(numPoints, false);
		//sort the points ascendingly
		std::sort(seq.begin(), seq.end());
	}

	for(i = 0; i < numPoints; i+=2) {
		swap_ranges(i1.begin()+seq[i], i1.begin()+seq[i+1], i2.begin()+seq[i]);
	}
}

/**
* The Crosser_GeneString_NPoint constructor.
* @param numPoints The number of crossing points.
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class Gene>
Crosser_GeneString_NPoint<Gene>::Crosser_GeneString_NPoint(
	unsigned int numPoints,
	ProbabilityType crossoverRate,
	bool useOpenMP
):
	Crosser<std::vector<Gene> >(crossoverRate, useOpenMP), _numPoints(numPoints) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Crosser_GeneString_NPoint, 1)

#endif // GEN_NPOINTVCROSSER_H_INCLUDED
