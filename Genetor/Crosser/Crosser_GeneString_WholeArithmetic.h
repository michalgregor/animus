#ifndef GEN_ARITHMETICVCROSSER_H_INCLUDED
#define GEN_ARITHMETICVCROSSER_H_INCLUDED

#include "Crosser.h"
#include <vector>

namespace genetor {

/**
* @class Crosser_GeneString_WholeArithmetic
* @author Michal Gregor
* Applies arithmetic Crosser (new_gene_a = alpha*old_gene_a + (1-alpha)*old_gene_b)
* to every gene.
**/

template<class Gene>
class Crosser_GeneString_WholeArithmetic: public Crosser<std::vector<Gene> > {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Crosser<std::vector<Gene> > >(*this);
		ar & _alpha;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	//! The arithmetic crossover coefficient.
	RealType _alpha;

	virtual void operator()(std::vector<Gene>& i1, std::vector<Gene>& i2);

public:
	explicit Crosser_GeneString_WholeArithmetic(
		RealType alpha = 0.5f,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~Crosser_GeneString_WholeArithmetic() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* The crossing operation itself. The crossover happens in-place.
* @param i1 The first partner individual.
* @param i2 The second partner individual.
**/

template<class Gene>
void Crosser_GeneString_WholeArithmetic<Gene>::operator()(std::vector<Gene>& i1, std::vector<Gene>& i2) {
	unsigned int size = (i1.size() > i2.size())?(i2.size()):(i1.size());

	for(unsigned int i = 0; i < size; i++) {
		Gene x(i1[i]);
		i1[i] = _alpha*i2[i] + (1 - _alpha)*x;
		i2[i] = _alpha*x + (1 - _alpha)*i2[i];
	}
}

/**
* Constuctor.
* @param alpha The alpha value of the arithmetic crossover (see class info).
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class Gene>
Crosser_GeneString_WholeArithmetic<Gene>::Crosser_GeneString_WholeArithmetic(
	RealType alpha,
	ProbabilityType crossoverRate,
	bool useOpenMP
): Crosser<std::vector<Gene> >(crossoverRate, useOpenMP), _alpha(alpha) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Crosser_GeneString_WholeArithmetic, 1)

#endif // GEN_ARITHMETICVCROSSER_H_INCLUDED
