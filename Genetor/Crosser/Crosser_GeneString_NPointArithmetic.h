#ifndef GEN_NPOINTARITHMETICVCROSSER_H_INCLUDED
#define GEN_NPOINTARITHMETICVCROSSER_H_INCLUDED

#include "Crosser.h"
#include "../utils/npoint_division.h"
#include "../utils/arithmetic_crossover.h"

namespace genetor {

/**
* @class Crosser_GeneString_NPointArithmetic
* @author Michal Gregor
* Applies arithmetic crossover (new_gene_a = alpha*old_gene_a + (1-alpha)*old_gene_b)
* to selected sections of the individual.
**/

template<class GeneString>
class Crosser_GeneString_NPointArithmetic: public Crosser<GeneString> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Crosser<GeneString> >(*this);
		ar & _numPoints;
		ar & _alpha;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	typedef typename std::iterator_traits<typename GeneString::iterator>::value_type GeneType;

	//! Number of crossing points. Should be even.
	unsigned int _numPoints;
	//! The arithmetic crossover coefficient.
	GeneType _alpha;

protected:
	virtual void operator()(GeneString& i1, GeneString& i2);

public:
	//! Returns the number of crossing points.
	unsigned int GetNumPoints() const {
		return _numPoints;
	}

	//! Sets the number of crossing points.
	void SetNumPoints(unsigned int numPoints) {
		_numPoints = numPoints;
	}

	explicit Crosser_GeneString_NPointArithmetic(
		unsigned int numPoints = 2,
		RealType alpha = 0.5f,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~Crosser_GeneString_NPointArithmetic() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* The crossing operation itself. The crossover happens in-place.
* @param i1 The first partner individual.
* @param i2 The second partner individual.
**/

template<class GeneString>
void Crosser_GeneString_NPointArithmetic<GeneString>::operator()(GeneString& i1, GeneString& i2) {
	unsigned int size = i1.size();
	unsigned int i, numPoints(_numPoints);

	if(size != i2.size()) throw TracedError_InvalidArgument("Chromosomes are not equally long.");
	std::vector<unsigned int> points = npoint_division(numPoints, 0, size);

	for(i = 0; i < numPoints; i+=2) {
		for(unsigned int j = points[i]; j < points[i+1]; j++) {
			typename GeneString::iterator iter1 = i1.begin(), iter2 = i2.begin();
			std::advance(iter1, j); std::advance(iter2, j);
			arithmetic_crossover(*iter1, *iter2, _alpha);
		}
	}
}

/**
* Constuctor.
* @param numPoints The number of points to cross at.
* @param alpha The alpha value of the arithmetic crossover (see class info).
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class GeneString>
Crosser_GeneString_NPointArithmetic<GeneString>::
Crosser_GeneString_NPointArithmetic(
	unsigned int numPoints,
	RealType alpha,
	ProbabilityType crossoverRate,
	bool useOpenMP
): Crosser<GeneString>(crossoverRate, useOpenMP), _numPoints(numPoints),
_alpha(alpha) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Crosser_GeneString_NPointArithmetic, 1)

#endif // GEN_NPOINTARITHMETICVCROSSER_H_INCLUDED
