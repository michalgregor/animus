#ifndef GEN_BINARYNPOINTVECTORCROSSER_H_INCLUDED
#define GEN_BINARYNPOINTVECTORCROSSER_H_INCLUDED

#include <Systematic/utils/Binary.h>
#include <Systematic/math/Compare.h>
#include <Systematic/generator/HybridSeqGenerator.h>
#include <vector>

#include "Crosser.h"

namespace genetor {

/**
* @class Crosser_GeneString_BinaryNPoint
* @author Michal Gregor
* This Crosser utilizes binary operators to implement n-point binary
* crossover to an arbitrary type. Note that this may be an inefficient
* operator for most data types and it is not computationally fast either.
**/

template<class Gene>
class Crosser_GeneString_BinaryNPoint: public Crosser<std::vector<Gene> > {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Crosser<std::vector<Gene> > >(*this);
		ar & _numPoints;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! Number of crossing points. Should be even.
	unsigned int _numPoints;

protected:
	void ElementarySwap(std::vector<Gene>& a, std::vector<Gene>& b, unsigned int pos1, unsigned int pos2) const;

public:
	virtual void operator()(std::vector<Gene>& i1, std::vector<Gene>& i2);

	//! Returns the number of crossing points.
	unsigned int GetNumPoints() const {
		return _numPoints;
	}

	//! Sets the number of crossing points.
	void SetNumPoints(unsigned int numPoints) {
		_numPoints = numPoints;
	}

	explicit Crosser_GeneString_BinaryNPoint(
		unsigned int numPoints = 2,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~Crosser_GeneString_BinaryNPoint() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* A utility function that does the actual swapping.
**/

template<class Gene>
void Crosser_GeneString_BinaryNPoint<Gene>::ElementarySwap(
	std::vector<Gene>& a, std::vector<Gene>& b, unsigned int pos1, unsigned int pos2
) const {
	unsigned int geneSize = sizeof(Gene)*CHAR_BIT;
	float lbound(pos1/geneSize), ubound(pos2/geneSize);
	unsigned int ilbound = ceil(lbound), iubound = ubound;

	//swap whole genes in the range
	std::swap_ranges(&a + ilbound, &a + iubound,  &b + ilbound);

	unsigned int bias = floor(lbound)*geneSize;

	//boundaries
	if((unsigned int)lbound == iubound) BinarySwap<Gene>(a[(unsigned int)lbound], b[(unsigned int)lbound], pos1 - bias, pos2 - bias);
	else {
		if(!Compare(lbound, (float)ilbound)) {
			BinarySwap(a[(unsigned int)lbound], b[(unsigned int)lbound], pos1 - bias, geneSize);
		}

		if(!Compare(ubound, (float)iubound)) {
			BinarySwap(a[iubound], b[iubound], 0, pos2 - floor(ubound)*geneSize);
		}
	}
}

/**
* The crossing operation. crossover happens in-place.
* @param i1 The first partner individual.
* @param i2 The second partner individual.
**/

template<class Gene>
void Crosser_GeneString_BinaryNPoint<Gene>::operator()(std::vector<Gene>& ch1, std::vector<Gene>& ch2) {
	unsigned int i, numPoints(_numPoints);

	if(ch1.size() != ch2.size()) throw TracedError_InvalidArgument("Chromosomes are not equally long.");

	unsigned int geneSize = sizeof(Gene)*CHAR_BIT, dataSize = ch1.size()*geneSize;

	//if neither chromosome is long enough to accomodate all
	if(numPoints > (dataSize+1)/2) numPoints = (dataSize+1)/2;
	if(numPoints == 0) return;

	HybridSeqGenerator<unsigned int> generator(numPoints, 0, dataSize);
	std::vector<unsigned int> seq;

	//if odd, begin is selected as a crossing point to make the number even
	if(numPoints & 1) {
		numPoints++;

		seq = generator(numPoints, false);
		//sort the points ascendingly
		std::sort(seq.begin(), seq.end());

		seq[0] = 0;
	} else {
		seq = generator(numPoints, false);
		//sort the points ascendingly
		std::sort(seq.begin(), seq.end());
	}

	//go through the points and swap
	for(i = 0; i < numPoints; i+=2) {
		ElementarySwap(ch1, ch2, seq[i], seq[i+1]);
	}
}

/**
* The Crosser_GeneString_BinaryNPoint constructor.
* @param numPoints The number of crossing points. Should be even.
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class Gene>
Crosser_GeneString_BinaryNPoint<Gene>::
Crosser_GeneString_BinaryNPoint(unsigned int numPoints, ProbabilityType crossoverRate, bool useOpenMP):
	Crosser<std::vector<Gene> >(crossoverRate, useOpenMP), _numPoints(numPoints) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Crosser_GeneString_BinaryNPoint, 1)

#endif // GEN_BINARYNPOINTVECTORCROSSER_H_INCLUDED
