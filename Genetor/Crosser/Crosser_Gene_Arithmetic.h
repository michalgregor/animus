#ifndef GEN_ARITHMETICCROSSER_H_INCLUDED
#define GEN_ARITHMETICCROSSER_H_INCLUDED

#include "Crosser.h"

namespace genetor {

/**
* @class Crosser_Gene_Arithmetic
* @author Michal Gregor
*
* Applies arithmetic crossover:
*
* (new_gene_a = alpha*old_gene_a + (1-alpha)*old_gene_b)
*
* to a single numeric value.
**/

template<class Gene>
class Crosser_Gene_Arithmetic: public Crosser<Gene> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Crosser<Gene> >(*this);
		ar & _alpha;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	//! The arithmetic crossover coefficient.
	RealType _alpha;

public:
	virtual void operator()(Gene& gene1, Gene& gene2);

	explicit Crosser_Gene_Arithmetic(
		RealType alpha,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~Crosser_Gene_Arithmetic() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Applies in-place arithmetic crossover to a pair of numeric values.
* @param gene1 The first partner.
* @param gene1 The second partner.
**/

template<class Gene>
void Crosser_Gene_Arithmetic<Gene>::operator()(Gene& gene1, Gene& gene2) {
	Gene x(gene1);
	gene1 = _alpha*gene2 + (1 - _alpha)*x;
	gene2 = _alpha*x + (1 - _alpha)*gene2;
}

/**
* Constuctor.
* @param alpha The alpha value of the arithmetic crossover (see class info).
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class Gene>
Crosser_Gene_Arithmetic<Gene>::Crosser_Gene_Arithmetic(
	RealType alpha,
	ProbabilityType crossoverRate,
	bool useOpenMP
): Crosser<Gene>(crossoverRate, useOpenMP), _alpha(alpha) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Crosser_Gene_Arithmetic, 1)

#endif // GEN_ARITHMETICCROSSER_H_INCLUDED
