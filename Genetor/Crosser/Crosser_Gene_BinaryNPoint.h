#ifndef GEN_BINARYNPOINTCROSSER_H_INCLUDED
#define GEN_BINARYNPOINTCROSSER_H_INCLUDED

#include <Systematic/utils/Binary.h>
#include <Systematic/generator/HybridSeqGenerator.h>

#include "Crosser.h"

namespace genetor {

using std::vector;

/**
* @class Crosser_Gene_BinaryNPoint
* @author Michal Gregor
*
* Employs a method very similar to that of the Crosser_GeneString_BinaryNPoint. The
* difference is that Crosser_GeneString_BinaryNPoint operates on vectors of values,
* whereas Crosser_Gene_BinaryNPoint operates on values.
**/

template<class Gene>
class Crosser_Gene_BinaryNPoint: public Crosser<Gene> {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<Crosser<Gene> >(*this);
		ar & _numPoints;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

public:
	typedef Gene Genotype;

private:
	//! Number of crossing points. Should be even.
	unsigned int _numPoints;

public:
	virtual void operator()(Gene& i1, Gene& i2);

	unsigned int GetNumPoints() const;
	void SetNumPoints(unsigned int numPoints);

	explicit Crosser_Gene_BinaryNPoint(
		unsigned int numPoints = 2,
		ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE,
		bool useOpenMP = true
	);

	virtual ~Crosser_Gene_BinaryNPoint() {
		SYS_EXPORT_INSTANCE()
	}
};

/**
* Returns the number of crossing points.
**/

template<class Gene>
unsigned int Crosser_Gene_BinaryNPoint<Gene>::GetNumPoints() const {
	return _numPoints;
}

/**
* Sets the number of crossing points.
**/

template<class Gene>
void Crosser_Gene_BinaryNPoint<Gene>::SetNumPoints(unsigned int numPoints) {
	_numPoints = numPoints;
}

/**
* The crossing operation. crossover happens in-place.
* @param i1 The first partner individual.
* @param i2 The second partner individual.
**/

template<class Gene>
void Crosser_Gene_BinaryNPoint<Gene>::operator()(Gene& i1, Gene& i2) {
	unsigned int i, numPoints(_numPoints);
	unsigned int geneSize = sizeof(Gene)*CHAR_BIT;

	//if neither chromosome is long enough to accomodate all
	if(numPoints > (geneSize+1)/2) numPoints = (geneSize+1)/2;
	if(numPoints == 0) return;

	HybridSeqGenerator<unsigned int> generator(numPoints, 0, geneSize);
	vector<unsigned int> seq;

	//if odd, begin is selected as a crossing point to make the number even
	if(numPoints & 1) {
		numPoints++;

		seq = generator(numPoints, false);
		//sort the points ascendingly
		std::sort(seq.begin(), seq.end());

		seq[0] = 0;
	} else {
		seq = generator(numPoints, false);
		//sort the points ascendingly
		std::sort(seq.begin(), seq.end());
	}

	//go through the points and swap
	for(i = 0; i < numPoints; i+=2) {
		BinarySwap(i1, i2, seq[i], seq[i+1]);
	}

}
/**
* The Crosser_Gene_BinaryNPoint constructor.
* @param numPoints The number of crossing points. Should be even.
* @param crossoverRate The probability that crossover, as implemented by
* operator(), will be applied to any given individual.
* @param useOpenMP Set to true if OpenMP is to be used to parallelise
* the application of the operator.
**/

template<class Gene>
Crosser_Gene_BinaryNPoint<Gene>::
Crosser_Gene_BinaryNPoint(unsigned int numPoints, ProbabilityType crossoverRate, bool useOpenMP):
Crosser<Gene>(crossoverRate, useOpenMP), _numPoints(numPoints) {}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Crosser_Gene_BinaryNPoint, 1)

#endif // GEN_BINARYNPOINTCROSSER_H_INCLUDED
