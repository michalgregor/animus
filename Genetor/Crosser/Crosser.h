#ifndef GEN_CROSSER_H_INCLUDED
#define GEN_CROSSER_H_INCLUDED

#include "../system.h"
#include "../ReproductionOperator.h"
#include <Systematic/generator/BoundaryGenerator.h>
#include <Systematic/exception/OpenMPException.h>

namespace genetor {

#define GEN_DEF_CROSSOVER_RATE 1.0f

/**
* @class Crosser
* @author Michal Gregor
* The common interface for ReproductionOperators that carry out crossover.
* The crossover is defined by implementing operator(). It is applied to every
* individual with the specified probability.
*
* @tparam Individual Type of the Individual that the Crosser is to operate on.
**/

template<class Individual>
class Crosser: public ReproductionOperator<Individual>  {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/

	template<class Archive>
	void serialize(Archive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<ReproductionOperator<Individual> >(*this);
		ar & _crossoverRate;
		ar & _randomGenerator;
		ar & _useOpenMP;
	}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

private:
	//! The probability that mutation as implemented by operator() will
	//! be applied to any given individual.
	ProbabilityType _crossoverRate;
	//! Generator used to decide whether to apply crossover or not.
	systematic::BoundaryGenerator<ProbabilityType> _randomGenerator;
	//! Set to true if OpenMP is to be used to parallelise the application
	//! of the operator.
	bool _useOpenMP;

private:
	virtual void operate(
		const typename ReproductionOperator<Individual>::iterator& first,
		const typename ReproductionOperator<Individual>::iterator& last
	);

public:
	//! Returns true if the operator is set to use OpenMP-based parallelization.
	bool getUseOpenMP() {return _useOpenMP;}
	//! Sets whether the operator is to use OpenMP-based parallelization.
	void setUseOpenMP(bool useOpenMP) {_useOpenMP = useOpenMP;}

	//! This method applies in-place crossover to a single pair of individuals.
	virtual void operator()(Individual& i1, Individual& i2) = 0;

	explicit Crosser(ProbabilityType crossoverRate = GEN_DEF_CROSSOVER_RATE, bool useOpenMP = true);
	virtual ~Crosser() = 0;
};

/**
 * Implementation of the ReproductionOperator interface.
 */

template<class Individual>
void Crosser<Individual>::operate(
	const typename ReproductionOperator<Individual>::iterator& first,
	const typename ReproductionOperator<Individual>::iterator& last
) {
	systematic::OpenMPException<TracedError_Generic> openMPException;
	typename ReproductionOperator<Individual>::iterator iter;

	#pragma omp parallel for if(_useOpenMP)
	for(iter = first + 1; iter < last; iter += 2) {
		try {
			operator()(*iter, *(iter-1));
		} catch(std::exception& err) {
			openMPException << err;
		} catch(...) {
			openMPException << "Unknown exception.";
		}
	}

	openMPException.rethrow();
}

/**
* Constructor.
 * @param crossoverRate The probability that crossover, as implemented by
 * operator(), will be applied to any given individual.
 * @param  Set to true if OpenMP is to be used to parallelise the application
 * of the operator.
**/

template<class Individual>
Crosser<Individual>::Crosser(ProbabilityType crossoverRate, bool useOpenMP):
_crossoverRate(crossoverRate), _randomGenerator(0, 1), _useOpenMP(useOpenMP) {}

/**
* Body of the pure virtual destructor.
**/

template<class Individual>
Crosser<Individual>::~Crosser() {
	SYS_EXPORT_INSTANCE()
}

} //namespace genetor

SYS_EXPORT_TEMPLATE(genetor::Crosser, 1)

#endif // GEN_CROSSER_H_INCLUDED
