# README #

Animus is a set of machine learning libraries. Library Systematic holds some common infrastructure on which the other libraries rely. 

## How to Build ##

The source contains Cmake makefiles. These are periodically being tested on Linux. They have been tested on Windows for some time now, but
if any modifications will be required to get the libraries up and running, they will not be large ones. No testing was done on other platforms
such as OS X. If anybody is willing to try, however, the required fixes should be trivial.

## License ##

Animus is covered by the New BSD License:

Copyright (c) 2014, Michal Gregor
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of its contributors may be
	  used to endorse or promote products derived from this software without
	  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Dependencies ##

* The Boost C++ libraries. Distributed under the Boost Software License. Obtainable from http://www.boost.org/.
* The Eigen3 library, distributed under Mozilla Public License 2.0. Source code available at http://eigen.tuxfamily.org/.

## Optional Dependencies ##

* Rlearner optionally depends on Torch3 for neural network support. Torch3 is available at http://torch.ch/torch3/.
* Optional linking to binutils-dev on Linux provides Systematic/exception/backward.hpp backtraces with additional info.
* Doxygen for generating documentation.
* SWIG and CPython for generating Python bindings.

## Credits ##

* The Rlearner library is a fork of RLToolbox by Gerhard and Stephan Neumann. The original sources can be downloaded from http://www.igi.tu-graz.ac.at/gerhard/ril-toolbox.
* The fixed arithmetic classes included in Systematic/math/FixedArithmetic.h are from Evan Teran. The original can be downloaded from http://www.codef00.com/code/Fixed.h.
* Lite xformat (source code available under the Boost Software License from http://www.cs.umd.edu/~saeed/lite/html/index.html) is used in Systematic/xformat.h to provide scanf-like interface for istreams.
* François-Xavier Bourlet's backward-cpp is used in Systematic/exception/backward.hpp,.cpp to provide backtraces on Linux. The source is covered by MIT license and available from https://github.com/bombela/backward-cpp.
* The fmtlib library (http://fmtlib.net; in Systematic/format/) is used for providing formatted output. The original source is covered by the BSD license and can be retrieved at https://github.com/fmtlib/fmt.
