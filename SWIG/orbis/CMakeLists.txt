FILE(COPY "__init__.py" DESTINATION "./")
FILE(COPY "annalyst/som_plot.py" DESTINATION "./")

INSTALL(FILES "__init__.py" DESTINATION ${PYTHON_MODULE_PATH} )
INSTALL(FILES "annalyst/som_plot.py" DESTINATION ${PYTHON_MODULE_PATH} )

macro(SWIG_ADD_LIBRARY name language)
  SWIG_MODULE_INITIALIZE(${name} ${language})
  set(swig_dot_i_sources)
  set(swig_other_sources)
  foreach(it ${ARGN})
    if(${it} MATCHES "\\.i$")
      set(swig_dot_i_sources ${swig_dot_i_sources} "${it}")
    else()
      set(swig_other_sources ${swig_other_sources} "${it}")
    endif()
  endforeach()

  set(swig_generated_sources)
  foreach(it ${swig_dot_i_sources})
    SWIG_ADD_SOURCE_TO_MODULE(${name} swig_generated_source ${it})
    set(swig_generated_sources ${swig_generated_sources} "${swig_generated_source}")
  endforeach()
  get_directory_property(swig_extra_clean_files ADDITIONAL_MAKE_CLEAN_FILES)
  set_directory_properties(PROPERTIES
    ADDITIONAL_MAKE_CLEAN_FILES "${swig_extra_clean_files};${swig_generated_sources}")
  add_library(${SWIG_MODULE_${name}_REAL_NAME}
    SHARED
    ${swig_generated_sources}
    ${swig_other_sources})
  set_target_properties(${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES NO_SONAME ON)
  string(TOLOWER "${language}" swig_lowercase_language)
  if ("${swig_lowercase_language}" STREQUAL "octave")
    set_target_properties(${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES PREFIX "")
    set_target_properties(${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES SUFFIX ".oct")
  elseif ("${swig_lowercase_language}" STREQUAL "go")
    set_target_properties(${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES PREFIX "")
  elseif ("${swig_lowercase_language}" STREQUAL "java")
    if (APPLE)
        # In java you want:
        #      System.loadLibrary("LIBRARY");
        # then JNI will look for a library whose name is platform dependent, namely
        #   MacOS  : libLIBRARY.jnilib
        #   Windows: LIBRARY.dll
        #   Linux  : libLIBRARY.so
        set_target_properties (${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES SUFFIX ".jnilib")
      endif ()
  elseif ("${swig_lowercase_language}" STREQUAL "lua")
    set_target_properties(${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES PREFIX "")
  elseif ("${swig_lowercase_language}" STREQUAL "python")
    # this is only needed for the python case where a _modulename.so is generated
    set_target_properties(${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES PREFIX "")
    # Python extension modules on Windows must have the extension ".pyd"
    # instead of ".dll" as of Python 2.5.  Older python versions do support
    # this suffix.
    # http://docs.python.org/whatsnew/ports.html#SECTION0001510000000000000000
    # <quote>
    # Windows: .dll is no longer supported as a filename extension for extension modules.
    # .pyd is now the only filename extension that will be searched for.
    # </quote>
    if(WIN32 AND NOT CYGWIN)
      set_target_properties(${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES SUFFIX ".pyd")
    endif()
  elseif ("${swig_lowercase_language}" STREQUAL "r")
    set_target_properties(${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES PREFIX "")
  elseif ("${swig_lowercase_language}" STREQUAL "ruby")
    # In ruby you want:
    #      require 'LIBRARY'
    # then ruby will look for a library whose name is platform dependent, namely
    #   MacOS  : LIBRARY.bundle
    #   Windows: LIBRARY.dll
    #   Linux  : LIBRARY.so
    set_target_properties (${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES PREFIX "")
    if (APPLE)
      set_target_properties (${SWIG_MODULE_${name}_REAL_NAME} PROPERTIES SUFFIX ".bundle")
    endif ()
  endif ()
endmacro()

function(add_swig module_name file_name)
	set_property(SOURCE ${file_name} PROPERTY SWIG_MODULE_NAME ${module_name})
	SET_SOURCE_FILES_PROPERTIES(${file_name} PROPERTIES CPLUSPLUS ON)
	SET_SOURCE_FILES_PROPERTIES(${file_name} PROPERTIES SWIG_FLAGS "")
	SWIG_ADD_LIBRARY(${module_name} python ${file_name})
	SWIG_LINK_LIBRARIES(${module_name} ${LINK_LIBRARIES} ${SWIG_PYTHON_LIBS} "${ARGN}")
	INSTALL ( TARGETS _${module_name} DESTINATION ${PYTHON_MODULE_PATH} )
	INSTALL ( FILES ${CMAKE_CURRENT_BINARY_DIR}/${module_name}.py DESTINATION ${PYTHON_MODULE_PATH} )
endfunction()

ADD_SUBDIRECTORY(common)
ADD_SUBDIRECTORY(systematic)
ADD_SUBDIRECTORY(annalyst)
ADD_SUBDIRECTORY(rlearner)
ADD_SUBDIRECTORY(motivator)

# Common
add_swig(__common__ common/__common__.i Systematic)

# Systematic
add_swig(__algebra__ systematic/__algebra__.i Systematic)
add_swig(__systematic__ systematic/__systematic__.i Systematic)
add_swig(systematic systematic/systematic.i Systematic)
add_custom_target(pySystematic DEPENDS ___common__ ___algebra__ ___systematic__ _systematic)

# Annalyst
add_swig(__annalyst__ annalyst/__annalyst__.i _systematic Annalyst Systematic)
add_swig(annalyst annalyst/annalyst.i _systematic Annalyst Systematic)
add_custom_target(pyAnnalyst DEPENDS ___common__ ___annalyst__ _annalyst)

# Rlearner
add_swig(__rlearner_base__ rlearner/__rlearner_base__.i Rlearner Annalyst Systematic)
add_swig(__rlearner_benchmarks__ rlearner/__rlearner_benchmarks__.i Rlearner Annalyst Systematic)
add_swig(rlearner rlearner/rlearner.i Rlearner Annalyst Systematic)
add_custom_target(pyRlearner DEPENDS ___common__ _rlearner ___rlearner_base__ ___rlearner_benchmarks__)

# Motivator
add_swig(__motivator__ motivator/__motivator__.i Motivator Rlearner Annalyst Systematic)
add_swig(motivator motivator/motivator.i Motivator Rlearner Annalyst Systematic)
add_custom_target(pyMotivator DEPENDS ___common__ ___motivator__ _motivator)

# == All SWIG interfaces ==
add_custom_target(pyOrbis DEPENDS pySystematic pyAnnalyst pyRlearner pyMotivator)
