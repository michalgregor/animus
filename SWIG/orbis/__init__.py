import sys
from orbis.__common__ import *

toProtect = [];

def __find_obj_func__(prefix, obj, suffix):
    try:    
        func = getattr(sys.modules['__main__'], prefix + obj.__class__.__name__ + suffix)
    except AttributeError:
        try:
            func = getattr(sys.modules[obj.__module__], prefix + obj.__class__.__name__ + suffix)      
            
        except AttributeError:
            return None
    
    return func

def __call_obj_func__(prefix, obj, suffix = ''):
    return __find_obj_func__(prefix, obj, suffix)(obj)

def raw_ptr(obj):   
    return __call_obj_func__('raw_', obj)
    
def shared_get(obj):
    return __call_obj_func__('sharedget_', obj)
    
def shared_cast(obj, outtype):
    return __call_obj_func__('shared_cast_', obj, '_' + outtype.__name__)
    
def dynamic_cast(obj):
    func = __find_obj_func__('dynamic_cast_', obj)
    
    if func == None:
        return obj
    else:
        dc = func(obj)
        return dc.get()

def protect(obj, protectedList = toProtect):
    protectedList.append(obj)
    obj.thisown = 0
    return obj
    
def disown(obj):
    obj.thisown = 0
    return obj
    
def unprotect(protectedList):
    for obj in reversed(protectedList):
        obj.thisown = 1
    protectedList = []