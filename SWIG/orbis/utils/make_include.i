// Includes the specified include file both in SWIG and in C++.
// The argument should be enclosed either in "" or in <>.

%define make_include(FILE)
	%{
		#include FILE
	%}

	#ifdef ##__SWIG_IMPORT_ONLY__
		%import FILE;
	#else
		%include FILE;
	#endif
%enddef







%define import_module(FILE)
	#ifdef ##__SWIG_IMPORT_ONLY__
		%include FILE
	#else
		#define __SWIG_IMPORT_ONLY__
		%include FILE
		#undef __SWIG_IMPORT_ONLY__
	#endif
%enddef

// A convenience shortcut for:
// %{
//		#include FILE
// %}
//
// The argument should be enclosed either in "" or in <>.
%define add_dependency(FILE)
	%{
		#include FILE
	%}
%enddef
