// An auxiliary macro that either generates a matching %template directive,
// if we are fully processing the file, or else generates an %ignore directive
// if we are only importing (__SWIG_IMPORT_ONLY__ is defined).
//
// Note: __SWIG_IMPORT_ONLY__ gets defined by the import_module() macro.
%define make_template(TYPE_NAME)
	#ifndef ##__SWIG_IMPORT_ONLY__
		%template(TYPE_NAME) 
	#else
		%ignore 
	#endif
%enddef
