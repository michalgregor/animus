%define pydirector_cast(TYPE)
%extend TYPE {
	PyObject* __cast__() {
		constexpr auto msg = 
			"Method __cast__ can only be called on an object of a python-derived class.";

		if(!$self) throw TracedError_InvalidArgument(msg);
		auto* obj = dynamic_cast<Swig::Director*>($self);
		if(!obj) throw TracedError_InvalidArgument(msg);
		PyObject* ret = obj->swig_get_self();
		if(!ret) throw TracedError_InvalidArgument(msg);

		Py_INCREF(ret);
		return ret;
	}
}
%enddef

/*
*	A macro that combines %feature("director"), smart_ptr and pydirector_cast.
*/
%define make_director(TYPE)
	%feature("director") TYPE;
	pydirector_cast(TYPE)
%enddef
