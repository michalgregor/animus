//------------------------------------------------------------------------------
//							Common stuff
//------------------------------------------------------------------------------

%{
	char __aux_buffer_float_format__[2] = "f";
	char __aux_buffer_int_format__[2] = "i";
%}

//------------------------------------------------------------------------------
//					Buffer protocol for vector-like objects
//
// Requirements: The vector-like object has to implement the following members:
//		* data(): returns a ptr to the underlying data
//		* size(): returns the number of elements in the vector
//------------------------------------------------------------------------------

%define buffer_protocol_float(CLASSNAME, CLASSID)
	__buffer_protocol_impl__(CLASSNAME, CLASSID, __aux_buffer_float_format__)
%enddef

%define buffer_protocol_int(CLASSNAME, CLASSID)
	__buffer_protocol_impl__(CLASSNAME, CLASSID, __aux_buffer_int_format__)
%enddef

%define __buffer_protocol_impl__(CLASSNAME, CLASSID, DATA_FORMAT)

%feature("python:bf_getbuffer") CLASSNAME "&__"#CLASSID##"_as_buffer__";
%feature("python:bf_releasebuffer") CLASSNAME "&__"#CLASSID##"_release_buffer__";

%{
	static int __##CLASSID##_as_buffer__(PyObject *obj, Py_buffer *view, int flags) {
		if (view == NULL) {
			PyErr_SetString(PyExc_ValueError, "NULL view in getbuffer");
			return -1;
		}

		SwigPyObject *sobj = SWIG_Python_GetSwigThis(obj);
		CLASSNAME* self = (CLASSNAME*) sobj->ptr;

		// the shape array
		Py_ssize_t* shape = new Py_ssize_t[1];
		shape[0] = self->size();

		view->obj = (PyObject*) obj;
		view->buf = (void*) self->data();
		view->len = self->size() * sizeof(CLASSNAME::value_type);
		view->readonly = 0;
		view->itemsize = sizeof(CLASSNAME::value_type);
		view->format = DATA_FORMAT;
		view->ndim = 1;
		view->shape = shape;  // length-1 sequence of dimensions
		view->strides = &view->itemsize;  // for the simple case we can do this
		view->suboffsets = NULL;
		view->internal = NULL;

		Py_INCREF(obj);  // need to increase the reference count
		return 0;
	}

	static void __##CLASSID##_release_buffer__(PyObject *obj, Py_buffer *view) {
		// we do not decrement obj, PyBuffer_Release does that automatically
		delete[] view->shape;
	}
%}

%enddef

//------------------------------------------------------------------------------
//						Buffer protocol for matrices
//
// Requirements: The vector-like object has to implement the following members:
//		* data(): returns a ptr to the underlying data
//		* rows(), cols(): return the number of rows and cols in the matrix
//------------------------------------------------------------------------------

%define buffer_protocol_float_matrix(CLASSNAME, CLASSID)
	__buffer_protocol_impl_matrix__(CLASSNAME, CLASSID, __aux_buffer_float_format__)
%enddef

%define buffer_protocol_int_matrix(CLASSNAME, CLASSID)
	__buffer_protocol_impl_matrix__(CLASSNAME, CLASSID, __aux_buffer_int_format__)
%enddef

%define __buffer_protocol_impl_matrix__(CLASSNAME, CLASSID, DATA_FORMAT)

%feature("python:bf_getbuffer") CLASSNAME "&__"#CLASSID##"_as_buffer__";
%feature("python:bf_releasebuffer") CLASSNAME "&__"#CLASSID##"_release_buffer__";

%{
	static int __##CLASSID##_as_buffer__(PyObject *obj, Py_buffer *view, int flags) {
		if (view == NULL) {
			PyErr_SetString(PyExc_ValueError, "NULL view in getbuffer");
			return -1;
		}

		SwigPyObject *sobj = SWIG_Python_GetSwigThis(obj);
		CLASSNAME* self = (CLASSNAME*) sobj->ptr;

		// the shape array
		Py_ssize_t* shape = new Py_ssize_t[2];

		shape[0] = self->rows();
		shape[1] = self->cols();

		Py_ssize_t* strides = new Py_ssize_t[2];
		strides[0] = sizeof(CLASSNAME::value_type);
		strides[1] = strides[0]*shape[0];

		view->obj = (PyObject*) obj;
		view->buf = (void*) self->data();
		view->len = self->rows() * self->cols() * sizeof(CLASSNAME::value_type);
		view->readonly = 0;
		view->itemsize = sizeof(CLASSNAME::value_type);
		view->format = DATA_FORMAT;
		view->ndim = 2;
		view->shape = shape;  // length-1 sequence of dimensions
		view->strides = strides;  // for the simple case we can do this
		view->suboffsets = NULL;
		view->internal = NULL;

		// fix strides for ColMajor matrices
		if(!CLASSNAME::IsRowMajor) {
			view->strides[1] = sizeof(CLASSNAME::value_type);
			view->strides[0] = strides[1]*shape[1];
		}

		Py_INCREF(obj);  // need to increase the reference count
		return 0;
	}

	static void __##CLASSID##_release_buffer__(PyObject *obj, Py_buffer *view) {
		// we do not decrement obj, PyBuffer_Release does that automatically
		delete[] view->shape;
		delete[] view->strides;
	}
%}

%enddef

