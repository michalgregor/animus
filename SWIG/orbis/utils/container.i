%define %orbis_container(Sequence...)

%feature("python:slot", "sq_contains", functype="objobjproc") __contains__;
%feature("python:slot", "mp_length", functype="lenfunc") __len__;
%feature("python:slot", "mp_subscript", functype="binaryfunc") __getitem__;
%feature("python:slot", "mp_ass_subscript", functype="objobjargproc") __setitem__;

bool __contains__(Sequence::reference val) {
	return std::find(begin(*$self), end(*$self), val) != end(*$self);
}

size_t __len__() {
	return $self->size();
}

Sequence::reference __getitem__(size_t index) {
	if(index >= $self->size())
		throw TracedError_OutOfRange("Index overflow.");

    return $self->operator[](index);
}

void __setitem__(size_t index, Sequence::const_reference val) {
	if(index >= $self->size())
		throw TracedError_OutOfRange("Index overflow.");

    $self->operator[](index) = val;
}

%enddef

