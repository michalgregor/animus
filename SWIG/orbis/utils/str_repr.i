#ifndef __STR_REPR_I__
#define __STR_REPR_I__

%define __STR__()

%feature("python:slot", "tp_repr", functype="reprfunc") __str__;
%feature("python:slot", "tp_str", functype="reprfunc") __repr__;

std::string __str__() {
  std::ostringstream out;
  out << *$self;
  return out.str();
}
std::string __repr__() {
  std::ostringstream out;
  out << *$self;
  return out.str();
}

%enddef

#endif // __STR_REPR_I__

