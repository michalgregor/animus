%define dynamic_cast(Type)
%inline %{
	class dynamic_cast_##Type {
	private:
		Type* _obj;

	public:
		void set(Type* obj) {
			_obj = obj;
		}		
		
		Type* get() {
			return _obj;
		}

		dynamic_cast_##Type(Type* obj): _obj(obj) {}
	};
%}
%enddef
