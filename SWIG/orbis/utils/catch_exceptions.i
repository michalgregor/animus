%include exception.i

%exception {
  try {
    $action
  } catch(const TracedError_OutOfRange& e) {
	SWIG_exception(SWIG_IndexError, e.what());
  } catch(const TracedError_InvalidArgument& e) {
	SWIG_exception(SWIG_ValueError, e.what());
  } catch (const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  }
}
