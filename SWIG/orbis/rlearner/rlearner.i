#ifndef __SWIG__RLEARNER_I__
#define __SWIG__RLEARNER_I__

#ifndef __SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") rlearner
	%feature("autodoc", "3");

	%pythoncode %{
		import orbis.systematic;
		from orbis.__rlearner_base__ import *;
		from orbis.__rlearner_benchmarks__ import *;
		#from orbis.__rlearner_algo__ import *;
	%}
#endif

%{
	#include <Rlearner/system.h>
	using namespace rlearner;
%}

%include "../system.i"

#endif // __SWIG__RLEARNER_I__

