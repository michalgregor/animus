#ifndef SWIG___RLEARNER_BASE___I
#define SWIG___RLEARNER_BASE___I

#ifndef __SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") __rlearner_base__
	%feature("autodoc", "3");
#endif

%include "../system.i"
%include "std_list.i"
%include "std_map.i"
%include "std_set.i"
%include "std_vector.i"
%include "std_pair.i"

import_module("rlearner.i")
import_module("../systematic/__algebra__.i")
import_module("../systematic/__systematic__.i")

namespace rlearner {}

using namespace systematic;
using namespace annalyst;
using namespace rlearner;

//------------------------------------------------------------------------------
//					Director and smartptr declarations
//------------------------------------------------------------------------------

// algebra.h
// only has typedefs

// baseobjects.h
make_director(rlearner::ActionObject)
make_director(rlearner::StateObject)
make_director(rlearner::StateModifiersObject)
smartptr(rlearner::ActionObject)
smartptr(rlearner::StateObject)
smartptr(rlearner::StateModifiersObject)

// parameters.h
make_director(rlearner::Parameters)
make_director(rlearner::ParameterObject)
make_director(rlearner::AdaptiveParameterCalculator)
make_director(rlearner::AdaptiveParameterBoundedValuesCalculator)
make_director(rlearner::AdaptiveParameterUnBoundedValuesCalculator)
smartptr(rlearner::Parameters)
smartptr(rlearner::ParameterObject)
smartptr(rlearner::AdaptiveParameterCalculator)
smartptr(rlearner::AdaptiveParameterBoundedValuesCalculator)
smartptr(rlearner::AdaptiveParameterUnBoundedValuesCalculator)

// learndataobject.h
make_director(rlearner::LearnDataObject)
smartptr(rlearner::LearnDataObject)

// state.h
// make_director(rlearner::StateCollection)								-- not needed
// make_director(rlearner::State)											-- not needed
// make_director(rlearner::StateList)										-- not needed
smartptr(rlearner::StateCollection)
smartptr(rlearner::State)
smartptr(rlearner::StateList)

// stateproperties.h
make_director(rlearner::StateProperties)
smartptr(rlearner::StateProperties)

// statecollection.h
make_director(rlearner::StateCollectionReporter)
//make_director(rlearner::StateCollectionImpl)							-- unnecessary
make_director(rlearner::StateCollectionListReporter)
//make_director(rlearner::StateCollectionList)							-- unnecessary
smartptr(rlearner::StateCollectionReporter)
smartptr(rlearner::StateCollectionImpl)
smartptr(rlearner::StateCollectionListReporter)
smartptr(rlearner::StateCollectionList)

// statemodifier.h
make_director(rlearner::StateModifier)
//make_director(rlearner::NeuralNetworkStateModifier)						-- unnecessary
make_director(rlearner::FeatureCalculator)
make_director(rlearner::StateMultiModifier)
make_director(rlearner::StateVariablesChooser)
smartptr(rlearner::StateModifier)
smartptr(rlearner::NeuralNetworkStateModifier)
smartptr(rlearner::FeatureCalculator)
smartptr(rlearner::StateMultiModifier)
smartptr(rlearner::StateVariablesChooser)

// StateModifierList.h
make_director(rlearner::ModifierListReporter)
smartptr(rlearner::StateModifierList)
smartptr(rlearner::ModifierListReporter)

// discretizer.h
make_director(rlearner::AbstractStateDiscretizer)
make_director(rlearner::ModelStateDiscretizer)
make_director(rlearner::SingleStateDiscretizer)
make_director(rlearner::DiscreteStateOperatorAnd)
smartptr(rlearner::AbstractStateDiscretizer)
smartptr(rlearner::ModelStateDiscretizer)
smartptr(rlearner::SingleStateDiscretizer)
smartptr(rlearner::DiscreteStateOperatorAnd)

// action.h
make_director(rlearner::Action)
make_director(rlearner::ActionData)
make_director(rlearner::MultiStepActionData)
make_director(rlearner::HierarchicalStack)
make_director(rlearner::MultiStepAction)
make_director(rlearner::PrimitiveAction)
make_director(rlearner::NumericAction)
make_director(rlearner::ExtendedAction)
make_director(rlearner::ActionSet)
//make_director(rlearner::ActionDataSet)									-- unnecessary
make_director(rlearner::ActionList)

smartptr(rlearner::Action)
smartptr(rlearner::ActionData)
smartptr(rlearner::MultiStepActionData)
smartptr(rlearner::HierarchicalStack)
smartptr(rlearner::MultiStepAction)
smartptr(rlearner::PrimitiveAction)
smartptr(rlearner::NumericAction)
smartptr(rlearner::ExtendedAction)
smartptr(rlearner::ActionSet)
smartptr(rlearner::ActionDataSet)
smartptr(rlearner::ActionList)

// ActionEncoder.h
make_director(rlearner::ActionEncoder)
smartptr(rlearner::ActionEncoder)

// agentlistener.h
make_director(rlearner::SemiMDPListener)
make_director(rlearner::SemiMDPRewardListener)
make_director(rlearner::AdaptiveParameterFromNStepsCalculator)
make_director(rlearner::AdaptiveParameterFromNEpisodesCalculator)
make_director(rlearner::AdaptiveParameterFromAverageRewardCalculator)
smartptr(rlearner::SemiMDPListener)
smartptr(rlearner::SemiMDPRewardListener)
smartptr(rlearner::AdaptiveParameterFromNStepsCalculator)
smartptr(rlearner::AdaptiveParameterFromNEpisodesCalculator)
smartptr(rlearner::AdaptiveParameterFromAverageRewardCalculator)

// tdlearner.h
make_director(rlearner::TDLearner)
make_director(rlearner::QLearner)
make_director(rlearner::SarsaLearner)
make_director(rlearner::TDGradientLearner)
make_director(rlearner::TDResidualLearner)
make_director(rlearner::QAverageTDErrorLearner)
make_director(rlearner::QAverageTDVarianceLearner)
smartptr(rlearner::TDLearner)
smartptr(rlearner::QLearner)
smartptr(rlearner::SarsaLearner)
smartptr(rlearner::TDGradientLearner)
smartptr(rlearner::TDResidualLearner)
smartptr(rlearner::QAverageTDErrorLearner)
smartptr(rlearner::QAverageTDVarianceLearner)

// agentcontroller.h
make_director(rlearner::AgentController)
make_director(rlearner::AgentStatisticController)
make_director(rlearner::DeterministicController)
smartptr(rlearner::AgentController)
smartptr(rlearner::AgentStatisticController)
smartptr(rlearner::DeterministicController)

// policies.h
make_director(rlearner::QGreedyPolicy)
make_director(rlearner::ActionDistribution)
make_director(rlearner::SoftMaxDistribution)
make_director(rlearner::AbsoluteSoftMaxDistribution)
make_director(rlearner::GreedyDistribution)
make_director(rlearner::EpsilonGreedyDistribution)
make_director(rlearner::StochasticPolicy)
make_director(rlearner::QStochasticPolicy)
make_director(rlearner::VMStochasticPolicy)
smartptr(rlearner::QGreedyPolicy)
smartptr(rlearner::ActionDistribution)
smartptr(rlearner::SoftMaxDistribution)
smartptr(rlearner::AbsoluteSoftMaxDistribution)
smartptr(rlearner::GreedyDistribution)
smartptr(rlearner::EpsilonGreedyDistribution)
smartptr(rlearner::StochasticPolicy)
smartptr(rlearner::QStochasticPolicy)
smartptr(rlearner::VMStochasticPolicy)

// mdp.h
make_director(rlearner::SemiMDPSender)
make_director(rlearner::SemiMarkovDecisionProcess)
make_director(rlearner::HierarchicalSemiMarkovDecisionProcess)
smartptr(rlearner::SemiMDPSender)
smartptr(rlearner::SemiMarkovDecisionProcess)
smartptr(rlearner::HierarchicalSemiMarkovDecisionProcess)

// agent.h
make_director(rlearner::Agent)
smartptr(rlearner::Agent)

// agentlogger.h
//make_director(rlearner::AgentLogger) 				-- uses virtual inheritance
make_director(rlearner::EpisodeOutput)
make_director(rlearner::EpisodeMatlabOutput)
make_director(rlearner::EpisodeOutputStateChanged)
make_director(rlearner::StateOutput)
make_director(rlearner::ActionOutput)
smartptr(rlearner::AgentLogger)
smartptr(rlearner::EpisodeOutput)
smartptr(rlearner::EpisodeMatlabOutput)
smartptr(rlearner::EpisodeOutputStateChanged)
smartptr(rlearner::StateOutput)
smartptr(rlearner::ActionOutput)

// rewardmodel.h
make_director(rlearner::FeatureStateRewardFunction)
make_director(rlearner::AgentLoFeatureRewardModelgger)
make_director(rlearner::FeatureRewardModel)
make_director(rlearner::FeatureStateRewardModel)
make_director(rlearner::RewardEpisode)
make_director(rlearner::RewardHistorySubset)
make_director(rlearner::RewardLogger)
make_director(rlearner::SemiMDPLastNRewardFunction)
smartptr(rlearner::FeatureStateRewardFunction)
smartptr(rlearner::AgentLoFeatureRewardModelgger)
smartptr(rlearner::FeatureRewardModel)
smartptr(rlearner::FeatureStateRewardModel)
smartptr(rlearner::RewardEpisode)
smartptr(rlearner::RewardHistorySubset)
smartptr(rlearner::RewardLogger)
smartptr(rlearner::SemiMDPLastNRewardFunction)

// environmentmodel.h
make_director(rlearner::EnvironmentModel)
smartptr(rlearner::EnvironmentModel)

// AgentSimulator.h
//make_director(rlearner::AgentSimulator)						--unnecessary
smartptr(rlearner::AgentSimulator)

// transitionfunction.h
make_director(rlearner::TransitionFunction)
make_director(rlearner::ExtendedActionTransitionFunction)
make_director(rlearner::ComposedTransitionFunction)
make_director(rlearner::ContinuousTimeTransitionFunction)
make_director(rlearner::ContinuousTimeAndActionTransitionFunction)
make_director(rlearner::LinearActionContinuousTimeTransitionFunction)
make_director(rlearner::DynamicLinearContinuousTimeModel)
make_director(rlearner::TransitionFunctionEnvironment)
make_director(rlearner::TransitionFunctionFromStochasticModel)
make_director(rlearner::QFunctionFromTransitionFunction)
make_director(rlearner::ContinuousTimeQFunctionFromTransitionFunction)

smartptr(rlearner::TransitionFunction)
smartptr(rlearner::ExtendedActionTransitionFunction)
smartptr(rlearner::ComposedTransitionFunction)
smartptr(rlearner::ContinuousTimeTransitionFunction)
smartptr(rlearner::ContinuousTimeAndActionTransitionFunction)
smartptr(rlearner::LinearActionContinuousTimeTransitionFunction)
smartptr(rlearner::TransitionFunctionEnvironment)
smartptr(rlearner::DynamicLinearContinuousTimeModel)
smartptr(rlearner::TransitionFunctionFromStochasticModel)
smartptr(rlearner::QFunctionFromTransitionFunction)
smartptr(rlearner::ContinuousTimeQFunctionFromTransitionFunction)

// rewardfunction.h
make_director(rlearner::RewardFunction)
make_director(rlearner::FeatureRewardFunction)
make_director(rlearner::StateReward)
make_director(rlearner::ConstantReward)
make_director(rlearner::ZeroReward)
make_director(rlearner::RewardFunctionFromValueFunction)
make_director(rlearner::FeatureRewardFunctionFromValueFunction)
smartptr(rlearner::RewardFunction)
smartptr(rlearner::FeatureRewardFunction)
smartptr(rlearner::StateReward)
smartptr(rlearner::ConstantReward)
smartptr(rlearner::ZeroReward)
smartptr(rlearner::RewardFunctionFromValueFunction)
smartptr(rlearner::FeatureRewardFunctionFromValueFunction)

// gradientfunction.h
make_director(rlearner::AdaptiveEtaCalculator)
make_director(rlearner::IndividualEtaCalculator)
make_director(rlearner::VarioEta)
make_director(rlearner::GradientUpdateFunction)
make_director(rlearner::GradientFunction)
smartptr(rlearner::AdaptiveEtaCalculator)
smartptr(rlearner::IndividualEtaCalculator)
smartptr(rlearner::VarioEta)
smartptr(rlearner::GradientUpdateFunction)
smartptr(rlearner::GradientFunction)

// vfunction.h
make_director(rlearner::AbstractVFunction)
make_director(rlearner::ZeroVFunction)
make_director(rlearner::VFunctionSum)
//make_director(rlearner::DivergentVFunctionException) 			-- unnecessary
make_director(rlearner::GradientVFunction)
make_director(rlearner::VFunctionInputDerivationCalculator)
make_director(rlearner::VFunctionNumericInputDerivationCalculator)
make_director(rlearner::FeatureVFunction)
make_director(rlearner::VTable)
//make_director(rlearner::RewardAsVFunction)					-- unnecessary
smartptr(rlearner::AbstractVFunction)
smartptr(rlearner::ZeroVFunction)
smartptr(rlearner::VFunctionSum)
//smartptr(rlearner::DivergentVFunctionException)			-- exception class
smartptr(rlearner::GradientVFunction)
smartptr(rlearner::VFunctionInputDerivationCalculator)
smartptr(rlearner::VFunctionNumericInputDerivationCalculator)
smartptr(rlearner::FeatureVFunction)
smartptr(rlearner::VTable)
smartptr(rlearner::RewardAsVFunction)

// qfunction.h
make_director(rlearner::AbstractQFunction)
make_director(rlearner::QFunctionSum)
//make_director(rlearner::DivergentQFunctionException)			-- unnecessary
//make_director(rlearner::GradientQFunction)
make_director(rlearner::QFunction)
make_director(rlearner::QFunctionFromStochasticModel)
make_director(rlearner::FeatureQFunction)
make_director(rlearner::ComposedQFunction)
smartptr(rlearner::AbstractQFunction)
smartptr(rlearner::QFunctionSum)
//smartptr(rlearner::DivergentQFunctionException)			-- exception class
smartptr(rlearner::GradientQFunction)
smartptr(rlearner::QFunction)
smartptr(rlearner::QFunctionFromStochasticModel)
smartptr(rlearner::FeatureQFunction)
smartptr(rlearner::ComposedQFunction)

// benchmarks/Task.h
make_director(rlearner::Task)
smartptr(rlearner::Task)

//------------------------------------------------------------------------------
//						Includes and templates
//------------------------------------------------------------------------------

// algebra.h
make_include(<Rlearner/algebra.h>)

// baseobjects.h
make_include(<Rlearner/baseobjects.h>)

// parameters.h
make_include(<Rlearner/parameters.h>)
make_template(ParamStrPair) std::pair<ParameterObject*, std::string>; 
make_template(ParamStrPairStdList) std::list<std::pair<ParameterObject*, std::string> >;

// learndataobject.h
make_include(<Rlearner/learndataobject.h>)

// state.h
make_include(<Rlearner/state.h>)
//make_template(VectorVectorRealType) std::vector<std::vector<RealType> >;	-- protected
//make_template(VectorVectorInt) std::vector<std::vector<int> >;			-- protected
//make_template(BoolStdVector) std::vector<std::vector<RealType> >;			-- protected

// stateproperties.h
make_include(<Rlearner/stateproperties.h>)

// statecollection.h
make_include(<Rlearner/statecollection.h>)

// statemodifier.h
make_include(<Rlearner/statemodifier.h>)
make_template(StateCollectionImplStdList) std::list<StateCollectionImpl*>;

// StateModifierList.h
make_include(<Rlearner/StateModifierList.h>)
make_template(StateModListStdSet) std::set<StateModifierList*>;
make_template(StateModStateStdPair) std::pair<StateModifier *, State *>;
make_template(IntPairModifierStateStdMap) std::map<int, std::pair<StateModifier *, State *>*>;

// discretizer.h
make_include(<Rlearner/discretizer.h>)

// action.h
make_include(<Rlearner/action.h>)
make_template(ActionSPtrStdVector) std::vector<shared_ptr<Action> >;
make_template(ActionDataStdMap) std::map<Action*, ActionData*>;

// ActionEncoder.h
make_include(<Rlearner/ActionEncoder.h>)

// agentlistener.h
make_include(<Rlearner/agentlistener.h>)

// tdlearner.h
make_include(<Rlearner/tdlearner.h>)

// agentcontroller.h
make_include(<Rlearner/agentcontroller.h>)

// policies.h
make_include(<Rlearner/policies.h>)

// mdp.h
make_include(<Rlearner/mdp.h>)

// agent.h
make_include(<Rlearner/agent.h>)

// agentlogger.h
make_include(<Rlearner/agentlogger.h>)

// rewardmodel.h
make_include(<Rlearner/rewardmodel.h>)
make_template(IntRealTypeStdMap) std::map<int, RealType>;
//make_template(Feature2DArray) MyArray2D<FeatureMap *>; 				-- protected

// environmentmodel.h
make_include(<Rlearner/environmentmodel.h>)

// AgentSimulator.h
make_include(<Rlearner/AgentSimulator.h>)
//make_template(StateModifierStateEntryStdMap) std::map<StateModifier*, StateEntry>;
//make_template(StatePropertiesStateEntryStdMap) std::map<StateProperties*, StateEntry>;

// transitionfunction.h
make_include(<Rlearner/transitionfunction.h>)
make_template(TransitionFunctionStdList) std::list<TransitionFunction *>;

// rewardfunction.h
make_include(<Rlearner/rewardfunction.h>)

// gradientfunction.h
make_include(<Rlearner/gradientfunction.h>)

// vfunction.h
make_include(<Rlearner/vfunction.h>)

// qfunction.h
make_include(<Rlearner/qfunction.h>)

// Task.h
make_include(<Rlearner/Task.h>)

//------------------------------------------------------------------------------
//						Missing type traits
//------------------------------------------------------------------------------

%make_traits(rlearner::StateModifierList);
%make_traits(rlearner::ActionData)
%make_traits(rlearner::Action)

#endif // SWIG___RLEARNER_BASE___I

