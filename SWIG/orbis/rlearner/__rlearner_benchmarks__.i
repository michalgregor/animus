#ifndef SWIG___RLEARNER_BENCHMARKS___I
#define SWIG___RLEARNER_BENCHMARKS___I

#ifndef __SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") __rlearner_benchmarks__
	%feature("autodoc", "3");
#endif

%include "../system.i"
%include "std_list.i"
%include "std_map.i"
%include "std_set.i"
%include "std_vector.i"
%include "std_pair.i"

import_module("rlearner.i")
import_module("../systematic/__algebra__.i")
import_module("../systematic/__systematic__.i")
import_module("__rlearner_base__.i")

namespace systematic {}
namespace annalyst {}
namespace rlearner {}

using namespace systematic;
using namespace annalyst;
using namespace rlearner;

//------------------------------------------------------------------------------
//					Director and smartptr declarations
//------------------------------------------------------------------------------

// benchmarks/acrobotmodel.h
smartptr(rlearner::AcroBotModel)
smartptr(rlearner::AcroBotRewardFunction)
smartptr(rlearner::AcroBotHeightRewardFunction)
smartptr(rlearner::AcroBotVelocityRewardFunction)
smartptr(rlearner::AcroBotExpRewardFunction)

// benchmarks/cartpolemodel.h
smartptr(rlearner::CartPoleModel)
smartptr(rlearner::CartPoleRewardFunction)
smartptr(rlearner::CartPoleHeightRewardFunction)

// benchmarks/GridWorld.h
make_director(rlearner::GridWorld)
smartptr(rlearner::GridWorld)

// benchmarks/GridWorldModel.h
make_director(rlearner::GridWorldModel)
make_director(rlearner::GridWorldState_Local4)
make_director(rlearner::GridWorldState_Local4X)
make_director(rlearner::GridWorldState_Local8)
make_director(rlearner::GridWorldDiscreteState_Global)
make_director(rlearner::GridWorldDiscreteState_Local)
make_director(rlearner::GridWorldDiscreteState_SmallLocal)
make_director(rlearner::GridWorldAction)
make_director(rlearner::RaceTrackDiscreteState)
//make_director(rlearner::RaceTrack)						-- non-polymorphic

smartptr(rlearner::GridWorldModel)
smartptr(rlearner::GridWorldState_Local4)
smartptr(rlearner::GridWorldState_Local4X)
smartptr(rlearner::GridWorldState_Local8)
smartptr(rlearner::GridWorldDiscreteState_Global)
smartptr(rlearner::GridWorldDiscreteState_Local)
smartptr(rlearner::GridWorldDiscreteState_SmallLocal)
smartptr(rlearner::GridWorldAction)
smartptr(rlearner::RaceTrackDiscreteState)
smartptr(rlearner::RaceTrack)

// benchmarks/multipolemodel.h
smartptr(rlearner::MultiPoleDiscreteState)
smartptr(rlearner::MultiPoleFailedState)
smartptr(rlearner::MultiPoleModel)
smartptr(rlearner::MultiPoleContinuousReward)
smartptr(rlearner::MultiPoleAction)
smartptr(rlearner::MultiPoleController)
smartptr(rlearner::MultiPoleDiscreteController)

// benchmarks/pendulummodel.h
smartptr(rlearner::PendulumModel)
smartptr(rlearner::PendulumRewardFunction)
smartptr(rlearner::PendulumUpTimeCalculator)

// benchmarks/Task_Charging.h
%rename(TaskCharging_ActionType) rlearner::rlearner_charging::ActionType;
%rename(TaskCharging_Location) rlearner::rlearner_charging::Location;
%rename(TaskCharging_ManuAction) rlearner::rlearner_charging::ManuAction;
%rename(TaskCharging_ManuState) rlearner::rlearner_charging::ManuState;

//smartptr(rlearner::rlearner_charging::ActionType) 			-- enum class
//smartptr(rlearner::rlearner_charging::Location)				-- enum class
smartptr(rlearner::rlearner_charging::ManuAction)
smartptr(rlearner::rlearner_charging::ManuState)
smartptr(rlearner::Task_Charging)

// benchmarks/Task_Complex.h
%rename(TaskComplex_ActionType) rlearner::rlearner_complex::ActionType;
%rename(TaskComplex_Location) rlearner::rlearner_complex::Location;
%rename(TaskComplex_ManuAction) rlearner::rlearner_complex::ManuAction;
%rename(TaskComplex_ManuState) rlearner::rlearner_complex::ManuState;

//smartptr(rlearner::rlearner_complex::ActionType) 				-- enum class
//smartptr(rlearner::rlearner_complex::Location)				-- enum class
smartptr(rlearner::rlearner_complex::ManuAction)
smartptr(rlearner::rlearner_complex::ManuState)
smartptr(rlearner::Task_Complex)

// benchmarks/Task_DeepSea.h
smartptr(rlearner::Task_DeepSea_TimeReward)
smartptr(rlearner::Task_DeepSea)

// benchmarks/Task_GridWorld.h
make_director(rlearner::Task_GridWorld)
smartptr(rlearner::Task_GridWorld)

// benchmarks/Task_HungryThirsty.h
smartptr(rlearner::HugryThirstyAction)
smartptr(rlearner::HugryThirstyGridWorldModel)
smartptr(rlearner::Task_HugryThirsty)

// benchmarks/Task_ShortcutMaze.h
smartptr(rlearner::Task_ShortcutMaze)

// benchmarks/Task_SingleProduct.h
%rename(TaskComplex_ActionType) rlearner::rlearner_complex::ActionType;
%rename(TaskComplex_Location) rlearner::rlearner_complex::Location;
%rename(TaskComplex_ManuAction) rlearner::rlearner_complex::ManuAction;
%rename(TaskComplex_ManuState) rlearner::rlearner_complex::ManuState;

//smartptr(rlearner::rlearner_singleproduct::ActionType) 		-- enum class
//smartptr(rlearner::rlearner_singleproduct::Location)			-- enum class
smartptr(rlearner::rlearner_singleproduct::ManuAction)
smartptr(rlearner::rlearner_singleproduct::ManuState)
smartptr(rlearner::Task_SingleProduct)

// benchmarks/taxidomain.h
smartptr(rlearner::TaxiDomain)
smartptr(rlearner::PickupAction)
smartptr(rlearner::PutdownAction)
smartptr(rlearner::TaxiHierarchicalBehaviour)
smartptr(rlearner::TaxiIsTargetDiscreteState)

//------------------------------------------------------------------------------
//						Includes and templates
//------------------------------------------------------------------------------

// benchmarks/acrobotmodel.h
make_include(<Rlearner/benchmarks/acrobotmodel.h>)

// benchmarks/cartpolemodel.h
make_include(<Rlearner/benchmarks/cartpolemodel.h>)

// benchmarks/GridWorld.h
make_include(<Rlearner/benchmarks/GridWorld.h>)
%template(CharStdSet) std::set<char>;

// benchmarks/GridWorldModel.h
make_include(<Rlearner/benchmarks/GridWorldModel.h>)
%template(CharRealTypeStdMap) std::map<char, RealType>;

// benchmarks/multipolemodel.h
make_include(<Rlearner/benchmarks/multipolemodel.h>)

// benchmarks/pendulummodel.h
make_include(<Rlearner/benchmarks/pendulummodel.h>)

// benchmarks/Task_Charging.h
make_include(<Rlearner/benchmarks/Task_Charging.h>)

// benchmarks/Task_Complex.h
make_include(<Rlearner/benchmarks/Task_Complex.h>)

// benchmarks/Task_DeepSea.h
make_include(<Rlearner/benchmarks/Task_DeepSea.h>)

// benchmarks/Task_GridWorld.h
make_include(<Rlearner/benchmarks/Task_GridWorld.h>)

// benchmarks/Task_HungryThirsty.h
make_include(<Rlearner/benchmarks/Task_HungryThirsty.h>)

// benchmarks/Task_ShortcutMaze.h
make_include(<Rlearner/benchmarks/Task_ShortcutMaze.h>)

// benchmarks/Task_SingleProduct.h
make_include(<Rlearner/benchmarks/Task_SingleProduct.h>)

// benchmarks/taxidomain.h
make_include(<Rlearner/benchmarks/taxidomain.h>)

#endif // SWIG___RLEARNER_BENCHMARKS___I

