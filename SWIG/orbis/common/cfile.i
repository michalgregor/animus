#include <cstdio>

namespace std {

	FILE* fopen(const char* filename, const char* mode);
	int fclose(FILE* stream);

} //namespace std
