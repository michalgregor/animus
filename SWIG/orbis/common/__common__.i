#ifndef __SWIG___COMMON___I__
#define __SWIG___COMMON___I__

#ifndef __SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") __common__
#endif

%{
	#include <Systematic/system.h>
	#include <Systematic/math/RealType.h>
	#include <Systematic/utils/VectorStreamOperator.h>
	using namespace systematic;
%}

%include "std_vector.i"
%include "carrays.i"
%include "../utils/str_repr.i"
%include "../utils/smartptr.i"

%include <Systematic/configure.h>
%include <Systematic/math/RealType.h>
#define _r

%define add_vector_def(VECTOR_TYPE, VECTOR_NAME)
	smartptr(VECTOR_TYPE)
	%template(VECTOR_NAME) VECTOR_TYPE;
	%extend VECTOR_TYPE {__STR__()}
%enddef

add_vector_def(std::vector<int>, vectorInt)
add_vector_def(std::vector<unsigned int>, vectorUInt)
add_vector_def(std::vector<RealType>, vectorReal)

add_vector_def(std::vector<std::vector<int> >, vectorInt2D)
add_vector_def(std::vector<std::vector<unsigned int> >, vectorUInt2D)
add_vector_def(std::vector<std::vector<RealType> >, vectorReal2D)

//%array_class(int, arrayInt);				-- at this point, we don't really need plain arrays
//%array_class(unsigned int, arrayUInt);	-- at this point, we don't really need plain arrays
//%array_class(RealType, arrayReal);		-- at this point, we don't really need plain arrays

%include "cfile.i"
%include "std_fstream.i"
%include "std_rand.i"

#endif // __SWIG___COMMON___I__

