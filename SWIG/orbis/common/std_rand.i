%{
	#include <cstdlib>
%}

namespace std {

extern const int RandMax = RAND_MAX;

int rand(void);
void srand (unsigned int seed);

} // namespace std

%{

namespace std {

extern const int RandMax = RAND_MAX;

} // namespace std

%}
