%{
	#include <iostream>
	#include <fstream>

	using std::basic_filebuf;
%}

%include "std_iostream.i"
%include "../utils/smartptr.i"

namespace std {

typedef basic_ostream<char> ostream;
typedef basic_istream<char> istream;

typedef basic_ofstream<char> ofstream;
typedef basic_ifstream<char> ifstream;

typedef basic_fstream<char> fstream;
typedef basic_iostream<char> iostream;

//--------------------------------------------------------
//						basic_ostream
//--------------------------------------------------------
template<class charT, class traits = char_traits<charT> >
class basic_ostream {
protected:
	basic_ostream& operator=(const basic_ostream& rhs) = delete;
	basic_ostream(const basic_ostream& rhs) = delete;
};

//--------------------------------------------------------
//						basic_istream
//--------------------------------------------------------
template<class charT, class traits = char_traits<charT> >
class basic_istream {
protected:
	basic_istream& operator=(const basic_istream& rhs) = delete;
	basic_istream(const basic_istream& rhs) = delete;
};

//--------------------------------------------------------
//						ofstream
//--------------------------------------------------------

template<class charT, class traits = char_traits<charT> >
class basic_ofstream: public std::basic_ostream<charT, traits> {
public:
	typedef charT char_type;
	typedef typename traits::int_type int_type;
	typedef typename traits::pos_type pos_type;
	typedef typename traits::off_type off_type;
	typedef traits traits_type;

	basic_ofstream();
	explicit basic_ofstream(const char* s, ios_base::openmode mode =
	        ios_base::out);
	explicit basic_ofstream(const string& s, ios_base::openmode mode =
	        ios_base::out);
	basic_ofstream(const basic_ofstream& rhs) = delete;
	//basic_ofstream(basic_ofstream&& rhs);

	basic_ofstream& operator=(const basic_ofstream& rhs) = delete;
	//basic_ofstream& operator=(basic_ofstream&& rhs);
	//void swap(basic_ofstream& rhs);

	basic_filebuf<charT, traits>* rdbuf() const;
	bool is_open() const;
	void open(const char* s, ios_base::openmode mode = ios_base::out);
	void open(const string& s, ios_base::openmode mode = ios_base::out);
	void close();

private:
	basic_filebuf<charT, traits> sb; // exposition only
};

//--------------------------------------------------------
//						ifstream
//--------------------------------------------------------

template<class charT, class traits = char_traits<charT> >
class basic_ifstream: public basic_istream<charT, traits> {
public:
	typedef charT char_type;
	typedef typename traits::int_type int_type;
	typedef typename traits::pos_type pos_type;
	typedef typename traits::off_type off_type;
	typedef traits traits_type;

	basic_ifstream();
	explicit basic_ifstream(const char* s, ios_base::openmode mode =
	        ios_base::in);
	explicit basic_ifstream(const string& s, ios_base::openmode mode =
	        ios_base::in);
	basic_ifstream(const basic_ifstream& rhs) = delete;
	//basic_ifstream(basic_ifstream&& rhs);

	basic_ifstream& operator=(const basic_ifstream& rhs) = delete;
	//basic_ifstream& operator=(basic_ifstream&& rhs);
	//void swap(basic_ifstream& rhs);

	basic_filebuf<charT, traits>* rdbuf() const;
	bool is_open() const;
	void open(const char* s, ios_base::openmode mode = ios_base::in);
	void open(const string& s, ios_base::openmode mode = ios_base::in);
	void close();
	
private:
	basic_filebuf<charT, traits> sb; // exposition only
};

//--------------------------------------------------------
//						fstream
//--------------------------------------------------------

template<class charT, class traits = char_traits<charT> >
class basic_fstream: public basic_iostream<charT, traits> {
public:
	typedef charT char_type;
	typedef typename traits::int_type int_type;
	typedef typename traits::pos_type pos_type;
	typedef typename traits::off_type off_type;
	typedef typename traits traits_type;

	basic_fstream();
	explicit basic_fstream(const char* s,
	        ios_base::openmode mode = ios_base::in | ios_base::out);
	explicit basic_fstream(const string& s,
	        ios_base::openmode mode = ios_base::in | ios_base::out);
	basic_fstream(const basic_fstream& rhs) = delete;
	//basic_fstream(basic_fstream&& rhs);

	basic_fstream& operator=(const basic_fstream& rhs) = delete;
	//basic_fstream& operator=(basic_fstream&& rhs);
	//void swap(basic_fstream& rhs);

	basic_filebuf<charT, traits>* rdbuf() const;
	bool is_open() const;
	void open(const char* s,
	        ios_base::openmode mode = ios_base::in | ios_base::out);
	void open(const string& s,
	        ios_base::openmode mode = ios_base::in | ios_base::out);
	void close();
	
private:
	basic_filebuf<charT, traits> sb; // exposition only
};

} //namespace std

smartptr(std::basic_istream<char>)
smartptr(std::basic_ostream<char>)
smartptr(std::basic_iostream<char>)

%template(ostream) std::basic_ostream<char>;
%template(istream) std::basic_istream<char>;
%template(iostream) std::basic_iostream<char>;

smartptr(std::basic_ofstream<char>)
smartptr(std::basic_ifstream<char>)
smartptr(std::basic_fstream<char>)

%template(ofstream) std::basic_ofstream<char>;
%template(ifstream) std::basic_ifstream<char>;
%template(fstream) std::basic_fstream<char>;

