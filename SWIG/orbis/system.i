#ifndef __SWIG__SYSTEM_I__
#define __SWIG__SYSTEM_I__

%include "std_string.i"
%include "std_pair.i"
%include "std_list.i"
%include "attribute.i"
%include "utils/make_include.i"
%include "utils/make_template.i"
%include "utils/smartptr.i"
%include "utils/make_director.i"
%include "utils/buffer_protocol.i"
%include "utils/str_repr.i"

import_module("common/__common__.i")

%define SWIGError(MSG, SWIG_EXCEPTION, CPP_EXCEPTION)
	SWIG_exception(SWIG_EXCEPTION, MSG);
%enddef

%define SYSError(MSG, SWIG_EXCEPTION, CPP_EXCEPTION)
	throw CPP_EXCEPTION(MSG);
%enddef

%include "utils/operators_python.i"
%include "utils/dynamic_cast.i"
%include "utils/nowarnings.i"
%include "utils/iterator.i"
%include "utils/make_traits.i"
%include "utils/container.i"
%include "utils/catch_exceptions.i"

#endif // __SWIG__SYSTEM_I__

