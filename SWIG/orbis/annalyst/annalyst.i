#ifndef __SWIG_ANNALYST_I__
#define __SWIG_ANNALYST_I__

#if !__SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") annalyst
	%feature("autodoc", "3");

	%pythoncode %{
		import orbis.systematic;
		from orbis.__annalyst__ import *;
	%}
#endif

#endif // __SWIG_ANNALYST_I__

