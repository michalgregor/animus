# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pylab as plt
import orbis.annalyst

def __hitmapPlot__(matrix, max_weight=None, ax=None):
    """Draw the SOM hitmap."""
    if ax is None:
        plt.figure()
        ax = plt.gca()
        
    if not max_weight:
        max_weight = np.abs(matrix).max()

    ax.set_aspect('equal', 'box')  

    for (r,c), hits in np.ndenumerate(matrix):
        # Set the size of the box.
        if hits == 0: size = 0; fontcolor = 'black'
        else: size = max((hits + 4) / (max_weight + 4), 0.2); fontcolor = 'white'
        # Set font size.
        fontsize = min(size*4+10, 12)
        
        # Draw the inner rectangle.
        rect = plt.Rectangle(
            [c - size / 2, r - size / 2], size, size,
            facecolor='gray', edgecolor='gray'
        ); ax.add_patch(rect)
        # Draw the number of hits.
        ax.text(
            c, r, int(hits),
            color=fontcolor,
            fontsize=fontsize,
            horizontalalignment='center', verticalalignment='center')
        # Draw the outer rectangle.
        size = 1
        rect = plt.Rectangle(
            [c - size / 2, r - size / 2], size, size,
            facecolor='none', edgecolor='black'
        ); ax.add_patch(rect)
        
    ax.set_xlabel("cols")
    ax.set_ylabel("rows")
    
    ax.set_yticks([i for i in range(0, matrix.shape[0] + 2, 1)])
    ax.set_xticks([i for i in range(0, matrix.shape[1] + 2, 1)])

    # Scale the axes.
    ax.autoscale_view(tight=True)
    ax.invert_yaxis()

def __umatrixPlot__(umatrix, ax=None):
    """Draw the SOM umatrix."""
    if ax is None:
        plt.figure()
        ax = plt.gca()    
    
    ax.imshow(umatrix, cmap='Blues', interpolation="nearest")
    
    rows = int((len(umatrix) + 1) / 2); cols = int((len(umatrix[0]) + 1) / 2)
    size = 0.2
    
    ax.set_xticks([i for i in range(0, umatrix.shape[1] + 2, 2)])
    ax.set_xticklabels([int(i/2) for i in ax.get_xticks()])
    
    ax.set_yticks([i for i in range(0, umatrix.shape[0] + 2, 2)])
    ax.set_yticklabels([int(i/2) for i in ax.get_yticks()])  
    
    for r in range(rows):
        for c in range(cols):
            circ = plt.Circle(
                [c*2, r*2], size,
                facecolor='blue', edgecolor='black'
            ); ax.add_patch(circ)
    
    ax.autoscale_view(tight=True)
    ax.set_xlabel("cols")
    ax.set_ylabel("rows")

def __hitmapCategoryPlot__(som, data, labels=None, colors=None, ax=None,
                       return_positions=False, label_names=None,
                       color_names=True, fontsize=10, always_draw_points=False,
                       *args, **kwargs):
    """
        Draw the SOM hitmap category plot.
        
        return_positions: If True, the function also returns arrays x, y with
        positions in which the individual data points were plotted.
    """
    
    top = som.topology()
    rows = top.rows()
    cols = top.cols()
    
    if ax is None:
        plt.figure()
        ax = plt.gca()
   
    ax.set_aspect('equal', 'box')  
    ax.set_xticks([i for i in range(cols)])
    ax.set_yticks([i for i in range(rows)])
    
    ax.set_xlabel("cols")
    ax.set_ylabel("rows")
    
    x = np.zeros([len(data)])
    y = np.zeros([len(data)])
    
    for r in range(rows):
        for c in range(cols):
            # Draw the outer rectangle.
            size = 1
            rect = plt.Rectangle(
                [c - size / 2, r - size / 2], size, size,
                facecolor='none', edgecolor='black'
            ); ax.add_patch(rect) 
    
    for i, xx in enumerate(data):
        winnerPos = top.computeGridPosition(som.predict(xx.reshape([1, -1]))[0])
                
        y[i] = winnerPos[0] + np.random.uniform(-0.45, 0.45)
        x[i] = winnerPos[1] + np.random.uniform(-0.45, 0.45)

    if len(args) > 0:
        s = args[0]
        del args[0]
    elif 's' in kwargs:
        s = kwargs['s']
        del kwargs['s']
    else:
        s = 30
        
    if (not always_draw_points) and (not label_names is None):
        s = 0
    
    if not labels is None:
        if (not colors is None):
            sc = ax.scatter(x, y, s, c=[colors[l] for l in labels], *args, **kwargs)
        else:
            sc = ax.scatter(x, y, s, *args, **kwargs)
        
        if not label_names is None:
            if color_names and (not colors is None):
                for xx, yy, l in zip(x, y, labels):
                    ax.annotate(s=label_names[l], xy=(xx, yy), color=colors[l], ha="center", size=fontsize)
            else:
                for xx, yy, l in zip(x, y, labels):
                    ax.annotate(s=label_names[l], xy=(xx, yy), ha="center", size=fontsize)

    # Scale the axes.
    ax.autoscale_view(tight=True)
    ax.invert_yaxis()
    
    if return_positions:
        return sc, x, y
    else:
        return sc

def __plotSomSpace__(som, data_points=None, labels=None, colors=None, ax=None, labelClusters=True):
    """ 
    Plots the input space of the SOM with positions of the neurons
    and the topology connections. This only works for SOMs with 2 inputs.
    """
    
    if som.numInputs() != 2:
        raise RuntimeError("This method only works for SOMs with 2 inputs.")
    
    top = som.topology()
    if top.getTopologyType() != orbis.annalyst.TOT_rectangular:
        raise RuntimeError("This method only supports the rectangular topology.")
    
    clusterWeights = som.weights()
    
    if ax is None:
        plt.figure()
        ax = plt.gca()
        
    if data_points is not None:
        if labels is not None and colors is not None:
            ax.scatter(data_points[:, 0], data_points[:, 1], s=30,
                       c=[colors[l] for l in labels])
        else:
            ax.scatter(data_points[:, 0], data_points[:, 1], s=30)
    
    for r in range(top.rows()):
        for c in range(top.cols()):
            thisAN = top.computeNeuronIndex(r, c)
            
            if(r + 1 < top.rows()):
                otherAN = top.computeNeuronIndex(r+1, c)         
                ax.plot(
                    [clusterWeights[thisAN][0], clusterWeights[otherAN][0]],
                    [clusterWeights[thisAN][1], clusterWeights[otherAN][1]],
                    'o-r',
                    markerfacecolor='r',
                    markersize=10
                )
                
            if(c + 1 < top.cols()):
                otherAN = top.computeNeuronIndex(r, c+1)
                ax.plot(
                    [clusterWeights[thisAN][0], clusterWeights[otherAN][0]],
                    [clusterWeights[thisAN][1], clusterWeights[otherAN][1]],
                    'o-r',
                    markerfacecolor='r',
                    markersize=10
                )
                
            if(r + 1 < top.rows() and c + 1 < top.cols()):
                otherAN = top.computeNeuronIndex(r+1, c+1)
                ax.plot(
                    [clusterWeights[thisAN][0], clusterWeights[otherAN][0]],
                    [clusterWeights[thisAN][1], clusterWeights[otherAN][1]],
                    'o-r',
                    markerfacecolor='r',
                    markersize=9
                )
                
            if(r > 0 and c + 1 < top.cols()):
                otherAN = top.computeNeuronIndex(r-1, c+1)
                ax.plot(
                    [clusterWeights[thisAN][0], clusterWeights[otherAN][0]],
                    [clusterWeights[thisAN][1], clusterWeights[otherAN][1]],
                    'o-r',
                    markerfacecolor='r',
                    markersize=9
                )
                
            if(labelClusters):
                ax.text(clusterWeights[thisAN][0]+0.3, clusterWeights[thisAN][1]-0.3,
                    '({}, {})'.format(r, c),
                    bbox=dict(facecolor='white', alpha=0.75),
                    fontsize=10
                )

    ax.grid(True)
    ax.set_xlabel(r"$x_1$")
    ax.set_ylabel(r"$x_2$")
    ax.autoscale_view(tight=True)
