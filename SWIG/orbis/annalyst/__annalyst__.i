#ifndef __SWIG___ANNALYST___I__
#define __SWIG___ANNALYST___I__

#if !__SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") __annalyst__
	%feature("autodoc", "3");

	%pythoncode %{
		from orbis.som_plot import __hitmapPlot__, __umatrixPlot__, __hitmapCategoryPlot__, __plotSomSpace__;
	%}
#endif

%include "../system.i"
%include "std_pair.i"
import_module("../systematic/__algebra__.i")
import_module("../systematic/__systematic__.i")

using namespace systematic;
using namespace annalyst;

//------------------------------------------------------------------------------
//					Director and smartptr declarations
//------------------------------------------------------------------------------

// Classifier.h
make_director(annalyst::Classifier)
smartptr(annalyst::Classifier)

// Topology.h
//make_director(annalyst::Neighbour)						-- non-virtual
//make_director(annalyst::Neighbourhood)					-- non-virtual
//make_director(annalyst::Topology)							-- non-virtual
//smartptr(annalyst::Neighbour)
smartptr(annalyst::Neighbourhood)
smartptr(annalyst::Topology)

// Network_SOM.h
make_director(annalyst::Network_SOM)
smartptr(annalyst::Network_SOM)

// Network_HSOM.h
make_director(annalyst::Network_HSOM)
smartptr(annalyst::Network_HSOM)

// Forecaster.h
make_director(annalyst::Forecaster)
smartptr(annalyst::Forecaster)

//------------------------------------------------------------------------------
//						Includes and templates
//------------------------------------------------------------------------------

// Classifier.h
make_include(<Annalyst/Classifier.h>)

// Topology.h
make_include(<Annalyst/Topology.h>)
%template(NetworkSizePair) std::pair<NetworkSize, NetworkSize>;
%swig_pair_methods(std::pair<NetworkSize, NetworkSize>)

%extend annalyst::Neighbourhood {
	%orbis_iterators(Neighbourhood)
}

// Network_SOM.h
%rename(__Network_SOM_Impl__) Network_SOM;
make_include(<Annalyst/Network_SOM.h>)

// Network_HSOM.h
make_include(<Annalyst/Network_HSOM.h>)

// Forecaster.h
make_include(<Annalyst/Forecaster.h>)
make_template(VectorRowVector) std::vector<RowVector>;

//------------------------------------------------------------------------------
//						Missing type traits
//------------------------------------------------------------------------------

%make_traits(annalyst::Neighbour);

//------------------------------------------------------------------------------
//						Python extensions
//------------------------------------------------------------------------------

#if !__SWIG_IMPORT_ONLY__

%pythoncode {
class Network_SOM(__Network_SOM_Impl__):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	def hitmap(self, data, *args, **kwargs):
	    """Draw the SOM hitmap."""
	    hitmatrix = self.computeHitMatrix(data)
	    return __hitmapPlot__(hitmatrix, *args, **kwargs)

	def umatrix(self, *args, **kwargs):
	    """Draw the SOM umatrix."""
	    umatrix = self.computeUMatrix()
	    return __umatrixPlot__(umatrix, *args, **kwargs)

	def hitmapCategory(self, *args, **kwargs):
	    """Draw the SOM hitmap category plot."""
	    return __hitmapCategoryPlot__(self, *args, **kwargs)
	
	def spacePlot(self, *args, **kwargs):
	    """
	    Plots the input space of the SOM with positions of the neurons
	    and the topology connections. This only works for SOMs with 2 inputs.
	    """
	    return __plotSomSpace__(self, *args, **kwargs)
}

#endif

#endif // __SWIG___ANNALYST___I__

