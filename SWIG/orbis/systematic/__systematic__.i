#ifndef SWIG___SYSTEMATIC___I
#define SWIG___SYSTEMATIC___I

#ifndef __SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") __systematic__
	%feature("autodoc", "3");
#endif

// import other modules
%include "../system.i"
import_module("systematic.i")
import_module("__algebra__.i")

//------------------------------------------------------------------------------
//					Director and smartptr declarations
//------------------------------------------------------------------------------

//-- main stuff --
// nothing to declare

//-- container --
// only has templates and macros

//-- detail --
// nothing to declare

//-- dynamic_typing --
// not useful in python

//-- exception --
// nothing to declare

//-- format --
// not useful in python

//-- generator --
// handled in generators.i

//-- iterator --
// nothing to declare

//-- math --
// nothing to declare

//-- pointer --
// nothing to declare

//-- range --
// nothing to declare

//-- type_id --
smartptr(systematic::StdTypeID)
smartptr(systematic::TypeID)

//-- type_traits --
// nothing to declare

//-- utils --
smartptr(systematic::ProcessIn)
smartptr(systematic::ProcessOut)
smartptr(systematic::StorageToken)

//------------------------------------------------------------------------------
//						Includes and templates
//------------------------------------------------------------------------------

//-- main stuff --

make_include(<Systematic/configure.h>)
//make_include(<Systematic/export.h>)			-- we empty-define the macros
//make_include(<Systematic/macros.h>)			-- we empty-define the macros
//make_include(<Systematic/serialization.h>)	-- only includes




//make_include(<Systematic/streams.h>)





make_include(<Systematic/system.h>)
//make_include(<Systematic/type_info.h>)		-- only templates and includes

//-- container --

//make_include(<Systematic/container/AppendContainer.h>)		-- a template
//make_include(<Systematic/container/circular_buffer.h>)		-- a template
//make_include(<Systematic/container/ContainerView.h>)			-- a template
make_include(<Systematic/container/vector_forward.h>)

//-- detail --

make_include(<Systematic/detail/EmptyObject.h>)

//-- dynamic_typing --

// not useful in python

//-- exception --

// make_include(<Systematic/exception/assert.h)				-- we empty-define the macros
// make_include(<Systematic/exception/backward.hpp)			-- not useful in python
// make_include(<Systematic/exception/OpenMPException.h)	-- not useful in python
// make_include(<Systematic/exception/systhrowassert.h)		-- we empty-define the macros
make_include(<Systematic/exception/TracedError.h>)

//-- format --
// not useful in python

//-- generator --
import_module("generators.i")

//-- iterator --

//make_include(checked_advance.h)								-- templates
//make_include(<Systematic/iterator/eigen_dense_iterator.h>)	-- not necessary in python	
//make_include(<Systematic/iterator/eigen_reverse_iterator.h>)	-- templates
//make_include(<Systematic/iterator/eigen_sparse_iterator.h>)	-- templates
//make_include(<Systematic/iterator/EmulatedIterator.h>)		-- templates
//make_include(<Systematic/iterator/EmulatedPair.h>)			-- templates
//make_include(<Systematic/iterator/iterator.h>)				-- templates
//make_include(<Systematic/iterator/MappedTypeIterator.h>)		-- templates
//make_include(<Systematic/iterator/PolyIterator_Forward.h>)	-- templates
//make_include(<Systematic/iterator/PolyIterator_Random.h>)		-- templates

//-- math --

//algebra.h 										-- handled by __algebra__
//algebra_decl.h									-- handled by __algebra__
make_include(<Systematic/math/Compare.h>)
%template(Compare) Compare<RealType, RealType, RealType>;
%template(Compare) Compare<RealType, RealType>;
%template(CompareSimply) CompareSimply<RealType, RealType>;

make_include(<Systematic/math/Constant.h>)
make_include(<Systematic/math/exp2.h>)

//fixed.h											-- difficult to interface; not necessary
//FixedArithmetic.h									-- difficult to interface; not necessary

make_include(<Systematic/math/Hypervolume.h>)

//numeric_cast.h									-- templates, not necessary in python
//RealType.h										-- handled in systematic.i

make_include(<Systematic/math/Round.h>)
%template(Round) systematic::Round<RealType>;

make_include(<Systematic/math/sign.h>)
%template(sign) systematic::sign<RealType>;

//statistics.h										-- templates, not necessary in python

//-- pointer --
//EmulatedPtr.h										-- templates
//smart_ptr.h										-- templates, declarations, handled elsewhere

//-- range --
// IterRangeContainer.h								-- templates, not necessary in python
// IterRangeRAContainer.h							-- templates, not necessary in python

//-- reporter --
//Reporter.h										-- templates
//ReporterSocket.h									-- templates

//-- serialization --
make_include(<Systematic/serialization/archive.h>)
//BoostCircularBuffer.h								-- not necessary in python
//boost_intrusive_ptr.h								-- not necessary in python
//boost_random_mt19937.h							-- not necessary in python
//fake_serialize.h									-- we empty-define the macros
//PtrSerialize.h									-- not necessary in python
//random_common.h									-- not necessary in python
//random_typenames.h								-- not necessary in python
//split.h											-- not necessary in python
//tuple.h											-- not necessary in python
//TypeID.h											-- not necessary in python

//-- type_id --
//detail											-- not necessary in python
//NoType.h											-- not necessary in python
//pair.h											-- not necessary in python
make_include(<Systematic/type_id/StdTypeID.h>)
make_include(<Systematic/type_id/TypeID.h>)
//TypeInfo.h										-- we empty-define the macros

//-- type_traits --
//DisableSpecs.h									-- not necessary in python
//has_signature.h									-- templates
//matrix_type.h										-- templates

//-- utils --
make_include(<Systematic/utils/Binary.h>)
//clone.h											-- templates; we empty-define the macros
make_include(<Systematic/utils/DataFile.h>)
make_include(<Systematic/utils/demangle.h>)
make_include(<Systematic/utils/enumdef.h>)
//EnumDefTranslator.h								-- templates
//EnumStreamOperator.h								-- templates
make_include(<Systematic/utils/function.h>)
//FunctorSocketBase.h								-- templates
//IndexSortAscending.h								-- templates
//IndexSortDescending.h								-- templates
//InnerContainer.h									-- templates
make_include(<Systematic/utils/Process.h>)
make_include(<Systematic/utils/rand.h>)
//RefCounterVirtual.h								-- not necessary in python
//Singleton.h										-- templates
make_include(<Systematic/utils/StorageToken.h>)
//StreamMask.h										-- templates
make_include(<Systematic/utils/TempIO.h>)
//ToString.h										-- templates
//tuple_for_each.h									-- templates
//VectorStreamOperator.h							-- templates

#endif // SWIG___SYSTEMATIC___I

