#ifndef SWIG___ALGEBRA___I
#define SWIG___ALGEBRA___I

#ifndef __SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") __algebra__
	%feature("autodoc", "3");
#endif

%include "../system.i"
import_module("__systematic__.i")

// numpy initialization
%init %{
	import_array();
%}

make_include(<Systematic/configure.h>)
make_include(<Systematic/math/RealType.h>)
make_include(<Systematic/math/algebra_decl.h>)

namespace systematic {

template<class ScalarType>
class Ref {
public:
	Ref& operator=(const Ref& obj) = delete;
	Ref(const Ref& obj);
	Ref(Ref&& obj);
};

%template(MatrixRefInst) Ref<Matrix>; 
%template(MatrixCRefInst) Ref<const Matrix>;

%template(RowVectorRefInst) Ref<RowVector>; 
%template(RowVectorCRefInst) Ref<const RowVector>;

} // namespace systematic

//------------------------------------------------------------------------------
//						Matrix numpy interface
//------------------------------------------------------------------------------

%define matrix_out_typemap(OBJ, RESULT)
	typedef typename std::remove_reference<
		decltype(OBJ->shareData())>::type PtrType;
	auto dataPtr(new PtrType(OBJ->shareData()));
	auto data = dataPtr->get();

	auto descr = PyArray_DescrFromType(NUMPY_REAL_TYPE);

    npy_intp dims[2] = {OBJ->rows(), OBJ->cols()};
	npy_intp strides[2];

	strides[0] = OBJ->rowStride() * sizeof(RealType);
	strides[1] = OBJ->colStride() * sizeof(RealType);

	RESULT = PyArray_NewFromDescr(&PyArray_Type, descr, 2, dims,
		strides, (void*) data, NPY_ARRAY_CARRAY, NULL);
	auto array = (PyArrayObject*) RESULT;

	auto capsule = PyCapsule_New((void*) dataPtr, "MatrixDataCapsule",
		free_matrix_data);
	PyArray_SetBaseObject(array, capsule);
%enddef

%define matrix_in_typemap(NEWOP, CLASSNAME, INPUT, RESULT, ERROR_FUNC)
	typedef typename std::remove_const<typename
		std::remove_reference<CLASSNAME>::type>::type ClassType;

	if(!PyArray_Check(INPUT) or PyArray_NDIM(INPUT) != 2)
		ERROR_FUNC("A 2-dimensional numpy array expected as input.",
			SWIG_ValueError, TracedError_InvalidArgument);

	if(PyArray_TYPE(INPUT) != NUMPY_REAL_TYPE)
		ERROR_FUNC("Wrong data type (systematic::RealType expected).",
			SWIG_TypeError, TracedError_InvalidArgument);

	if(!(PyArray_FLAGS(INPUT) & NPY_ARRAY_CARRAY))
		ERROR_FUNC("The data in the array must be aligned, contiguous and writeable.",
			SWIG_ValueError, TracedError_InvalidArgument);

	npy_intp* dims = PyArray_DIMS(INPUT);
	npy_intp* strides = PyArray_STRIDES(INPUT);
	RealType* data = (RealType*) PyArray_DATA(INPUT);
	auto itemSize = PyArray_ITEMSIZE(INPUT);

	// check whether the data does not actually originally come from C++
	// if so, we reference the original data directly
	PyObject* capsule = PyArray_BASE(INPUT);
	if(PyCapsule_IsValid(capsule, "MatrixDataCapsule")) {
		auto dataPtr = (shared_ptr<RealType>*)
			PyCapsule_GetPointer(capsule, "MatrixDataCapsule");
		RESULT = NEWOP ClassType(
			*dataPtr, false, dims[0], dims[1], strides[0] / itemSize,
			strides[1] / itemSize
		);
	} else {
	// if not, we need to make sure the numpy array is not deleted while
	// therefore is a C++ matrix that references its data
		Py_INCREF(INPUT);
		RESULT = NEWOP ClassType(
			shared_ptr<RealType>(data,
			[INPUT](RealType*){Py_DECREF(INPUT);}), false,
			dims[0], dims[1], strides[0] / itemSize, strides[1] / itemSize
		);
	}
%enddef

namespace systematic {

%typemap(out) Matrix&, const Matrix&, Matrix*, const Matrix*,
	MatrixRef&, MatrixCRef&, MatrixRef*, MatrixCRef*,
	const MatrixRef&, const MatrixCRef&, const MatrixRef*, const MatrixCRef*
{
	matrix_out_typemap($1, $result)
}

%typemap(out) Matrix, MatrixRef, MatrixCRef {
	auto swig_arg_ptr = &$1;
	matrix_out_typemap(swig_arg_ptr, $result)
}

%typemap(typecheck, precedence=1) Matrix, MatrixRef,
MatrixCRef, Matrix&, const Matrix&, Matrix*, const Matrix*, MatrixRef&,
MatrixCRef&, MatrixRef*, MatrixCRef*, const MatrixRef&, const MatrixCRef&,
const MatrixRef*, const MatrixCRef* {
	$1 = PyArray_Check($input) and PyArray_NDIM($input) == 2 and
	PyArray_TYPE($input) == NUMPY_REAL_TYPE and (PyArray_FLAGS($input) & NPY_ARRAY_CARRAY);	
}

%typemap(in) Matrix, MatrixRef, MatrixCRef {
	matrix_in_typemap(SWIG_EMPTY, $1_basetype, $input, $1, SWIGError)
}

%typemap(in) Matrix&, const Matrix&, Matrix*, const Matrix*,
	MatrixRef&, MatrixCRef&, MatrixRef*, MatrixCRef*,
	const MatrixRef&, const MatrixCRef&, const MatrixRef*, const MatrixCRef*
{
	matrix_in_typemap(new, $1_basetype, $input, $1, SWIGError)
}

%typemap(directorin) Matrix*, const Matrix*, MatrixRef*, MatrixCRef*,
	const MatrixRef*, const MatrixCRef*
{
	PyObject* resobj;
	matrix_out_typemap($1, resobj);
	$input = resobj;
}

%typemap(directorin) Matrix, MatrixRef, MatrixCRef, Matrix&, const Matrix&,
	MatrixRef&, MatrixCRef&, const MatrixRef&, const MatrixCRef&
{
	PyObject* resobj;
	auto swig_arg_ptr = &$1;
	matrix_out_typemap(swig_arg_ptr, resobj)
	$input = resobj;
}

%typemap(directorout) Matrix, MatrixRef, MatrixCRef
{
	PyObject* pyin = (PyObject*) $input;
	matrix_in_typemap(SWIG_EMPTY, Matrix, pyin, $result, SYSError)
}

%typemap(directorout) Matrix*, const Matrix*, MatrixRef*, MatrixCRef*,
	const MatrixRef*, const MatrixCRef*, Matrix&, const Matrix&,
	MatrixRef&, MatrixCRef&, const MatrixRef&, const MatrixCRef&
{
	PyObject* pyin = (PyObject*) $input;
	matrix_in_typemap(new, $1_basetype, pyin, $result, SYSError)
}

// free the temporary Matrix
%typemap(freearg) Matrix&, const Matrix&, Matrix*, const Matrix*,
	MatrixRef&, MatrixCRef&, MatrixRef*, MatrixCRef*,
	const MatrixRef&, const MatrixCRef&, const MatrixRef*, MatrixCRef*
{
	delete $1;
}

%typemap(memberin) Matrix, MatrixRef, MatrixCRef {
	$1.referenceData(*$input);
}

} // namespace systematic

//------------------------------------------------------------------------------
//						RowVector numpy interface
//------------------------------------------------------------------------------

%define rowvector_out_typemap(OBJ, RESULT)
	typedef typename std::remove_reference<
		decltype(OBJ->shareData())>::type PtrType;
	auto dataPtr(new PtrType(OBJ->shareData()));
	auto data = dataPtr->get();

	auto descr = PyArray_DescrFromType(NUMPY_REAL_TYPE);

    npy_intp dims[1] = {OBJ->size()};
	npy_intp strides[1];

	strides[0] = OBJ->stride() * sizeof(RealType);

	RESULT = PyArray_NewFromDescr(&PyArray_Type, descr, 1, dims,
		strides, (void*) data, NPY_ARRAY_CARRAY, NULL);
	auto array = (PyArrayObject*) RESULT;

	auto capsule = PyCapsule_New((void*) dataPtr, "MatrixDataCapsule",
		free_matrix_data);
	PyArray_SetBaseObject(array, capsule);
%enddef

%define rowvector_in_typemap(NEWOP, CLASSNAME, INPUT, RESULT, ERROR_FUNC)
	typedef typename std::remove_const<typename
		std::remove_reference<CLASSNAME>::type>::type ClassType;

	if(!PyArray_Check(INPUT) or PyArray_NDIM(INPUT) != 1)
		ERROR_FUNC("A 1-dimensional numpy array expected as input.",
			SWIG_ValueError, TracedError_InvalidArgument);

	if(PyArray_TYPE(INPUT) != NUMPY_REAL_TYPE)
		ERROR_FUNC("Wrong data type (systematic::RealType expected).",
			SWIG_TypeError, TracedError_InvalidArgument);

	if(!(PyArray_FLAGS(INPUT) & NPY_ARRAY_CARRAY))
		ERROR_FUNC("The data in the array must be aligned, contiguous and writeable.",
			SWIG_ValueError, TracedError_InvalidArgument);

	npy_intp* dims = PyArray_DIMS(INPUT);
	npy_intp* strides = PyArray_STRIDES(INPUT);
	RealType* data = (RealType*) PyArray_DATA(INPUT);
	auto itemSize = PyArray_ITEMSIZE(INPUT);

	// check whether the data does not actually originally come from C++
	// if so, we reference the original data directly
	PyObject* capsule = PyArray_BASE(INPUT);
	if(PyCapsule_IsValid(capsule, "MatrixDataCapsule")) {
		auto dataPtr = (shared_ptr<RealType>*)
			PyCapsule_GetPointer(capsule, "MatrixDataCapsule");
		RESULT = NEWOP ClassType(
			*dataPtr, false, dims[0], strides[0] / itemSize
		);
	} else {
	// if not, we need to make sure the numpy array is not deleted while
	// therefore is a C++ matrix that references its data
		Py_INCREF(INPUT);
		RESULT = NEWOP ClassType(
			shared_ptr<RealType>(data,
				[INPUT](RealType*){Py_DECREF(INPUT);}),
			false, dims[0], strides[0] / itemSize
		);
	}
%enddef

namespace systematic {

%typemap(out) RowVector&, const RowVector&, RowVector*, const RowVector*,
	RowVectorRef&, RowVectorCRef&, RowVectorRef*, RowVectorCRef*,
	const RowVectorRef&, const RowVectorCRef&, const RowVectorRef*, const RowVectorCRef*
{
	rowvector_out_typemap($1, $result)
}

%typemap(out) RowVector, RowVectorRef, RowVectorCRef {
	auto swig_arg_ptr = &$1;
	rowvector_out_typemap(swig_arg_ptr, $result)
}

%typemap(typecheck, precedence=0) RowVector, RowVectorRef,
RowVectorCRef, RowVector&, const RowVector&, RowVector*, const RowVector*,
RowVectorRef&, RowVectorCRef&, RowVectorRef*, RowVectorCRef*,
const RowVectorRef&, const RowVectorCRef&, const RowVectorRef*, const RowVectorCRef*
{
	$1 = PyArray_Check($input) and PyArray_NDIM($input) == 1 and
	PyArray_TYPE($input) == NUMPY_REAL_TYPE and (PyArray_FLAGS($input) & NPY_ARRAY_CARRAY);	
}

%typemap(in) RowVector, RowVectorRef, RowVectorCRef {
	rowvector_in_typemap(SWIG_EMPTY, $1_basetype, $input, $1, SWIGError)
}

%typemap(in) RowVector&, const RowVector&, RowVector*, const RowVector*,
	RowVectorRef&, RowVectorCRef&, RowVectorRef*, RowVectorCRef*,
	const RowVectorRef&, const RowVectorCRef&, const RowVectorRef*, const RowVectorCRef*
{
	rowvector_in_typemap(new, $1_basetype, $input, $1, SWIGError)
}

%typemap(directorin) RowVector*, const RowVector*, RowVectorRef*, RowVectorCRef*,
	const RowVectorRef*, const RowVectorCRef*
{
	PyObject* resobj;
	auto swig_arg_ptr = &$1;
	rowvector_out_typemap(swig_arg_ptr, resobj);
	$input = resobj;
}

%typemap(directorin) RowVector, RowVectorRef, RowVectorCRef, RowVector&,
	const RowVector&, RowVectorRef&, RowVectorCRef&,
	const RowVectorRef&, const RowVectorCRef&
{
	PyObject* resobj;
	auto swig_arg_ptr = &$1;
	rowvector_out_typemap(swig_arg_ptr, resobj)
	$input = resobj;
}

%typemap(directorout) RowVector, RowVectorRef, RowVectorCRef
{
	PyObject* pyin = (PyObject*) $input;
	rowvector_in_typemap(SWIG_EMPTY, RowVector, pyin, $result, SYSError)
}

%typemap(directorout) RowVector*, const RowVector*, RowVectorRef*, RowVectorCRef*,
	const RowVectorRef*, const RowVectorCRef*, RowVector&, const RowVector&,
	RowVectorRef&, RowVectorCRef&, const RowVectorRef&, const RowVectorCRef&
{
	PyObject* pyin = (PyObject*) $input;
	rowvector_in_typemap(new, $1_basetype, pyin, $result, SYSError)
}

// free the temporary RowVector
%typemap(freearg, match="in") RowVector&, const RowVector&, RowVector*,
	const RowVector*, RowVectorRef&, RowVectorCRef&, RowVectorRef*, RowVectorCRef*,
	const RowVectorRef&, const RowVectorCRef&, const RowVectorRef*, const RowVectorCRef*
{
	delete $1;
}

%typemap(freearg, match="directorout") RowVector&, const RowVector&, RowVector*,
	const RowVector*, RowVectorRef&, RowVectorCRef&, RowVectorRef*, RowVectorCRef*,
	const RowVectorRef&, const RowVectorCRef&, const RowVectorRef*, const RowVectorCRef*
{
	delete $1;
}

%typemap(memberin) RowVector, RowVectorRef, RowVectorCRef {
	$1.referenceData(*$input);
}

} // namespace systematic

//------------------------------------------------------------------------------
//								SparseMatrix
//------------------------------------------------------------------------------

namespace systematic {
	class SparseRowXpr;
} // namespace systematic

//--------------------------------------------------
//					SparseRowXpr
//--------------------------------------------------

namespace systematic {

class SparseRowXpr {
public:
	SparseRowXpr(const SparseRowXpr&);
	SparseRowXpr& operator=(const SparseRowXpr&) = delete;
	SparseRowXpr() = delete;
};

%extend SparseRowXpr {

%feature("python:slot", "mp_length", functype="lenfunc") __len__;
%feature("python:slot", "mp_subscript", functype="binaryfunc") __getitem__;
%feature("python:slot", "mp_ass_subscript", functype="objobjargproc") __setitem__;
	
AlgebraIndex __len__() {
	return $self->size();
}

RealType __getitem__(AlgebraIndex index) {
	if(index >= $self->size())
		throw TracedError_OutOfRange("Index overflow.");

    return $self->coeff(index);
}

void __setitem__(AlgebraIndex index, RealType val) {
	if(index >= $self->size())
		throw TracedError_OutOfRange("Index overflow.");

    $self->coeffRef(index) = val;
}

PyObject* tolist() {
	PyObject* list = PyList_New($self->size());
	
	for(AlgebraIndex i = 0; i < $self->size(); i++) {
		PyList_SetItem(list, i, PyFloat_FromDouble($self->coeff(i)));
	}

	return list;
}

//__STR__()

}

} // namespace systematic

//--------------------------------------------------
//					the Triplet
//--------------------------------------------------

namespace systematic {

class Triplet {
public:
  Triplet() : m_row(0), m_col(0), m_value(0) {}

  Triplet(const unsigned int& i, const unsigned int& j, const RealType& v = RealType(0))
    : m_row(i), m_col(j), m_value(v)
  {}

  /** \returns the row index of the element */
  const unsigned int& row() const { return m_row; }

  /** \returns the column index of the element */
  const unsigned int& col() const { return m_col; }

  /** \returns the value of the element */
  const RealType& value() const { return m_value; }

protected:
  unsigned int m_row, m_col;
  RealType m_value;
};

%extend Triplet {

	%feature("python:slot", "tp_repr", functype="reprfunc") __str__;
	%feature("python:slot", "tp_str", functype="reprfunc") __repr__;

	std::string __str__() {
	  std::ostringstream out;
	  out << "t(" << $self->row() << ", " << $self->col() << ", " << $self->value() << ")";
	  return out.str();
	}

	std::string __repr__() {
	  std::ostringstream out;
	  out << "t(" << $self->row() << ", " << $self->col() << ", " << $self->value() << ")";
	  return out.str();
	}

}

} //namespace systematic

%template(Triplets) std::vector<systematic::Triplet>;

//--------------------------------------------------
//				the SparseMatrix
//--------------------------------------------------

namespace systematic {

class SparseMatrix {
public:
	AlgebraIndex rows() const;
	AlgebraIndex cols() const;
	AlgebraIndex size() const;

	void makeCompressed();
	bool isCompressed() const;
	
	SparseMatrix copyShape() const;

public:
	SparseMatrix(AlgebraIndex rows, AlgebraIndex cols);
	SparseMatrix(AlgebraIndex rows_, AlgebraIndex cols_, const std::vector<Triplet>& triplets);
};

%extend SparseMatrix {

	%feature("python:slot", "mp_length", functype="lenfunc") __len__;
	%feature("python:slot", "mp_subscript", functype="binaryfunc") __getitem__;

	void insert(AlgebraIndex row, AlgebraIndex col, RealType value) {
		$self->insert(row, col) = value;
	}

	AlgebraIndex __len__() {
		return $self->rows();
	}

	SparseRowXpr __getitem__(AlgebraIndex row) {
		if(row >= $self->rows())
			throw TracedError_OutOfRange("Index overflow.");

		return $self->row(row);
	}

	RealType __getitem__(AlgebraIndex row, AlgebraIndex col) {
		if(row >= $self->rows() || col >= $self->cols())
			throw TracedError_OutOfRange("Index overflow.");

		return $self->coeff(row, col);
	}

	PyObject* tolist() {
		Matrix mat(*$self);
		PyObject* list = PyList_New(mat.rows());
	
		for(AlgebraIndex row = 0; row < mat.rows(); row++) {
			PyObject* rowlist = PyList_New(mat.cols());
			for(AlgebraIndex col = 0; col < mat.cols(); col++) {
				PyList_SetItem(rowlist, col, PyFloat_FromDouble(mat(row, col)));
			}
		
			PyList_SetItem(list, row, rowlist);
		}

		return list;
	}

	std::vector<systematic::Triplet> extractTriplets() {
		std::vector<Triplet> triplets;
		triplets.reserve($self->nonZeros());

		for(int i = 0; i < $self->outerSize(); ++i) {
		  for(auto iter = begin(*$self, i); iter != end(*$self, i); iter++) {
			  Triplet t(iter.row(), iter.col(), iter.value());
			  triplets.push_back(t);
		  }
		}

		return triplets;
	}

	__STR__()

	Matrix dense() const {
		return Matrix(*$self);
	}
}

} // namespace systematic

//--------------------------------------------------
//				SparseBlocks
//--------------------------------------------------

namespace systematic {

%define def_SparseBlock_type(BLOCK_TYPE)

class BLOCK_TYPE {
public:
	AlgebraIndex rows() const;
	AlgebraIndex cols() const;
	AlgebraIndex size() const;
	
	BLOCK_TYPE& operator=(const BLOCK_TYPE&) = delete;
	BLOCK_TYPE(const BLOCK_TYPE&) = default;
	BLOCK_TYPE() = delete;
};

%extend BLOCK_TYPE {

	%feature("python:slot", "mp_length", functype="lenfunc") __len__;
	%feature("python:slot", "mp_subscript", functype="binaryfunc") __getitem__;

	AlgebraIndex __len__() {
		return $self->rows();
	}
	
	RealType __getitem__(AlgebraIndex row, AlgebraIndex col) {
		if(row >= $self->rows() || col >= $self->cols())
			throw TracedError_OutOfRange("Index overflow.");

		return $self->coeff(row, col);
	}

	PyObject* tolist() {
		Matrix mat(*$self);
		PyObject* list = PyList_New(mat.rows());
	
		for(AlgebraIndex row = 0; row < mat.rows(); row++) {
			PyObject* rowlist = PyList_New(mat.cols());
			for(AlgebraIndex col = 0; col < mat.cols(); col++) {
				PyList_SetItem(rowlist, col, PyFloat_FromDouble(mat(row, col)));
			}
		
			PyList_SetItem(list, row, rowlist);
		}

		return list;
	}

	//__STR__()

	Matrix dense() const {
		return Matrix(*$self);
	}
}

%enddef

def_SparseBlock_type(SparseBlock)
def_SparseBlock_type(ConstSparseBlock)

} //namespace systematic

#endif // SWIG___ALGEBRA___I

