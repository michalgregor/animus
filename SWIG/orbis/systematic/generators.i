// macros and defines

make_director(systematic::Generator<RealType>);
make_director(systematic::Generator<unsigned int>);
make_director(systematic::Generator<int>);

%define generator_in_typemap(TYPE, TYPE_NAME, INPUT, RESULT, NEW, DEREF, ERROR_FUNC)
	void* ptr = nullptr;
	static swig_type_info* info = nullptr;

	if(!info) {
		info = SWIG_TypeQuery(#TYPE" *");

		if(!info) {
			ERROR_FUNC(
				(std::string("Type query for type '") + #TYPE" *" + "' was insuccessful.").c_str(),
				SWIG_ValueError, TracedError_InvalidArgument
			);
		}
	}
	
	auto ret = SWIG_ConvertPtr($input, &ptr, info, 0);
	isCast = SWIG_IsOK(ret);

	if(isCast) RESULT = DEREF reinterpret_cast<TYPE*>(ptr);
	else RESULT = NEW SwigDirector_##TYPE_NAME(INPUT);
%enddef

%define generator_typemaps(TYPE, TYPE_NAME)
	%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER) TYPE, const TYPE,
	TYPE*, const TYPE*, TYPE&, const TYPE& {
		$1 = true;
		static swig_type_info* info = nullptr;
		
		if(!info) {
			info = SWIG_TypeQuery(%str(TYPE)" *");
			if(!info) {
				SWIG_exception(SWIG_ValueError,
					(std::string("Type query for type '") + #TYPE" *" + "' was insuccessful.").c_str()
				);
			}
		}

		void* ptr;
		auto ret = SWIG_ConvertPtr($input, &ptr, info, 0);
		if(!SWIG_IsOK(ret)) {
			if(!PyCallable_Check($input)) $1 = false;
		}
	}

	%typemap(in) TYPE* (bool isCast), const TYPE* (bool isCast),
	TYPE& (bool isCast), const TYPE& (bool isCast) {
		generator_in_typemap(TYPE, TYPE_NAME, $input, $1, new, SWIG_EMPTY, SWIGError)
	}

	%typemap(in) TYPE (bool isCast) {
		generator_in_typemap(TYPE, TYPE_NAME, $input, $1, SWIG_EMPTY, *, SWIGError)
	}

	%typemap(directorout) TYPE* (bool isCast), const TYPE* (bool isCast),
	TYPE& (bool isCast), const TYPE& (bool isCast) {
		generator_in_typemap(TYPE, TYPE_NAME, $input, $result, new, SWIG_EMPTY, SYSError)
	}

	%typemap(directorout) TYPE (bool isCast) {
		generator_in_typemap(TYPE, TYPE_NAME, $input, $result, SWIG_EMPTY, *, SYSError)
	}

	%typemap(freearg) TYPE*, const TYPE*, TYPE&, const TYPE& {	
		if(!isCast$argnum) delete $1;
	}
%enddef

// includes and templates
make_include(<Systematic/generator/RandEngine.h>)
make_include(<Systematic/generator/Generator.h>)

namespace systematic {

class RealType;

%template(GeneratorReal) Generator<RealType>;
%template(GeneratorUInt) Generator<unsigned int>;
%template(GeneratorInt) Generator<int>;

generator_typemaps(%arg(systematic::Generator<systematic::RealType>), GeneratorReal)
generator_typemaps(%arg(systematic::Generator<unsigned int>), GeneratorUInt)
generator_typemaps(%arg(systematic::Generator<int>), GeneratorInt)

} // namespace systematic

