#ifndef SWIG_SYSTEMATIC_I
#define SWIG_SYSTEMATIC_I

#ifndef __SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") systematic
	%feature("autodoc", "3");

	// python imports
	%pythoncode %{
		from orbis.__algebra__ import *;
		from orbis.__common__ import *;
		from orbis.__systematic__ import *;
	%}
#endif

%include "../system.i"

// C++ includes and definitions
%{
	#ifndef SWIG_SYSTEMATIC_INCLUDES_H
	#define SWIG_SYSTEMATIC_INCLUDES_H

	#include <Systematic/system.h>
	#include <Systematic/math/RealType.h>
	using namespace systematic;

	#ifdef SYS_ALGEBRA_FLOAT
		#define NUMPY_REAL_TYPE NPY_FLOAT
	#else
		#define NUMPY_REAL_TYPE NPY_DOUBLE
	#endif

	#include <numpy/arrayobject.h>

	void free_matrix_data(PyObject* capsule) {
		auto dataPtr = (shared_ptr<RealType>*) PyCapsule_GetPointer(capsule, "MatrixDataCapsule");
		delete dataPtr;
	}

	#include <Systematic/math/algebra.h>

	namespace systematic {
		typedef Matrix::RowXpr RowXpr;
		typedef Matrix::ConstRowXpr ConstRowXpr;
		typedef typename Ref<Matrix>::RowXpr RowXprRef;
		typedef typename Ref<Matrix>::ConstRowXpr ConstRowXprRef;
		typedef typename SparseMatrix::RowXpr SparseRowXpr;
	}

	#endif // SWIG_SYSTEMATIC_INCLUDES_H
%}

// some dummy macro definitions

%define SYS_COMMA ,
%enddef

%define SWIG_EMPTY
%enddef

#define SYS_WNONVIRT_DTOR_OFF
#define SYS_WNONVIRT_DTOR_ON
#define SYS_WFLOAT_EQUAL_OFF
#define SYS_WFLOAT_EQUAL_ON
#define _SYS_FUNC_SIG_

#define SYS_EXPORT_CLASS(X)
#define SYS_EXPORT_TEMPLATE(X, Y)
#define SYS_REGISTER_TYPENAME(X)
#define SYS_REGISTER_TEMPLATENAME(X, Y)
#define SYS_REGISTER_TEMPLATENAME_DETAILED(X, Y, Z)
#define UNUSED(X)
#define SYS_API

#define SYS_CLONEABLE_VOID()												\
friend class systematic::clone_access;										\
typedef int SYS_CLASS_TYPE;													\
virtual SYS_CLASS_TYPE* clone_impl() const {}

#define SYS_CLONEABLE()
#define BOOST_MPL_ASSERT(X)
#define SYS_DIAG_OFF(X)
#define SYS_DIAG_ON(X)

#define SYS_ASSERT(CONDITION)
#define SYS_ASSERT_MSG(CONDITION, MESSAGE)
#define SYS_ALWAYS_ASSERT(CONDITION)
#define SYS_ALWAYS_ASSERT_MSG(CONDITION, MESSAGE)

#define SYS_THROW_ASSERT(CONDITION, EXCEPTIONTYPE)
#define SYS_THROW_ASSERT_MSG(CONDITION, EXCEPTIONTYPE, MESSAGE)

#define SYS_FAKE_SERIALIZE_CLASS(TYPE)
#define SYS_FAKE_SERIALIZE_TEMPLATE(TEMPLATE, NUM_PARAMS)
#define SYS_FAKE_SERIALIZE_TEMPLATE_DETAILED(TEMPLATE, TPARAMS_DECL, TPARAMS_VALS)

#endif //__SWIG__SYSTEMATIC_I__

