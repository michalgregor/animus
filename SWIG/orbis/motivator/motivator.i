#ifndef __SWIG_MOTIVATOR_I__
#define __SWIG_MOTIVATOR_I__

#ifndef __SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") motivator
	%feature("autodoc", "3");

	%pythoncode %{
		from orbis.__motivator__ import *;
	%}
#endif

%{
	#include <Rlearner/system.h>
	using namespace rlearner;
%}

#endif // __SWIG_MOTIVATOR_I__

