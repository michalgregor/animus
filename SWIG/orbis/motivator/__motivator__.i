#ifndef __SWIG___MOTIVATOR___I__
#define __SWIG___MOTIVATOR___I__

#ifndef __SWIG_IMPORT_ONLY__
	%module(directors="1", package="orbis") __motivator__
	%feature("autodoc", "3");

	%pythoncode %{
		import orbis.systematic;
		import orbis.annalyst;
		import orbis.rlearner;
	%}
#endif

%include "../system.i"

import_module("../systematic/__algebra__.i")
import_module("../systematic/__systematic__.i")
import_module("../annalyst/__annalyst__.i")
import_module("../rlearner/__rlearner_base__.i")

make_include(<Motivator/system.h>)

using namespace systematic;
using namespace annalyst;
using namespace rlearner;
using namespace motivator;
using namespace SHARED_PTR_NAMESPACE;

//------------------------------------------------------------------------------
//					Director and smartptr declarations
//------------------------------------------------------------------------------

// Curve/Curve.h
make_director(motivator::Curve);
smartptr(motivator::Curve);

// Curve/Curve_LineSegment.h
make_director(motivator::Curve_LineSegment);
smartptr(motivator::Curve_LineSegment);

// Curve/Curve_Rectangle.h
make_director(motivator::Curve_Rectangle);
smartptr(motivator::Curve_Rectangle);

// Curve/Curve_Triangle.h
make_director(motivator::Curve_Triangle);
smartptr(motivator::Curve_Triangle);

// Curve/Curve_Wundt.h
make_director(motivator::Curve_Wundt);
smartptr(motivator::Curve_Wundt);

// Drive/Drive.h
make_director(motivator::Drive);
smartptr(motivator::Drive);

// Drive/Drive_Homeostatic.h
make_director(motivator::Drive_Homeostatic);
smartptr(motivator::Drive_Homeostatic);

// Drive/Drive_Hullian.h
make_director(motivator::Drive_Hullian);
smartptr(motivator::Drive_Hullian);

// Drive/Drive_SingleDim.h
make_director(motivator::Drive_SingleDim);
smartptr(motivator::Drive_SingleDim);

// Reporter/EpisodicReporter.h
// template; nothing to declare

// Reporter/EpisodicReporterSocket.h
// template; nothing to declare

// Reporter/NoveltyReporter.h
make_director(motivator::EpisodicReporter<RealType>);
smartptr(motivator::EpisodicReporter<RealType>);

// Reporter/NoveltyReporter_Average.h
make_director(motivator::NoveltyReporter_Average);
smartptr(motivator::NoveltyReporter_Average);

// Reporter/NoveltyReporter_Data.h
make_director(motivator::NoveltyReporter_Data);
smartptr(motivator::NoveltyReporter_Data);

// Reporter/NoveltyReporter_File.h
make_director(motivator::NoveltyReporter_File);
smartptr(motivator::NoveltyReporter_File);

// Reporter/NoveltyReporterSocket.h
smartptr(motivator::EpisodicReporterSocket<RealType>);

// ChebyshevEntry.h
// nothing to declare

// ChebyshevRewardEntry.h
// nothing to declare

// NoveltyDetector.h
make_director(motivator::NoveltyDetector);
smartptr(motivator::NoveltyDetector);

// CuriosityReward.h
make_director(motivator::CuriosityReward);
smartptr(motivator::CuriosityReward);

// DummyStateReward.h
make_director(motivator::DummyStateReward);
smartptr(motivator::DummyStateReward);

// NoveltyReward.h
make_director(motivator::NoveltyReward)
smartptr(motivator::NoveltyReward)

// MultiMotivator.h
make_director(motivator::MultiMotivator)
smartptr(motivator::MultiMotivator)

// NoveltyDetector_Forecaster.h
make_director(motivator::NoveltyDetector_Forecaster)
smartptr(motivator::NoveltyDetector_Forecaster)

// NoveltyDetector_HSOM.h
make_director(motivator::NoveltyDetector_HSOM)
smartptr(motivator::NoveltyDetector_HSOM)

// NoveltyDetector_HSOM_Multistep.h
make_director(motivator::NoveltyDetector_HSOM_Multistep)
smartptr(motivator::NoveltyDetector_HSOM_Multistep)

// NoveltyDetector_PairForecaster.h
make_director(motivator::NoveltyDetector_PairForecaster)
smartptr(motivator::NoveltyDetector_PairForecaster)

// NoveltyDetector_Sum.h
make_director(motivator::NoveltyDetector_Sum)
smartptr(motivator::NoveltyDetector_Sum)

// QScalarization_Chebyshev.h
make_director(motivator::QScalarization_Chebyshev)
smartptr(motivator::QScalarization_Chebyshev)

// QScalarization_ChebyshevReward.h
make_director(motivator::QScalarization_ChebyshevReward)
smartptr(motivator::QScalarization_ChebyshevReward)

// QScalarization_Wundt.h
make_director(motivator::QScalarization_Wundt)
smartptr(motivator::QScalarization_Wundt)

// StateType.h
// nothing to declare

// VScalarization_Chebyshev.h
make_director(motivator::VScalarization_Chebyshev)
smartptr(motivator::VScalarization_Chebyshev)

// VScalarization_ChebyshevReward.h
make_director(motivator::VScalarization_ChebyshevReward)
smartptr(motivator::VScalarization_ChebyshevReward)

//------------------------------------------------------------------------------
//						Includes and templates
//------------------------------------------------------------------------------

// Curve/Curve.h
make_include(<Motivator/Curve/Curve.h>)

// Curve/Curve_LineSegment.h
make_include(<Motivator/Curve/Curve_LineSegment.h>)

// Curve/Curve_Rectangle.h
make_include(<Motivator/Curve/Curve_Rectangle.h>)

// Curve/Curve_Triangle.h
make_include(<Motivator/Curve/Curve_Triangle.h>)

// Curve/Curve_Wundt.h
make_include(<Motivator/Curve/Curve_Wundt.h>)

// Drive/Drive.h
make_include(<Motivator/Drive/Drive.h>)

// Drive/Drive_Homeostatic.h
make_include(<Motivator/Drive/Drive_Homeostatic.h>)

// Drive/Drive_Hullian.h
make_include(<Motivator/Drive/Drive_Hullian.h>)

// Drive/Drive_SingleDim.h
make_include(<Motivator/Drive/Drive_SingleDim.h>)

// Reporter/EpisodicReporter.h
make_include(<Motivator/Reporter/EpisodicReporter.h>)

// Reporter/EpisodicReporterSocket.h
make_include(<Motivator/Reporter/EpisodicReporterSocket.h>)

// Reporter/NoveltyReporter.h
make_include(<Motivator/Reporter/NoveltyReporter.h>)
make_template(NoveltyReporter) motivator::EpisodicReporter<RealType>;

// Reporter/NoveltyReporter_Average.h
make_include(<Motivator/Reporter/NoveltyReporter_Average.h>)

// Reporter/NoveltyReporter_Data.h
make_include(<Motivator/Reporter/NoveltyReporter_Data.h>)

// Reporter/NoveltyReporter_File.h
make_include(<Motivator/Reporter/NoveltyReporter_File.h>)

// Reporter/NoveltyReporterSocket.h
make_include(<Motivator/Reporter/NoveltyReporterSocket.h>)
make_template(NoveltyReporterSocket) motivator::EpisodicReporterSocket<RealType>;

// ChebyshevEntry.h
make_include(<Motivator/ChebyshevEntry.h>)

// ChebyshevRewardEntry.h
make_include(<Motivator/ChebyshevRewardEntry.h>)

// NoveltyDetector.h
make_include(<Motivator/NoveltyDetector.h>)

// CuriosityReward.h
make_include(<Motivator/CuriosityReward.h>)

// DummyStateReward.h
make_include(<Motivator/DummyStateReward.h>)

// NoveltyReward.h
make_include(<Motivator/NoveltyReward.h>)

// MultiMotivator.h
make_include(<Motivator/MultiMotivator.h>)

// NoveltyDetector_Forecaster.h
make_include(<Motivator/NoveltyDetector_Forecaster.h>)

// NoveltyDetector_PairForecaster.h
make_include(<Motivator/NoveltyDetector_PairForecaster.h>)

// NoveltyDetector_HSOM.h
make_include(<Motivator/NoveltyDetector_HSOM.h>)

// NoveltyDetector_HSOM_Multistep.h
make_include(<Motivator/NoveltyDetector_HSOM_Multistep.h>)

// NoveltyDetector_Sum.h
make_include(<Motivator/NoveltyDetector_Sum.h>)

// WundtEntry.h
make_include(<Motivator/WundtEntry.h>)

// QScalarization_Chebyshev.h
make_include(<Motivator/QScalarization_Chebyshev.h>)

// QScalarization_ChebyshevReward.h
make_include(<Motivator/QScalarization_ChebyshevReward.h>)

// QScalarization_Wundt.h
make_include(<Motivator/QScalarization_Wundt.h>)

// StateType.h
make_include(<Motivator/StateType.h>)

// VScalarization_Chebyshev.h
make_include(<Motivator/VScalarization_Chebyshev.h>)

// VScalarization_ChebyshevReward.h
make_include(<Motivator/VScalarization_ChebyshevReward.h>)

#endif // __SWIG___MOTIVATOR___I__

