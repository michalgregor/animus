#ifndef Motivator_QScalarization_Chebyshev_H_
#define Motivator_QScalarization_Chebyshev_H_

#include "system.h"
#include "ChebyshevEntry.h"

#include <Rlearner/qfunction.h>
#include <Rlearner/agentlistener.h>

#include <map>
#include <utility>

namespace motivator {

class QScalarization_Chebyshev: public AbstractQFunction, public SemiMDPListener {
public:
	typedef std::map<AbstractQFunction*, ChebyshevEntry> map_type;

protected:
	//! The map containing the functions.
	map_type _functionMap;
	//! The utopian constant (the number that is added to the best known value
	//! for an objective so far to produce the utopian point).
	RealType _tau = 1;

	//! The coefficient by which the scalarized value is multiplied.
	RealType _coeff = -1;

protected:
	typedef map_type::iterator iterator;
	typedef map_type::const_iterator const_iterator;

	/**
	 * Returns an iterator to the entry corresponding to the specified function.
	 *
	 * \exception TracedError_OutOfRange Throws when function is not registered
	 * with the scalarization at all.
	 */
	const_iterator findEntry(AbstractQFunction* function) const {
		const_iterator iter = _functionMap.find(function);
		if(iter == _functionMap.end()) throw TracedError_OutOfRange("No such function registered.");
		return iter;
	}

	/**
	 * Returns an iterator to the entry corresponding to the specified function.
	 *
	 * \exception TracedError_OutOfRange Throws when function is not registered
	 * with the scalarization at all.
	 */
	iterator findEntry(AbstractQFunction* function) {
		iterator iter = _functionMap.find(function);
		if(iter == _functionMap.end()) throw TracedError_OutOfRange("No such function registered.");
		return iter;
	}

public:
	map_type& getEntries() {
		return _functionMap;
	}

	const map_type& getEntries() const {
		return _functionMap;
	}

public:
	virtual void nextStep(StateCollection *, Action *, StateCollection *);
	virtual void newEpisode();

public:
	/**
	 * Returns the coefficient by which the resulting scalarized value
	 * is multiplied before being returned by the getValue() function.
	 */
	RealType getCoeff() const {
		return _coeff;
	}

	/**
	 * Sets the coefficient by which the resulting scalarized value
	 * is multiplied before being returned by the getValue() function.
	 */
	void setCoeff(RealType coeff) {
		_coeff = coeff;
	}

	//! Returns tau. @copydoc _tau
	RealType getTau() const {
		return _tau;
	}

	//! Sets tau. @copydoc _tau
	void setTau(RealType tau) {
		_tau = tau;
	}

	virtual RealType getValue(StateCollection *state, Action *action,
		ActionData *data = nullptr);

	virtual AbstractQETraces *getStandardETraces() {
		return nullptr;
	}

	/**
	 * Returns the entry corresponding to the specified value function.
	 *
	 * \exception TracedError_OutOfRange Throws when function is not registered
	 * with the scalarization at all.
	 */
	ChebyshevEntry& getFunctionEntry(AbstractQFunction* function) {
		return findEntry(function)->second;
	}

	/**
	 * Returns the entry corresponding to the specified value function.
	 *
	 * \exception TracedError_OutOfRange Throws when function is not registered
	 * with the scalarization at all.
	 */
	const ChebyshevEntry& getFunctionEntry(AbstractQFunction* function) const {
		return findEntry(function)->second;
	}

	/**
	 * Adds a value function with its corresponding reward function, and weight
	 * to the scalarization.
	 *
	 * If an entry already exists for the function, it is replaced by the new
	 * entry (note: this also has the effect of resetting the utopia value).
	 */
	void addFunction(AbstractQFunction* function, RealType weight = 1.0) {
		_functionMap.insert(std::make_pair(function, ChebyshevEntry(weight)));
	}

	/**
	 * Removes the specified function from the scalarization. If scalarization
	 * contains no such function, this has no effect.
	 */
	void removeFunction(AbstractQFunction* function) {
		auto iter = _functionMap.find(function);
		if(iter != _functionMap.end()) _functionMap.erase(iter);
	}

	void normWeights(RealType factor);

public:
	QScalarization_Chebyshev& operator=(const QScalarization_Chebyshev&) = delete;
	QScalarization_Chebyshev(const QScalarization_Chebyshev&) = delete;

	QScalarization_Chebyshev(RealType tau = 1);
	virtual ~QScalarization_Chebyshev() = default;
};

} //namespace motivator

#endif //Motivator_QScalarization_Chebyshev_H_
