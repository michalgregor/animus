#include "NoveltyDetector.h"

namespace motivator {
		
/**
* Boost serialization function.
**/
void NoveltyDetector::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<SemiMDPListener>(*this);
	ar & reporterSocket;
}

void NoveltyDetector::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<SemiMDPListener>(*this);
	ar & reporterSocket;
}

}//namespace motivator
