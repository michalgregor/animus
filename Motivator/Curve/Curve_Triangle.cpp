#include "Curve_Triangle.h"

namespace motivator {

/**
 * Boost serialization function
 **/
void Curve_Triangle::serialize(IArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Curve>(*this);
	ar & left;
	ar & right;
	ar & center;
	ar & min;
	ar & max;
}

void Curve_Triangle::serialize(OArchive& ar,
    const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Curve>(*this);
	ar & left;
	ar & right;
	ar & center;
	ar & min;
	ar & max;
}

/**
 * Takes in novelty, and returns curiosity.
 */
RealType Curve_Triangle::operator()(RealType x) {
	if(x < left) return min;
	else if(x > right) return min;
	else if(x < center) {
		RealType steepness = max/(center - left);
		return steepness*x - steepness*left + min;
	} else {
		RealType steepness = max/(center - right);
		return steepness*x - steepness*right + min;
	}
}

Curve_Triangle::Curve_Triangle(RealType left_, RealType center_,
	RealType right_, RealType min_, RealType max_):
	left(left_), center(center_), right(right_), min(min_), max(max_) {}

} //namespace motivator
