#ifndef Motivator_Curve_Rectangle_H_
#define Motivator_Curve_Rectangle_H_

#include "../system.h"
#include "Curve.h"

namespace motivator {

/**
* @class Curve_Rectangle
* @author Michal Gregor
*
* Implements the Wundt curiosity curve.
**/
class MOTIVATOR_API Curve_Rectangle: public Curve {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//! Specifies where the rectangle begins.
	RealType left;
	//! Specifies where the rectangle ends.
	RealType right;
	//! The minimum value of curiosity (outside the rectangle).
	RealType min;
	//! The maximum value of curiosity (inside the rectangle).
	RealType max;

public:
	/**
	 * Takes an x-value, and returns the corresponding y-value.
	 */
	virtual RealType operator()(RealType x);

public:
	Curve_Rectangle(RealType left_ = 0_r, RealType right_ = 1_r,
		RealType min_ = 0_r, RealType max_ = 1_r);
	virtual ~Curve_Rectangle() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::Curve_Rectangle)

#endif //Motivator_Curve_Rectangle_H_
