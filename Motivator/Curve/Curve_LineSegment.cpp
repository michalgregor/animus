#include "Curve_LineSegment.h"

namespace motivator {

void Curve_LineSegment::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Curve>(*this);
	ar & sx;
	ar & sy;
	ar & ex;
	ar & ey;
}

void Curve_LineSegment::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Curve>(*this);
	ar & sx;
	ar & sy;
	ar & ex;
	ar & ey;
}

RealType Curve_LineSegment::operator()(RealType x) {
	if(x < sx) return sy;
	else if(x > ex) return ey;
	else return (ey - sy)/(x - sx);
}

Curve_LineSegment::Curve_LineSegment(): sx(0), sy(0), ex(0), ey(0) {}

Curve_LineSegment::Curve_LineSegment(RealType sx_, RealType sy_, RealType ex_, RealType ey_):
	sx(sx_), sy(sy_), ex(ex_), ey(ey_) {}

} //namespace motivator
