#ifndef Motivator_Curve_Triangle_H_
#define Motivator_Curve_Triangle_H_

#include "../system.h"
#include "Curve.h"

namespace motivator {

/**
* @class Curve_Triangle
* @author Michal Gregor
*
* Implements the Wundt curiosity curve.
**/
class MOTIVATOR_API Curve_Triangle: public Curve {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//! Specifies where the left vertex of the triangle should be (the point
	//! after which curiosity goes from min to max).
	RealType left;
	//! Specifies where the centre vertex of the triangle should be
	//! (curiosity is at value max here).
	RealType center;
	//! Specifies where the right vertex of the triangle should be (the point
	//! at which curiosity has gone from max to min).
	RealType right;

	//! The absolute value of curiosity added to the triangle.
	RealType min;
	//! The maximum value of curiosity.
	RealType max;

public:
	/**
	 * Takes an x-value, and returns the corresponding y-value.
	 */
	virtual RealType operator()(RealType x);

public:
	Curve_Triangle(RealType left_ = 0_r, RealType center_ = 0.5_r,
					RealType right_ = 1_r, RealType min_ = 0_r, RealType max_ = 1_r);
	virtual ~Curve_Triangle() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::Curve_Triangle)

#endif //Motivator_Curve_Triangle_H_
