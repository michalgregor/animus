#ifndef Motivator_Curve_H_
#define Motivator_Curve_H_

#include "../system.h"

namespace motivator {

/**
* @class Curve
* @author Michal Gregor
*
* An interface for curiosity curves.
**/
class MOTIVATOR_API Curve {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	/**
	 * Takes an x-value, and returns the corresponding y-value.
	 */
	virtual RealType operator()(RealType novelty) = 0;

public:
	virtual ~Curve() = 0;
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::Curve)

#endif //Motivator_Curve_H_
