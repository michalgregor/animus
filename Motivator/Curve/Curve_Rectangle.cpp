#include "Curve_Rectangle.h"

namespace motivator {

/**
 * Boost serialization function
 **/
void Curve_Rectangle::serialize(IArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Curve>(*this);
	ar & left;
	ar & right;
	ar & min;
	ar & max;
}

void Curve_Rectangle::serialize(OArchive& ar,
    const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Curve>(*this);
	ar & left;
	ar & right;
	ar & min;
	ar & max;
}

/**
 * Takes in novelty, and returns curiosity.
 */
RealType Curve_Rectangle::operator()(RealType x) {
	if(x < left || x > right) return min;
	else return max;
}

Curve_Rectangle::Curve_Rectangle(RealType left_, RealType right_,
		RealType min_, RealType max_):
	left(left_), right(right_), min(min_), max(max_) {}

} //namespace motivator
