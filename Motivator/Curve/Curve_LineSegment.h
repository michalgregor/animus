#ifndef Motivator_Curve_LineSegment_H_
#define Motivator_Curve_LineSegment_H_

#include "../system.h"
#include "Curve.h"

namespace motivator {

/**
* @class Curve_LineSegment
* @author Michal Gregor
*
* Implements a line segment.
*
* The line segment goes linearly from the specified startp oint (sx, sy)
* to the specified end point (ex, ey). Before and after those points its
* value remains constant:
* - sy for any x < sx;
* - ey for any y > ex.
**/
class MOTIVATOR_API Curve_LineSegment: public Curve {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//! X coordinate of the start point.
	RealType sx;
	//! Y coordinate of the start point.
	RealType sy;

	//! X coordinate of the start point.
	RealType ex;
	//! Y coordinate of the start point.
	RealType ey;

public:
	/**
	 * Takes an x-value, and returns the corresponding y-value.
	 */
	virtual RealType operator()(RealType x);

public:
	Curve_LineSegment();
	Curve_LineSegment(RealType sx, RealType sy, RealType ex, RealType ey);
	virtual ~Curve_LineSegment() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::Curve_LineSegment)

#endif //Motivator_Curve_LineSegment_H_
