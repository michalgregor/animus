#include "Curve.h"

namespace motivator {

/**
* Boost serialization function.
**/
void Curve::serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}
void Curve::serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

/**
 * Empty body of the pure virtual destructor.
 */
Curve::~Curve() {}

}//namespace motivator
