#include "Curve_Wundt.h"

namespace motivator {

/**
 * Boost serialization function
 **/
void Curve_Wundt::serialize(IArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Curve>(*this);
	ar & absoluteTerm;
	ar & fmaxMinus;
	ar & fminMinus;
	ar & rhoMinus;
	ar & fmaxPlus;
	ar & fminPlus;
	ar & rhoPlus;
}

void Curve_Wundt::serialize(OArchive& ar,
    const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<Curve>(*this);
	ar & absoluteTerm;
	ar & fmaxMinus;
	ar & fminMinus;
	ar & rhoMinus;
	ar & fmaxPlus;
	ar & fminPlus;
	ar & rhoPlus;
}

/**
 * Takes in novelty, and returns curiosity.
 */
RealType Curve_Wundt::operator()(RealType x) {
	return wundtCurve(x);
}

Curve_Wundt::Curve_Wundt(RealType absoluteTerm_, RealType fmaxMinus_,
		RealType fminMinus_, RealType rhoMinus_,
		RealType fmaxPlus_, RealType fminPlus_,
		RealType rhoPlus_):
	absoluteTerm(absoluteTerm_), fmaxMinus(fmaxMinus_), fminMinus(fminMinus_),
	rhoMinus(rhoMinus_), fmaxPlus(fmaxPlus_), fminPlus(fminPlus_), rhoPlus(rhoPlus_) {}

} //namespace motivator
