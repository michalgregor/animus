#ifndef Motivator_Curve_Wundt_H_
#define Motivator_Curve_Wundt_H_

#include "../system.h"
#include "Curve.h"

namespace motivator {

/**
* @class Curve_Wundt
* @author Michal Gregor
*
* Implements the Wundt curiosity curve.
**/
class MOTIVATOR_API Curve_Wundt: public Curve {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//! The absolute term added to the wundt curve.
	RealType absoluteTerm;

	//! The maximum negative reward.
	RealType fmaxMinus;
	//! The minimum novelty to receive negative reward.
	RealType fminMinus;
	//! Slope of the negative reward function.
	RealType rhoMinus;

	//! The maximum positive reward.
	RealType fmaxPlus;
	//! The minimum novelty to receive positive reward.
	RealType fminPlus;
	//! Slope of the positive reward function.
	RealType rhoPlus;

protected:
	/**
	 * Computes point on one half of the Wundt curve.
	 */
	RealType curveF(RealType x, RealType Fmax, RealType Fmin, RealType rho) const {
		return Fmax / (1 + std::exp(-rho * (2 * x - Fmin)));
	}

	/**
	 * Computes a point on the Wundt curve.
	 */
	RealType wundtCurve(RealType x) const {
		return curveF(x, fmaxPlus, fminPlus, rhoPlus) - curveF(x, fmaxMinus, fminMinus, rhoMinus) + absoluteTerm;
	}

public:
	/**
	 * Takes an x-value, and returns the corresponding y-value.
	 */
	virtual RealType operator()(RealType x);

public:
	Curve_Wundt(RealType absoluteTerm_ = 0.0_r, RealType fmaxMinus_ = 0.3_r,
			RealType fminMinus_ = 0.7_r, RealType rhoMinus_ = 0.3_r,
			RealType fmaxPlus_ = 0.3_r, RealType fminPlus_ = 0.3_r,
			RealType rhoPlus_ = 0.3_r);
	virtual ~Curve_Wundt() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::Curve_Wundt)

#endif //Motivator_Curve_Wundt_H_
