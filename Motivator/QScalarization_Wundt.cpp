#include "QScalarization_Wundt.h"
#include <cmath>
#include <limits>

namespace motivator {

/**
 * Computes the rewards using each reward function, and updates the utopia.
 */
void QScalarization_Wundt::nextStep(StateCollection* oldState, Action* action, StateCollection* newState) {
	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		RealType utopiaReward = iter->second.rewardFunction->getReward(oldState, action, newState) + iter->second.tau;

		RealType antiUtopiaReward = iter->second.rewardFunction->getReward(oldState, action, newState) - iter->second.tau;

		if(utopiaReward > iter->second.utopia) iter->second.utopia = utopiaReward;
		if(antiUtopiaReward < iter->second.antiUtopia) iter->second.antiUtopia = antiUtopiaReward;
	}
}

/**
 * Applies scalarization to compute the value of the state.
 */
RealType QScalarization_Wundt::getValue(StateCollection* state,
		Action* action, ActionData* data)
{

	/*RealType sumValue = 0;

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		RealType value = iter->second.weight * std::abs(iter->first->getValue(state, action, data) - iter->second.utopia);

//		if(iter->second.utopia < iter->first->getValue(state, action, data)) {
//			value = -value;
//		}

		sumValue += value;
	}

	return _coeff*sumValue;*/



	RealType maxValue = -std::numeric_limits<RealType>::max();

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		RealType value = iter->second.weight * std::abs(iter->first->getValue(state, action, data) - iter->second.utopia);

		/*if(iter->second.utopia < iter->first->getValue(state, action, data)) {
			value = 0;
		}*/

		if(value > maxValue) maxValue = value;
	}

	return _coeff*maxValue;


//	RealType value = 0;
//
//	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
//		RealType ival = iter->second.weight * (iter->second.utopia - iter->first->getValue(state, action, data));
//		value += 10*ival;
//	}
//
//	return _coeff*value;








//	RealType maxValue = -std::numeric_limits<RealType>::max();
//	map_type::iterator maxIter;
//
//	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
//		RealType value = iter->second.weight * (iter->first->getValue(state, action, data) - iter->second.utopia);
//		if(value > maxValue) {
//			maxValue = value;
//			maxIter = iter;
//		}
//	}



//	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
//		RealType mid = (iter->second.utopia - iter->second.antiUtopia)/2;
//		RealType value = iter->second.weight * std::abs(iter->first->getValue(state, action, data) - mid);
//
//		if(value > maxValue) {
//			maxValue = value;
//			maxIter = iter;
//		}
//	}

//	// Scale the value.
//	RealType maxValue = -std::numeric_limits<RealType>::max();
//	map_type::iterator maxIter;
//
//	if(maxValue < maxIter->second.antiUtopia) maxValue = 0;
//	else if(maxValue > maxIter->second.utopia) maxValue = 0;
//	else {
//		RealType mid = (maxIter->second.utopia - maxIter->second.antiUtopia)/2;
//
//		if(maxValue <= mid) {
//			maxValue = (maxValue - maxIter->second.antiUtopia)/mid;
//		} else {
//			maxValue = 1 - (maxValue - mid)/mid;
//		}
//
//
//		if(maxValue <= mid) {
//			maxValue = 1 - (maxValue - maxIter->second.antiUtopia)/mid;
//		} else {
//			maxValue = (maxValue - mid)/mid;
//		}
//
//	}
//
//	return _coeff*maxValue;
}

/**
 * Normalises the sum of all weights to factor.
 */
void QScalarization_Wundt::normWeights(RealType factor) {
	RealType sum = 0.0;

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		sum += iter->second.weight;
	}

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		iter->second.weight *= factor / sum;
	}
}

QScalarization_Wundt::QScalarization_Wundt():
	AbstractQFunction(nullptr), _functionMap() {}

} //namespace motivator
