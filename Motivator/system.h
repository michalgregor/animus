#ifndef Motivator_system_H_
#define Motivator_system_H_

#include <Systematic/system.h>
#include <Annalyst/system.h>
#include <Rlearner/system.h>

#include <Systematic/math/numeric_cast.h>

#include <Systematic/configure.h>

namespace motivator {
	//this is required so that shared_ptr and intrusive_ptr
	//are resolved unambiguously
	using systematic::shared_ptr;
	using systematic::intrusive_ptr;

	using namespace systematic;
	using namespace annalyst;
	using namespace rlearner;
} //namespace motivator

#endif //Motivator_system_H_
