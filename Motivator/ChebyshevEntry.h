#ifndef Motivator_ChebyshevEntry_H_
#define Motivator_ChebyshevEntry_H_

#include "system.h"

namespace motivator {

struct ChebyshevEntry {
	RealType weight = 1;
	RealType utopia = -std::numeric_limits<RealType>::max();

	//! Resets the utopian point back to -std::numeric_limits<RealType>::max().
	void resetUtopianPoint() {
		utopia = -std::numeric_limits<RealType>::max();
	}

	ChebyshevEntry(RealType weight_, RealType utopia_ = -std::numeric_limits<RealType>::max()):
		weight(weight_), utopia(utopia_) {}
};

} //namespace motivator

#endif //Motivator_ChebyshevEntry_H_
