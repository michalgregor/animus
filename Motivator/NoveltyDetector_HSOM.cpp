#include "NoveltyDetector_HSOM.h"
#include <Systematic/generator/BoundaryGenerator.h>

namespace motivator {

/**
 * Boost serialization function.
 **/
void NoveltyDetector_HSOM::serialize(IArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyDetector>(*this);

	ar & _hsom;
	ar & _discreteStates;
	ar & _continuousStates;

	//TODO:serialization of StateProperties
	//ar & _properties;
}

void NoveltyDetector_HSOM::serialize(OArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyDetector>(*this);

	ar & _hsom;
	ar & _discreteStates;
	ar & _continuousStates;

	//TODO:serialization of StateProperties
	//ar & _properties;
}

/**
 * Updates the _dataset using the specified state.
 */
void NoveltyDetector_HSOM::prepareDataset(State* state) {
	auto numDiscrete = _discreteStates.size(), numContinuous =
	        _continuousStates.size(), numStates = numDiscrete + numContinuous;
	unsigned int i = 0;

	if(_dataset.cols() != numStates) {
		_dataset.resize(1, numStates);
	}

	RowVectorRef stateVector(_dataset.row(0));

	//DISCRETE STATES
	for (unsigned int j = 0; j < numDiscrete; j++, i++) {
		stateVector[i] = numeric_cast<RealType>(
		        state->getDiscreteState(_discreteStates[j]));
	}

	//CONTINUOUS STATES
	for (unsigned int j = 0; j < numContinuous; j++, i++) {
		stateVector[i] = numeric_cast<RealType>(
		        state->getContinuousState(_continuousStates[j]));
	}
}

/**
 * This method updates the memory of the detector.
 */
void NoveltyDetector_HSOM::nextStep(
	StateCollection* /*oldState*/,
	Action* /*action*/,
	StateCollection* newState
) {
	prepareDataset(newState->getState());

	//ADD OBSERVATION, GET PREDICTION
	_hsom->fit(_dataset);
}

/**
 * Returns how novel current situation (state, state transition, ...) is.
 * Memory of the detector is NOT updated by this function.
 */
RealType NoveltyDetector_HSOM::computeNovelty(StateCollection* UNUSED(oldState), Action* UNUSED(action), StateCollection *newState) {
	prepareDataset(newState->getState());
	RealType novelty = _hsom->computeNovelty(_dataset.row(0));
	reporterSocket(novelty);
	return novelty;
}

/**
 * Resets the NoveltyDetector to its initial state erasing all its memory
 * thus effecting a complete recovery.
 */
void NoveltyDetector_HSOM::reset() {
	_hsom->resetHabituation();
}

/**
 * Default constructor. To be used for serialization mainly. Does not create
 * a forecaster.
 **/
NoveltyDetector_HSOM::NoveltyDetector_HSOM() :
		_hsom(), _dataset(), _discreteStates(), _continuousStates() {
}

/**
 * Default constructor.
 *
 * @param stateType Whether to use discrete states, continuous states, or both.
 * @param numStates The number of state dimensions that will be forecast
 * (associated with the number of inputs to the multi-forecaster).
 **
 * Creates a default OTFA-based multi-forecaster. The default forecaster will
 * likely not be perfectly tuned for every possible task.
 */
NoveltyDetector_HSOM::NoveltyDetector_HSOM(
	std::vector<unsigned int> discreteStates,
	std::vector<unsigned int> continuousStates
):
	_hsom(), _dataset(), _discreteStates(std::move(discreteStates)),
	_continuousStates(std::move(continuousStates))
{

	NetworkSize numStates = numeric_cast<NetworkSize>(
			_discreteStates.size() + _continuousStates.size());

	BoundaryGenerator<RealType> generator(-0.5, 0.5);
	_hsom = make_shared<Network_HSOM>(generator, numStates, numStates, numStates);
}

/**
 * Constructor.
 *
 * @param stateType Whether to use discrete or continuous states.
 * @param hsom The habituated SOM to be used to determine novelty.
 */
NoveltyDetector_HSOM::NoveltyDetector_HSOM(
        std::vector<unsigned int> discreteStates,
        std::vector<unsigned int> continuousStates,
        const shared_ptr<Network_HSOM>& hsom) :
		_hsom(hsom), _dataset(), _discreteStates(std::move(discreteStates)),
		_continuousStates(std::move(continuousStates))
{}

} //namespace motivator
