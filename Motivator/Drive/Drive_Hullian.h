#ifndef Motivator_Drive_Hullian_H_
#define Motivator_Drive_Hullian_H_

#include "Drive_SingleDim.h"

namespace motivator {

/**
 * @class Drive_Hullian
 * This class models the concept of a hullian drive.
 *
 * In hullian drives, the drive level ranges from totally unsatisfied
 * (minimum drive level) to completely satisfied (maximum drive level).
 * Reward is calculated by taking the difference between the the new drive
 * level, and the old drive level. The agent is rewarded for increasing the
 */
class MOTIVATOR_API Drive_Hullian: public Drive_SingleDim {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The minimum drive level (point of starvation).
	RealType _minLevel;
	//! The maximum drive level (point of satiation).
	RealType _maxLevel;
	//! The coefficient by which the value of the state is multiplied when
	//! producing the drive level.
	RealType _driveMultiplier = 1;

public:
	/**
	 * Returns the drive multiplier.
	 * \copydoc _driveMultiplier
	 */
	RealType getDriveMultiplier() const {
		return _driveMultiplier;
	}

	/**
	 * Sets the drive multiplier.
	 * \copydoc _driveMultiplier
	 */
	void setDriveMultiplier(RealType multiplier) {
		_driveMultiplier = multiplier;
	}

	/**
	 * Returns the minimum drive level.
	 * \copydoc _minLevel
	 */
	RealType getMinLevel() const {
		return _minLevel;
	}

	/**
	 * Sets the minimum drive level.
	 * \copydoc _minLevel
	 */
	void setMinLevel(RealType num) {
		_minLevel = num;
	}

	/**
	 * Returns the maximum drive level.
	 * \copydoc _maxLevel
	 */
	RealType getMaxLevel() const {
		return _maxLevel;
	}

	/**
	 * Sets the maximum drive level.
	 * \copydoc _maxLevel
	 */
	void setMaxLevel(RealType num) {
		_maxLevel = num;
	}

public:
	//! Virtual function for calculating the motivation.
	virtual RealType getMotivation(State* oldState, Action* action, State* newState);

public:
	Drive_Hullian(StateProperties* properties);
	Drive_Hullian(StateProperties* properties, RealType minLevel,
		RealType maxLevel, RealType driveMultiplier,
		unsigned int stateDim, bool isDiscrete);

private:
	Drive_Hullian();
};

} //namespace motivator

#endif //Motivator_Drive_Hullian_H_

SYS_EXPORT_CLASS(motivator::Drive_Hullian)
