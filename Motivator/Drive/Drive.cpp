#include "Drive.h"
#include <Rlearner/state.h>
#include <Rlearner/stateproperties.h>

namespace motivator {

void Drive::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<RewardFunction>(*this);
	ar & properties;
}

void Drive::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<RewardFunction>(*this);
	ar & properties;
}

RealType Drive::getReward(StateCollection* oldState, Action* action,
		StateCollection* newState) {
	return numeric_cast<RealType>(getMotivation(oldState->getState(properties), action, newState->getState(properties)));

}

Drive::Drive(StateProperties* properties_): properties(properties_) {}

Drive::Drive(): properties(nullptr) {}

} //namespace motivator
