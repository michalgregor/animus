#include "Drive_Homeostatic.h"

namespace motivator {

void Drive_Homeostatic::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Drive_SingleDim>(*this);
	ar & _rangeMin;
	ar & _rangeMax;
	ar & _driveMultiplier;
}

void Drive_Homeostatic::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Drive_SingleDim>(*this);
	ar & _rangeMin;
	ar & _rangeMax;
	ar & _driveMultiplier;
}

RealType Drive_Homeostatic::getMotivation(State* oldState, Action*, State* newState) {
	RealType oldVal = getValue(oldState);
	RealType newVal = getValue(newState);

	// return 0 if both values in the optimal range
	if(	oldVal <= _rangeMax && oldVal >= _rangeMin &&
		newVal <= _rangeMax && newVal >= _rangeMin) return 0;

	return dist(oldVal) - dist(newVal);
}

Drive_Homeostatic::Drive_Homeostatic():
	Drive_SingleDim(nullptr), _rangeMin(0), _rangeMax(0) {}

Drive_Homeostatic::Drive_Homeostatic(StateProperties* properties_):
	Drive_SingleDim(properties_), _rangeMin(0), _rangeMax(0) {}

/**
 * Constructor.
 * @param rangeMin \copydoc rangeMin
 * @param rangeMax \copydoc rangeMax
 * @param driveMultiplier \copydoc driveMultiplier
 * @param stateDim \copydoc _stateDim
 * @param isDiscrete \copydoc _isDiscrete
 */
Drive_Homeostatic::Drive_Homeostatic(StateProperties* properties_, RealType rangeMin,
	RealType rangeMax, RealType driveMultiplier, unsigned int stateDim, bool isDiscrete):
	Drive_SingleDim(properties_, stateDim, isDiscrete), _rangeMin(rangeMin),
	_rangeMax(rangeMax), _driveMultiplier(driveMultiplier) {}


} //namespace motivator
