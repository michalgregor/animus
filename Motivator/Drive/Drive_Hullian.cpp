#include "Drive_Hullian.h"

namespace motivator {

void Drive_Hullian::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Drive_SingleDim>(*this);
	ar & _minLevel;
	ar & _maxLevel;
	ar & _driveMultiplier;
}

void Drive_Hullian::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Drive_SingleDim>(*this);
	ar & _minLevel;
	ar & _maxLevel;
	ar & _driveMultiplier;
}

RealType Drive_Hullian::getMotivation(State* oldState, Action*, State* newState) {
	RealType oldVal = getValue(oldState);
	RealType newVal = getValue(newState);

	return std::max(std::min(newVal, _maxLevel), _minLevel) - std::max(std::min(oldVal, _maxLevel), _minLevel);
}

Drive_Hullian::Drive_Hullian():
	Drive_SingleDim(nullptr), _minLevel(0), _maxLevel(0) {}

Drive_Hullian::Drive_Hullian(StateProperties* properties_):
	Drive_SingleDim(properties_), _minLevel(0), _maxLevel(0) {}

Drive_Hullian::Drive_Hullian(StateProperties* properties_, RealType minLevel,
	RealType maxLevel, RealType driveMultiplier, unsigned int stateDim, bool isDiscrete):
	Drive_SingleDim(properties_, stateDim, isDiscrete), _minLevel(minLevel),
	_maxLevel(maxLevel), _driveMultiplier(driveMultiplier) {}

} //namespace motivator
