#include "Drive_SingleDim.h"

namespace motivator {

void Drive_SingleDim::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Drive>(*this);
	ar & _isDiscrete;
	ar & _stateDim;
}

void Drive_SingleDim::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Drive>(*this);
	ar & _isDiscrete;
	ar & _stateDim;
}

/**
 * Default constructor. Preferably only for serialization.
 */
Drive_SingleDim::Drive_SingleDim(): Drive(nullptr), _isDiscrete(true), _stateDim(0) {}

/**
 * Default constructor.
 * \warning A curve has to be set before the drive is computed.
 */
Drive_SingleDim::Drive_SingleDim(StateProperties* properties_):
	Drive(properties_), _isDiscrete(true), _stateDim(0) {}

Drive_SingleDim::Drive_SingleDim(StateProperties* properties_, unsigned int stateDim, bool isDiscrete):
		Drive(properties_), _isDiscrete(isDiscrete), _stateDim(stateDim) {}

} //namespace motivator
