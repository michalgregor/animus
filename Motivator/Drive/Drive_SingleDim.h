#ifndef Motivator_Drive_SingleDim_H_
#define Motivator_Drive_SingleDim_H_

#include "Drive.h"
#include <Rlearner/state.h>

namespace motivator {

/**
 * @class Drive_SingleDim
 * An implementation of drive that takes uses a Curve to map the input
 * state to a drive level.
 */
class MOTIVATOR_API Drive_SingleDim: public Drive {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	//! Remembers whether the drive should work with discrete or continuous states.
	bool _isDiscrete;
	//! The dimension of the state used by the drive.
	unsigned int _stateDim;

protected:
	//! Returns value of the state dimension used by the drive.
	RealType getValue(State* state) const {
		if(_isDiscrete) return numeric_cast<RealType>(state->getDiscreteState(_stateDim));
		else return numeric_cast<RealType>(state->getContinuousState(_stateDim));
	}

public:
	/**
	 * Returns whether the drive works with discrete or continuous states.
	 */
	bool getIsDiscrete() const {
		return _isDiscrete;
	}

	/**
	 * Sets whether the drive works with discrete or continuous states.
	 */
	void setIsDiscrete(bool isDiscrete) {
		_isDiscrete = isDiscrete;
	}

	/**
	 * Returns the state dimension used by the drive.
	 * \copydoc _stateDim
	 */
	unsigned int setStateDimension() const {
		return _stateDim;
	}

	/**
	 * Sets the state dimension used by the drive.
	 * \copydoc _stateDim
	 */
	void setStateDimension(unsigned int stateDim) {
		_stateDim = stateDim;
	}

public:
	Drive_SingleDim(StateProperties* properties);

	/**
	 * Constructor.
	 * @param stateDim \copydoc _stateDim
	 * @param isDiscrete \copydoc _isDiscrete
	 */
	Drive_SingleDim(StateProperties* properties, unsigned int stateDim, bool isDiscrete);

	virtual ~Drive_SingleDim() = default;

private:
	Drive_SingleDim();
};

} //namespace motivator

SYS_EXPORT_CLASS(motivator::Drive_SingleDim)

#endif //Motivator_Drive_SingleDim_H_
