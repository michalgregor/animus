#ifndef Motivator_Drive_H_
#define Motivator_Drive_H_

#include "../system.h"
#include <Rlearner/rewardfunction.h>

namespace motivator {

/**
 * @class Drive
 * The base class of drives - classes, which observe the state, and use it
 * to calculate rewards.
 *
 * The canonical example of a drive is hunger - the drive will monitor
 * the internal state of the agent, and drive it to seek food when necessary.
 */
class MOTIVATOR_API Drive: public RewardFunction {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

protected:
	StateProperties* properties;

public:
	//! Virtual function for calculating the motivation.
	virtual RealType getMotivation(State* oldState, Action* action, State* newState) = 0;

public:
	//! The drive as it is given by getMotivation() is passed as the reward.
	virtual RealType getReward(StateCollection* oldState, Action* action,
		StateCollection* newState);

public:
	Drive& operator=(const Drive&) = default;
	Drive(const Drive&) = default;

	Drive(StateProperties* properties_);
	virtual ~Drive() = default;

private:
	Drive();
};

} //namespace motivator

SYS_EXPORT_CLASS(motivator::Drive)

#endif //Motivator_Drive_H_
