#ifndef Motivator_Drive_Homeostatic_H_
#define Motivator_Drive_Homeostatic_H_

#include "Drive_SingleDim.h"

namespace motivator {

/**
 * @class Drive_Homeostatic
 * This class models the concept of a homeostatic drive.
 *
 * In homeostatic drives, an optimal range is specified for the drive level.
 * Motivation is non-zero for drive levels outside the optimal range, and
 * is generated using drive level difference. Moving the level towards the
 * optimal range is rewarded, and moving it outside the range is punished.
 */
class MOTIVATOR_API Drive_Homeostatic: public Drive_SingleDim {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The minimum of the optimal range.
	RealType _rangeMin;
	//! The maximum of the optimal range.
	RealType _rangeMax;
	//! The coefficient by which the value of the state is multiplied when
	//! producing the drive level.
	RealType _driveMultiplier = 1;

protected:
	//! Returns the distance of the drive level from the optimal range.
	RealType dist(RealType level) {
		if(level > _rangeMax) return level - _rangeMax;
		else if(level < _rangeMin) return _rangeMin - level;
		else return 0;
	}

public:
	/**
	 * Returns the drive multiplier.
	 * \copydoc _driveMultiplier
	 */
	RealType getDriveMultiplier() const {
		return _driveMultiplier;
	}

	/**
	 * Sets the drive multiplier.
	 * \copydoc _driveMultiplier
	 */
	void setDriveMultiplier(RealType multiplier) {
		_driveMultiplier = multiplier;
	}

	/**
	 * Returns the minimum of the optimal range.
	 */
	RealType getRangeMin() const {
		return _rangeMin;
	}

	/**
	 * Sets the minimum of the optimal range.
	 */
	void setRangeMin(RealType num) {
		_rangeMin = num;
	}

	/**
	 * Sets the maximum of the optimal range.
	 */
	void setRangeMax(RealType num) {
		_rangeMax = num;
	}

public:
	//! Virtual function for calculating the motivation.
	virtual RealType getMotivation(State* oldState, Action* action, State* newState);

public:
	Drive_Homeostatic(StateProperties* properties);
	Drive_Homeostatic(StateProperties* properties, RealType rangeMin,
		RealType rangeMax, RealType driveMultiplier,
		unsigned int stateDim, bool isDiscrete);

private:
	Drive_Homeostatic();
};

} //namespace motivator

#endif //Motivator_Drive_Homeostatic_H_

SYS_EXPORT_CLASS(motivator::Drive_Homeostatic)
