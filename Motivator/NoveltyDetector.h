#ifndef Motivator_NoveltyDetector_H_
#define Motivator_NoveltyDetector_H_

#include "system.h"

#include <Rlearner/agentlistener.h>
#include <Rlearner/statecollection.h>

#include "Reporter/NoveltyReporterSocket.h"

namespace motivator {

/**
* @class NoveltyDetector
* @author Michal Gregor
*
* An interface for various implementations of novelty detectors.
**/
class MOTIVATOR_API NoveltyDetector: public SemiMDPListener {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//! The reporter socket used to report values of novelty. The values are
	//! to be reported by implementations of computeNovelty().
	NoveltyReporterSocket reporterSocket;

public:
	/**
	 * This method updates the memory of the detector.
	 */
	virtual void nextStep(StateCollection* oldState, Action* action, StateCollection* newState) = 0;

	/**
	 * Notifies the detector that a new episode has started. This should always
	 * call reporterSocket.newEpisode().
	 */
	virtual void newEpisode() {
		reporterSocket.newEpisode();
	}

	/**
	 * Returns how novel current situation (state, state transition, ...) is.
	 * Memory of the detector is NOT updated by this function.
	 *
	 * This function also reports novelty using reporterSocket.
	 */
	virtual RealType computeNovelty(StateCollection *oldState, Action *action, StateCollection *newState) = 0;

	/**
	 * Returns the novelty calculated when RealType was last called.
	 */
	virtual RealType getNovelty() const = 0;

	/**
	 * Resets the NoveltyDetector to its initial state erasing all its memory
	 * thus effecting a complete recovery.
	 */
	virtual void reset() = 0;

public:
	NoveltyDetector& operator=(const NoveltyDetector&) = delete;
	NoveltyDetector(const NoveltyDetector&) = delete;

	NoveltyDetector() = default;
	virtual ~NoveltyDetector() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::NoveltyDetector)

#endif //Motivator_NoveltyDetector_H_
