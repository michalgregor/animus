#ifndef Motivator_NoveltyDetector_Sum_H_
#define Motivator_NoveltyDetector_Sum_H_

#include "system.h"

#include <Rlearner/agentlistener.h>
#include <Rlearner/statecollection.h>
#include <Annalyst/Network_HSOM.h>

#include "NoveltyDetector.h"

namespace motivator {

/**
 * @class NoveltyDetector_Sum
 * @author Michal Gregor
 *
 * A NoveltyDetector based on a Forecaster from Annalyst.
 **/
class MOTIVATOR_API NoveltyDetector_Sum: public NoveltyDetector {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	RealType _novelty = 0;

public:
	//! A vector of the novelty detectors.
	std::vector<shared_ptr<NoveltyDetector> > detectors;

public:
	virtual void nextStep(StateCollection* oldState, Action* action,
	        StateCollection* newState);
	virtual RealType computeNovelty(StateCollection *oldState, Action *action, StateCollection *newState);
	virtual RealType getNovelty() const;
	virtual void reset();

public:
	NoveltyDetector_Sum();
	NoveltyDetector_Sum(const std::vector<shared_ptr<NoveltyDetector> >& detectors);
	NoveltyDetector_Sum(std::vector<shared_ptr<NoveltyDetector> >&& detectors);

	virtual ~NoveltyDetector_Sum() {
	}
};

} //namespace motivator

SYS_EXPORT_CLASS(motivator::NoveltyDetector_Sum)

#endif //Motivator_NoveltyDetector_Sum_H_
