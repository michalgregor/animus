#ifndef Motivator_NoveltyReward_H_
#define Motivator_NoveltyReward_H_

#include "system.h"
#include "NoveltyDetector.h"
#include <Rlearner/rewardfunction.h>

namespace motivator {

/**
* @class NoveltyReward
* @author Michal Gregor
*
* A motivation model based on curiosity.
**/
class MOTIVATOR_API NoveltyReward: public RewardFunction {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

protected:
	//! The novelty detector used to determine curiosity.
	shared_ptr<NoveltyDetector> _novelty;

public:
	//! The reporter socket used to report values of reward. The values are
	//! to be reported by implementations of getReward().
	NoveltyReporterSocket rewardSocket;

public:
	/**
	* Returns the novelty detector used to determine novelty.
	**/
	const shared_ptr<NoveltyDetector>& getNoveltyDetector() const {
		return _novelty;
	}

	/**
	* Sets the novelty detector used to determine novelty.
	*
	* \note The detector will generally need to be added as a listener to work
	* correctly. It does not happen automagically.
	**/
	void setNoveltyDetector(const shared_ptr<NoveltyDetector>& noveltyDetector) {
		_novelty = noveltyDetector;
	}

public:
	virtual RealType getReward(StateCollection *oldState, Action *action, StateCollection *newState);

public:
	NoveltyReward& operator=(const NoveltyReward& obj);
	NoveltyReward(const NoveltyReward& obj);

	NoveltyReward();
	NoveltyReward(StateProperties *properties);
	NoveltyReward(const shared_ptr<NoveltyDetector>& noveltyDetector);
	virtual ~NoveltyReward() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::NoveltyReward)

#endif //Motivator_NoveltyReward_H_
