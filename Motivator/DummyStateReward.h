#ifndef Motivator_DummyStateReward_H_
#define Motivator_DummyStateReward_H_

#include "system.h"

#include <Rlearner/rewardfunction.h>

namespace motivator {

/**
* @class DummyStateReward
* @author Michal Gregor
*
* A convertor from RewardFunction to StateReward that passes a nullptr
* to RewardFunction instead of the data not available to StateReward.
**/
class MOTIVATOR_API DummyStateReward: public StateReward {
private:
	//! The underlying RewardFunction.
	shared_ptr<RewardFunction> _rewardFunction;

public:
	virtual RealType getStateReward(State *modelState);

	const shared_ptr<RewardFunction>& getRewardFunction() {
		return _rewardFunction;
	}

	void setRewardFunction(const shared_ptr<RewardFunction>& rewardFunction) {
		_rewardFunction = rewardFunction;
	}

public:
	DummyStateReward();
	DummyStateReward(StateProperties *properties, const shared_ptr<RewardFunction>& rewardFunction = shared_nullptr);
	virtual ~DummyStateReward() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::DummyStateReward)

#endif //Motivator_DummyStateReward_H_
