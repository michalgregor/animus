#ifndef Motivator_StateType_H_
#define Motivator_StateType_H_

namespace motivator {

/**
 * @enum StateType
 *
 * An enum used to specify type of state.
 */
enum StateType {
	/**
	 * Discrete state.
	 */
	ST_DISCRETE,
	/**
	 * Continuous state.
	 */
	ST_CONTINUOUS
};

/**
 * @enum StateTypeMulti
 *
 * An enum used to specify type of state.
 */
enum StateTypeMulti {
	/**
	 * Use discrete states.
	 */
	STM_DISCRETE = 1,
	/**
	 * Use continuous states.
	 */
	STM_CONTINUOUS = 2,
	/**
	 * Use both - discrete and continuous states.
	 */
	STM_BOTH = 3
};

} //namespace motivator

#endif //Motivator_StateType_H_
