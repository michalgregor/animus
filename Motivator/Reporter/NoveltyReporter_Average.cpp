#include "NoveltyReporter_Average.h"

namespace motivator {

/**
* Boost serialization function.
**/
void NoveltyReporter_Average::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyReporter>(*this);
	ar & _noveltySum;
	ar & _numSamples;
}

void NoveltyReporter_Average::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyReporter>(*this);
	ar & _noveltySum;
	ar & _numSamples;
}

void NoveltyReporter_Average::operator()(RealType data) {
	_noveltySum += data;
	++_numSamples;
}

}//namespace motivator
