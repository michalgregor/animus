#include "NoveltyReporter_File.h"

namespace motivator {

/**
* Boost serialization function.
**/
void NoveltyReporter_File::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyReporter>(*this);
	//todo: can't serialize ostream
}

void NoveltyReporter_File::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyReporter>(*this);
	//todo: can't serialize ostream
}

void NoveltyReporter_File::operator()(RealType data) {
	*_file << data << "\t";
}

void NoveltyReporter_File::newEpisode() {
	*_file << std::endl;
}

}//namespace motivator
