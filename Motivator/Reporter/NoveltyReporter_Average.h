#ifndef Motivator_NoveltyReporter_Average_H_
#define Motivator_NoveltyReporter_Average_H_

#include "../system.h"
#include "NoveltyReporter.h"

namespace motivator {

/**
* @class NoveltyReporter_Average
* @author Michal Gregor
*
* Computes the average of reported novelty values.
**/
class MOTIVATOR_API NoveltyReporter_Average: public NoveltyReporter {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! Novelty sum.
	RealType _noveltySum;
	//! Number of samples.
	RealType _numSamples;

public:
	virtual void operator()(RealType data);

	/**
	 * Returns the sum of reported novelty values.
	 */
	RealType getNoveltySum() const {
		return _noveltySum;
	}

	/**
	 * Returns the number of reported novelty values.
	 */
	RealType getNumSamples() const {
		return _numSamples;
	}

	/**
	 * Returns the average of reported values.
	 */
	RealType getAverage() const {
		return _noveltySum/_numSamples;
	}

	/**
	 * Resets the number of reported values, their sum, and the average.
	 */
	void reset() {
		_noveltySum = 0;
		_numSamples = 0;
	}

	virtual ~NoveltyReporter_Average() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::NoveltyReporter_Average)

#endif //Motivator_NoveltyReporter_Average_H_
