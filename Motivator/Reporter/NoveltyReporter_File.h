#ifndef Motivator_NoveltyReporter_File_H_
#define Motivator_NoveltyReporter_File_H_

#include "../system.h"
#include "NoveltyReporter.h"

#include <ostream>

namespace motivator {

/**
 * @class NoveltyReporter_File
 * @author Michal Gregor
 *
 * Writes the novelty values into a file.
 **/
class MOTIVATOR_API NoveltyReporter_File: public NoveltyReporter {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The file to writes the reports into.
	shared_ptr<std::ostream> _file;

public:
	virtual void operator()(RealType data);
	virtual void newEpisode();

	void setFile(const shared_ptr<std::ostream>& file) {
		_file = file;
	}

	const shared_ptr<std::ostream>& getFile() {
		return _file;
	}

public:
	NoveltyReporter_File(const shared_ptr<std::ostream>& file = shared_nullptr):
		_file(file) {}

	virtual ~NoveltyReporter_File() {}
};

} //namespace motivator

SYS_EXPORT_CLASS(motivator::NoveltyReporter_File)

#endif //Motivator_NoveltyReporter_File_H_
