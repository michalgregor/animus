#ifndef Motivator_EpisodicReporterSocket_H_
#define Motivator_EpisodicReporterSocket_H_

#include "../system.h"
#include <Systematic/utils/FunctorSocketBase.h>
#include "EpisodicReporter.h"

namespace motivator {

/**
* @class EpisodicReporterSocket
* @author Michal Gregor
*
* A socket for EpisodicReporter objects.
**/
SYS_DIAG_OFF(effc++)
template<class DataType>
class EpisodicReporterSocket: private FunctorSocketBase<EpisodicReporter<DataType> > {
SYS_DIAG_ON(effc++)
private:
	typedef FunctorSocketBase<EpisodicReporter<DataType> > BaseSocket;

private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function.
	**/
	void serialize(IArchive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<BaseSocket >(*this);
	}

	void serialize(OArchive& ar, const unsigned int UNUSED(version)) {
		ar & boost::serialization::base_object<BaseSocket >(*this);
	}

public:
	using BaseSocket::add;
	using BaseSocket::erase;
	using BaseSocket::clear;
	using BaseSocket::size;

	/**
	* Passes reported data to every connected reporter.
	**/
	void operator()(DataType data) {
		for(auto iter = this->begin(); iter != this->end(); iter++) {
			(**iter)(data);
		}
	}

	/**
	 * Reports a new episode.
	 */
	void newEpisode() {
		for(auto iter = this->begin(); iter != this->end(); iter++) {
			(*iter)->newEpisode();
		}
	}

public:
	EpisodicReporterSocket& operator=(const EpisodicReporterSocket& obj) = delete;
	EpisodicReporterSocket(const EpisodicReporterSocket& obj) = delete;
	EpisodicReporterSocket() {}

	~EpisodicReporterSocket() {
		SYS_EXPORT_INSTANCE();
	}
};

} //namespace motivator

SYS_EXPORT_TEMPLATE(motivator::EpisodicReporterSocket, 1)

#endif //Motivator_EpisodicReporterSocket_H_
