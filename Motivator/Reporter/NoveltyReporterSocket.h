#ifndef Motivator_NoveltyReporterSocket_H_
#define Motivator_NoveltyReporterSocket_H_

#include "../system.h"
#include "EpisodicReporterSocket.h"

namespace motivator {

extern template class MOTIVATOR_API EpisodicReporterSocket<RealType>;
typedef EpisodicReporterSocket<RealType> NoveltyReporterSocket;

} //namespace motivator

#endif //Motivator_NoveltyReporterSocket_H_
