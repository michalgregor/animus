#ifndef Motivator_EpisodicReporter_H_
#define Motivator_EpisodicReporter_H_

#include "../system.h"
#include <Systematic/reporter/Reporter.h>

namespace motivator {

/**
* @class EpisodicReporter
* @author Michal Gregor
*
* A reporter used to report novelty values.
**/
template<class DataType>
class MOTIVATOR_API EpisodicReporter {
private:
	friend class boost::serialization::access;

	/**
	* Boost serialization function
	**/
	void serialize(IArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}
	void serialize(OArchive& UNUSED(ar), const unsigned int UNUSED(version)) {}

public:
	//! Implement operator() to provide reporting of supplied data.
	virtual void operator()(DataType data) = 0;

	/**
	 * Reports a new episode.
	 */
	virtual void newEpisode() {}

	virtual ~EpisodicReporter() = 0;
};

/**
 * Body of the pure virtual destructor.
 */
template<class DataType>
EpisodicReporter<DataType>::~EpisodicReporter() {
	SYS_EXPORT_INSTANCE()
}

} //namespace motivator

SYS_EXPORT_TEMPLATE(motivator::EpisodicReporter, 1)

#endif //Motivator_EpisodicReporter_H_
