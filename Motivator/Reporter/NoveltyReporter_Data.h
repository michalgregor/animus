#ifndef Motivator_NoveltyReporter_Data_H_
#define Motivator_NoveltyReporter_Data_H_

#include "../system.h"
#include "NoveltyReporter.h"

#include <ostream>

namespace motivator {

/**
* @class NoveltyReporter_Data
* @author Michal Gregor
*
* Writes the novelty values into a file.
**/
class MOTIVATOR_API NoveltyReporter_Data: public NoveltyReporter {
public:
	typedef std::vector<std::vector<RealType> > DataMatrix;

private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! The file to writes the reports into.
	DataMatrix _data;

public:
	virtual void operator()(RealType data);
	virtual void newEpisode();

	DataMatrix& data() {
		return _data;
	}

	const DataMatrix& data() const {
		return _data;
	}

public:
	NoveltyReporter_Data(): _data(1, DataMatrix::value_type()) {}

	virtual ~NoveltyReporter_Data() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::NoveltyReporter_Data)

#endif //Motivator_NoveltyReporter_Data_H_
