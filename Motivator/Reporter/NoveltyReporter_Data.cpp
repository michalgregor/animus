#include "NoveltyReporter_Data.h"

namespace motivator {

/**
* Boost serialization function.
**/
void NoveltyReporter_Data::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyReporter>(*this);
	//todo: can't serialize ostream
}

void NoveltyReporter_Data::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyReporter>(*this);
	//todo: can't serialize ostream
}

void NoveltyReporter_Data::operator()(RealType dataPiece) {
	_data.back().push_back(dataPiece);
}

void NoveltyReporter_Data::newEpisode() {
	_data.push_back(DataMatrix::value_type());
}

}//namespace motivator
