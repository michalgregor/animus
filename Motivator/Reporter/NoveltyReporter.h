#ifndef Motivator_NoveltyReporter_H_
#define Motivator_NoveltyReporter_H_

#include "../system.h"
#include "EpisodicReporter.h"

namespace motivator {

extern template class MOTIVATOR_API EpisodicReporter<RealType>;
typedef EpisodicReporter<RealType> NoveltyReporter;

}//namespace motivator

#endif //Motivator_NoveltyReporter_H_
