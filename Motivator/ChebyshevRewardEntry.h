#ifndef Motivator_ChebyshevRewardEntry_H_
#define Motivator_ChebyshevRewardEntry_H_

#include "system.h"
#include <Rlearner/rewardfunction.h>

namespace motivator {

struct ChebyshevRewardEntry {
	RewardFunction* rewardFunction = nullptr;
	RealType weight = 1;
	RealType utopia = -std::numeric_limits<RealType>::max();

	//! Resets the utopian point back to -std::numeric_limits<RealType>::max().
	void resetUtopianPoint() {
		utopia = -std::numeric_limits<RealType>::max();
	}

	ChebyshevRewardEntry(RewardFunction* rewardFunction_, RealType weight_, RealType utopia_ = -std::numeric_limits<RealType>::max()):
		rewardFunction(rewardFunction_), weight(weight_), utopia(utopia_) {}
};

} //namespace motivator

#endif //Motivator_ChebyshevRewardEntry_H_
