#ifndef Motivator_NoveltyDetector_HSOM_Multistep_H_
#define Motivator_NoveltyDetector_HSOM_Multistep_H_

#include "system.h"

#include <Rlearner/agentlistener.h>
#include <Rlearner/statecollection.h>
#include <Annalyst/Network_HSOM.h>

#include "NoveltyDetector.h"

namespace motivator {

/**
 * @class NoveltyDetector_HSOM_Multistep
 * @author Michal Gregor
 *
 * A version of NoveltyDetector_HSOM_Multistep that does clustering above several previous
 * states in addition to the current one.
 **/
class MOTIVATOR_API NoveltyDetector_HSOM_Multistep: public NoveltyDetector {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! Stores the habituated self-organising map used to evaluate novelty.
	shared_ptr<Network_HSOM> _hsom;
	//! The dataset used to interface with the HSOM.
	Matrix _dataset;

	//! Specifies which discrete state dimensions to use in forecasting.
	std::vector<unsigned int> _discreteStates;
	//! Specifies which continuous state dimensions to use in forecasting.
	std::vector<unsigned int> _continuousStates;

	//! The number of time steps used to form a single input to the HSOM.
	unsigned int _numSteps = 2;
	//! Position of the oldest state vector in _stateVectors.
	unsigned int _posBegin = 0;
	//! Stores all the state vectors required to form the input to the HSOM.
	std::vector<RowVector> _stateVectors;

public:
	typedef std::vector<RowVector>::size_type size_type;

protected:
	void clearStateVectors() {
		_stateVectors.clear();
		_posBegin = 0;
	}

	void computeStateVector(State* state, RowVector& stateVector);

	RowVector& getNewStateVector() {
		if(_stateVectors.size() < _numSteps) {
			_stateVectors.push_back(RowVector());
			return _stateVectors.back();
		} else {
			unsigned int pos = _posBegin;
			_posBegin = (_posBegin + 1) % _numSteps;
			return _stateVectors[pos];
		}
	}

public:
	const shared_ptr<Network_HSOM>& getNetwork() {
		return _hsom;
	}

	shared_ptr<const Network_HSOM> getNetwork() const {
		return _hsom;
	}

public:
	RealType getHabituationRate() const {
		return _hsom->getHabituationRate();
	}

	void setHabituationRate(RealType habituationRate) {
		_hsom->setHabituationRate(habituationRate);
	}

	RealType getRecoveryRate() const {
		return _hsom->getRecoveryRate();
	}

	void setRecoveryRate(RealType recoveryRate) {
		_hsom->setRecoveryRate(recoveryRate);
	}

public:
	virtual void nextStep(StateCollection* oldState, Action* action,
	        StateCollection* newState);
	virtual RealType computeNovelty(StateCollection *oldState, Action *action, StateCollection *newState);
	virtual void reset();

public:
	//! Returns the number of time steps used to form a single input to the HSOM.
	size_type getNumSteps() const {
		return _stateVectors.capacity();
	}

public:
	NoveltyDetector_HSOM_Multistep();

	NoveltyDetector_HSOM_Multistep(
			unsigned int numSteps,
			std::vector<unsigned int> discreteStates,
	        std::vector<unsigned int> continuousStates);

	NoveltyDetector_HSOM_Multistep(
			unsigned int numSteps,
			std::vector<unsigned int> discreteStates,
			std::vector<unsigned int> continuousStates,
	        const shared_ptr<Network_HSOM>& hsom);

	virtual ~NoveltyDetector_HSOM_Multistep() {}
};

} //namespace motivator

SYS_EXPORT_CLASS(motivator::NoveltyDetector_HSOM_Multistep)

#endif //Motivator_NoveltyDetector_HSOM_Multistep_H_
