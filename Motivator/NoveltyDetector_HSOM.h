#ifndef Motivator_NoveltyDetector_HSOM_H_
#define Motivator_NoveltyDetector_HSOM_H_

#include "system.h"

#include <Rlearner/agentlistener.h>
#include <Rlearner/statecollection.h>
#include <Annalyst/Network_HSOM.h>

#include "NoveltyDetector.h"

namespace motivator {

/**
 * @class NoveltyDetector_HSOM
 * @author Michal Gregor
 *
 * A NoveltyDetector based on a Forecaster from Annalyst.
 **/
class MOTIVATOR_API NoveltyDetector_HSOM: public NoveltyDetector {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! Stores the habituated self-organising map used to evaluate novelty.
	shared_ptr<Network_HSOM> _hsom;
	//! The dataset used to interface with the HSOM.
	Matrix _dataset;

	//! Specifies which discrete state dimensions to use in forecasting.
	std::vector<unsigned int> _discreteStates;
	//! Specifies which continuous state dimensions to use in forecasting.
	std::vector<unsigned int> _continuousStates;

protected:
	void prepareDataset(State* state);

public:
	const shared_ptr<Network_HSOM>& getNetwork() {
		return _hsom;
	}

	shared_ptr<const Network_HSOM> getNetwork() const {
		return _hsom;
	}

public:
	RealType getHabituationRate() const {
		return _hsom->getHabituationRate();
	}

	void setHabituationRate(RealType habituationRate) {
		_hsom->setHabituationRate(habituationRate);
	}

	RealType getRecoveryRate() const {
		return _hsom->getRecoveryRate();
	}

	void setRecoveryRate(RealType recoveryRate) {
		_hsom->setRecoveryRate(recoveryRate);
	}

public:
	virtual void nextStep(StateCollection* oldState, Action* action,
	        StateCollection* newState);
	virtual RealType computeNovelty(StateCollection *oldState, Action *action, StateCollection *newState);
	virtual void reset();

public:
	NoveltyDetector_HSOM();

	NoveltyDetector_HSOM(
			std::vector<unsigned int> discreteStates,
	        std::vector<unsigned int> continuousStates);

	NoveltyDetector_HSOM(
			std::vector<unsigned int> discreteStates,
			std::vector<unsigned int> continuousStates,
	        const shared_ptr<Network_HSOM>& hsom);

	virtual ~NoveltyDetector_HSOM() {
	}
};

} //namespace motivator

SYS_EXPORT_CLASS(motivator::NoveltyDetector_HSOM)

#endif //Motivator_NoveltyDetector_HSOM_H_
