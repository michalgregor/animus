//#ifndef Motivator_NoveltyDetector_PairForecaster_H_
//#define Motivator_NoveltyDetector_PairForecaster_H_
//
//#include "system.h"
//
//#include <Annalyst/Forecasting/PairForecaster.h>
//#include "ActionEncoder.h"
//#include "NoveltyDetector.h"
//
//namespace motivator {
//
///**
// * @class NoveltyDetector_PairForecaster
// * @author Michal Gregor
// *
// * A NoveltyDetector based on a PairForecaster from Annalyst.
// **/
//class MOTIVATOR_API NoveltyDetector_PairForecaster: public NoveltyDetector {
//private:
//	friend class boost::serialization::access;
//
//	void serialize(IArchive& ar, const unsigned int version);
//	void serialize(OArchive& ar, const unsigned int version);
//
//private:
//	/**
//	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
//	 * TO EVERY DERIVED CLASS.
//	 *
//	 * Used to create a copy of the object of the most-derived class.
//	 */
//	SYS_CLONEABLE()
//
//private:
//	//! Stores the forecaster used to evaluate novelty.
//	shared_ptr<PairForecaster> _forecaster;
//	//! Stores novelty as calculated during the last nextStep() call.
//	RealType _novelty;
//
//	//! Specifies which discrete state dimensions to use as input observations.
//	std::vector<unsigned int> _discreteStatesInput;
//	//! Specifies which continuous state dimensions to use as input observations.
//	std::vector<unsigned int> _continuousStatesInput;
//
//	//! Specifies which discrete state dimensions to use as output observations.
//	std::vector<unsigned int> _discreteStatesOutput;
//	//! Specifies which continuous state dimensions to use as output observations.
//	std::vector<unsigned int> _continuousStatesOutput;
//
//	//! The ActionEncoder used to encode action for the input observation. If
//	//! null, the action is not used at as part of the input observation.
//	shared_ptr<ActionEncoder> _inputActionEncoder;
//
//protected:
//	std::vector<RealType> computeStateVector(State* state,
//			Action* action, const std::vector<unsigned int>& discreteStates,
//	        const std::vector<unsigned int>& continuousStates,
//	        const shared_ptr<ActionEncoder>& actionEncoder);
//
//public:
//	virtual void nextStep(StateCollection* oldState, Action* action,
//	        StateCollection* newState);
//
//	virtual RealType computeNovelty(StateCollection *oldState, Action *action, StateCollection *newState);
//	virtual RealType getNovelty() const;
//	virtual void reset();
//
//public:
//	NoveltyDetector_PairForecaster& operator=(
//	        const NoveltyDetector_PairForecaster& obj);
//	NoveltyDetector_PairForecaster(const NoveltyDetector_PairForecaster& obj);
//
//	NoveltyDetector_PairForecaster();
//
//	NoveltyDetector_PairForecaster(
//			std::vector<unsigned int> discreteStatesInput,
//			std::vector<unsigned int> continuousStatesInput,
//			std::vector<unsigned int> discreteStatesOutput,
//			std::vector<unsigned int> continuousStatesOutput,
//			const shared_ptr<ActionEncoder>& inputActionEncoder = shared_nullptr
//	);
//
//	NoveltyDetector_PairForecaster(
//			std::vector<unsigned int> discreteStatesInput,
//			std::vector<unsigned int> continuousStatesInput,
//			std::vector<unsigned int> discreteStatesOutput,
//			std::vector<unsigned int> continuousStatesOutput,
//	        const shared_ptr<PairForecaster>& forecaster,
//	        const shared_ptr<ActionEncoder>& inputActionEncoder = shared_nullptr);
//
//	virtual ~NoveltyDetector_PairForecaster() {
//	}
//};
//
//} //namespace motivator
//
//SYS_EXPORT_CLASS(motivator::NoveltyDetector_PairForecaster)
//
//#endif //Motivator_NoveltyDetector_PairForecaster_H_
