#include "MultiMotivator.h"
#include <boost/serialization/utility.hpp>

namespace motivator {
	
/**
* Boost serialization function.
**/
void MultiMotivator::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<RewardFunction>(*this);
	ar & _rewardFunctions;
}

void MultiMotivator::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<RewardFunction>(*this);
	ar & _rewardFunctions;
}

RealType MultiMotivator::getReward(StateCollection *oldState, Action *action, StateCollection *newState) {
	typedef std::vector<shared_ptr<RewardFunction> >::size_type SizeType;
	SizeType num = _rewardFunctions.size();
	RealType reward = 0;

	for(SizeType i = 0; i < num; i++) {
		reward += _rewardFunctions[i].second * _rewardFunctions[i].first->getReward(oldState, action, newState);
	}

	return reward;
}

//TODO: assignment will not work, copying a graph of objects

/**
* Assignment operator.
**/
MultiMotivator& MultiMotivator::operator=(const MultiMotivator& obj) {
	RewardFunction::operator=(obj);
	_rewardFunctions = obj._rewardFunctions;

	return *this;
}

/**
* Copy constructor.
**/
MultiMotivator::MultiMotivator(const MultiMotivator& obj):
	RewardFunction(obj), _rewardFunctions(obj._rewardFunctions) {}

/**
* Constructor.
**/
MultiMotivator::MultiMotivator(): _rewardFunctions() {}

}//namespace motivator
