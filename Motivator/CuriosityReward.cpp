#include "CuriosityReward.h"

namespace motivator {

/**
 * Boost serialization function.
 **/
void CuriosityReward::serialize(IArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyReward>(*this);
	ar & _curiosityCurve;
}

void CuriosityReward::serialize(OArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyReward>(*this);
	ar & _curiosityCurve;
}

RealType CuriosityReward::getReward(StateCollection *oldState, Action *action, StateCollection *newState) {
	RealType reward = (*_curiosityCurve)(_novelty->computeNovelty(oldState, action, newState));
	rewardSocket(reward);
	return reward;
}

//TODO: copying graph of objects

/**
 * Assignment operator.
 **/
CuriosityReward& CuriosityReward::operator=(const CuriosityReward& obj) {
	RewardFunction::operator=(obj);
//	_curiosityCurve = clone(obj._curiosityCurve);

	return *this;
}

/**
 * Copy constructor.
 **/
CuriosityReward::CuriosityReward(const CuriosityReward& obj):
		NoveltyReward(obj), _curiosityCurve() {
}

/**
 * Constructor.
 * @param noveltyDetector The novelty detector used to determine novelty.
 * @param curiosityCurve The curiosity curve used to compute curiosity
 * from novelty.
 *
 * \note The detector will generally need to be added as a listener to work
 * correctly. It does not happen automagically.
 **/
CuriosityReward::CuriosityReward(
        const shared_ptr<NoveltyDetector>& noveltyDetector,
        const shared_ptr<Curve>& curiosityCurve) :
		NoveltyReward(noveltyDetector), _curiosityCurve(
		        curiosityCurve) {
}

/**
 * Default constructor.
 **/
CuriosityReward::CuriosityReward() : NoveltyReward(), _curiosityCurve() {}

CuriosityReward::CuriosityReward(StateProperties *properties_):
	NoveltyReward(properties_), _curiosityCurve() {}


} //namespace motivator
