#ifndef Motivator_CuriosityReward_H_
#define Motivator_CuriosityReward_H_

#include "system.h"
#include "NoveltyReward.h"
#include "Curve/Curve.h"

namespace motivator {

/**
 * @class CuriosityReward
 * @author Michal Gregor
 *
 * A motivation model based on curiosity.
 **/
class MOTIVATOR_API CuriosityReward: public NoveltyReward {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	//! The curiosity curve used to convert novelty to curiosity.
	shared_ptr<Curve> _curiosityCurve;

public:
	/**
	 * Returns a pointer to the curiosity curve.
	 */
	const shared_ptr<Curve>& getCurve() const {
		return _curiosityCurve;
	}

	/**
	 * Sets the curiosity curve.
	 */
	void setCurve(const shared_ptr<Curve>& curiosityCurve) {
		_curiosityCurve = curiosityCurve;
	}

public:
	virtual RealType getReward(StateCollection *oldState, Action *action, StateCollection *newState);

public:
	CuriosityReward& operator=(const CuriosityReward& obj);
	CuriosityReward(const CuriosityReward& obj);

	CuriosityReward();
	CuriosityReward(StateProperties *properties);
	CuriosityReward(const shared_ptr<NoveltyDetector>& noveltyDetector,
	        const shared_ptr<Curve>& curiosityCurve);

	virtual ~CuriosityReward() = default;
};

} //namespace motivator

SYS_EXPORT_CLASS(motivator::CuriosityReward)

#endif //Motivator_CuriosityReward_H_
