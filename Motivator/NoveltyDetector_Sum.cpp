#include "NoveltyDetector_Sum.h"

namespace motivator {

/**
 * Boost serialization function.
 **/
void NoveltyDetector_Sum::serialize(IArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyDetector>(*this);

	ar & _novelty;
	ar & detectors;

	//TODO:serialization of StateProperties
	//ar & _properties;
}

void NoveltyDetector_Sum::serialize(OArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyDetector>(*this);

	ar & _novelty;
	ar & detectors;

	//TODO:serialization of StateProperties
	//ar & _properties;
}

/**
 * This method updates the memory of the detector.
 */
void NoveltyDetector_Sum::nextStep(StateCollection* oldState, Action* action,
        StateCollection* newState) {
	for(auto detector: detectors) {
		detector->nextStep(oldState, action, newState);
	}
}

/**
 * Returns how novel current situation (state, state transition, ...) is.
 * Memory of the detector is NOT updated by this function.
 */
RealType NoveltyDetector_Sum::computeNovelty(StateCollection* oldState, Action* action, StateCollection *newState) {
	_novelty = 0;

	for(auto detector: detectors) {
		_novelty += detector->computeNovelty(oldState, action, newState);
	}

	return _novelty;
}

/**
 * Returns how novel current situation (state, state transition, ...) is.
 */
RealType NoveltyDetector_Sum::getNovelty() const {
	return _novelty;
}

/**
 * Resets the NoveltyDetector to its initial state erasing all its memory
 * thus effecting a complete recovery.
 */
void NoveltyDetector_Sum::reset() {
	for(auto detector: detectors) {
		detector->reset();
	}
}

NoveltyDetector_Sum::NoveltyDetector_Sum(): detectors() {}

NoveltyDetector_Sum::NoveltyDetector_Sum(const std::vector<shared_ptr<NoveltyDetector> >& detectors_):
	detectors(detectors_){}

NoveltyDetector_Sum::NoveltyDetector_Sum(std::vector<shared_ptr<NoveltyDetector> >&& detectors_):
	detectors(std::move(detectors_)) {}

} //namespace motivator
