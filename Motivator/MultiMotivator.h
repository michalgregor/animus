#ifndef Motivator_MultiMotivator_H_
#define Motivator_MultiMotivator_H_

#include "system.h"
#include <Rlearner/rewardfunction.h>
#include <utility>
#include <algorithm>

namespace motivator {

/**
* @class MultiMotivator
* @author Michal Gregor
*
* A reward function which combines several other reward functions.
*
* MultiMotivator has the same iterator invalidation rules as std::vector.
**/
class MOTIVATOR_API MultiMotivator: public RewardFunction {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE()

private:
	typedef std::vector<std::pair<shared_ptr<RewardFunction>, RealType> > VectorType;
	VectorType _rewardFunctions;

public:
	typedef VectorType::iterator iterator;
	typedef VectorType::reverse_iterator reverse_iterator;
	typedef VectorType::const_iterator const_iterator;
	typedef VectorType::const_reverse_iterator const_reverse_iterator;
	typedef VectorType::size_type size_type;

public:
	const_iterator begin() const {
		return _rewardFunctions.begin();
	}

	const_iterator end() const {
		return _rewardFunctions.end();
	}

	const_reverse_iterator rbegin() const {
		return _rewardFunctions.rbegin();
	}

	const_reverse_iterator rend() const {
		return _rewardFunctions.rend();
	}

	iterator begin() {
		return _rewardFunctions.begin();
	}

	iterator end() {
		return _rewardFunctions.end();
	}

	reverse_iterator rbegin() {
		return _rewardFunctions.rbegin();
	}

	reverse_iterator rend() {
		return _rewardFunctions.rend();
	}

public:
	/**
	 * Returns the coefficient of the reward function at position iter.
	 */
	RealType getCoefficient(const_iterator iter) const {
		return iter->second;
	}

	/**
	 * Sets the coefficient of the reward function at position iter.
	 */
	void setCoefficient(iterator iter, RealType coefficient) {
		iter->second = coefficient;
	}

	/**
	 * Adds the specified reward function with the specified coefficient and
	 * returns the iterator.
	 */
	iterator add(const shared_ptr<RewardFunction>& rewardFunction, RealType coefficient) {
		_rewardFunctions.push_back(std::make_pair(rewardFunction, coefficient));
		return end() - 1;
	}

	/**
	 * Finds the first occurrence of rewardFunction and returns iterator to that
	 * element. Returns end() if no occurrence is found.
	 *
	 * @param rewardFunction The reward function that is to be found.
	 */
	const_iterator find(const shared_ptr<RewardFunction>& rewardFunction) const {
		return std::find_if(begin(), end(), [&rewardFunction](const VectorType::value_type& val){return val.first == rewardFunction;});
	}

	/**
	 * Finds the first occurrence of rewardFunction and returns iterator to that
	 * element. Returns end() if no occurrence is found.
	 *
	 * @param rewardFunction The reward function that is to be found.
	 */
	iterator find(const shared_ptr<RewardFunction>& rewardFunction) {
		return std::find_if(begin(), end(), [&rewardFunction](const VectorType::value_type& val){return val.first == rewardFunction;});
	}

	/**
	 * Removes the corresponding element from the MultiMotivator.
	 */
	void remove(iterator iter) {
		_rewardFunctions.erase(iter);
	}

	/**
	 * Finds the first occurrence of rewardFunction and removes it. If no
	 * occurrence is found this has no effect.
	 *
	 * @return Returns true if the element has been found and removed.
	 */
	bool remove(const shared_ptr<RewardFunction>& rewardFunction) {
		auto iter = find(rewardFunction);
		if(iter != end()) {
			remove(iter);
			return true;
		} else return false;
	}

	/**
	 * Returns the number of reward functions.
	 */
	size_type size() const {
		return _rewardFunctions.size();
	}

	/**
	 * Removes all reward functions.
	 */
	void clear() {
		_rewardFunctions.clear();
	}

public:
	virtual RealType getReward(StateCollection *oldState, Action *action, StateCollection *newState);

public:
	MultiMotivator& operator=(const MultiMotivator& obj);
	MultiMotivator(const MultiMotivator& obj);

	MultiMotivator();
	virtual ~MultiMotivator() {}
};

}//namespace motivator

SYS_EXPORT_CLASS(motivator::MultiMotivator)

#endif //Motivator_MultiMotivator_H_
