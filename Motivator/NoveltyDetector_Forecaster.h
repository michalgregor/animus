#ifndef Motivator_NoveltyDetector_Forecaster_H_
#define Motivator_NoveltyDetector_Forecaster_H_

#include "system.h"

#include <Annalyst/Forecaster.h>

#include "NoveltyDetector.h"

namespace motivator {

/**
 * @class NoveltyDetector_Forecaster
 * @author Michal Gregor
 *
 * A NoveltyDetector based on a Forecaster from Annalyst.
 **/
class MOTIVATOR_API NoveltyDetector_Forecaster: public NoveltyDetector {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! Stores the forecaster used to evaluate novelty.
	shared_ptr<Forecaster> _forecaster;
	//! Stores novelty as calculated during the last nextStep() call.
	RealType _novelty;

	//! Specifies which discrete state dimensions to use in forecasting.
	std::vector<unsigned int> _discreteStates;
	//! Specifies which continuous state dimensions to use in forecasting.
	std::vector<unsigned int> _continuousStates;

protected:
	RowVector computeStateVector(State* state);

public:
	virtual void nextStep(StateCollection* oldState, Action* action,
	        StateCollection* newState);

	virtual RealType computeNovelty(StateCollection *oldState, Action *action, StateCollection *newState);
	virtual RealType getNovelty() const;
	virtual void reset();

public:
	NoveltyDetector_Forecaster();

//	NoveltyDetector_Forecaster(
//			std::vector<unsigned int> discreteStates,
//			std::vector<unsigned int> continuousStates);

	NoveltyDetector_Forecaster(
			std::vector<unsigned int> discreteStates,
			std::vector<unsigned int> continuousStates,
	        const shared_ptr<Forecaster>& forecaster);

	virtual ~NoveltyDetector_Forecaster() {
	}
};

} //namespace motivator

SYS_EXPORT_CLASS(motivator::NoveltyDetector_Forecaster)

#endif //Motivator_NoveltyDetector_Forecaster_H_
