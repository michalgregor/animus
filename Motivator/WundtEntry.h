#ifndef Motivator_WundtEntry_H_
#define Motivator_WundtEntry_H_

#include "system.h"
#include <Rlearner/rewardfunction.h>

namespace motivator {

struct WundtEntry {
	RewardFunction* rewardFunction = nullptr;
	RealType weight = 1;
	RealType utopia = 0;
	RealType antiUtopia = 0;
	//! The utopian constant (the number that is added to the best known value
	//! for an objective so far to produce the utopian point).
	RealType tau = 0;

	WundtEntry(RewardFunction* rewardFunction_, RealType weight_, RealType tau_, RealType utopia_ = 0.0, RealType antiUtopia_ = 0.0):
		rewardFunction(rewardFunction_), weight(weight_), utopia(utopia_),
		antiUtopia(antiUtopia_), tau(tau_) {}
};

} //namespace motivator

#endif //Motivator_WundtEntry_H_
