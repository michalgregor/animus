#include "NoveltyReward.h"

namespace motivator {

/**
* Boost serialization function.
**/
void NoveltyReward::serialize(IArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<RewardFunction>(*this);
	ar & _novelty;
}

void NoveltyReward::serialize(OArchive& ar, const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<RewardFunction>(*this);
	ar & _novelty;
}

RealType NoveltyReward::getReward(StateCollection *oldState, Action *action, StateCollection *newState) {
	RealType reward = _novelty->computeNovelty(oldState, action, newState);
	rewardSocket(reward);
	return reward;
}

//TODO: copying graph of objects

/**
* Assignment operator.
**/
NoveltyReward& NoveltyReward::operator=(const NoveltyReward& obj) {
	RewardFunction::operator=(obj);
//	_novelty = clone(obj._novelty);

	return *this;
}

/**
* Copy constructor.
**/
NoveltyReward::NoveltyReward(const NoveltyReward& obj):
	RewardFunction(obj), _novelty() {}

/**
* Constructor.
* @param noveltyDetector The novelty detector used to determine novelty.
*
* \note The detector will generally need to be added as a listener to work
* correctly. It does not happen automagically.
**/
NoveltyReward::NoveltyReward(const shared_ptr<NoveltyDetector>& noveltyDetector):
	RewardFunction(), _novelty(noveltyDetector) {}

/**
* Default constructor - should for the most part only be used for serialization.
**/
NoveltyReward::NoveltyReward(): RewardFunction(), _novelty() {}

NoveltyReward::NoveltyReward(StateProperties* /*properties*/):
	RewardFunction(), _novelty() {}

}//namespace motivator
