#include "QScalarization_Chebyshev.h"
#include <cmath>
#include <limits>

namespace motivator {

/**
 * Computes the rewards using each reward function, and updates the utopia.
 */
void QScalarization_Chebyshev::nextStep(StateCollection* oldState, Action* action, StateCollection* /*newState*/) {
	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		RealType utopianValue = iter->first->getValue(oldState, action, nullptr) + _tau;
		if(utopianValue > iter->second.utopia) iter->second.utopia = utopianValue;
	}
}

/**
 * Upon reaching a new episode, we reset the utopian point.
 */
void QScalarization_Chebyshev::newEpisode() {
	for(auto& entry: _functionMap) {
		entry.second.resetUtopianPoint();
	}
}

/**
 * Applies scalarization to compute the value of the state.
 */
RealType QScalarization_Chebyshev::getValue(StateCollection* state,
		Action* action, ActionData* data)
{
	RealType maxValue = -std::numeric_limits<RealType>::max();

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		RealType value = iter->second.weight * std::abs(iter->first->getValue(state, action, data) - iter->second.utopia);
		if(value > maxValue) maxValue = value;
	}

	return _coeff*maxValue;
}

/**
 * Normalises the sum of all weights to factor.
 */
void QScalarization_Chebyshev::normWeights(RealType factor) {
	RealType sum = 0.0;

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		sum += iter->second.weight;
	}

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		iter->second.weight *= factor / sum;
	}
}

QScalarization_Chebyshev::QScalarization_Chebyshev(RealType tau):
	AbstractQFunction(nullptr), _functionMap(), _tau(tau) {}

} //namespace motivator
