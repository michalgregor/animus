//#include "NoveltyDetector_PairForecaster.h"
//#include <Annalyst/Forecasting/PairForecaster_OTFA.h>
//
//
//namespace motivator {
//
///**
// * Boost serialization function.
// **/
//void NoveltyDetector_PairForecaster::serialize(IArchive& ar,
//        const unsigned int UNUSED(version)) {
//	ar & boost::serialization::base_object<NoveltyDetector>(*this);
//
//	ar & _forecaster;
//	ar & _novelty;
//	ar & _discreteStatesInput;
//	ar & _continuousStatesInput;
//	ar & _discreteStatesOutput;
//	ar & _continuousStatesOutput;
//	ar & _inputActionEncoder;
//
//	//TODO: _properties
//	//ar & _properties;
//}
//
//void NoveltyDetector_PairForecaster::serialize(OArchive& ar,
//        const unsigned int UNUSED(version)) {
//	ar & boost::serialization::base_object<NoveltyDetector>(*this);
//	ar & _forecaster;
//	ar & _novelty;
//	ar & _discreteStatesInput;
//	ar & _continuousStatesInput;
//	ar & _discreteStatesOutput;
//	ar & _continuousStatesOutput;
//	ar & _inputActionEncoder;
//
//	//TODO: _properties
//	//ar & _properties;
//}
//
///**
// * Computes the state vector used by the forecaster.
// */
//std::vector<RealType> NoveltyDetector_PairForecaster::computeStateVector(
//        State* state, Action* action, const std::vector<unsigned int>& discreteStates,
//        const std::vector<unsigned int>& continuousStates,
//        const shared_ptr<ActionEncoder>& actionEncoder) {
//
//	auto numDiscrete = discreteStates.size(), numContinuous =
//			continuousStates.size(), actionSize = actionEncoder?(actionEncoder->size()):(0),
//			numStates = numDiscrete + numContinuous + actionSize;
//	decltype(numDiscrete) i = 0;
//
//	std::vector<RealType> stateVector(numStates, 0);
//
//	//DISCRETE STATES
//	for (decltype(numDiscrete) j = 0; j < numDiscrete; j++, i++) {
//		stateVector[i] = numeric_cast<RealType>(
//				state->getDiscreteState(discreteStates[j]));
//	}
//
//	//CONTINUOUS STATES
//	for (decltype(numContinuous) j = 0; j < numContinuous; j++, i++) {
//		stateVector[i] = numeric_cast<RealType>(
//				state->getContinuousState(continuousStates[j]));
//	}
//
//	if(actionEncoder) {
//		std::vector<RealType> actionCode = actionEncoder->operator()(action);
//
//		//ACTION
//		for (decltype(numContinuous) j = 0; j < actionSize; j++, i++) {
//			stateVector[i] = actionCode[j];
//		}
//	}
//
//	return stateVector;
//}
//
///**
// * This method updates the memory of the detector.
// */
//void NoveltyDetector_PairForecaster::nextStep(StateCollection* oldStateCol,
//        Action* action, StateCollection* newStateCol) {
//	std::vector<RealType> inputObservation = computeStateVector(newStateCol->getState(), action, _discreteStatesInput, _continuousStatesInput, _inputActionEncoder);
//	std::vector<RealType> outputObservation = computeStateVector(newStateCol->getState(), nullptr, _discreteStatesOutput, _continuousStatesOutput, shared_nullptr);
//
////	std::vector<RealType> newOutputVector = computeStateVector(newStateCol->getState(), nullptr, _discreteStatesOutput, _continuousStatesOutput, shared_nullptr);
//
//	_forecaster->addObservation(inputObservation, outputObservation);
////	_forecaster->getPrediction(
////	        std::vector<std::vector<RealType> >(1, newOutputVector), _novelty);
//
//
//
////	if(_oldState) delete _oldState;
////	if(_newState) delete _newState;
////
////	_oldState = oldStateCol->getState()->clone();
////	_newState = newStateCol->getState()->clone();
//}
//
///**
// * Returns how novel current situation (state, state transition, ...) is.
// * Memory of the detector is NOT updated by this function.
// */
//RealType NoveltyDetector_PairForecaster::computeNovelty(StateCollection* oldState, Action* action, StateCollection *newState) {
////	std::cout << "computeNovelty: " << newState->getState()->getDiscreteState(4) << ", " << (_inputActionEncoder->operator ()(action))[0] << std::endl;
////	std::cout << "computeNovelty: " << oldState << "; " << action << "; " << newState << std::endl;
//
////	if(!_oldState || !_newState) std::cout << "no comp" << std::endl;
////	else if(!_newState->equals(oldState->getState())) {
////		std::cout << "comp: old(";
////
////		for(unsigned int i = 0; i < _newState->getNumDiscreteStates(); i++) {
////			std::cout << _newState->getDiscreteState(i) << ", ";
////		}
////
////		std::cout << "); \t";
////
////		for(unsigned int i = 0; i < oldState->getState()->getNumDiscreteStates(); i++) {
////			std::cout << oldState->getState()->getDiscreteState(i) << ", ";
////		}
////
////		std::cout << "); " << std::endl;
////
////
////	} else std::cout << "comp false" << std::endl;
//
////	else std::cout << "old state ==: " << (_oldState->equals(oldState->getState())) << " ; new state ==: " << (_newState->equals(newState->getState())) << std::endl;
//
//	std::vector<RealType> newStateVector = computeStateVector(newState->getState(), nullptr, _discreteStatesOutput, _continuousStatesOutput, shared_nullptr);
//
//	_forecaster->getPrediction(
//	        std::vector<std::vector<RealType> >(1, newStateVector), _novelty);
//
//	reporterSocket(_novelty);
//
//	return _novelty;
//}
//
///**
// * Returns how novel current situation (state, state transition, ...) is.
// */
//RealType NoveltyDetector_PairForecaster::getNovelty() const {
//	return _novelty;
//}
//
///**
// * Resets the NoveltyDetector to its initial state erasing all its memory
// * thus effecting a complete recovery.
// */
//void NoveltyDetector_PairForecaster::reset() {
//	_forecaster->reset();
//}
//
///**
// * Assignment operator.
// **/
//NoveltyDetector_PairForecaster& NoveltyDetector_PairForecaster::operator=(
//        const NoveltyDetector_PairForecaster& obj) {
//	NoveltyDetector::operator=(obj);
//
//	_forecaster = shared_ptr<PairForecaster>(clone(obj._forecaster));
//	_novelty = obj._novelty;
//	_discreteStatesInput = obj._discreteStatesInput;
//	_continuousStatesInput = obj._continuousStatesInput;
//	_discreteStatesOutput = obj._discreteStatesOutput;
//	_continuousStatesOutput = obj._continuousStatesOutput;
//
//	_inputActionEncoder = obj._inputActionEncoder;
//
//	//TODO: is it right to copy action encoder this way?
//
//	return *this;
//}
//
///**
// * Copy constructor.
// **/
//NoveltyDetector_PairForecaster::NoveltyDetector_PairForecaster(
//        const NoveltyDetector_PairForecaster& obj) :
//		NoveltyDetector(obj), _forecaster(clone(obj._forecaster)), _novelty(
//		        obj._novelty), _discreteStatesInput(obj._discreteStatesInput),
//		        _continuousStatesInput(obj._continuousStatesInput),
//		        _discreteStatesOutput(obj._discreteStatesOutput),
//		        _continuousStatesOutput(obj._continuousStatesOutput),
//		        _inputActionEncoder(obj._inputActionEncoder) {
//}
//
///**
// * Default constructor. To be used for serialization mainly. Does not create
// * a forecaster.
// **/
//NoveltyDetector_PairForecaster::NoveltyDetector_PairForecaster() :
//		_forecaster(), _novelty(0), _discreteStatesInput(), _continuousStatesInput(),
//		_discreteStatesOutput(), _continuousStatesOutput(), _inputActionEncoder()
//		{
//}
//
///**
// * Default constructor.
// *
// * @param discreteStatesInput Specifies which discrete state dimensions to use
// * as input observations.
// * @param continuousStatesInput Specifies which continuous state dimensions to use
// * as input observations.
// * @param
// *
// * @param numStates The number of state dimensions that will be forecast
// * (associated with the number of inputs to the multi-forecaster).
// **
// * Creates a default OTFA-based multi-forecaster. The default forecaster will
// * likely not be perfectly tuned for every possible task.
// */
//NoveltyDetector_PairForecaster::NoveltyDetector_PairForecaster(
//		std::vector<unsigned int> discreteStatesInput,
//		std::vector<unsigned int> continuousStatesInput,
//		std::vector<unsigned int> discreteStatesOutput,
//		std::vector<unsigned int> continuousStatesOutput,
//        const shared_ptr<ActionEncoder>& inputActionEncoder
//):
//		_forecaster(), _novelty(0), _discreteStatesInput(std::move(discreteStatesInput)),
//		_continuousStatesInput(std::move(continuousStatesInput)),
//		_discreteStatesOutput(std::move(discreteStatesOutput)),
//		_continuousStatesOutput(std::move(continuousStatesOutput)),
//		_inputActionEncoder(inputActionEncoder) {
//
//	NetworkSize dimensionInput = _discreteStatesInput.size() + _continuousStatesInput.size();
//	if(_inputActionEncoder) dimensionInput += _inputActionEncoder->size();
//
//	NetworkSize dimensionOutput = _discreteStatesOutput.size() + _continuousStatesOutput.size();
//
//	_forecaster = shared_ptr<PairForecaster_OTFA>(
//	        new PairForecaster_OTFA( { 5 * dimensionInput, 5 * dimensionOutput },
//	                std::vector<unsigned int>(dimensionInput, 1), std::vector<unsigned int>(dimensionOutput, 1), 1, 1, 1, 10));
//}
//
///**
// * Constructor.
// *
// * @param stateType Whether to use discrete or continuous states.
// * @param forecaster The forecaster to be used to determine novelty.
// */
//NoveltyDetector_PairForecaster::NoveltyDetector_PairForecaster(
//		std::vector<unsigned int> discreteStatesInput,
//		std::vector<unsigned int> continuousStatesInput,
//		std::vector<unsigned int> discreteStatesOutput,
//		std::vector<unsigned int> continuousStatesOutput,
//        const shared_ptr<PairForecaster>& forecaster,
//        const shared_ptr<ActionEncoder>& inputActionEncoder):
//		_forecaster(forecaster), _novelty(0), _discreteStatesInput(std::move(discreteStatesInput)),
//		_continuousStatesInput(std::move(continuousStatesInput)),
//		_discreteStatesOutput(std::move(discreteStatesOutput)),
//		_continuousStatesOutput(std::move(continuousStatesOutput)),
//		_inputActionEncoder(inputActionEncoder) {
//}
//
//} //namespace motivator
