#include "NoveltyDetector_HSOM_Multistep.h"
#include <Systematic/generator/BoundaryGenerator.h>

namespace motivator {

/**
 * Boost serialization function.
 **/
void NoveltyDetector_HSOM_Multistep::serialize(IArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyDetector>(*this);

	ar & _hsom;
	ar & _discreteStates;
	ar & _continuousStates;

	ar & _numSteps;
	ar & _posBegin;
	ar & _stateVectors;

	//TODO:serialization of StateProperties
	//ar & _properties;
}

void NoveltyDetector_HSOM_Multistep::serialize(OArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyDetector>(*this);

	ar & _hsom;
	ar & _discreteStates;
	ar & _continuousStates;

	ar & _numSteps;
	ar & _posBegin;
	ar & _stateVectors;

	//TODO:serialization of StateProperties
	//ar & _properties;
}

/**
 * Computes a state vector corresponding to the current state.
 */
void NoveltyDetector_HSOM_Multistep::computeStateVector(State* state, RowVector& stateVector) {
	auto numDiscrete = _discreteStates.size(), numContinuous =
			_continuousStates.size(), numStates = numDiscrete + numContinuous;
	unsigned int i = 0;

	stateVector.resize(numStates);

	//DISCRETE STATES
	for (unsigned int j = 0; j < numDiscrete; j++, i++) {
		stateVector[i] = numeric_cast<RealType>(
				state->getDiscreteState(_discreteStates[j]));
	}

	//CONTINUOUS STATES
	for (unsigned int j = 0; j < numContinuous; j++, i++) {
		stateVector[i] = numeric_cast<RealType>(
				state->getContinuousState(_continuousStates[j]));
	}
}

/**
 * This method updates the memory of the detector.
 */
void NoveltyDetector_HSOM_Multistep::nextStep(
	StateCollection* /*oldState*/, Action* /*action*/, StateCollection* newState
) {
	computeStateVector(newState->getState(), getNewStateVector());

	if(_stateVectors.size() == _numSteps) {
		//copy all the stuff into the dataset

		auto vecsize = _stateVectors.at(0).size();
		auto reserveSize = vecsize * _stateVectors.size();

		if(_dataset.cols() != reserveSize) {
			_dataset.resize(1, reserveSize);
		}

		auto input = _dataset.row(0);
		unsigned int  num = _posBegin + _numSteps;
		unsigned int ipos = 0;

		for(unsigned int i = _posBegin; i < num; i++) {
			input.segment(ipos, vecsize) = _stateVectors[i%_numSteps];
			ipos += vecsize;
		}

		//ADD OBSERVATION, GET PREDICTION
		_hsom->fit(_dataset);
	}
}

/**
 * Returns how novel current situation (state, state transition, ...) is.
 * Memory of the detector is NOT updated by this function.
 */
RealType NoveltyDetector_HSOM_Multistep::computeNovelty(StateCollection* UNUSED(oldState), Action* UNUSED(action), StateCollection *newState) {
	RealType novelty = 1_r;

	if(_stateVectors.size() == _numSteps) {
		RowVector currentStateVector;
		computeStateVector(newState->getState(), currentStateVector);

		auto vecsize = _stateVectors.at(0).size();
		auto reserveSize = vecsize * _stateVectors.size();
		RowVector input((AlgebraIndex) reserveSize);

		unsigned int num = _posBegin + _numSteps;
		decltype(vecsize) ipos = 0;

		for(unsigned int i = _posBegin + 1; i < num; i++) {
			input.segment(ipos, vecsize) = _stateVectors[i%_numSteps];
			ipos += vecsize;
		}

		input.segment(ipos, vecsize) = currentStateVector;
		novelty = _hsom->computeNovelty(input);
	}

	reporterSocket(novelty);
	return novelty;
}

/**
 * Resets the NoveltyDetector to its initial state erasing all its memory
 * thus effecting a complete recovery.
 */
void NoveltyDetector_HSOM_Multistep::reset() {
	_hsom->resetHabituation();
	clearStateVectors();
}

/**
 * Default constructor. To be used for serialization mainly. Does not create
 * a forecaster.
 **/
NoveltyDetector_HSOM_Multistep::NoveltyDetector_HSOM_Multistep() :
		_hsom(), _dataset(), _discreteStates(), _continuousStates(), _stateVectors() {
}

/**
 * Default constructor.
 *
 * @param stateType Whether to use discrete states, continuous states, or both.
 * @param numStates The number of state dimensions that will be forecast
 * (associated with the number of inputs to the multi-forecaster).
 **
 * Creates a default OTFA-based multi-forecaster. The default forecaster will
 * likely not be perfectly tuned for every possible task.
 */
NoveltyDetector_HSOM_Multistep::NoveltyDetector_HSOM_Multistep(
	unsigned int numSteps,
	std::vector<unsigned int> discreteStates,
	std::vector<unsigned int> continuousStates
):
	_hsom(), _dataset(), _discreteStates(std::move(discreteStates)),
	_continuousStates(std::move(continuousStates)), _numSteps(numSteps), _stateVectors()
{
	NetworkSize numStates = numeric_cast<NetworkSize>(
			_discreteStates.size() + _continuousStates.size());
	BoundaryGenerator<RealType> generator(-0.5, 0.5);

	_hsom = make_shared<Network_HSOM>(generator, numStates*_numSteps, numStates*_numSteps, numStates*_numSteps);
}

/**
 * Constructor.
 *
 * @param stateType Whether to use discrete or continuous states.
 * @param hsom The habituated SOM to be used to determine novelty.
 */
NoveltyDetector_HSOM_Multistep::NoveltyDetector_HSOM_Multistep(
		unsigned int numSteps,
        std::vector<unsigned int> discreteStates,
        std::vector<unsigned int> continuousStates,
        const shared_ptr<Network_HSOM>& hsom) :
		_hsom(hsom), _dataset(), _discreteStates(std::move(discreteStates)), _continuousStates(std::move(
		        continuousStates)), _numSteps(numSteps), _stateVectors()
{}

} //namespace motivator
