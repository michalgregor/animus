#include "VScalarization_ChebyshevReward.h"
#include <cmath>
#include <limits>

namespace motivator {

/**
 * Computes the rewards using each reward function, and updates the utopia.
 */
void VScalarization_ChebyshevReward::nextStep(StateCollection* oldState, Action* action, StateCollection* newState) {
	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		RealType utopiaReward = iter->second.rewardFunction->getReward(oldState, action, newState) + _tau;
		if(utopiaReward > iter->second.utopia) iter->second.utopia = utopiaReward;
	}
}

/**
 * Upon reaching a new episode, we reset the utopian point.
 */
void VScalarization_ChebyshevReward::newEpisode() {
	for(auto& entry: _functionMap) {
		entry.second.resetUtopianPoint();
	}
}

/**
 * Applies scalarization to compute the value of the state.
 */
RealType VScalarization_ChebyshevReward::getValue(StateCollection* state) {
	RealType maxValue = -std::numeric_limits<RealType>::max();

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		RealType value = iter->second.weight * std::abs(iter->first->getValue(state) - iter->second.utopia);
		if(value > maxValue) maxValue = value;
	}

	return _coeff*maxValue;
}

/**
 * Normalises the sum of all weights to factor.
 */
void VScalarization_ChebyshevReward::normWeights(RealType factor) {
	RealType sum = 0.0;

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		sum += iter->second.weight;
	}

	for(auto iter = _functionMap.begin(); iter != _functionMap.end(); iter++) {
		iter->second.weight *= factor / sum;
	}
}

VScalarization_ChebyshevReward::VScalarization_ChebyshevReward(RealType tau):
	AbstractVFunction(nullptr), _functionMap(), _tau(tau) {}

} //namespace motivator
