#include "NoveltyDetector_Forecaster.h"

namespace motivator {

/**
 * Boost serialization function.
 **/
void NoveltyDetector_Forecaster::serialize(IArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyDetector>(*this);

	ar & _forecaster;
	ar & _novelty;
	ar & _discreteStates;
	ar & _continuousStates;

	//TODO: _properties
	//ar & _properties;
}

void NoveltyDetector_Forecaster::serialize(OArchive& ar,
        const unsigned int UNUSED(version)) {
	ar & boost::serialization::base_object<NoveltyDetector>(*this);
	ar & _forecaster;
	ar & _novelty;
	ar & _discreteStates;
	ar & _continuousStates;

	//TODO: _properties
	//ar & _properties;
}

/**
 * Computes the state vector used by the forecaster.
 */
RowVector NoveltyDetector_Forecaster::computeStateVector(
        State* state) {

	auto numDiscrete = _discreteStates.size(), numContinuous =
	        _continuousStates.size(), numStates = numDiscrete + numContinuous;
	decltype(numDiscrete) i = 0;

	RowVector stateVector = RowVector::Zero(numStates);

	//DISCRETE STATES
	for (decltype(numDiscrete) j = 0; j < numDiscrete; j++, i++) {
		stateVector[i] = numeric_cast<RealType>(
				state->getDiscreteState(_discreteStates[j]));
	}

	//CONTINUOUS STATES
	for (decltype(numContinuous) j = 0; j < numContinuous; j++, i++) {
		stateVector[i] = numeric_cast<RealType>(
				state->getContinuousState(_continuousStates[j]));
	}

	return stateVector;
}

/**
 * This method updates the memory of the detector.
 */
void NoveltyDetector_Forecaster::nextStep(
	StateCollection* /*oldStateCol*/,
    Action* UNUSED(action), StateCollection* newStateCol)
{
//	std::vector<RealType> oldStateVector = computeStateVector(oldStateCol->getState());
	RowVector newstateVector = computeStateVector(newStateCol->getState());

//	_forecaster->addObservation(oldStateVector);
	_forecaster->addObservation(newstateVector);
//	_forecaster->getPrediction(
//	        std::vector<std::vector<RealType> >(1, newStateVector), _novelty);
}

/**
 * Returns how novel current situation (state, state transition, ...) is.
 * Memory of the detector is NOT updated by this function.
 */
RealType NoveltyDetector_Forecaster::computeNovelty(StateCollection* UNUSED(oldState), Action* /*action*/, StateCollection *newState) {
	RowVector newStateVector = computeStateVector(newState->getState());
	_forecaster->getPrediction(MatrixCRef(newStateVector), _novelty);
	reporterSocket(_novelty);
	return _novelty;
}

/**
 * Returns how novel current situation (state, state transition, ...) is.
 */
RealType NoveltyDetector_Forecaster::getNovelty() const {
	return _novelty;
}

/**
 * Resets the NoveltyDetector to its initial state erasing all its memory
 * thus effecting a complete recovery.
 */
void NoveltyDetector_Forecaster::reset() {
	_forecaster->reset();
}

/**
 * Default constructor. To be used for serialization mainly. Does not create
 * a forecaster.
 **/
NoveltyDetector_Forecaster::NoveltyDetector_Forecaster() :
		_forecaster(), _novelty(0), _discreteStates(), _continuousStates() {
}

///**
// * Default constructor.
// *
// * @param stateType Whether to use discrete states, continuous states, or both.
// * @param numStates The number of state dimensions that will be forecast
// * (associated with the number of inputs to the multi-forecaster).
// **
// * Creates a default OTFA-based multi-forecaster. The default forecaster will
// * likely not be perfectly tuned for every possible task.
// */
//NoveltyDetector_Forecaster::NoveltyDetector_Forecaster(
//        std::vector<unsigned int> discreteStates,
//        std::vector<unsigned int> continuousStates) :
//		_forecaster(), _novelty(0), _discreteStates(std::move(discreteStates)), _continuousStates(
//		        std::move(continuousStates)) {
//
//	NetworkSize numStates = numeric_cast<NetworkSize>(
//			_discreteStates.size() + _continuousStates.size());
//	_forecaster = shared_ptr<Forecaster>(
//	        new Forecaster_OTFA( { 5 * numStates, 5 * numStates },
//	                std::vector<unsigned int>(numStates, 1), 1, 1, 1, 10));
//}

/**
 * Constructor.
 *
 * @param stateType Whether to use discrete or continuous states.
 * @param forecaster The forecaster to be used to determine novelty.
 */
NoveltyDetector_Forecaster::NoveltyDetector_Forecaster(
		std::vector<unsigned int> discreteStates,
		std::vector<unsigned int> continuousStates,
        const shared_ptr<Forecaster>& forecaster) :
		_forecaster(forecaster), _novelty(0), _discreteStates(std::move(discreteStates)), _continuousStates(
		        std::move(continuousStates)) {
}

} //namespace motivator
