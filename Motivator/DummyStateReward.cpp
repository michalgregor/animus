#include "DummyStateReward.h"
#include <Rlearner/state.h>

namespace motivator {

RealType DummyStateReward::getStateReward(State *modelState) {
	return _rewardFunction->getReward(modelState, nullptr, modelState);
}

DummyStateReward::DummyStateReward(): StateReward(nullptr), _rewardFunction() {}

/**
* Constructor.
**/
DummyStateReward::DummyStateReward(StateProperties *properties, const shared_ptr<RewardFunction>& rewardFunction):
StateReward(properties), _rewardFunction(rewardFunction) {}

}//namespace motivator
