#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE "AlgebraTest"

#include <Systematic/math/algebra.h>
#include <boost/test/unit_test.hpp>

using namespace systematic;

BOOST_AUTO_TEST_CASE( matrix_constructors )
{
	{
		Matrix mat(3, 3);

		BOOST_CHECK(mat.rows() == 3);
		BOOST_CHECK(mat.cols() == 3);
	}

	{
		Matrix mat = {
			{1, 2, 3},
			{4, 5, 6},
			{7, 8, 9}
		};

		BOOST_CHECK(mat.rows() == 3);
		BOOST_CHECK(mat.cols() == 3);

		BOOST_CHECK(mat(0, 0) == 1);
		BOOST_CHECK(mat(0, 1) == 2);
		BOOST_CHECK(mat(0, 2) == 3);
		BOOST_CHECK(mat(1, 0) == 4);
		BOOST_CHECK(mat(1, 1) == 5);
		BOOST_CHECK(mat(1, 2) == 6);
		BOOST_CHECK(mat(2, 0) == 7);
		BOOST_CHECK(mat(2, 1) == 8);
		BOOST_CHECK(mat(2, 2) == 9);
	}
}

BOOST_AUTO_TEST_CASE( matrix_resize )
{
	{
		Matrix mat = {
			{1, 2, 3},
			{4, 5, 6},
			{7, 8, 9}
		};

		mat.conservativeResize(5, 5);

		BOOST_CHECK(mat.rows() == 5);
		BOOST_CHECK(mat.cols() == 5);

		BOOST_CHECK(mat(0, 0) == 1);
		BOOST_CHECK(mat(0, 1) == 2);
		BOOST_CHECK(mat(0, 2) == 3);
		BOOST_CHECK(mat(1, 0) == 4);
		BOOST_CHECK(mat(1, 1) == 5);
		BOOST_CHECK(mat(1, 2) == 6);
		BOOST_CHECK(mat(2, 0) == 7);
		BOOST_CHECK(mat(2, 1) == 8);
		BOOST_CHECK(mat(2, 2) == 9);
	}

	{
		Matrix mat = {
			{1, 2, 3},
			{4, 5, 6},
			{7, 8, 9}
		};

		mat.conservativeResize(2, 2);

		BOOST_CHECK(mat.rows() == 2);
		BOOST_CHECK(mat.cols() == 2);

		BOOST_CHECK(mat(0, 0) == 1);
		BOOST_CHECK(mat(0, 1) == 2);
		BOOST_CHECK(mat(1, 0) == 4);
		BOOST_CHECK(mat(1, 1) == 5);
	}
}

BOOST_AUTO_TEST_CASE( rowvector_resize )
{
	{
		RowVector vec = {1, 2, 3, 4, 5, 6};
		vec.conservativeResize(3);

		BOOST_CHECK(vec.size() == 3);

		BOOST_CHECK(vec[0] == 1);
		BOOST_CHECK(vec[1] == 2);
		BOOST_CHECK(vec[2] == 3);
	}

	{
		RowVector vec = {1, 2, 3};
		vec.conservativeResize(6);

		BOOST_CHECK(vec.size() == 6);

		BOOST_CHECK(vec[0] == 1);
		BOOST_CHECK(vec[1] == 2);
		BOOST_CHECK(vec[2] == 3);
	}
}

