#include "Network_HSOM.h"
#include <algorithm>

namespace annalyst {

/**
 * Boost serialization function.
 **/
void Network_HSOM::serialize(IArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Network_SOM>(*this);
	ar & _habituationRate;
	ar & _recoveryRate;
	ar & _normalize;
	ar & _habituationNeighbourhoodRadius;
	ar & _habituableWeights;
}

void Network_HSOM::serialize(OArchive& ar, const unsigned int) {
	ar & boost::serialization::base_object<Network_SOM>(*this);
	ar & _habituationRate;
	ar & _recoveryRate;
	ar & _normalize;
	ar & _habituationNeighbourhoodRadius;
	ar & _habituableWeights;
}

void Network_HSOM::resetHabituation() {
	_habituableWeights.setConstant(_initWeight);
}

RealType Network_HSOM::habituate(const RowVectorCRef& data) {
	auto winner = predict(data);
	return habituate(winner);
}

RowVector Network_HSOM::habituate(const MatrixCRef& data) {
	RowVector res(data.rows());

	for(AlgebraIndex i = 0; i < data.rows(); i++) {
		auto winner = predict(data.row(i));
		res[i] = habituate(winner);
	}

	return res;
}

RealType Network_HSOM::habituate(NetworkSize winner) {
	auto winnerPos = _topology.computeGridPosition(winner);
	auto neigh = _topology.createNeighbourhood(winnerPos, _habituationNeighbourhoodRadius);

	RowVector activations = RowVector::Constant(_habituableWeights.size(), 0);
	for(auto& n: neigh) {
		activations[n.neuronIndex] = neigh.nearness(n);
	}

	if(_normalize) {
		RealType sum = 0, norm = 0;

		for(AlgebraIndex i = 0; i < _habituableWeights.size(); i++) {
			sum += activations[i] * _habituableWeights[i];
			norm += activations[i];

			_habituableWeights[i] += _recoveryRate * (_initWeight - _habituableWeights[i])
				- _habituationRate * activations[i];
		}

		return sum / norm;
	} else {
		RealType sum = 0;

		for(AlgebraIndex i = 0; i < _habituableWeights.size(); i++) {
			sum += activations[i] * _habituableWeights[i];

			_habituableWeights[i] += _recoveryRate * (_initWeight - _habituableWeights[i])
				- _habituationRate * activations[i];
		}

		return sum;
	}
}

RealType Network_HSOM::computeNovelty(const RowVectorCRef& data) const {
	auto winner = predict(data);
	return computeNovelty(winner);
}

RowVector Network_HSOM::computeNovelty(const MatrixCRef& data) const {
	RowVector res(data.rows());

	for(AlgebraIndex i = 0; i < data.rows(); i++) {
		auto winner = predict(data.row(i));
		res[i] = computeNovelty(winner);
	}

	return res;
}

RealType Network_HSOM::computeNovelty(NetworkSize winner) const {
	auto winnerPos = _topology.computeGridPosition(winner);
	auto neigh = _topology.createNeighbourhood(winnerPos, _habituationNeighbourhoodRadius);

	RowVector activations = RowVector::Constant(_habituableWeights.size(), 0);
	for(auto& n: neigh) {
		activations[n.neuronIndex] = neigh.nearness(n);
	}

	if(_normalize) {
		RealType sum = 0, norm = 0;

		for(AlgebraIndex i = 0; i < _habituableWeights.size(); i++) {
			sum += activations[i] * _habituableWeights[i];
			norm += activations[i];
		}

		return sum / norm;
	} else {
		RealType sum = 0;

		for(AlgebraIndex i = 0; i < _habituableWeights.size(); i++) {
			sum += activations[i] * _habituableWeights[i];
		}

		return sum;
	}
}

Matrix Network_HSOM::computeHabituationMatrix() const {
	Matrix habit(_topology.rows(), _topology.cols());

	for(NetworkSize i = 0; i < _topology.rows(); i++) {
		for(NetworkSize j = 0; j < _topology.cols(); j++) {
			habit.coeffRef(i, j) = _habituableWeights[_topology.computeNeuronIndex(i, j)];
		}
	}

	return habit;
}

Network_HSOM::Network_HSOM(): Network_SOM(), _habituableWeights() {}

Network_HSOM::Network_HSOM(
	Generator<RealType>& generator, NetworkSize numInputs,
	NetworkSize numRows, NetworkSize numCols
):
	Network_SOM(generator, numInputs, numRows, numCols),
	_habituableWeights(RowVector::Constant(numClusters(), _initWeight))
{}

Network_HSOM::Network_HSOM(
	Generator<RowVector>& generator, NetworkSize numRows, NetworkSize numCols
):
	Network_SOM(generator, numRows, numCols),
	_habituableWeights(RowVector::Constant(numClusters(), _initWeight))
{}

Network_HSOM::Network_HSOM(
	const MatrixCRef& matrix, NetworkSize numRows, NetworkSize numCols
):
	Network_SOM(matrix, numRows, numCols),
	_habituableWeights(RowVector::Constant(numClusters(), _initWeight))
{}

} //namespace annalyst
