#ifndef Annalyst_Forecasting_Forecaster_H_
#define Annalyst_Forecasting_Forecaster_H_

#include "system.h"
#include "algebra.h"

namespace annalyst {

/**
* @class Forecaster
* @author Michal Gregor
*
* A common interface of multi-observation forecasters (several values are
* presented to the forecaster at every time step).
**/
class ANNALYST_API Forecaster {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version) {}
	void serialize(OArchive& ar, const unsigned int version) {}

private:
	/**
	 * IMPLEMENT IN EVERY NON-ABSTRACT DERIVED CLASS!!! COPY THIS DESCRIPTION
	 * TO EVERY DERIVED CLASS.
	 *
	 * Used to create a copy of the object of the most-derived class.
	 */
	SYS_CLONEABLE_VOID()

public:
	/**
	 * Resets the Forecaster into its initial state so that it forgets all it
	 * has learned from added observations.
	 */
	virtual void reset() = 0;

	/**
	 * Returns a prediction. The prediction is based on observations presented
	 * using the addObservation methods. The prediction is returned as a const
	 * reference so that it does not have to be recomputed on every call to
	 * getPrediction().
	 */
	virtual Matrix getPrediction() = 0;

	/**
	 * Returns a prediction. The prediction is based on observations presented
	 * using the addObservation methods. The prediction is returned as a const
	 * reference so that it does not have to be recomputed on every call to
	 * getPrediction().
	 *
	 * This version also computes a forecaster-specific error measure.
	 */
	virtual Matrix getPrediction(const MatrixCRef& correctPredictions, RealType& error) = 0;

	/**
	 * Adds an observation (next term in a time-series).
	 */
	virtual void addObservation(const RowVectorCRef& observation) = 0;

	/**
	 * Adds multiple observations one by one.
	 */
	virtual void addObservation(const MatrixCRef& observation) = 0;

public:
	virtual ~Forecaster() = default;
};

}//namespace annalyst

SYS_EXPORT_CLASS(annalyst::Forecaster)

#endif //Annalyst_Forecasting_Forecaster_H_
