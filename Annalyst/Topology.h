#ifndef Annalyst_Topology_H_
#define Annalyst_Topology_H_

#include "system.h"
#include <cmath>

namespace annalyst {

class LayerRef;

/**
 * Layer topology type (for SOMs).
 */
enum TopologyType {
	//! Rectangular topology.
	TOT_rectangular,
	//! Hexagonal topology.
	TOT_hexagonal
};

/**
 * Neighbourhood type - determines in what way nearness
 * is calculated from topological distance.
 */
enum NeighbourhoodType {
	//! Linear: nearness = max(1 - distance / radius * 0.9, 0).
	NT_linear,
	//! Gaussian: nearness = exp(-distance*distance / radius).
	NT_gaussian,
	//! Unitary -- nearness equals 1 for all neighbours.
	NT_unitary
};

/**
 * Converts hexagonal (cube) coordinates hx, hy, hz to rectangular
 * (even-r offset) coordinates rx, ry.
 */
inline void hex2rect(
	NetworkSizeDiff hx, NetworkSizeDiff /*hy*/, NetworkSizeDiff hz,
	NetworkSizeDiff& rx, NetworkSizeDiff& ry
) {
	rx = hz;
	ry = hx + (hz - (hz & 1)) / 2;
}

/**
 * Converts rectangular (even-r offset) coordinates rx, ry to hexagonal (cube)
 * coordinates hx, hy, hz.
 */
inline void rect2hex(
	NetworkSizeDiff rx, NetworkSizeDiff ry,
	NetworkSizeDiff& hx, NetworkSizeDiff& hy, NetworkSizeDiff& hz
) {
	hx = ry - (rx - (rx & 1))/2;
	hz = rx;
	hy = -hx - hz;
}

/**
 * A structure representing a neighbour in a neighbourhood.
 */
struct Neighbour {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

public:
	//! Row coordinate of the neighbour in the grid.
	NetworkSize m;
	//! Column coordinate of the neighbour in the grid.
	NetworkSize n;
	//! Index of the associated neuron in the neuron container.
	NetworkSize neuronIndex;
	//! Distance of the neighbour from neighbourhood centre.
	NetworkSize distance;

	Neighbour(): m(0), n(0), neuronIndex(0), distance(0) {}

	/**
	 * @param m_ @copydoc m
	 * @param n_ @copydoc n
	 * @param neuronIndex_ @copydoc neuronIndex
	 * @param distance_ @copydoc distance
	 */
	Neighbour(
		NetworkSize m_, NetworkSize n_,
		NetworkSize neuronIndex_, NetworkSize distance_
	): m(m_), n(n_), neuronIndex(neuronIndex_), distance(distance_) {}
};

/**
 * Computes nearness given a Neighbour, radius and type of the neighbourhood.
 */
RealType nearness(
	const Neighbour& n,
	RealType radius,
	NeighbourhoodType ntype
);

/**
 * A class representing a neighbourhood.
 */
class Neighbourhood {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	typedef std::vector<Neighbour> vectype;

	//! Radius of the neighbourhood.
	RealType _radius;
	//! Type of the neighbourhood.
	NeighbourhoodType _neighbourhoodType;
	//! The list of all neighbours including the centre.
	vectype _neighbours;

public:
	typedef vectype::value_type value_type;
	typedef vectype::reference reference;
	typedef vectype::iterator iterator;
	typedef vectype::const_iterator const_iterator;
	typedef vectype::reverse_iterator reverse_iterator;
	typedef vectype::const_reverse_iterator const_reverse_iterator;
	typedef vectype::size_type size_type;

public:
	iterator begin() {return _neighbours.begin();}
	const_iterator begin() const {return _neighbours.begin();}
	iterator end() {return _neighbours.end();}
	const_iterator end() const {return _neighbours.end();}

	reverse_iterator rbegin() {return _neighbours.rbegin();}
	const_reverse_iterator rbegin() const {return _neighbours.rbegin();}
	reverse_iterator rend() {return _neighbours.rend();}
	const_reverse_iterator rend() const {return _neighbours.rend();}

	size_type size() const {return _neighbours.size();}

public:
	/**
	 * A function that computes nearness to the centre of the neighbourhood.
	 */
	RealType nearness(const Neighbour& n) const {
		return annalyst::nearness(n, _radius, _neighbourhoodType);
	}

public:
	Neighbourhood(): _radius(0), _neighbourhoodType(NT_linear), _neighbours() {}

	/**
	 * Constructor.
	 * @param neighbours A vector of neighbours belonging to the neighbourhood.
	 * @param radius Radius of the neighbourhood.
	 * @param neighbourhoodType Type of the neighbourhood (for nearness).
	 */
	Neighbourhood(
		const std::vector<Neighbour>& neighbours,
		RealType radius, NeighbourhoodType neighbourhoodType
	): _radius(radius), _neighbourhoodType(neighbourhoodType),
	   _neighbours(neighbours) {}

	/**
	 * Constructor.
	 * @param neighbours A vector of neighbours belonging to the neighbourhood.
	 * @param radius Radius of the neighbourhood.
	 * @param neighbourhoodType Type of the neighbourhood (for nearness).
	 */
	Neighbourhood(
		std::vector<Neighbour>&& neighbours,
		RealType radius, NeighbourhoodType neighbourhoodType
	): _radius(radius), _neighbourhoodType(neighbourhoodType),
	   _neighbours(std::move(neighbours)) {}
};

/**
 * A class representing a topology. It can be used to create neighbourhoods.
 */
class Topology {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	//! Type of the topology.
	TopologyType _topologyType;
	//! Type of the neighbourhood (nearness function).
	NeighbourhoodType _neighbourhoodType;
	//! Number of rows in the grid.
	NetworkSize _numRows;
	//! Number of columns in the grid.
	NetworkSize _numCols;

public:
	 /**
	  * Computes grid coordinates of the specified neuron.
	  */
	std::pair<NetworkSize, NetworkSize> computeGridPosition(
		NetworkSize neuronIndex
	) const {
		return {neuronIndex / _numCols, neuronIndex % _numCols};
	}

	/**
	 * Converts grid position to neuron index.
	 */
	NetworkSize computeNeuronIndex(NetworkSize row, NetworkSize col) const {
		return row*_numCols + col;
	}

	/**
	 * Creates a neighbourhood with the specified centre and the specified
	 * radius.
	 */
	Neighbourhood createNeighbourhood(
		std::pair<NetworkSize, NetworkSize> centre, NetworkSize radius
	) const {
		return createNeighbourhood(centre.first, centre.second, radius);
	}

	//! Creates a neighbourhood with the specified centre (cm, cn) and radius.
	Neighbourhood createNeighbourhood(
		NetworkSize cm, NetworkSize cn, NetworkSize radius
	) const;

public:
	/**
	 * Returns the topology type.
	 */
	TopologyType getTopologyType() const {
		return _topologyType;
	}

	/**
	 * Sets the topology type.
	 */
	void setTopologyType(TopologyType topologyType) {
		_topologyType = topologyType;
	}

	/**
	 * Returns the neighbourhood type (nearness function).
	 */
	NeighbourhoodType getNeighbourhoodType() const {
		return _neighbourhoodType;
	}

	/**
	 * Sets the neighbourhood type (nearness function).
	 */
	void setNeighbourhoodType(NeighbourhoodType neighbourhoodType) {
		_neighbourhoodType = neighbourhoodType;
	}

	/**
	 * Returns the width of the grid.
	 */
	NetworkSize rows() const {
		return _numRows;
	}

	/**
	 * Returns the width of the grid.
	 */
	NetworkSize cols() const {
		return _numCols;
	}

	/**
	 * Returns the size of the grid underlying the topology (width * height).
	 */
	NetworkSize size() const {
		return _numCols*_numRows;
	}

	/**
	 * Sets the dimensions of the grid underlying the topology.
	 */
	void setDimensions(NetworkSize numRows, NetworkSize numCols) {
		_numRows = numRows;
		_numCols = numCols;
	}

public:
	Topology(): _topologyType(TOT_rectangular), _neighbourhoodType(NT_gaussian),
		_numRows(0), _numCols(0) {}

	/**
	 * Constructor.
	 * @param numCols Width of the grid.
	 * @param numRows Height of the grid.
	 * @param topologyType Type of the topology (how distance is computed).
	 * @param neighbourhoodType Type of the neighbourhood (how nearness
	 * is computed).
	 */
	Topology(
		NetworkSize numRows,
		NetworkSize numCols,
		TopologyType topologyType = TOT_rectangular,
		NeighbourhoodType neighbourhoodType = NT_gaussian
	): _topologyType(topologyType), _neighbourhoodType(neighbourhoodType),
		_numRows(numRows), _numCols(numCols) {}
};

} //namespace annalyst

#endif //Annalyst_Topology_H_
