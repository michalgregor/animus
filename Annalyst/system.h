#ifndef Annalyst_system_H_
#define Annalyst_system_H_

#include <vector>

#include <Systematic/system.h>
#include <Systematic/math/numeric_cast.h>
#include <Systematic/math/RealType.h>

namespace annalyst {
	//this is required so that shared_ptr and intrusive_ptr
	//are resolved unambiguously
	using systematic::shared_ptr;
	using systematic::intrusive_ptr;
	using systematic::make_shared;

	using namespace systematic;

	typedef std::vector<NetworkSize> SizeLayers;
} //namespace annalyst

#endif //Annalyst_system_H_
