#ifndef Annalyst_Network_SOM_H_
#define Annalyst_Network_SOM_H_

#include "algebra.h"
#include "Topology.h"

#include <Systematic/generator/Generator.h>

namespace annalyst {

/**
 * @class Network_SOM
 * @author Michal Gregor
 *
 * Implements a self-organizing map (SOM).
 **/
class ANNALYST_API Network_SOM {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version) const;

protected:
	Matrix _weights;
	Topology _topology;

protected:
	NetworkSize _numEpochs = 200;
	NetworkSize _stepsToShrink = 190;
	NetworkSize _initialRadius = 5;
	NetworkSize _finalRadius = 1;
	RealType _learningRate = 0.1_r;

public:
	NetworkSize getNumEpochs() const {return _numEpochs;}
	void setNumEpochs(NetworkSize numEpochs) {_numEpochs = numEpochs;}

	NetworkSize getStepsToShrink() const {return _stepsToShrink;}
	void setStepsToShrink(NetworkSize stepsToShrink) {_stepsToShrink = stepsToShrink;}

	NetworkSize getInitialRadius() const {return _initialRadius;}
	void setInitialRadius(NetworkSize initialRadius) {_initialRadius = initialRadius;}

	NetworkSize getFinalRadius() const {return _finalRadius;}
	void setFinalRadius(NetworkSize finalRadius) {_finalRadius = finalRadius;}

	RealType getLearningRate() const {return _learningRate;}
	void setLearningRate(RealType learningRate) {_learningRate = learningRate;}

public:
	//! Carries out a single epoch of learning.
	//! @param radius Radius of the neighbourhood.
	virtual void fit_epoch(const MatrixCRef& data, NetworkSize radius, RealType learningRate);

	virtual void fit(
		const MatrixCRef& data, NetworkSize numEpochs, NetworkSize stepsToShrink = 190,
		NetworkSize initialRadius = 5, NetworkSize finalRadius = 1,
		RealType learningRate = 0.1_r
	);

	virtual void fit(const MatrixCRef& data) {
		fit(data, _numEpochs, _stepsToShrink, _initialRadius, _finalRadius, _learningRate);
	}

public:
	Matrix computeUMatrix();
	Matrix computeHitMatrix(const MatrixCRef& data);

public:
	//! Returns the index of the winner neuron.
	virtual NetworkSize predict(const RowVectorCRef& input) const;
	//! Returns the index of the winner neuron for every row in the data.
	virtual std::vector<NetworkSize> predict(const MatrixCRef& input) const;

	//! Returns the distances for all the neurons.
	virtual RowVector predict_distances(const RowVectorCRef& input) const;
	//! Returns the distances for all the neurons and for every row in the data.
	virtual Matrix predict_distances(const MatrixCRef& input) const;

	AlgebraIndex numClusters() const {return _weights.rows();}
	AlgebraIndex numInputs() const {return _weights.cols();}

public:
	//! Returns the number of rows in the topology.
	NetworkSize numRows() const {return _topology.rows();}
	//! Returns the number of columns in the topology.
	NetworkSize numCols() const {return _topology.cols();}

	Topology& topology() {return _topology;}
	const Topology& topology() const {return _topology;}

public:
	//! \note You should not attempt to resize the weight matrix directly. You
	//! should instead use the resize method of the network.
	Matrix& weights() {return _weights;}
	const Matrix& weights() const {return _weights;}

	RowVectorRef clusterWeights(AlgebraIndex clusterNum) {
		return _weights.row(clusterNum);
	}

public:
	//! Initializes the weight matrix using the specified generator and also
	//! resizes the topology accordingly.
	void initialize(Generator<RowVector>& generator, NetworkSize numRows, NetworkSize numCols);

	//! Initializes the weight matrix using the specified generator and also
	//! resizes the topology accordingly.
	void initialize(Generator<RealType>& generator, NetworkSize numInputs, NetworkSize numRows, NetworkSize numCols);

	//! Initializes the cluster centres by picking random input items (rows)
	//! from the specified dataset. Also resize the topology accordingly.
	void initialize(const MatrixCRef& matrix, NetworkSize numRows, NetworkSize numCols);

public:
	Network_SOM();
	Network_SOM(Generator<RealType>& generator, NetworkSize numInputs, NetworkSize numRows, NetworkSize numCols);
	Network_SOM(Generator<RowVector>& generator, NetworkSize numRows, NetworkSize numCols);
	Network_SOM(const MatrixCRef& matrix, NetworkSize numRows, NetworkSize numCols);

	virtual ~Network_SOM() {}
};

} //namespace annalyst

SYS_EXPORT_CLASS(annalyst::Network_SOM)

#endif //Annalyst_Network_SOM_H_
