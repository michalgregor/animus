#include "Network_SOM.h"
#include <Systematic/generator/BoundaryGenerator.h>
#include <Systematic/math/Round.h>

namespace annalyst {

void Network_SOM::serialize(IArchive& ar, const unsigned int) {
	ar & _weights;
	ar & _topology;
}

void Network_SOM::serialize(OArchive& ar, const unsigned int) const {
	ar & _weights;
	ar & _topology;
}

Matrix Network_SOM::computeUMatrix() {
	NetworkSize rows = _topology.rows(), cols = _topology.cols();
	if(!rows || !cols) return Matrix(); // check for empty topologies

	// Initialize the U-matrix to all-zeros.
	Matrix matrix(2*rows - 1, 2*cols - 1);
	matrix.setZero();

	switch(_topology.getTopologyType()) {
	case TOT_rectangular: {
		for(NetworkSize r = 0; r < rows; r++) {
			for(NetworkSize c = 0; c < cols; c++) {
				RealType mean4 = 0;
				RealType mean4num = 0;

				// east
				if(c + 1 < cols) {
					mean4 += matrix.coeffRef(2*r, 2*c + 1) =
						(_weights.row(_topology.computeNeuronIndex(r, c)) -
							_weights.row(_topology.computeNeuronIndex(r, c + 1))
						).norm();
					mean4num += 1;
				}

				//south
				if(r + 1 < rows) {
					mean4 += matrix.coeffRef(2*r + 1, 2*c) =
						(_weights.row(_topology.computeNeuronIndex(r, c)) -
							_weights.row(_topology.computeNeuronIndex(r + 1, c))
						).norm();
					mean4num += 1;
				}

				// south-east
				if(r + 1 < rows && c + 1 < cols) {
					matrix.coeffRef(2*r + 1, 2*c + 1) = 0.5_r*((
						_weights.row(_topology.computeNeuronIndex(r, c)) -
						_weights.row(_topology.computeNeuronIndex(r + 1, c + 1))
					).norm() + (
						_weights.row(_topology.computeNeuronIndex(r + 1, c)) -
						_weights.row(_topology.computeNeuronIndex(r, c + 1))
					).norm());
				}

				// add other squares to the mean, if available
				if(r > 0) {
					mean4 += matrix.coeffRef(2*r - 1, 2*c);
					mean4num += 1;
				}

				if(c > 0) {
					mean4 += matrix.coeffRef(2*r, 2*c - 1);
					mean4num += 1;
				}

				// use the mean for the center
				matrix.coeffRef(2*r, 2*c) = mean4 / mean4num;
			}
		}

	} break;
	case TOT_hexagonal:
		// TODO: Implement umatrix for hexagonal topology.
	default:
		throw TracedError_NotImplemented("Method computeUMatrix not implemented for this topology type.");
	break;
	}

	return matrix;
}

Matrix Network_SOM::computeHitMatrix(const MatrixCRef& data) {
	Matrix hits(_topology.rows(), _topology.cols());
	hits.setZero();

	for(auto item: data) {
		auto pos = _topology.computeGridPosition(predict(item));
		hits.coeffRef(pos.first, pos.second) += 1;
	}

	return hits;
}

void Network_SOM::fit_epoch(const MatrixCRef& data, NetworkSize radius, RealType learningRate) {
	RealType scaledRate = learningRate / static_cast<RealType>(data.rows());

	for(auto item: data) {
		auto winnerPos = _topology.computeGridPosition(predict(item));
		auto neigh = _topology.createNeighbourhood(winnerPos, radius);

		for(auto& n: neigh) {
			_weights.row(n.neuronIndex) += neigh.nearness(n) * scaledRate *
				(item - _weights.row(n.neuronIndex));
		}
	}
}

void Network_SOM::fit(
	const MatrixCRef& data, NetworkSize numEpochs, NetworkSize stepsToShrink,
	NetworkSize initialRadius, NetworkSize finalRadius, RealType learningRate
) {
	SYS_THROW_ASSERT(initialRadius >= finalRadius, TracedError_InvalidArgument);

	if(initialRadius == finalRadius) {
		for(NetworkSize epoch = 0; epoch < numEpochs; epoch++) {
			fit_epoch(data, initialRadius, learningRate);
		}
	} else {
		RealType grad = (static_cast<RealType>(finalRadius + 1) -
			static_cast<RealType>(initialRadius)) / static_cast<RealType>(stepsToShrink);

		for(NetworkSize epoch = 0; epoch < numEpochs; epoch++) {
			NetworkSize radius = (epoch >= stepsToShrink) ? finalRadius :
				static_cast<NetworkSize>(Round(static_cast<RealType>(epoch) * grad +
				static_cast<RealType>(initialRadius)));

			fit_epoch(data, radius, learningRate);
		}
	}
}

NetworkSize Network_SOM::predict(const RowVectorCRef& input) const {
	NetworkSize minIndex = 0;
	RealType minDistance = (input - _weights.row(0)).norm();

	for(NetworkSize i = 1; i < _weights.rows(); i++) {
		RealType distance = (input - _weights.row(i)).norm();

		if(distance < minDistance) {
			minDistance = distance;
			minIndex = i;
		}
	}

	return minIndex;
}

std::vector<NetworkSize> Network_SOM::predict(const MatrixCRef& input) const {
	AlgebraIndex num = input.rows();
	std::vector<NetworkSize> res(num);

	for(AlgebraIndex j = 0; j < num; j++) {
		NetworkSize minIndex = 0;
		RealType minDistance = (input.row(j) - _weights.row(0)).norm();

		for(NetworkSize i = 1; i < _weights.rows(); i++) {
			RealType distance = (input.row(j) - _weights.row(i)).norm();

			if(distance < minDistance) {
				minDistance = distance;
				minIndex = i;
			}
		}

		res[j] = minIndex;
	}

	return res;
}

RowVector Network_SOM::predict_distances(const RowVectorCRef& input) const {
	RowVector res(_weights.rows());

	for(NetworkSize j = 0; j < _weights.rows(); j++) {
		res[j] = (input - _weights.row(j)).norm();
	}

	return res;
}

Matrix Network_SOM::predict_distances(const MatrixCRef& input) const {
	AlgebraIndex num = input.rows();
	Matrix res(num, _weights.rows());

	for(AlgebraIndex i = 0; i < num; i++) {
		for(NetworkSize j = 0; j < _weights.rows(); j++) {
			res.row(i)[j] = (input.row(i) - _weights.row(j)).norm();
		}
	}

	return res;
}

void Network_SOM::initialize(Generator<RowVector>& generator, NetworkSize numRows, NetworkSize numCols) {
	NetworkSize numClusters = numRows * numCols;

	if(!numClusters) {
		_weights.resize(0, 0);
		return;
	}

	_topology.setDimensions(numRows, numCols);

	RowVector row = generator();
	_weights.resize(numClusters, row.size());
	_weights.row(0) = row;

	for(NetworkSize i = 1; i < numClusters; i++) {
		_weights.row(i) = generator();
	}
}

void Network_SOM::initialize(Generator<RealType>& generator, NetworkSize numInputs, NetworkSize numRows, NetworkSize numCols) {
	NetworkSize numClusters = numRows * numCols;
	_topology.setDimensions(numRows, numCols);
	_weights.resize(numClusters, numInputs);

	for(NetworkSize i = 0; i < numClusters; i++) {
		for(NetworkSize j = 0; j < numInputs; j++) {
			_weights(i, j) = generator();
		}
	}
}

void Network_SOM::initialize(const MatrixCRef& matrix, NetworkSize numRows, NetworkSize numCols) {
	NetworkSize numClusters = numRows * numCols;
	_topology.setDimensions(numRows, numCols);

	BoundaryGenerator<AlgebraIndex> gen(0, matrix.rows() - 1);
	_weights.resize(numClusters, matrix.cols());

	for(NetworkSize i = 0; i < numClusters; i++) {
		_weights.row(i) = matrix.row(gen());
	}
}

Network_SOM::Network_SOM(): _weights() {}

Network_SOM::Network_SOM(
	Generator<RealType>& generator,
	NetworkSize numInputs,
	NetworkSize numRows,
	NetworkSize numCols
): _weights(), _topology() {
	initialize(generator, numInputs, numRows, numCols);
}

Network_SOM::Network_SOM(
	Generator<RowVector>& generator,
	NetworkSize numRows,
	NetworkSize numCols
): _weights(), _topology() {
	initialize(generator, numRows, numCols);
}

Network_SOM::Network_SOM(
	const MatrixCRef& matrix,
	NetworkSize numRows,
	NetworkSize numCols
): _weights(), _topology() {
	initialize(matrix, numRows, numCols);
}

} // namespace annalyst
