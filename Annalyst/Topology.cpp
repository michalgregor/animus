#include "Topology.h"
#include <boost/serialization/vector.hpp>

namespace annalyst {

void Neighbour::serialize(IArchive& ar, const unsigned int /*version*/) {
	ar & m;
	ar & n;
	ar & neuronIndex;
	ar & distance;
}

void Neighbour::serialize(OArchive& ar, const unsigned int /*version*/) {
	ar & m;
	ar & n;
	ar & neuronIndex;
	ar & distance;
}

void Neighbourhood::serialize(IArchive& ar, const unsigned int /*version*/) {
	ar & _radius;
	ar & _neighbourhoodType;
	ar & _neighbours;
}

void Neighbourhood::serialize(OArchive& ar, const unsigned int /*version*/) {
	ar & _radius;
	ar & _neighbourhoodType;
	ar & _neighbours;
}

void Topology::serialize(IArchive& ar, const unsigned int /*version*/) {
	ar & _topologyType;
	ar & _neighbourhoodType;
	ar & _numRows;
	ar & _numCols;
}

void Topology::serialize(OArchive& ar, const unsigned int /*version*/) {
	ar & _topologyType;
	ar & _neighbourhoodType;
	ar & _numRows;
	ar & _numCols;
}

RealType nearness(
	const Neighbour& n,
	RealType radius,
	NeighbourhoodType ntype
) {
	RealType dist = static_cast<RealType>(n.distance);

	SYS_WFLOAT_EQUAL_OFF
	if(!radius) return 1;
	SYS_WFLOAT_EQUAL_ON

	switch(ntype) {
	case NT_linear:
		return std::max(1 - dist / radius * 0.9_r, 0_r);
	break;
	case NT_gaussian:
		return std::exp(-dist*dist/radius);
	break;
	case NT_unitary:
		return 1_r;
	break;
	default:
		SYS_ALWAYS_ASSERT_MSG(false, "Unknown neighbourhood type.")
	break;
	}

	return 0;
}

//! Creates a neighbourhood with the specified centre (cm, cn) and radius.
Neighbourhood Topology::createNeighbourhood(
	NetworkSize cm, NetworkSize cn, NetworkSize radius
) const {
	std::vector<Neighbour> neighbours;

	switch(_topologyType) {
	case TOT_hexagonal:
		NetworkSizeDiff cx, cy, cz;
		rect2hex(cm, cn, cx, cy, cz);

		for(NetworkSizeDiff i = 0; i <= static_cast<NetworkSizeDiff>(radius); i++) {
			for(NetworkSizeDiff j = cx - static_cast<NetworkSizeDiff>(radius); j <= cx + static_cast<NetworkSizeDiff>(radius) - i; j++) {
				NetworkSizeDiff y = cy + i;
				NetworkSizeDiff x = j;
				NetworkSizeDiff z = -x - y;

				NetworkSizeDiff m, n;
				hex2rect(x, y, z, m, n);
				NetworkSize index = computeNeuronIndex(m, n);

				if(m >= 0 && n >= 0 &&
					static_cast<NetworkSize>(m) < _numRows &&
					static_cast<NetworkSize>(n) < _numCols
				) {
					NetworkSize distance = (std::abs(cx - x) + std::abs(cy - y) + std::abs(cz - z))/2;
					neighbours.push_back(Neighbour(m, n, index, distance));
				}
			}
		}

		for(NetworkSizeDiff i = 1; i <= static_cast<NetworkSizeDiff>(radius); i++) {
			for(NetworkSizeDiff j = cx - static_cast<NetworkSizeDiff>(radius) + i; j <= cx + static_cast<NetworkSizeDiff>(radius); j++) {
				NetworkSizeDiff y = cy - i;
				NetworkSizeDiff x = j;
				NetworkSizeDiff z = -x - y;

				NetworkSizeDiff m, n;
				hex2rect(x, y, z, m, n);
				NetworkSize index = computeNeuronIndex(m, n);

				if(m >= 0 && n >= 0 &&
					static_cast<NetworkSize>(m) < _numRows &&
					static_cast<NetworkSize>(n) < _numCols
				) {
					NetworkSize distance = (std::abs(cx - x) + std::abs(cy - y) + std::abs(cz - z))/2;
					neighbours.push_back(Neighbour(m, n, index, distance));
				}
			}
		}

	break;
	case TOT_rectangular: {
		NetworkSizeDiff ibegin = std::min(static_cast<NetworkSize>(std::max(static_cast<NetworkSizeDiff>(cm) - static_cast<NetworkSizeDiff>(radius), 0)), _numRows);
		NetworkSizeDiff iend = std::min(std::max(cm + radius + 1, 0u), _numRows);

		NetworkSizeDiff jbegin = std::min(static_cast<NetworkSize>(std::max(static_cast<NetworkSizeDiff>(cn) - static_cast<NetworkSizeDiff>(radius), 0)), _numCols);
		NetworkSizeDiff jend = std::min(std::max(cn + radius + 1, 0u), _numCols);

		for(NetworkSizeDiff i = ibegin; i <  iend; i++) {
			for(NetworkSizeDiff j = jbegin; j < jend; j++) {
				NetworkSize index = computeNeuronIndex(i, j);
				neighbours.push_back(Neighbour(i, j, index,
					std::max(std::abs(i - static_cast<NetworkSizeDiff>(cm)),
					std::abs(j - static_cast<NetworkSizeDiff>(cn)))));
			}
		}
	} break;
	default:
		SYS_ALWAYS_ASSERT_MSG(false, "Unknown topology type.")
	break;
	}

	return Neighbourhood(neighbours, static_cast<RealType>(radius), _neighbourhoodType);
}

} // namespace annalyst
