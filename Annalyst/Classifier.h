#ifndef ANNALYST_CLASSIFIER_H_
#define ANNALYST_CLASSIFIER_H_

#include "algebra.h"

namespace annalyst {

class Classifier {
public:
	virtual void fit(const MatrixCRef& input, const MatrixCRef& output) = 0;
	virtual Matrix predict(const MatrixCRef& input) = 0;

public:
	virtual ~Classifier() = default;
};

} // namespace annalyst

#endif /* ANNALYST_CLASSIFIER_H_ */
