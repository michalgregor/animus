#ifndef Annalyst_Network_HSOM_H_
#define Annalyst_Network_HSOM_H_

#include "system.h"
#include "Network_SOM.h"

namespace annalyst {

/**
 * @class Network_HSOM
 * @author Michal Gregor
 *
 * This class provides a habituated self-organising map. It builds upon
 * Network_SOM by adding a habituation layer.
 **/
class ANNALYST_API Network_HSOM: public Network_SOM {
private:
	friend class boost::serialization::access;

	void serialize(IArchive& ar, const unsigned int version);
	void serialize(OArchive& ar, const unsigned int version);

private:
	RealType _habituationRate = 0.1_r;
	RealType _recoveryRate = 0.7_r;
	bool _normalize = true;
	NetworkSize _habituationNeighbourhoodRadius = 2;

	RowVector _habituableWeights;

	//! The initial weight of habituable synapse.
	static constexpr RealType _initWeight = 1.0_r;
	//! The minimum weight after habituation.
	static constexpr RealType _minWeight = 0.0_r;
	//! The maximum weight after habituation.
	static constexpr RealType _maxWeight = 1.0_r;

public:
	RealType getHabituationRate() const {return _habituationRate;}
	void setHabituationRate(RealType habituationRate) {_habituationRate = habituationRate;}
	RealType getRecoveryRate() const {return _recoveryRate;}
	void setRecoveryRate(RealType recoveryRate) {_recoveryRate = recoveryRate;}

	//! Whether the output should be normalized so that the maximum is 1.
	bool getIsNormalized() const {return _normalize;}
	//! Whether the output should be normalized so that the maximum is 1.
	void setIsNormalized(bool normalize) {_normalize = normalize;}

	NetworkSize getHabitutationNeighbourhoodRadius() const {
		return _habituationNeighbourhoodRadius;
	}

	void setHabitutationNeighbourhoodRadius(NetworkSize radius) {
		_habituationNeighbourhoodRadius = radius;
	}

public:
	//! Resets habituation, clearing the memory of the network.
	void resetHabituation();

	//! Shows the specified data to HSOM and makes it habituate to it.
	//! @return Returns the novelty.
	RealType habituate(const RowVectorCRef& data);
	//! Shows the specified data to HSOM row by row and makes it habituate.
	//! @returns The novelty of all the individual rows.
	RowVector habituate(const MatrixCRef& data);
	//! Makes the HSOM to habituate to the specified winner neuron (and its
	//! neighbourhood).
	RealType habituate(NetworkSize winner);

	//! Computes the novelty of the specified vector (without doing habituation).
	RealType computeNovelty(const RowVectorCRef& data) const;

	//! Computes the novelty for each row in the data (without doing habituation).
	RowVector computeNovelty(const MatrixCRef& data) const;

	//! Computes the novelty for the specified winner neuron (without doing
	//! habituation).
	RealType computeNovelty(NetworkSize winner) const;

	//! Returns the level of habituation for each neuron in the topology.
	//! @note This function does not effect habituation.
	Matrix computeHabituationMatrix() const;

public:
	Network_HSOM();
	Network_HSOM(Generator<RealType>& generator, NetworkSize numInputs, NetworkSize numRows, NetworkSize numCols);
	Network_HSOM(Generator<RowVector>& generator, NetworkSize numRows, NetworkSize numCols);
	Network_HSOM(const MatrixCRef& matrix, NetworkSize numRows, NetworkSize numCols);

	virtual ~Network_HSOM() = default;
};

} //namespace annalyst

SYS_EXPORT_CLASS(annalyst::Network_HSOM)

#endif //Annalyst_Network_HSOM_H_
